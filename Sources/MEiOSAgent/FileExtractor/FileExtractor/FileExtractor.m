#include "FileExtractor.h"

#include <dlfcn.h>
#include <mach-o/nlist.h>
#include <mach-o/dyld.h>
#include <mach-o/dyld_images.h>

struct MessageStart
{
    mach_msg_header_t header;
    int u1;
    int u2;
    int u3;
    int entPortType;
    int entPort;
    int entPortSize;
    int urlType;
    const UInt8 * url;
    int urlSize;
    int userType;
    const char *user;
    int userSize;
    int optionsType;
    const char *options;
    int optionsSize;
    int NDR_recordType;
    int NDR_record;
    int NDR_record_int_rep;
    char appId[256];
    char docId[256];
    int urlSize_;
    int userSize_;
    int optionsSize_;
};

int _MyLSPluginClient_StartOpenOperation(int serverPort, int a2, int entPort, const char *appId, const char *docId, const UInt8 *url, int urlSize, char *user, int userSize, char *options, int optionsSize, int *res)
{
    struct MessageStart msg;
    
    
    msg.u1 = 5;
    msg.u2 = a2;
    msg.entPortType = 0x110000;
    msg.entPort = entPort;
    msg.urlType = 0x100000;
    msg.url = url;
    msg.urlSize = urlSize;
    msg.userType = 0x1000100;
    msg.user = user;
    msg.userSize = userSize;
    msg.optionsType = 0x1000100;
    msg.options = options;
    msg.optionsSize = optionsSize;
    msg.NDR_recordType = 0x1000100;
    msg.NDR_record_int_rep = *(int *)&NDR_record.int_rep;
    msg.NDR_record = *(int *)&NDR_record;
    mig_strncpy(msg.appId, appId, 256);
    mig_strncpy(msg.docId, docId, 256);
    msg.urlSize_ = urlSize;
    msg.userSize_ = userSize;
    msg.optionsSize_ = optionsSize;
    msg.header.msgh_bits = 0x80001513;
    msg.header.msgh_remote_port = serverPort;
    msg.header.msgh_local_port = mig_get_reply_port();
    msg.header.msgh_id = 14009;
    mach_msg_return_t result = mach_msg(&msg.header, 3, sizeof(msg), 0x30u, msg.header.msgh_local_port, 0, 0);
    if ( (unsigned int)(result - 0x10000002) >= 2 )
    {
        if ( !result )
        {
            if ( msg.header.msgh_id != 71
                && msg.header.msgh_id == 14109
                && (msg.header.msgh_bits & 0x80000000) == 0
                && msg.header.msgh_size == 40
                && !msg.u3 )
                *res = msg.entPortType;
            return 0;
        }
        if ( result != 0x10000010 )
        {
            return result;
        }
    }
    mig_put_reply_port(msg.header.msgh_local_port);
    return result;
}

mach_port_t _MyLSPluginGetServerPort_sServerPort = 0;

#define	BOOTSTRAP_MAX_NAME_LEN			128
typedef char name_t[BOOTSTRAP_MAX_NAME_LEN];

extern kern_return_t
bootstrap_look_up(mach_port_t bp, const name_t service_name, mach_port_t *sp);

mach_port_t _MyLSPluginGetServerPort(int reset)
{
    if ( reset )
    {
        if ( _MyLSPluginGetServerPort_sServerPort )
        {
            mach_port_destroy(mach_task_self_, _MyLSPluginGetServerPort_sServerPort);
            _MyLSPluginGetServerPort_sServerPort = 0;
        }
    }
    else
    {
        if ( _MyLSPluginGetServerPort_sServerPort )
        {
            return _MyLSPluginGetServerPort_sServerPort;
        }
    }
    
    if ( bootstrap_look_up(bootstrap_port, "com.apple.lsd", &_MyLSPluginGetServerPort_sServerPort) )
    {
        _MyLSPluginGetServerPort_sServerPort = 0;
    }
    return _MyLSPluginGetServerPort_sServerPort;
}

void * handleSpringBoardServices = NULL;

typedef int (*SBSCreateClientEntitlementEnforcementPortF)();

SBSCreateClientEntitlementEnforcementPortF SBSCreateClientEntitlementEnforcementPort_f = NULL;

typedef int (*SBSCleanupClientEntitlementEnforcementPortF)(int port);

SBSCleanupClientEntitlementEnforcementPortF SBSCleanupClientEntitlementEnforcementPort_f = NULL;

int  MySBSCreateClientEntitlementEnforcementPort()
{
    if ( !SBSCreateClientEntitlementEnforcementPort_f )
    {
        if (!handleSpringBoardServices)
        {
            handleSpringBoardServices = dlopen("/System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices", RTLD_LOCAL | RTLD_LAZY);
        }
        if (handleSpringBoardServices)
        {
            SBSCreateClientEntitlementEnforcementPort_f = (SBSCreateClientEntitlementEnforcementPortF)dlsym(handleSpringBoardServices, "SBSCreateClientEntitlementEnforcementPort");
        }
        else
        {
            SBSCreateClientEntitlementEnforcementPort_f = NULL;
        }
    }
    return SBSCreateClientEntitlementEnforcementPort_f();
}

int MySBSCleanupClientEntitlementEnforcementPort(int port)
{
    if ( !SBSCleanupClientEntitlementEnforcementPort_f )
    {
        if (!handleSpringBoardServices)
        {
            handleSpringBoardServices = dlopen("/System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices", RTLD_LOCAL | RTLD_LAZY);
        }
        if (handleSpringBoardServices)
        {
            SBSCleanupClientEntitlementEnforcementPort_f = (SBSCreateClientEntitlementEnforcementPortF)dlsym(handleSpringBoardServices, "SBSCleanupClientEntitlementEnforcementPort");
        }
        else
        {
            SBSCleanupClientEntitlementEnforcementPort_f = NULL;
        }
    }
    return SBSCleanupClientEntitlementEnforcementPort_f(port);
}



int _MyLSStartOpenOperation(CFURLRef url, CFStringRef appId)
{
    int serverPort = _MyLSPluginGetServerPort(0);
    
    if ( serverPort )
    {
        
        int enforcementPort = MySBSCreateClientEntitlementEnforcementPort();
        
        
        CFDataRef urlData = CFURLCreateData(kCFAllocatorDefault, url, kCFStringEncodingUTF8, YES);
        char appIdArray[256];
        char docIdArray[256];
        docIdArray[0] = 0;
        
        if ( !appId || CFStringGetCString(appId, appIdArray, 256, kCFStringEncodingUTF8) != YES)
        {
            appIdArray[0] = 0;
        }
        
        const UInt8* urlDataArray = 0;
        int urlDataArraySize = 0;
        if ( urlData )
        {
            urlDataArray = CFDataGetBytePtr(urlData);
            urlDataArraySize = CFDataGetLength(urlData);
        }
        int result = 0;
        
        if ( _MyLSPluginClient_StartOpenOperation(
                                                  serverPort,
                                                  0,
                                                  enforcementPort,
                                                  appIdArray,
                                                  docIdArray,
                                                  urlDataArray,
                                                  urlDataArraySize,
                                                  0,
                                                  0,
                                                  0,
                                                  0,
                                                  &result) )
        {
            int srca = _MyLSPluginGetServerPort(1);
            if ( !srca )
            {
                MySBSCleanupClientEntitlementEnforcementPort(enforcementPort);
                if ( urlData )
                {
                    CFRelease(urlData);
                }
                return result;
            }
            if ( urlData )
            {
                urlDataArray = CFDataGetBytePtr(urlData);
                urlDataArraySize = CFDataGetLength(urlData);
            }
            
            
            if ( _MyLSPluginClient_StartOpenOperation(
                                                      srca,
                                                      0,
                                                      enforcementPort,
                                                      appIdArray,
                                                      docIdArray,
                                                      urlDataArray,
                                                      urlDataArraySize,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      &result) )
            {
                MySBSCleanupClientEntitlementEnforcementPort(enforcementPort);
                if ( urlData )
                {
                    CFRelease(urlData);
                }
                
                return result;
            }
        }
        
        return result;
    }
    
    return -1;
}

struct MessageFinish
{
    mach_msg_header_t header;
    int u1;
    int u2;
    int u3;
    int urlType;
    const UInt8 * url;
    int urlSize;
    int NDR_recordType;
    int NDR_record;
    int NDR_record_int_rep;
    int urlSize_;
};

int _MyLSPluginClient_FinishOpenOperation(mach_port_t port, int a2, const UInt8 *urlArray, int urlSize, int *a5)
{
    struct MessageFinish msg;
    
    msg.u1 = 2;
    msg.u2 = a2;
    msg.urlType = 0x110000;
    msg.url = urlArray;
    msg.urlSize = urlSize;
    msg.NDR_recordType = 0x1000100;
    msg.NDR_record_int_rep = *(int *)&NDR_record.int_rep;
    msg.NDR_record = *(int *)&NDR_record;
    msg.urlSize_ = urlSize;
    msg.header.msgh_bits = 0x80001513;
    msg.header.msgh_remote_port = port;
    msg.header.msgh_local_port = mig_get_reply_port();
    msg.header.msgh_id = 14010;
    mach_msg_return_t res = mach_msg(&msg.header, 3, sizeof(msg), 0x30u, msg.header.msgh_local_port, 0, 0);
    if ( (unsigned int)(res - 0x10000002) < 2 )
    {
        mig_put_reply_port(msg.header.msgh_local_port);
        return res;
    }
    if ( res )
    {
        if ( res != 0x10000010 )
        {
            mig_dealloc_reply_port(msg.header.msgh_local_port);
            return res;
        }
        mig_put_reply_port(msg.header.msgh_local_port);
        return res;
    }
    if ( msg.header.msgh_id == 71 )
        return -308;
    if ( msg.header.msgh_id != 14110 )
        return -301;
    if ( (msg.header.msgh_bits & 0x80000000) != 0 )
        return -300;
    if ( msg.header.msgh_size == 0x28 )
    {
        if ( !msg.u3 )
        {
            res = 0;
            *a5 = msg.urlType;
        }
        return res;
    }
    if ( msg.header.msgh_size != 36 )
        return -300;
    res = msg.u3;
    if ( !msg.u3 )
        res = -300;
    return res;
}

int _MyLSFinishOpenOperation(CFURLRef url)
{
    mach_port_t serverPort = _MyLSPluginGetServerPort(0);
    if ( serverPort == 0 || url == NULL)
    {
        return 61;
    }
    else
    {
        CFDataRef urlData = CFURLCreateData(kCFAllocatorDefault, url, kCFStringEncodingUTF8, YES);
        const UInt8 * urlDataArray = CFDataGetBytePtr(urlData);
        int urlDataSize = CFDataGetLength(urlData);
        int res = 0;
        if ( _MyLSPluginClient_FinishOpenOperation(serverPort, 0, urlDataArray, urlDataSize, &res) )
        {
            serverPort = _MyLSPluginGetServerPort(1);
            if (serverPort != 0)
            {
                urlDataArray = CFDataGetBytePtr(urlData);
                if (urlDataArray != 0)
                {
                    urlDataSize = CFDataGetLength(urlData);
                    if (urlDataSize != 0)
                    {
                        if (_MyLSPluginClient_FinishOpenOperation(serverPort, 0, urlDataArray, urlDataSize, &res))
                        {
                            res = 61;
                        }
                        
                    }
                }
            }
        }
        if ( urlData )
        {
            CFRelease(urlData);
        }
        return res;
    }
}

BOOL ExtractFile(const char* filePath, const char* appDest)
{
    CFStringRef cfURLString = CFStringCreateWithCString(kCFAllocatorDefault, filePath, kCFStringEncodingUTF8);
    CFURLRef cfURL = CFURLCreateWithString(kCFAllocatorDefault, cfURLString, NULL);
    CFStringRef cfAppId = CFStringCreateWithCString(kCFAllocatorDefault, appDest, kCFStringEncodingUTF8);
    if (_MyLSStartOpenOperation(cfURL, cfAppId) == 36)
    {
        //_MyLSFinishOpenOperation(cfURL);
        return YES;
    }
    return NO;
}

