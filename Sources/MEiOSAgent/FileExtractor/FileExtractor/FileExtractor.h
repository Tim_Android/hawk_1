#ifndef FILE_EXTRACTOR_H
#define FILE_EXTRACTOR_H

#ifdef __cplusplus
extern "C" {
#endif
    
    BOOL ExtractFile(const char* filePath, const char* appDest);
    
#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif // FILE_EXTRACTOR_H
