#! /bin/bash

set -e

BUILD_DIR="./build_dir"
APP_NAME="Hawk"
APPLICATIONS_DIR="$BUILD_DIR/$APP_NAME/Applications"
BUNDLE_NAME="${APP_NAME}.app"
XCODE_BUILD_DIR="./iOS Settings/build/Release-iphoneos"
CYDIA_FILES_DIR="./CydiaPackageHelpers"
DEB_PACKAGE_NAME="${APP_NAME}.deb"

rm -Rf "$BUILD_DIR"

#
# Build Hawk
#

xcodebuild -workspace "MEiOSAgent.xcworkspace" -scheme "$APP_NAME" -sdk "iphoneos" -configuration "Release"

#
# Prepare build directory and additional files
#

mkdir "$BUILD_DIR"
mkdir -p "$APPLICATIONS_DIR"

cp -R "$XCODE_BUILD_DIR/$BUNDLE_NAME" "$APPLICATIONS_DIR"
rm "$APPLICATIONS_DIR/$BUNDLE_NAME/embedded.mobileprovision"

cp -R "$CYDIA_FILES_DIR/Documents" "$APPLICATIONS_DIR/$BUNDLE_NAME"
chmod -R a+w "$APPLICATIONS_DIR/$BUNDLE_NAME/Documents/"

find "$BUILD_DIR" -name '*.DS_Store' -type f -delete

cp -R "$CYDIA_FILES_DIR/DEBIAN" "$BUILD_DIR/$APP_NAME"
cp "$CYDIA_FILES_DIR/dpkg-deb" "$BUILD_DIR/"
cp "$CYDIA_FILES_DIR/CydiaIcon.png" "$BUILD_DIR/"
cp "$CYDIA_FILES_DIR/Packages" "$BUILD_DIR/Packages_template"

#
# Generate deb-package
#

cd "$BUILD_DIR"

./dpkg-deb -b "$APP_NAME"

#
# Generate correct "Packages" file
#

DEB_MD5=`md5 $DEB_PACKAGE_NAME | awk '{print $4}'`
DEB_SIZE=`wc -c $DEB_PACKAGE_NAME | awk '{print $1}'`

sed -e "s/Size: [0-9]*/Size: $DEB_SIZE/g" -e "s/MD5sum: [0-9,a-z]*/MD5sum: $DEB_MD5/g" "Packages_template" > Packages

bzip2 Packages

#
# Cleanup
#

rm -R "Hawk"
rm -R "dpkg-deb"
rm -R "Packages_template"
