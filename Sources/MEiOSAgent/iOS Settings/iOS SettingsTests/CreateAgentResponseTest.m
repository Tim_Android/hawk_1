#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>
#import "CreateAgentResponse.h"

@interface CreateAgentResponseTest : XCTestCase

@end

@implementation CreateAgentResponseTest

- (void)testCreateAgentResponseInitWithNil
{
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:nil];
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init from nil");
}

- (void)testCreateAgentResponseInitWithEmptyData
{
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[[NSData alloc] init]];
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init from empty buffer");
}

- (void)testCreateAgentResponseCorruptedData
{
    NSString *inputData = @"{\"agentId\":null,\"st";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init from incorrect JSON data");
}

- (void)testCreateAgentResponseWithoutRequiredElement
{
    NSString *inputData = @"{\"state\":0}"; //CreateAgentResponse requires "agentId" JSON element, but it is missing here
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init with missing of required JSON element");
}

- (void)testCreateAgentResponseWithInvalidType1
{
    NSString *inputData = @"{\"agentId\":[],\"state\":0}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init with ARRAY JSON element");
}
- (void)testCreateAgentResponseWithInvalidType2
{
    NSString *inputData = @"{\"agentId\":\"STRING_VALUE\",\"state\":0}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"CreateAgentResponse must be nil after init with STRING JSON element");
}

- (void)testCreateAgentResponseSuccessWithNullResult
{
    NSString *inputData = @"{\"agentId\":null,\"state\":0}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"CreateAgentResponse is not initialized with correct JSON data");
    XCTAssertNil(response.agentId, @"CreateAgentResponse agentId must be nil, if response contains explicit null value");
}

- (void)testCreateAgentResponseSuccessWithNullResult2
{
    //Test: if authentication state is SUCCESSFUL, but agentId is NIL - the response must be considered as correct (CreateAgentResponse isn't responsible for that)
    NSString *inputData = @"{\"agentId\":null,\"state\":1}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"CreateAgentResponse is not initialized with correct JSON data");
    XCTAssertNil(response.agentId, @"CreateAgentResponse agentId must be nil, if response contains explicit null value");
}

- (void)testCreateAgentResponseSuccess
{
    NSString *inputData = @"{\"agentId\":56,\"state\":1}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"CreateAgentResponse is not initialized with correct JSON data");
    XCTAssert([response.agentId isEqualToString:@"56"]);
}

@end
