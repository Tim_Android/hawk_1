#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "ILoginChecker.h"
#import "MobileEnterpriseCore.h"
#import "CreateAgentResponse.h"
#import "RestClient.h"
#import "GlobalCfg.h"


//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


#define kFileNameContainingAgentId @"AgentId"
#define kDefaultLogin @"login"
#define kDefaultPassword @"password"


@interface MobileEnterpriseCore (CategoryForTesting)

//we must redeclare private methods here to make possible adding stubs for them
//otherwise they are invisible for compiler
//don't worry, if we create a stub for not existing method, OCMock will show a runtime error - it won't be skipped

- (RestClient *)restClient;
- (void)setIsAgentReadyToWork:(BOOL)isAgentReadyToWork;
- (BOOL)readAgentIdUsingDocumentsPath:(NSString *)documentsPath;
- (BOOL)saveAgentIdUsingDocumentsPath:(NSString *)documentsPath;
- (void)activateAgent;

@end


@interface CoreAuthenticationTest : XCTestCase

@property (strong, nonatomic) NSString *agentIdFilePath;

@end

@implementation CoreAuthenticationTest

- (void)setUp
{
    NSString *docsDir = [[GlobalCfg sharedInstance] docsDir];
    _agentIdFilePath = [docsDir stringByAppendingPathComponent:kFileNameContainingAgentId];
}

#pragma mark Tests of init and isAgentReadyToWork

- (void)testInit_IsAgentReadyToWork_YES
{
    //we cannot create a stub for methods called from inside of init method
    //that's why let's use environment changes for purposes of this test
    
    NSString *agentId = @"14";
    NSError *err = nil;
    BOOL isWriteSucceeded = [agentId writeToFile:_agentIdFilePath atomically:NO encoding:NSUTF8StringEncoding error:&err];
    if (!isWriteSucceeded || err)
    {
        XCTFail(@"Failed to create AgentId file");
    }
    
    //perform actions
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    
    //check results
    XCTAssertNotNil(core);
    XCTAssert(core.isAgentReadyToWork, @"If AgentId file exists and not empty, agent must be ready to work");
}

- (void)testInit_IsAgentReadyToWork_NO
{
    //we cannot create a stub for methods called from inside of init method
    //that's why let's use environment changes for purposes of this test

    if ([[NSFileManager defaultManager] fileExistsAtPath:_agentIdFilePath])
    {
        NSError *err = nil;
        BOOL isDeleteSucceeded = [[NSFileManager defaultManager] removeItemAtPath:_agentIdFilePath error:&err];
        if (!isDeleteSucceeded || err)
        {
            XCTFail(@"AgentId file must not exist for this test. Deleting of this file has failed.");
        }
    }
    
    //perform actions
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    
    //check results
    XCTAssertNotNil(core);
    XCTAssert(!core.isAgentReadyToWork, @"If AgentId file doesn't exist, agent must NOT be ready to work");
}

- (void)testInit_IsAgentReadyToWork_NO_EmptyFile
{
    //we cannot create a stub for methods called from inside of init method
    //that's why let's use environment changes for purposes of this test
    
    NSString *emptyStr = @"";
    NSError *err = nil;
    BOOL isWriteSucceeded = [emptyStr writeToFile:_agentIdFilePath atomically:NO encoding:NSUTF8StringEncoding error:&err];
    if (!isWriteSucceeded || err)
    {
        XCTFail(@"Failed to create empty AgentId file");
    }
    
    //perform actions
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    
    //check results
    XCTAssertNotNil(core);
    XCTAssert(!core.isAgentReadyToWork, @"If AgentId file is empty, agent must NOT be ready to work");
}

#pragma mark Test for unreachable host (authenticateWithLogin:withPassword: method)

- (void)testAuthenticate_HostUnreachable
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(NO);

    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorNoInternetConnection);
    
    OCMVerify([mockRestClient isHostReachable]);
}

#pragma mark Tests for Authentication branch of authenticateWithLogin:withPassword: method

- (void)testAuthenticate_Success
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(YES); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"state\":1}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]).andReturn(response);
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, CredentialsAreCorrect);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]);
}

- (void)testAuthenticate_WrongCredentials
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(YES); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"state\":0}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]).andReturn(response);
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, CredentialsAreWrong);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]);
}

- (void)testAuthenticate_ServerError
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(YES); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"state\":2}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]).andReturn(response);
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternalOnServer);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]);
}

- (void)testAuthenticate_ErrorInternal
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(YES); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    OCMStub([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]).andReturn(nil);
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternal);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient authenticate:kDefaultLogin password:kDefaultPassword]);
}

#pragma mark Tests for CreateAgent branch of authenticateWithLogin:withPassword: method

- (void)testCreateAgent_Success
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockCore setUninstallationEnabled:NO]).andReturn(YES);
    OCMStub([mockCore saveAgentIdUsingDocumentsPath:[OCMArg any]]).andReturn(YES);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"agentId\":14,\"state\":1}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(response);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Uninstallation prevention feature must not be called"); }] setUninstallationEnabled:NO];
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Uninstallation prevention feature must not be called"); }] setUninstallationEnabled:YES];

    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, CredentialsAreCorrect);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
    OCMVerify([mockRestClient setAgentId:@"14"]);
    OCMVerify([mockCore saveAgentIdUsingDocumentsPath:[OCMArg any]]);
    OCMVerify([mockCore activateAgent]);
}

- (void)testCreateAgent_SuccessButAgentIdIsNull
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"agentId\":null,\"state\":1}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(response);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Agent must not be activated"); }] activateAgent];
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternal);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
}

- (void)testCreateAgent_NotSavedAgentId
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockCore saveAgentIdUsingDocumentsPath:[OCMArg any]]).andReturn(NO);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"agentId\":14,\"state\":1}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(response);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Agent must not be activated"); }] activateAgent];
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternal);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
    OCMVerify([mockCore saveAgentIdUsingDocumentsPath:[OCMArg any]]);
}

- (void)testCreateAgent_WrongCredentials
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"agentId\":null,\"state\":0}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(response);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Agent must not be activated"); }] activateAgent];
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"AgentId must not be saved"); }] saveAgentIdUsingDocumentsPath:[OCMArg any]];
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, CredentialsAreWrong);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
}

- (void)testCreateAgent_ServerError
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    NSString *inputData = @"{\"agentId\":null,\"state\":2}";
    CreateAgentResponse *response = [[CreateAgentResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(response);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Agent must not be activated"); }] activateAgent];
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"AgentId must not be saved"); }] saveAgentIdUsingDocumentsPath:[OCMArg any]];
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternalOnServer);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
}

- (void)testCreateAgent_ErrorInternal
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    id mockRestClient = OCMPartialMock(core.restClient);
    
    OCMStub([mockCore isAgentReadyToWork]).andReturn(NO); //set correct decision: YES -> Authentication; NO -> CreateAgent
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    
    OCMStub([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]).andReturn(nil);
    
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"Agent must not be activated"); }] activateAgent];
    [[[mockCore stub] andDo:^(NSInvocation *invocation) { XCTFail(@"AgentId must not be saved"); }] saveAgentIdUsingDocumentsPath:[OCMArg any]];
    
    //perform actions
    AuthenticationResult authResult = [core authenticateWithLogin:kDefaultLogin withPassword:kDefaultPassword];
    
    //check results
    XCTAssertEqual(authResult, ErrorInternal);
    
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore isAgentReadyToWork]);
    OCMVerify([mockRestClient createAgentForLogin:kDefaultLogin password:kDefaultPassword deviceInfo:[OCMArg any]]);
}

@end
