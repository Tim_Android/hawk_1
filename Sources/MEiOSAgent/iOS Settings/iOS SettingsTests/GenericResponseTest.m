#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>
#import "GenericResponse.h"

@interface GenericResponseTest : XCTestCase

@end

@implementation GenericResponseTest

- (void)testGenericResponseInitWithNil
{
    GenericResponse *response = [[GenericResponse alloc] initWithData:nil];
    XCTAssertNil(response, @"GenericResponse must be nil after init from nil");
}

- (void)testGenericResponseInitWithEmptyData
{
    GenericResponse *response = [[GenericResponse alloc] initWithData:[[NSData alloc] init]];
    XCTAssertNil(response, @"GenericResponse must be nil after init from empty buffer");
}

- (void)testGenericResponseCorruptedData
{
    NSString *inputData = @"{\"agentId\":null,\"st";
    GenericResponse *response = [[GenericResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"GenericResponse must be nil after init from incorrect JSON data");
}

- (void)testGenericResponseSuccess
{
    NSString *inputData = @"{\"agentId\":null,\"state\":0}";
    GenericResponse *response = [[GenericResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"GenericResponse failed to init from correct data");
    XCTAssertNotNil(response.responseDictionary, @"GenericResponse responseDictionary is nil");
    XCTAssertNotNil([response.responseDictionary objectForKey:@"agentId"], @"GenericResponse agentId is nil");
    XCTAssertNotNil([response.responseDictionary objectForKey:@"state"], @"GenericResponse state is nil");
}

@end