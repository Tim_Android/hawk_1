#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "MobileEnterpriseCore.h"


//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


@interface MobileEnterpriseCore (CategoryForTesting)

//we must redeclare private methods here to make possible adding stubs for them
//otherwise they are invisible for compiler
//don't worry, if we create a stub for not existing method, OCMock will show a runtime error - it won't be skipped

- (CLLocationManager *)locManager;
- (void)activateAgent;

@end


//stubs for CLLocationManager respondsToSelector (this method cannot be mocked with OCMock)
@interface LocationManagerOld : CLLocationManager
- (BOOL)respondsToSelector:(SEL)aSelector;
@end

@implementation LocationManagerOld
- (BOOL)respondsToSelector:(SEL)aSelector
{
    return NO;
}
@end

@interface LocationManagerIOS8 : CLLocationManager
- (BOOL)respondsToSelector:(SEL)aSelector;
@end

@implementation LocationManagerIOS8
- (BOOL)respondsToSelector:(SEL)aSelector
{
    return YES;
}
@end


//tets

@interface LocationManagerTest : XCTestCase

@property (strong, nonatomic) NSString *agentIdFilePath;

@end

@implementation LocationManagerTest

- (void)testStartLocationManager_Before_iOS8
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    
    CLLocationManager *locManagerStub = [[LocationManagerOld alloc] init]; //create correct stub, which will say that is doesn't implement requestAlwaysAuthorization method
    id mockLocationManager = OCMPartialMock(locManagerStub);
    
    OCMStub([mockCore locManager]).andReturn(locManagerStub);
    
    [[[mockLocationManager stub] andDo:^(NSInvocation *invocation) { XCTFail(@"requestAlwaysAuthorization must not be called for iOS < 8"); }] requestAlwaysAuthorization];
    
    //perform actions
    [core activateAgent];
    
    //check results
    OCMVerify([mockLocationManager startUpdatingHeading]);
    OCMVerify([mockLocationManager startUpdatingLocation]);
}

- (void)testStartLocationManager_Since_iOS8
{
    //init objects
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    
    CLLocationManager *locManagerStub = [[LocationManagerIOS8 alloc] init]; //create correct stub, which will say that it implements requestAlwaysAuthorization method
    id mockLocationManager = OCMPartialMock(locManagerStub);
    
    OCMStub([mockCore locManager]).andReturn(locManagerStub);
    
    //perform actions
    [core activateAgent];
    
    //check results
    OCMVerify([mockLocationManager requestAlwaysAuthorization]);
    OCMVerify([mockLocationManager startUpdatingHeading]);
    OCMVerify([mockLocationManager startUpdatingLocation]);
}

@end