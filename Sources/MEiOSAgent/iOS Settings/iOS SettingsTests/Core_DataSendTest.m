#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "ILoginChecker.h"
#import "MobileEnterpriseCore.h"
#import "CreateAgentResponse.h"
#import "RestClient.h"
#import "GlobalCfg.h"

#import "LocalStorage.h"

#import "ContactsCache.h"
#import "ContactData.h"
#import "DeviceInfoData.h"
#import "Utils.h"

#import "TestDataCreator.h"

//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


@interface MobileEnterpriseCore (CategoryForTesting)

//we must redeclare private methods here to make possible adding stubs for them
//otherwise they are invisible for compiler
//don't worry, if we create a stub for not existing method, OCMock will show a runtime error - it won't be skipped

- (RestClient *)restClient;
- (LocalStorage *)localStorage;
- (void)setLocalStorage:(LocalStorage *)value;
- (NSData *)obtainMmsAttachmentAtPath:(NSString *)filePath;

- (void)removeObtainedFilesAndClearInbox;
- (void)reportRestError;
- (BOOL)sendMmsAttachments;

@end


@interface Core_DataSendTest : XCTestCase
@property (strong, nonatomic) NSArray *loadedSms;
@property (strong, nonatomic) NSArray *loadedMms;
@property (strong, nonatomic) NSArray *loadedCalls;
@property (strong, nonatomic) NSArray *loadedLocations;
@property (strong, nonatomic) NSArray *loadedCrashes;
@property (strong, nonatomic) NSArray *loadedAttachments;
@end

@implementation Core_DataSendTest

- (void)setUp
{
    [super setUp];
    
    _loadedSms = [TestDataCreator createStubSms];
    _loadedMms = [TestDataCreator createStubMms];
    _loadedCalls = [TestDataCreator createStubCalls];
    _loadedLocations = [TestDataCreator createStubLocations];
    _loadedCrashes = [TestDataCreator createStubCrash];
    _loadedAttachments = [TestDataCreator createStubAttachmentsInfo];
}

#pragma mark --- Main data sending tests ---

- (void)testDataSend_HostNotReachable
{
//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastAcquisitionDate must NOT be called during data sending"); });
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must NOT be called in this test"); });

    //mock MobileEnterpriseCore and RestClient
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore removeObtainedFilesAndClearInbox]);

    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(NO);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    
//======= perform actions =======
    SendDataResult sendResult = [mockCore doDataResend];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == SendData_ErrorNoInternetConnection);
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockCore removeObtainedFilesAndClearInbox]);
}

- (void)testDataSend_ErrorDuringSend
{
//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastAcquisitionDate must NOT be called during data sending"); });
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must NOT be called in this test"); });
    
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore removeObtainedFilesAndClearInbox]);
    OCMStub([mockCore reportRestError]);
    OCMStub([mockCore sendMmsAttachments]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"attachments must NOT be sent if main data was not sent"); });

    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO);
    
    //mock LocalStorage
    NSArray *devInfoArray = [NSArray arrayWithObject:[[DeviceInfoData alloc] init]];
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadDeviceInfo]).andReturn(devInfoArray);
    OCMStub([mockLocalStorage loadMessageDataSinceDate:[OCMArg any]]).andReturn(_loadedSms);
    OCMStub([mockLocalStorage loadMmsDataSinceDate:[OCMArg any]]).andReturn(_loadedMms);
    OCMStub([mockLocalStorage loadCallDataSinceDate:[OCMArg any]]).andReturn(_loadedCalls);
    OCMStub([mockLocalStorage loadLocationDataSinceDate:[OCMArg any]]).andReturn(_loadedLocations);
    OCMStub([mockLocalStorage loadCrashDataSinceDate:[OCMArg any]]).andReturn(_loadedCrashes);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"not sent items must NOT be removed from local storage"); });
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    SendDataResult sendResult = [mockCore doDataResend];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == SendData_ErrorInternal);
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockClassCfg getLastResendDate]);
    
    OCMVerify([mockLocalStorage loadDeviceInfo]);
    OCMVerify([mockLocalStorage loadMessageDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadMmsDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadCallDataSinceDate:nil]); //we need to load all calls, without filtering them by time
    OCMVerify([mockLocalStorage loadLocationDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadCrashDataSinceDate:lastResendDate]);
    
    OCMVerify([mockRestClient updateData:[OCMArg any]]);
    OCMVerify([mockCore reportRestError]);
    
    OCMVerify([mockCore removeObtainedFilesAndClearInbox]);
}

- (void)testDataSend_Success
{
//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastAcquisitionDate must NOT be called during data sending"); });
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]);
    
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore removeObtainedFilesAndClearInbox]);
    OCMStub([mockCore reportRestError]).andDo(^(NSInvocation *invocation) { XCTFail(@"reportRestError must NOT be called in this test"); });
    OCMStub([mockCore sendMmsAttachments]).andReturn(NO); //send attachments is failed in this test - it must not affect main data sending
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(YES); //successfully sent
    
    //mock LocalStorage
    NSArray *devInfoArray = [NSArray arrayWithObject:[[DeviceInfoData alloc] init]];
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadDeviceInfo]).andReturn(devInfoArray);
    OCMStub([mockLocalStorage loadMessageDataSinceDate:[OCMArg any]]).andReturn(_loadedSms);
    OCMStub([mockLocalStorage loadMmsDataSinceDate:[OCMArg any]]).andReturn(_loadedMms);
    OCMStub([mockLocalStorage loadCallDataSinceDate:[OCMArg any]]).andReturn(_loadedCalls);
    OCMStub([mockLocalStorage loadLocationDataSinceDate:[OCMArg any]]).andReturn(_loadedLocations);
    OCMStub([mockLocalStorage loadCrashDataSinceDate:[OCMArg any]]).andReturn(_loadedCrashes);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    SendDataResult sendResult = [mockCore doDataResend];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == SendData_Success);
    OCMVerify([mockRestClient isHostReachable]);
    OCMVerify([mockClassCfg getLastResendDate]);
    
    OCMVerify([mockLocalStorage loadDeviceInfo]);
    OCMVerify([mockLocalStorage loadMessageDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadMmsDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadCallDataSinceDate:nil]); //we need to load all calls, without filtering them by time
    OCMVerify([mockLocalStorage loadLocationDataSinceDate:lastResendDate]);
    OCMVerify([mockLocalStorage loadCrashDataSinceDate:lastResendDate]);
    
    OCMVerify([mockLocalStorage eraseDataItems:_loadedSms ofType:[MessageData class]]);
    OCMVerify([mockLocalStorage eraseDataItems:_loadedMms ofType:[MmsData class]]);
    OCMVerify([mockLocalStorage eraseDataItems:_loadedCalls ofType:[CallData class]]);
    OCMVerify([mockLocalStorage eraseDataItems:_loadedLocations ofType:[LocationData class]]);
    OCMVerify([mockLocalStorage eraseDataItems:_loadedCrashes ofType:[CrashData class]]);
    
    OCMVerify([mockRestClient updateData:[OCMArg any]]);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]);
    
    OCMVerify([mockCore sendMmsAttachments]);
    
    OCMVerify([mockCore removeObtainedFilesAndClearInbox]);
}

#pragma mark --- Attachments sending tests ---

- (void)testSendAttachments_NoAttachments
{
//======= init objects =======
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore reportRestError]).andDo(^(NSInvocation *invocation) { XCTFail(@"reportRestError must NOT be called in this test"); });
    OCMStub([mockCore obtainMmsAttachmentAtPath:[OCMArg any]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"this method must not be called in this test"); });
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"uploadAttachment must NOT be called in this test"); });
    
    //mock LocalStorage
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadNotSentAttachments]).andReturn(nil);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"nothing must be erased in this test"); });
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    BOOL sendResult = [mockCore sendMmsAttachments];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == YES);
    OCMVerify([mockLocalStorage loadNotSentAttachments]);
}

- (void)testSendAttachments_NoBackgoundTime
{
//======= init objects =======
    //mock [[UIApplication sharedApplication] backgroundTimeRemaining]
    id mockApplication = OCMPartialMock([UIApplication sharedApplication]);
    OCMStub([mockApplication backgroundTimeRemaining]).andReturn(9);
    
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore reportRestError]).andDo(^(NSInvocation *invocation) { XCTFail(@"reportRestError must NOT be called in this test"); });
    OCMStub([mockCore obtainMmsAttachmentAtPath:[OCMArg any]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"this method must not be called in this test"); });
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"uploadAttachment must NOT be called in this test"); });
    
    //mock LocalStorage
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadNotSentAttachments]).andReturn(_loadedAttachments);
    __block NSArray *erasedObjects = nil;
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSArray *tmpArgument = nil;
        [invocation getArgument:&tmpArgument atIndex:2];
        if (tmpArgument)
            erasedObjects = [tmpArgument copy];
    });
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    BOOL sendResult = [mockCore sendMmsAttachments];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == NO);
    OCMVerify([mockLocalStorage loadNotSentAttachments]);
    OCMVerify([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[MmsAttachmentInfoData class]]);
    XCTAssert(erasedObjects == nil || [erasedObjects count] == 0);
}

- (void)testSendAttachments_FailedToRetrieveAttachments
{
//======= init objects =======
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore reportRestError]).andDo(^(NSInvocation *invocation) { XCTFail(@"reportRestError must NOT be called in this test"); });
    OCMStub([mockCore obtainMmsAttachmentAtPath:[OCMArg any]]).andReturn(nil);
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"uploadAttachment must NOT be called in this test"); });
    
    //mock LocalStorage
    __block NSArray *erasedObjects = nil;
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadNotSentAttachments]).andReturn(_loadedAttachments);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSArray *tmpArgument = nil;
        [invocation getArgument:&tmpArgument atIndex:2];
        if (tmpArgument)
            erasedObjects = [tmpArgument copy];
    });
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    BOOL sendResult = [mockCore sendMmsAttachments];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == YES);
    OCMVerify([mockLocalStorage loadNotSentAttachments]);
    OCMVerify([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[MmsAttachmentInfoData class]]); //checking arrays in OCMArg seems to have some bugs - let's check in another way
    XCTAssert([_loadedAttachments isEqualToArray:erasedObjects]);
}

- (void)testSendAttachments_PartialFail
{
//======= init objects =======
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore reportRestError]);
    OCMStub([mockCore obtainMmsAttachmentAtPath:[OCMArg any]]).andReturn([[NSData alloc] init]);
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"1"]).andReturn(YES);
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:[OCMArg any]]).andReturn(NO);
    
    //mock LocalStorage
    __block NSArray *erasedObjects = nil;
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadNotSentAttachments]).andReturn(_loadedAttachments);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSArray *tmpArgument = nil;
        [invocation getArgument:&tmpArgument atIndex:2];
        if (tmpArgument)
            erasedObjects = [tmpArgument copy];
    });
    
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    BOOL sendResult = [mockCore sendMmsAttachments];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == NO);
    OCMVerify([mockLocalStorage loadNotSentAttachments]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"1"]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"2"]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"3"]);
    
    OCMVerify([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[MmsAttachmentInfoData class]]); //checking arrays in OCMArg seems to have some bugs - let's check in another way
    XCTAssert([erasedObjects count] == 1);
    XCTAssert([erasedObjects containsObject:[_loadedAttachments objectAtIndex:0]]);
    
    OCMVerify([mockCore reportRestError]);
}

- (void)testSendAttachments_Success
{
//======= init objects =======
    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    OCMStub([mockCore reportRestError]);
    OCMStub([mockCore obtainMmsAttachmentAtPath:[OCMArg any]]).andReturn([[NSData alloc] init]);
    OCMStub([mockCore reportRestError]).andDo(^(NSInvocation *invocation) { XCTFail(@"reportRestError must NOT be called in this test"); });
    
    //mock RetClient
    id mockRestClient = OCMPartialMock(core.restClient);
    OCMStub([mockRestClient isHostReachable]).andReturn(YES);
    OCMStub([mockRestClient updateData:[OCMArg any]]).andReturn(NO).andDo(^(NSInvocation *invocation) { XCTFail(@"updateData must NOT be called in this test"); });
    OCMStub([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:[OCMArg any]]).andReturn(YES);
    
    //mock LocalStorage
    __block NSArray *erasedObjects = nil;
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage loadNotSentAttachments]).andReturn(_loadedAttachments);
    OCMStub([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[OCMArg any]]).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSArray *tmpArgument = nil;
        [invocation getArgument:&tmpArgument atIndex:2];
        if (tmpArgument)
            erasedObjects = [tmpArgument copy];
    });
    
    OCMStub([mockLocalStorage clear]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"LocalStorate must NOT be cleared during data sending"); });
    
//======= perform actions =======
    BOOL sendResult = [mockCore sendMmsAttachments];
    
//======= check results (order of invocation is not checked)
    XCTAssert(sendResult == YES);
    OCMVerify([mockLocalStorage loadNotSentAttachments]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"1"]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"2"]);
    OCMVerify([mockRestClient uploadMMSAttachment:[OCMArg any] attachmentId:@"3"]);
    
    OCMVerify([mockLocalStorage eraseDataItems:[OCMArg any] ofType:[MmsAttachmentInfoData class]]); //checking arrays in OCMArg seems to have some bugs - let's check in another way
    XCTAssert([_loadedAttachments isEqualToArray:erasedObjects]);
}

@end
