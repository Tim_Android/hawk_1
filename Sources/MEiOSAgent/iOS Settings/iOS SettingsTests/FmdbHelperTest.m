#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "FMDatabase.h"
#import "FmdbHelper.h"
#import "Utils.h"


#define SQL_CREATE_TABLE_ERASINGITEMS @"CREATE TABLE TestErasingItems (type INTEGER, value TEXT);"
#define SQL_SELECT_ALL_FROM_ERASINGITEMS @"SELECT rowid, type, value FROM TestErasingItems;"


@interface FmdbHelperTest : XCTestCase
@property (strong, nonatomic) NSString *testDbPath;
@end

@implementation FmdbHelperTest

- (void)setUp
{
    [super setUp];
    
    NSString *docsDir = [Utils getPathForDocumentsDirectory];
    _testDbPath = [docsDir stringByAppendingPathComponent:@"test_fmdbhelper.db"];
}

- (void)tearDown
{    
    [[NSFileManager defaultManager] removeItemAtPath:_testDbPath error:NULL];
    
    [super tearDown];
}

- (void)testDeleteItemsByIds
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:_testDbPath])
    {
        NSError *error = nil;
        BOOL isRemoved = [fm removeItemAtPath:_testDbPath error:&error];
        XCTAssert(isRemoved && !error, @"We must be sure that test DB doesn't exist before starting this test");
    }
    
    NSError *error = nil;
    FMDatabase *db = [FMDatabase databaseWithPath:_testDbPath];
    BOOL isOk = [FmdbHelper executeUpdate:SQL_CREATE_TABLE_ERASINGITEMS db:db error:&error];
    XCTAssert(isOk, @"Failed to prepare test environment");
    
    isOk = [FmdbHelper executeStatements:
            @"INSERT INTO TestErasingItems VALUES (5, 'A'); "
            "INSERT INTO TestErasingItems VALUES (6, 'B'); "
            "INSERT INTO TestErasingItems VALUES (7, 'C'); "
            "INSERT INTO TestErasingItems VALUES (8, 'D'); "
            db:db error:&error];
    XCTAssert(isOk, @"Failed to prepare test environment");

    {
        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], @"rowid", [NSNumber numberWithInt:5], @"type", @"A", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:3], @"rowid", [NSNumber numberWithInt:7], @"type", @"C", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:4], @"rowid", [NSNumber numberWithInt:8], @"type", @"D", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
    
//Check: nothing is removed for nil array
    {
        error = nil;
        isOk = [FmdbHelper deleteItemsWithIds:nil fromTable:@"TestErasingItems" fromDb:db error:&error];
        XCTAssert(isOk && !error);
    
        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], @"rowid", [NSNumber numberWithInt:5], @"type", @"A", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:3], @"rowid", [NSNumber numberWithInt:7], @"type", @"C", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:4], @"rowid", [NSNumber numberWithInt:8], @"type", @"D", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
    
//Check: nothing is removed for empty array
    {
        error = nil;
        isOk = [FmdbHelper deleteItemsWithIds:[[NSArray alloc] init] fromTable:@"TestErasingItems" fromDb:db error:&error];
        XCTAssert(isOk && !error);

        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], @"rowid", [NSNumber numberWithInt:5], @"type", @"A", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:3], @"rowid", [NSNumber numberWithInt:7], @"type", @"C", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:4], @"rowid", [NSNumber numberWithInt:8], @"type", @"D", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
    
//Check: remove one element
    {
        error = nil;
        isOk = [FmdbHelper deleteItemsWithIds:[NSArray arrayWithObject:[NSNumber numberWithInt:1]] fromTable:@"TestErasingItems" fromDb:db error:&error];
        XCTAssert(isOk && !error);
    
        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:3], @"rowid", [NSNumber numberWithInt:7], @"type", @"C", @"value", nil]];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:4], @"rowid", [NSNumber numberWithInt:8], @"type", @"D", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
    
//Check: remove two elements
    {
        error = nil;
        NSArray *idsToRemove = [NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil];
        isOk = [FmdbHelper deleteItemsWithIds:idsToRemove fromTable:@"TestErasingItems" fromDb:db error:&error];
        XCTAssert(isOk && !error);
    
        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
    
//Check: removing not existing items must not be considered as error
    {
        error = nil;
        NSArray *idsToRemove = [NSArray arrayWithObjects:[NSNumber numberWithInt:14], [NSNumber numberWithInt:15], nil]; //not existing items
        isOk = [FmdbHelper deleteItemsWithIds:idsToRemove fromTable:@"TestErasingItems" fromDb:db error:&error];
        XCTAssert(isOk && !error);
        
        NSMutableArray *selectedRecords = [[NSMutableArray alloc] init];
        isOk = [FmdbHelper executeQuery:SQL_SELECT_ALL_FROM_ERASINGITEMS db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
            [selectedRecords addObject:rslt];
        }];
        XCTAssert(isOk && !error, @"Ensure the test table has correct format.");
        
        NSMutableArray *expectedRecords = [[NSMutableArray alloc] init];
        [expectedRecords addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"rowid", [NSNumber numberWithInt:6], @"type", @"B", @"value", nil]];
        XCTAssert([selectedRecords isEqualToArray:expectedRecords]);
    }
}

- (void)test_versionOfDb_defaultVersion
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    XCTAssertEqual(0, [FmdbHelper versionOfDb: db]);
}

- (void)test_setVersionForDb
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    
    [FmdbHelper setVersion: 1 forDb: db];
    XCTAssertEqual(1, [FmdbHelper versionOfDb: db]);
    
    [FmdbHelper setVersion: 2 forDb: db];
    XCTAssertEqual(2, [FmdbHelper versionOfDb: db]);
    
    [FmdbHelper setVersion: 0 forDb: db];
    XCTAssertEqual(0, [FmdbHelper versionOfDb: db]);
}

- (void)test_doesTableExist_emptyDataBase
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    
    XCTAssertFalse([FmdbHelper doesTableExist:@"test_table" inDb:db]);
}

- (void)test_doesTableExist
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
  
    [FmdbHelper executeUpdate:@"CREATE TABLE test_table (type INTEGER)" db:db error:NULL];
    XCTAssert([FmdbHelper doesTableExist:@"test_table" inDb:db]);
    
    XCTAssertFalse([FmdbHelper doesTableExist:@"test_table1" inDb:db]);
}

- (void)test_doesTableExist_afterDrop
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    
    [FmdbHelper executeUpdate:@"CREATE TABLE test_table (type INTEGER)" db:db error:NULL];
    [FmdbHelper executeUpdate:@"DROP TABLE test_table" db:db error:NULL];
    
    XCTAssertFalse([FmdbHelper doesTableExist:@"test_table" inDb:db]);
}

@end
