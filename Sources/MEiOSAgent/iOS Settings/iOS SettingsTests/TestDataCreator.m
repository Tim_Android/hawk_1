#import "TestDataCreator.h"
#import "Utils.h"

@implementation TestDataCreator
+ (NSArray *)createStubSms
{
    NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"+1555", @"address",
                           @"1423526401", @"date",
                           [Utils encodeToBase64:@"Hello"], @"body",
                           @"Outbox", @"type",
                           [NSNumber numberWithInt:1], @"rowid",
                           nil];
    return [NSArray arrayWithObject:[[MessageData alloc] initWithLocalDbEntry:entry]]; //emulate loading from local storage
}
+ (NSArray *)createStubMms
{
    NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"+1555", @"address",
                           @"1423526402", @"date",
                           [Utils encodeToBase64:@"Hello"], @"body",
                           @"Outbox", @"type",
                           @"Subj", @"sub",
                           [NSNumber numberWithInt:2], @"rowid",
                           @"", @"attachments",
                           nil];
    return [NSArray arrayWithObject:[[MmsData alloc] initWithLocalDbEntry:entry]]; //emulate loading from local storage
}
+ (NSArray *)createStubCalls
{
    NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Missed", @"type",
                           @"+1555", @"fromNumber",
                           @"1423526403", @"date",
                           @"0", @"duration",
                           @"Mobile", @"fromNumberType",
                           @"Nicole", @"fromNumberName",
                           [NSNumber numberWithInt:3], @"rowid",
                           nil];
    return [NSArray arrayWithObject:[[CallData alloc] initWithLocalDbEntry:entry]]; //emulate loading from local storage
    
}
+ (NSArray *)createStubLocations
{
    NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"10", @"latitude",
                           @"20", @"longitude",
                           @"30", @"altitude",
                           @"40", @"accuracy",
                           @"50", @"bearing",
                           @"60", @"speed",
                           @"1423526404", @"time",
                           @"GPS", @"provider",
                           [NSNumber numberWithInt:4], @"rowid",
                           nil];
    return [NSArray arrayWithObject:[[LocationData alloc] initWithLocalDbEntry:entry]]; //emulate loading from local storage
    
}
+ (NSArray *)createStubCrash
{
    NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"CrashText", @"text",
                           [NSNumber numberWithLong:1423526405], @"date",
                           [NSNumber numberWithInt:5], @"rowid",
                           nil];
    return [NSArray arrayWithObject:[[CrashData alloc] initWithLocalDbEntry:entry]]; //emulate loading from local storage
    
}
+ (NSArray *)createStubAttachmentsInfo
{
    //emulate loading from local storage
    NSMutableArray *attachmentsInfo = [[NSMutableArray alloc] init];
    MmsAttachmentInfoData *tmpAttach = nil;
    
    tmpAttach = [MmsAttachmentInfoData attachmentFromString:@"path1:jpg:1:101"];
    tmpAttach.localStorageId = [NSNumber numberWithInt:901];
    [attachmentsInfo addObject:tmpAttach];
    
    tmpAttach = [MmsAttachmentInfoData attachmentFromString:@"path2:jpg:2:102"];
    tmpAttach.localStorageId = [NSNumber numberWithInt:902];
    [attachmentsInfo addObject:tmpAttach];

    tmpAttach = [MmsAttachmentInfoData attachmentFromString:@"path3:jpg:3:103"];
    tmpAttach.localStorageId = [NSNumber numberWithInt:903];
    [attachmentsInfo addObject:tmpAttach];
    return attachmentsInfo;
}
@end