#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "TestDataCreator.h"
#import "Utils.h"
#import "FmdbHelper.h"

//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


@interface NSInvocation (CategoryForTesting)
- (void)copyAgrument:(id*)destination atIndex:(NSInteger)idx;
@end

@implementation NSInvocation (CategoryForTesting)
- (void)copyAgrument:(id*)destination atIndex:(NSInteger)idx
{
    __unsafe_unretained id tmpArgument = nil;
    [self getArgument:&tmpArgument atIndex:idx];
    *destination = [tmpArgument copy];
}
@end


@interface DeviceData_EraseByIdTest : XCTestCase
@end

@implementation DeviceData_EraseByIdTest

- (void)testEraseById_Sms
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *smsToErase = [TestDataCreator createStubSms];
    BOOL result = [MessageData eraseFromDb:@"Local_storage_unused.db" items:smsToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObject:[NSNumber numberWithInt:1]];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"message" isEqualToString:erasedFromTable]);
}

- (void)testEraseById_Mms
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *mmsToErase = [TestDataCreator createStubMms];
    BOOL result = [MmsData eraseFromDb:@"Local_storage_unused.db" items:mmsToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObject:[NSNumber numberWithInt:2]];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"mms" isEqualToString:erasedFromTable]);
}

- (void)testEraseById_Calls
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *callsToErase = [TestDataCreator createStubCalls];
    BOOL result = [CallData eraseFromDb:@"Local_storage_unused.db" items:callsToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObject:[NSNumber numberWithInt:3]];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"call" isEqualToString:erasedFromTable]);
}

- (void)testEraseById_Locations
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *locationsToErase = [TestDataCreator createStubLocations];
    BOOL result = [LocationData eraseFromDb:@"Local_storage_unused.db" items:locationsToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObject:[NSNumber numberWithInt:4]];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"location" isEqualToString:erasedFromTable]);
}

- (void)testEraseById_Crashes
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *crashesToErase = [TestDataCreator createStubCrash];
    BOOL result = [CrashData eraseFromDb:@"Local_storage_unused.db" items:crashesToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObject:[NSNumber numberWithInt:5]];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"crash" isEqualToString:erasedFromTable]);
}

- (void)testEraseById_Attachments
{
//======= init objects =======
    id mockDbHelperClass = OCMStrictClassMock([FmdbHelper class]);
    __block NSArray *erasedObjects = nil;
    __block NSString *erasedFromTable = nil;
    OCMStub([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation)
    {
        [invocation copyAgrument:&erasedObjects atIndex:2];
        [invocation copyAgrument:&erasedFromTable atIndex:3];
    });
    
//======= perform actions =======
    NSError *error = nil;
    NSArray *attachmentsToErase = [TestDataCreator createStubAttachmentsInfo];
    BOOL result = [MmsAttachmentInfoData eraseFromDb:@"Local_storage_unused.db" items:attachmentsToErase error:&error];
    
//======= check results =======
    XCTAssert(result == YES);
    OCMVerify([mockDbHelperClass deleteItemsWithIds:[OCMArg any] fromTable:[OCMArg any] fromDb:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]); //checking arguments in OCMVerify sometimes has some bugs - let's check in another way
    NSArray *expectedIdsToErase = [NSArray arrayWithObjects:[NSNumber numberWithInt:901], [NSNumber numberWithInt:902], [NSNumber numberWithInt:903], nil];
    XCTAssert([expectedIdsToErase isEqualToArray:erasedObjects]);
    XCTAssert([@"attachments" isEqualToString:erasedFromTable]);
}

@end