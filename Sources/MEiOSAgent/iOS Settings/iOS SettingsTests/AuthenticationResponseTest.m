#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>
#import "AuthenticationResponse.h"

@interface AuthenticationResponseTest : XCTestCase

@end

@implementation AuthenticationResponseTest

- (void)testAuthenticationResponseInitWithNil
{
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:nil];
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init from nil");
}

- (void)testAuthenticationResponseInitWithEmptyData
{
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[[NSData alloc] init]];
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init from empty buffer");
}

- (void)testAuthenticationResponseCorruptedData
{
    NSString *inputData = @"{\"agentId\":null,\"st";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init from incorrect JSON data");
}

- (void)testAuthenticationResponseWithoutRequiredElement
{
    NSString *inputData = @"{\"agentId\":null}"; //AuthenticationResponse requires "state" JSON element, but it is missing here
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init with missing of required JSON element");
}

- (void)testAuthenticationResponseNullValue
{
    NSString *inputData = @"{\"state\":null}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init with NULL JSON element");
}

- (void)testAuthenticationResponseIncorrectType1
{
    NSString *inputData = @"{\"state\":[]}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init with ARRAY JSON element");
}

- (void)testAuthenticationResponseIncorrectType2
{
    NSString *inputData = @"{\"state\":\"STRING_VALUE\"}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init with STRING JSON element");
}

- (void)testAuthenticationResponseWrongCredentials
{
    NSString *inputData = @"{\"state\":0}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"AuthenticationResponse is not initialized with correct JSON data");
    XCTAssert(response.isServerError == NO);
    XCTAssert(response.isAuthenticated == NO);
}

- (void)testAuthenticationResponseCorrectCredentials
{
    NSString *inputData = @"{\"state\":1}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"AuthenticationResponse is not initialized with correct JSON data");
    XCTAssert(response.isServerError == NO);
    XCTAssert(response.isAuthenticated == YES);
}

- (void)testAuthenticationResponseServerError
{
    NSString *inputData = @"{\"state\":2}";
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNotNil(response, @"AuthenticationResponse is not initialized with correct JSON data");
    XCTAssert(response.isServerError == YES);
    XCTAssert(response.isAuthenticated == NO);
}

- (void)testAuthenticationResponseUnknownCode
{
    NSString *inputData = @"{\"state\":999}"; //invalid state code
    AuthenticationResponse *response = [[AuthenticationResponse alloc] initWithData:[inputData dataUsingEncoding:NSUTF8StringEncoding]];
    
    XCTAssertNil(response, @"AuthenticationResponse must be nil after init with INCORRECT state code");
}

@end
