
#import <XCTest/XCTest.h>

#import "MmsData.h"

#import "Utils.h"
#import "FMDB.h"
#import "FmdbHelper.h"

@interface MmsDataTests : XCTestCase
{
    NSString* _testDbPath;
}

@end

@implementation MmsDataTests

- (void)setUp
{
    [super setUp];
    
    NSString *docsDir = [Utils getPathForDocumentsDirectory];
    _testDbPath = [docsDir stringByAppendingPathComponent:@"test_fmdbhelper.db"];
}

- (void)tearDown
{
    [[NSFileManager defaultManager] removeItemAtPath:_testDbPath error:NULL];
    
    [super tearDown];
}

- (void)test_createDb
{
    NSError* error = nil;
    BOOL ok = [MmsData createTableInDb:_testDbPath dropIfExists:NO error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    XCTAssert([FmdbHelper doesTableExist:@"mms" inDb:db]);
}

- (void)test_updateDbFrom_0_to_1_version
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    [FmdbHelper executeUpdate: @"create table if not exists message (address text, date integer, body text, type text)" db:db error:NULL]; //create table in old format
    db = nil;
    
    NSError* error = nil;
    BOOL ok = [MmsData updateTableInDb:_testDbPath fromVersion0To1Error:&error];
    XCTAssert(ok);
    XCTAssertNil(error);
    
    db = [FMDatabase databaseWithPath:_testDbPath];
    XCTAssert([FmdbHelper isColumnExist:@"contactName" inTable:@"mms" inDb:db error:NULL]);
}

- (void)test_saveLoadMessagesToDb_emptyArray
{
    [MmsData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MmsData saveIntoDb:_testDbPath data:@[] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);

    NSArray* mmses = [MmsData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssertNil(mmses);
}

- (void)test_saveLoad_MessagesToDb
{
    MmsData* mms1 = [MmsData new];
    mms1.phoneNumber = @"123";
    mms1.contactName = @"Test name #1";
    mms1.timestamp = [NSDate dateWithTimeIntervalSince1970: 1];
    mms1.body = @"Test body #1";
    mms1.messageType = @"Test type #1";
    mms1.messageSubject = @"Test subject #1";
    mms1.attachments = @[];
    
    MmsData* mms2 = [MmsData new];
    mms2.phoneNumber = @"123";
    mms2.contactName = @"Test name #2";
    mms2.timestamp = [NSDate dateWithTimeIntervalSince1970: 2];
    mms2.body = @"Test body #2";
    mms2.messageType = @"Test type #2";
    mms2.messageSubject = @"Test subject #2";
    mms2.attachments = @[];
    
    MmsData* mms3 = [MmsData new];
    mms3.phoneNumber = @"123";
    mms3.contactName = @"Test name #3";
    mms3.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    mms3.body = @"Test body #3";
    mms3.messageType = @"Test type #3";
    mms3.messageSubject = @"Test subject #3";
    mms3.attachments = @[];
    
    [MmsData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MmsData saveIntoDb:_testDbPath data:@[mms1, mms2, mms3] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    NSArray<MmsData*>* mmses = [MmsData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssert(mmses);
    XCTAssertEqual(3, mmses.count);
    
    XCTAssertEqualObjects(mmses[0].phoneNumber, @"123");
    XCTAssertEqualObjects(mmses[0].contactName, @"Test name #1");
    XCTAssertEqualObjects(mmses[0].timestamp, [NSDate dateWithTimeIntervalSince1970: 1]);
    XCTAssertEqualObjects(mmses[0].body, @"Test body #1");
    XCTAssertEqualObjects(mmses[0].messageType, @"Test type #1");
    XCTAssertEqualObjects(mmses[0].messageSubject, @"Test subject #1");
    XCTAssertNil(mmses[0].attachments);
    
    XCTAssertEqualObjects(mmses[1].phoneNumber, @"123");
    XCTAssertEqualObjects(mmses[1].contactName, @"Test name #2");
    XCTAssertEqualObjects(mmses[1].timestamp, [NSDate dateWithTimeIntervalSince1970: 2]);
    XCTAssertEqualObjects(mmses[1].body, @"Test body #2");
    XCTAssertEqualObjects(mmses[1].messageType, @"Test type #2");
    XCTAssertEqualObjects(mmses[1].messageSubject, @"Test subject #2");
    XCTAssertNil(mmses[1].attachments);
    
    XCTAssertEqualObjects(mmses[2].phoneNumber, @"123");
    XCTAssertEqualObjects(mmses[2].contactName, @"Test name #3");
    XCTAssertEqualObjects(mmses[2].timestamp, [NSDate dateWithTimeIntervalSince1970: 3]);
    XCTAssertEqualObjects(mmses[2].body, @"Test body #3");
    XCTAssertEqualObjects(mmses[2].messageType, @"Test type #3");
    XCTAssertEqualObjects(mmses[2].messageSubject, @"Test subject #3");
    XCTAssertNil(mmses[2].attachments);
}

- (void)test_saveLoad_MessagesToDb_NilContactName
{
    MmsData* mms1 = [MmsData new];
    mms1.phoneNumber = @"123";
    mms1.contactName = nil;
    mms1.timestamp = [NSDate dateWithTimeIntervalSince1970: 1];
    mms1.body = @"Test body #1";
    mms1.messageType = @"Test type #1";
    mms1.messageSubject = @"Test subject #1";
    mms1.attachments = @[];
    
    [MmsData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MmsData saveIntoDb:_testDbPath data:@[mms1] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    NSArray<MmsData*>* mmses = [MmsData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssert(mmses);
    XCTAssertEqual(1, mmses.count);
    
    XCTAssertEqualObjects(mmses[0].phoneNumber, @"123");
    XCTAssertNil(mmses[0].contactName);
    XCTAssertEqualObjects(mmses[0].timestamp, [NSDate dateWithTimeIntervalSince1970: 1]);
    XCTAssertEqualObjects(mmses[0].body, @"Test body #1");
    XCTAssertEqualObjects(mmses[0].messageType, @"Test type #1");
    XCTAssertEqualObjects(mmses[0].messageSubject, @"Test subject #1");
    XCTAssertNil(mmses[0].attachments);
}

@end
