#import <Foundation/Foundation.h>

#import "MessageData.h"
#import "MmsData.h"
#import "CallData.h"
#import "LocationData.h"
#import "CrashData.h"
#import "MmsAttachmentInfoData.h"


@interface MessageData (CategoryForTesting)
- (id)initWithLocalDbEntry:(NSDictionary *)entry;
@end
@interface MmsData (CategoryForTesting)
- (id)initWithLocalDbEntry:(NSDictionary *)entry;
@end
@interface CallData (CategoryForTesting)
- (id)initWithLocalDbEntry:(NSDictionary *)entry;
@end
@interface LocationData (CategoryForTesting)
- (id)initWithLocalDbEntry:(NSDictionary *)entry;
@end
@interface CrashData (CategoryForTesting)
- (id)initWithLocalDbEntry:(NSDictionary *)entry;
@end
@interface MmsAttachmentInfoData (CategoryForTesting)
@property (strong, nonatomic) NSNumber *localStorageId;
@end


@interface TestDataCreator : NSObject
+ (NSArray *)createStubSms;
+ (NSArray *)createStubMms;
+ (NSArray *)createStubCalls;
+ (NSArray *)createStubLocations;
+ (NSArray *)createStubCrash;
+ (NSArray *)createStubAttachmentsInfo;
@end


