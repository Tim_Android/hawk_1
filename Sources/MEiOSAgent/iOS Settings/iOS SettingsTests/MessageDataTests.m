
#import <XCTest/XCTest.h>

#import "MessageData.h"

#import "Utils.h"
#import "FMDB.h"
#import "FmdbHelper.h"

@interface MessageDataTests : XCTestCase
{
    NSString* _testDbPath;
}

@end

@implementation MessageDataTests

- (void)setUp
{
    [super setUp];
    
    NSString *docsDir = [Utils getPathForDocumentsDirectory];
    _testDbPath = [docsDir stringByAppendingPathComponent:@"test_fmdbhelper.db"];
}

- (void)tearDown
{
    [[NSFileManager defaultManager] removeItemAtPath:_testDbPath error:NULL];
    
    [super tearDown];
}

- (void)test_createDb
{
    NSError* error = nil;
    BOOL ok = [MessageData createTableInDb:_testDbPath dropIfExists:NO error:&error];

    XCTAssert(ok);
    XCTAssertNil(error);
    
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    XCTAssert([FmdbHelper doesTableExist:@"message" inDb:db]);
}

- (void)test_updateDbFrom_0_to_1_version
{
    FMDatabase* db = [FMDatabase databaseWithPath:_testDbPath];
    [FmdbHelper executeUpdate: @"create table if not exists message (address text, date integer, body text, type text)" db:db error:NULL]; //create table in old format
    db = nil;
    
    NSError* error = nil;
    BOOL ok = [MessageData updateTableInDb:_testDbPath fromVersion0To1Error:&error];
    XCTAssert(ok);
    XCTAssertNil(error);
    
    db = [FMDatabase databaseWithPath:_testDbPath];
    XCTAssert([FmdbHelper isColumnExist:@"contactName" inTable:@"message" inDb:db error:NULL]);
}

- (void)test_saveLoadMessagesToDb_emptyArray
{
    [MessageData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MessageData saveIntoDb:_testDbPath data:@[] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    NSArray* mmses = [MessageData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssertNil(mmses);
}

- (void)test_saveLoad_MessagesToDb
{
    MessageData* sms1 = [MessageData new];
    sms1.phoneNumber = @"123";
    sms1.contactName = @"Test name #1";
    sms1.timestamp = [NSDate dateWithTimeIntervalSince1970: 1];
    sms1.body = @"Test body #1";
    sms1.messageType = @"Test type #1";
    
    MessageData* sms2 = [MessageData new];
    sms2.phoneNumber = @"123";
    sms2.contactName = @"Test name #2";
    sms2.timestamp = [NSDate dateWithTimeIntervalSince1970: 2];
    sms2.body = @"Test body #2";
    sms2.messageType = @"Test type #2";
    
    MessageData* sms3 = [MessageData new];
    sms3.phoneNumber = @"123";
    sms3.contactName = @"Test name #3";
    sms3.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    sms3.body = @"Test body #3";
    sms3.messageType = @"Test type #3";
    
    [MessageData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MessageData saveIntoDb:_testDbPath data:@[sms1, sms2, sms3] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    NSArray<MessageData*>* smses = [MessageData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssert(smses);
    XCTAssertEqual(3, smses.count);
    
    XCTAssertEqualObjects(smses[0].phoneNumber, @"123");
    XCTAssertEqualObjects(smses[0].contactName, @"Test name #1");
    XCTAssertEqualObjects(smses[0].timestamp, [NSDate dateWithTimeIntervalSince1970: 1]);
    XCTAssertEqualObjects(smses[0].body, @"Test body #1");
    XCTAssertEqualObjects(smses[0].messageType, @"Test type #1");
    
    XCTAssertEqualObjects(smses[1].phoneNumber, @"123");
    XCTAssertEqualObjects(smses[1].contactName, @"Test name #2");
    XCTAssertEqualObjects(smses[1].timestamp, [NSDate dateWithTimeIntervalSince1970: 2]);
    XCTAssertEqualObjects(smses[1].body, @"Test body #2");
    XCTAssertEqualObjects(smses[1].messageType, @"Test type #2");
    
    XCTAssertEqualObjects(smses[2].phoneNumber, @"123");
    XCTAssertEqualObjects(smses[2].contactName, @"Test name #3");
    XCTAssertEqualObjects(smses[2].timestamp, [NSDate dateWithTimeIntervalSince1970: 3]);
    XCTAssertEqualObjects(smses[2].body, @"Test body #3");
    XCTAssertEqualObjects(smses[2].messageType, @"Test type #3");
}

- (void)test_saveLoad_MessagesToDb_NilContactName
{
    MessageData* sms = [MessageData new];
    sms.phoneNumber = @"123";
    sms.contactName = nil;
    sms.timestamp = [NSDate dateWithTimeIntervalSince1970: 1];
    sms.body = @"Test body #1";
    sms.messageType = @"Test type #1";
    
    [MessageData createTableInDb:_testDbPath dropIfExists:NO error:NULL];
    
    NSError* error = nil;
    BOOL ok = [MessageData saveIntoDb:_testDbPath data:@[sms] error:&error];
    
    XCTAssert(ok);
    XCTAssertNil(error);
    
    NSArray<MessageData*>* smses = [MessageData loadFromDb:_testDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] error:&error];
    
    XCTAssertNil(error);
    XCTAssert(smses);
    XCTAssertEqual(1, smses.count);
    
    XCTAssertEqualObjects(smses[0].phoneNumber, @"123");
    XCTAssertNil(smses[0].contactName);
    XCTAssertEqualObjects(smses[0].timestamp, [NSDate dateWithTimeIntervalSince1970: 1]);
    XCTAssertEqualObjects(smses[0].body, @"Test body #1");
    XCTAssertEqualObjects(smses[0].messageType, @"Test type #1");
}

@end
