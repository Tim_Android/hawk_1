#import <XCTest/XCTest.h>

#import "Utils.h"
#import "Constants.h"

#import "CallData.h"

#import "ContactData.h"
#import "ContactsCache.h"

#import "FMDB.h"
#import "FmdbHelper.h"

@interface CallDataTest : XCTestCase

@property (strong, nonatomic) NSString *callsOldDbPath;
@property (strong, nonatomic) NSString *calls8xDbPath;
@property (strong, nonatomic) NSString *callsOldNoCallerIdDbPath;
@property (strong, nonatomic) NSString *calls8xNoCallerIdDbPath;
@property (strong, nonatomic) NSString *callsOldSelectByIdDbPath;
@property (strong, nonatomic) NSString *calls8xSelectByIdDbPath;
@property (strong, nonatomic) NSString *contactsDbPath;

@end


@implementation CallDataTest

- (void)setUp
{
    [super setUp];
    
    NSBundle *currentBundle = [NSBundle bundleForClass:[self class]];
    _callsOldDbPath = [currentBundle pathForResource:@"call_history" ofType:@"db"];
    _calls8xDbPath = [currentBundle pathForResource:@"CallHistory" ofType:@"storedata"];
    _callsOldNoCallerIdDbPath = [currentBundle pathForResource:@"call_history_NoCallerID" ofType:@"db"];
    _calls8xNoCallerIdDbPath = [currentBundle pathForResource:@"call_history8x_NoCallerID" ofType:@"storedata"];
    _callsOldSelectByIdDbPath = [currentBundle pathForResource:@"call_history_selectById" ofType:@"db"];
    _calls8xSelectByIdDbPath = [currentBundle pathForResource:@"call_history8x_selectById" ofType:@"db"];
    _contactsDbPath = [currentBundle pathForResource:@"AddressBook" ofType:@"db"];
}

- (void)testCallData
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.callsOldDbPath version:CallsDbVersion_old sinceDate:nil withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 100, @"Wrong count of DB entries. Did you change DB?");
    
    
    CallData *call = [calls objectAtIndex:42];
    XCTAssert([call.person isEqualToString:@"Kipper, Amber"], @"Call #42 now has another Person");
    XCTAssert([call.callType isEqualToString:@"Incoming"], @"Call #42 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"8013196347"], @"Call #42 now has another Phone Number");
    
    call = [calls objectAtIndex:43];
    XCTAssert([call.person isEqualToString:@"Schroader, Russ"], @"Call #43 now has another Person");
    XCTAssert([call.callType isEqualToString:@"Missed"], @"Call #43 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"5129684360"], @"Call #43 now has another Phone Number");
    
    call = [calls objectAtIndex:44];
    XCTAssert([call.person isEqualToString:@"Schroader, Russ"], @"Call #44 now has another Person");
    XCTAssert([call.callType isEqualToString:@"Outgoing"], @"Call #44 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"5129684360"], @"Call #44 now has another Phone Number");
    XCTAssertEqual([call.duration doubleValue], 707, @"Call #44 now has another Duration");
    
    NSArray *jsonObj = [CallData jsonObjectWithData:calls];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Calls");
    
    
    //check the same #42, #43, #44 objects after convertion into dictionaries
    NSDictionary *callDictionary = [jsonObj objectAtIndex:42];
    XCTAssert([[callDictionary objectForKey:@"fromNumber"]     isEqualToString:@"8013196347"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberName"] isEqualToString:@"Kipper, Amber"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberType"] isEqualToString:@"Mobile"]);
    XCTAssert([[callDictionary objectForKey:@"toNumber"]       isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberName"]   isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberType"]   isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"type"]           isEqualToString:@"Incoming"]);
    XCTAssert([[callDictionary objectForKey:@"date"]           isEqualToString:@"/Date(1331078342000)/"]);
    XCTAssert([[callDictionary objectForKey:@"duration"] doubleValue] == 240.0);
    
    callDictionary = [jsonObj objectAtIndex:43];
    XCTAssert([[callDictionary objectForKey:@"fromNumber"]     isEqualToString:@"5129684360"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberName"] isEqualToString:@"Schroader, Russ"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberType"] isEqualToString:@"Mobile"]);
    XCTAssert([[callDictionary objectForKey:@"toNumber"]       isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberName"]   isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberType"]   isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"type"]           isEqualToString:@"Missed"]);
    XCTAssert([[callDictionary objectForKey:@"date"]           isEqualToString:@"/Date(1331154032000)/"]);
    XCTAssert([[callDictionary objectForKey:@"duration"] doubleValue] == 0.0);
    
    callDictionary = [jsonObj objectAtIndex:44];
    XCTAssert([[callDictionary objectForKey:@"fromNumber"]     isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberName"] isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"fromNumberType"] isEqualToString:@"N/A"]);
    XCTAssert([[callDictionary objectForKey:@"toNumber"]       isEqualToString:@"5129684360"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberName"]   isEqualToString:@"Schroader, Russ"]);
    XCTAssert([[callDictionary objectForKey:@"toNumberType"]   isEqualToString:@"Mobile"]);
    XCTAssert([[callDictionary objectForKey:@"type"]           isEqualToString:@"Outgoing"]);
    XCTAssert([[callDictionary objectForKey:@"date"]           isEqualToString:@"/Date(1331165743000)/"]);
    XCTAssert([[callDictionary objectForKey:@"duration"] doubleValue] == 707.0);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

- (void)testCallData_iOS8
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSDate *sinceDate = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:443400000.0]; //about 20 Jan 2015 - taken from DB to select only 2 calls
    
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.calls8xDbPath version:CallsDbVersion_iOS8 sinceDate:sinceDate withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 2, @"Wrong count of DB entries. Did you change DB?");
    
    CallData *call = [calls objectAtIndex:0];
    XCTAssert([call.callType isEqualToString:@"Outgoing"], @"Call #0 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"tester.ds123@gmail.com"], @"Call #0 now has another Phone Number");
    XCTAssertEqual([call.duration doubleValue], 19.0, @"Call #0 now has another Duration");
    
    call = [calls objectAtIndex:1];
    XCTAssert([call.callType isEqualToString:@"Outgoing"], @"Call #1 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"tester.ds123@gmail.com"], @"Call #1 now has another Phone Number");
    XCTAssertEqual([call.duration doubleValue], 0.0, @"Call #1 now has another Duration");
    
    NSArray *jsonObj = [CallData jsonObjectWithData:calls];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Calls");
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

- (void)testCallData_NoCallerID_oldDB
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.callsOldNoCallerIdDbPath version:CallsDbVersion_old sinceDate:nil withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 1, @"Wrong count of DB entries. Did you change DB?");
    
    CallData *call = [calls objectAtIndex:0];
    XCTAssert([call.phoneNumber isEqualToString:@"No Caller ID"]);
    XCTAssert([call.person isEqualToString:@"N/A"]);
    XCTAssert([call.callType isEqualToString:@"Incoming"]);
    XCTAssert([call.numberType isEqualToString:@"N/A"]);
    XCTAssert([call.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1331073123]]); //06 Mar 2012 22:32:03
    XCTAssert([call.duration isEqualToNumber:[NSNumber numberWithInt:163]]);
}

- (void)testCallData_NoCallerID_iOS8
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.calls8xNoCallerIdDbPath version:CallsDbVersion_iOS8 sinceDate:nil withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 1, @"Wrong count of DB entries. Did you change DB?");
    
    CallData *call = [calls objectAtIndex:0];
    XCTAssert([call.phoneNumber isEqualToString:@"No Caller ID"]);
    XCTAssert([call.person isEqualToString:@"N/A"]);
    XCTAssert([call.callType isEqualToString:@"Missed"]);
    XCTAssert([call.numberType isEqualToString:@"N/A"]);
    XCTAssert([call.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1421412343]]); //16 Jan 2015 12:45:43
    XCTAssert([call.duration isEqualToNumber:[NSNumber numberWithInt:0]]);
}

- (void)testMaxCallId_oldDb
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSNumber *maxCallId = [CallData maxIdFromInternalDb:self.callsOldDbPath version:CallsDbVersion_old error:&err];
    XCTAssertNotNil(maxCallId);
    XCTAssert([maxCallId isEqualToNumber:[NSNumber numberWithInt:6762]]);
}

- (void)testMaxCallId_iOS8
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSNumber *maxCallId = [CallData maxIdFromInternalDb:self.calls8xDbPath version:CallsDbVersion_iOS8 error:&err];
    XCTAssertNotNil(maxCallId);
    XCTAssert([maxCallId isEqualToNumber:[NSNumber numberWithInt:17]]);
}

- (void)testLoadCallsById_oldDb
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSNumber *newMaxId = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.callsOldSelectByIdDbPath version:CallsDbVersion_old afterId:[NSNumber numberWithInt:6758] withContactsProvider:cache maxId:&newMaxId error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 1, @"There 2 calls in DB. Only one must be selected.");
    
    XCTAssertNotNil(newMaxId);
    XCTAssert([newMaxId isEqualToNumber:[NSNumber numberWithInt:6759]]);
    
    CallData *call = [calls objectAtIndex:0];
    XCTAssert([call.phoneNumber isEqualToString:@"5129684360"]);
    XCTAssert([call.person isEqualToString:@"Schroader, Russ"]);
    XCTAssert([call.callType isEqualToString:@"Outgoing"]);
    XCTAssert([call.numberType isEqualToString:@"Mobile"]);
    XCTAssert([call.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1425049200]]); //27 Feb 2015 15:00:00
    XCTAssert([call.duration isEqualToNumber:[NSNumber numberWithInt:300]]);
}

- (void)testLoadCallsById_iOS8
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    NSError *err = nil;
    NSNumber *newMaxId = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.calls8xSelectByIdDbPath version:CallsDbVersion_iOS8 afterId:[NSNumber numberWithInt:546] withContactsProvider:cache maxId:&newMaxId error:&err];
    XCTAssertNotNil(calls, @"Failed to load calls");
    XCTAssertEqual([calls count], 1, @"There 2 calls in DB. Only one must be selected.");
    
    XCTAssertNotNil(newMaxId);
    XCTAssert([newMaxId isEqualToNumber:[NSNumber numberWithInt:547]]);
    
    CallData *call = [calls objectAtIndex:0];
    XCTAssert([call.phoneNumber isEqualToString:@"tester.ds123@gmail.com"]);
    XCTAssert([call.person isEqualToString:@"N/A"]);
    XCTAssert([call.callType isEqualToString:@"Incoming"]);
    XCTAssert([call.numberType isEqualToString:@"N/A"]);
    XCTAssert([call.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1425049200]]); //27 Feb 2015 15:00:00
    XCTAssert([call.duration isEqualToNumber:[NSNumber numberWithInt:300]]);
}

@end
