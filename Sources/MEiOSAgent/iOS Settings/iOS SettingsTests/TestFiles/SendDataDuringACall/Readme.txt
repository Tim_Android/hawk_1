Sending data during a call situation:
1) call is started at 15:00, but it is not written to the database yet (because it's in progress)
2) agent acquires data at 15:02 - it doesn't see a call.
3) call is finished at 15:05, and ONLY AT THIS MOMENT it is written into database with start time 15:00 and duration 5 minutes.
4) agent acquires data at 15:12, AND IT MUST NOT SKIP THAT CALL.

Also pay attention that the call with bigger ID not always has later time. That call which was finished later will have bigger ID.
So, user can miss an incoming call while another call is in progress - in this situation missed call will have smaller ID, but later time.