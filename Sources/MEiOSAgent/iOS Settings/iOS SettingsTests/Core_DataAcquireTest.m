#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "ILoginChecker.h"
#import "MobileEnterpriseCore.h"
#import "CreateAgentResponse.h"
#import "RestClient.h"
#import "GlobalCfg.h"

#import "LocalStorage.h"

#import "ContactsCache.h"
#import "ContactData.h"
#import "MessageData.h"
#import "MmsData.h"
#import "CallData.h"


//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


// ================================================
//        DATA ACQUISITION TEST description
// ================================================
// - Data acquisition includes complex logic, and it's parts cannot be checked separately at the moment.
// - Mock objects preparation is complicated, and to prevent copy-pasting similar parts from test to test,
//   we use joining tests for different stages of data acquisition into one test.
// - Please read description inside of each test

#define SMS_DB_NAME @"sms.db"
#define CALLS_DB_NAME @"call_history.db"
#define CALLS8X_DB_NAME @"call_history8x.db"
#define CONTACTS_DB_NAME @"AddressBook.db"

#pragma mark --- Helpers ---

@interface MobileEnterpriseCore (CategoryForTesting)

//we must redeclare private methods here to make possible adding stubs for them
//otherwise they are invisible for compiler
//don't worry, if we create a stub for not existing method, OCMock will show a runtime error - it won't be skipped

- (RestClient *)restClient;
- (ContactsCache *)contactsCache;
- (void)setContactsCache:(ContactsCache *)value;
- (LocalStorage *)localStorage;
- (void)setLocalStorage:(LocalStorage *)value;

- (void)setIsAgentReadyToWork:(BOOL)isAgentReadyToWork;
- (BOOL)readAgentIdUsingDocumentsPath:(NSString *)documentsPath;
- (BOOL)saveAgentIdUsingDocumentsPath:(NSString *)documentsPath;
- (void)activateAgent;
- (void)startUpdatingLocationAndHeading;
- (void)obtainDatabases;

@end


@interface TestDataAcquireHelper : NSObject
+ (void)makeNilStubsForLoadingContactData:(id)mockContactData messageData:(id)mockMessageData mmsData:(id)mockMmsData callData:(id)mockCallData;
+ (LocalStorage *)createNiceMockLocalStorageForCore:(MobileEnterpriseCore *)core;
@end

@implementation TestDataAcquireHelper

+ (void)makeNilStubsForLoadingContactData:(id)mockContactData messageData:(id)mockMessageData mmsData:(id)mockMmsData callData:(id)mockCallData
{
    OCMStub([mockContactData loadFromInternalDb:[OCMArg any] forNumber:[OCMArg any] sinceDate:[OCMArg any]]).andReturn(nil);
    OCMStub([mockMessageData loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    OCMStub([mockMmsData loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    OCMStub([mockCallData loadFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    OCMStub([mockCallData loadFromInternalDb:[OCMArg any] version:CallsDbVersion_old sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
}

+ (LocalStorage *)createNiceMockLocalStorageForCore:(MobileEnterpriseCore *)core
{
    LocalStorage *mockLocalStorage = OCMPartialMock([core localStorage]);
    OCMStub([mockLocalStorage size]).andReturn([NSNumber numberWithInt:28672]); //just a normal size: no need to clear
    OCMStub([mockLocalStorage saveDeviceInfo:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveMessageData:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveMmsData:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveCallData:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]).andReturn(YES);
    return mockLocalStorage;
}

@end


@interface Core_DataAcquireTest : XCTestCase
@property (strong, nonatomic) NSString *expectedSmsDbPath;
@property (strong, nonatomic) NSString *expectedCallsDbPath;
@property (strong, nonatomic) NSString *expectedCalls8xDbPath;
@property (strong, nonatomic) NSString *expectedContactsDbPath;
@end

@implementation Core_DataAcquireTest

- (void)setUp
{
    [super setUp];
    
    NSString *docsDir = [[GlobalCfg sharedInstance] docsDir];
    _expectedSmsDbPath = [docsDir stringByAppendingPathComponent:SMS_DB_NAME];
    _expectedCallsDbPath = [docsDir stringByAppendingPathComponent:CALLS_DB_NAME];
    _expectedCalls8xDbPath = [docsDir stringByAppendingPathComponent:CALLS8X_DB_NAME];
    _expectedContactsDbPath = [docsDir stringByAppendingPathComponent:CONTACTS_DB_NAME];
}

#pragma mark --- Tests ---

- (void)testDataAcquire1
{
//Description, what is checked here:
// - acquired data is successfully saved and lastAcquisitionDate is updated
// - contactsCache is created and rebuilt - like it's a first data acquisition since agent application start
// - calls database: first data acquisition since agent installation is emulated (we don't know call IDs, so filter them by date)
// - calls database: only old format database (before iOS 8) exists
// - local storage is NOT cleared

//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);

    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]);
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must not be called during data acquisition"); });
    OCMStub([mockClassCfg getMaxAcquiredCallId]).andReturn([NSNumber numberWithInteger:0]);
    OCMStub([mockClassCfg setMaxAcquiredCallId:[OCMArg any]]);
    
    //mock MobileEnterpriseCore
    id mockCore = OCMPartialMock([[MobileEnterpriseCore alloc] init]);
    
    //mock properties
    LocalStorage *mockLocalStorage = [TestDataAcquireHelper createNiceMockLocalStorageForCore:mockCore];
    OCMStub([mockLocalStorage clear]).andDo(^(NSInvocation *invocation) { XCTFail(@"localStorage must NOT be cleared in this test"); });
    
    __block id mockContactsCache = nil;
    OCMStub([mockCore contactsCache]).andDo(^(NSInvocation *invocation) { [invocation setReturnValue:&mockContactsCache]; } );
    OCMStub([mockCore setContactsCache:[OCMArg any]]).andDo(^(NSInvocation *invocation)
    {
        mockContactsCache = OCMPartialMock([[ContactsCache alloc] init]);
        OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:YES]);
        OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must be rebuilt"); });
    });

    //mock methods
    OCMStub([mockCore startUpdatingLocationAndHeading]);
    OCMStub([mockCore obtainDatabases]);
    
    //device data mocks
    id mockContactDataClass = OCMStrictClassMock([ContactData class]);
    id mockMessageDataClass = OCMStrictClassMock([MessageData class]);
    id mockMmsDataClass = OCMStrictClassMock([MmsData class]);
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    [TestDataAcquireHelper makeNilStubsForLoadingContactData:mockContactDataClass messageData:mockMessageDataClass mmsData:mockMmsDataClass callData:mockCallDataClass];
    OCMStub([mockCallDataClass maxIdFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn([NSNumber numberWithInteger:14]);
    OCMStub([mockCallDataClass maxIdFromInternalDb:[OCMArg any] version:CallsDbVersion_old error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn([NSNumber numberWithInteger:14]);
    
    //mock NSFileManager
    id mockFileManager = OCMPartialMock([NSFileManager defaultManager]);
    id mockFileManagerClass = OCMStrictClassMock([NSFileManager class]);
    OCMStub([mockFileManagerClass defaultManager]).andReturn(mockFileManager);
    OCMStub([mockFileManager fileExistsAtPath:_expectedCallsDbPath]).andReturn(YES); //only old calls DB exists in this test
    OCMStub([mockFileManager fileExistsAtPath:_expectedCalls8xDbPath]).andReturn(NO);
    
//======= perform actions =======
    [mockCore doDataAcquire];
    
//======= check results (order of invocation is not checked)
    OCMVerify([mockClassCfg getLastAcquisitionDate]);
    
    OCMVerify([mockCore startUpdatingLocationAndHeading]);
    OCMVerify([mockCore obtainDatabases]);

    XCTAssert(mockContactsCache != nil, @"ContactsCache was nil at the beginning of this test - it must be created and rebuilt before acquiring other data");
    OCMVerify([mockContactDataClass loadFromInternalDb:_expectedContactsDbPath forNumber:nil sinceDate:nil]); //the date argument must be nil to load all existing contacts
    OCMVerify([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:YES]); //ensure contactsCache was rebuilt

    //ensure device data was loaded since lastAcquisitionDate
    OCMVerify([mockMessageDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockMmsDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockCallDataClass loadFromInternalDb:_expectedCallsDbPath version:CallsDbVersion_old sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockCallDataClass maxIdFromInternalDb:_expectedCallsDbPath version:CallsDbVersion_old error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    
    OCMVerify([mockLocalStorage size]);
    OCMVerify([mockLocalStorage saveDeviceInfo:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMessageData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveCallData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]);

    OCMVerify([mockClassCfg setLastAcquisitionDate:[OCMArg any]]); //ensure lastAcquisitionDate was updated
    OCMVerify([mockClassCfg setMaxAcquiredCallId:[NSNumber numberWithInteger:14]]);
}

- (void)testDataAcquire2
{
//Description, what is checked here:
// - acquired data is failed to save, so lastAcquisitionDate is NOT updated
// - contactsCache is already created - so it is only updated with new contacts during data acquisition
// - calls database: first data acquisition since agent installation is emulated (we don't know call IDs, so filter them by date)
// - calls database: both DB files (iOS 8 and old format) exist - so only iOS 8 format must be parsed
// - local storage is cleared
    
//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"lastAcquisitionDate must NOT be changed after FAILED acquisition"); });
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must not be called during data acquisition"); });
    OCMStub([mockClassCfg getMaxAcquiredCallId]).andReturn([NSNumber numberWithInteger:0]);
    OCMStub([mockClassCfg setMaxAcquiredCallId:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"maxAcquiredCallId must NOT be changed after FAILED acquisition"); });

    //mock MobileEnterpriseCore
    MobileEnterpriseCore *core = [[MobileEnterpriseCore alloc] init];
    id mockCore = OCMPartialMock(core);
    
    //mock properties
    LocalStorage *mockLocalStorage = OCMPartialMock([mockCore localStorage]);
    OCMStub([mockLocalStorage size]).andReturn([NSNumber numberWithInt:(10240 * 1024)]); //this size or bigger - local storage must be cleared
    OCMStub([mockLocalStorage saveDeviceInfo:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveMessageData:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveMmsData:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage saveCallData:[OCMArg any]]).andReturn(NO); //this result will be considered as data acquisition fail
    OCMStub([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]).andReturn(YES);
    OCMStub([mockLocalStorage clear]).andReturn(YES);
    
    id mockContactsCache = OCMPartialMock([[ContactsCache alloc] init]);
    OCMStub([mockCore contactsCache]).andReturn(mockContactsCache);
    OCMStub([mockCore setContactsCache:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be changed in this test"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:YES]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be rebuilt"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]);

    //mock methods
    OCMStub([mockCore startUpdatingLocationAndHeading]);
    OCMStub([mockCore obtainDatabases]);
    
    //device data mocks
    id mockContactDataClass = OCMStrictClassMock([ContactData class]);
    id mockMessageDataClass = OCMStrictClassMock([MessageData class]);
    id mockMmsDataClass = OCMStrictClassMock([MmsData class]);
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    [TestDataAcquireHelper makeNilStubsForLoadingContactData:mockContactDataClass messageData:mockMessageDataClass mmsData:mockMmsDataClass callData:mockCallDataClass];
    OCMStub([mockCallDataClass maxIdFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn([NSNumber numberWithInteger:14]);
    OCMStub([mockCallDataClass maxIdFromInternalDb:[OCMArg any] version:CallsDbVersion_old error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn([NSNumber numberWithInteger:14]);
    
    //mock NSFileManager
    id mockFileManager = OCMPartialMock([NSFileManager defaultManager]);
    id mockFileManagerClass = OCMStrictClassMock([NSFileManager class]);
    OCMStub([mockFileManagerClass defaultManager]).andReturn(mockFileManager);
    OCMStub([mockFileManager fileExistsAtPath:_expectedCallsDbPath]).andReturn(YES); //both calls databases exist - it's a correct case for devices which were updated to iOS 8.
    OCMStub([mockFileManager fileExistsAtPath:_expectedCalls8xDbPath]).andReturn(YES);

//======= perform actions =======
    [core doDataAcquire];
    
//======= check results (order of invocation is not checked)
    OCMVerify([mockClassCfg getLastAcquisitionDate]);
    
    OCMVerify([mockCore startUpdatingLocationAndHeading]);
    OCMVerify([mockCore obtainDatabases]);
    
    OCMVerify([mockContactDataClass loadFromInternalDb:_expectedContactsDbPath forNumber:nil sinceDate:lastAcquisitionDate]); //contacts must be loaded since lastAcquisitionDate
    OCMVerify([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]); //ensure contactsCache was NOT rebuilt
    
    //ensure device data was loaded since lastAcquisitionDate
    OCMVerify([mockMessageDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockMmsDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockCallDataClass loadFromInternalDb:_expectedCalls8xDbPath version:CallsDbVersion_iOS8 sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockCallDataClass maxIdFromInternalDb:_expectedCalls8xDbPath version:CallsDbVersion_iOS8 error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    
    OCMVerify([mockLocalStorage size]);
    OCMVerify([mockLocalStorage clear]);
    OCMVerify([mockLocalStorage saveDeviceInfo:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMessageData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveCallData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]);
}

- (void)testDataAcquire_onlyCalls8xExists_filterCallsById
{
//Description, what is checked here:
// - [ALREADY CHECKED in another test] acquired data is successfully saved and lastAcquisitionDate is updated
// - [ALREADY CHECKED in another test] contactsCache is updated (not rebuilt)
// - calls database: only iOS 8 format database exists
// - calls database: iOS 8 type, max call ID is known - so we must select only new IDs
// - [ALREADY CHECKED in another test] local storage is NOT cleared
    
//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]);
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must not be called during data acquisition"); });
    OCMStub([mockClassCfg getMaxAcquiredCallId]).andReturn([NSNumber numberWithInteger:14]);
    OCMStub([mockClassCfg setMaxAcquiredCallId:[OCMArg any]]);

    //mock MobileEnterpriseCore
    id mockCore = OCMPartialMock([[MobileEnterpriseCore alloc] init]);
    
    //mock properties
    LocalStorage *mockLocalStorage = [TestDataAcquireHelper createNiceMockLocalStorageForCore:mockCore];
    OCMStub([mockLocalStorage clear]).andDo(^(NSInvocation *invocation) { XCTFail(@"localStorage must NOT be cleared in this test"); });
    
    id mockContactsCache = OCMPartialMock([[ContactsCache alloc] init]);
    OCMStub([mockCore contactsCache]).andReturn(mockContactsCache);
    OCMStub([mockCore setContactsCache:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be changed in this test"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:YES]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be rebuilt"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]);
    
    //mock methods
    OCMStub([mockCore startUpdatingLocationAndHeading]);
    OCMStub([mockCore obtainDatabases]);
    
    //device data mocks
    id mockContactDataClass = OCMStrictClassMock([ContactData class]);
    id mockMessageDataClass = OCMStrictClassMock([MessageData class]);
    id mockMmsDataClass = OCMStrictClassMock([MmsData class]);
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockContactDataClass loadFromInternalDb:[OCMArg any] forNumber:[OCMArg any] sinceDate:[OCMArg any]]).andReturn(nil);
    OCMStub([mockMessageDataClass loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    OCMStub([mockMmsDataClass loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    
    //calls must not be filtered using time criteria in this test - they must be filtered by ID
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"CallData must not be filtered by time in this test"); });
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_old sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"CallData must not be filtered by time in this test"); });
    
    NSNumber *newMaxId = [NSNumber numberWithInteger:16];
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 afterId:[OCMArg any] withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSNumber **outMaxId = nil;
        [invocation getArgument:&outMaxId atIndex:6];
        *outMaxId = newMaxId;
    });
    
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_old afterId:[OCMArg any] withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSNumber **outMaxId = nil;
        [invocation getArgument:&outMaxId atIndex:6];
        *outMaxId = newMaxId;
    });
    
    //mock NSFileManager
    id mockFileManager = OCMPartialMock([NSFileManager defaultManager]);
    id mockFileManagerClass = OCMStrictClassMock([NSFileManager class]);
    OCMStub([mockFileManagerClass defaultManager]).andReturn(mockFileManager);
    OCMStub([mockFileManager fileExistsAtPath:_expectedCallsDbPath]).andReturn(NO);
    OCMStub([mockFileManager fileExistsAtPath:_expectedCalls8xDbPath]).andReturn(YES); //only calls 8x DB exists in this test
    
//======= perform actions =======
    [mockCore doDataAcquire];
    
//======= check results (order of invocation is not checked)
    OCMVerify([mockClassCfg getLastAcquisitionDate]);
    
    OCMVerify([mockCore startUpdatingLocationAndHeading]);
    OCMVerify([mockCore obtainDatabases]);
    
    XCTAssert(mockContactsCache != nil, @"ContactsCache was nil at the beginning of this test - it must be created and rebuilt before acquiring other data");
    OCMVerify([mockContactDataClass loadFromInternalDb:_expectedContactsDbPath forNumber:nil sinceDate:lastAcquisitionDate]); //contacts must be loaded since lastAcquisitionDate
    OCMVerify([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]); //ensure contactsCache was NOT rebuilt
    
    //ensure device data was loaded since lastAcquisitionDate
    OCMVerify([mockMessageDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockMmsDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    NSNumber *expectedCallId = [NSNumber numberWithInteger:14];
    OCMVerify([mockCallDataClass loadFromInternalDb:_expectedCalls8xDbPath version:CallsDbVersion_iOS8 afterId:expectedCallId withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    
    OCMVerify([mockLocalStorage size]);
    OCMVerify([mockLocalStorage saveDeviceInfo:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMessageData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveCallData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]);
    
    OCMVerify([mockClassCfg setLastAcquisitionDate:[OCMArg any]]); //ensure lastAcquisitionDate was updated
    OCMVerify([mockClassCfg setMaxAcquiredCallId:[NSNumber numberWithInteger:16]]);
}

- (void)testDataAcquire_filterCallsById_oldCallsDb
{
//Description, what is checked here:
// - [ALREADY CHECKED in another test] acquired data is successfully saved and lastAcquisitionDate is updated
// - [ALREADY CHECKED in another test] contactsCache is updated (not rebuilt)
// - [ALREADY CHECKED in another test] calls database: only old format database (before iOS 8) exists
// - calls database: old (before iOS 8) type, max call ID is known - so we must select only new IDs
// - [ALREADY CHECKED in another test] local storage is NOT cleared

//======= init objects =======
    //mock GlobalCfg
    id mockClassCfg = OCMStrictClassMock([GlobalCfg class]);
    
    //use different stub values for different dates
    NSDate *lastAcquisitionDate = [[NSDate alloc] initWithTimeIntervalSince1970:1424253600];
    NSDate *lastResendDate = [[NSDate alloc] initWithTimeIntervalSince1970:1423526400];
    
    OCMStub([mockClassCfg getLastAcquisitionDate]).andReturn(lastAcquisitionDate);
    OCMStub([mockClassCfg setLastAcquisitionDate:[OCMArg any]]);
    OCMStub([mockClassCfg getLastResendDate]).andReturn(lastResendDate);
    OCMStub([mockClassCfg setLastResendDate:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"setLastResendDate must not be called during data acquisition"); });
    OCMStub([mockClassCfg getMaxAcquiredCallId]).andReturn([NSNumber numberWithInteger:14]);
    OCMStub([mockClassCfg setMaxAcquiredCallId:[OCMArg any]]);
    
    //mock MobileEnterpriseCore
    id mockCore = OCMPartialMock([[MobileEnterpriseCore alloc] init]);
    
    //mock properties
    LocalStorage *mockLocalStorage = [TestDataAcquireHelper createNiceMockLocalStorageForCore:mockCore];
    OCMStub([mockLocalStorage clear]).andDo(^(NSInvocation *invocation) { XCTFail(@"localStorage must NOT be cleared in this test"); });
    
    id mockContactsCache = OCMPartialMock([[ContactsCache alloc] init]);
    OCMStub([mockCore contactsCache]).andReturn(mockContactsCache);
    OCMStub([mockCore setContactsCache:[OCMArg any]]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be changed in this test"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:YES]).andDo(^(NSInvocation *invocation) { XCTFail(@"contactsCache must NOT be rebuilt"); });
    OCMStub([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]);
    
    //mock methods
    OCMStub([mockCore startUpdatingLocationAndHeading]);
    OCMStub([mockCore obtainDatabases]);
    
    //device data mocks
    id mockContactDataClass = OCMStrictClassMock([ContactData class]);
    id mockMessageDataClass = OCMStrictClassMock([MessageData class]);
    id mockMmsDataClass = OCMStrictClassMock([MmsData class]);
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockContactDataClass loadFromInternalDb:[OCMArg any] forNumber:[OCMArg any] sinceDate:[OCMArg any]]).andReturn(nil);
    OCMStub([mockMessageDataClass loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    OCMStub([mockMmsDataClass loadFromInternalDb:[OCMArg any] sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil);
    
    //calls must not be filtered using time criteria in this test - they must be filtered by ID
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"CallData must not be filtered by time in this test"); });
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_old sinceDate:[OCMArg any] withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation) { XCTFail(@"CallData must not be filtered by time in this test"); });
    
    NSNumber *newMaxId = [NSNumber numberWithInteger:16];
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_iOS8 afterId:[OCMArg any] withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSNumber **outMaxId = nil;
        [invocation getArgument:&outMaxId atIndex:6];
        *outMaxId = newMaxId;
    });
    
    OCMStub([mockCallDataClass loadFromInternalDb:[OCMArg any] version:CallsDbVersion_old afterId:[OCMArg any] withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(nil).andDo(^(NSInvocation *invocation)
    {
        __unsafe_unretained NSNumber **outMaxId = nil;
        [invocation getArgument:&outMaxId atIndex:6];
        *outMaxId = newMaxId;
    });
    
    //mock NSFileManager
    id mockFileManager = OCMPartialMock([NSFileManager defaultManager]);
    id mockFileManagerClass = OCMStrictClassMock([NSFileManager class]);
    OCMStub([mockFileManagerClass defaultManager]).andReturn(mockFileManager);
    OCMStub([mockFileManager fileExistsAtPath:_expectedCallsDbPath]).andReturn(YES); //only old calls DB exists in this test
    OCMStub([mockFileManager fileExistsAtPath:_expectedCalls8xDbPath]).andReturn(NO);
    
//======= perform actions =======
    [mockCore doDataAcquire];
    
//======= check results (order of invocation is not checked)
    OCMVerify([mockClassCfg getLastAcquisitionDate]);
    
    OCMVerify([mockCore startUpdatingLocationAndHeading]);
    OCMVerify([mockCore obtainDatabases]);
    
    XCTAssert(mockContactsCache != nil, @"ContactsCache was nil at the beginning of this test - it must be created and rebuilt before acquiring other data");
    OCMVerify([mockContactDataClass loadFromInternalDb:_expectedContactsDbPath forNumber:nil sinceDate:lastAcquisitionDate]); //contacts must be loaded since lastAcquisitionDate
    OCMVerify([mockContactsCache updateWithAddressBook:[OCMArg any] rebuild:NO]); //ensure contactsCache was NOT rebuilt
    
    //ensure device data was loaded since lastAcquisitionDate
    OCMVerify([mockMessageDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    OCMVerify([mockMmsDataClass loadFromInternalDb:_expectedSmsDbPath sinceDate:lastAcquisitionDate withContactsProvider:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    NSNumber *expectedCallId = [NSNumber numberWithInteger:14];
    OCMVerify([mockCallDataClass loadFromInternalDb:_expectedCallsDbPath version:CallsDbVersion_old afterId:expectedCallId withContactsProvider:[OCMArg any] maxId:(NSNumber __autoreleasing **)[OCMArg anyPointer] error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
    
    OCMVerify([mockLocalStorage size]);
    OCMVerify([mockLocalStorage saveDeviceInfo:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMessageData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveCallData:[OCMArg any]]);
    OCMVerify([mockLocalStorage saveMmsAttachmentsInfo:[OCMArg any]]);
    
    OCMVerify([mockClassCfg setLastAcquisitionDate:[OCMArg any]]); //ensure lastAcquisitionDate was updated
    OCMVerify([mockClassCfg setMaxAcquiredCallId:[NSNumber numberWithInteger:16]]);
}

- (void) test_MessagesAndContacts_Merging
{
    NSString* smsDbPath = [[NSBundle bundleForClass:[self class]] pathForResource: @"iOS9_sms" ofType: @"db"];
    
    id<IContactsProvider> contactsProvider = OCMProtocolMock(@protocol(IContactsProvider));
    OCMStub([contactsProvider searchForNumber: @"+380636463036"]).andReturn([self makeMockContactWithName: @"Test name #1"]);
    OCMStub([contactsProvider searchForNumber: @"8017870033"]).andReturn([self makeMockContactWithName: @"Test name #2"]);
    OCMStub([contactsProvider searchForNumber: @"1020"]).andReturn([self makeMockContactWithName: @"Test name #3"]);
    
    OCMStub([contactsProvider searchForNumber: @"76575757567657"]).andReturn(nil);
    
    NSError* error = nil;
    NSArray<MessageData*>* messages = [MessageData loadFromInternalDb:smsDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] withContactsProvider:contactsProvider
                                                                error:&error];
    
    XCTAssertNil(error);
    
    XCTAssertEqualObjects(@"Test name #1", [self messageForNumber: @"+380636463036" inMessages: messages].contactName);
    XCTAssertEqualObjects(@"Test name #2", [self messageForNumber: @"8017870033"    inMessages: messages].contactName);
    XCTAssertEqualObjects(@"Test name #3", [self messageForNumber: @"1020"          inMessages: messages].contactName);
    
    MessageData* messageWithNilName = [self messageForNumber: @"76575757567657" inMessages: messages];
    
    XCTAssertNotNil(messageWithNilName);
    XCTAssertNil(messageWithNilName.contactName);
}

- (void) test_MmsAndContacts_Merging
{
    NSString* smsDbPath = [[NSBundle bundleForClass:[self class]] pathForResource: @"smsGroupConversations" ofType: @"db"];
    
    id<IContactsProvider> contactsProvider = OCMProtocolMock(@protocol(IContactsProvider));
    OCMStub([contactsProvider searchForNumber: @"test.ds@icloud.com"]).andReturn([self makeMockContactWithName: @"Test name #1"]);
    OCMStub([contactsProvider searchForNumber: @"testapriorit@mail.ru, vovathegreat@mail.ru"]).andReturn([self makeMockContactWithName: @"Test name #2"]);
    
    OCMStub([contactsProvider searchForNumber: @"76575757567657"]).andReturn(nil);
    
    NSError* error = nil;
    NSArray<MmsData*>* mmses = [MmsData loadFromInternalDb:smsDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] withContactsProvider:contactsProvider error:&error];
    
    XCTAssertNil(error);
    
    
    XCTAssertEqualObjects(@"Test name #1", [self mmsForNumber: @"test.ds@icloud.com"                         inMmses: mmses].contactName);
    XCTAssertEqualObjects(@"Test name #2", [self mmsForNumber: @"testapriorit@mail.ru, vovathegreat@mail.ru" inMmses: mmses].contactName);
}

- (void) test_MmsAndContacts_Merging_withNilName
{
    NSString* smsDbPath = [[NSBundle bundleForClass:[self class]] pathForResource: @"smsGroupConversations" ofType: @"db"];
    
    id<IContactsProvider> contactsProvider = OCMProtocolMock(@protocol(IContactsProvider));
    OCMStub([contactsProvider searchForNumber: [OCMArg any]]).andReturn(nil);
    
    NSError* error = nil;
    NSArray<MmsData*>* mmses = [MmsData loadFromInternalDb:smsDbPath sinceDate:[NSDate dateWithTimeIntervalSince1970: 0] withContactsProvider:contactsProvider error:&error];
    
    XCTAssertNil(error);

    MmsData* mms = [self mmsForNumber: @"test.ds@icloud.com" inMmses: mmses];
    XCTAssertNotNil(mms);
    XCTAssertNil(mms.contactName);
    
    mms = [self mmsForNumber: @"testapriorit@mail.ru, vovathegreat@mail.ru" inMmses: mmses];
    XCTAssertNotNil(mms);
    XCTAssertNil(mms.contactName);
}

#pragma mark - private

- (ContactData*)makeMockContactWithName:(NSString*)name
{
    ContactData* contact = OCMClassMock([ContactData class]);
    OCMStub([contact person]).andReturn(name);
 
    return contact;
}

- (MmsData*) mmsForNumber: (NSString*)phoneNumber inMmses: (NSArray<MmsData*>*)mmses
{
    for (MmsData* mms in mmses)
    {
        if ([mms.phoneNumber isEqualToString: phoneNumber])
        {
            return mms;
        }
    }
    
    return nil;
}

- (MessageData*) messageForNumber: (NSString*)phoneNumber inMessages: (NSArray<MessageData*>*)messages
{
    for (MessageData* message in messages)
    {
        if ([message.phoneNumber isEqualToString: phoneNumber])
        {
            return message;
        }
    }
    
    return nil;
}

@end
