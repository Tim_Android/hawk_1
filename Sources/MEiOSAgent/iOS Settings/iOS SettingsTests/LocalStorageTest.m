#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "LocalStorage.h"
#import "CallData.h"
#import "Utils.h"

#import "TestDataCreator.h"

//Some tests for LocalStorage also exist in iOS_SettingsTests.m

//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))


@interface LocalStorageTest : XCTestCase
@property (strong, nonatomic) NSString *localStorageDbPath;
@end

@implementation LocalStorageTest

- (void)setUp
{
    [super setUp];
    
    NSString *docsDir = [Utils getPathForDocumentsDirectory];
    _localStorageDbPath = [docsDir stringByAppendingPathComponent:@"LocalStorageTest_1.db"];
}

- (void)testEraseItemsByIds_EraseNil
{
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockCallDataClass eraseFromDb:[OCMArg any] items:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"eraseFromDb must not be called in this test"); });
    
    LocalStorage *localStorage = [[LocalStorage alloc] initWithDbPath:_localStorageDbPath];
    BOOL result = [localStorage eraseDataItems:nil ofType:[CallData class]];
    
    XCTAssert(result == YES);
}

- (void)testEraseItemsByIds_EraseEmptyArray
{
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockCallDataClass eraseFromDb:[OCMArg any] items:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES).andDo(^(NSInvocation *invocation) { XCTFail(@"eraseFromDb must not be called in this test"); });
    
    LocalStorage *localStorage = [[LocalStorage alloc] initWithDbPath:_localStorageDbPath];
    BOOL result = [localStorage eraseDataItems:[[NSArray alloc] init] ofType:[CallData class]];
    
    XCTAssert(result == YES);
}

- (void)testEraseItemsByIds_Ok
{
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockCallDataClass eraseFromDb:[OCMArg any] items:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(YES);
    
    NSArray *callsToErase = [TestDataCreator createStubCalls];
    
    LocalStorage *localStorage = [[LocalStorage alloc] initWithDbPath:_localStorageDbPath];
    BOOL result = [localStorage eraseDataItems:callsToErase ofType:[CallData class]];
    
    XCTAssert(result == YES);
    OCMVerify([mockCallDataClass eraseFromDb:_localStorageDbPath items:callsToErase error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
}

- (void)testEraseItemsByIds_Fail
{
    id mockCallDataClass = OCMStrictClassMock([CallData class]);
    OCMStub([mockCallDataClass eraseFromDb:[OCMArg any] items:[OCMArg any] error:(NSError __autoreleasing **)[OCMArg anyPointer]]).andReturn(NO); //return NO, must be considered as fail
    
    NSArray *callsToErase = [TestDataCreator createStubCalls];
    
    LocalStorage *localStorage = [[LocalStorage alloc] initWithDbPath:_localStorageDbPath];
    BOOL result = [localStorage eraseDataItems:callsToErase ofType:[CallData class]];
    
    XCTAssert(result == NO);
    OCMVerify([mockCallDataClass eraseFromDb:_localStorageDbPath items:callsToErase error:(NSError __autoreleasing **)[OCMArg anyPointer]]);
}

@end