#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>

#import "Utils.h"
#import "Constants.h"

#import "MessageData.h"
#import "MmsData.h"
#import "MmsAttachmentInfoData.h"
#import "LocationData.h"
#import "CallData.h"
#import "CrashData.h"

#import "ContactData.h"
#import "ContactsCache.h"

#import "LocalStorage.h"

#import "FMDB.h"
#import "FmdbHelper.h"

@interface iOS_SettingsTests : XCTestCase

@property (strong, nonatomic) NSString *smsDbPath;
@property (strong, nonatomic) NSString *smsWithItemTypeDbPath;
@property (strong, nonatomic) NSString *smsWithGroupConversationsDbPath;
@property (strong, nonatomic) NSString *callsOldDbPath;
@property (strong, nonatomic) NSString *calls8xDbPath;
@property (strong, nonatomic) NSString *contactsDbPath;
@property (strong, nonatomic) NSString *localStorageDbPath;
@property (strong, nonatomic) LocalStorage *localStorage;

@end

//
// Warning!
// If you see a lot of unexpected test failures - pick DB's from project dir. and copy
// them to iPhone simulator. Test target can contain corrupted DB's in resources.
//

@implementation iOS_SettingsTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    NSBundle *currentBundle = [NSBundle bundleForClass:[self class]];
    _smsDbPath = [currentBundle pathForResource:@"sms" ofType:@"db"];
    _callsOldDbPath = [currentBundle pathForResource:@"call_history" ofType:@"db"];
    _calls8xDbPath = [currentBundle pathForResource:@"CallHistory" ofType:@"storedata"];
    _contactsDbPath = [currentBundle pathForResource:@"AddressBook" ofType:@"db"];
    _smsWithItemTypeDbPath = [currentBundle pathForResource:@"smsWithItemType" ofType:@"db"];
    _smsWithGroupConversationsDbPath = [currentBundle pathForResource:@"smsGroupConversations" ofType:@"db"];

    NSString *docsDir = [Utils getPathForDocumentsDirectory];
    _localStorageDbPath = [docsDir stringByAppendingPathComponent:@"test_local_storage.sqlitedb"];
    _localStorage = [[LocalStorage alloc] initWithDbPath:self.localStorageDbPath];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark -
#pragma mark Device Data

- (void)testMessageData_NoItemTypeColumn
{
    //in this test we use old DB format (there is no item_type column in message table)
    NSError *err = nil;
    
    //first - ensure the database doesn't have item_type column
    FMDatabase *db = [FMDatabase databaseWithPath:self.smsDbPath];
    BOOL isItemTypeColumnExist = [FmdbHelper isColumnExist:@"item_type" inTable:@"message" inDb:db error:&err];
    XCTAssert(isItemTypeColumnExist == NO, "This test must work with database without item_type column in message table");

    NSArray *msgs = [MessageData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to load messages");
    XCTAssertEqual([msgs count], 3091, @"Wrong count of DB entries. Did you change DB?");
    
    MessageData *msg = [msgs objectAtIndex:1234];
    XCTAssert([msg.phoneNumber isEqualToString:@"+18018368388"], @"Message #1234 now has another phone number");
    
    NSArray *jsonObj = [MessageData jsonObjectWithData:msgs];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Messages");
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

- (void)testMessageData_WithItemTypeColumn
{
    //in this test we use new DB format (item_type column exists in message table)
    NSError *err = nil;
    
    //first - ensure the database has item_type column
    FMDatabase *db = [FMDatabase databaseWithPath:self.smsWithItemTypeDbPath];
    BOOL isItemTypeColumnExist = [FmdbHelper isColumnExist:@"item_type" inTable:@"message" inDb:db error:&err];
    XCTAssert(isItemTypeColumnExist == YES, "This test must work with database with item_type column in message table");

    NSArray *msgs = [MessageData loadFromInternalDb:self.smsWithItemTypeDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to load messages");
    XCTAssertEqual([msgs count], 1, @"It seems, notifications are present in results. Only one message is expected.");
    
    MessageData *msg = [msgs objectAtIndex:0];
    XCTAssert([msg.phoneNumber isEqualToString:@"test.ds@icloud.com"]);
    XCTAssert([msg.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1423762944]]); //12 Feb 2015 17:42:24
    XCTAssert([msg.body isEqualToString:@"Message for ticket 2"]);
    XCTAssert([msg.messageType isEqualToString:@"Inbox"]);
    
    NSArray *jsonObj = [MessageData jsonObjectWithData:msgs];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Messages");
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

- (void)testMessageData_GroupConversations
{
    NSError *err = nil;
    NSArray *msgs = [MessageData loadFromInternalDb:self.smsWithGroupConversationsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to load messages");
    XCTAssertEqual([msgs count], 3, @"Wrong count of DB entries. Did you change DB?");
    
    MessageData *msg = [msgs objectAtIndex:0];
    XCTAssert([msg.phoneNumber isEqualToString:@"testapriorit@mail.ru, vovathegreat@mail.ru"]);
    XCTAssert([msg.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1424884992]]); //25 Feb 2015 17:23:12
    XCTAssert([msg.body isEqualToString:@"After first 10"]);
    XCTAssert([msg.messageType isEqualToString:@"Inbox"]);
    
    msg = [msgs objectAtIndex:1];
    XCTAssert([msg.phoneNumber isEqualToString:@"testapriorit@mail.ru, vovathegreat@mail.ru"]);
    XCTAssert([msg.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1425033937]]); //27 Feb 2015 10:45:37
    XCTAssert([msg.body isEqualToString:@"Test text "]);
    XCTAssert([msg.messageType isEqualToString:@"Outbox"]);

    msg = [msgs objectAtIndex:2];
    XCTAssert([msg.phoneNumber isEqualToString:@"testapriorit@mail.ru"]);
    XCTAssert([msg.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1425033953]]); //27 Feb 2015 10:45:53
    XCTAssert([msg.body isEqualToString:@"Hi"]);
    XCTAssert([msg.messageType isEqualToString:@"Outbox"]);
}

- (void)testMmsData
{
    NSError *err = nil;

    NSArray *msgs = [MmsData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to load mms's");
    XCTAssertEqual([msgs count], 110, @"Wrong count of DB entries. Did you change DB?");
    
    MmsData *mms = [msgs objectAtIndex:83]; // This MMS has three attachments
    NSArray *attachments = mms.attachments;
    XCTAssertEqual([attachments count], 3, @"Failed to match attachments count");
    
    NSArray *jsonObj = [MmsData jsonObjectWithData:msgs];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from mms's");
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

- (void)testMmsData_GroupConversations
{
    NSError *err = nil;
    NSArray *msgs = [MmsData loadFromInternalDb:self.smsWithGroupConversationsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to load mms's");
    XCTAssertEqual([msgs count], 3, @"Wrong count of DB entries. Did you change DB?");
    
    MmsData *mms = [msgs objectAtIndex:0];
    XCTAssert([mms.phoneNumber isEqualToString:@"test.ds@icloud.com"]);
    XCTAssert([mms.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1424878371]]); //25 Feb 2015 15:32:51
    XCTAssert([mms.body isEqualToString:@"N/A"]);
    XCTAssert([mms.messageType isEqualToString:@"Outbox"]);
    XCTAssert([mms.messageSubject isEqualToString:@"N/A"]);
    XCTAssert([mms.messageId isEqualToNumber:[NSNumber numberWithInt:259]]);
    XCTAssertNotNil(mms.attachments);
    XCTAssert([mms.attachments count] == 1);
    MmsAttachmentInfoData *attachment = [mms.attachments objectAtIndex:0];
    XCTAssert([attachment.attachmentPath isEqualToString:@"~/Library/SMS/Attachments/9b/11/86A8F402-9F1B-4561-B867-41A6B025F1D7/IMG_0031.JPG"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    XCTAssert([attachment.attachmentId isEqualToNumber:[NSNumber numberWithInt:184]]);
    XCTAssert([attachment.messageId isEqualToNumber:[NSNumber numberWithInt:259]]);

    mms = [msgs objectAtIndex:1];
    XCTAssert([mms.phoneNumber isEqualToString:@"testapriorit@mail.ru, vovathegreat@mail.ru"]);
    XCTAssert([mms.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1424885373]]); //25 Feb 2015 17:29:33
    XCTAssert([mms.body isEqualToString:@"Outbox 3 messages"]);
    XCTAssert([mms.messageType isEqualToString:@"Inbox"]);
    XCTAssert([mms.messageSubject isEqualToString:@"N/A"]);
    XCTAssert([mms.messageId isEqualToNumber:[NSNumber numberWithInt:273]]);
    XCTAssertNotNil(mms.attachments);
    XCTAssert([mms.attachments count] == 2);
    attachment = [mms.attachments objectAtIndex:0];
    XCTAssert([attachment.attachmentPath isEqualToString:@"~/Library/SMS/Attachments/84/04/BABDBF4D-1D3A-4C49-9BB0-81F87967A1A2/IMG_1017.jpg"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    XCTAssert([attachment.attachmentId isEqualToNumber:[NSNumber numberWithInt:190]]);
    XCTAssert([attachment.messageId isEqualToNumber:[NSNumber numberWithInt:273]]);
    attachment = [mms.attachments objectAtIndex:1];
    XCTAssert([attachment.attachmentPath isEqualToString:@"~/Library/SMS/Attachments/ff/15/395A32E9-CD04-4848-948C-D8B3E766D4A5/IMG_6849.jpg"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    XCTAssert([attachment.attachmentId isEqualToNumber:[NSNumber numberWithInt:191]]);
    XCTAssert([attachment.messageId isEqualToNumber:[NSNumber numberWithInt:273]]);
    
    mms = [msgs objectAtIndex:2];
    XCTAssert([mms.phoneNumber isEqualToString:@"testapriorit@mail.ru, vovathegreat@mail.ru"]);
    XCTAssert([mms.timestamp isEqualToDate:[NSDate dateWithTimeIntervalSince1970:1425033937]]); //27 Feb 2015 10:45:37
    XCTAssert([mms.body isEqualToString:@"N/A"]);
    XCTAssert([mms.messageType isEqualToString:@"Outbox"]);
    XCTAssert([mms.messageSubject isEqualToString:@"N/A"]);
    XCTAssert([mms.messageId isEqualToNumber:[NSNumber numberWithInt:274]]);
    XCTAssertNotNil(mms.attachments);
    XCTAssert([mms.attachments count] == 2);
    attachment = [mms.attachments objectAtIndex:0];
    XCTAssert([attachment.attachmentPath isEqualToString:@"~/Library/SMS/Attachments/8b/11/238694CE-2C44-4240-8D05-2286D2D6E4A7/IMG_0031.JPG"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    XCTAssert([attachment.attachmentId isEqualToNumber:[NSNumber numberWithInt:192]]);
    XCTAssert([attachment.messageId isEqualToNumber:[NSNumber numberWithInt:274]]);
    attachment = [mms.attachments objectAtIndex:1];
    XCTAssert([attachment.attachmentPath isEqualToString:@"~/Library/SMS/Attachments/5a/10/B72C4705-4021-4834-BC39-0096463B97F5/IMG_0029.JPG"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    XCTAssert([attachment.attachmentId isEqualToNumber:[NSNumber numberWithInt:193]]);
    XCTAssert([attachment.messageId isEqualToNumber:[NSNumber numberWithInt:274]]);
}

- (void)testContactsData
{
    NSArray *contacts = nil;
    
    contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    XCTAssertEqual(54, [contacts count], @"Wrong count of DB entries. Did you change DB?");
    
    // Since date
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    [dc setDay:26];
    [dc setMonth:12];
    [dc setYear:2013];
    [dc setHour:19];
    [dc setMinute:1];
    [dc setSecond:23];

    NSCalendar *c = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [c dateFromComponents:dc];
    
    contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:date];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    XCTAssertEqual(9, [contacts count], @"Wrong count of DB entries. Did you change DB?");
}

- (void)testCrashData
{
    NSException *ex = [self exceptionForAddingNilToArray];
    XCTAssertNotNil(ex);
    
    CrashData* crashData = [[CrashData alloc] initWithException:ex textLog:nil];
    
    NSRange r1 = [[crashData text] rangeOfString:@"-[__NSArrayM insertObject:atIndex:]: object cannot be nil"];
    NSRange r2 = [[crashData text] rangeOfString:@"-[iOS_SettingsTests testCrashData]"];
    
    XCTAssertNotEqual(r1.location, NSNotFound, @"Failed to find 'reason' in exception");
    XCTAssertNotEqual(r2.location, NSNotFound, @"Failed to validate stack trace");
    
    NSArray *jsonObj = [CrashData jsonObjectWithData:[NSArray arrayWithObject:crashData]];
    XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Crash entry");
    
    NSError *err = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
    XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
    XCTAssertNotEqual([jsonData length], 0, @"JSON data is empty");
}

#pragma mark -
#pragma mark MMS Attachments

- (void)testMmsAttachmentData
{
    NSArray *attachments = [MmsAttachmentInfoData loadAttachmentsFromDb:self.smsDbPath forMessageId:@(1988)];
    XCTAssertEqual([attachments count], 3);
    
    // Serialization of single item
    NSString *strSingleAttachment = [MmsAttachmentInfoData stringFromAttachment:[attachments objectAtIndex:0]];
    MmsAttachmentInfoData *attachment = [MmsAttachmentInfoData attachmentFromString:strSingleAttachment];
    XCTAssertEqual([attachment.messageId intValue], 1988);
    XCTAssertEqual([attachment.attachmentId intValue], 86);
    XCTAssert([attachment.attachmentPath isEqualToString:@"/var/mobile/Library/SMS/Parts/3f/03/6531-0.jpg"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
    
    // Serialization of array
    NSString *strAttachments = [MmsAttachmentInfoData stringFromAttachmentsArray:attachments];
    NSArray *arrAttachments = [MmsAttachmentInfoData attachmentsArrayFromString:strAttachments];
    XCTAssertEqual([arrAttachments count], 3);
    
    attachment = [arrAttachments objectAtIndex:0];
    XCTAssertEqual([attachment.messageId intValue], 1988);
    XCTAssertEqual([attachment.attachmentId intValue], 86);
    XCTAssert([attachment.attachmentPath isEqualToString:@"/var/mobile/Library/SMS/Parts/3f/03/6531-0.jpg"]);
    XCTAssert([attachment.attachmentType isEqualToString:@"image/jpeg"]);
}

#pragma mark -
#pragma mark Other Things

- (void)testHostReachable
{
    XCTAssertTrue([Utils isHostReachable:@"www.google.com"]);
    XCTAssertFalse([Utils isHostReachable:@"www.fgdhasfd.com"]);
}

- (void)testJsonObjSplitting  //TODO: fix this logic of splitting JSON objects into chunks, look comments in [Utils chunkBigJsonObject:onChunksNum:]
{
    NSArray *jsonObj = nil;
    NSUInteger jsonDataSz = 0;
    
    {
        NSError *err = nil;
        NSMutableArray *msgs = [NSMutableArray arrayWithArray:[MessageData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err]];
        XCTAssertNotNil(msgs, @"failed to load messages");
        
        // This will make splitting harder
        [msgs addObject:[msgs objectAtIndex:0]];

        // Validate whole object
        jsonObj = [MessageData jsonObjectWithData:msgs];
        XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonObj], @"Failed to validate JSON object created from Messages");

        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:0 error:&err];
        XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);

        // Validate size
        jsonDataSz = [jsonData length];
        XCTAssert(jsonDataSz >= (kJsonMaxTransferableSize), @"JSON data is not large enough");
    }
    
    // Split into chunks
    NSUInteger jsonChunksNum = (jsonDataSz / kJsonMaxTransferableSize) + 1;
    NSArray *jsonChunks = [Utils chunkBigJsonObject:jsonObj onChunksNum:jsonChunksNum];
    
    XCTAssertNotNil(jsonChunks, @"Failed to split large JSON object");
    
    {
        NSUInteger totalEntries = 0;
        NSError *err = nil;
        
        // Validate every chunk 
        for (id jsonChunk in jsonChunks)
        {
            XCTAssertTrue([NSJSONSerialization isValidJSONObject:jsonChunk], @"Failed to validate JSON chunk");
            
            NSData *jsonChunkData = [NSJSONSerialization dataWithJSONObject:jsonChunk options:0 error:&err];
            XCTAssertNil(err, @"JSON serialization failed with error:\n%@", err);
            
            NSUInteger jsonChunkDataSz = [jsonChunkData length];
            XCTAssert(jsonChunkDataSz <= (kJsonMaxTransferableSize + 1024), @"JSON data is very large");
            
            totalEntries += [jsonChunk count];
        }
        
        XCTAssertEqual(totalEntries, [jsonObj count], @"Failed to match count of entries between chunked and solid JSON objects");
    }
    
    XCTAssertEqual([jsonChunks count], 4, @"Unexpected count of JSON chunks");
    XCTAssertEqual([[jsonChunks objectAtIndex:3] count], 1, @"Unexpected count of entries if last chunk");
}

- (void)testDbErrorReporting
{
    NSError *err = nil;
    NSArray *msgs = [MessageData loadFromInternalDb:@"/usr/bin" sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNil(msgs, @"Fail to generate error during loading Message Data");
    XCTAssertEqual(14, [err code], @"Failed to macth actual and expected error codes");
    
    NSString *descr = [[err userInfo] objectForKey:NSLocalizedDescriptionKey];
    XCTAssert([descr isEqualToString:@"unable to open database file"], @"Failed to macth actual and expected error messages");
}
 
#pragma mark -
#pragma mark Local Storage

- (void)testLocalStorageForCalls
{
    // Since date
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    [dc setDay:14];
    [dc setMonth:3];
    [dc setYear:2012];
    [dc setHour:0];
    [dc setMinute:0];
    [dc setSecond:0];

    NSDate *date = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] dateFromComponents:dc];
    
    // Get Contacts and cache'em
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    ContactsCache *cache = [[ContactsCache alloc] init];
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    // Build data arrays
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.callsOldDbPath version:CallsDbVersion_old sinceDate:nil withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to get Calls");
    
    // Save something
    BOOL isSaved = [self.localStorage saveCallData:calls];
    XCTAssert(isSaved, @"Failed to save CallsData in Local Storage");
    
    // Load data back
    NSArray *loaded = [self.localStorage loadCallDataSinceDate:date];
    XCTAssertNotNil(loaded, @"Failed to load CallsData from Local Storage");
    XCTAssertEqual(14, [loaded count], @"Failed to match count of entries before and after saving");
    
    // Erase
    BOOL isErased = [self.localStorage eraseCallDataSinceDate:date];
    XCTAssert(isErased, @"Failed to erase CallsData from Local Storage");
    
    // Try loading again
    loaded = [self.localStorage loadCallDataSinceDate:nil];
    XCTAssertNotNil(loaded, @"Failed to load CallsData from Local Storage");
    XCTAssertEqual(86, [loaded count], @"Failed to match count of entries before and after erasing");
    
    // Erase again
    isErased = [self.localStorage eraseCallDataSinceDate:nil];
    XCTAssert(isErased, @"Failed to erase CallsData from Local Storage");
    
    // Try loading again
    loaded = [self.localStorage loadCallDataSinceDate:nil];
    XCTAssertNil(loaded, @"Failed to erase all CallsData from Local Storage");
}

- (void)testLocalStorageForCalls_iOS8
{
    //in this test we check new format of source database (iOS8) doesn't affect local storage
    
    NSDate *sinceDate = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:443400000.0]; //about 20 Jan 2015 - taken from DB to select only 2 calls

    // Get Contacts and cache'em
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    ContactsCache *cache = [[ContactsCache alloc] init];
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    // Build data arrays
    NSError *err = nil;
    NSArray *calls = [CallData loadFromInternalDb:self.calls8xDbPath version:CallsDbVersion_iOS8 sinceDate:nil withContactsProvider:cache error:&err];
    XCTAssertNotNil(calls, @"Failed to get Calls");
    
    // Save something
    BOOL isSaved = [self.localStorage saveCallData:calls];
    XCTAssert(isSaved, @"Failed to save CallsData in Local Storage");
    
    // Load data back
    NSArray *loaded = [self.localStorage loadCallDataSinceDate:sinceDate];
    XCTAssertNotNil(loaded, @"Failed to load CallsData from Local Storage");
    XCTAssertEqual(2, [loaded count], @"Failed to match count of entries before and after saving");
    
    CallData *call = [loaded objectAtIndex:0];
    XCTAssert([call.callType isEqualToString:@"Outgoing"], @"Call #0 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"tester.ds123@gmail.com"], @"Call #0 now has another Phone Number");
    XCTAssertEqual([call.duration doubleValue], 19.0, @"Call #0 now has another Duration");
    
    call = [loaded objectAtIndex:1];
    XCTAssert([call.callType isEqualToString:@"Outgoing"], @"Call #1 now has another Type");
    XCTAssert([call.phoneNumber isEqualToString:@"tester.ds123@gmail.com"], @"Call #1 now has another Phone Number");
    XCTAssertEqual([call.duration doubleValue], 0.0, @"Call #1 now has another Duration");
}

- (void)testLocalStorageForSms
{
    // Since date
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    [dc setDay:14];
    [dc setMonth:3];
    [dc setYear:2012];
    [dc setHour:0];
    [dc setMinute:0];
    [dc setSecond:0];
    
    NSDate *date = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] dateFromComponents:dc];
    
    NSError *err = nil;
    NSArray *msgs = [MessageData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to get Messages");
    
    BOOL isSaved = [self.localStorage saveMessageData:msgs];
    XCTAssert(isSaved, @"Failed to save MessageData in Local Storage");
    
    NSArray *loaded = [self.localStorage loadMessageDataSinceDate:date];
    XCTAssertNotNil(loaded, @"Failed to load MessageData from Local Storage");
    XCTAssertEqual(80, [loaded count], @"Failed to match count of entries before and after saving");
    
    BOOL isErased = [self.localStorage eraseMessageDataSinceDate:date];
    XCTAssert(isErased, @"Failed to erase CallsData from Local Storage");
    
    loaded = [self.localStorage loadMessageDataSinceDate:nil];
    XCTAssertNotNil(loaded, @"Failed to load MessageData from Local Storage");
    XCTAssertEqual(3011, [loaded count], @"Failed to match count of entries before and after erasing");
    
    isErased = [self.localStorage eraseMessageDataSinceDate:nil];
    XCTAssert(isErased, @"Failed to erase MessageData from Local Storage");
    
    loaded = [self.localStorage loadMessageDataSinceDate:nil];
    XCTAssertNil(loaded, @"Failed to erase all MessageData from Local Storage");
}

- (void)testLocalStorageForMms
{
    // Since date
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    [dc setDay:14];
    [dc setMonth:3];
    [dc setYear:2012];
    [dc setHour:0];
    [dc setMinute:0];
    [dc setSecond:0];
    
    NSDate *date = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] dateFromComponents:dc];
    
    NSError *err = nil;
    NSArray *mmss = [MmsData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(mmss, @"Failed to get Messages");
    
    BOOL isSaved = [self.localStorage saveMmsData:mmss];
    XCTAssert(isSaved, @"Failed to save MmsData in Local Storage");
    
    NSArray *loaded = [self.localStorage loadMmsDataSinceDate:date];
    XCTAssertNotNil(loaded, @"Failed to load MmsData from Local Storage");
    XCTAssertEqual(2, [loaded count], @"Failed to match count of entries before and after saving");
    
    BOOL isErased = [self.localStorage eraseMmsDataSinceDate:date];
    XCTAssert(isErased, @"Failed to erase MmsData from Local Storage");
    
    loaded = [self.localStorage loadMmsDataSinceDate:nil];
    XCTAssertNotNil(loaded, @"Failed to load MmsData from Local Storage");
    XCTAssertEqual(108, [loaded count], @"Failed to match count of entries before and after erasing");
    
    isErased = [self.localStorage eraseMmsDataSinceDate:nil];
    XCTAssert(isErased, @"Failed to erase MmsData from Local Storage");
    
    loaded = [self.localStorage loadMmsDataSinceDate:nil];
    XCTAssertNil(loaded, @"Failed to erase all MmsData from Local Storage");
}

- (void)testLocalStorageForLocation
{
    // Simulate location
    NSDate* date = [NSDate date];
    CLLocationCoordinate2D coord = {1, 2};
    CLLocation *location = [[CLLocation alloc] initWithCoordinate:coord altitude:1.1 horizontalAccuracy:2.2 verticalAccuracy:3.3 course:4.4 speed:5.5 timestamp:date];
    LocationData *locationData = [[LocationData alloc] initWithLocation:location andHeading:nil];
    
    NSArray *locations = [NSArray arrayWithObjects:locationData, locationData, locationData, locationData, locationData, nil];
    XCTAssertNotNil(locations, @"Failed to create Locations");
    
    BOOL isSaved = [self.localStorage saveLocationData:locations];
    XCTAssert(isSaved, @"Failed to save MessageData in Local Storage");
    
    NSArray *loaded = [self.localStorage loadLocationDataSinceDate:date];
    XCTAssertNotNil(loaded, @"Failed to load LocationData from Local Storage");
    XCTAssertEqual([locations count], [loaded count], @"Failed to match count of entries before and after saving");
    
    BOOL isErased = [self.localStorage eraseLocationDataSinceDate:nil];
    XCTAssert(isErased, @"Failed to erase LocationData from Local Storage");
    
    loaded = [self.localStorage loadLocationDataSinceDate:date];
    XCTAssertNil(loaded, @"Failed to erase all LocationData from Local Storage");
}

- (void)testLocalStorageForCrashData
{
    [self.localStorage eraseCrashDataSinceDate: nil];
    
    NSException *ex = [self exceptionForAddingNilToArray];
    XCTAssertNotNil(ex);
    
    CrashData* crashData = [[CrashData alloc] initWithException:ex textLog:nil];
    
    BOOL isSaved = [self.localStorage saveCrashData:[NSArray arrayWithObjects:crashData, crashData, nil]];
    XCTAssert(isSaved, @"Failed to save CrashData in Local Storage");
    
    NSArray *loaded = [self.localStorage loadCrashDataSinceDate:nil];
    XCTAssertNotNil(loaded, @"Failed to load LocationData from Local Storage");
    XCTAssertEqual(2, [loaded count], @"Failed to match count of entries before and after saving");
}

- (void)testLocalStorageSizeReducing
{
    // We have huge count of sms's
    if (!self.localStorage)
        self.localStorage = [[LocalStorage alloc] initWithDbPath:self.localStorageDbPath];
    
    XCTAssertNotNil(self.localStorage, @"Failed to init Local Storage");
    
    NSError *err = nil;
    NSArray *msgs = [MessageData loadFromInternalDb:self.smsDbPath sinceDate:nil withContactsProvider:nil error:&err];
    XCTAssertNotNil(msgs, @"Failed to get Messages");
    
    BOOL isSaved = [self.localStorage saveMessageData:msgs];
    XCTAssert(isSaved, @"Failed to save MessageData in Local Storage");
    
    // Stamp size
    const long dbSizeBeforeClear = [[self.localStorage size] longValue];
    
    NSArray *loaded = [self.localStorage loadMessageDataSinceDate:nil];
    XCTAssertNotNil(loaded, @"Failed to load MessageData from Local Storage");
    XCTAssertEqual([msgs count], [loaded count], @"Failed to match count of entries before and after saving");
    
    // Attempt to reduce database
    BOOL isCleared = [self.localStorage clear];
    XCTAssert(isCleared, @"Failed to reduce database size");
    
    // Validate reduce successful
    const long dbSizeAfterClear = [[self.localStorage size] longValue];
    XCTAssert(dbSizeBeforeClear > dbSizeAfterClear, @"Database size reducing has no effect");
}

- (void)test_MessageData_JsonSerialization_EmptyArray
{
    NSArray<MessageData*>* messages = @[];
    NSArray* jsonMessages = [MessageData jsonObjectWithData: messages];
    XCTAssertEqual(jsonMessages.count, 0);
}

- (void)test_MessageData_JsonSerialization
{
    MessageData* message = [MessageData new];
    message.phoneNumber = @"1234567";
    message.contactName = @"Test Name";
    message.body = @"Test text";
    message.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    message.messageType = @"test";
    
    NSArray<MessageData*>* messages = @[message];
    NSArray* jsonMessages = [MessageData jsonObjectWithData: messages];
    
    XCTAssertEqual(jsonMessages.count, 1);
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"contactName"], @"Test Name");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"body"], @"Test text");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"type"], @"test");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"address"], @"1234567");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"date"], @"/Date(3000)/");
}

- (void)test_MessageData_JsonSerialization_withNilName
{
    MessageData* message = [MessageData new];
    message.phoneNumber = @"1234567";
    message.contactName = nil;
    message.body = @"Test text";
    message.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    message.messageType = @"test";
    
    NSArray<MessageData*>* messages = @[message];
    NSArray* jsonMessages = [MessageData jsonObjectWithData: messages];
    
    XCTAssertEqual(jsonMessages.count, 1);
    XCTAssertNil([jsonMessages[0] valueForKey: @"contactName"]);
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"body"], @"Test text");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"type"], @"test");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"address"], @"1234567");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"date"], @"/Date(3000)/");
}

- (void)test_MmsData_JsonSerialization_EmptyArray
{
    NSArray<MmsData*>* messages = @[];
    NSArray* jsonMessages = [MessageData jsonObjectWithData: messages];
    XCTAssertEqual(jsonMessages.count, 0);
}

- (void)test_MmsData_JsonSerialization
{
    MmsData* message = [MmsData new];
    message.phoneNumber = @"1234567";
    message.contactName = @"Test Name";
    message.body = @"Test text";
    message.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    message.messageType = @"test";
    message.messageSubject = @"Test subject";
    message.attachments = @[];
    
    NSArray<MmsData*>* messages = @[message];
    NSArray* jsonMessages = [MmsData jsonObjectWithData: messages];
    
    XCTAssertEqual(jsonMessages.count, 1);
    
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"contactName"], @"Test Name");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"body"], @"Test text");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"type"], @"test");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"address"], @"1234567");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"date"], @"/Date(3000)/");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"sub"], @"Test subject");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"attachments"], @[]);
}

- (void)test_MmsData_JsonSerialization_withNilName
{
    MmsData* message = [MmsData new];
    message.phoneNumber = @"1234567";
    message.contactName = nil;
    message.body = @"Test text";
    message.timestamp = [NSDate dateWithTimeIntervalSince1970: 3];
    message.messageType = @"test";
    message.messageSubject = @"Test subject";
    message.attachments = @[];
    
    NSArray<MmsData*>* messages = @[message];
    NSArray* jsonMessages = [MmsData jsonObjectWithData: messages];
    
    XCTAssertEqual(jsonMessages.count, 1);
    
    XCTAssertNil([jsonMessages[0] valueForKey: @"contactName"]);
    
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"body"], @"Test text");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"type"], @"test");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"address"], @"1234567");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"date"], @"/Date(3000)/");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"sub"], @"Test subject");
    XCTAssertEqualObjects([jsonMessages[0] valueForKey: @"attachments"], @[]);
}

#pragma mark - private

- (NSException*) exceptionForAddingNilToArray
{
    @try
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        NSObject *nilObject = nil;
        [array addObject:nilObject];
    }
    @catch (NSException *exception)
    {
        NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithDictionary:[exception userInfo]];
        info[@(kUEH_StacktraceKey)] = [Utils getStack];
        
        return [NSException exceptionWithName:[exception name] reason:[exception reason] userInfo:info];
    }

    return nil;
}

@end
