#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "OCMock.h"

#import "ContactData.h"
#import "ContactsCache.h"

//====== DEV NOTES ======
//General comment for unit-tests with OCMock:
//  If you add stub for any method (e.g. OCMStub([mockObj someMethod]).andReturn(YES)),
//  DONT FORGET to add verification, that this method is called (e.g. OCMVerify([mockObj someMethod]))

@interface ContactsCache (CategoryForTesting)

//we must redeclare private methods here to make possible adding stubs for them
//otherwise they are invisible for compiler
//don't worry, if we create a stub for not existing method, OCMock will show a runtime error - it won't be skipped

- (NSString *)getPhoneCodeForCurrentCountry;

@end

@interface ContactsCacheTest : XCTestCase

@property (strong, nonatomic) NSString *contactsDbPath;

@end

@implementation ContactsCacheTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    NSBundle *currentBundle = [NSBundle bundleForClass:[self class]];
    _contactsDbPath = [currentBundle pathForResource:@"AddressBook" ofType:@"db"];
}

- (void)testContactsCache_Basic
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    ContactData *contact = nil;
    
    contact = [cache searchForNumber:@"8013196347"];
    XCTAssertNotNil(contact, @"Failed to find contact in cache");
    XCTAssert([[contact person] isEqualToString:@"Kipper, Amber"], @"Failed to find contact");
    XCTAssert([[contact typeForNumber:@"8013196347"] isEqualToString:@"Mobile"], @"Failed to get contact type");
    
    contact = [cache searchForNumber:@"7035952979"];
    XCTAssertNotNil(contact, @"Failed to find contact in cache");
    XCTAssert([[contact person] isEqualToString:@"Kipper, Amber"], @"Failed to find contact");
    XCTAssert([[contact typeForNumber:@"7035952979"] isEqualToString:@"Work"], @"Failed to get contact type");
    
    contact = [cache searchForNumber:@"8013693540"];
    XCTAssertNotNil(contact, @"Failed to find contact in cache");
    XCTAssert([[contact person] isEqualToString:@"Murdock, Munro"], @"Failed to find contact");
    XCTAssert([[contact typeForNumber:@"8013693540"] isEqualToString:@"Mobile"], @"Failed to get contact type");
}

- (void)testContactsCache_CountryCodeVariations
{
    //this is a special test for such situations:
    // - if there is a saved contact without country code, the device will automatically add country code during the call (I've checked it only for outgoing calls)
    // - the country code can change at runtime - and the device will add new prefix for such contacts
    
    //init objects
    ContactData *foundContact = nil;
    ContactsCache *cache = [[ContactsCache alloc] init];
    NSArray *contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:nil];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    
    id mockCache = OCMPartialMock(cache);
    OCMStub([mockCache getPhoneCodeForCurrentCountry]).andReturn(@"+1"); //USA or Canada
    
    //perform actions
    [cache updateWithAddressBook:contacts rebuild:YES];

    //=======================
    //check results for contact which is STORED in Accounts.db WITHOUT country code
    foundContact = [cache searchForNumber:@"7035071053"]; //search by original value (without country code)
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Bocra, Nicole"]);
    XCTAssert([[foundContact typeForNumber:@"7035071053"] isEqualToString:@"Mobile"]);
    
    foundContact = [cache searchForNumber:@"+17035071053"]; //search with country code
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Bocra, Nicole"]);
    XCTAssert([[foundContact typeForNumber:@"+17035071053"] isEqualToString:@"Mobile"]);
    
    foundContact = [cache searchForNumber:@"+57035071053"]; //search with another country code - must not be found
    XCTAssertNil(foundContact, @"The number must not be found with another country code");

    //=======================
    //check results for contact which is STORED in Accounts.db with PART of country code
    foundContact = [cache searchForNumber:@"14357570382"]; //search by original value (with PART of country code)
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Braun, Don"]);
    XCTAssert([[foundContact typeForNumber:@"14357570382"] isEqualToString:@"Mobile"]);
    
    foundContact = [cache searchForNumber:@"+14357570382"]; //search with FULL country code
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Braun, Don"]);
    XCTAssert([[foundContact typeForNumber:@"+14357570382"] isEqualToString:@"Mobile"]);
    
    //=======================
    //check results for contact which is STORED in Accounts.db with FULL country code
    foundContact = [cache searchForNumber:@"+18014733026"]; //search by original value (WITH country code)
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Briggs, James"]);
    XCTAssert([[foundContact typeForNumber:@"+18014733026"] isEqualToString:@"Mobile"]);
    
    foundContact = [cache searchForNumber:@"8014733026"]; //search without country code
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Briggs, James"]);
    XCTAssert([[foundContact typeForNumber:@"8014733026"] isEqualToString:@"Mobile"]);

    foundContact = [cache searchForNumber:@"18014733026"]; //search by almost full country code (country code without +)
    XCTAssertNotNil(foundContact, @"Failed to find contact in cache");
    XCTAssert([[foundContact person] isEqualToString:@"Briggs, James"]);
    XCTAssert([[foundContact typeForNumber:@"18014733026"] isEqualToString:@"Mobile"]);

    
    OCMVerify([mockCache getPhoneCodeForCurrentCountry]);
}

- (void)testContactsCacheUpdatability
{
    ContactsCache *cache = [[ContactsCache alloc] init];
    
    NSDateComponents *dc = nil;
    NSCalendar *c = nil;
    NSDate *date = nil;
    NSArray *contacts = nil;
    ContactData *cd = nil;
    
    dc = [[NSDateComponents alloc] init];
    [dc setDay:26];
    [dc setMonth:12];
    [dc setYear:2013];
    [dc setHour:19];
    [dc setMinute:1];
    [dc setSecond:23];
    c = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    date = [c dateFromComponents:dc];
    
    contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:date];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    XCTAssertEqual(9, [contacts count], @"Wrong count of DB entries. Did you change DB?");
    
    [cache updateWithAddressBook:contacts rebuild:YES];
    
    // This numbers should be absent
    // 8017639851
    // 8016169831
    cd = [cache searchForNumber:@"8017639851"];
    XCTAssertNil(cd, @"This entry should be absent");
    cd = [cache searchForNumber:@"8016169831"];
    XCTAssertNil(cd, @"This entry should be absent");
    
    // Update
    dc = [[NSDateComponents alloc] init];
    [dc setDay:26];
    [dc setMonth:12];
    [dc setYear:2013];
    [dc setHour:19];
    [dc setMinute:1];
    [dc setSecond:22]; // Second earlier
    c = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    date = [c dateFromComponents:dc];
    
    contacts = [ContactData loadFromInternalDb:self.contactsDbPath forNumber:nil sinceDate:date];
    XCTAssertNotNil(contacts, @"Failed to load contacts");
    XCTAssertEqual(37, [contacts count], @"Wrong count of DB entries. Did you change DB?");
    
    [cache updateWithAddressBook:contacts rebuild:NO];
    
    // This numbers should be:
    // 8017639851 absent
    // 8016169831 present
    cd = [cache searchForNumber:@"8017639851"];
    XCTAssertNil(cd, @"This entry should be absent");
    cd = [cache searchForNumber:@"8016169831"];
    XCTAssertNotNil(cd, @"This entry should be present");
}

@end
