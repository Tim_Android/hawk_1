#import "Constants.h"
#import "MmsData.h"
#import "MmsAttachmentInfoData.h"
#import "ContactData.h"
#import "FMDB.h"
#import "FmdbHelper.h"
#import "Utils.h"
#import "GlobalCfg.h"

// See MessageData.m for details

#define kMms_PhoneNumber    "address"
#define kMms_ContactName    "contactName"
#define kMms_LocalTime      "date"
#define kMms_MsgBody        "body"
#define kMms_MessageType    "type"

#define kMms_Subject        "sub"
#define kMms_Attachments    "attachments"

// Local storage table
#define kMms_TableName      "mms"

@interface MmsData ()

@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation MmsData

#pragma mark -
#pragma mark Initialization from internal DB

+ (NSString *)resolveMessageState:(NSDictionary *)entry
{
    NSString *msgState = nil;
    
    // !!!: Currently unused, but may be helpful in future
    //BOOL isDelivered = [[entry objectForKey:@"is_delivered"] boolValue];
    //BOOL isPrepared = [[entry objectForKey:@"is_prepared"] boolValue];
    //BOOL isRead = [[entry objectForKey:@"is_read"] boolValue];
    
    BOOL isFromMe = [[entry objectForKey:@"is_from_me"] boolValue];
    BOOL isSent = [[entry objectForKey:@"is_sent"] boolValue];
    
    if (isFromMe)
        msgState = @"Outbox";
    else
        msgState = @"Inbox";
    
    if (isFromMe && !isSent)
        msgState = @"Failed";
    
    assert(msgState);
    return msgState;
}

- (id)initWithInternalDbEntry:(NSDictionary *)entry andAttachments:(NSArray *)attachments withContactsProvider:(id<IContactsProvider>)contactsProvider
{
    self = [super init];
    
    if (self)
    {
        _phoneNumber = [entry objectForKey:@"phone_numbers"];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@"date"] integerValue]];
        _body = [entry objectForKey:@"text"];
        _messageType = [MmsData resolveMessageState:entry];
        _messageSubject = [entry objectForKey:@"subject"];
        _messageId = [entry objectForKey:@"ROWID"];
        
        ContactData* contact = [contactsProvider searchForNumber: _phoneNumber];
        _contactName = contact.person;
        
        if ([_body isKindOfClass:[NSString class]])
        {
            // Usually MMS has one or few entries of addtional symbol sequence at the end - "\xef\xbf\xbc"
            // We should remove it
            _body = [_body stringByReplacingOccurrencesOfString:@"\xef\xbf\xbc" withString:@""];
            
            if (0 == [_body length])
                _body = @(kNotAvailable);
        }
        else
            _body = @(kNotAvailable);
        
        if (!_messageSubject || [_messageSubject isEqual:[NSNull null]])
            _messageSubject = @(kNotAvailable);
        
        _attachments = attachments;
        
#ifdef DEBUG
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        [df setDateStyle:NSDateFormatterShortStyle];
        NSLog(@"New MMS: %@, %@, %lu, %@", _phoneNumber, _messageType, (long)[_body length], [df stringFromDate:_timestamp]);
#endif // DEBUG
        
    }
    
    return self;
}

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error
{
    NSString *query = nil;
    NSTimeInterval timeIntervalSince1970 = (date ? [date timeIntervalSince1970] : 0);
    query = [NSString stringWithFormat:
         @"SELECT m.rowid, m.date + %.0f as date, group_concat(h.id, ', ') AS phone_numbers, m.service, m.is_from_me, m.text, m.subject, m.is_sent "
         "FROM message m "
         "LEFT JOIN chat_message_join ON m.rowid = chat_message_join.message_id "
         "LEFT JOIN chat_handle_join ON chat_message_join.chat_id = chat_handle_join.chat_id "
         "LEFT JOIN handle h ON h.rowid = chat_handle_join.handle_id "
         "WHERE (m.date + %.0f) >= %.0f AND m.cache_has_attachments = 1 "
         "GROUP BY m.rowid "
         "ORDER BY m.rowid ASC", NSTimeIntervalSince1970, NSTimeIntervalSince1970, timeIntervalSince1970];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        NSNumber *messageId = [rslt objectForKey:@"ROWID"];
        NSArray *attData = [MmsAttachmentInfoData loadAttachmentsFromDb:dbPath forMessageId:messageId];
        MmsData *mmsData = [[MmsData alloc] initWithInternalDbEntry:rslt andAttachments:attData withContactsProvider:contactsProvider];
        [entries addObject:mmsData];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (MmsData *entry in data)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:entry.phoneNumber forKey:@(kMms_PhoneNumber)];
        [dict setObject:entry.body forKey:@(kMms_MsgBody)];
        [dict setObject:entry.messageType forKey:@(kMms_MessageType)];
        [dict setObject:[NSString stringWithFormat:@"/Date(%.0f)/", [entry.timestamp timeIntervalSince1970]*1000] forKey:@(kMms_LocalTime)]; // in milliseconds
        [dict setObject:entry.messageSubject forKey:@(kMms_Subject)];
        
        if (entry.contactName)
        {
            [dict setObject:entry.contactName forKey:@(kMms_ContactName)];
        }
        
        NSArray *attachments = [MmsAttachmentInfoData jsonObjectWithData:entry.attachments];
        [dict setObject:attachments forKey:@(kMms_Attachments)];
        
        [jsonObject addObject:dict];
    }
    
    return jsonObject;
}

#pragma mark -
#pragma mark Local Storage

- (id)initWithLocalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _phoneNumber = [entry objectForKey:@(kMms_PhoneNumber)];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@(kMms_LocalTime)] doubleValue]];
        _body = [Utils decodeFromBase64:[entry objectForKey:@(kMms_MsgBody)]];
        _messageType = [entry objectForKey:@(kMms_MessageType)];
        _messageSubject = [entry objectForKey:@(kMms_Subject)];
        _attachments = [MmsAttachmentInfoData attachmentsArrayFromString:[entry objectForKey:@(kMms_Attachments)]];
        _localStorageId = [entry objectForKey:@"rowid"];
        
        if (![entry[@(kMms_ContactName)] isEqualToString: @"(null)"])
        {
            _contactName = entry[@(kMms_ContactName)];
        }
    }
    
    return self;
}

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSString *query = (date)
        ? [[NSString alloc] initWithFormat:@"select rowid, * from %@ where date >= %.0f order by rowid asc;", @(kMms_TableName), [date timeIntervalSince1970]]
        : [[NSString alloc] initWithFormat:@"select rowid, * from %@ order by rowid asc;", @(kMms_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        MmsData *data = [[MmsData alloc] initWithLocalDbEntry:rslt];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (MmsData* entry in data)
    {
        [statements appendFormat:
         @"insert into %@ values ('%@', %.0f, '%@', '%@', '%@', '%@', '%@');",
         @(kMms_TableName),
         entry.phoneNumber,
         [entry.timestamp timeIntervalSince1970],
         [Utils encodeToBase64:entry.body],
         entry.messageType,
         entry.messageSubject,
         [MmsAttachmentInfoData stringFromAttachmentsArray:entry.attachments],
         entry.contactName];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    
    if (dropIfExists)
    {
        NSString *query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kMms_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    isOk &= [self createTableInDb:db error:error];
    return isOk;
}

+ (BOOL)updateTableInDb:(NSString *)dbPath fromVersion0To1Error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    if ([FmdbHelper doesTableExist:@(kMms_TableName) inDb:db])
    {
        NSString *query = [[NSString alloc] initWithFormat:
                           @"alter table %@ add column %@ text",
                           @(kMms_TableName),
                           @(kMms_ContactName)];
        
        return [FmdbHelper executeUpdate:query db:db error:error];
    }
    else
    {
        return [self createTableInDb:db error:error];
    }
}

+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSTimeInterval timestamp = (date) ? [date timeIntervalSince1970] : 0;
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where date >= %.0f", @(kMms_TableName), timestamp];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kMms_TableName) fromDb:db error:error];
}

+ (BOOL)createTableInDb:(FMDatabase*)db error:(NSError **)error
{
    NSString* query = [[NSString alloc] initWithFormat:
                       @"create table if not exists %@ (%@ text, %@ integer, %@ text, %@ text, %@ text, %@ text, %@ text)",
                       @(kMms_TableName),
                       @(kMms_PhoneNumber),
                       @(kMms_LocalTime),
                       @(kMms_MsgBody),
                       @(kMms_MessageType),
                       @(kMms_Subject),
                       @(kMms_Attachments),
                       @(kMms_ContactName)];
    
    return [FmdbHelper executeUpdate:query db:db error:error];
}

@end
