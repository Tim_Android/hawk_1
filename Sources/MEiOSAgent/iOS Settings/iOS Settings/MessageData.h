#import <Foundation/Foundation.h>

#import "IPersistableData.h"
#import "IConvertableData.h"
#import "IContactsProvider.h"

@interface MessageData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *contactName;
@property (strong, nonatomic) NSDate *timestamp;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *messageType;

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error;

@end
