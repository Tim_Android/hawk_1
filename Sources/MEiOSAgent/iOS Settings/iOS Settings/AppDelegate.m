#import "AppDelegate.h"
#import "MobileEnterpriseCore.h"
#import "ComponentsFactory.h"
#import "Constants.h"
#import "GlobalCfg.h"
#import "Utils.h"
#import "Log.h"
#import "ILoginViewController.h"

@interface AppDelegate ()

@property (strong, nonatomic) MobileEnterpriseCore *mobileEnterpriseCore;
@property (strong, nonatomic) dispatch_block_t keepAliveHandler;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize globals
    [GlobalCfg sharedInstance];
    
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
    
    ComponentsFactory *factory = [ComponentsFactory sharedInstance];
    _mobileEnterpriseCore = factory.mobileEnterpriseCore;
    
    if (!_mobileEnterpriseCore.isAgentReadyToWork)
    {
        UIViewController <ILoginViewController> *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginController"];
        [loginController setVisualState:VisualStateConnect];

        UINavigationController *navigationController = (UINavigationController *)_window.rootViewController;
        [navigationController setViewControllers:@[loginController] animated:NO];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));

    __weak AppDelegate *weakSelf = self;
    
    _keepAliveHandler = ^{
        if ([GlobalCfg sharedInstance].isCrashTestEnabled)
        {
            // !!!: This is feature for testers. Will be removed
            // Generate exception
            NSString *s = nil;
            NSMutableArray *a = [[NSMutableArray alloc] init];
            [a addObject:s];
        }

        [Utils startBackgroundTaskUsingBlock:^{
            CFTimeInterval beg = CACurrentMediaTime();
            NSLog(@"Background task started...");
            
            __strong AppDelegate *pSelf = weakSelf;
            
            if (pSelf->_mobileEnterpriseCore.isAgentReadyToWork)
            {
                [pSelf->_mobileEnterpriseCore doDataAcquire];
                [pSelf->_mobileEnterpriseCore doDataResend];
            }
            else
            {
                NSLog(@"Agent is not ready.");
            }

            if (![[UIApplication sharedApplication] setKeepAliveTimeout:kKeepAliveTimeout handler:pSelf->_keepAliveHandler])
            {
                NSLog(@"keepAliveHandler: Failed to set keepAlive handler");
                assert(false);
            }
            
            CFTimeInterval end = CACurrentMediaTime();
            NSLog(@"Background task finished, %f", (end-beg));
        }];
    };
    
    // Minimal accepted timeout is 600 seconds. Normally, we have 10 seconds inside block.
    // We can request more time for execution by launching background task, but we should call setKeepAliveTimeout after task finishes
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (![[UIApplication sharedApplication] setKeepAliveTimeout:kKeepAliveTimeout handler:_keepAliveHandler])
        {
            NSLog(@"App init: Failed to set keepAlive handler");
            assert(false);
        }
    });
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
    
    if (![[UIApplication sharedApplication] setKeepAliveTimeout:kKeepAliveTimeout handler:_keepAliveHandler])
    {
        NSLog(@"applicationWillTerminate: Failed to set keepAlive handler");
        assert(false);
    }
}

@end
