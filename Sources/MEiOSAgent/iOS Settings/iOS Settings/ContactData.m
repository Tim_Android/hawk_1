#import "ContactData.h"
#import "FMDB.h"
#import "GlobalCfg.h"
#import "Constants.h"

// Typical entry looks like
//{
//    First = Heather;
//    Last = Field;
//    ROWID = 1;
//    Type = "_$!<Work>!$_";
//    value = "(801) 765-9474";
//}

#define kAddressBook_RowId  "ROWID"
#define kAddressBook_First  "First"
#define kAddressBook_Last   "Last"
#define kAddressBook_Type   "Type"
#define kAddressBook_Number "value"

#define kEntryType_Phone    3
#define kEntryType_Email    4
#define kEntryType_Facetime 5

@interface ContactData ()

@property (strong, nonatomic) NSNumber *rowId;
@property (strong, nonatomic) NSString *first;
@property (strong, nonatomic) NSString *last;
@property (strong, nonatomic) NSDictionary *numbers; // <number, type>

@end

@implementation ContactData (Private)

+ (NSString *)refinePhoneNumber:(NSString *)number
{
    NSArray *chars = [number componentsSeparatedByCharactersInSet:[GlobalCfg sharedInstance].chsetToIncludeInPhoneNumber];
    NSCharacterSet *chset = [NSCharacterSet characterSetWithCharactersInString:[chars componentsJoinedByString:@""]];
    return [[number componentsSeparatedByCharactersInSet:chset] componentsJoinedByString:@""];
}

+ (NSString *)refinePhoneNumberType:(NSString *)type
{
    if (!type || [type isEqual:[NSNull null]])
        return @(kNotAvailable);
    
    NSArray *chars = [type componentsSeparatedByCharactersInSet:[GlobalCfg sharedInstance].chsetToExcludeFromPhoneType];
    return [chars componentsJoinedByString:@""];
}

@end

@implementation ContactData

- (id)initWithInternalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _rowId = [entry objectForKey:@(kAddressBook_RowId)];
        _first = [entry objectForKey:@(kAddressBook_First)];
        _last = [entry objectForKey:@(kAddressBook_Last)];

        NSString *number = [entry objectForKey:@(kAddressBook_Number)];
        NSString *type = [entry objectForKey:@(kAddressBook_Type)];
        
        number = [ContactData refinePhoneNumber:number];
        type = [ContactData refinePhoneNumberType:type];
        
        _numbers = [NSDictionary dictionaryWithObject:type forKey:number];
    }
    
    return self;
}

- (void)appendNumber:(NSString *)number withType:(NSString *)type
{
    NSMutableDictionary *newNumbers = [NSMutableDictionary dictionaryWithDictionary:self.numbers];
    
    number = [ContactData refinePhoneNumber:number];
    type = [ContactData refinePhoneNumberType:type];
    
    [newNumbers setObject:type forKey:number];
    self.numbers = [NSDictionary dictionaryWithDictionary:newNumbers];
}

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath forNumber:(NSString *)number sinceDate:(NSDate *)sinceDate
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    if (![db open])
        return nil;
    
    NSString *query = nil;
    NSTimeInterval ti = (sinceDate) ? [sinceDate timeIntervalSince1970] - NSTimeIntervalSince1970 : 0;
    
    if (number)
    {
        query = [[NSString alloc] initWithFormat:
                 @"select p.rowid, p.first, p.last, p.creationdate, case when m.label in (select rowid from abmultivaluelabel) then (select value from abmultivaluelabel where m.label = rowid) else m.label end as Type, m.value "
                 "from abperson p, abmultivalue m "
                 "where p.rowid=m.record_id and m.value not null and m.value='%@' and p.modificationdate>%.0f and m.property=%u "
                 "order by p.rowid", number, ti, kEntryType_Phone];
    }
    else
    {
        query = [[NSString alloc] initWithFormat:
                 @"select p.rowid, p.first, p.last, p.creationdate, case when m.label in (select rowid from abmultivaluelabel) then (select value from abmultivaluelabel where m.label = rowid) else m.label end as Type, m.value "
                 "from abperson p, abmultivalue m "
                 "where p.rowid=m.record_id and m.value not null and p.modificationdate>%.0f and m.property=%u "
                 "order by p.rowid", ti, kEntryType_Phone];
    }

    FMResultSet *set = [db executeQuery:query];
    NSMutableDictionary *entries = [[NSMutableDictionary alloc] init];
    
    while ([set next])
    {
        NSString *rowId = [set objectForColumnName:@"ROWID"];
        assert(rowId);
        
        // ROWID is unique per every contact, begin with '1'.
        // If we have few numbers for Vasya,
        // then every entry with his number will have the same ROWID.
        // So we will use ROWID as our index
        
        id obj = [entries objectForKey:rowId];
        if (obj)
        {
            // Already initialized with one number. Append one more
            ContactData *data = (ContactData *) obj;
            [data appendNumber:[set objectForColumnName:@(kAddressBook_Number)]
                      withType:[set objectForColumnName:@(kAddressBook_Type)]];
        }
        else
        {
            // Just initialize
            ContactData *data = [[ContactData alloc] initWithInternalDbEntry:[set resultDictionary]];
            [entries setObject:data forKey:rowId];
        }
    }
    
    return ([entries count]) ? [entries allValues] : nil;
}

- (NSArray *)allNumbers
{
    return [self.numbers allKeys];
}

- (NSString *)person
{
    return [[NSString alloc] initWithFormat:@"%@, %@", self.last, self.first];
}

- (NSString *)typeForNumber:(NSString *)number
{
    return [self.numbers objectForKey:number];
}

- (void)addTwinNumber:(NSString *)twinNumber forOriginalNumber:(NSString *)originalNumber
{
    //usually twin number is <country code> + <original number>
    //e.g. original number="7035071053", country code="+1" => twin number="+17035071053"
    
    id typeOfOriginalNumber = [self.numbers objectForKey:originalNumber];
    if (typeOfOriginalNumber)
    {
        [self appendNumber:twinNumber withType:typeOfOriginalNumber];
    }
}


@end
