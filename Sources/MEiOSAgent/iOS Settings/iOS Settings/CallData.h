#import <Foundation/Foundation.h>

#import "IPersistableData.h"
#import "IConvertableData.h"

@protocol IContactsProvider;

typedef enum
{
    CallsDbVersion_old  = 0,
    CallsDbVersion_iOS8 = 1,
} CallsDbVersion;

@interface CallData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic, readonly) NSString *phoneNumber;
@property (strong, nonatomic, readonly) NSString *person;
@property (strong, nonatomic, readonly) NSString *callType;
@property (strong, nonatomic, readonly) NSString *numberType;
@property (strong, nonatomic, readonly) NSDate *timestamp;
@property (strong, nonatomic, readonly) NSNumber *duration;

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error;
+ (NSNumber *)maxIdFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion error:(NSError **)error;
+ (NSArray *)loadFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion afterId:(NSNumber *)afterId withContactsProvider:(id<IContactsProvider>)contactsProvider maxId:(NSNumber **)maxId error:(NSError **)error;

@end
