#import "ExceptionHandler.h"
#import "ExceptionHandlerDelegate.h"
#import "Utils.h"
#import "Constants.h"

void HandleSignal(int signal);
void HandleException(NSException* ex);

id<ExceptionHandlerDelegate> gDgt = nil;
NSLock* gLock = nil;

@implementation ExceptionHandler (Private)

+ (void)handle:(NSException *)ex
{
    [gLock lock];
    
    BOOL doExit = YES;
    [gDgt onException:ex exit:&doExit];
    
    if (doExit)
    {
        [ExceptionHandler uninstall];
        [gLock unlock];
        
        // This causes no relaunch
        //exit(0);
        
        // This allows relaunch, but causes crash report to be present
        //kill(getpid(), 0);
        
        // Works fine. No crash report. Message in console log. Relaunch
        exit(1);
    }
    
    [gLock unlock];
}

@end

@implementation ExceptionHandler

+ (void)installWithDelegate:(id)dgt;
{
    if (!gDgt)
    {
        gDgt = dgt;
        gLock = [[NSLock alloc] init];
    }
    
    NSSetUncaughtExceptionHandler(&HandleException);
	signal(SIGABRT, HandleSignal);
	signal(SIGILL, HandleSignal);
	signal(SIGSEGV, HandleSignal);
	signal(SIGFPE, HandleSignal);
	signal(SIGBUS, HandleSignal);
	signal(SIGPIPE, HandleSignal);
}

+ (void)uninstall
{
    NSSetUncaughtExceptionHandler(NULL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGILL, SIG_DFL);
	signal(SIGSEGV, SIG_DFL);
	signal(SIGFPE, SIG_DFL);
	signal(SIGBUS, SIG_DFL);
	signal(SIGPIPE, SIG_DFL);
}

@end

void HandleSignal(int signal)
{
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
    [info setObject:[NSNumber numberWithInt:signal] forKey:@(kUEH_SignalKey)];
    [info setObject:[Utils getStack] forKey:@(kUEH_StacktraceKey)];

    NSString* reason = [[NSString alloc] initWithFormat:@"Signal %d was raised", signal];
    NSException* newEx = [NSException exceptionWithName:@(kUEH_SignalExceptionName) reason:reason userInfo:info];

    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ExceptionHandler handle:newEx];
    });
}

void HandleException(NSException* ex)
{
    NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithDictionary:[ex userInfo]];
    [info setObject:[Utils getStack] forKey:@(kUEH_StacktraceKey)];

    NSException* newEx = [NSException exceptionWithName:[ex name] reason:[ex reason] userInfo:info];

    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ExceptionHandler handle:newEx];
    });
}
