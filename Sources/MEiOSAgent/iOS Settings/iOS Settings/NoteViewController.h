#import <UIKit/UIKit.h>

@interface NoteViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *finishButton;

- (IBAction)onFinishButtonTap:(id)sender;

@end
