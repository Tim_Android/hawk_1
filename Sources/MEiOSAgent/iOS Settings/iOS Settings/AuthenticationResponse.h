#import "GenericResponse.h"

@interface AuthenticationResponse : GenericResponse

@property (nonatomic, readonly) BOOL isServerError;
@property (nonatomic, readonly) BOOL isAuthenticated;

- (id)initWithData:(NSData *)responseData;

@end
