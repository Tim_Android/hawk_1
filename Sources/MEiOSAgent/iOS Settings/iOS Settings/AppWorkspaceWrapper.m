#import "AppWorkspaceWrapper.h"
#import "LSApplicationWorkspace.h"

#define KEY_APPLICATIONTYPE     @"ApplicationType"
#define APPLICATION_TYPE_SYSTEM @"System"
#define APPLICATION_TYPE_USER   @"User"

@implementation AppWorkspaceWrapper (Private)

+ (NSDictionary *)appDictionaryForBundleId:(NSString *)bundleId usingWorkspace:(LSApplicationWorkspace *)appWorkspace
{
    NSDictionary *installedApplications = [appWorkspace installedApplications];
    NSDictionary *appDictionary = [installedApplications objectForKey:bundleId];
    return appDictionary;
}

+ (BOOL)updateDictionaryForBundleId:(NSString *)bundleId setObject:(id)object forKey:(NSString *)key
{
    LSApplicationWorkspace *appWorkspace = [LSApplicationWorkspace defaultWorkspace];

    NSMutableDictionary *appDictionary = [[AppWorkspaceWrapper appDictionaryForBundleId:bundleId usingWorkspace:appWorkspace] mutableCopy];
    if (appDictionary == nil)
    {
        return NO;
    }
    [appDictionary setObject:object forKey:key];
    return [appWorkspace registerApplicationDictionary:appDictionary];
}

@end


@implementation AppWorkspaceWrapper

+ (BOOL)setUninstallationForBundleId:(NSString *)bundleId enabled:(BOOL)enabled
{
    return [AppWorkspaceWrapper updateDictionaryForBundleId:bundleId setObject:(enabled ? APPLICATION_TYPE_USER : APPLICATION_TYPE_SYSTEM) forKey:KEY_APPLICATIONTYPE];
}

+ (BOOL)isUninstallationEnabledForBundleId:(NSString *)bundleId
{
    LSApplicationWorkspace *appWorkspace = [LSApplicationWorkspace defaultWorkspace];

    NSDictionary *appDictionary = [AppWorkspaceWrapper appDictionaryForBundleId:bundleId usingWorkspace:appWorkspace];
    if (appDictionary == nil)
    {
        return YES;
    }
    NSString *appType = [appDictionary objectForKey:KEY_APPLICATIONTYPE];
    if (appType && [appType isEqualToString:APPLICATION_TYPE_USER])
    {
        return YES;
    }
    return NO;
}

@end
