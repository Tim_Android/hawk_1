#import <Foundation/Foundation.h>

#import "IPersistableData.h"
#import "IConvertableData.h"

@class CLLocation;
@class CLHeading;

@interface LocationData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic, readonly) NSNumber *latitude;
@property (strong, nonatomic, readonly) NSNumber *longitude;
@property (strong, nonatomic, readonly) NSNumber *altitude;
@property (strong, nonatomic, readonly) NSNumber *accuracy;
@property (strong, nonatomic, readonly) NSNumber *course;
@property (strong, nonatomic, readonly) NSNumber *speed;
@property (strong, nonatomic, readonly) NSDate *timestamp;
@property (strong, nonatomic, readonly) NSString *provider;

- (id)initWithLocation:(CLLocation *)location andHeading:(CLHeading *)heading;

@end
