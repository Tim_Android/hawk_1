#import <UIKit/UIKit.h>
#import "ILoginChecker.h"
#import "ILoginViewController.h"

@interface LoginViewController : UIViewController <ILoginViewController, UIAlertViewDelegate, UITextFieldDelegate>

//ILoginViewController protocol
@property (weak, nonatomic) id <ILoginChecker> loginCheckerDelegate;
@property (nonatomic) LoginControllerVisualState visualState;

//own controls
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *authenticateButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *waitIndicator;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

- (IBAction)onLoginFieldReturn:(UITextField *)sender;
- (IBAction)onPasswordFieldReturn:(UITextField *)sender;
- (IBAction)onLogInTap:(id)sender;
- (IBAction)onLoginOrPasswordChanged:(id)sender;

@end

