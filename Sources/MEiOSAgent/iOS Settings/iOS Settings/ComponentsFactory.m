#import "ComponentsFactory.h"
#import "MobileEnterpriseCore.h"

@implementation ComponentsFactory

+ (ComponentsFactory *)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _mobileEnterpriseCore = [[MobileEnterpriseCore alloc] init];
        _loginCheckerDelegate = _mobileEnterpriseCore;
        _uninstallationSwitcherDelegate = _mobileEnterpriseCore;
        _dataSenderDelegate = _mobileEnterpriseCore;
    }
    return self;
}

@end
