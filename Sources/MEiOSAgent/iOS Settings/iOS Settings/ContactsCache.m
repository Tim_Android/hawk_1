#import "ContactsCache.h"
#import "ContactData.h"
#import "Constants.h"

@interface ContactsCache ()

@property (strong, nonatomic) NSDictionary *addressBookCache;
@property (strong, nonatomic) NSLock *lock;
@property (strong, nonatomic) NSDictionary *phoneCountryCodes;
@property (strong, nonatomic) NSString *currentPhoneCountryCode;

@end

@implementation ContactsCache

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _lock = [[NSLock alloc] init];
        _phoneCountryCodes = [ContactsCache loadPhoneCountryCodes];
    }
    
    return self;
}

- (void)updateWithAddressBook:(NSArray *)addressBook rebuild:(BOOL)rebuild
{
    [self.lock lock];
    
    NSMutableDictionary *dict = nil;
    
    if (rebuild)
        dict = [[NSMutableDictionary alloc] init];
    else
        dict = (NSMutableDictionary*) self.addressBookCache;
    
    for (ContactData *entry in addressBook)
    {
        for (NSString *number in [entry allNumbers])
        {
            if (!rebuild)
            {
                // In this case we shouldn't replace existing entries
                id obj = [dict objectForKey:number];
                if (obj)
                    continue;
            }
            
            [dict setObject:entry forKey:number];
        }
    }
    
    self.addressBookCache = dict;
    
    //country code must be updated, because location can change during agent work
    self.currentPhoneCountryCode = [self getPhoneCodeForCurrentCountry];
    [self.lock unlock];
}

- (ContactData *)searchForNumber:(NSString *)phoneNumber
{
    [self.lock lock];
    ContactData *foundContact = [self.addressBookCache objectForKey:phoneNumber];
    
    if (foundContact == nil && phoneNumber != nil)
    {
        //if you change this logic - don't forget to check and update unit-tests
        if (self.currentPhoneCountryCode != nil && phoneNumber.length > self.currentPhoneCountryCode.length)
        {
            NSString *phoneNumberWhichWasFoundInCache = nil;
            
            if ([phoneNumber hasPrefix:self.currentPhoneCountryCode])
            {
                NSUInteger codeLength = self.currentPhoneCountryCode.length;
                for (NSUInteger i = 0; (foundContact == nil && i < codeLength); ++i)
                {
                    NSString *numberWithoutPartOfCountryCode = [phoneNumber substringFromIndex:(codeLength - i)];
                    foundContact = [self.addressBookCache objectForKey:numberWithoutPartOfCountryCode];
                    if (foundContact != nil)
                    {
                        phoneNumberWhichWasFoundInCache = numberWithoutPartOfCountryCode;
                    }
                }
            }
            else
            {
                //try adding country code before the phoneNumber and find
                NSString *phoneWithCountryCode = [self.currentPhoneCountryCode stringByAppendingString:phoneNumber];
                foundContact = [self.addressBookCache objectForKey:phoneWithCountryCode];
                
                if (foundContact != nil)
                {
                    phoneNumberWhichWasFoundInCache = phoneWithCountryCode;
                }
                else
                {
                    //try adding '+' before the phoneNumber and find
                    NSString *phoneWithPlus = [(@"+") stringByAppendingString:phoneNumber];
                    foundContact = [self.addressBookCache objectForKey:phoneWithPlus];
                    if (foundContact != nil)
                    {
                        phoneNumberWhichWasFoundInCache = phoneWithPlus;
                    }
                }
            }
            //TODO: implement search if there is ONLY PART of country code in the phoneNumber

            if (phoneNumberWhichWasFoundInCache != nil)
            {
                [foundContact addTwinNumber:phoneNumber forOriginalNumber:phoneNumberWhichWasFoundInCache];
            }
        }
    }
    [self.lock unlock];
    return foundContact;
}

- (NSString *)getPhoneCodeForCurrentCountry
{
    if (_phoneCountryCodes)
    {
        NSString *isoCountryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
        if (isoCountryCode != nil)
        {
            return [self.phoneCountryCodes objectForKey:isoCountryCode];
        }
    }
    return nil;
}

+ (NSDictionary *)loadPhoneCountryCodes
{
    NSString *countryCodesPath = [[NSBundle mainBundle] pathForResource:@(kCountryCodesFileName) ofType:nil];
    NSData *countryCodesData = [NSData dataWithContentsOfFile:countryCodesPath];
    if (countryCodesData == nil)
    {
        return nil;
    }

    NSError *err = nil;
    NSDictionary *countryCodes = [NSJSONSerialization JSONObjectWithData:countryCodesData options:0 error:&err];
    if (countryCodes == nil)
    {
        NSLog(@"Failed to load country codes: err=%@", (err == nil) ? @"<nil>" : err.description);
    }
    return countryCodes;
}



@end
