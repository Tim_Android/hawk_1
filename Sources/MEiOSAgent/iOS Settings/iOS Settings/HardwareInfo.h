#import <Foundation/Foundation.h>

@interface HardwareInfo : NSObject

@property (strong, nonatomic, readonly) NSString *model;
@property (strong, nonatomic, readonly) NSString *modelNumber;
@property (strong, nonatomic, readonly) NSString *regionInfo;
@property (strong, nonatomic, readonly) NSString *deviceUuid;

+ (HardwareInfo *)sharedInstance;

- (NSString *)phoneNumber;
- (NSString *)imei;

@end
