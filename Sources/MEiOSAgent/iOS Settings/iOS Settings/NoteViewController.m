#import "NoteViewController.h"
#import "StyleUtils.h"

#define kSegueGotoHome @"gotoHome"

@interface NoteViewController ()

@end

@implementation NoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [StyleUtils setOrangeStyleForButton:_finishButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onFinishButtonTap:(id)sender
{
    [self performSegueWithIdentifier:kSegueGotoHome sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueGotoHome])
    {
        [[[segue destinationViewController] navigationItem] setHidesBackButton:YES];
    }
}

@end
