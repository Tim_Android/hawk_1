#ifndef iOS_Settings_ConstantsUI_h
#define iOS_Settings_ConstantsUI_h

#define kDefaultFontName       "Helvetica"
#define kDefaultNavBarFontSize 18
#define kDefaultButtonFontSize 17

#define kBackButtonFontName    "Arial" //it looks better then Helvetica in iOS 6
#define kBackButtonFontSize    14

#endif
