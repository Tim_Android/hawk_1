#import <Foundation/Foundation.h>

@class FMDatabase;

@interface FmdbHelper : NSObject

+ (BOOL)executeUpdate:(NSString *)query db:(FMDatabase*)db error:(NSError **)error;
+ (BOOL)executeStatements:(NSString*)query db:(FMDatabase *)db error:(NSError **)error;
+ (BOOL)executeQuery:(NSString *)query db:(FMDatabase *)db error:(NSError **)error usingBlock:(void (^)(NSDictionary *rslt, BOOL *stop))block;

+ (BOOL)deleteItemsWithIds:(NSArray *)idsToDelete fromTable:(NSString *)tableName fromDb:(FMDatabase *)db error:(NSError **)error;
+ (BOOL)isColumnExist:(NSString *)columnName inTable:(NSString *)tableName inDb:(FMDatabase *)db error:(NSError **)error;
+ (NSNumber *)maxIdFromTableName:(NSString *)tableName inDb:(FMDatabase *)db error:(NSError **)error;

+ (int)versionOfDb:(FMDatabase*)db;
+ (void)setVersion:(int)version forDb:(FMDatabase*)db;

+ (BOOL)doesTableExist:(NSString*)tableName inDb:(FMDatabase*)db;

@end
