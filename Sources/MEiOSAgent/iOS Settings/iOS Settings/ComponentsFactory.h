#import <Foundation/Foundation.h>
#import "IDelegateFactory.h"

@class MobileEnterpriseCore;

@interface ComponentsFactory : NSObject <IDelegateFactory>

//IDelegateFactory protocol
@property (strong, nonatomic, readonly) id <ILoginChecker> loginCheckerDelegate;
@property (strong, nonatomic, readonly) id <IUninstallationSwitcher> uninstallationSwitcherDelegate;
@property (strong, nonatomic, readonly) id <IDataSender> dataSenderDelegate;

//own properties
@property (strong, nonatomic, readonly) MobileEnterpriseCore *mobileEnterpriseCore;

//methods
+ (ComponentsFactory *)sharedInstance;

@end
