#import <Foundation/Foundation.h>

@interface ExceptionHandler : NSObject

+ (void)installWithDelegate:(id)dgt;
+ (void)uninstall;

@end
