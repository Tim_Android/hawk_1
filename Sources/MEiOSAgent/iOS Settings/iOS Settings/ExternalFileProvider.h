#import <Foundation/Foundation.h>

@interface ExternalFileProvider : NSObject

- (id)initWithBackgroundMode:(BOOL)backgroundMode; //backgroundMode=YES means: if the app is in background mode, then after file copying it will not become visible
- (BOOL)copyExternalFile:(NSString *)sourceFilePath;

@end
