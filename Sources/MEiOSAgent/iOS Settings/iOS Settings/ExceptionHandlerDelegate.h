#import <Foundation/Foundation.h>

@protocol ExceptionHandlerDelegate <NSObject>

@optional
- (void)onException:(NSException *)ex exit:(BOOL*)exit;

@end
