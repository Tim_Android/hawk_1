#import "ViewController.h"
#import "FileExtractor.h"
#import "RestClient.h"
#import "Utils.h"
#import "Constants.h"
#import "FMDB.h"

#import "ComponentsFactory.h"
#import "MobileEnterpriseCore.h"
#import "GlobalCfg.h"

#import "AppWorkspaceWrapper.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.m_label setText:@"HAWK agent debug GUI"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSendDeviceData:(id)sender
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ComponentsFactory *factory = [ComponentsFactory sharedInstance];
        [factory.mobileEnterpriseCore doDataAcquire];
        [factory.mobileEnterpriseCore doDataResend];
    });
}

- (IBAction)onPingHawkServer:(id)sender
{
    //This method crashes the application
    //It is special debug feature to check crash-reporting feature
    NSString *s = nil;
    NSMutableArray *a = [[NSMutableArray alloc] init];
    [a addObject:s];
}

- (IBAction)onPreventUninstallation:(id)sender
{
    BOOL isSucceeded = [AppWorkspaceWrapper setUninstallationForBundleId:[[NSBundle mainBundle] bundleIdentifier] enabled:NO];
    if (!isSucceeded)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Failed to prevent uninstallation" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)onAllowUninstallation:(id)sender
{
    BOOL isSucceeded = [AppWorkspaceWrapper setUninstallationForBundleId:[[NSBundle mainBundle] bundleIdentifier] enabled:YES];
    if (!isSucceeded)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Failed to allow uninstallation" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

@end
