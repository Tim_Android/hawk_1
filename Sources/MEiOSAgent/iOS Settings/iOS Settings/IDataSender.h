#import <Foundation/Foundation.h>

typedef enum
{
    SendData_ErrorInternal             = 0,
    SendData_ErrorNoInternetConnection = 1,
    SendData_ErrorServerIsUnavailable  = 2,
    SendData_ErrorInternalOnServer     = 3,
    SendData_Success                   = 4,
} SendDataResult;

@protocol IDataSender <NSObject>

- (SendDataResult)sendData;

@end