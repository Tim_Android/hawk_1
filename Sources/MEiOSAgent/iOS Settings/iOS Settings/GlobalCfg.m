#import "GlobalCfg.h"
#import "Constants.h"
#import "Utils.h"
#import "HardwareInfo.h"
#import "Log.h"

@interface GlobalCfg ()

@property (strong, nonatomic) NSString *docsDir;

@property (strong, nonatomic) NSURL* serviceUrl;
@property (strong, nonatomic) NSString *agentName;
@property (strong, nonatomic) NSString *svnRevision;
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *phoneNumber;

@property (strong, nonatomic) NSString *smsDbPath;
@property (strong, nonatomic) NSString *smsDbWalPath;
@property (strong, nonatomic) NSString *callsDbPath;
@property (strong, nonatomic) NSString *contactsDbPath;
@property (strong, nonatomic) NSString *contactsDbWalPath;

@property (strong, nonatomic) NSString *tempSmsDbPath;
@property (strong, nonatomic) NSString *tempSmsDbWalPath;
@property (strong, nonatomic) NSString *tempCallsDbPath;
@property (strong, nonatomic) NSString *tempContactsDbPath;
@property (strong, nonatomic) NSString *tempContactsDbWalPath;

@property (strong, nonatomic) NSString *localStorageDbPath;
@property (nonatomic) long long localStorageMaxSz;

@property (nonatomic) BOOL isFirstLaunch;
@property (nonatomic) BOOL isCrashTestEnabled;

@property (strong, nonatomic) NSCharacterSet *chsetToIncludeInPhoneNumber;
@property (strong, nonatomic) NSCharacterSet *chsetToExcludeFromPhoneType;
@property (strong, nonatomic) NSCharacterSet *chsetToExcludeFromMmsBody;

@property (strong, nonatomic) NSString *logPath;
@property (strong, nonatomic) NSNumber *logLevel;

@end

@implementation GlobalCfg

+ (GlobalCfg *)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        NSDictionary *cfg = [GlobalCfg loadCfg];
        
        _logLevel = [cfg objectForKey:@(kLogLevel)];
        g_maxlevel = [_logLevel intValue];
        
        _deviceId = [[HardwareInfo sharedInstance] deviceUuid];
        
        if (_deviceId == nil)
        {
            NSLog(@"Device UUID is not retrieved (nil) - the app cannot run without it.");
            assert(0); //crash
        }
        else if (_deviceId.length == 0)
        {
            NSLog(@"Device UUID is not retrieved (empty string) - the app cannot run without it.");
            assert(0); //crash
        }
        
        _serviceUrl = [[NSURL alloc] initWithString:[cfg objectForKey:@(kServiceUrl)]];
        _agentName = [cfg objectForKey:@(kAgentName)];
        _svnRevision = [cfg objectForKey:@(kSvnRevision)];
        _isCrashTestEnabled = [[cfg objectForKey:@(kCrashTestEnabled)] boolValue];
        _supportEmail = [cfg objectForKey:@(kKeySupportEmail)];
        _emailLogMaxSize = [[cfg objectForKey:@(kKeyMaxEmailLogSize)] longValue] * 1024; //KBytes -> bytes
        
        _docsDir = [Utils getPathForDocumentsDirectory];
        _smsDbPath = [_docsDir stringByAppendingPathComponent:@"sms.db"];
        _smsDbWalPath = [_docsDir stringByAppendingPathComponent:@"sms.db-wal"];
        _callsDbPath = [_docsDir stringByAppendingPathComponent:@"call_history.db"];
        _callsDbWalPath = [_docsDir stringByAppendingPathComponent:@"call_history.db-wal"];
        _contactsDbPath = [_docsDir stringByAppendingPathComponent:@"AddressBook.db"];
        _contactsDbWalPath = [_docsDir stringByAppendingPathComponent:@"AddressBook.db-wal"];
        _callsDbPath8x = [_docsDir stringByAppendingPathComponent:@"call_history8x.db"];
        _callsDbWalPath8x = [_docsDir stringByAppendingPathComponent:@"call_history8x.db-wal"];
        
        if ([Utils isCydiaMode])
        {
            NSString *jailDocsDir = @"/private/var/mobile/Library/Application Support/Containers/com.hawk/Documents/";
            _tempSmsDbPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/sms.db"];
            _tempSmsDbWalPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/sms.db-wal"];
            _tempCallsDbPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/call_history.db"];
            _tempCallsDbWalPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/call_history.db-wal"];
            _tempContactsDbPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/AddressBook.sqlitedb"];
            _tempContactsDbWalPath = [jailDocsDir stringByAppendingPathComponent:@"Inbox/AddressBook.sqlitedb-wal"];
            _tempCallsDbPath8x = [jailDocsDir stringByAppendingPathComponent:@"Inbox/CallHistory.storedata"];
            _tempCallsDbWalPath8x = [jailDocsDir stringByAppendingPathComponent:@"Inbox/CallHistory.storedata-wal"];
        }
        else
        {
            _tempSmsDbPath = [_docsDir stringByAppendingPathComponent:@"Inbox/sms.db"];
            _tempSmsDbWalPath = [_docsDir stringByAppendingPathComponent:@"Inbox/sms.db-wal"];
            _tempCallsDbPath = [_docsDir stringByAppendingPathComponent:@"Inbox/call_history.db"];
            _tempCallsDbWalPath = [_docsDir stringByAppendingPathComponent:@"Inbox/call_history.db-wal"];
            _tempContactsDbPath = [_docsDir stringByAppendingPathComponent:@"Inbox/AddressBook.sqlitedb"];
            _tempContactsDbWalPath = [_docsDir stringByAppendingPathComponent:@"Inbox/AddressBook.sqlitedb-wal"];
            _tempCallsDbPath8x = [_docsDir stringByAppendingPathComponent:@"Inbox/CallHistory.storedata"];
            _tempCallsDbWalPath8x = [_docsDir stringByAppendingPathComponent:@"Inbox/CallHistory.storedata-wal"];
        }
        
        _localStorageMaxSz = [[cfg objectForKey:@(kLocalStorageMaxSz)] longLongValue] * 1024; // in bytes
        _localStorageDbPath = [_docsDir stringByAppendingPathComponent:@(kLocalStorageName)];
        
        _logPath = [_docsDir stringByAppendingPathComponent:@(kLogFileName)];
        
        NSMutableCharacterSet *charSet = [NSMutableCharacterSet decimalDigitCharacterSet];
        [charSet addCharactersInString:@"+"];
        
        _chsetToIncludeInPhoneNumber = charSet;
        _chsetToExcludeFromPhoneType = [NSCharacterSet characterSetWithCharactersInString:@"() -_$!<>"];
        _chsetToExcludeFromMmsBody = [NSCharacterSet characterSetWithCharactersInString:@"\xef\xbf\xbc"];
        
        // Be cautious, we store inverted flag
        _isFirstLaunch = ![[NSUserDefaults standardUserDefaults] boolForKey:@(kIsFirstLaunchKey)];
        if (_isFirstLaunch)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@(kIsFirstLaunchKey)];
    }
    
    return self;
}

+ (NSDictionary *)loadCfg
{
    NSString *configPath = [[NSBundle mainBundle] pathForResource:@(kConfigName) ofType:nil];
    NSData *configData = [NSData dataWithContentsOfFile:configPath];
    assert(configData);
    
    NSError *err = nil;
    NSDictionary *config = [NSJSONSerialization JSONObjectWithData:configData options:0 error:&err];
    assert(!err);
    
    return config;
}

+ (NSDate *)getLastResendDate
{
    NSInteger lastResendTime = [[NSUserDefaults standardUserDefaults] integerForKey:@(kLastResendTimeKey)];
    return (lastResendTime != 0) ? [[NSDate alloc] initWithTimeIntervalSince1970:lastResendTime] : [NSDate date];
}

+ (void)setLastResendDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setInteger:[[NSDate date] timeIntervalSince1970] forKey:@(kLastResendTimeKey)];
}

+ (NSDate *)getLastAcquisitionDate
{
    NSInteger lastAcquisitionTime = [[NSUserDefaults standardUserDefaults] integerForKey:@(kLastAcquisitionTimeKey)];
    return (lastAcquisitionTime != 0) ? [[NSDate alloc] initWithTimeIntervalSince1970:lastAcquisitionTime] : [NSDate date];
}

+ (void)setLastAcquisitionDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setInteger:[[NSDate date] timeIntervalSince1970] forKey:@(kLastAcquisitionTimeKey)];
}

+ (NSNumber *)getMaxAcquiredCallId
{
    return [NSNumber numberWithInteger:[[NSUserDefaults standardUserDefaults] integerForKey:@(kMaxAcquiredCallId)]];
}

+ (void)setMaxAcquiredCallId:(NSNumber *)callId
{
    [[NSUserDefaults standardUserDefaults] setInteger:[callId integerValue] forKey:@(kMaxAcquiredCallId)];
}

@end
