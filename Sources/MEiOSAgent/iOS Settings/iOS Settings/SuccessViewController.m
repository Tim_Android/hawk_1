#import "SuccessViewController.h"
#import "StyleUtils.h"

#define kSegueGotoNote @"gotoNote"

@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [StyleUtils setOrangeStyleForButton:_okButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)OnOkTap:(id)sender
{
    [self performSegueWithIdentifier:kSegueGotoNote sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueGotoNote])
    {
        [[[segue destinationViewController] navigationItem] setHidesBackButton:NO];
    }
}

@end
