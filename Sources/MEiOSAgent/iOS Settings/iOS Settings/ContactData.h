#import <Foundation/Foundation.h>

@interface ContactData : NSObject

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath forNumber:(NSString *)number sinceDate:(NSDate *)sinceDate;

- (NSArray *)allNumbers;
- (NSString *)typeForNumber:(NSString *)number;
- (NSString *)person;

- (void)addTwinNumber:(NSString *)twinNumber forOriginalNumber:(NSString *)originalNumber;

@end
