#import "HardwareInfo.h"

#include <dlfcn.h>
#include <sys/sysctl.h>
#include <mach/mach_host.h>

#import "FCUUID.h"

// IORegistry
#define kIODeviceTreePlane "IODeviceTree"

enum {
    kIORegistryIterateRecursively = 0x00000001,
    kIORegistryIterateParents = 0x00000002
};

typedef	char        io_string_t[512];
typedef mach_port_t io_object_t;
typedef io_object_t io_registry_entry_t;
typedef char        io_name_t[128];
typedef UInt32      IOOptionBits;

typedef CFTypeRef (*PIORegistryEntrySearchCFProperty)(io_registry_entry_t entry, const io_name_t plane, CFStringRef key, CFAllocatorRef allocator, IOOptionBits options);
typedef kern_return_t (*PIOMasterPort)(mach_port_t bootstrapPort, mach_port_t *masterPort);
typedef kern_return_t (*Pmach_port_deallocate)(ipc_space_t task, mach_port_name_t name);
typedef io_registry_entry_t (*PIORegistryGetRootEntry)(mach_port_t masterPort);
typedef kern_return_t (*PIORegistryEntryCreateCFProperties)(io_registry_entry_t entry, CFMutableDictionaryRef *properties, CFAllocatorRef allocator, IOOptionBits options);
typedef kern_return_t (*PIORegistryEntryGetPath)(io_registry_entry_t entry, const io_name_t plane, io_string_t path);
typedef kern_return_t (*PIORegistryEntryGetChildEntry)(io_registry_entry_t entry, const io_name_t plane, io_registry_entry_t *child);

typedef NSString *(*PCTSettingCopyMyPhoneNumber)();

@interface HardwareInfo ()

@property (strong, nonatomic) NSString *model;
@property (strong, nonatomic) NSString *modelNumber;
@property (strong, nonatomic) NSString *serialNumber;
@property (strong, nonatomic) NSString *regionInfo;
@property (strong, nonatomic) NSString *deviceUuid;

@end

@implementation HardwareInfo (Private)

+ (NSString *)getSysInfoByName:(char *)typeSpecifier
{
    size_t size;
    sysctlbyname(typeSpecifier, NULL, &size, NULL, 0);
    
    char *answer = malloc(size);
    sysctlbyname(typeSpecifier, answer, &size, NULL, 0);
    
    NSString *results = [NSString stringWithCString:answer encoding:NSUTF8StringEncoding];
    
    free(answer);
    return results;
}

+ (NSArray *)getIoRegistryValue:(NSString *)searchKey
{
    // TODO: Duplications want to be parametrized
    void *lib = dlopen("/System/Library/Frameworks/IOKit.framework/IOKit", RTLD_LAZY);
    PIORegistryEntrySearchCFProperty pIORegistryEntrySearchCFProperty = (PIORegistryEntrySearchCFProperty)dlsym(lib, "IORegistryEntrySearchCFProperty");
    PIOMasterPort pIOMasterPort = (PIOMasterPort)dlsym(lib, "IOMasterPort");
    Pmach_port_deallocate pmach_port_deallocate = (Pmach_port_deallocate)dlsym(lib, "mach_port_deallocate");
    PIORegistryGetRootEntry pIORegistryGetRootEntry = (PIORegistryGetRootEntry)dlsym(lib, "IORegistryGetRootEntry");
    
    mach_port_t masterPort;
    CFTypeID propID = (CFTypeID) NULL;
    unsigned int bufSize;
    
    kern_return_t kr = pIOMasterPort(MACH_PORT_NULL, &masterPort);
    if (kr != noErr) return nil;
    
    io_registry_entry_t entry = pIORegistryGetRootEntry(masterPort);
    if (entry == MACH_PORT_NULL) return nil;
    
    CFTypeRef prop = (CFTypeRef) pIORegistryEntrySearchCFProperty(entry, kIODeviceTreePlane, (__bridge CFStringRef) searchKey, nil, kIORegistryIterateRecursively);
    if (!prop) return nil;
    
    propID = CFGetTypeID(prop);
    if (!(propID == CFDataGetTypeID()))
    {
        pmach_port_deallocate(mach_task_self(), masterPort);
        return nil;
    }
    
    CFDataRef propData = (CFDataRef) prop;
    if (!propData) return nil;
    
    bufSize = (unsigned int) CFDataGetLength(propData);
    if (!bufSize) return nil;
    
    NSString *p1 = [[NSString alloc] initWithBytes:CFDataGetBytePtr(propData) length:bufSize encoding:1];
    pmach_port_deallocate(mach_task_self(), masterPort);
    return [p1 componentsSeparatedByString:@"\0"];
}

@end

@implementation HardwareInfo

+ (id)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _serialNumber = [[HardwareInfo getIoRegistryValue:@"serial-number"] objectAtIndex:0];
        _regionInfo = [[HardwareInfo getIoRegistryValue:@"region-info"] objectAtIndex:0];           // LL/A
        _model = [HardwareInfo getSysInfoByName:"hw.model"];                                        // N41AP
        _modelNumber = [[HardwareInfo getIoRegistryValue:@"model-number"] objectAtIndex:0];         // MD293
        _deviceUuid = [FCUUID uuidForDevice];

        if (_modelNumber && _regionInfo)
            _modelNumber = [[NSString alloc] initWithFormat:@"%@%@", _modelNumber, _regionInfo];    // MD293LL/A
        
        if (_modelNumber)
            _model = _modelNumber;  // If we can get model from registry, we should use it
    }
    
    return self;
}

- (NSString *)phoneNumber
{
    void *lib = dlopen("/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony", RTLD_LAZY);
    PCTSettingCopyMyPhoneNumber pCTSettingCopyMyPhoneNumber = (PCTSettingCopyMyPhoneNumber)dlsym(lib, "CTSettingCopyMyPhoneNumber");
    return pCTSettingCopyMyPhoneNumber();
}

- (NSString *)imei
{
    NSArray *results = [HardwareInfo getIoRegistryValue:@"device-imei"];
    return (results) ? [results objectAtIndex:0] : nil;
}

// !!!: This sample can be useful during IORegistry research 
//+ (void)traverseIoReg
//{
//    void *lib = dlopen("/System/Library/Frameworks/IOKit.framework/IOKit", RTLD_LAZY);
//    
//    PIORegistryEntryCreateCFProperties pIORegistryEntryCreateCFProperties = (PIORegistryEntryCreateCFProperties)dlsym(lib, "IORegistryEntryCreateCFProperties");
//    PIOMasterPort pIOMasterPort = (PIOMasterPort)dlsym(lib, "IOMasterPort");
//    Pmach_port_deallocate pmach_port_deallocate = (Pmach_port_deallocate)dlsym(lib, "mach_port_deallocate");
//    PIORegistryGetRootEntry pIORegistryGetRootEntry = (PIORegistryGetRootEntry)dlsym(lib, "IORegistryGetRootEntry");
//    PIORegistryEntryGetPath pIORegistryEntryGetPath = (PIORegistryEntryGetPath)dlsym(lib, "IORegistryEntryGetPath");
//    PIORegistryEntryGetChildEntry pIORegistryEntryGetChildEntry = (PIORegistryEntryGetChildEntry)dlsym(lib, "IORegistryEntryGetChildEntry");
//    
//    kern_return_t kr = noErr;
//    mach_port_t masterPort;
//    kr = pIOMasterPort(MACH_PORT_NULL, &masterPort);
//    assert(!kr);
//    
//    io_registry_entry_t entry = pIORegistryGetRootEntry(masterPort);
//    assert(entry);
//    
//    CFMutableDictionaryRef props = 0;
//    kr = pIORegistryEntryCreateCFProperties(entry, &props, kCFAllocatorDefault, 0);
//    assert(!kr);
//    
////    io_string_t path = {0};
////    kr = pIORegistryEntryGetPath(entry, kIODeviceTreePlane, path);
////    assert(!kr);
//    
//    io_registry_entry_t child;
//    kr = pIORegistryEntryGetChildEntry(entry, kIODeviceTreePlane, &child);
//    assert(!kr);
//    
//    kr = pIORegistryEntryCreateCFProperties(child, &props, kCFAllocatorDefault, 0);
//    assert(!kr);
//    
//    pmach_port_deallocate(mach_task_self(), masterPort);
//}

@end
