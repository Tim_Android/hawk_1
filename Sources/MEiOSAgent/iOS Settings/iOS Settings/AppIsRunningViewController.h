#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AppIsRunningViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *sendDataButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *sendDataWaitIndicator;
@property (weak, nonatomic) IBOutlet UILabel *sendResultLabel;
@property (weak, nonatomic) IBOutlet UIButton *contactUsButton;

- (IBAction)onSettingsTap:(id)sender;
- (IBAction)onSendDataTap:(id)sender;
- (IBAction)onContactUsTap:(id)sender;
@end
