#import "RestClient.h"
#import "Constants.h"
#import "AuthenticationResponse.h"
#import "CreateAgentResponse.h"
#import "Utils.h"

// HTTP codes
#define kHttpOk 200

// other constants
#define kAgentTypeIOS @"1"

@interface RestClient ()

@property (strong, nonatomic) NSURL *serviceUrl;
@property (strong, nonatomic) NSString *hostName;
@property (strong, nonatomic) NSString *deviceId;

@property (strong, atomic) NSError *lastError;
@property (strong, atomic) NSHTTPURLResponse *lastResponse;
@property (strong, atomic) NSData *lastResponseData;
@property (strong, nonatomic) NSCondition *responseIsAvail;

@end

@implementation RestClient

- (id)initWithUrl:(NSURL *)url agentId:(NSString *)agentId deviceId:(NSString *)deviceId
{
    self = [super init];
    
    if (self)
    {
        _serviceUrl = url;
        _hostName = [_serviceUrl host];
        _agentId = agentId;
        _deviceId = deviceId;
        _responseIsAvail = [[NSCondition alloc] init];
    }
    
    return self;
}

- (BOOL)sendRequest:(NSURLRequest *)request
{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    self.lastResponse = nil;
    self.lastError = nil;
    self.lastResponseData = nil;
    
    [conn setDelegateQueue:queue];
    [conn start];
    
    [self.responseIsAvail lock];
    
    BOOL isOk = YES;
    NSDate *date = [[NSDate alloc] initWithTimeInterval:kUrlRequestTimeout sinceDate:[NSDate date]];
    
    if (![self.responseIsAvail waitUntilDate:date])
    {
        isOk = NO;
        [conn cancel];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"waitUntilDate:, time limit was reached" forKey:@"Reason"];
        self.lastError = [[NSError alloc] initWithDomain:@"RestClient" code:errno userInfo:userInfo];
    }
    
    [self.responseIsAvail unlock];
    return isOk;
}

- (BOOL)isHostReachable
{
    return [Utils isHostReachable:_hostName];
}

- (BOOL)updateData:(NSData *)jsonData
{
    if (_agentId == nil)
    {
        return NO;
    }
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    NSString *relativeUrl = [NSString stringWithFormat:@"/%@/%@/Update", self.agentId, self.deviceId];
    
    [req setHTTPMethod:@"POST"];
    [req setURL:[self.serviceUrl URLByAppendingPathComponent:relativeUrl]];
    [req setTimeoutInterval:kUrlRequestTimeout];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req addValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"utf-8" forHTTPHeaderField:@"Content-Encoding"];
    [req addValue:@"ios-agent" forHTTPHeaderField:@"User-Agent"];
    [req setHTTPBody:jsonData];
    
    if (![self sendRequest:req])
        return NO;

    return (kHttpOk == [self.lastResponse statusCode]);
}

- (BOOL)uploadMMSAttachment:(NSData *)binaryAttachment attachmentId:(NSString *)attachmentId
{
    if (_agentId == nil)
    {
        return NO;
    }

    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    NSString *relativeUrl = [NSString stringWithFormat:@"/%@/%@/UploadMMSAttachment/%@", self.agentId, self.deviceId, attachmentId];
    
    [req setHTTPMethod:@"POST"];
    [req setURL:[self.serviceUrl URLByAppendingPathComponent:relativeUrl]];
    [req setTimeoutInterval:kUrlRequestTimeout];
    [req addValue:@"binary/octet-stream" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"utf-8" forHTTPHeaderField:@"Content-Encoding"];
    [req addValue:@"ios-agent" forHTTPHeaderField:@"User-Agent"];
    [req setHTTPBody:binaryAttachment];
    
    if (![self sendRequest:req])
        return NO;
    
    return (kHttpOk == [self.lastResponse statusCode]);
}

- (BOOL)pingServer:(NSString **)rslt;
{
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    
    [req setHTTPMethod:@"POST"];
    [req setURL:[self.serviceUrl URLByAppendingPathComponent:@"/Ping"]];
    [req setTimeoutInterval:kUrlRequestTimeout];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req addValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"utf-8" forHTTPHeaderField:@"Content-Encoding"];
    [req addValue:@"ios-agent" forHTTPHeaderField:@"User-Agent"];
    
    self.lastResponseData = nil;

    if (![self sendRequest:req])
        return NO;
    
    BOOL isOk = (kHttpOk == [self.lastResponse statusCode]);
    *rslt = (isOk) ? [[NSString alloc] initWithData:self.lastResponseData encoding:NSUTF8StringEncoding] : nil;
    return isOk;
}

- (CreateAgentResponse *)createAgentForLogin:(NSString *)login password:(NSString *)password deviceInfo:(NSData *)jsonData
{
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    
    NSString *relativeUrl = [NSString stringWithFormat:@"/%@/%@/CreateAgent/%@", login, password, kAgentTypeIOS];
    
    [req setHTTPMethod:@"POST"];
    [req setURL:[self.serviceUrl URLByAppendingPathComponent:relativeUrl]];
    [req setTimeoutInterval:kUrlRequestTimeout];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req addValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"utf-8" forHTTPHeaderField:@"Content-Encoding"];
    [req addValue:@"ios-agent" forHTTPHeaderField:@"User-Agent"];
    [req setHTTPBody:jsonData];
    
    self.lastResponseData = nil;
    
    if (![self sendRequest:req])
    {
        return nil;
    }
    
    if (kHttpOk == [self.lastResponse statusCode])
    {
        return [[CreateAgentResponse alloc] initWithData:_lastResponseData];
    }
    return nil;
}

- (AuthenticationResponse *)authenticate:(NSString *)login password:(NSString *)password
{
    if (_agentId == nil)
    {
        return nil;
    }
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    
    NSString *relativeUrl = [NSString stringWithFormat:@"/%@/%@/Authentication/%@", login, password, _agentId];
    
    [req setHTTPMethod:@"POST"];
    [req setURL:[self.serviceUrl URLByAppendingPathComponent:relativeUrl]];
    [req setTimeoutInterval:kUrlRequestTimeout];
    [req addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req addValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"utf-8" forHTTPHeaderField:@"Content-Encoding"];
    [req addValue:@"ios-agent" forHTTPHeaderField:@"User-Agent"];
    
    self.lastResponseData = nil;
    
    if (![self sendRequest:req])
    {
        return nil;
    }
    
    if (kHttpOk == [self.lastResponse statusCode])
    {
        return [[AuthenticationResponse alloc] initWithData:_lastResponseData];
    }

    return nil;
}

//
// NSURLConnectionDataDelegate
//
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.lastResponse = (NSHTTPURLResponse *)[response copy];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.lastResponseData != nil)
    {
        NSMutableData *temp = [NSMutableData dataWithData:self.lastResponseData];
        [temp appendData:data];
        self.lastResponseData = [NSData dataWithData:temp];
    }
    else
        self.lastResponseData = [NSData dataWithData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self.responseIsAvail lock];
    [self.responseIsAvail signal];
    [self.responseIsAvail unlock];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.lastError = [error copy];
    
    [self.responseIsAvail lock];
    [self.responseIsAvail signal];
    [self.responseIsAvail unlock];
}


//
// NSURLConnectionDelegate
//
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    BOOL rslt = [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
	return rslt;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
	{
		// For the time being we only trust our own domain
		if ([challenge.protectionSpace.host isEqualToString:self.hostName])
		{
			NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
		}
	}
    else
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end
