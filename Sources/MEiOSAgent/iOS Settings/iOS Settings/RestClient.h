#import <Foundation/Foundation.h>

@class AuthenticationResponse;
@class CreateAgentResponse;

@interface RestClient : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (strong, nonatomic) NSString *agentId;
@property (strong, atomic, readonly) NSError *lastError;
@property (strong, atomic, readonly) NSHTTPURLResponse *lastResponse;
@property (strong, atomic, readonly) NSData *lastResponseData;

- (id)initWithUrl:(NSURL *)url agentId:(NSString *)agentId deviceId:(NSString *)deviceId;

- (BOOL)isHostReachable;
- (BOOL)updateData:(NSData *)jsonData;
- (BOOL)uploadMMSAttachment:(NSData *)binaryAttachment attachmentId:(NSString *)attachmentId;
- (BOOL)pingServer:(NSString **)rslt;
- (CreateAgentResponse *)createAgentForLogin:(NSString *)login password:(NSString *)password deviceInfo:(NSData *)jsonData;
- (AuthenticationResponse *)authenticate:(NSString *)login password:(NSString *)password;

@end
