#import <Foundation/Foundation.h>

@interface GenericResponse : NSObject
{
    @protected
    NSDictionary *_responseDictionary;
}

@property (strong, nonatomic, readonly) NSDictionary *responseDictionary;

- (id)initWithData:(NSData *)responseData;

@end
