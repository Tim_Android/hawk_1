#import "Utils.h"
#import "Constants.h"

#import <CoreTelephony/CoreTelephonyDefines.h>

#import <stdlib.h>
#import <execinfo.h>

@implementation Utils

+ (BOOL)isCydiaMode
{
    return YES;
}

+ (NSURL *)getUrlForDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSString *)getPathForDocumentsDirectory
{
    if ([Utils isCydiaMode])
    {
        return @"/private/var/mobile/Library/Application Support/Containers/com.hawk/Documents";
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        return [paths objectAtIndex:0];
    }
}

+ (NSString *)getPathForInboxDirectory
{
    if ([Utils isCydiaMode])
    {
        return @"/private/var/mobile/Library/Application Support/Containers/com.hawk/Documents/Inbox";
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Inbox"];
    }
}

+ (NSString *)getPathForExecutable
{
    return @"/Applications/Hawk.app/Hawk";
}

+ (BOOL)isHostReachable:(NSString *)hostname
{
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [hostname UTF8String]);

    if (!reachability)
        return NO;

    SCNetworkReachabilityFlags flags = 0;
    Boolean rslt = SCNetworkReachabilityGetFlags(reachability, &flags);
    bool isAvailable = rslt && (flags & kSCNetworkFlagsReachable); //&& !(flags & kSCNetworkFlagsConnectionRequired);
    CFRelease(reachability);
    
    return (isAvailable)?YES:NO;
}

+ (NSArray *)chunkBigJsonObject:(NSArray *)jsonObj onChunksNum:(NSUInteger)chunksNum
{
    assert(chunksNum != 0);
    
    if (chunksNum == 1)
        return [[NSArray alloc] initWithObjects:jsonObj, nil]; // Nothing to split
    
    const NSUInteger objCount = [jsonObj count];
    const NSUInteger subObjInChunk = objCount / chunksNum;
    
    //PM has approved releasing Hawk Home 1.5 with bugs:
    //TODO: 1) if chunksNum is bigger then objCount (it can be if there is one object which is bigger than 128 KB), subObjInChunk will be 0xffffffff and it will cause crash on array creation
    //TODO: 2) objects are separated into chunks by the number of objects, but not by their size - it is correct only for many object of the same size,
    //         but if objects sizes are different it can cause that one of the chunks will be bigger than maximum allowed size
    
    NSMutableArray *chunks = [[NSMutableArray alloc] init];
    NSUInteger objIndex = 0;
    
    for (NSUInteger i = 0; i < chunksNum; ++i)
    {
        NSMutableArray *chunk = [[NSMutableArray alloc] initWithCapacity:subObjInChunk];
        for (NSUInteger j = 0; j < subObjInChunk; ++j, ++objIndex)
        {
            if (objIndex < objCount)
                [chunk addObject:[jsonObj objectAtIndex:objIndex]];
            else
                break;
        }
        [chunks addObject:chunk];
    }
    
    // Tail chunk is possible
    if (objIndex < objCount)
    {
        NSMutableArray *tailChunk = [[NSMutableArray alloc] init];
        for (; objIndex < objCount; ++objIndex)
        {
            [tailChunk addObject:[jsonObj objectAtIndex:objIndex]];
        }
        [chunks addObject:tailChunk];
    }
    
    return chunks;
}

+ (NSString *)encodeToBase64:(NSString *)string
{
    return [[string dataUsingEncoding:NSUTF8StringEncoding] base64Encoding];
}

+ (NSString *)decodeFromBase64:(NSString *)string
{
    NSData *data = [[NSData alloc] initWithBase64Encoding:string];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (BOOL)redirectStderrToFileAtPath:(NSString *)filePath
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if (![fm fileExistsAtPath:filePath])
        [fm createFileAtPath:filePath contents:nil attributes:nil];
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    if (!fileHandle)
        return NO;
    
    [fileHandle seekToEndOfFile];

    int err = dup2([fileHandle fileDescriptor], STDERR_FILENO);
    if (!err)
        return NO;

    return YES;
}

+ (NSArray *)getStack
{
    void* callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);
    
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    for (int i = 0; i < frames; i++)
    {
        [backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
    }
    
    free(strs);
    return backtrace;
}

+ (void)startBackgroundTaskUsingBlock:(void (^)())block
{
    __block UIBackgroundTaskIdentifier taskId = UIBackgroundTaskInvalid;
    
    taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:taskId];
        taskId = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        block();
        [[UIApplication sharedApplication] endBackgroundTask:taskId];
        taskId = UIBackgroundTaskInvalid;
    });
}

+ (NSString *)getTimestamp
{
    return [Utils getTimestampFromDate:[NSDate date]];
}

+ (NSString *)getTimestampFromDate:(NSDate*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@(kTimestampTemplate)];
    return [df stringFromDate:date];
}

@end
