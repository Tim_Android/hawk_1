#import <Foundation/Foundation.h>

@interface LocalStorage : NSObject

@property (nonatomic, strong, readonly) NSError *lastError;

- (id)initWithDbPath:(NSString *)filePath;

- (NSNumber *)size;
- (BOOL)clear;

- (BOOL)saveDeviceInfo:(id)deviceInfo;
- (BOOL)saveCallData:(NSArray *)callData;
- (BOOL)saveMessageData:(NSArray *)msgData;
- (BOOL)saveMmsData:(NSArray *)mmsData;
- (BOOL)saveLocationData:(NSArray *)locationData;
- (BOOL)saveCrashData:(NSArray *)crashData;
- (BOOL)saveMmsAttachmentsInfo:(NSArray *)attachmentsInfo;

- (id)loadDeviceInfo;
- (NSArray *)loadCallDataSinceDate:(NSDate *)date;
- (NSArray *)loadMessageDataSinceDate:(NSDate *)date;
- (NSArray *)loadMmsDataSinceDate:(NSDate *)date;
- (NSArray *)loadLocationDataSinceDate:(NSDate *)date;
- (NSArray *)loadCrashDataSinceDate:(NSDate *)date;
- (NSArray *)loadNotSentAttachments;

- (BOOL)eraseDeviceInfo;
- (BOOL)eraseCallDataSinceDate:(NSDate *)date;
- (BOOL)eraseMessageDataSinceDate:(NSDate *)date;
- (BOOL)eraseMmsDataSinceDate:(NSDate *)date;
- (BOOL)eraseLocationDataSinceDate:(NSDate *)date;
- (BOOL)eraseCrashDataSinceDate:(NSDate *)date;

- (BOOL)eraseDataItems:(NSArray *)items ofType:(Class)itemType;

@end
