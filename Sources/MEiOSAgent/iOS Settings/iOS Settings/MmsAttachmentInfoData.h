#import <Foundation/Foundation.h>

#import "IConvertableData.h"
#import "IPersistableData.h"

@interface MmsAttachmentInfoData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic, readonly) NSString *attachmentPath;
@property (strong, nonatomic, readonly) NSString *attachmentType;
@property (strong, nonatomic, readonly) NSNumber *attachmentId;
@property (strong, nonatomic, readonly) NSNumber *messageId;

+ (NSArray *)loadAttachmentsFromDb:(NSString *)dbPath forMessageId:(NSNumber *)messageId;
+ (NSString *)stringFromAttachmentsArray:(NSArray *)attacments;
+ (NSArray *)attachmentsArrayFromString:(NSString *)string;
+ (NSString *)stringFromAttachment:(MmsAttachmentInfoData *)attachmentInfo;
+ (MmsAttachmentInfoData *)attachmentFromString:(NSString *)string;

@end
