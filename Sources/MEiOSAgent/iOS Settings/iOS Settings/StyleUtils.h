#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StyleUtils : NSObject

+ (BOOL)setBackgroundImageWithName:(NSString *)imageName forTextField:(UITextField *)textField;
+ (BOOL)setBackgroundImageWithName:(NSString *)imageName forButton:(UIButton *)button forState:(UIControlState)state;
+ (BOOL)setOrangeStyleForButton:(UIButton *)button;

@end
