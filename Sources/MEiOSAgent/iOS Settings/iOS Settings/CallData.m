#import "Constants.h"
#import "CallData.h"
#import "FMDB.h"
#import "FmdbHelper.h"
#import "IContactsProvider.h"
#import "ContactData.h"

// Feture Ids
#define kCall_FromNumber        "fromNumber"
#define kCall_FromNumberName    "fromNumberName"
#define kCall_FromNumberType    "fromNumberType"
#define kCall_ToNumber          "toNumber"
#define kCall_ToNumberName      "toNumberName"
#define kCall_ToNumberType      "toNumberType"
#define kCall_Type              "type"
#define kCall_Date              "date"
#define kCall_Duration          "duration"

// Call types
#define kCallType_Incoming  4
#define kCallType_Outgoing  5
#define kCallType_Blocked   8

#define kCall_TableName     "call"

// Call types required by server specification
#define kCallTypeForServer_Incoming "Incoming"
#define kCallTypeForServer_Missed   "Missed"
#define kCallTypeForServer_Outgoing "Outgoing"

// Flags - for iOS < 8:
//  0x0         Incoming call flag
//  0x1         Outgoing call flag
//  0x4         Regular call
//  0x8         Very rare and probably similar to 4 above
//  0x10        FaceTime call
//  0x10000     No network flag
//  0x20000     Some kind of error
//  0x40000     Some kind of error
//  0x80000     Some kind of error
//  0x100000    Dropped Due to Network Problems flag

// ZORIGINATED column (is used instead of flags to detect call type) - for iOS 8
// 0 - incoming
// 1 - outgoing

// Entry of call_history.db usually looks like this
//{
//    ROWID = 6662;                 // Primary key
//    address = "+18013698210";     // Phone number or FaceTime ID of contact that has been called/has received call from
//    assisted = 0;
//    "country_code" = 310;         // The mobile country code (MCC) of the country the phone was in when the call was placed. Example: 234 for United Kingdom. (list of MCC/MNC)
//    date = 1330266413;            // The date of the phone call
//    duration = 52;                // Number of seconds the phone call lasted
//    "face_time_data" = "<null>";
//    flags = 4;                    // Flag is 4 for incoming calls, 5 for outgoing calls, 8 for blocked calls
//    id = "-1";                    // Contacts or Address Book ID of the contact being called. This column would be -1 if the contact is unknown (e.g. not in address book). This column is always -1 on incoming calls.
//    name = "";
//    "network_code" = 410;         // The mobile network code (MNC) of the network the phone was on when the call was placed. Example: 30 for T-Mobile (UK) (list of MCC/MNC)
//    originalAddress = "";
//    read = 1;                     //read=0 if the call is an missed call, which hasn't been "read". Example there is a (number) on the Phone icon, showing number of missed calls. read=1 if the missed calls has been checked.
//}

//{
//    ROWID = 6664;
//    address = "greenday96@gmail.com";
//    assisted = 0;
//    "country_code" = 310;
//    date = 1330298814;
//    duration = 0;
//    "face_time_data" = "<null>";
//    flags = 20;
//    id = "-1";
//    name = "";
//    "network_code" = 410;
//    originalAddress = "";
//    read = 1;
//}

//{
//    ROWID = 6761;
//    address = 7184401982;
//    assisted = 0;
//    "country_code" = 000;
//    date = 1401117076;
//    duration = 0;
//    "face_time_data" = "<null>";
//    flags = 65541;
//    id = "-1";
//    name = "";
//    "network_code" = 00;
//    originalAddress = "<null>";
//    read = 0;
//}

@interface CallData ()

@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *person;
@property (strong, nonatomic) NSString *callType;
@property (strong, nonatomic) NSString *numberType;
@property (strong, nonatomic) NSDate *timestamp;
@property (strong, nonatomic) NSNumber *duration;
@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation CallData

#pragma mark -
#pragma mark Initialization from internal DB

+ (NSString *)resolveCallType:(NSDictionary *)entry
{
    NSString *callType = nil;
    NSInteger duration = [[entry objectForKey:@"duration"] integerValue];
    NSInteger flags = [[entry objectForKey:@"flags"] integerValue];
    
    if (flags & 0x1)
        callType = @(kCallTypeForServer_Outgoing);
    else
        callType = (duration) ? @(kCallTypeForServer_Incoming) : @(kCallTypeForServer_Missed);
    
    assert(callType);
    return callType;
}

- (id)initWithInternalDbEntry:(NSDictionary *)entry withContactsProvider:(id<IContactsProvider>)contactsProvider
{
    self = [super init];
    
    if (self)
    {
        _duration = [entry objectForKey:@"duration"];
        _callType = [CallData resolveCallType:entry];
        
        id addressDbEntry = [entry objectForKey:@"address"];
        _phoneNumber = ((addressDbEntry == nil || ![addressDbEntry isKindOfClass:[NSString class]]) ? @"No Caller ID" : addressDbEntry );
        
        id date1970 = [entry objectForKey:@"date1970"];
        if (date1970 != nil)
        {
            _timestamp = [NSDate dateWithTimeIntervalSince1970:[date1970 doubleValue]];
        }
        else
        {
            id date2001 = [entry objectForKey:@"date2001"];
            _timestamp = [NSDate dateWithTimeIntervalSinceReferenceDate:[date2001 doubleValue]];
        }
        
        ContactData *contactData = [contactsProvider searchForNumber:_phoneNumber];
        _numberType = [contactData typeForNumber:_phoneNumber];
        _person = [contactData person];

        // This is possible, this is normal case
        if (!_numberType)
            _numberType = @(kNotAvailable);
        
        if (!_person)
            _person = @(kNotAvailable);
        
#ifdef DEBUG
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        [df setDateStyle:NSDateFormatterShortStyle];
        NSLog(@"New call: %@, %@, 0x%lx, %@, %@, %@", _phoneNumber, _person, (long)[[entry objectForKey:@"flags"] integerValue], _numberType, [df stringFromDate:_timestamp], _duration);
#endif // DEBUG
        
    }
    
    return self;
}

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error
{
    NSString *query = nil;
    if (dbVersion == CallsDbVersion_iOS8)
    {
        NSString *dateFilter = (date) ? [[NSString alloc] initWithFormat:@" where date2001 >= %.0f", [date timeIntervalSinceReferenceDate]] : @"";
        query = [[NSString alloc] initWithFormat:@"select rowid, ZORIGINATED as flags, ZADDRESS as address, ZDATE as date2001, ZDURATION as duration from ZCALLRECORD %@;", dateFilter];
    }
    else
    {
        NSString *dateFilter = (date) ? [[NSString alloc] initWithFormat:@" where date1970 >= %.0f", [date timeIntervalSince1970]] : @"";
        query = [[NSString alloc] initWithFormat:@"select rowid, flags, address, date as date1970, duration from call %@;", dateFilter];
    }
   
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        CallData *data = [[CallData alloc] initWithInternalDbEntry:rslt withContactsProvider:contactsProvider];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (NSNumber *)maxIdFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion error:(NSError **)error
{
    NSString *callsTableName = (dbVersion == CallsDbVersion_iOS8 ? @"ZCALLRECORD" : @"call");
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper maxIdFromTableName:callsTableName inDb:db error:error];
}

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath version:(CallsDbVersion)dbVersion afterId:(NSNumber *)afterId withContactsProvider:(id<IContactsProvider>)contactsProvider maxId:(NSNumber **)maxId error:(NSError **)error
{
    NSString *query = nil;
    NSString *callsTableName = nil;
    if (dbVersion == CallsDbVersion_iOS8)
    {
        query = [[NSString alloc] initWithFormat:@"SELECT rowid, ZORIGINATED AS flags, ZADDRESS AS address, ZDATE AS date2001, ZDURATION AS duration FROM ZCALLRECORD "
                 "WHERE rowid > %ld ", (long)[afterId integerValue]];
        callsTableName = @"ZCALLRECORD";
    }
    else
    {
        query = [[NSString alloc] initWithFormat:@"SELECT rowid, flags, address, date AS date1970, duration FROM call "
                 "WHERE rowid > %ld ", (long)[afterId integerValue]];
        callsTableName = @"call";
    }
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        CallData *data = [[CallData alloc] initWithInternalDbEntry:rslt withContactsProvider:contactsProvider];
        [entries addObject:data];
    }];
    *maxId = [FmdbHelper maxIdFromTableName:callsTableName inDb:db error:error];
    
    if (!isOk)
    {
        return nil;
    }
    return ([entries count]) ? entries : nil;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (CallData *entry in data)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:entry.callType forKey:@(kCall_Type)];
        [dict setObject:entry.duration forKey:@(kCall_Duration)];
        
        if ([entry.callType isEqualToString:@(kCallTypeForServer_Outgoing)])
        {
            [dict setObject:entry.phoneNumber forKey:@(kCall_ToNumber)];
            [dict setObject:entry.person      forKey:@(kCall_ToNumberName)];
            [dict setObject:entry.numberType  forKey:@(kCall_ToNumberType)];
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_FromNumber)];
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_FromNumberName)];
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_FromNumberType)];
        }
        else
        {
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_ToNumber)];
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_ToNumberName)];
            [dict setObject:@(kNotAvailable)  forKey:@(kCall_ToNumberType)];
            [dict setObject:entry.phoneNumber forKey:@(kCall_FromNumber)];
            [dict setObject:entry.person      forKey:@(kCall_FromNumberName)];
            [dict setObject:entry.numberType  forKey:@(kCall_FromNumberType)];
        }
        
        // JSON requires date in such format
        [dict setObject:[NSString stringWithFormat:@"/Date(%.0f)/", [entry.timestamp timeIntervalSince1970]*1000] forKey:@(kCall_Date)]; // in milliseconds

        [jsonObject addObject:dict];
    }
    
    return jsonObject;
}

#pragma mark -
#pragma mark Local Storage

- (id)initWithLocalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _callType = [entry objectForKey:@(kCall_Type)];
        _phoneNumber = [entry objectForKey:@(kCall_FromNumber)];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@(kCall_Date)] doubleValue]];
        _duration = [entry objectForKey:@(kCall_Duration)];
        _numberType = [entry objectForKey:@(kCall_FromNumberType)];
        _person = [entry objectForKey:@(kCall_FromNumberName)];
        _localStorageId = [entry objectForKey:@"rowid"];
    }
    
    return self;
}

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSString *query = (date)
        ? [[NSString alloc] initWithFormat:@"select rowid, * from %@ where date >= %.0f order by rowid asc;", @(kCall_TableName), [date timeIntervalSince1970]]
        : [[NSString alloc] initWithFormat:@"select rowid, * from %@ order by rowid asc;", @(kCall_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        CallData *data = [[CallData alloc] initWithLocalDbEntry:rslt];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (CallData *entry in data)
    {
        [statements appendFormat:
         @"insert into %@ values ('%@', '%@', '%@', '%@', %ld, %ld);",
         @(kCall_TableName),
         entry.phoneNumber,
         entry.numberType,
         entry.person,
         entry.callType,
         (long)[entry.timestamp timeIntervalSince1970],
         (long)[entry.duration integerValue]];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    NSString *query = nil;
    
    if (dropIfExists)
    {
        query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kCall_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    query = [[NSString alloc] initWithFormat:
             @"create table if not exists %@ (%@ text, %@ text, %@ text, %@ text, %@ integer, %@ integer)",
             @(kCall_TableName),
             @(kCall_FromNumber),
             @(kCall_FromNumberType),
             @(kCall_FromNumberName),
             @(kCall_Type),
             @(kCall_Date),
             @(kCall_Duration)];
    
    isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    return isOk;
}

+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSTimeInterval timestamp = (date) ? [date timeIntervalSince1970] : 0;
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where date >= %.0f", @(kCall_TableName), timestamp];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kCall_TableName) fromDb:db error:error];
}

@end
