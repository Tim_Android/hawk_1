#import "StyleUtils.h"

@implementation StyleUtils

+ (BOOL)setBackgroundImageWithName:(NSString *)imageName forTextField:(UITextField *)textField
{
    UIImage *backgroundImage = [UIImage imageNamed:imageName];
    if (backgroundImage == nil)
    {
        return NO;
    }
    backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 9)];
    [textField setBackground:backgroundImage];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    return YES;
}

+ (BOOL)setBackgroundImageWithName:(NSString *)imageName forButton:(UIButton *)button forState:(UIControlState)state
{
    UIImage *backgroundImage = [UIImage imageNamed:imageName];
    if (backgroundImage == nil)
    {
        return NO;
    }
    backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 9, 0, 9)];
    [button setBackgroundImage:backgroundImage forState:state];
    return YES;
}

+ (BOOL)setOrangeStyleForButton:(UIButton *)button
{
    BOOL success = YES;
    success &= [StyleUtils setBackgroundImageWithName:@"button_orange" forButton:button forState:UIControlStateNormal];
    success &= [StyleUtils setBackgroundImageWithName:@"button_orange_tap" forButton:button forState:UIControlStateHighlighted];
    success &= [StyleUtils setBackgroundImageWithName:@"button_orange_inactive" forButton:button forState:UIControlStateDisabled];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    return success;
}

@end
