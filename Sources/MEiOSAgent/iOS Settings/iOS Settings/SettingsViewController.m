#import "SettingsViewController.h"
#import "ComponentsFactory.h"
#import "IUninstallationSwitcher.h"

@interface SettingsViewController ()

@property (strong, nonatomic, readonly) id <IUninstallationSwitcher> uninstallationSwitcherDelegate;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    id <IDelegateFactory> delegateFactory = [ComponentsFactory sharedInstance];
    _uninstallationSwitcherDelegate = delegateFactory.uninstallationSwitcherDelegate;
    
    if (_uninstallationSwitcherDelegate)
    {
        [_allowUninstallationSwitch setOn:[_uninstallationSwitcherDelegate isUninstallationEnabled] animated:NO];
        [_allowUninstallationSwitch setEnabled:YES];
    }
    else
    {
        [_allowUninstallationSwitch setEnabled:NO];
    }
    _restartDeviceLabel.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Returning to root view when app goes into background

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)onAppWillResignActive
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - Public Actions

- (IBAction)onAllowUninstallationChanged:(UISwitch *)sender
{
    BOOL isSucceeded = NO;
    if (_uninstallationSwitcherDelegate != nil)
    {
        isSucceeded = [_uninstallationSwitcherDelegate setUninstallationEnabled:sender.isOn];
    }

    if (isSucceeded)
    {
        _restartDeviceLabel.hidden = NO;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"An internal error has occurred. Please try again. If the problem persists, please contact our support." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [sender setOn:(!sender.isOn) animated:NO];
    }
}

@end
