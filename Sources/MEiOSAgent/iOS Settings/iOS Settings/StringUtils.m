#import "StringUtils.h"

@implementation NSString (UtilsCategory)

- (void)splitIntoChunksWithLength:(int)chunkLength usingBlock:(void (^)(NSString *chunk))block
{
    for (int currentChunkStart = 0; currentChunkStart < self.length; currentChunkStart += chunkLength)
    {
        int currentChunkLength = MIN(chunkLength, self.length - currentChunkStart);
        NSString *currentChunk = [self substringWithRange:NSMakeRange(currentChunkStart, currentChunkLength)];
        block(currentChunk);
    }
}

@end
