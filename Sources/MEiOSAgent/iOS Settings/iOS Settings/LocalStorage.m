#import "LocalStorage.h"

#import "Utils.h"
#import "Constants.h"

#import "CallData.h"
#import "MessageData.h"
#import "MmsData.h"
#import "LocationData.h"
#import "CrashData.h"
#import "MmsAttachmentInfoData.h"

#import "FMDB.h"
#import "FmdbHelper.h"

#define kCurrentDbVersion 1

@interface LocalStorage ()

@property (nonatomic, strong) NSString *dbPath;
@property (nonatomic, strong) NSLock *dbLock;
@property (atomic, strong) id deviceInfo;
@property (nonatomic, strong) NSError *lastError;

@end

@implementation LocalStorage (Private)

+ (BOOL)vacuumizeSqliteDb:(NSString *)dbPath
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:dbPath];
    
    if (![db open])
        return NO;
    
    return [db executeUpdate:@"vacuum"] && [db close];
}

@end

@implementation LocalStorage

- (id)initWithDbPath:(NSString *)filePath
{
    self = [super init];
    BOOL isOk = YES;
    
    if (self)
    {
        _dbPath = filePath;
        _dbLock = [[NSLock alloc] init];
        
        [_dbLock lock];
        
        isOk = [self updateDbIfNeeded];
        
        NSError *error = nil;
        
        if (isOk)
            isOk = [CallData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        if (isOk)
            isOk = [MessageData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        if (isOk)
            isOk = [MmsData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        if (isOk)
            isOk = [LocationData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        if (isOk)
            isOk = [CrashData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        if (isOk)
            isOk = [MmsAttachmentInfoData createTableInDb:_dbPath dropIfExists:NO error:&error];
        
        _lastError = error;
        
        [_dbLock unlock];
    }
    
    return (isOk) ? self : nil;
}

- (NSNumber *)size;
{
    [self.dbLock lock];
    
    NSDictionary *attrs = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.dbPath])
    {
        NSError *err = nil;
        attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:self.dbPath error:&err];
        self.lastError = err;
    }
    
    [self.dbLock unlock];
    
    return (attrs) ? [attrs objectForKey:@"NSFileSize"] : [[NSNumber alloc] initWithInt:-1];
}

- (BOOL)clear
{
    BOOL isOk = YES;
    
    [self.dbLock lock];
    NSError *err = nil;
    isOk &= [CallData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [MessageData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [MmsData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [LocationData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [CrashData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [MmsAttachmentInfoData createTableInDb:self.dbPath dropIfExists:YES error:&err];
    isOk &= [LocalStorage vacuumizeSqliteDb:self.dbPath];
    [self.dbLock unlock];
    
    return isOk;
}

#pragma mark -
#pragma mark Saving

- (BOOL)saveDeviceInfo:(id)deviceInfo
{
    self.deviceInfo = deviceInfo;
    return YES;
}

- (BOOL)saveCallData:(NSArray *)callData
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [CallData saveIntoDb:self.dbPath data:callData error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)saveMessageData:(NSArray *)msgData
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [MessageData saveIntoDb:self.dbPath data:msgData error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)saveMmsData:(NSArray *)mmsData
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [MmsData saveIntoDb:self.dbPath data:mmsData error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)saveLocationData:(NSArray *)locationData
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [LocationData saveIntoDb:self.dbPath data:locationData error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)saveCrashData:(NSArray *)crashData
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [CrashData saveIntoDb:self.dbPath data:crashData error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)saveMmsAttachmentsInfo:(NSArray *)attachmentsInfo
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [MmsAttachmentInfoData saveIntoDb:self.dbPath data:attachmentsInfo error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

#pragma mark -
#pragma mark Loading

- (id)loadDeviceInfo
{
    return self.deviceInfo;
}

- (NSArray *)loadCallDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *callData = [CallData loadFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return callData;
}

- (NSArray *)loadMessageDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *msgData = [MessageData loadFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return msgData;
}

- (NSArray *)loadMmsDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *mmsData = [MmsData loadFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return mmsData;
}

- (NSArray *)loadLocationDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *locationData = [LocationData loadFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return locationData;
}

- (NSArray *)loadCrashDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *crashData = [CrashData loadFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return crashData;
}

- (NSArray *)loadNotSentAttachments
{
    [self.dbLock lock];
    NSError *err = nil;
    NSArray *attachments = [MmsAttachmentInfoData loadFromDb:self.dbPath sinceDate:nil error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return attachments;
}

#pragma mark -
#pragma mark Erasing

- (BOOL)eraseDeviceInfo
{
    self.deviceInfo = nil;
    return YES;
}

- (BOOL)eraseCallDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [CallData eraseFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)eraseMessageDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [MessageData eraseFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)eraseMmsDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [MmsData eraseFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)eraseLocationDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [LocationData eraseFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)eraseCrashDataSinceDate:(NSDate *)date
{
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [CrashData eraseFromDb:self.dbPath sinceDate:date error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

- (BOOL)eraseDataItems:(NSArray *)items ofType:(Class)itemType
{
    if (!items || ![items count])
    {
        return YES;
    }
    [self.dbLock lock];
    NSError *err = nil;
    BOOL isOk = [itemType eraseFromDb:self.dbPath items:items error:&err];
    self.lastError = err;
    [self.dbLock unlock];
    return isOk;
}

#pragma mark - Update db version

- (BOOL)updateDbIfNeeded
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:_dbPath];
    const int oldDbVersion = [FmdbHelper versionOfDb:db];
    if (oldDbVersion < kCurrentDbVersion)
    {
        BOOL isOk = YES;
        isOk &= [MessageData updateTableInDb:_dbPath fromVersion0To1Error:NULL];
        isOk &= [MmsData updateTableInDb:_dbPath fromVersion0To1Error:NULL];
        
        [FmdbHelper setVersion:kCurrentDbVersion forDb:db];
        
        return isOk;
    }
    
    return YES;
}

@end


