#import "AuthenticationResponse.h"

#define kServerCodeBadCredentials            0
#define kServerCodeAuthenticateSuccess       1
#define kServerCodeAuthenticateInternalError 2

#define kResponseKeyState @"state"

@implementation AuthenticationResponse

- (id)initWithData:(NSData *)responseData
{
    self = [super initWithData:responseData];
    if (self)
    {
        _isServerError= NO;
        _isAuthenticated = NO;

        id authStateObject = [_responseDictionary objectForKey:kResponseKeyState];
        if (authStateObject == nil)
        {
            NSLog(@"AuthenticationResponse parsing error, there is no 'state' attribure in response.");
            return nil;
        }
        else if (![authStateObject isKindOfClass:[NSNumber class]])
        {
            NSLog(@"AuthenticationResponse parsing error, 'state' has type %@, but NSNumber is expected.", NSStringFromClass([authStateObject class]));
            return nil;
        }
        else
        {
            long authState = [authStateObject integerValue];
            if (authState == kServerCodeAuthenticateSuccess)
            {
                _isAuthenticated = YES;
            }
            else if (authState == kServerCodeBadCredentials)
            {
                _isAuthenticated = NO;
            }
            else if (authState == kServerCodeAuthenticateInternalError)
            {
                _isServerError = YES;
            }
            else
            {
                NSLog(@"AuthenticationResponse parsing error, 'state' has unknown code=%li.", authState);
                return nil;
            }
        }
    }
    return self;
}

@end
