#import <Foundation/Foundation.h>

@protocol IUninstallationSwitcher <NSObject>

- (BOOL)setUninstallationEnabled:(BOOL)enabled;
- (BOOL)isUninstallationEnabled;

@end