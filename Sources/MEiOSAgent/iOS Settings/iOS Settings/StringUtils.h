#import <Foundation/Foundation.h>

@interface NSString (UtilsCategory)

- (void)splitIntoChunksWithLength:(int)chunkLength usingBlock:(void (^)(NSString *chunk))block;

@end
