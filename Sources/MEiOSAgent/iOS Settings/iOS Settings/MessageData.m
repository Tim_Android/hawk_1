#import "Constants.h"
#import "MessageData.h"
#import "ContactData.h"
#import "FMDB.h"
#import "FmdbHelper.h"
#import "Utils.h"
#import "Log.h"

// Typical entry looks like
//{
//    ROWID = 1;
//    date = 302038892;         // Note. This is Mac Absolute Time
//    id = "+18018360831";
//    "is_from_me" = 0;
//    service = SMS;
//    text = "Dinner? Not sure what to make. We have salmon and mahi mahi. Potatoes? Some veggies? Maybe Dahl?";
//}

// Server requires types:   |   Table keys      |    Notes
// - Inbox                      - is_from_me
// - Sent                       - is_sent
// - Draft
// - Outbox                     - is_from_me
// - Failed                                         // failed outgoing
// - Queued                                         // send later

// Available:
// - is_delivered
// - is_finished    // mostly 1
// - is_emote       // mostly 0
// - is_from_me
// - is_empty       // mostly 0
// - is_delayed     // mostly 0
// - is_auto_reply  // mostly 0
// - is_prepared
// - is_read
// - is_system_message  // mostly 0
// - is_sent
// - is_service_message // mostly0
// - is_forward         // mostly 0
// - is_archive         // mostly 0

// Used for transfer
#define kSms_PhoneNumber    "address"
#define kSms_ContactName    "contactName"
#define kSms_LocalTime      "date"
#define kSms_MsgBody        "body"
#define kSms_MsgType        "type"

// Local storage table
#define kSms_TableName      "message"

@interface MessageData ()

@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation MessageData

#pragma mark -
#pragma mark Initialization from internal DB

+ (NSString *)resolveMessageState:(NSDictionary *)entry
{
    NSString *msgState = nil;
    
    // !!!: Currently unused, but may be helpful in future
    //BOOL isDelivered = [[entry objectForKey:@"is_delivered"] boolValue];
    //BOOL isPrepared = [[entry objectForKey:@"is_prepared"] boolValue];
    //BOOL isRead = [[entry objectForKey:@"is_read"] boolValue];
    
    BOOL isFromMe = [[entry objectForKey:@"is_from_me"] boolValue];
    BOOL isSent = [[entry objectForKey:@"is_sent"] boolValue];
    
    if (isFromMe)
        msgState = @"Outbox";
    else
        msgState = @"Inbox";
    
    if (isFromMe && !isSent)
        msgState = @"Failed";
    
    assert(msgState);
    return msgState;
}

- (id)initWithInternalDbEntry:(NSDictionary *)entry withContactsProvider: (id<IContactsProvider>)contactsProvider
{
    self = [super init];
    
    if (self)
    {
        _phoneNumber = [entry objectForKey:@"phone_numbers"];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@"date"] integerValue]];
        _body = [entry objectForKey:@"text"];
        _messageType = [MessageData resolveMessageState:entry];
        
        ContactData* contact = [contactsProvider searchForNumber: _phoneNumber];
        _contactName = contact.person;
        
        // This can be empty and should be fixed
        if (!_body || [_body isEqual:[NSNull null]])
            _body = @(kNotAvailable);
        
#ifdef DEBUG
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        [df setDateStyle:NSDateFormatterShortStyle];
        NSLog(@"New message: %@, %@, %lu, %@", _phoneNumber, _messageType, (long)[_body length], [df stringFromDate:_timestamp]);
#endif // DEBUG
        
    }
    
    return self;
}

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error;
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    //additional condition is used to select only normal messages and skip notifications
    //e.g.: incoming message with attachment notification (item_type=1) or notification that your audio message is received (item_type=5)
    NSError *columnError = nil;
    BOOL isItemTypeColumnExist = [FmdbHelper isColumnExist:@"item_type" inTable:@"message" inDb:db error:&columnError];
    NSString *additionalCondition = (isItemTypeColumnExist ? @" AND m.item_type = 0 " : @"");
    if (columnError != nil)
    {
        isItemTypeColumnExist = NO;
        NSLog_Error(@"Cannot check if column exists, err=%@", columnError.description);
    }

    NSString *query = nil;
    NSTimeInterval timeIntervalSince1970 = (date ? [date timeIntervalSince1970] : 0);
    query = [NSString stringWithFormat:
             @"SELECT m.rowid, m.date + %.0f as date, group_concat(h.id, ', ') AS phone_numbers, m.service, is_delivered, is_from_me, is_prepared, is_read, is_sent, text "
             "FROM message m "
             "LEFT JOIN chat_message_join ON m.rowid = chat_message_join.message_id "
             "LEFT JOIN chat_handle_join ON chat_message_join.chat_id = chat_handle_join.chat_id "
             "LEFT JOIN handle h ON h.rowid = chat_handle_join.handle_id "
             "WHERE (m.date + %.0f) >= %.0f AND m.cache_has_attachments = 0 %@ "
             "GROUP BY m.rowid "
             "ORDER BY m.rowid ASC", NSTimeIntervalSince1970, NSTimeIntervalSince1970, timeIntervalSince1970, additionalCondition];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        MessageData *data = [[MessageData alloc] initWithInternalDbEntry:rslt withContactsProvider:contactsProvider];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (MessageData *entry in data)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:entry.phoneNumber forKey:@(kSms_PhoneNumber)];
        [dict setObject:entry.body forKey:@(kSms_MsgBody)];
        [dict setObject:entry.messageType forKey:@(kSms_MsgType)];
        [dict setObject:[NSString stringWithFormat:@"/Date(%.0f)/", [entry.timestamp timeIntervalSince1970]*1000] forKey:@(kSms_LocalTime)]; // in milliseconds
        
        if (entry.contactName)
        {
            [dict setObject:entry.contactName forKey:@(kSms_ContactName)];
        }
        
        [jsonObject addObject:dict];
    }
    
    return jsonObject;
}

#pragma mark -
#pragma mark Local Storage

- (id)initWithLocalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _phoneNumber = [entry objectForKey:@(kSms_PhoneNumber)];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@(kSms_LocalTime)] doubleValue]];
        _messageType = [entry objectForKey:@(kSms_MsgType)];
        _body = [Utils decodeFromBase64:[entry objectForKey:@(kSms_MsgBody)]];
        _localStorageId = [entry objectForKey:@"rowid"];
        
        if (![entry[@(kSms_ContactName)] isEqualToString: @"(null)"])
        {
            _contactName = entry[@(kSms_ContactName)];
        }
    }
    
    return self;
}

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSString *query = (date)
        ? [[NSString alloc] initWithFormat:@"select rowid, * from %@ where date >= %.0f order by rowid asc;", @(kSms_TableName), [date timeIntervalSince1970]]
        : [[NSString alloc] initWithFormat:@"select rowid, * from %@ order by rowid asc;", @(kSms_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        MessageData *data = [[MessageData alloc] initWithLocalDbEntry:rslt];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (MessageData* entry in data)
    {
        [statements appendFormat:
         @"insert into %@ values ('%@', %.0f, '%@', '%@', '%@');",
         @(kSms_TableName),
         entry.phoneNumber,
         [entry.timestamp timeIntervalSince1970],
         [Utils encodeToBase64:entry.body],
         entry.messageType,
         entry.contactName];
    }

    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    
    if (dropIfExists)
    {
        NSString *query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kSms_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    isOk &= [self createTableInDb:db error:error];
    return isOk;
}

+ (BOOL)updateTableInDb:(NSString *)dbPath fromVersion0To1Error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    if ([FmdbHelper doesTableExist:@(kSms_TableName) inDb:db])
    {
        NSString *query = [[NSString alloc] initWithFormat:
                           @"alter table %@ add column %@ text",
                           @(kSms_TableName),
                           @(kSms_ContactName)];
        
        return [FmdbHelper executeUpdate:query db:db error:error];
    }
    else
    {
        return [self createTableInDb:db error:error];
    }
}

+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSTimeInterval timestamp = (date) ? [date timeIntervalSince1970] : 0;
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where date >= %.0f", @(kSms_TableName), timestamp];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kSms_TableName) fromDb:db error:error];
}

+ (BOOL)createTableInDb:(FMDatabase*)db error:(NSError **)error
{
    NSString* query = [[NSString alloc] initWithFormat:
                       @"create table if not exists %@ (%@ text, %@ integer, %@ text, %@ text, %@ text)",
                       @(kSms_TableName),
                       @(kSms_PhoneNumber),
                       @(kSms_LocalTime),
                       @(kSms_MsgBody),
                       @(kSms_MsgType),
                       @(kSms_ContactName)];
    
    return [FmdbHelper executeUpdate:query db:db error:error];
}

@end
