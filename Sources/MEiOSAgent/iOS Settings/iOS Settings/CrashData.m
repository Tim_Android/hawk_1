#import "CrashData.h"
#import "FMDB.h"
#import "FmdbHelper.h"
#import "Constants.h"
#import "StringUtils.h"

#define kCrash_Text         "text"
#define kCrash_Date         "date"

#define kCrash_TableName    "crash"

#define kMaxCrashLogItemLength 4000 //limitation in server database is 4000
#define kMaxCrashLogLength (kMaxCrashLogItemLength * 2 - 10) //one crash log will be devided into a few items because we don't want to fix server database now

@interface CrashData ()

@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation CrashData (Private)

+ (NSString *)buildMsg:(NSException *)ex textLog:(NSString *)textLog
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterMediumStyle];
    [df setDateStyle:NSDateFormatterShortStyle];
    
    NSMutableString *msg = [[NSMutableString alloc] init];
    [msg appendFormat:@"Reason: %@ ", [ex reason]];
    
    NSArray *stacktrace = [[ex userInfo] objectForKey:@(kUEH_StacktraceKey)];
    if (stacktrace)
    {
        [msg appendFormat:@"Stack: %@ ", [stacktrace componentsJoinedByString:@" "]];
    }
    
    NSNumber *signal = [[ex userInfo] objectForKey:@(kUEH_SignalKey)];
    if (signal)
        [msg appendFormat:@"Signal: %@ ", signal];
    
    if (textLog)
    {
        NSString *logPartToUse = nil;
        NSUInteger remainingLogSize = kMaxCrashLogLength - [msg length];
        if ([textLog length] > remainingLogSize)
        {
            logPartToUse = [textLog substringFromIndex:([textLog length] - remainingLogSize)];
        }
        else
        {
            logPartToUse = textLog;
        }
        
        logPartToUse = [logPartToUse stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        if ([logPartToUse length] > remainingLogSize)
        {
            //recheck size after replacing quotes
            logPartToUse = [logPartToUse substringFromIndex:([logPartToUse length] - remainingLogSize)];
        }
        
        NSArray *log = [logPartToUse componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        [msg appendFormat:@"Log: %@ ", [log componentsJoinedByString:@" "]];
    }
    
    return msg;
}

@end

@implementation CrashData

- (id)initWithException:(NSException *)ex
{
    self = [super init];
    
    if (self)
    {
        _text = [CrashData buildMsg:ex textLog:nil];
        _date = [NSDate date];
        
#ifdef DEBUG
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        [df setDateStyle:NSDateFormatterShortStyle];
        NSLog(@"New crash: %@", _text);
#endif // DEBUG
    }
    
    return self;
}

- (id)initWithException:(NSException *)ex textLog:(NSString *)textLog
{
    self = [super init];
    
    if (self)
    {
        _text = [CrashData buildMsg:ex textLog:textLog];
        _date = [NSDate date];
        
#ifdef DEBUG
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeStyle:NSDateFormatterMediumStyle];
        [df setDateStyle:NSDateFormatterShortStyle];
        NSLog(@"New crash: %@", _text);
#endif // DEBUG
    }
    
    return self;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (CrashData *entry in data)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:entry.text forKey:@"data"];
        [dict setObject:[NSString stringWithFormat:@"/Date(%.0f)/", [entry.date timeIntervalSince1970]*1000] forKey:@"date"];
        
        [jsonObject addObject:dict];
    }
    
    return jsonObject;
}

#pragma mark -
#pragma mark Local Storage

- (id)initWithLocalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _text = [entry objectForKey:@(kCrash_Text)];
        _date = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@(kCrash_Date)] longValue]];
        _localStorageId = [entry objectForKey:@"rowid"];
    }
    
    return self;
}

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSString *query = (date)
        ? [[NSString alloc] initWithFormat:@"select rowid, * from %@ where date >= %.0f order by rowid asc;", @(kCrash_TableName), [date timeIntervalSince1970]]
        : [[NSString alloc] initWithFormat:@"select rowid, * from %@ order by rowid asc;", @(kCrash_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        CrashData *data = [[CrashData alloc] initWithLocalDbEntry:rslt];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    __block NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (CrashData* entry in data)
    {
        //split big log into different log items, because server has limitation for the size of one item
        [entry.text splitIntoChunksWithLength:kMaxCrashLogItemLength usingBlock:^(NSString *chunk) {
            [statements appendFormat:
             @"insert into %@ values ('%@', %.0f);",
             @(kCrash_TableName),
             chunk,
             [entry.date timeIntervalSince1970]];
        }];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    NSString *query = nil;
    
    if (dropIfExists)
    {
        query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kCrash_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    query = [[NSString alloc] initWithFormat:
             @"create table if not exists %@ (%@ text, %@ integer)",
             @(kCrash_TableName),
             @(kCrash_Text),
             @(kCrash_Date)];
    
    isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    return isOk;
}

+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSTimeInterval timestamp = (date) ? [date timeIntervalSince1970] : 0;
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where date >= %.0f", @(kCrash_TableName), timestamp];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kCrash_TableName) fromDb:db error:error];
}

@end
