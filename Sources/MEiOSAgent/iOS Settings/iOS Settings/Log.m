#import <Foundation/Foundation.h>
#include <vector>
#include <string>

#include "Log.h"

int g_maxlevel = LLDEBUG;
const char* g_levels[] = {"Silent", "Error", "Warning", "Info", "Debug", "Verbose"};

@implementation Logger

+ (void)print:(int)loglevel withFormat:(NSString *)fmt, ...;
{
    int errbackup = errno;
    
    if (loglevel > g_maxlevel)
        return;
    
    va_list args_list;
    va_start (args_list, fmt);
    NSString *args = [[NSString alloc] initWithFormat:fmt arguments:args_list];
    va_end (args_list);
    
    if (loglevel == LLVERBOSE)
    {
        NSLog(@"%@", args);
        errno = errbackup;
        return;
    }
    
    NSString *type = @(g_levels[loglevel]);

    NSLog(@"[%@]: %@", type, args);
    errno = errbackup;
}

@end
