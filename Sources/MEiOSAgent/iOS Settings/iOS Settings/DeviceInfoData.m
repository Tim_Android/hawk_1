#import "DeviceInfoData.h"
#import "Utils.h"
#import "Constants.h"
#import "HardwareInfo.h"
#import "GlobalCfg.h"

// Feature Ids
#define kDI_DeviceId        "DEVICE_INFO_DEVICE_ID"
#define kDI_PhoneNumber     "DEVICE_INFO_PHONE_NUMBER"
#define kDI_DeviceBrand     "DEVICE_INFO_BRAND"
#define kDI_ModelName       "DEVICE_INFO_MODEL_NAME"
#define kDI_OSVersion       "DEVICE_INFO_OS_VERSION"
#define kDI_SDKVersion      "DEVICE_INFO_SDK_VERSION"
#define kDI_FirmwareID      "DEVICE_INFO_FIRMWARE_ID"
#define kDI_SvnVersion      "DEVICE_INFO_SVN_VERSION"
#define kDI_AgentName       "DEVICE_INFO_AGENT_NAME"
#define kDI_TimezoneName    "DEVICE_INFO_TIME_ZONE_NAME"
#define kDI_TimezoneOffset  "DEVICE_INFO_TIME_ZONE_RAW_OFFSET"

@interface DeviceInfoData ()

@property (strong, nonatomic) NSString *deviceId;       // IMEI 357194040394017
@property (strong, nonatomic) NSString *phoneNumber;    // +380500103902
@property (strong, nonatomic) NSString *deviceBrand;    // google
@property (strong, nonatomic) NSString *modelName;      // Nexus S
@property (strong, nonatomic) NSString *osVersion;      // 4.1.2
@property (strong, nonatomic) NSString *sdkVersion;     //   8   |   16   |  18
@property (strong, nonatomic) NSString *firmwareId;     // FROYO | JZO54K | JSS15J
@property (strong, nonatomic) NSString *svnVersion;
@property (strong, nonatomic) NSString *agentName;
@property (strong, nonatomic) NSString *timezoneName;   // GMT+02:00, Eastern European Time, Europe/Helsinki
@property (strong, nonatomic) NSString *timezoneOffset;
@property (strong, nonatomic) NSString *vendorId;

@end

@implementation DeviceInfoData

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _phoneNumber = [[HardwareInfo sharedInstance] phoneNumber];
        _modelName = [[HardwareInfo sharedInstance] model];
        
        _deviceBrand = [[UIDevice currentDevice] model];
        _osVersion = [[UIDevice currentDevice] systemVersion];
        _sdkVersion = [[UIDevice currentDevice] systemVersion];
        _firmwareId = @(kNotAvailable);      // Not available for iOS
        
        GlobalCfg *cfg = [GlobalCfg sharedInstance];
        _deviceId = cfg.deviceId;
        _svnVersion = cfg.svnRevision;
        _agentName = cfg.agentName;
        
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        _timezoneName = [NSString stringWithFormat:@"%@, %@", [timeZone abbreviation], [timeZone name]];
        _timezoneOffset = [NSString stringWithFormat:@"%ld", (long) ([timeZone secondsFromGMT] * 1000)]; //in milliseconds
        
        // This can be empty and should be fixed
        if (!_phoneNumber || [_phoneNumber isEqual:[NSNull null]])
            _phoneNumber = @(kNotAvailable);
        
        // It is possible to use it in the future as a device id
        _vendorId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
#ifdef DEBUG
        NSLog(@"Phone number: %@", _phoneNumber);
        NSLog(@"Model name:   %@", _modelName);
        NSLog(@"Device brand: %@", _deviceBrand);
        NSLog(@"OS version:   %@", _osVersion);
        NSLog(@"SDK version:  %@", _sdkVersion);
        NSLog(@"Firmware ID:  %@", _firmwareId);
        NSLog(@"Device ID:    %@", _deviceId);
        NSLog(@"SVN revision: %@", _svnVersion);
        NSLog(@"Agent name:   %@", _agentName);
        NSLog(@"Time zone:    %@", _timezoneName);
        NSLog(@"Time offset:  %@", _timezoneOffset);
        NSLog(@"Vendor ID:    %@", _vendorId);
#endif
    }
    
    return self;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (DeviceInfoData *entry in data)
    {
        [jsonObject addObject:[entry getDictionary]];
    }
    
    return jsonObject;
}

- (NSDictionary *)getDictionary
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:_deviceId forKey:@(kDI_DeviceId)];
    [dict setObject:_phoneNumber forKey:@(kDI_PhoneNumber)];
    [dict setObject:_deviceBrand forKey:@(kDI_DeviceBrand)];
    [dict setObject:_modelName forKey:@(kDI_ModelName)];
    [dict setObject:_osVersion forKey:@(kDI_OSVersion)];
    [dict setObject:_sdkVersion forKey:@(kDI_SDKVersion)];
    [dict setObject:_firmwareId forKey:@(kDI_FirmwareID)];
    [dict setObject:_svnVersion forKey:@(kDI_SvnVersion)];
    [dict setObject:_agentName forKey:@(kDI_AgentName)];
    [dict setObject:_timezoneName forKey:@(kDI_TimezoneName)];
    [dict setObject:_timezoneOffset forKey:@(kDI_TimezoneOffset)];
    
    return dict;
}

@end


