#import "AppIsRunningViewController.h"
#import "ComponentsFactory.h"
#import "ILoginViewController.h"
#import "IDataSender.h"
#import "ConstantsUI.h"
#import "StyleUtils.h"
#import "Utils.h"
#import "GlobalCfg.h"

#define kSegueGotoLogin @"gotoLogin"

#define kButtonTitleSendData           @"Refresh connection"
#define kButtonTitleSendDataInProgress @"Refreshing connection"

@interface AppIsRunningViewController ()
@property (nonatomic) SendDataResult sendDataResult;
@property (weak, nonatomic) id <IDataSender> dataSenderDelegate;
@end

@implementation AppIsRunningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    id <IDelegateFactory> delegateFactory = [ComponentsFactory sharedInstance];
    _dataSenderDelegate = delegateFactory.dataSenderDelegate;

    [_sendResultLabel setText:@""];
    [StyleUtils setOrangeStyleForButton:_sendDataButton];
    
    //configuring appearance of Settings button
    _settingsButton.titleLabel.font = [UIFont fontWithName:@(kDefaultFontName) size:kDefaultButtonFontSize];
    [_settingsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_settingsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];

    UIImage *settingsImage = [UIImage imageNamed:@"settings_back"];
    settingsImage = [settingsImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 26)];
    [_settingsButton setBackgroundImage:settingsImage forState:UIControlStateNormal];
    [_settingsButton setBackgroundImage:settingsImage forState:UIControlStateDisabled];
    
    //configuring appearance of Contact us button
    UIImage *contactUsImage = [UIImage imageNamed:@"rounded_rect"];
    contactUsImage = [contactUsImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)];
    [_contactUsButton setBackgroundImage:contactUsImage forState:UIControlStateNormal];
    [_contactUsButton setBackgroundImage:contactUsImage forState:UIControlStateDisabled];
    UIImage *contactUsTapImage = [UIImage imageNamed:@"rounded_rect_orange"];
    contactUsTapImage = [contactUsTapImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)];
    [_contactUsButton setBackgroundImage:contactUsTapImage forState:UIControlStateSelected];
    [_contactUsButton setBackgroundImage:contactUsTapImage forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSettingsTap:(id)sender
{
    [self performSegueWithIdentifier:kSegueGotoLogin sender:self];
}

- (IBAction)onSendDataTap:(id)sender
{
    [self startSendingDataAsync];
}

- (IBAction)onContactUsTap:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        [mailViewController setToRecipients:[NSArray arrayWithObject:[GlobalCfg sharedInstance].supportEmail]];
        
        NSString *logPath = [GlobalCfg sharedInstance].logPath;
        NSData *logData = nil;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:logPath])
        {
            logData = [[NSData alloc] initWithContentsOfFile:logPath];
            const long logSizeToSend = [GlobalCfg sharedInstance].emailLogMaxSize;
            if (logData.length > logSizeToSend)
            {
                logData = [logData subdataWithRange:NSMakeRange([logData length] - logSizeToSend, logSizeToSend)];
            }
        }
        else
        {
            logData = [[logPath stringByAppendingString:@" log file doesn't exist"] dataUsingEncoding:NSUTF8StringEncoding];
        }
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"yyyy-MM-dd_HH-mm-ss";

        NSString *attachFileName = [NSString stringWithFormat:@"Hawk_%@.log", [df stringFromDate:[NSDate date]]];
        [mailViewController addAttachmentData:logData mimeType:@"text/plain" fileName:attachFileName];
        [mailViewController setSubject:@"Hawk iOS logs"];
        [mailViewController setMessageBody:@"Dear support team,\n\n<Problem description>\nLogs are attached." isHTML:NO];

        mailViewController.mailComposeDelegate = self;
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contacting support" message:@"Please configure your device to send mail to contact support" delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:recepient@example.com"]];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueGotoLogin])
    {
        UIViewController *destinationViewController = [segue destinationViewController];
        [[destinationViewController navigationItem] setHidesBackButton:NO];
        if ([destinationViewController conformsToProtocol:@protocol(ILoginViewController)])
        {
            UIViewController <ILoginViewController> *loginController = (UIViewController <ILoginViewController> *)destinationViewController;
            [loginController setVisualState:VisualStateLogin];
        }
    }
}

#pragma mark - Sending Data

- (void)startSendingDataAsync
{
    [_sendResultLabel setText:@""];
    [self setAllControlsWaitingState:YES];
    
    NSLog(@"Manual data sending started...");
    
    [Utils startBackgroundTaskUsingBlock:^{
        _sendDataResult = SendData_ErrorInternal;
        if (_dataSenderDelegate)
        {
            _sendDataResult = [_dataSenderDelegate sendData];
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self showDataSendingResult];
            [self setAllControlsWaitingState:NO];
        });
        NSLog(@"Manual data sending finished.");
    }];
}

- (void)showDataSendingResult
{
    if (_sendDataResult == SendData_Success)
    {
        [_sendResultLabel setTextColor:[UIColor darkGrayColor]];
        [_sendResultLabel setText:@"Connection has been successfully refreshed."];
    }
    else
    {
        [_sendResultLabel setTextColor:[UIColor colorWithRed:0.76 green:0.26 blue:0.06 alpha:1.0]];
        if(_sendDataResult == SendData_ErrorInternal)
        {
            [_sendResultLabel setText:@"An error has occurred during refreshing."];
        }
        else if(_sendDataResult == SendData_ErrorInternalOnServer)
        {
            [_sendResultLabel setText:@"An error has occurred on the server."];
        }
        else if(_sendDataResult == SendData_ErrorNoInternetConnection)
        {
            [_sendResultLabel setText:@"Error: No Internet connection."];
        }
        else if(_sendDataResult == SendData_ErrorServerIsUnavailable)
        {
            [_sendResultLabel setText:@"Error: Server is unavailable."];
        }
    }
}

#pragma mark Private
#pragma mark Controls state maintenance

- (void)setAllControlsWaitingState:(BOOL)isWaiting
{
    BOOL isEnabled = !isWaiting;
    
    [_sendDataButton setTitle:(isWaiting ? kButtonTitleSendDataInProgress : kButtonTitleSendData) forState:UIControlStateDisabled];
    
    [_sendDataButton setEnabled:isEnabled];
    [_sendDataWaitIndicator setHidden:isEnabled];
    [_contactUsButton setEnabled:isEnabled];
}

#pragma mark - Clearing success/error messages when app goes into background

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)onAppWillResignActive
{
    [_sendResultLabel setText:@""];
}


@end
