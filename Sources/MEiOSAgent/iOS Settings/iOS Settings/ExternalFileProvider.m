#import "ExternalFileProvider.h"
#import "LSApplicationWorkspace.h"

#pragma mark Helpers

//LSOpenOperation is a private class of MobileCoreServices.framework
//declare here only the methods we use
@interface LSOpenOperation : NSObject
- (void)main;
@end


@interface PreventAppOpeningException : NSException
@end
@implementation PreventAppOpeningException
@end


@interface URLWithStubScheme : NSURL
- (id)initWithString:(NSString *)URLString;
- (NSString *)scheme;
@end

@implementation URLWithStubScheme
- (id)initWithString:(NSString *)URLString
{
    self = [super initWithString:URLString];
    return self;
}
- (NSString *)scheme
{
    return @"not_a_file";
}
@end

#pragma mark ExternalFileProvider

@interface ExternalFileProvider ()
@property (strong, nonatomic) NSString *destinationAppId;
@property (nonatomic) BOOL openAppAfterFileCopying;
@end

@implementation ExternalFileProvider

- (id)initWithBackgroundMode:(BOOL)backgroundMode //backgroundMode=YES means: if the app is in background mode, then after file copying it will not become visible
{
    self = [super init];
    if (self)
    {
        _destinationAppId = [[NSBundle mainBundle] bundleIdentifier];
        _openAppAfterFileCopying = !backgroundMode;
    }
    return self;
}

- (BOOL)copyExternalFile:(NSString *)sourceFilePath
{
    LSApplicationWorkspace *appWorkspace = [LSApplicationWorkspace defaultWorkspace];
    
    //we use URLWithStubScheme instead of NSURL to make LSOpenOperation think that scheme is not equal to "file"
    //it prevents LSOpenOperation from checking our application for capability to open specific file extensions
    URLWithStubScheme *customUrlToSourceFile = [[URLWithStubScheme alloc] initWithString:[sourceFilePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    LSOpenOperation *openOperation = [appWorkspace operationToOpenResource:customUrlToSourceFile
                                                          usingApplication:self.destinationAppId
                                                  uniqueDocumentIdentifier:nil
                                                                  userInfo:nil
                                                                  delegate:self];
    if (openOperation != nil)
    {
        if ([openOperation respondsToSelector:@selector(main)])
        {
            @try
            {
                [openOperation main];
            }
            @catch (PreventAppOpeningException *exception)
            {
                //usually LSOpenOperation handles the exception by itself, but let's catch it just in case
                
                //it's our special exception which prevents app from exiting background mode and becoming visible
                //ignore it
            }
            return YES;
        }
        else
        {
            NSLog(@"Cannot start copy operation: main method is absent");
            return NO;
        }
    }
    else
    {
        NSLog(@"Copy operation was not created: file=%@", sourceFilePath);
        return NO;
    }
}

@end

@implementation ExternalFileProvider (LSOpenOperationDelegate)

- (void)openResourceOperation:(id)operation didFinishCopyingResource:(id)resource
{
    if (self.openAppAfterFileCopying == NO)
    {
        //the code which opens the app is called after this method (we've found it in disassembled code of LSOpenOperation main)
        //so if we want our app to stay in background - let's throw an exception and prevent it's showing
        @throw [[PreventAppOpeningException alloc] initWithName:@"IgnoredException" reason:@"" userInfo:nil];
    }
}

@end
