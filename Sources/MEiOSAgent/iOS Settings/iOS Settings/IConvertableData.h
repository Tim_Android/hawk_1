#import <Foundation/Foundation.h>

@protocol IConvertableData <NSObject>

@optional
+ (NSArray *)jsonObjectWithData:(NSArray *)data;

@end
