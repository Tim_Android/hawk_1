#import <Foundation/Foundation.h>

#import "IConvertableData.h"

@interface DeviceInfoData : NSObject <IConvertableData>

- (NSDictionary *)getDictionary;

@end
