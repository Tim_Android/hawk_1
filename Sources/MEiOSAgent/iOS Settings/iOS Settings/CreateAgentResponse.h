#import "AuthenticationResponse.h"

@interface CreateAgentResponse : AuthenticationResponse

@property (strong, nonatomic, readonly) NSString *agentId;

- (id)initWithData:(NSData *)responseData;

@end
