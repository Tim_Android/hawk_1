#import <Foundation/Foundation.h>
#import "IContactsProvider.h"

@interface ContactsCache : NSObject <IContactsProvider>

- (void)updateWithAddressBook:(NSArray *)addressBook rebuild:(BOOL)rebuild;

@end
