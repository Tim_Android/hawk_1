#import "LocationData.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import "FMDB.h"
#import "FmdbHelper.h"

// Feature Ids
#define kLocation_Provider      "provider"
#define kLocation_Accuracy      "accuracy"
#define kLocation_Bearing       "bearing"
#define kLocation_Speed         "speed"
#define kLocation_Latitude      "latitude"
#define kLocation_Longitude     "longitude"
#define kLocation_Altitude      "altitude"
#define kLocation_Timestamp     "time"

#define kLocation_TableName     "location"

// Server requires types:
// - GPS coordinates (more accurate)
// - GSM/Wi-Fi coordinates (less accurate)

// These are values accepted by server with "provider" key
#define kProviderGps            "gps"
#define kProviderNetwork        "network"

@interface LocationData ()

@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@property (strong, nonatomic) NSNumber *altitude;
@property (strong, nonatomic) NSNumber *accuracy;
@property (strong, nonatomic) NSNumber *course;
@property (strong, nonatomic) NSNumber *speed;
@property (strong, nonatomic) NSDate *timestamp;
@property (strong, nonatomic) NSString *provider;
@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation LocationData

- (id)initWithLocation:(CLLocation *)location andHeading:(CLHeading *)heading
{
    self = [super init];
    
    if (self)
    {
        _latitude = [[NSNumber alloc] initWithDouble:location.coordinate.latitude];
        _longitude = [[NSNumber alloc] initWithDouble:location.coordinate.longitude];
        _altitude = [[NSNumber alloc] initWithDouble:location.altitude];
        _accuracy = [[NSNumber alloc] initWithDouble:location.horizontalAccuracy];
        _timestamp = location.timestamp;
        _provider = @(kProviderGps);
        
//        _course = [[NSNumber alloc] initWithDouble:location.course];
//        _speed = [[NSNumber alloc] initWithDouble:location.speed];
        
// !!!: This can calculate speed from distance, but it is a bad way. Find possibility to make CoreLocation work properly.
        if (!heading)
            _course = [[NSNumber alloc] initWithDouble:location.course];
        else
            _course = [[NSNumber alloc] initWithDouble:(location.course < 0) ? heading.magneticHeading : location.course];
        
        static CLLocation *previousLocation = nil;
        static NSDate *previousTimestamp = nil;
        
        if (!previousLocation)
            _speed = @(-1);
        else
        {
            CLLocationDistance distance = [location distanceFromLocation:previousLocation];
            NSTimeInterval time = [location.timestamp timeIntervalSinceDate:previousTimestamp];
            _speed = [[NSNumber alloc] initWithDouble:(distance/time)];
        }
        
        previousLocation = location;
        previousTimestamp = location.timestamp;
    }
    
    return self;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (LocationData *entry in data)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:entry.latitude forKey:@(kLocation_Latitude)];
        [dict setObject:entry.longitude forKey:@(kLocation_Longitude)];
        [dict setObject:entry.altitude forKey:@(kLocation_Altitude)];
        [dict setObject:entry.accuracy forKey:@(kLocation_Accuracy)];
        [dict setObject:entry.course forKey:@(kLocation_Bearing)];
        [dict setObject:entry.provider forKey:@(kLocation_Provider)];
        [dict setObject:entry.speed forKey:@(kLocation_Speed)];
        [dict setObject:[NSString stringWithFormat:@"/Date(%.0f)/", [entry.timestamp timeIntervalSince1970]*1000] forKey:@(kLocation_Timestamp)]; // in milliseconds
        
        [jsonObject addObject:dict];
    }
    
    return jsonObject;
}

#pragma mark -
#pragma mark Local Storage

- (id)initWithLocalDbEntry:(NSDictionary *)entry
{
    self = [super init];
    
    if (self)
    {
        _latitude = [entry objectForKey:@(kLocation_Latitude)];
        _longitude = [entry objectForKey:@(kLocation_Longitude)];
        _altitude = [entry objectForKey:@(kLocation_Altitude)];
        _accuracy = [entry objectForKey:@(kLocation_Accuracy)];
        _course = [entry objectForKey:@(kLocation_Bearing)];
        _speed = [entry objectForKey:@(kLocation_Speed)];
        _timestamp = [NSDate dateWithTimeIntervalSince1970:[[entry objectForKey:@(kLocation_Timestamp)] doubleValue]];
        _provider = [entry objectForKey:@(kLocation_Provider)];
        _localStorageId = [entry objectForKey:@"rowid"];
    }
    
    return self;
}

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSString *query = (date)
    ? [[NSString alloc] initWithFormat:@"select rowid, * from %@ where time >= %.0f order by rowid asc;", @(kLocation_TableName), [date timeIntervalSince1970]]
    : [[NSString alloc] initWithFormat:@"select rowid, * from %@ order by rowid asc;", @(kLocation_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        LocationData *data = [[LocationData alloc] initWithLocalDbEntry:rslt];
        [entries addObject:data];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (LocationData* entry in data)
    {
        [statements appendFormat:
         @"insert into %@ values (%f, %f, %f, %f, %f, '%@', %f, %.0f);",
         @(kLocation_TableName),
         [entry.latitude doubleValue],
         [entry.longitude doubleValue],
         [entry.altitude doubleValue],
         [entry.accuracy doubleValue],
         [entry.course doubleValue],
         entry.provider,
         [entry.speed doubleValue],
         [entry.timestamp timeIntervalSince1970]];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    NSString *query = nil;
    
    if (dropIfExists)
    {
        query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kLocation_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    query = [[NSString alloc] initWithFormat:
             @"create table if not exists %@ (%@ real, %@ real, %@ real, %@ real, %@ real, %@ text, %@ real, %@ integer)",
             @(kLocation_TableName),
             @(kLocation_Latitude),
             @(kLocation_Longitude),
             @(kLocation_Altitude),
             @(kLocation_Accuracy),
             @(kLocation_Bearing),
             @(kLocation_Provider),
             @(kLocation_Speed),
             @(kLocation_Timestamp)];
    
    isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    return isOk;
}

+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    NSTimeInterval timestamp = (date) ? [date timeIntervalSince1970] : 0;
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where time >= %.0f", @(kLocation_TableName), timestamp];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kLocation_TableName) fromDb:db error:error];
}

@end
