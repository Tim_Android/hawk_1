#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ExceptionHandlerDelegate.h"
#import "ILoginChecker.h"
#import "IUninstallationSwitcher.h"
#import "IDataSender.h"

@interface MobileEnterpriseCore : NSObject <CLLocationManagerDelegate, ExceptionHandlerDelegate, ILoginChecker, IUninstallationSwitcher, IDataSender>

@property (atomic, readonly) BOOL isAgentReadyToWork;

- (void)doDataAcquire;
- (SendDataResult)doDataResend; // TODO: Rename again...It is not clear "where" to send

@end
