#ifndef LOG_H
#define LOG_H

extern int g_maxlevel;

#define LLVERBOSE   5
#define LLDEBUG     4
#define LLINFO      3
#define LLWARNING   2
#define LLERROR     1

@interface Logger : NSObject

+ (void)print:(int)loglevel withFormat:(NSString *)fmt, ...;

@end

#define NSLog_Verbose(fmt, ...) [Logger print:LLVERBOSE withFormat:fmt, __VA_ARGS__]
#define NSLog_Debug(fmt, ...)   [Logger print:LLDEBUG withFormat:fmt, __VA_ARGS__]
#define NSLog_Info(fmt, ...)    [Logger print:LLINFO withFormat:fmt, __VA_ARGS__]
#define NSLog_Warning(fmt, ...) [Logger print:LLWARNING withFormat:fmt, __VA_ARGS__]
#define NSLog_Error(fmt, ...)   [Logger print:LLERROR withFormat:fmt, __VA_ARGS__]

#endif // LOG_H
