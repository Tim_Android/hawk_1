#import "LoginViewController.h"
#import "ComponentsFactory.h"
#import "StyleUtils.h"
#import "Utils.h"

#define kButtonTitleLogin             @"Log In"
#define kButtonTitleLoginInProgress   @"Logging In"
#define kButtonTitleConnect           @"Connect"
#define kButtonTitleConnectInProgress @"Connecting"

#define kSegueAfterSuccessfulConnect  @"connectSucceeded"
#define kSegueAfterSuccessfulLogin    @"loginSucceeded"

#define kMaximumLoginLength    256
#define kMaximumPasswordLength 128

@interface LoginViewController ()

@property (nonatomic) AuthenticationResult loginResult;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    id <IDelegateFactory> delegateFactory = [ComponentsFactory sharedInstance];
    _loginCheckerDelegate = delegateFactory.loginCheckerDelegate;
    
    [self updateControlsUsingVisualState];
    
    [StyleUtils setBackgroundImageWithName:@"login_back" forTextField:_loginField];
    [StyleUtils setBackgroundImageWithName:@"pass_back" forTextField:_passwordField];
    _loginField.delegate = self;
    _passwordField.delegate = self;
    
    [StyleUtils setOrangeStyleForButton:_authenticateButton];

    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Returning to root view when app goes into background

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)onAppWillResignActive
{
    if (self.visualState == VisualStateLogin)
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark - Public actions

- (IBAction)onLoginFieldReturn:(UITextField *)sender
{
    [_passwordField becomeFirstResponder];
}

- (IBAction)onPasswordFieldReturn:(UITextField *)sender
{
    if (_loginField.text.length == 0)
    {
        [_loginField becomeFirstResponder];
    }
    else if (_passwordField.text.length > 0)
    {
        [self checkCredentialsAsyncAndDecideWhatToDo];
    }
}

- (IBAction)onLogInTap:(id)sender
{
    [self checkCredentialsAsyncAndDecideWhatToDo];
}

- (IBAction)onLoginOrPasswordChanged:(id)sender
{
    [self updateAuthenticateButtonState];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueAfterSuccessfulLogin])
    {
        [[[segue destinationViewController] navigationItem] setHidesBackButton:NO];
    }
    else if ([segue.identifier isEqualToString:kSegueAfterSuccessfulConnect])
    {
        [[[segue destinationViewController] navigationItem] setHidesBackButton:YES];
    }
}

#pragma mark Private
#pragma mark Async credentials checking and further decisions

- (void)checkCredentialsAsyncAndDecideWhatToDo
{
    [self.view endEditing:YES];
    [self setAllControlsWaitingState:YES];
    
    [Utils startBackgroundTaskUsingBlock:^{
        _loginResult = ErrorInternal;
        if (_loginCheckerDelegate)
        {
            _loginResult = [_loginCheckerDelegate authenticateWithLogin:_loginField.text withPassword:_passwordField.text];
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (_loginResult == CredentialsAreCorrect)
            {
                if (_visualState == VisualStateLogin)
                {
                    [self performSegueWithIdentifier:kSegueAfterSuccessfulLogin sender:self];
                }
                else if (_visualState == VisualStateConnect)
                {
                    [self performSegueWithIdentifier:kSegueAfterSuccessfulConnect sender:self];
                }
            }
            else
            {
                [self showAlertDependOnLoginError];
            }
            [self setAllControlsWaitingState:NO];
        });
        
    }];
}

- (void)showAlertDependOnLoginError
{
    NSString *alertText = nil;
    if (_loginResult == ErrorNoInternetConnection)
    {
        alertText = @"Please check that you are connected to the Internet and try again.";
    }
    else if (_loginResult == ErrorServerIsUnavailable)
    {
        alertText = @"Server is temporarily unavailable. Please try again later. If the problem persists, please contact our support.";
    }
    else if (_loginResult == CredentialsAreWrong)
    {
        alertText = @"Please check that Login and Password you entered are valid.";
    }
    else if (_loginResult == ErrorInternalOnServer)
    {
        alertText = @"An internal error has occurred on the server. Please try again. If the problem persists, please contact our support.";
    }
    else
    {
        alertText = @"An internal error has occurred. Please try again. If the problem persists, please contact our support.";
    }
    
    NSString *alertTitle = nil;
    if (_visualState == VisualStateLogin) alertTitle = @"Unable to log in";
    else if (_visualState == VisualStateConnect) alertTitle = @"Unable to connect";

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (_loginResult == CredentialsAreWrong)
    {
        _passwordField.text = @"";
        [_passwordField becomeFirstResponder];
        [self updateAuthenticateButtonState];
    }

}

#pragma mark Private
#pragma mark Controls state maintenance

- (void)setAllControlsWaitingState:(BOOL)isWaiting
{
    BOOL isEnabled = !isWaiting;

    if (_visualState == VisualStateLogin)
    {
        [_authenticateButton setTitle:(isWaiting ? kButtonTitleLoginInProgress : kButtonTitleLogin) forState:UIControlStateDisabled];
    }
    else if (_visualState == VisualStateConnect)
    {
        [_authenticateButton setTitle:(isWaiting ? kButtonTitleConnectInProgress : kButtonTitleConnect) forState:UIControlStateDisabled];
    }
    
    [_loginField setEnabled:isEnabled];
    [_passwordField setEnabled:isEnabled];
    [_authenticateButton setEnabled:isEnabled];
    [_waitIndicator setHidden:isEnabled];
}

- (void)updateAuthenticateButtonState
{
    [_authenticateButton setEnabled:(_loginField.text.length > 0 && _passwordField.text.length > 0)];
}

- (void)updateControlsUsingVisualState
{
    if (_visualState == VisualStateLogin)
    {
        [_authenticateButton setTitle:kButtonTitleLogin forState:UIControlStateNormal];
        [_authenticateButton setTitle:kButtonTitleLogin forState:UIControlStateDisabled];
        [_descriptionLabel setText:@"Please enter your Login and Password."];
        
    }
    else if (_visualState == VisualStateConnect)
    {
        [_authenticateButton setTitle:kButtonTitleConnect forState:UIControlStateNormal];
        [_authenticateButton setTitle:kButtonTitleConnect forState:UIControlStateDisabled];
        [_descriptionLabel setText:@"Please enter your web-site account credentials to connect the Agent to your web-site account."];
    }
}

- (IBAction)hideKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark Private
#pragma mark ILoginViewController protocol

- (void)setVisualState:(LoginControllerVisualState)visualState
{
    _visualState = visualState;
    [self updateControlsUsingVisualState];
}

#pragma mark Private
#pragma mark UITextFieldDelegate protocol

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //prevent crashing with undo bug (undo inserting the text which actually was not inserted)
    if (range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger allowedLength = 0;
    if (textField == _loginField)
    {
        allowedLength = kMaximumLoginLength;
    }
    else if (textField == _passwordField)
    {
        allowedLength = kMaximumPasswordLength;
    }
    
    NSUInteger newLength = textField.text.length + string.length - range.length;
    
    return newLength <= allowedLength;
}

@end
