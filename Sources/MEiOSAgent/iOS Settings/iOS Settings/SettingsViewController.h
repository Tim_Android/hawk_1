#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *restartDeviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UISwitch *allowUninstallationSwitch;

- (IBAction)onAllowUninstallationChanged:(UISwitch *)sender;

@end
