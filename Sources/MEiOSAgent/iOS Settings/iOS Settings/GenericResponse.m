#import "GenericResponse.h"

@implementation GenericResponse

- (id)initWithData:(NSData *)responseData
{
    self = [super init];
    if (self)
    {
        _responseDictionary = nil;
        
        if (responseData == nil)
        {
            return nil;
        }
        
        NSError *err = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];
        
        if (err != nil)
        {
            NSLog(@"GenericResponse converting to JSON error, code=%li, desc=%@", (long)err.code, err.description);
            return nil;
        }
        else if (response == nil)
        {
            NSLog(@"GenericResponse converting to JSON error, err=nil, dictionary=nil");
            return nil;
        }
        else
        {
            _responseDictionary = response;
        }
        
    }
    return self;
}
@end
