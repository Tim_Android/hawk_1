#ifndef CONSTANTS_H
#define CONSTANTS_H

#define kServiceUrl         "service_url"
#define kAgentName          "agent_name"
#define kSvnRevision        "svn_revision"
#define kLogLevel           "log_level"

#define kKeepAliveTimeout       600 // seconds
#define kUrlRequestTimeout      30  // seconds
#define kBackgroundExecLimit    10  // seconds

#define kLogFileName          "ios_settings.log"
#define kConfigName           "config.json"
#define kLocalStorageName     "local_storage.sqlitedb"
#define kLocalStorageMaxSz    "ls_max_sz"
#define kCrashTestEnabled     "crash_test_enabled"
#define kKeySupportEmail      "support_email"
#define kKeyMaxEmailLogSize   "email_log_max_sz_kb"
#define kTimestampTemplate    "yyMMdd_HHmmss"
#define kCountryCodesFileName "phoneCountryCodes.json"

#define kNotAvailable       "N/A"

//#define kMEServiceUrl        "https://win-hawk-vm:89/MobileEnterpriseService.svc"
//#define kMEServiceUrl        "https://hawk-test:8000/MobileEnterpriseService.svc"
//#define kMEServiceUrl        "http://polozov-pc"

#define kCallHistoryDbUrl       "file:///private/var/wireless/Library/CallHistory/call_history.db"
#define kCallHistoryDbWalUrl    "file:///private/var/wireless/Library/CallHistory/call_history.db-wal"
#define kCallHistoryDbUrl8x     "file:///private/var/mobile/Library/CallHistoryDB/CallHistory.storedata"
#define kCallHistoryDbWalUrl8x  "file:///private/var/mobile/Library/CallHistoryDB/CallHistory.storedata-wal"
#define kSmsDbUrl               "file:///private/var/mobile/Library/SMS/sms.db"
#define kSmsDbWalUrl            "file:///private/var/mobile/Library/SMS/sms.db-wal"
#define kAddressBookDbUrl       "file:///private/var/mobile/Library/AddressBook/AddressBook.sqlitedb"
#define kAddressBookDbWalUrl    "file:///private/var/mobile/Library/AddressBook/AddressBook.sqlitedb-wal"

// Features
#define kFeature_DeviceInfo         "deviceInfo"
#define kFeature_CrashReport        "crashLog"
#define kFeature_LocationHistory    "gpsLog"
#define kFeature_CallHistory        "callLog"
#define kFeature_SmsHistory         "smsLog"
#define kFeature_MmsHistory         "mmsLog"

// IIS has a limit for incoming data. We should keep it in mind or enjoy 413 error
#define kJsonMaxTransferableSize    (128*1024)
#define kBinMaxTransferableSize     (2.5*1024*1024)

// User defaults
#define kLastResendTimeKey      "lastResendTime"
#define kLastAcquisitionTimeKey "lastSnapshotTime"
#define kMaxAcquiredCallId      "lastCallId"
#define kIsFirstLaunchKey       "isFirstLaunch"

#define kUEH_SignalExceptionName    "UEH_SignalExceptionName"
#define kUEH_SignalKey              "UEH_SignalKey"
#define kUEH_StacktraceKey          "UEH_StacktraceKey"

#endif // CONSTANTS_H
