#import <UIKit/UIKit.h>

@interface SuccessViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *okButton;

- (IBAction)OnOkTap:(id)sender;

@end
