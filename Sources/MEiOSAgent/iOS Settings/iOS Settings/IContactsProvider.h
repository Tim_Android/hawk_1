#import <Foundation/Foundation.h>

@class ContactData;

@protocol IContactsProvider <NSObject>

- (ContactData *)searchForNumber:(NSString *)phoneNumber;

@end
