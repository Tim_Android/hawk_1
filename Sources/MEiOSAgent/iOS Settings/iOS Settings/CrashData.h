#import <Foundation/Foundation.h>

#import "IPersistableData.h"
#import "IConvertableData.h"

@interface CrashData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic, readonly) NSString *text;
@property (strong, nonatomic, readonly) NSDate *date;

- (id)initWithException:(NSException *)ex;
- (id)initWithException:(NSException *)ex textLog:(NSString *)textLog;

@end
