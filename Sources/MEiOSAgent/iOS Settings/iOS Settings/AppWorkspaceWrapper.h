#import <Foundation/Foundation.h>

@interface AppWorkspaceWrapper : NSObject

+ (BOOL)setUninstallationForBundleId:(NSString *)bundleId enabled:(BOOL)enabled;
+ (BOOL)isUninstallationEnabledForBundleId:(NSString *)bundleId;

@end
