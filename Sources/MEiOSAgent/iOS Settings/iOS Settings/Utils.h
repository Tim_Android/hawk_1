#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface Utils : NSObject

+ (BOOL)isCydiaMode;
+ (NSURL *)getUrlForDocumentsDirectory;
+ (NSString *)getPathForDocumentsDirectory;
+ (NSString *)getPathForInboxDirectory;
+ (NSString *)getPathForExecutable;
+ (BOOL)isHostReachable:(NSString *)hostname;

+ (NSArray *)chunkBigJsonObject:(NSArray *)jsonObj onChunksNum:(NSUInteger)chunksNum;

+ (NSString *)encodeToBase64:(NSString *)string;
+ (NSString *)decodeFromBase64:(NSString *)string;

+ (BOOL)redirectStderrToFileAtPath:(NSString *)filePath;
+ (NSArray *)getStack;

+ (void)startBackgroundTaskUsingBlock:(void (^)())block;
+ (NSString *)getTimestamp;
+ (NSString *)getTimestampFromDate:(NSDate*)date;

@end
