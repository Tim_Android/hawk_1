#import "MmsAttachmentInfoData.h"
#import "Constants.h"
#import "FMDB.h"
#import "FmdbHelper.h"
#import "Utils.h"
#import "GlobalCfg.h"

#define kMmsAttachment_Path     "attachment_path"
#define kMmsAttachment_Type     "type"
#define kMmsAttachment_Id       "_id"
#define kMmsAttachment_MsgId    "message_id"

// Local storage table
#define kAttachmentLocalStorage_TableName "attachments"
#define kAttachmentLocalStorage_Info "attachment_info"

@interface MmsAttachmentInfoData ()

@property (strong, nonatomic) NSString *attachmentPath;
@property (strong, nonatomic) NSString *attachmentType;
@property (strong, nonatomic) NSNumber *attachmentId;
@property (strong, nonatomic) NSNumber *messageId;
@property (strong, nonatomic) NSNumber *localStorageId;

@end

@implementation MmsAttachmentInfoData

- (id)initWithInternalDbEntry:(NSDictionary *)entry andMesssageId:(NSNumber *)messageId
{
    self = [super init];
    
    if (self)
    {
        _attachmentPath = [entry objectForKey:@"filename"];
        _attachmentType = [entry objectForKey:@"mime_type"];
        _attachmentId = [entry objectForKey:@"ROWID"];
        _messageId = messageId;
    }
    
    return self;
}

+ (NSArray *)loadAttachmentsFromDb:(NSString *)dbPath forMessageId:(NSNumber *)messageId
{
    NSError *error = nil;
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSString *query = [NSString stringWithFormat:
                       @"SELECT a.rowid, a.mime_type, a.filename FROM message_attachment_join maj, attachment a "
                       "WHERE maj.message_id = %lu AND maj.attachment_id = a.rowid ORDER BY a.rowid ASC", [messageId longValue]];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    BOOL isOk = [FmdbHelper executeQuery:query db:db error:&error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        id obj = [[MmsAttachmentInfoData alloc] initWithInternalDbEntry:rslt andMesssageId:messageId];
        [entries addObject:obj];
    }];
    
    if (!isOk)
        return nil;
    
    return ([entries count]) ? entries : nil;
}

#pragma mark -
#pragma mark JSON Serialization

+ (NSArray *)jsonObjectWithData:(NSArray *)data
{
    NSMutableArray *jsonObject = [[NSMutableArray alloc] init];
    
    for (MmsAttachmentInfoData *entry in data)
    {
        NSMutableDictionary *attachmentInfo = [[NSMutableDictionary alloc] init];
        [attachmentInfo setObject:entry.attachmentType forKey:@(kMmsAttachment_Type)];
        [attachmentInfo setObject:entry.attachmentId forKey:@(kMmsAttachment_Id)];
        [jsonObject addObject:attachmentInfo];
    }
    
    return jsonObject;
}

+ (NSString *)stringFromAttachmentsArray:(NSArray *)attachments
{
    NSMutableString *string = [[NSMutableString alloc] init];
    
    for (MmsAttachmentInfoData *entry in attachments) {
        [string appendFormat:@"%@;", [MmsAttachmentInfoData stringFromAttachment:entry]];
    }
    
    return string;
}

+ (NSArray *)attachmentsArrayFromString:(NSString *)string
{
    NSMutableArray *attachments = [[NSMutableArray alloc] init];
    for (NSString *entry in [string componentsSeparatedByString:@";"])
    {
        if ([entry length] != 0)
        {
            [attachments addObject:[MmsAttachmentInfoData attachmentFromString:entry]];
        }
    }
    return ([attachments count]) ? attachments : nil;
}

+ (NSString *)stringFromAttachment:(MmsAttachmentInfoData *)attachmentInfo
{
    return [NSString stringWithFormat:@"%@:%@:%@:%@", attachmentInfo.attachmentPath, attachmentInfo.attachmentType, attachmentInfo.attachmentId, attachmentInfo.messageId];
}

+ (MmsAttachmentInfoData *)attachmentFromString:(NSString *)string
{
    NSArray *components = [string componentsSeparatedByString:@":"];
    MmsAttachmentInfoData *obj = [[MmsAttachmentInfoData alloc] init];
    obj.attachmentPath = [components objectAtIndex:0];
    obj.attachmentType = [components objectAtIndex:1];
    obj.attachmentId = [NSNumber numberWithInteger:[[components objectAtIndex:2] integerValue]];
    obj.messageId = [NSNumber numberWithInteger:[[components objectAtIndex:3] integerValue]];
    return obj;
}

#pragma mark -
#pragma mark Local Storage

+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error
{
    //the date argument is ignored, it exists only for compatibility with IPersistableData protocol
    NSString *query = [[NSString alloc] initWithFormat:@"select rowid, attachment_info from %@ order by rowid asc;", @(kAttachmentLocalStorage_TableName)];
    
    __block NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    BOOL isOk = YES;
    
    isOk &= [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop)
    {
        MmsAttachmentInfoData *data = [MmsAttachmentInfoData attachmentFromString:[rslt objectForKey:@(kAttachmentLocalStorage_Info)]];
        data.localStorageId = [rslt objectForKey:@"rowid"];
        [entries addObject:data];
    }];
    
    if (!isOk)
    {
        return nil;
    }
    
    return ([entries count]) ? entries : nil;
}

+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error
{
    NSMutableString *statements = [[NSMutableString alloc] init];
    
    for (MmsAttachmentInfoData *entry in data)
    {
        [statements appendFormat:
         @"insert into %@ values ('%@');",
         @(kAttachmentLocalStorage_TableName),
         [MmsAttachmentInfoData stringFromAttachment:entry]];
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    return [FmdbHelper executeStatements:statements db:db error:error];
}

+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    BOOL isOk = YES;
    NSString *query = nil;
    
    if (dropIfExists)
    {
        query = [[NSString alloc] initWithFormat:@"drop table if exists %@", @(kAttachmentLocalStorage_TableName)];
        isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    }
    
    query = [[NSString alloc] initWithFormat:
             @"create table if not exists %@ (%@ text)",
             @(kAttachmentLocalStorage_TableName),
             @(kAttachmentLocalStorage_Info)];
    
    isOk &= [FmdbHelper executeUpdate:query db:db error:error];
    return isOk;
}

+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    NSArray *idsArray = [itemsToErase valueForKey:NSStringFromSelector(@selector(localStorageId))];
    return [FmdbHelper deleteItemsWithIds:idsArray fromTable:@(kAttachmentLocalStorage_TableName) fromDb:db error:error];
}

@end
