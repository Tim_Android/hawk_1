#import <Foundation/Foundation.h>

// TODO: Rename to GlobalAppConfig
@interface GlobalCfg : NSObject

@property (strong, nonatomic, readonly) NSString *docsDir;

@property (strong, nonatomic, readonly) NSURL* serviceUrl;
@property (strong, nonatomic, readonly) NSString *agentName;
@property (strong, nonatomic, readonly) NSString *svnRevision;
@property (strong, nonatomic, readonly) NSString *deviceId;
@property (strong, nonatomic, readonly) NSString *phoneNumber;

@property (strong, nonatomic, readonly) NSString *smsDbPath;
@property (strong, nonatomic, readonly) NSString *smsDbWalPath;
@property (strong, nonatomic, readonly) NSString *callsDbPath;
@property (strong, nonatomic, readonly) NSString *callsDbWalPath;
@property (strong, nonatomic, readonly) NSString *contactsDbPath;
@property (strong, nonatomic, readonly) NSString *contactsDbWalPath;
@property (strong, nonatomic, readonly) NSString *callsDbPath8x;
@property (strong, nonatomic, readonly) NSString *callsDbWalPath8x;

@property (strong, nonatomic, readonly) NSString *tempSmsDbPath;
@property (strong, nonatomic, readonly) NSString *tempSmsDbWalPath;
@property (strong, nonatomic, readonly) NSString *tempCallsDbPath;
@property (strong, nonatomic, readonly) NSString *tempCallsDbWalPath;
@property (strong, nonatomic, readonly) NSString *tempContactsDbPath;
@property (strong, nonatomic, readonly) NSString *tempContactsDbWalPath;
@property (strong, nonatomic, readonly) NSString *tempCallsDbPath8x;
@property (strong, nonatomic, readonly) NSString *tempCallsDbWalPath8x;

@property (strong, nonatomic, readonly) NSString *localStorageDbPath;
@property (nonatomic, readonly) long long localStorageMaxSz;

@property (nonatomic, readonly) BOOL isFirstLaunch;
@property (nonatomic, readonly) BOOL isCrashTestEnabled;

@property (strong, nonatomic, readonly) NSCharacterSet *chsetToIncludeInPhoneNumber;
@property (strong, nonatomic, readonly) NSCharacterSet *chsetToExcludeFromPhoneType;
@property (strong, nonatomic, readonly) NSCharacterSet *chsetToExcludeFromMmsBody;

@property (strong, nonatomic, readonly) NSString *logPath;
@property (strong, nonatomic, readonly) NSNumber *logLevel;

@property (strong, nonatomic, readonly) NSString *supportEmail;
@property (nonatomic, readonly) long emailLogMaxSize;

+ (GlobalCfg *)sharedInstance;

+ (NSDate *)getLastResendDate;
+ (void)setLastResendDate:(NSDate *)date;
+ (NSDate *)getLastAcquisitionDate;
+ (void)setLastAcquisitionDate:(NSDate *)date;
+ (NSNumber *)getMaxAcquiredCallId;
+ (void)setMaxAcquiredCallId:(NSNumber *)callId;

@end
