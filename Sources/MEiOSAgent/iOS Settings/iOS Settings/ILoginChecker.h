#import <Foundation/Foundation.h>

typedef enum
{
    ErrorInternal             = 0,
    ErrorNoInternetConnection = 1,
    ErrorServerIsUnavailable  = 2,
    ErrorInternalOnServer     = 3,
    CredentialsAreWrong       = 4,
    CredentialsAreCorrect     = 5,
} AuthenticationResult;

@protocol ILoginChecker <NSObject>

- (AuthenticationResult)authenticateWithLogin:(NSString *)login withPassword:(NSString *)password;

@end