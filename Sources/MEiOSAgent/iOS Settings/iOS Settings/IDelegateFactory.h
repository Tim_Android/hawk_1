#import <Foundation/Foundation.h>

@protocol ILoginChecker;
@protocol IUninstallationSwitcher;
@protocol IDataSender;

@protocol IDelegateFactory <NSObject>

@property (strong, nonatomic, readonly) id <ILoginChecker> loginCheckerDelegate;
@property (strong, nonatomic, readonly) id <IUninstallationSwitcher> uninstallationSwitcherDelegate;
@property (strong, nonatomic, readonly) id <IDataSender> dataSenderDelegate;

@end