#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *m_label;

- (IBAction)onSendDeviceData:(id)sender;
- (IBAction)onPingHawkServer:(id)sender;
- (IBAction)onPreventUninstallation:(id)sender;
- (IBAction)onAllowUninstallation:(id)sender;

@end
