#import <Foundation/Foundation.h>

@protocol IPersistableData <NSObject>

@optional
+ (BOOL)createTableInDb:(NSString *)dbPath dropIfExists:(BOOL)dropIfExists error:(NSError **)error;
+ (BOOL)updateTableInDb:(NSString *)dbPath fromVersion0To1Error:(NSError **)error;
+ (NSArray *)loadFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error;
+ (BOOL)saveIntoDb:(NSString *)dbPath data:(NSArray *)data error:(NSError **)error;
+ (BOOL)eraseFromDb:(NSString *)dbPath sinceDate:(NSDate *)date error:(NSError **)error;
+ (BOOL)eraseFromDb:(NSString *)dbPath items:(NSArray *)itemsToErase error:(NSError **)error;

@end
