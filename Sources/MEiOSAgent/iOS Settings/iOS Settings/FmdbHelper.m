#import "FmdbHelper.h"
#import "FMDB.h"

@implementation FmdbHelper

+ (BOOL)executeUpdate:(NSString *)query db:(FMDatabase*)db error:(NSError **)error;
{
    if ([db open])
    {
        if ([db executeUpdate:query])
            return YES;
    }
    
    if (error)
        *error = [db lastError];
    
    return NO;
}

+ (BOOL)executeStatements:(NSString*)query db:(FMDatabase *)db error:(NSError **)error
{
    if ([db open])
    {
        if ([db executeStatements:query])
            return YES;
    }
    
    if (error)
        *error = [db lastError];
    
    return NO;
}

+ (BOOL)executeQuery:(NSString *)query db:(FMDatabase *)db error:(NSError **)error usingBlock:(void (^)(NSDictionary *rslt, BOOL *stop))block
{
    if ([db open])
    {
        FMResultSet *set = [db executeQuery:query];
        
        if (set)
        {
            while ([set next])
            {
                BOOL stop = NO;
                block([set resultDictionary], &stop);
                
                if (stop)
                    break;
            }
            
            return YES;
        }
    }
    
    if (error)
        *error = [db lastError];
    
    return NO;
}

+ (BOOL)deleteItemsWithIds:(NSArray *)idsToDelete fromTable:(NSString *)tableName fromDb:(FMDatabase *)db error:(NSError **)error
{
    if (!idsToDelete || ![idsToDelete count])
    {
        return YES;
    }
    NSString *stringWithIds = [idsToDelete  componentsJoinedByString:@","];
    NSString *query = [[NSString alloc] initWithFormat:@"delete from %@ where rowid in (%@)", tableName, stringWithIds];
    return [FmdbHelper executeUpdate:query db:db error:error];
}

+ (BOOL)isColumnExist:(NSString *)columnName inTable:(NSString *)tableName inDb:(FMDatabase *)db error:(NSError **)error
{
    __block BOOL isExist = NO;
    NSString *statement = [[NSString alloc] initWithFormat:@"PRAGMA table_info(%@);", tableName];
    [FmdbHelper executeQuery:statement db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        id currentColumnName = [rslt objectForKey:@"name"];
        if (currentColumnName && [currentColumnName isKindOfClass:[NSString class]])
        {
            if ([currentColumnName isEqualToString:columnName])
            {
                isExist = YES;
                *stop = YES;
            }
        }
        else
        {
            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            [details setObject:@"column name is nil or not a string" forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"none" code:999 userInfo:details];
            *stop = YES;
        }
    }];
    return isExist;
}

+ (NSNumber *)maxIdFromTableName:(NSString *)tableName inDb:(FMDatabase *)db error:(NSError **)error
{
    NSString *query = nil;
    query = [[NSString alloc] initWithFormat:@"SELECT MAX(rowid) AS maxId FROM %@;", tableName];
    
    __block NSNumber *maxId = 0;
    
    BOOL isOk = [FmdbHelper executeQuery:query db:db error:error usingBlock:^(NSDictionary *rslt, BOOL *stop) {
        maxId = [NSNumber numberWithInteger:[[rslt objectForKey:@"maxId"] integerValue]];
        *stop = YES;
    }];
    
    return (isOk ? maxId : 0);
}

+ (int)versionOfDb:(FMDatabase*)db
{
    __block int resultVersion = 0;
    [FmdbHelper executeQuery:@"PRAGMA user_version"
                          db:db
                       error:NULL
                  usingBlock:^(NSDictionary *rslt, BOOL *stop)
                             {
                                 resultVersion = [rslt[@"user_version"] intValue];
                             }];
    
    return resultVersion;
}

+ (void)setVersion:(int)version forDb:(FMDatabase*)db
{
    [FmdbHelper executeUpdate: [NSString stringWithFormat: @"PRAGMA user_version=%d", version] db:db error:NULL];
}

+ (BOOL)doesTableExist:(NSString*)tableName inDb:(FMDatabase*)db
{
    __block BOOL exists = NO;
    [FmdbHelper executeQuery:[NSString stringWithFormat: @"SELECT * FROM sqlite_master WHERE name ='%@' and type='table'", tableName]
                          db:db
                       error:NULL
                  usingBlock:^(NSDictionary *rslt, BOOL *stop)
                             {
                                 exists = YES;
                             }];
    
    return exists;
}

@end
