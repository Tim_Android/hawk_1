#import "MainNavigationController.h"
#import "ConstantsUI.h"

@interface MainNavigationController ()

@end

@implementation MainNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //configuring navigation bar style
    //background image
    UIImage *navBarImage = [UIImage imageNamed:@"navbar"];
    navBarImage = [navBarImage resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 20.0, 8.0, 20.0)];
    [self.navigationBar setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage new]]; //hide shadow under bar
    
    //navigation bar title font
    NSDictionary *titleFontAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor blackColor], UITextAttributeTextColor,
                                    [UIColor clearColor], UITextAttributeTextShadowColor,
                                    [UIFont fontWithName:@(kDefaultFontName) size:kDefaultNavBarFontSize], UITextAttributeFont,
                                    nil];
    [self.navigationBar setTitleTextAttributes:titleFontAttributes];
    
    //back button arrow color
    [self.navigationBar setTintColor:[UIColor darkGrayColor]];
    
    [self.navigationBar setNeedsLayout]; //crutch to place title into horizontal center on iOS 6 (the bug reproduces only on the first view)
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
