#import <Foundation/Foundation.h>

#import "IPersistableData.h"
#import "IConvertableData.h"
#import "IContactsProvider.h"

@interface MmsData : NSObject <IPersistableData, IConvertableData>

@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *contactName;
@property (strong, nonatomic) NSDate *timestamp;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *messageType;
@property (strong, nonatomic) NSString *messageSubject;
@property (strong, nonatomic) NSNumber *messageId;
@property (strong, nonatomic) NSArray *attachments;

+ (NSArray *)loadFromInternalDb:(NSString *)dbPath sinceDate:(NSDate *)date withContactsProvider:(id<IContactsProvider>)contactsProvider error:(NSError **)error;

@end
