#import <Foundation/Foundation.h>

typedef enum
{
    VisualStateLogin   = 0,
    VisualStateConnect = 1,
} LoginControllerVisualState;

@protocol ILoginChecker;

@protocol ILoginViewController <NSObject>

@property (weak, nonatomic) id <ILoginChecker> loginCheckerDelegate;
@property (nonatomic) LoginControllerVisualState visualState;

@end