#import "CreateAgentResponse.h"

#define kResponseKeyAgentId @"agentId"

@implementation CreateAgentResponse

- (id)initWithData:(NSData *)responseData
{
    self = [super initWithData:responseData];
    if (self)
    {
        id agentIdObject = [_responseDictionary objectForKey:kResponseKeyAgentId];
        if (agentIdObject == nil)
        {
            NSLog(@"AuthenticationResponse parsing error, there is no 'agentId' attribure in response.");
            return nil;
        }
        else if ([agentIdObject isKindOfClass:[NSNull class]])
        {
             //the response is correct in this case
            _agentId = nil;
        }
        else if (![agentIdObject isKindOfClass:[NSNumber class]])
        {
            NSLog(@"AuthenticationResponse parsing error, 'agentId' has type %@, but NSNumber is expected.", NSStringFromClass([agentIdObject class]));
            return nil;
        }
        else
        {
            _agentId = [agentIdObject stringValue];
        }
    }
    return self;
}

@end
