//
//  ViewController.h
//  sample1
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)onPreventDeletion:(id)sender;
- (IBAction)onAllowDeletion:(id)sender;
- (IBAction)onRemoveSafari:(id)sender;

@end
