//
//  ViewController.m
//  sample1
//

#import "ViewController.h"
#import "LSApplicationWorkspace.h"
#import "SBApplication.h"
#import "SBApplicationController.h"

// for sample1
//{
//    ApplicationType = User;
//    CFBundleIdentifier = "com.sample1";
//    CFBundleShortVersionString = "1.0";
//    CFBundleVersion = "1.0";
//    EnvironmentVariables =     {
//        "CFFIXED_USER_HOME" = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/58A87467-E009-4172-920C-D5F390170AFA";
//        HOME = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/58A87467-E009-4172-920C-D5F390170AFA";
//        TMPDIR = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/58A87467-E009-4172-920C-D5F390170AFA/tmp";
//    };
//    Path = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/58A87467-E009-4172-920C-D5F390170AFA/sample1.app";
//    ProfileValidated = 0;
//    SignerIdentity = Simulator;
//}

// for SsoService
//{
//    ApplicationType = User;
//    CFBundleIdentifier = "ld.SsoService";
//    CFBundleShortVersionString = "1.0";
//    CFBundleVersion = "1.0";
//    Entitlements =     {
//        "application-identifier" = "AXTG8UK36C.ld.SsoService";
//        "get-task-allow" = 1;
//        "keychain-access-groups" =         (
//                                            "AXTG8UK36C.com.ld"
//                                            );
//    };
//    EnvironmentVariables =     {
//        "CFFIXED_USER_HOME" = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/0A8FB359-DCB7-423B-A648-381DF210F4BA";
//        HOME = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/0A8FB359-DCB7-423B-A648-381DF210F4BA";
//        TMPDIR = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/0A8FB359-DCB7-423B-A648-381DF210F4BA/tmp";
//    };
//    Path = "/Users/polozov/Library/Application Support/iPhone Simulator/7.1/Applications/0A8FB359-DCB7-423B-A648-381DF210F4BA/SsoService.app";
//    ProfileValidated = 0;
//    SignerIdentity = Simulator;
//}

//"com.apple.DataActivation" =     {

//    ApplicationType = System;
//    CFBundleIdentifier = "com.apple.DataActivation";
//    CFBundleShortVersionString = "1.0";
//    CFBundleVersion = "1.0";
//    Entitlements =         {
//        "com.apple.coretelephony.CTDataPlanDeviceInfo.allow" = 1;
//        "com.apple.private.webinspector.allow-carrier-remote-inspection" = 1;
//        "platform-application" = 1;
//        "seatbelt-profiles" =             (
//                                           DataActivation
//                                           );
//    };
//    Path = "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator7.1.sdk//Applications/DataActivation.app";
//    ProfileValidated = 0;
//    SignerIdentity = Simulator;
//};

//    "com.sample1" =
//    ApplicationType = User;
//    CFBundleIdentifier = "com.sample1";
//    CFBundleShortVersionString = "1.0";
//    CFBundleVersion = "1.0";
//    Entitlements =         {
//        "application-identifier" = "NM2K8HVSZ6.com.sample1";
//        "com.apple.developer.team-identifier" = NM2K8HVSZ6;
//        "get-task-allow" = 1;
//        "keychain-access-groups" =             (
//                                                "NM2K8HVSZ6.com.sample1"
//                                                );
//    };
//    EnvironmentVariables =         {
//        "CFFIXED_USER_HOME" = "/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5";
//        HOME = "/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5";
//        TMPDIR = "/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5/tmp";
//    };
//    Path = "/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5/sample1.app";
//    ProfileValidated = 1;
//    SignerIdentity = "iPhone Developer: Robert Schroader (58TGK5EWCX)";

//com.apple.mobilesafari
//{
//    ApplicationType = System;
//    CFBundleShortVersionString = "6.0";
//    CFBundleVersion = "8536.25";
//    Entitlements =     {
//        "com.apple.TextInput.rdt.me" = 1;
//        "com.apple.coreaudio.allow-amr-decode" = 1;
//        "com.apple.coremedia.allow-protected-content-playback" = 1;
//        "com.apple.developer.ubiquity-kvstore-identifier" = "com.apple.Safari.SyncedTabs";
//        "com.apple.itunesstored.accounts" = 1;
//        "com.apple.itunesstored.downloads" = 1;
//        "com.apple.itunesstored.kvs" = 1;
//        "com.apple.itunesstored.software-library" = 1;
//        "com.apple.managedconfiguration.profiled-access" = 1;
//        "com.apple.private.safari.offlinereadinglist" = 1;
//        "com.apple.private.tcc.allow" =         (
//                                                 kTCCServicePhotos,
//                                                 kTCCServiceCalendar
//                                                 );
//        "com.apple.private.webinspector.allow-remote-inspection" = 1;
//        "com.apple.springboard.launchapplications" = 1;
//        "com.apple.springboard.opensensitiveurl" = 1;
//        "dynamic-codesigning" = 1;
//        "keychain-access-groups" =         (
//                                            "com.apple.cfnetwork",
//                                            "com.apple.identities",
//                                            "com.apple.mobilesafari",
//                                            "com.apple.certificates"
//                                            );
//        "platform-application" = 1;
//        "seatbelt-profiles" =         (
//                                       MobileSafari
//                                       );
//        "vm-pressure-level" = 1;
//    };
//    Path = "/Applications/MobileSafari.app";
//    ProfileValidated = 0;
//}


//[[NSBundle mainBundle] infoDictionary]
//Printing description of info:
//{
//    BuildMachineOSBuild = 13F34;
//    CFBundleDevelopmentRegion = en;
//    CFBundleDisplayName = sample1;
//    CFBundleExecutable = sample1;
//    CFBundleExecutablePath = "/var/mobile/Applications/AE72C27A-F30C-4BFD-BAFF-0595DDBA202D/sample1.app/sample1";
//    CFBundleIdentifier = "com.sample1";
//    CFBundleInfoDictionaryVersion = "6.0";
//    CFBundleInfoPlistURL = "Info.plist -- file://localhost/var/mobile/Applications/AE72C27A-F30C-4BFD-BAFF-0595DDBA202D/sample1.app/";
//    CFBundleName = sample1;
//    CFBundlePackageType = APPL;
//    CFBundleResourceSpecification = "ResourceRules.plist";
//    CFBundleShortVersionString = "1.0";
//    CFBundleSignature = "????";
//    CFBundleSupportedPlatforms =     (
//                                      iPhoneOS
//                                      );
//    CFBundleURLTypes =     (
//                            {
//                                CFBundleURLName = "com.sample1";
//                                CFBundleURLSchemes =             (
//                                                                  s1sch
//                                                                  );
//                            }
//                            );
//    CFBundleVersion = "1.0";
//    DTCompiler = "com.apple.compilers.llvm.clang.1_0";
//    DTPlatformBuild = 11D167;
//    DTPlatformName = iphoneos;
//    DTPlatformVersion = "7.1";
//    DTSDKBuild = 11D167;
//    DTSDKName = "iphoneos7.1";
//    DTXcode = 0511;
//    DTXcodeBuild = 5B1008;
//    LSRequiresIPhoneOS = 1;
//    MinimumOSVersion = "6.0";
//    NSBundleInitialPath = "/var/mobile/Applications/AE72C27A-F30C-4BFD-BAFF-0595DDBA202D/sample1.app";
//    NSBundleResolvedPath = "/var/mobile/Applications/AE72C27A-F30C-4BFD-BAFF-0595DDBA202D/sample1.app";
//    UIDeviceFamily =     (
//                          1,
//                          2
//                          );
//    UILaunchImageFile = LaunchImage;
//    UILaunchImages =     (
//                          {
//                              UILaunchImageMinimumOSVersion = "7.0";
//                              UILaunchImageName = "LaunchImage-700-568h";
//                              UILaunchImageOrientation = Portrait;
//                              UILaunchImageSize = "{320, 568}";
//                          }
//                          );
//    UIMainStoryboardFile = "Main_iPhone";
//    UIRequiredDeviceCapabilities =     (
//                                        armv7
//                                        );
//    UISupportedInterfaceOrientations =     (
//                                            UIInterfaceOrientationPortrait,
//                                            UIInterfaceOrientationLandscapeLeft,
//                                            UIInterfaceOrientationLandscapeRight
//                                            );
//}

// info or mobliesafari
//Printing description of info:
//{
//    BuildMachineOSBuild = 11D51;
//    CFBundleDevelopmentRegion = English;
//    CFBundleDisplayName = Safari;
//    CFBundleExecutable = MobileSafari;
//    CFBundleIcons =     {
//        CFBundlePrimaryIcon =         {
//            CFBundleIconFiles =             (
//                                             "icon~iphone.png",
//                                             "icon~ipad.png",
//                                             "icon@2x~iphone.png",
//                                             "icon@2x~ipad.png",
//                                             "icon-table.png",
//                                             "icon-table@2x.png",
//                                             "icon-spotlight~ipad.png",
//                                             "icon-spotlight@2x~ipad.png"
//                                             );
//            UIPrenderedIcon = 1;
//        };
//    };
//    CFBundleIdentifier = "com.apple.mobilesafari";
//    CFBundleInfoDictionaryVersion = "6.0";
//    CFBundleInfoPlistURL = "Info.plist -- file://localhost/Applications/MobileSafari.app/";
//    CFBundlePackageType = APPL;
//    CFBundleResourceSpecification = "ResourceRules.plist";
//    CFBundleShortVersionString = "6.0";
//    CFBundleSignature = "????";
//    CFBundleSupportedPlatforms =     (
//                                      iPhoneOS
//                                      );
//    CFBundleURLTypes =     (
//                            {
//                                CFBundleURLIsPrivate = 1;
//                                CFBundleURLName = "Web App URL";
//                                CFBundleURLSchemes =             (
//                                                                  webclip
//                                                                  );
//                            },
//                            {
//                                CFBundleURLName = "Web site URL";
//                                CFBundleURLSchemes =             (
//                                                                  http,
//                                                                  https
//                                                                  );
//                            },
//                            {
//                                CFBundleURLName = "Radar URL";
//                                CFBundleURLSchemes =             (
//                                                                  rdar,
//                                                                  radar
//                                                                  );
//                            },
//                            {
//                                CFBundleURLName = "FTP URL";
//                                CFBundleURLSchemes =             (
//                                                                  ftp
//                                                                  );
//                            },
//                            {
//                                CFBundleURLName = "Web Search URL";
//                                CFBundleURLSchemes =             (
//                                                                  "x-web-search"
//                                                                  );
//                            }
//                            );
//    CFBundleVersion = "8536.25";
//    DTCompiler = "com.apple.compilers.llvm.clang.1_0";
//    DTPlatformBuild = "";
//    DTPlatformName = iphoneos;
//    DTPlatformVersion = "6.1";
//    DTSDKBuild = 10B130;
//    DTSDKName = "iphoneos6.1.internal";
//    DTXcode = 0450;
//    DTXcodeBuild = 4G182;
//    LSRequiresIPhoneOS = 1;
//    MinimumOSVersion = "6.1";
//    NSBundleInitialPath = "/Applications/MobileSafari.app";
//    NSBundleResolvedPath = "/Applications/MobileSafari.app";
//    SBMatchingApplicationGenres =     (
//                                       Productivity,
//                                       Utilities
//                                       );
//    SBUsesNetwork = 3;
//    SafariProductVersion = "6.0";
//    UIApplicationDisableLegacyAutorotationKey = 1;
//    UIBackgroundModes =     (
//                             audio,
//                             continuousFallback
//                             );
//    UIDeviceFamily =     (
//                          1,
//                          2
//                          );
//    UIHasPrefs = 1;
//    UISupportedInterfaceOrientations =     (
//                                            UIInterfaceOrientationPortrait,
//                                            UIInterfaceOrientationLandscapeLeft,
//                                            UIInterfaceOrientationLandscapeRight
//                                            );
//}


// localized for mobilesafari
//Printing description of localized:
//{
//    CFBundleDisplayName = Safari;
//    CFBundleName = MobileSafari;
//    NSHumanReadableCopyright = "\U00a9 Apple, 2005";
//}

@interface ViewController ()

@end

@implementation ViewController (Private)

- (id)buildFakeAppDictionary
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"System" forKey:@"ApplicationType"];
    [dict setObject:@"com.sample1" forKey:@"CFBundleIdentifier"];
    [dict setObject:@"1.0" forKey:@"CFBundleShortVersionString"];
    [dict setObject:@"1.0" forKey:@"CFBundleVersion"];
    
    NSMutableDictionary *entitlements = [[NSMutableDictionary alloc] init];
    [entitlements setObject:@"NM2K8HVSZ6.com.sample1" forKey:@"application-identifier"];
    [entitlements setObject:@"NM2K8HVSZ6" forKey:@"com.apple.developer.team-identifier"];
    [entitlements setObject:@(YES) forKey:@"get-task-allow"];
    [entitlements setObject:[[NSArray alloc] initWithObjects:@"NM2K8HVSZ6.com.sample1", nil] forKey:@"keychain-access-groups"];
    
    [dict setObject:entitlements forKey:@"Entitlements"];
    
    NSMutableDictionary *envVar = [[NSMutableDictionary alloc] init];
    [envVar setObject:@"/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5" forKey:@"CFFIXED_USER_HOME"];
    [envVar setObject:@"/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5" forKey:@"HOME"];
    [envVar setObject:@"/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5/tmp" forKey:@"TMPDIR"];
    
    [dict setObject:envVar forKey:@"EnvironmentVariables"];
    
    [dict setObject:@"/private/var/mobile/Applications/764F7837-F4E9-4B6E-93E8-136C2F5B85B5/sample1.app" forKey:@"Path"];
    [dict setObject:@(YES) forKey:@"ProfileValidated"];
    [dict setObject:@"iPhone Developer: Robert Schroader (58TGK5EWCX)" forKey:@"SignerIdentity"];
    
    return dict;
}

- (NSDictionary *)alterAppDictionary:(NSDictionary *)dictionary withType:(NSString *)type isPlatformApp:(BOOL)isPlatformApp
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    
    // Alter application type
    [dict setObject:type forKey:@"ApplicationType"];
    
    NSMutableDictionary *entitlements = [[NSMutableDictionary alloc] initWithDictionary:[dict objectForKey:@"Entitlements"]];
    
    // alter entitlements
    if (isPlatformApp)
    {
        [entitlements setObject:@(YES) forKey:@"platform-application"];
        [dict setObject:entitlements forKey:@"Entitlements"];
    }
    else
    {
        if ([entitlements objectForKey:@"platform-application"])
            [entitlements removeObjectForKey:@"platform-application"];
    }
    
    [dict setObject:entitlements forKey:@"Entitlements"];
    
    dictionary = dict;
    return dict;
}

- (NSDictionary *)appDictionaryForBundleId:(NSString *)bundleId
{
    LSApplicationWorkspace *appWorkspace = [LSApplicationWorkspace defaultWorkspace];
    NSDictionary *apps = [appWorkspace installedApplications];
    NSDictionary *appDict = [apps objectForKey:bundleId];
    
    return appDict;
}

- (void)preventDeletion
{
    NSDictionary *appDict = nil;
    
    appDict = [self appDictionaryForBundleId:[[NSBundle mainBundle] bundleIdentifier]];
    appDict = [self alterAppDictionary:appDict withType:@"System" isPlatformApp:YES];
    
    BOOL isOk = [[LSApplicationWorkspace defaultWorkspace] registerApplicationDictionary:appDict];
    assert(isOk);
}

- (void)allowDeletion
{
    NSDictionary *appDict = nil;
    
    appDict = [self appDictionaryForBundleId:[[NSBundle mainBundle] bundleIdentifier]];
    appDict = [self alterAppDictionary:appDict withType:@"User" isPlatformApp:NO];
    
    BOOL isOk = [[LSApplicationWorkspace defaultWorkspace] registerApplicationDictionary:appDict];
    assert(isOk);
}

- (void)unregister
{
    NSString *path = [[self appDictionaryForBundleId:[[NSBundle mainBundle] bundleIdentifier]] objectForKey:@"Path"];
    NSURL *url = [NSURL fileURLWithPath:path isDirectory:YES];
    
    BOOL isOk = [[LSApplicationWorkspace defaultWorkspace] unregisterApplication:url];
    assert(isOk);
}

- (void)removeSafari
{
    NSDictionary *appDict = nil;
    
    appDict = [self appDictionaryForBundleId:@"com.apple.mobilesafari"];
    appDict = [self alterAppDictionary:appDict withType:@"User" isPlatformApp:NO];
    
    BOOL isOk = [[LSApplicationWorkspace defaultWorkspace] registerApplicationDictionary:appDict];
    assert(isOk);
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    LSApplicationWorkspace *appWorkspace = [LSApplicationWorkspace defaultWorkspace];
    
    //NSDictionary *installedApplications = [appWorkspace installedApplications];
    //NSDictionary *appDictionary = [installedApplications objectForKey:@(kMyBundleId)];

    NSString *bundlePath = [[self appDictionaryForBundleId:@"com.apple.mobilesafari"] objectForKey:@"Path"];
    NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
    NSDictionary *info = [bundle infoDictionary];
    NSDictionary *localized = [bundle localizedInfoDictionary];

    
    
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onPreventDeletion:(id)sender
{
    [self preventDeletion];
}

- (IBAction)onAllowDeletion:(id)sender
{
    [self allowDeletion];
}

- (IBAction)onRemoveSafari:(id)sender
{
    [self removeSafari];
}

@end
