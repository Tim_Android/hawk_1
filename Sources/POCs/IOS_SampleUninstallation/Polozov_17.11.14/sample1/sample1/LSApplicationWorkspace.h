//
//  LSApplicationWorkspace.h
//  sample1
//
//  Created by Polozov on 10/30/14.
//  Copyright (c) 2014 Polozov. All rights reserved.
//

#ifndef LSApplicationWorkspace_h
#define LSApplicationWorkspace_h

// iOS 6

@interface LSApplicationWorkspace : NSObject {
}

+ (id)defaultWorkspace;

- (id)deviceIdentifierForVendor;
- (id)deviceIdentifierForAdvertising;

- (BOOL)invalidateIconCache:(id)arg1;

- (BOOL)unregisterApplication:(id)arg1;
- (BOOL)registerApplication:(id)arg1;
- (BOOL)registerApplicationDictionary:(id)arg1;

- (id)publicURLSchemes;
- (id)privateURLSchemes;

- (id)installedApplications;
- (id)applicationsAvailableForOpeningDocument:(id)arg1;
- (id)directionsApplications;
- (id)applicationForOpeningResource:(id)arg1;

- (id)operationToOpenResource:(id)arg1 usingApplication:(id)arg2 userInfo:(id)arg3;
- (id)operationToOpenResource:(id)arg1 usingApplication:(id)arg2 uniqueDocumentIdentifier:(id)arg3 userInfo:(id)arg4;
- (id)operationToOpenResource:(id)arg1 usingApplication:(id)arg2 uniqueDocumentIdentifier:(id)arg3 userInfo:(id)arg4 delegate:(id)arg5;

- (BOOL)openURL:(id)arg1;
- (BOOL)openURL:(id)arg1 withOptions:(id)arg2;
- (BOOL)openSensitiveURL:(id)arg1 withOptions:(id)arg2;

- (id)URLOverrideForURL:(id)arg1;
- (id)applicationsAvailableForHandlingURLScheme:(id)arg1;

// iOS 6 only
- (id)applicationIdentifierForOpeningResource:(id)arg1;

#if 0

// iOS 7

- (BOOL)_LSPrivateRebuildLaunchServicesDatabase;
- (BOOL)_LSPrivateRebuildApplicationDatabasesForSystemApps:(BOOL)arg1 internal:(BOOL)arg2 user:(BOOL)arg3;
- (void)clearCreatedProgressForBundleID:(id)arg1;
- (BOOL)installPhaseFinishedForProgress:(id)arg1;
- (id)installProgressForApplication:(id)arg1 withPhase:(unsigned int)arg2;
- (void)removeInstallProgressForBundleID:(id)arg1;
- (id)installProgressForBundleID:(id)arg1 makeSynchronous:(unsigned char)arg2;
- (void)_clearCachedAdvertisingIdentifier;
- (void)clearAdvertisingIdentifier;
- (BOOL)registerApplicationDictionary:(id)arg1 withObserverNotification:(unsigned int)arg2;
- (BOOL)uninstallApplication:(id)arg1 withOptions:(id)arg2 usingBlock:(id)arg3;
- (BOOL)uninstallApplication:(id)arg1 withOptions:(id)arg2;
- (BOOL)installApplication:(id)arg1 withOptions:(id)arg2 error:(id *)arg3 usingBlock:(id)arg4;
- (BOOL)installApplication:(id)arg1 withOptions:(id)arg2 error:(id *)arg3;
- (BOOL)installApplication:(id)arg1 withOptions:(id)arg2;
- (BOOL)applicationIsInstalled:(id)arg1;
- (id)allApplications;
- (id)unrestrictedApplications;
- (id)placeholderApplications;
- (id)allInstalledApplications;
- (id)operationToOpenResource:(id)arg1 usingApplication:(id)arg2 uniqueDocumentIdentifier:(id)arg3 sourceIsManaged:(BOOL)arg4 userInfo:(id)arg5 delegate:(id)arg6;
- (BOOL)openApplicationWithBundleID:(id)arg1;
- (id)applicationsWithAudioComponents;
- (id)applicationsWithUIBackgroundModes;
- (void)removeObserver:(id)arg1;
- (void)addObserver:(id)arg1;
- (id)delegateProxy;
- (id)remoteObserver;
- (BOOL)establishConnection;

#endif

@end

#endif
