package com.example.kriuchkovtymofii.poc_iavis3.fileObserver;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import com.example.kriuchkovtymofii.poc_iavis3.CallbackInterface;

import static com.example.kriuchkovtymofii.poc_iavis3.fileObserver.DirectoryFileObserver.MIN_CLICK_INTERVAL;

public class UriObserver  extends ContentObserver {

    public UriObserver(Handler handler, CallbackInterface callbackInterface) {
        super(handler);
        // TODO Auto-generated constructor stub
        this.callbackInterface = callbackInterface;
    }

    private long mLastClickTime;
    CallbackInterface callbackInterface;

    @Override
    public void onChange(boolean selfChange) {
        // TODO Auto-generated method stub
        super.onChange(selfChange);

        Log.d("INSTANT", "GETTING CHANGES");

        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;

        callbackInterface.onEvent();
    }

}