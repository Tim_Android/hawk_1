package com.example.kriuchkovtymofii.poc_iavis3.fileObserver;

import android.os.FileObserver;
import android.os.SystemClock;
import android.util.Log;

import com.example.kriuchkovtymofii.poc_iavis3.CallbackInterface;

public class DirectoryFileObserver extends FileObserver {
    String aboslutePath = "path to your directory";

    CallbackInterface callbackInterface;

    public DirectoryFileObserver(String path, CallbackInterface callbackInterface) {
        super(path, FileObserver.ALL_EVENTS);
        aboslutePath = path;
        this.callbackInterface = callbackInterface;
    }

    public static final long MIN_CLICK_INTERVAL = 600;
    private long mLastClickTime;

    @Override
    public void onEvent(int event, String path) {
        Log.d("TAGG ", "some Event: " + event);
        if (path == null) {
            return;
        }

        // Log.d("dasdasdas ", "some Event: ");

        //a new file or subdirectory was created under the monitored directory
        if ((FileObserver.CREATE & event) != 0) {
            Log.d("TAGG", "onEvent: " + aboslutePath + "/" + path + " is createdn");
            //Toast.makeText(getBaseContext(), file + " was saved!", toast.LENGTH_LONG).show();
        }

        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;

        if (path.equals("Camera")) {
            callbackInterface.onEvent();
        }

        switch (event) {
            case FileObserver.ACCESS:
                Log.i("RecursiveFileObserver", "ACCESS: " + path);
                break;
            case FileObserver.ATTRIB:
                Log.i("RecursiveFileObserver", "ATTRIB: " + path);
                break;
            case FileObserver.CLOSE_NOWRITE:
                Log.i("RecursiveFileObserver", "CLOSE_NOWRITE: " + path);
                break;
            case FileObserver.CLOSE_WRITE:
                Log.i("RecursiveFileObserver", "CLOSE_WRITE: " + path);
                break;
            case FileObserver.CREATE:
                Log.i("RecursiveFileObserver", "CREATE: " + path);
                break;
            case FileObserver.DELETE:
                Log.i("RecursiveFileObserver", "DELETE: " + path);
                break;
            case FileObserver.DELETE_SELF:
                Log.i("RecursiveFileObserver", "DELETE_SELF: " + path);
                break;
            case FileObserver.MODIFY:
                Log.i("RecursiveFileObserver", "MODIFY: " + path);
                break;
            case FileObserver.MOVE_SELF:
                Log.i("RecursiveFileObserver", "MOVE_SELF: " + path);
                break;
            case FileObserver.MOVED_FROM:
                Log.i("RecursiveFileObserver", "MOVED_FROM: " + path);
                break;
            case FileObserver.MOVED_TO:
                Log.i("RecursiveFileObserver", "MOVED_TO: " + path);
                break;
            case FileObserver.OPEN:
                Log.i("RecursiveFileObserver", "OPEN: " + path);
                break;
            default:
                Log.i("RecursiveFileObserver", "DEFAULT(" + event + ";) : " + path);
                break;
        }
    }

    @Override
    protected void finalize() {
        super.finalize();
    }
}