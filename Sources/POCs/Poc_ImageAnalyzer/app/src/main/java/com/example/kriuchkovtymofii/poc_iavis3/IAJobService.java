package com.example.kriuchkovtymofii.poc_iavis3;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class IAJobService extends JobService {

    private static final int JOB_ID = 1;
    public static final long ONE_DAY_INTERVAL = 24 * 60 * 60 * 1000L; // 1 Day
    public static final long INTERVAL_FIVE_MINUTES = 30 * 1000L; // 5 minuts
    public static final long ONE_WEEK_INTERVAL = 7 * 24 * 60 * 60 * 1000L; // 1 Week

    public static void schedule(Context context, long intervalMillis) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(context, IAJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, componentName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setMinimumLatency(intervalMillis);
        } else {
            builder.setPeriodic(intervalMillis);
        }

        jobScheduler.schedule(builder.build());
    }

    public static void cancel(Context context) {
        JobScheduler jobScheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }

    public IAJobService() {
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        IAIntentService.startService(this, "start", "");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.w("IAJobService", "onStartCommand callback called");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("IAJobService", "onCreate callback called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w("IAJobService", "onDestroy callback called");
    }
}
