package com.imageanalyzer.iavis_android_lib;

import java.nio.charset.StandardCharsets;

public class IAViSMobile{

    private static byte[] stringToBytes(String s) {
        return s.getBytes(StandardCharsets.US_ASCII);
    }

    public native void setNumThreads(int numThreads);

    public native int initialize(String strTemporaryDirectoryPath);

    public native float[] getConfidenceScore(byte[] data, int width, int height);

    public float[] getConfidenceScore(String imgPath) {
        return getConfidenceScore(stringToBytes(imgPath), 0, 0);
    }

    public native int[] predictImage(byte[] data, int width, int height, int k);

    public int[] predictImage(String imgPath, int k) {
        return predictImage(stringToBytes(imgPath), 0, 0, k);
    }

    public int[] predictImage(String imgPath) {
        return predictImage(imgPath, 5);
    }
}
