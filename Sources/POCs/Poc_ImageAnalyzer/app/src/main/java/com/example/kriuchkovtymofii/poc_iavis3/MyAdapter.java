package com.example.kriuchkovtymofii.poc_iavis3;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.kriuchkovtymofii.poc_iavis3.db.ImageModel;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private ArrayList<ImageModel> mDataset;

    public void setmDataset(ArrayList<ImageModel> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView textView2;
        public TextView textView3;
        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.textView);
            textView2 = v.findViewById(R.id.textView2);
            textView3 = v.findViewById(R.id.textView3);
            imageView = v.findViewById(R.id.imageView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<ImageModel> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText("Drugs : " + String.format("%.2f", mDataset.get(position).getDrugsValue()) + "%");
        holder.textView2.setText("Porn : " + String.format("%.2f", mDataset.get(position).getPornValue()) + "%");
        holder.textView3.setText("SwimUnderwear : " + String.format("%.2f", mDataset.get(position).getSwimUnderwearValue()) + "%");

        checkValue(mDataset.get(position).getDrugsValue(), holder.mTextView);
        checkValue(mDataset.get(position).getPornValue(), holder.textView2);
        checkValue(mDataset.get(position).getSwimUnderwearValue(), holder.textView3);

        Glide.with(holder.mTextView.getContext())
                .load(mDataset.get(position).getImagePath())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher)
                        .fitCenter())
                .into(holder.imageView);
    }

    void checkValue(float value, TextView textView){
        textView.setTextColor(textView.getContext().getResources().getColor(R.color.colorText));
        if (value > 70)
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.colorAccent));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}