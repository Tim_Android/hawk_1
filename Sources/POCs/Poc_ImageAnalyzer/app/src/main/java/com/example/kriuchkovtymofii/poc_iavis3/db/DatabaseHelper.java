package com.example.kriuchkovtymofii.poc_iavis3.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "notes_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(ImageModel.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + ImageModel.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void insertImages(ArrayList<ImageModel> imageModels) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0; i < imageModels.size(); i++) {
            ContentValues values = new ContentValues();
            // `id` and `timestamp` will be inserted automatically.
            // no need to add them
            values.put(ImageModel.IMAGE_PATH, imageModels.get(i).getImagePath());

            Float drugsValue = imageModels.get(i).getDrugsValue();
            if (drugsValue == null)
                drugsValue = -1.f;

            values.put(ImageModel.DRUGS, drugsValue);
            values.put(ImageModel.PORN, imageModels.get(i).getPornValue());
            values.put(ImageModel.SWIM_UNDERWARE, imageModels.get(i).getSwimUnderwearValue());

            // insert row
            long id = db.insert(ImageModel.TABLE_NAME, null, values);
        }

        // close db connection
        db.close();
    }

    public ArrayList<ImageModel> getAllNotes() {
        ArrayList<ImageModel> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ImageModel.TABLE_NAME + " ORDER BY " +
                ImageModel.IMAGE_PATH + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ImageModel imageModel = new ImageModel();
                imageModel.setImagePath(cursor.getString(cursor.getColumnIndex(ImageModel.IMAGE_PATH)));
                imageModel.setDrugsValue(cursor.getFloat(cursor.getColumnIndex(ImageModel.DRUGS)));
                imageModel.setPornValue(cursor.getFloat(cursor.getColumnIndex(ImageModel.PORN)));
                imageModel.setSwimUnderwearValue(cursor.getFloat(cursor.getColumnIndex(ImageModel.SWIM_UNDERWARE)));

                notes.add(imageModel);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public List<String> getAllPaths() {
        List<String> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ImageModel.TABLE_NAME + " ORDER BY " +
                ImageModel.IMAGE_PATH + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String path = cursor.getString(cursor.getColumnIndex(ImageModel.IMAGE_PATH));

                notes.add(path);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
}