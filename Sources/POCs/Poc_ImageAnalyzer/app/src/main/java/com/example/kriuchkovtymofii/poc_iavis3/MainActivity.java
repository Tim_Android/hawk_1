package com.example.kriuchkovtymofii.poc_iavis3;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.getkeepsafe.relinker.ReLinker;
import com.imageanalyzer.iavis_android_lib.IAViSMobile;

public class MainActivity extends AppCompatActivity implements IAViSListener {

    private static final int REQUEST_IMAGE_SELECT = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int SD_CARD_ACCESS_REQUEST = 5;
    private static String[] IAVIS_CATEGORIES;

    private Button btnInitializeIAViS;
    private Button btnSelectImage;
    private ImageView ivCaptured;
    private TextView tvLabel;
    private Bitmap bmp;
    private ProgressDialog dialog;
    private IAViSMobile iavisMobile;
    private float confidenceScores[] = null;
    private boolean accessRequestGranted = false;
    private EditText editTextIavisPath;
    private Button btnAnalyzeFolder;
    String IAViSDir = "/storage/emulated/0/IAViS";
    String categoryNames = IAViSDir + "/iavis.4";
    String folderName = "iavis";

   /* static {
        System.loadLibrary("IAViS");
        System.loadLibrary("IAViS_jni");
    }*/


    // Used to load the 'native-lib' library on application startup.
    /*static {
        System.loadLibrary("native-lib");
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ReLinker.loadLibrary(MainActivity.this, "IAViS", new ReLinker.LoadListener() {
            @Override
            public void success() {
                Log.d("dasdasdasdasdasd", "success1: ");
            }

            @Override
            public void failure(Throwable t) {
                Log.d("dasdasdasdasdasd", "success2: ");
            }
        });
        ReLinker.loadLibrary(MainActivity.this, "IAViS_jni");

        /*System.loadLibrary("IAViS");
        System.loadLibrary("IAViS_jni");*/


/*        String arch = System.getProperty("os.arch");
        Log.d("asdasdsadas", "onCreate: " + arch);*/

        ivCaptured = (ImageView) findViewById(R.id.ivCaptured);
        tvLabel = (TextView) findViewById(R.id.tvLabel);
        editTextIavisPath = (EditText) findViewById(R.id.editTextIavisPath);
        btnAnalyzeFolder = (Button) findViewById(R.id.btnAnalyzeFolder);
        btnInitializeIAViS = (Button) findViewById(R.id.btnInitializeIAViS);

        copyFileOrDir(folderName);


        Button btnSelectTEST = findViewById(R.id.btnSelectTEST);
        btnSelectTEST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initIAViS2();
            }
        });


        btnInitializeIAViS.setOnClickListener(v -> initIAViS());

        btnSelectImage = (Button) findViewById(R.id.btnSelect);
        btnSelectImage.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                initPrediction();
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_IMAGE_SELECT);
            }
        });

        // Initially, only initializing the IAViS is possible.
        btnInitializeIAViS.setEnabled(true);
        btnSelectImage.setEnabled(false);

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, SD_CARD_ACCESS_REQUEST);

        btnAnalyzeFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IAJobService.schedule(MainActivity.this, IAJobService.INTERVAL_FIVE_MINUTES);
            }
        });
    }

    void initIAViS2(){
        iavisMobile = new IAViSMobile();
        iavisMobile.setNumThreads(4);

        IAViSDir = getFilesDir().getAbsolutePath() + "/" + folderName;

        int iavisInitializationResult = iavisMobile.initialize(IAViSDir);

        // Initialization returns -4 if the evaluation license has expired...
        if (0 <= iavisInitializationResult) {

            // Read the category names
            // Get the labels for the categories.
            try (BufferedReader br = new BufferedReader(new FileReader(IAViSDir + categoryNames))) {
                List<String> lines = new ArrayList<String>();
                String line;
                while ((line = br.readLine()) != null) {
                    lines.add(line);
                }
                // add the category names to the list so that we can use them for displaying result.
                IAVIS_CATEGORIES = lines.toArray(new String[0]);
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Context context = getApplicationContext();

            CharSequence toastMessage = "";

            switch (iavisInitializationResult) {
                case -4:
                    toastMessage = "Trial Licence - expires 30/06/2017";
                    break;
                case -5:
                    toastMessage = "iavis.1 file missing, corrupt, or not accessible.";
                    break;
                case -6:
                    toastMessage = "error handling iavis.1 file.";
                    break;
                case -7:
                    toastMessage = "iavis.2 file missing, corrupt, or not accessible.";
                    break;
                case -8:
                    toastMessage = "error handling iavis.2 file.";
                    break;
                case -9:
                    toastMessage = "iavis.3 file missing, corrupt, or not accessible.";
                    break;
                default:
                    break;
            }

            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, toastMessage, duration);
            toast.show();

            // Disable the camera as well as image selection buttons and clear the output label
        }
    }

    private void initIAViS() {
        // First check if the access rights to SD card etc. are granted.
        // If yes, then continue, else return.
        if(false == accessRequestGranted)
        {
            return;
        }

        // check if the IAViS directory has any path mentioned...
        // If yes, only then conitnue.

        String strIAViSDirectory = editTextIavisPath.getText().toString().trim();

        File iavisDirectory = new File(strIAViSDirectory);
        if(iavisDirectory.exists() && iavisDirectory.isDirectory()) {
            if (0 < strIAViSDirectory.length()) {
                IAViSDir = strIAViSDirectory;
            }
            btnInitializeIAViS.setEnabled(false);
            btnSelectImage.setEnabled(false);
            tvLabel.setText("");

            iavisMobile = new IAViSMobile();
            iavisMobile.setNumThreads(4);
            int iavisInitializationResult = iavisMobile.initialize(IAViSDir);

            // Initialization returns -4 if the evaluation license has expired...
            if (0 <= iavisInitializationResult) {

                // Read the category names
                // Get the labels for the categories.
                try (BufferedReader br = new BufferedReader(new FileReader(categoryNames))) {
                    List<String> lines = new ArrayList<String>();
                    String line;
                    while ((line = br.readLine()) != null) {
                        lines.add(line);
                    }
                    // add the category names to the list so that we can use them for displaying result.
                    IAVIS_CATEGORIES = lines.toArray(new String[0]);
                    br.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                btnInitializeIAViS.setEnabled(false);
                btnSelectImage.setEnabled(true);
            } else {
                Context context = getApplicationContext();

                CharSequence toastMessage = "";

                switch (iavisInitializationResult) {
                    case -4:
                        toastMessage = "Trial Licence - expires 30/06/2017";
                        break;
                    case -5:
                        toastMessage = "iavis.1 file missing, corrupt, or not accessible.";
                        break;
                    case -6:
                        toastMessage = "error handling iavis.1 file.";
                        break;
                    case -7:
                        toastMessage = "iavis.2 file missing, corrupt, or not accessible.";
                        break;
                    case -8:
                        toastMessage = "error handling iavis.2 file.";
                        break;
                    case -9:
                        toastMessage = "iavis.3 file missing, corrupt, or not accessible.";
                        break;
                    default:
                        break;
                }

                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, toastMessage, duration);
                toast.show();

                // Disable the camera as well as image selection buttons and clear the output label
                btnInitializeIAViS.setEnabled(true);
                btnSelectImage.setEnabled(false);
                tvLabel.setText("");
            }

        }
    }

    ArrayList<ArrayList<Float>> arrayLists = new ArrayList<>();


    @Override
    public void onTaskCompleted(int result) {
        if (null != bmp) {
            ivCaptured.setImageBitmap(bmp);
        }

        String resultText = "";
        if (confidenceScores.length == 7) {
            resultText = IAVIS_CATEGORIES[1] + " : " + String.format("%.2f", confidenceScores[1]) + "\t\t\t" + IAVIS_CATEGORIES[2] + " : " + String.format("%.2f", confidenceScores[2]) + "\r\n" + IAVIS_CATEGORIES[3] + " : " + String.format("%.2f", confidenceScores[3]) + "\t\t\t" + IAVIS_CATEGORIES[4] + " : " + String.format("%.2f", confidenceScores[4]) + "\r\n" + IAVIS_CATEGORIES[5] + " : " + String.format("%.2f", confidenceScores[5]);
        } else if (confidenceScores.length == 1) {
            resultText = "failed scanning the image. \r\nPlease check if it exists \r\nand is accessible.";
        }

        tvLabel.setText(resultText);
        btnInitializeIAViS.setEnabled(false);
        btnSelectImage.setEnabled(true);

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_IMAGE_SELECT) && resultCode == RESULT_OK) {
            String imgPath;

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = MainActivity.this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgPath = cursor.getString(columnIndex);
            cursor.close();

            bmp = BitmapFactory.decodeFile(imgPath);

            dialog = ProgressDialog.show(MainActivity.this, "Predicting...", "Wait a bit...", true);

            IAViSTask cnnTask = new IAViSTask(MainActivity.this);
            cnnTask.execute(imgPath);
        } else {
            btnSelectImage.setEnabled(true);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initPrediction() {
        btnInitializeIAViS.setEnabled(false);
        btnSelectImage.setEnabled(false);
        tvLabel.setText("");
    }


    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = getFilesDir().getAbsolutePath() + "/" + path;
                File dir = new File(fullPath);
                if (!dir.exists())
                    dir.mkdir();
                for (int i = 0; i < assets.length; ++i) {
                    copyFileOrDir(path + "/" + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename) throws IOException {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = getFilesDir().getAbsolutePath() + "/" + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }

    }

    private boolean copyFile(String sourceFileName, String destFileName) throws IOException {
        AssetManager assetManager = getAssets();

        File destFile = new File(destFileName);

        File destParentDir = destFile.getParentFile();
        destParentDir.mkdir();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(sourceFileName);
            out = new FileOutputStream(destFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SD_CARD_ACCESS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, so do the
                    // iavis initialization related tasks.
                    accessRequestGranted = true;

                } else {

                    // permission denied, so disable the
                    // functionality that depends on this permission.
                    // Disable the image selection button and clear the output label
                    btnInitializeIAViS.setEnabled(false);
                    btnSelectImage.setEnabled(false);
                    tvLabel.setText("");
                    accessRequestGranted = false;
                }
                break;
            }
        }
    }


    private class IAViSTask extends AsyncTask<String, Void, Integer> {
        private IAViSListener listener;
        private long startTime;

        public IAViSTask(IAViSListener listener) {
            this.listener = listener;
        }

        @Override
        protected Integer doInBackground(String... strings) {
            startTime = SystemClock.uptimeMillis();


            float[] confidenceResults = iavisMobile.getConfidenceScore(strings[0]);
            if (confidenceScores == null) {
                confidenceScores = new float[confidenceResults.length];
            }
            for (int i = 0; i < confidenceResults.length; i++) {
                confidenceScores[i] = confidenceResults[i] * (float) 100.0;
            }
            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            listener.onTaskCompleted(integer);
            super.onPostExecute(integer);
        }
    }

}
