package com.example.kriuchkovtymofii.poc_iavis3;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.kriuchkovtymofii.poc_iavis3.db.DatabaseHelper;
import com.example.kriuchkovtymofii.poc_iavis3.db.ImageModel;
import com.imageanalyzer.iavis_android_lib.IAViSMobile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class IAIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.example.kriuchkovtymofii.poc_iavis.action.FOO";
    private static final String ACTION_BAZ = "com.example.kriuchkovtymofii.poc_iavis.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.example.kriuchkovtymofii.poc_iavis.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.example.kriuchkovtymofii.poc_iavis.extra.PARAM2";

    public IAIntentService() {
        super("IAIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startService(Context context, String param1, String param2) {
        Intent intent = new Intent(context, IAIntentService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(context, intent);
        } else {
            context.startService(intent);
        }
    }

    private static final String FRIDAY_CHANNEL_ID = "friday_channel_id";

    @Override
    public void onCreate() {
        super.onCreate();

        //path = getGalleryPath();
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(FRIDAY_CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{0, 100, 50, 100});
            manager.createNotificationChannel(channel);
            builder = new Notification.Builder(this, FRIDAY_CHANNEL_ID);
        } else {
            builder = new Notification.Builder(this).setVibrate(new long[]{0, 100, 50, 100});
        }

        startForeground(1, builder.build());

        IAViSDir = getFilesDir().getAbsolutePath() + "/" + folderName;

        //copyFileOrDir(folderName);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */

    DatabaseHelper databaseHelper;
    ArrayList<ImageModel> imageModels = new ArrayList<>();

    private void handleActionFoo(String param1, String param2) {
        Log.d("dasdsdsd", "onClick: ");

        initIAViS();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean("oldImagesAdded", false)) {
            ArrayList<String> paths = getAllPathsFromGallery();

            databaseHelper = new DatabaseHelper(this);

            arrayLists = new ArrayList<>();
            imageModels = new ArrayList<>();

            List<String> imageModelsFormDB = databaseHelper.getAllPaths();

            ArrayList<String> newImages = new ArrayList<>();
            for (int i = 0; i < paths.size(); i++) {
                if (!imageModelsFormDB.contains(paths.get(i)))
                    newImages.add(paths.get(i));
            }

            for (int i = 0; i < newImages.size(); i++) {
                float[] confidenceResults = iavisMobile.getConfidenceScore(newImages.get(i));

                imageModels.add(new ImageModel(newImages.get(i)));

                arrayLists.add(new ArrayList<>());
                for (float confidenceResult : confidenceResults) {
                    arrayLists.get(i).add(confidenceResult * (float) 100.0);
                }
            }

            for (int i = 0; i < arrayLists.size(); i++) {
                for (int j = 1; j < arrayLists.get(i).size(); j++) {
                    if (j == 1) {
                        // imageModels.get(i).setDrugs(IAVIS_CATEGORIES[j] + " : " + String.format("%.2f", arrayLists.get(i).get(j)) + "%");
                        imageModels.get(i).setDrugsValue(arrayLists.get(i).get(j));
                    } else if (j == 4) {
                        //imageModels.get(i).setPorn(IAVIS_CATEGORIES[j] + " : " + String.format("%.2f", arrayLists.get(i).get(j)) + "%");
                        imageModels.get(i).setPornValue(arrayLists.get(i).get(j));
                    } else if (j == 5) {
                        // imageModels.get(i).setSwimUnderwear(IAVIS_CATEGORIES[j] + " : " + String.format("%.2f", arrayLists.get(i).get(j)) + "%");
                        imageModels.get(i).setSwimUnderwearValue(arrayLists.get(i).get(j));
                    }
                }
            }

            databaseHelper.insertImages(imageModels);

            new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(IAIntentService.this, "Check completed", Toast.LENGTH_LONG).show());

            Intent intent = new Intent("CheckCompleted");
            sendOwnBroadcast(intent);
        }
    }

     void sendOwnBroadcast(Intent intent) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private ArrayList<String> getAllPathsFromGallery() {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    String IAViSDir;
    String folderName = "iavis";
    String categoryNames = "/iavis.4";
    private static String[] IAVIS_CATEGORIES;
    IAViSMobile iavisMobile;
    ArrayList<ArrayList<Float>> arrayLists = new ArrayList<>();

    private void initIAViS() {

        // check if the IAViS directory has any path mentioned...
        // If yes, only then conitnue.

        if (iavisMobile == null) {
            iavisMobile = new IAViSMobile();
            iavisMobile.setNumThreads(4);
        }

        int iavisInitializationResult = iavisMobile.initialize(IAViSDir);

        // Initialization returns -4 if the evaluation license has expired...
        if (0 <= iavisInitializationResult) {

            // Read the category names
            // Get the labels for the categories.
            try (BufferedReader br = new BufferedReader(new FileReader(IAViSDir + categoryNames))) {
                List<String> lines = new ArrayList<String>();
                String line;
                while ((line = br.readLine()) != null) {
                    lines.add(line);
                }
                // add the category names to the list so that we can use them for displaying result.
                IAVIS_CATEGORIES = lines.toArray(new String[0]);
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Context context = getApplicationContext();

            CharSequence toastMessage = "";

            switch (iavisInitializationResult) {
                case -4:
                    toastMessage = "Trial Licence - expires 30/06/2017";
                    break;
                case -5:
                    toastMessage = "iavis.1 file missing, corrupt, or not accessible.";
                    break;
                case -6:
                    toastMessage = "error handling iavis.1 file.";
                    break;
                case -7:
                    toastMessage = "iavis.2 file missing, corrupt, or not accessible.";
                    break;
                case -8:
                    toastMessage = "error handling iavis.2 file.";
                    break;
                case -9:
                    toastMessage = "iavis.3 file missing, corrupt, or not accessible.";
                    break;
                default:
                    break;
            }

            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, toastMessage, duration);
            toast.show();

            // Disable the camera as well as image selection buttons and clear the output label
        }


    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.w("IAIntentService", "onStartCommand callback called");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.w("IAIntentService", "onStart callback called");
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        Log.w("IAIntentService", "onDestroy callback called");
        super.onDestroy();
    }
}
