package com.example.kriuchkovtymofii.poc_iavis3.db;

import lombok.Data;

@Data
public class ImageModel {

    public static final String TABLE_NAME = "image_table";

    public static final String ID = "id";
    public static final String IMAGE_PATH = "imagePath";
    public static final String DRUGS = "drugs";
    public static final String PORN = "porn";
    public static final String SWIM_UNDERWARE = "swimUnderwear";

    int id;
    String imagePath;
    String drugs;
    Float drugsValue;
    String porn;
    float pornValue;
    String swimUnderwear;
    float swimUnderwearValue;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + IMAGE_PATH + " TEXT,"
                    + DRUGS + " REAL,"
                    + PORN + " REAL,"
                    + SWIM_UNDERWARE + " REAL"
                    + ")";

    public ImageModel(String imagePath) {
        this.imagePath = imagePath;
    }

    public ImageModel() {
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }

    public String getPorn() {
        return porn;
    }

    public void setPorn(String porn) {
        this.porn = porn;
    }

    public String getSwimUnderwear() {
        return swimUnderwear;
    }

    public void setSwimUnderwear(String swimUnderwear) {
        this.swimUnderwear = swimUnderwear;
    }

    public Float getDrugsValue() {
        return drugsValue;
    }

    public void setDrugsValue(float drugsValue) {
        this.drugsValue = drugsValue;
    }

    public float getPornValue() {
        return pornValue;
    }

    public void setPornValue(float pornValue) {
        this.pornValue = pornValue;
    }

    public float getSwimUnderwearValue() {
        return swimUnderwearValue;
    }

    public void setSwimUnderwearValue(float swimUnderwearValue) {
        this.swimUnderwearValue = swimUnderwearValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
