package com.example.kriuchkovtymofii.poc_iavis3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kriuchkovtymofii.poc_iavis3.db.DatabaseHelper;
import com.example.kriuchkovtymofii.poc_iavis3.db.ImageModel;
import com.example.kriuchkovtymofii.poc_iavis3.service.MainService;
import com.imageanalyzer.iavis_android_lib.IAViSMobile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class Main2Activity extends AppCompatActivity {

    String IAViSDir;
    String folderName = "iavis";
    String categoryNames = "/iavis.4";
    ProgressBar progressBar;
    IAViSMobile iavisMobile;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, SD_CARD_ACCESS_REQUEST);

        final Handler handler = new Handler();
        handler.postDelayed(() -> progressBar.setVisibility(View.INVISIBLE), 1000);

        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(new ArrayList<ImageModel>());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        TextView textView4 = findViewById(R.id.textView4);

        RVEmptyObserver observer = new RVEmptyObserver(mRecyclerView, textView4);
        mAdapter.registerAdapterDataObserver(observer);
        databaseHelper = new DatabaseHelper(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("CheckCompleted"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateList();
        }
    };

    int newFilesCnt;

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean("oldImagesAdded", false)) {
            updateList();
        }
    }

    void updateList() {
        ArrayList<String> paths = getAllShownImagesPath();

        ArrayList<ImageModel> imageModelsFormDB = databaseHelper.getAllNotes();

        ArrayList<ImageModel> imageModelsFormDBLocal = new ArrayList<>();

        for (int i = 0; i < imageModelsFormDB.size(); i++) {
            if (imageModelsFormDB.get(i).getDrugsValue() != -1.f) {
                imageModelsFormDBLocal.add(imageModelsFormDB.get(i));
            }
        }
        sortValues(imageModelsFormDBLocal);
        mAdapter.setmDataset(imageModelsFormDBLocal);
        if (imageModelsFormDBLocal.size() > 0) {
            Toast.makeText(this, imageModelsFormDBLocal.size() + " new images found", Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<String> newImages = new ArrayList<>();
        for (int i = 0; i < imageModelsFormDB.size(); i++) {
            if (!paths.contains(imageModelsFormDB.get(i).getImagePath())) {
                newImages.add(paths.get(i));
            }
        }

        newFilesCnt = newImages.size();

        int cnt = 10 - newFilesCnt < 0 ? 0 : 10 - newFilesCnt;

        if (newImages.size() == 0) {
            Toast.makeText(this, paths.size() + " old images found", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    void sortValues(ArrayList<ImageModel> imageModels) {
        Collections.sort(imageModels, (o1, o2) -> {
            if (o1.getId() == o2.getId())
                return 0;
            return o1.getId() < o2.getId() ? -1 : 1;
        });
    }


    RecyclerView mRecyclerView;
    MyAdapter mAdapter;

    public static final int SD_CARD_ACCESS_REQUEST = 5;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SD_CARD_ACCESS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, so do the
                    // iavis initialization related tasks.

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

                    if (!preferences.getBoolean("oldImagesAdded", false)) {
                        ArrayList<String> paths = getAllShownImagesPath();

                        ArrayList<ImageModel> imageModels = new ArrayList<>();
                        for (int i = 0; i < paths.size(); i++) {
                            imageModels.add(new ImageModel(paths.get(i)));
                        }

                        databaseHelper.insertImages(imageModels);

                        preferences.edit().putBoolean("oldImagesAdded", true).apply();
                    }

                    copyFileOrDir(folderName);


                    Intent myService = new Intent(Main2Activity.this, MainService.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(myService);
                    } else {
                        startService(myService);
                    }

                } else {

                    // permission denied, so disable the
                    // functionality that depends on this permission.
                    // Disable the image selection button and clear the output label
                }
                break;
            }
        }
    }

    private ArrayList<String> getAllShownImagesPath() {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = getFilesDir().getAbsolutePath() + "/" + path;
                File dir = new File(fullPath);
                if (!dir.exists())
                    dir.mkdir();
                for (int i = 0; i < assets.length; ++i) {
                    copyFileOrDir(path + "/" + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename) throws IOException {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = getFilesDir().getAbsolutePath() + "/" + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }

    }
}
