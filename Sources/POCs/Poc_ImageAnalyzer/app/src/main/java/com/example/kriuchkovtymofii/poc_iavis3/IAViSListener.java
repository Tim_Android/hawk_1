package com.example.kriuchkovtymofii.poc_iavis3;

public interface IAViSListener {
    void onTaskCompleted(int result);
}
