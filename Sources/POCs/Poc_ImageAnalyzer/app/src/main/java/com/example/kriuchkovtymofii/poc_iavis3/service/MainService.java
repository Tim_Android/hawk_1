package com.example.kriuchkovtymofii.poc_iavis3.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;

import com.example.kriuchkovtymofii.poc_iavis3.CallbackInterface;
import com.example.kriuchkovtymofii.poc_iavis3.IAIntentService;
import com.example.kriuchkovtymofii.poc_iavis3.R;
import com.example.kriuchkovtymofii.poc_iavis3.fileObserver.DirectoryFileObserver;
import com.example.kriuchkovtymofii.poc_iavis3.fileObserver.UriObserver;
import com.getkeepsafe.relinker.ReLinker;

import java.io.File;

public class MainService extends Service implements CallbackInterface {

    static DirectoryFileObserver directoryFileObserver;
    String path;

    public MainService() {
    }

    private static final String FRIDAY_CHANNEL_ID = "friday_channel_id";

    @Override
    public void onCreate() {
        super.onCreate();
        path = getGalleryPath();
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(FRIDAY_CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{0, 100, 50, 100});
            manager.createNotificationChannel(channel);
            builder = new Notification.Builder(this, FRIDAY_CHANNEL_ID);
        } else {
            builder = new Notification.Builder(this).setVibrate(new long[]{0, 100, 50, 100});
        }

          ReLinker.loadLibrary(MainService.this, "IAViS", new ReLinker.LoadListener() {
            @Override
            public void success() {
                Log.d("dasdasdasdasdasd", "success1: ");
            }

            @Override
            public void failure(Throwable t) {
                Log.d("dasdasdasdasdasd", "success2: ");
            }
        });
        ReLinker.loadLibrary(MainService.this, "IAViS_jni");

        startForeground(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int res = super.onStartCommand(intent, flags, startId);

        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M)
            startWatching();
        else {
            UriObserver observer = new UriObserver(new Handler(), this);
            Log.d("INSTANT", "registered content observer");

            this.getApplicationContext().getContentResolver().registerContentObserver(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false,
                    observer);

            Log.d("INSTANT", "registered content observer");
        }


        return Service.START_STICKY;
    }

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        MainService getService() {
            return MainService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return mBinder;
    }

    void startWatching() {
        directoryFileObserver = new DirectoryFileObserver(path, this) {
            @Override
            public void onEvent(int event, String path) {
                 super.onEvent(event, path);
            }
        };
        directoryFileObserver.startWatching();
    }

    String getGalleryPath() {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        if (!folder.exists()) {
            folder.mkdir();
        }

        return folder.getAbsolutePath();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MainServiceEvent22 ", "onDestroy: ");
    }

    @Override
    public void onEvent() {
        IAIntentService.startService(this, "start", "");
    }

}
