package com.example.kriuchkovtymofii.poc_iavis3;

import android.app.Application;
import android.content.Context;
import android.os.FileObserver;

import com.example.kriuchkovtymofii.poc_iavis3.fileObserver.DirectoryFileObserver;

import java.util.ArrayList;

public class App extends Application {

    private static ArrayList<FileObserver> mObservers = new ArrayList<FileObserver>();

    public static void addObserver(FileObserver observer){
        mObservers.add(observer);
    }

    public void removeObserver(FileObserver observer){
        mObservers.remove(observer);
    }

    public static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }

    DirectoryFileObserver directoryFileObserver;
}
