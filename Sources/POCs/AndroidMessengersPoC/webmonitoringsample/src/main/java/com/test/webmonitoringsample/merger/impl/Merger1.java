package com.test.webmonitoringsample.merger.impl;

import com.test.webmonitoringsample.merger.IMerger;

import java.util.List;

/**
 * Simple merger class that tries to find intersection of src with first and last dst elements
 * Add corresponding src subcollection to the dst beginning or ending
 * Do nothing if don't find this kind of intersection
 */
public class Merger1 implements IMerger {

    /**
     * <p> This merging algorithm compares all messages in src with edges messages in dst and add src messages to the list accordingly (if find any intersection)
     * <p> This merging algorithm works for Viber, WhatsApp, FbMessenger, FbMessengerLite
     *
     * @return See {@link com.test.webmonitoringsample.messengers.BaseMessengerProcessor#mergeMessages} for details about return value
     */
    @Override
    public <T> boolean merge(List<T> dst, List<T> src) {
        return merge(dst, src);
    }

    @Override
    public <T> boolean merge(List<T> dst, List<T> src, int dstElementsCountToMergeIn) {
        // add everything to empty list
        if (dst.isEmpty()) {
            dst.addAll(src);
            return true;
        }

        // try to add into beginning
        T firstDstElement = dst.get(0);
        int firstCrossIndexWithDstBeginElement = -1;
        for (int i = 0; i < src.size(); ++i) {
            if (src.get(i).equals(firstDstElement)) {
                firstCrossIndexWithDstBeginElement = i;
                break;
            }
        }
        if (firstCrossIndexWithDstBeginElement > 0) {
            dst.addAll(0, src.subList(0, firstCrossIndexWithDstBeginElement));
            return true;
        }

        // try to add into the end
        T lastDstElement = dst.get(dst.size() - 1);
        int firstCrossIndexWithDstEndElement = -1;
        for (int i = 0; i < src.size(); ++i) {
            if (src.get(i).equals(lastDstElement)) {
                firstCrossIndexWithDstEndElement = i;
                break;
            }
        }
        if (firstCrossIndexWithDstEndElement != -1
                && firstCrossIndexWithDstEndElement != src.size() - 1) {
            dst.addAll(dst.size(), src.subList(firstCrossIndexWithDstEndElement + 1, src.size()));
            return true;
        }

        return false;
    }
}
