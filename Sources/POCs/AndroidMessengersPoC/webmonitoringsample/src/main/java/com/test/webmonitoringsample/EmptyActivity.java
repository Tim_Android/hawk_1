package com.test.webmonitoringsample;

import android.app.Activity;
import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.util.Log;

import com.test.webmonitoringsample.activity.MainActivity;

import java.io.File;

public class EmptyActivity extends Activity {

    private MediaProjectionManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mgr = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        startActivityForResult(mgr.createScreenCaptureIntent(),
                MainActivity.REQUEST_SCREENSHOT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.REQUEST_SCREENSHOT) {
            if (resultCode == RESULT_OK) {
                Intent i = new Intent(this, ScreenshotService.class)
                        .putExtra(ScreenshotService.EXTRA_RESULT_CODE, resultCode)
                        .putExtra(ScreenshotService.EXTRA_RESULT_INTENT, data);

                // saveIntent(i, resultCode);

                File externalFilesDir = getExternalFilesDir(null);
                if (externalFilesDir != null) {
                    String STORE_DIRECTORY = externalFilesDir.getAbsolutePath() + "/screenshots/";
                    File storeDirectory = new File(STORE_DIRECTORY);
                    if (!storeDirectory.exists()) {
                        boolean success = storeDirectory.mkdirs();
                        if (!success) {
                            Log.e("dasdasd", "failed to create file storage directory.");
                            return;
                        }
                    }
                } else {
                    Log.e("asdasd", "failed to create file storage directory, getExternalFilesDir is null.");
                    return;
                }


                startService(i);
                finish();
            }
        }
    }
}
