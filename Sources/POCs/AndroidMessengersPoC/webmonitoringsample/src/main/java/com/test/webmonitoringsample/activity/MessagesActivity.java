package com.test.webmonitoringsample.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.adapter.MessagesAdapter;
import com.test.webmonitoringsample.messengers.MessengerTools;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import static com.test.webmonitoringsample.activity.ConversationsActivity.MESSENGER;
import static com.test.webmonitoringsample.messengers.MessengerProcessor.g_messengersInfo;

public class MessagesActivity extends Activity {
    private static final String INDEX = "INDEX";

    private RecyclerView.Adapter adapter;
    private MessengerInfo messengerInfo;
    private Conversation conversation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messages);
        TextView conversationTitile = (TextView) findViewById(R.id.conversationTitle);
        int conversationIndex = getIntent().getIntExtra(INDEX, -1);

        MessengerInfo.Messenger messenger = (MessengerInfo.Messenger) getIntent().getSerializableExtra(MESSENGER);
        messengerInfo = MessengerTools.getMessengerInfo(g_messengersInfo, messenger);
        conversation = messengerInfo.conversations.get(conversationIndex);
        conversationTitile.setText(conversation.title);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.messagesRecycleView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        setTitle(messenger.name() + " messages");

        adapter = new MessagesAdapter(conversation);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        HelperMethods.warnAccessibilityDisabled(this);
    }

    public static void start(Context context, int conversationIndex, MessengerInfo.Messenger messenger) {
        context.startActivity(new Intent(context, MessagesActivity.class).putExtra(INDEX, conversationIndex).putExtra(MESSENGER, messenger));
    }

    public void clearAll(View view) {
        messengerInfo.clear(conversation);
        adapter.notifyDataSetChanged();
    }
}
