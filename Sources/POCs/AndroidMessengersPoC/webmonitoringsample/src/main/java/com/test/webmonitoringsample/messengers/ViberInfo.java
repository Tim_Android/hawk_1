package com.test.webmonitoringsample.messengers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ViberInfo {
    public List<Conversation> conversations = new ArrayList<>();

    public static class Conversation {
        public String title = null;
        public LinkedList<Message> messages = new LinkedList<>();

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Conversation c = (Conversation) obj;
            return c.title.equals(this.title);
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + title.hashCode();
            result = 31 * result + messages.hashCode();
            return result;
        }
    }

    public static class Message {
        public String text = "";
        public Boolean isIncoming = false; // null if don't know
        public String time = "";

        public Message(String text, Boolean isIncoming, String time) {
            this.text = text;
            this.isIncoming = isIncoming;
            this.time = time;
        }

        @Override
        public boolean equals(Object obj) {
            // don't use direction cause sometimes we don't know it for all the messages
            // but we can merge with global list and then resolve direction as well
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Message m = (Message) obj;
            return m.text.equals(this.text)
                    && m.time.equals(this.time);
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + text.hashCode();
            result = 31 * result + time.hashCode();
            return result;
        }
    }
}
