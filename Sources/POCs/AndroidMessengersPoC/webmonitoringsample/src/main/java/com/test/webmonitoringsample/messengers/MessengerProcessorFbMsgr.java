package com.test.webmonitoringsample.messengers;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_OBJECT_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_STRTING_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerTools.getCommonDateAsStringFromDate;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.FB_MESSENGER;

public class MessengerProcessorFbMsgr extends BaseMessengerProcessor {

    public static final CharSequence PACKAGE_NAME = "com.facebook.orca";

    //public static final String MESSAGES_ROOT_PATH = "$.ROOT[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.ListView')].CHILDS[?(@.mClassName=='android.view.ViewGroup')]";
    public static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/messages_list')].CHILDS[?(@.mClassName=='android.view.ViewGroup')]";

    //2017
    private static final SimpleDateFormat YEAR_DATE_FORMAT = new SimpleDateFormat("yyyy");
    //2017
    private static final SimpleDateFormat MONTH_DATE_FORMAT = new SimpleDateFormat("MMM");
    //2017
    private static final SimpleDateFormat WEEK_DATE_FORMAT = new SimpleDateFormat("MMM");

    //16:55
    private static SimpleDateFormat ONLY_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");

    //MON AT 13:51
    private static SimpleDateFormat DAY_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("EEE 'AT' HH:mm");

    // JUN 7 AT 09:51
    private static SimpleDateFormat MONTH_DATE_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("MMM dd 'AT' HH:mm");
    // 2017 JUN 7 AT 09:51
    private static SimpleDateFormat TMP_YEAR_MONTH_DATE_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("yyyy MMM dd 'AT' HH:mm");

    // MAY 23, 2016 AT 22:24
    private static SimpleDateFormat MONTH_DATE_YEAR_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy 'AT' HH:mm");

    @Override
    public MessengerInfo.Messenger getCurrentMessenger() {
        return FB_MESSENGER;
    }

    @Override
    public Boolean getIsImage(JsonArray messagesJsonArray, int i) {
        String isImagePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/message_images')]", i);
        JsonArray isImageJsonArray = JsonPath.read(messagesJsonArray, isImagePath);
        List<Object> isImageList = AccessibilityNodeJsonSerializer.gson.fromJson(isImageJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isImage = isImageList != null && !isImageList.isEmpty();
        return isImage;
    }

    @Override
    public String getSender(JsonArray messagesJsonArray, int i) {
        String senderPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText.mText", i);

        JsonArray sendersJsonArray = JsonPath.read(messagesJsonArray, senderPath);
        List<String> sendersList = AccessibilityNodeJsonSerializer.gson.fromJson(sendersJsonArray, LIST_STRTING_TYPE.getType());
        String sender = getFirstStringOrNull(sendersList);
        return sender;
    }

    @Override
    public Boolean getIsIncomming(JsonArray messagesJsonArray, int i) {
        String statusPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/delivery_status_view')]", i);
        JsonArray statusJsonArray = JsonPath.read(messagesJsonArray, statusPath);
        List<Object> statusList = AccessibilityNodeJsonSerializer.gson.fromJson(statusJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isIncomming = statusList == null || statusList.isEmpty();
        return isIncomming;
    }

    @Override
    public String getTime(JsonArray messagesJsonArray, int i) {
        // same as visible date
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/message_divider_text')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String visibleDate = getFirstStringOrNull(dateList);

        String visibleTime = null;
        if (visibleDate != null) {
            int visibleDateLength = visibleDate.length();
            visibleTime = visibleDate.subSequence(visibleDateLength - 5, visibleDateLength).toString();
        }

        return visibleTime;
    }

    @Override
    public String getText(JsonArray messagesJsonArray, int i, boolean isImage) {
        String textPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/message_text')].mText.mText", i);

        JsonArray textJsonArray = JsonPath.read(messagesJsonArray, textPath);
        List<String> messageList = AccessibilityNodeJsonSerializer.gson.fromJson(textJsonArray, LIST_STRTING_TYPE.getType());
        String text = getFirstStringOrNull(messageList);
        return text;
    }

    @Override
    public String getVisibleDate(JsonArray messagesJsonArray, int i) {
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/message_divider_text')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String date = getFirstStringOrNull(dateList);

        if (date != null) {
            date = normalizeVisibleDate(date.toUpperCase());
        }

        return date;
    }

    @Override
    public Boolean getIsGroup(Object entireScreen) {
        return null;
    }

    @Override
    public List<String> getParticipants(Object entireScreen) {
        return null;
    }

    @Override
    public JsonArray getMessagesJsonArray(Object entireScreen) {
        JsonArray messagesJsonArray = JsonPath.read(entireScreen, MESSAGES_ROOT_PATH);
        return messagesJsonArray;
    }

    @Override
    public String getTitle(Object entireScreen) {
        //String titlePath = "$.ROOT[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.view.ViewGroup')].CHILDS[?(@.mClassName=='android.view.View')].mContentDescription.mText";
        //String titlePath = "$.ROOT[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.view.ViewGroup')].CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/thread_title_name')].mContentDescription.mText";
        String titlePath = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.facebook.orca:id/thread_title_name')].mContentDescription.mText";
        List<String> titles = JsonPath.parse(entireScreen).read(titlePath, LIST_STRTING_TYPE);
        String title = getFirstStringOrNull(titles);
        return title;
    }

    @Override
    public List<String> resolveParticipants(String participantsAsString) {
        return null;
    }

    @Override
    public List<String> getParticipantsAsListList(String participantsAsString) {
        return null;
    }

    @Override
    public FilesInfo getSdCardImages() {
        return null;
    }

    @Override
    public String normalizeVisibleDate(String date) {
        return normalizeVisibleDateForFb(date);
    }

    public static String normalizeVisibleDateForFb(String date) {
        Date currentDate = new Date();
        String normalizedDateAsString = null;

        Date normalizedDate = getNormalizedDate(ONLY_TIME_VISIBLE_DATE_FORMAT, date);
        if (normalizedDate != null) {
            normalizedDateAsString = getCommonDateAsStringFromDate(currentDate);
            return normalizedDateAsString;
        }

        normalizedDate = getNormalizedDate(DAY_TIME_VISIBLE_DATE_FORMAT, date);
        if (normalizedDate != null) {
            // https://stackoverflow.com/a/12783806
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - normalizedDate.getDay() - 1));
            normalizedDateAsString = getCommonDateAsStringFromDate(cal.getTime());
            return normalizedDateAsString;
        }

        normalizedDate = getNormalizedDate(MONTH_DATE_TIME_VISIBLE_DATE_FORMAT, date);
        if (normalizedDate != null) {
            String year = YEAR_DATE_FORMAT.format(currentDate);
            normalizedDate = getNormalizedDate(TMP_YEAR_MONTH_DATE_TIME_VISIBLE_DATE_FORMAT, year + " " + date);
            normalizedDateAsString = getCommonDateAsStringFromDate(normalizedDate);
            return normalizedDateAsString;
        }

        normalizedDate = getNormalizedDate(MONTH_DATE_YEAR_TIME_VISIBLE_DATE_FORMAT, date);
        if (normalizedDate != null) {
            normalizedDateAsString = getCommonDateAsStringFromDate(normalizedDate);
            return normalizedDateAsString;
        }

        return date;
    }

    private static Date getNormalizedDate(SimpleDateFormat potentialDateFormat, String date) {
        Date result = null;
        try {
            result = potentialDateFormat.parse(date);
        } catch (ParseException e) {
            //Log.d("ERROR", e.getMessage());
            //e.printStackTrace();
        }
        return result;
    }
}
