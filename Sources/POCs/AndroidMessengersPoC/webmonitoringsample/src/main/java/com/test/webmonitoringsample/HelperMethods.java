package com.test.webmonitoringsample;

import android.accessibilityservice.AccessibilityService;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.test.webmonitoringsample.activity.MainActivity;
import com.test.webmonitoringsample.browsers.UrlInfo;
import com.test.webmonitoringsample.messengers.AccessibilityNodeJsonSerializer;
import com.test.webmonitoringsample.messengers.FirstSearchResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import lombok.Cleanup;

import static android.R.attr.left;
import static android.R.attr.right;
import static com.test.webmonitoringsample.AccessibilityNodeInfoGuard.SAFE_RECYCLE;

public class HelperMethods {
    private static String TAG = MainActivity.TAG;

    // enough for sample proj...
    public static List<UrlInfo> urls = new ArrayList<>();


    /**
     * Note: It is a client responsibility to recycle both incommingNode and return value by calling AccessibilityNodeInfo#recycle() to avoid creating of multiple instances.
     * <p> Client should check FirstSearchResult.isIncomingParameter to avoid multiple recycling on the same nodeInfo instance
     */
    public static FirstSearchResult breadthFirstSearchNodeProcessingBest(AccessibilityNodeInfo incomingNode, INodeProcessor processor) {
        if (incomingNode == null) {
            return new FirstSearchResult();
        }

        final List<AccessibilityNodeInfoWithDepth> unvisited = new ArrayList<>();
        int depth = 0;

        // process incoming node
        // Don't cleanup incomming nodeInfo -- caller MUST do it
        if (breadthFirstSearchNodeProcessingImplBest(incomingNode, depth, processor, unvisited)) {
            return new FirstSearchResult(incomingNode, true, depth);
        }

        // process child nodes
        try {
            while (!unvisited.isEmpty()) {
                AccessibilityNodeInfoWithDepth nodeWithDepth = unvisited.remove(0);
                AccessibilityNodeInfo node = nodeWithDepth.node;
                depth = nodeWithDepth.depth;

                @Cleanup(SAFE_RECYCLE)
                AccessibilityNodeInfoGuard guard = new AccessibilityNodeInfoGuard(node);

                if (breadthFirstSearchNodeProcessingImplBest(node, depth, processor, unvisited)) {
                    guard.dontRecycle();
                    return new FirstSearchResult(node, false, depth);
                }
            }
            return new FirstSearchResult();
        } finally {
            // Recycle all of not processed nodes
            for (AccessibilityNodeInfoWithDepth node : unvisited) {
                if (node.node != null) {
                    node.node.recycle();
                }
            }
        }
    }

    private static boolean breadthFirstSearchNodeProcessingImplBest(AccessibilityNodeInfo node, int depth, INodeProcessor processor, final List<AccessibilityNodeInfoWithDepth> unvisited) {
        if (node == null) {
            return false;
        }

        if (processor.process(node, depth)) {
            return true;
        }

        for (int i = 0; i < node.getChildCount(); i++) {
            AccessibilityNodeInfo child = node.getChild(i);
            unvisited.add(new AccessibilityNodeInfoWithDepth(child, depth + 1));
        }
        return false;
    }

    public static boolean breadthFirstSearchNodeProcessing(AccessibilityNodeInfo nodeInfo, INodeProcessor processor) {
        if (nodeInfo == null) {
            return false;
        }

        List<AccessibilityNodeInfoWithDepth> unvisited = new ArrayList<>();

        // Don't cleanup incomming nodeInfo -- caller MUST do it
        if (processNodeWithDepthBreadth(nodeInfo, 0, unvisited, processor)) {
            return true;
        }

        try {
            while (!unvisited.isEmpty()) {
                AccessibilityNodeInfoWithDepth nodeWithDepth = unvisited.remove(0);
                @Cleanup("recycle") AccessibilityNodeInfo node = nodeWithDepth.node;
                int depth = nodeWithDepth.depth;

                if (processNodeWithDepthBreadth(node, depth, unvisited, processor)) {
                    return true;
                }
            }
            return false;
        } finally {
            for (AccessibilityNodeInfoWithDepth node : unvisited) {
                if(node.node != null) {
                    node.node.recycle();
                }
            }
        }
    }

    private static boolean processNodeWithDepthBreadth(AccessibilityNodeInfo node, int depth, List<AccessibilityNodeInfoWithDepth> unvisited, INodeProcessor processor) {
        if (node == null) {
            return false;
        }

        if (processor.process(node, depth)) {
            return true;
        }

        for (int i = 0; i < node.getChildCount(); i++) {
            AccessibilityNodeInfo child = node.getChild(i);
            unvisited.add(new AccessibilityNodeInfoWithDepth(child, depth + 1));
        }
        return false;
    }

    public static boolean depthFirstSearchNodeProcessing(AccessibilityNodeInfo nodeInfo, INodeProcessor processor, int currentDepth) {
        if (nodeInfo == null) {
            return false;
        }

        boolean found = processor.process(nodeInfo, currentDepth);
        if (found) {
            return true;
        }

        for (int i = 0; i < nodeInfo.getChildCount(); i++) {
            @Cleanup("recycle") AccessibilityNodeInfo child = nodeInfo.getChild(i);
            found = depthFirstSearchNodeProcessing(child, processor, currentDepth + 1);
            if(found) {
                return true;
            }
        }

        return false;
    }

    /**
     * Note: It is a client responsibility to recycle both incommingNode and return value by calling AccessibilityNodeInfo#recycle() to avoid creating of multiple instances.
     * <p> Client should check FirstSearchResult.isIncomingParameter to avoid multiple recycling on the same nodeInfo instance
     */
    public static FirstSearchResult depthFirstSearchNodeProcessingBest(AccessibilityNodeInfo node, INodeProcessor processor, int currentDepth) {
        if(node == null) {
            return new FirstSearchResult();
        }

        @Cleanup(SAFE_RECYCLE)
        AccessibilityNodeInfoGuard guard = new AccessibilityNodeInfoGuard(node);
        if (currentDepth == 0) {
            guard.dontRecycle();
        }

        boolean found = processor.process(node, currentDepth);
        if (found) {
            guard.dontRecycle();
            return new FirstSearchResult(node, currentDepth == 0, currentDepth);
        }

        for (int i = 0; i < node.getChildCount(); i++) {
            AccessibilityNodeInfo child = node.getChild(i);
            if(child == null) {
                continue;
            }

            FirstSearchResult result = depthFirstSearchNodeProcessingBest(child, processor, currentDepth + 1);
            if(result.nodeInfo != null) {
                return result;
            }
        }

        return new FirstSearchResult();
    }

    public static void printNodeTreeBreadth(AccessibilityNodeInfo nodeInfo) {
        breadthFirstSearchNodeProcessing(nodeInfo, new INodeProcessor() {
            @Override
            public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
                Log.d("BBB", depth + "__" + nodeInfo.getClassName() + ", " + nodeInfo.getViewIdResourceName() + ", " + nodeInfo.getText());
                return false;
            }
        });
    }

    public static void printNodeTreeDepth(AccessibilityNodeInfo nodeInfo, final String type) {
        depthFirstSearchNodeProcessing(nodeInfo, new INodeProcessor() {
            @Override
            public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
                Integer rowIndex = null;
                AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo = nodeInfo.getCollectionItemInfo();
                if (collectionItemInfo != null) {
                    rowIndex = collectionItemInfo.getRowIndex();
                }

                // to know message balloon position in the screen
                Rect rectScreen = new Rect();
                nodeInfo.getBoundsInScreen(rectScreen);

                Rect rectParent = new Rect();
                nodeInfo.getBoundsInParent(rectParent);

                Log.d("DDD", getIndentation(depth) + depth + "__" + nodeInfo.getClassName() + ", " + nodeInfo.getViewIdResourceName() + ", " + nodeInfo.getText() + ", " + rowIndex + ", sl:" + rectScreen.left + ", sr:" + rectScreen.right + ", pl:" + rectParent.left+ ", pr:" + rectParent.right);
                return false;
            }
        }, 0);
    }

    public static void printJson(String rootNode) {
        Log.d("VVV", rootNode);
    }

    private static String getIndentation(int depth) {
        return new String(new char[depth]).replace("\0", " ");
    }

    public static void printExecutionTime(String message, long startTime, long endTime) {
        Log.d("TTT", "TIME=" + ((endTime - startTime)/1000000) + " " + message);
    }

    public static long getTime() {
        return System.nanoTime();
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void printEventInfo(AccessibilityEvent event) {
        Log.d("ZZZ2", "_evType=" + AccessibilityEvent.eventTypeToString(event.getEventType())
//                + ", act=" + event.getAction()
//                + ", add=" + event.getAddedCount()
//                + ", bef=" + event.getBeforeText()
//                + ", cls=" + event.getClassName()
//                + ", cont=" + event.getContentDescription()
//                + ", curInd=" + event.getCurrentItemIndex()
//                + ", contChType=" + event.getContentChangeTypes()
//                + ", from=" + event.getFromIndex()
//                + ", itCount=" + event.getItemCount()
//                + ", maxScrlY=" + event.getMaxScrollX()
//                + ", maxScrlY=" + event.getMaxScrollY()
//                + ", mvmGranul=" + event.getMovementGranularity()
//                + ", parcel=" + event.getParcelableData()
//                //+ ", rec=" + event.getRecord(int index)
//                + ", recCount=" + event.getRecordCount()
//                + ", removed=" + event.getRemovedCount()
//                + ", scrlX=" + event.getScrollX()
//                + ", scrlY=" + event.getScrollY()
//                + ", text=" + event.getText()
//                + ", toInd=" + event.getToIndex()
//                + ", windId=" + event.getWindowId()
//                + ", isChk=" + event.isChecked()
//                + ", isEnbl=" + event.isEnabled()
//                + ", isFull=" + event.isFullScreen()
//                + ", isPwd=" + event.isPassword()
//                + ", isScrl=" + event.isScrollable()
//                // + ", XXX=" + event.obtain()
//                // + ", XXX=" + event.obtain(event.getRecord(0))
//                // + ", XXX=" + event.recycle()
                + ", package=" + (event.getPackageName() == null ? "null" : event.getPackageName()));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void printNodeInfo(AccessibilityNodeInfo nodeInfo) {
        Log.d("ZZZ1",
                "start=" + nodeInfo.getTextSelectionStart()
                        + ", end" + nodeInfo.getTextSelectionEnd()
//                + ", accessFocused=" + nodeInfo.isAccessibilityFocused()
//                + ", dismiss=" + nodeInfo.isDismissable()
//                + ", canPopup=" + Boolean.toString(nodeInfo.canOpenPopup())
//                + ", describe=" + nodeInfo.describeContents()
//                + ", actions=" + nodeInfo.getActionList()
//                + ", collection=" + nodeInfo.getCollectionInfo()
//                + ", collectionIt=" + nodeInfo.getCollectionItemInfo()
//                + ", contentDesc=" + nodeInfo.getContentDescription()
//                + ", extras=" + nodeInfo.getExtras()
//                + ", input=" + nodeInfo.getInputType()
//                + ", labelFor=" + nodeInfo.getLabelFor() // don't forget to recycle!
//                + ", live=" + nodeInfo.getLiveRegion()
//                + ", movement=" + nodeInfo.getMovementGranularities()
//                + ", range=" + nodeInfo.getRangeInfo()
        );
    }

    /**
     * Removes # sign (anchor) from url if need as well as ending "/" character
     *
     * @param url -- url
     * @return normalized url
     */
    private static String getNormalizedUrl(String url) {
        int anchorIndex = url.indexOf('#');
        String noAnchorUrl = anchorIndex == -1 ? url : url.substring(0, anchorIndex);
        return noAnchorUrl.endsWith("/") ? noAnchorUrl.substring(0, noAnchorUrl.length() - 1) : noAnchorUrl;
    }

    public static boolean isUrlValid(CharSequence url) {
        return url != null && Patterns.WEB_URL.matcher(url).matches();
    }

    /**
     * @param nodeInfo -- nodeInfo
     * @param event    -- accessibility event for debugging purposes
     * @return true if url was found (regardless added or not). False otherwise.
     */
    public static boolean addUrlIfNeed(AccessibilityNodeInfo nodeInfo, AccessibilityEvent event) {
        CharSequence nodeText = nodeInfo.getText();
        String visibleUrl = HelperMethods.getNormalizedUrl(nodeText.toString());
        String lastAccessedUrl = urls.isEmpty() ? null : urls.get(urls.size() - 1).name;
        if (!isLastSavedUrlEqualsToVisibleUrl(visibleUrl, lastAccessedUrl)) {
            urls.add(new UrlInfo(nodeInfo.getPackageName().toString(), visibleUrl, System.currentTimeMillis()));

//            HelperMethods.printNodeInfo(nodeInfo);
//            HelperMethods.printEventInfo(event);
//            HelperMethods.printNodeTreeDepth(event.getSource(), AccessibilityEvent.eventTypeToString(event.getEventType()));
            @Cleanup("recycle") AccessibilityNodeInfo root = HelperMethods.getRoot(nodeInfo);
            HelperMethods.printNodeTreeDepth(root, AccessibilityEvent.eventTypeToString(event.getEventType()));

            Log.d("ZZZ3", visibleUrl + ", " + nodeInfo.getViewIdResourceName() + ":" + HelperMethods.getNearbyParents(nodeInfo));
        }

        return true;
    }

    private static String getNearbyParents(AccessibilityNodeInfo nodeInfo) {
        String leafClassName = String.valueOf(nodeInfo.getClassName());
        @Cleanup("recycle") AccessibilityNodeInfo parent = nodeInfo.getParent();
        @Cleanup("recycle") AccessibilityNodeInfo grandPa = parent == null ? null : parent.getParent();

        return String.valueOf(grandPa.getClassName()) + "_" + grandPa.getViewIdResourceName()
                + "__" + String.valueOf(parent.getClassName()) + "_" + parent.getViewIdResourceName()
                + "__" + String.valueOf(nodeInfo.getClassName()) + "_" + nodeInfo.getViewIdResourceName();
    }

    /**
     * Checks whether last saved url and visible url are belong to the same web link
     * i.e. cats.com should be equal to http://cats.com and wise versa
     * the same with "https" and "www" (regardless of whether they are presented in any url or not).
     *
     * @param visibleUrl      -- visibleUrl
     * @param lastAccessedUrl -- can be null in case of empty urls collection
     * @return Return true if last saved url and visible url are belong to the same link
     */
    private static boolean isLastSavedUrlEqualsToVisibleUrl(String visibleUrl, String lastAccessedUrl) {
        // return visibleUrl.equals(lastAccessedUrl); // doesn't work for all cases for com.android.native browser
        return lastAccessedUrl != null && (visibleUrl.length() > lastAccessedUrl.length() ? visibleUrl.endsWith(lastAccessedUrl) : lastAccessedUrl.endsWith(visibleUrl));
    }

    public static boolean hasAccessibilityNodeInfoWithId(AccessibilityNodeInfo node, String viewId) {
        List<AccessibilityNodeInfo> result = node.findAccessibilityNodeInfosByViewId(viewId);
        for (AccessibilityNodeInfo n : result) {
            n.recycle();
        }

        return !result.isEmpty();
    }

    public static AccessibilityNodeInfo getRootNode(AccessibilityEvent event) {
        AccessibilityNodeInfo initialNodeInfo = event.getSource();
        if (initialNodeInfo == null) {
            return null;
        }
        @Cleanup(SAFE_RECYCLE)
        AccessibilityNodeInfoGuard initialNodeInfoGuard = new AccessibilityNodeInfoGuard(initialNodeInfo);

        AccessibilityNodeInfo rootNodeInfo = getRoot(initialNodeInfo);
        if (rootNodeInfo == null) {
            initialNodeInfoGuard.dontRecycle();
        }

        AccessibilityNodeInfo root = rootNodeInfo == null ? initialNodeInfo : rootNodeInfo;
        return root;
    }

    public static String readableEventName(AccessibilityEvent event) {
        return AccessibilityEvent.eventTypeToString(event.getEventType());
    }

    public static File[] getFilesOnFilesystem(String dir) {
        String sdCardDirString = Environment.getExternalStorageDirectory().toString();
        String targetDirString = sdCardDirString + dir;

        File targetDirFile = new File(targetDirString);

        File[] files = targetDirFile.listFiles();

        return files;
    }

    private static class AccessibilityNodeInfoWithDepth {
        AccessibilityNodeInfo node;
        int depth;

        AccessibilityNodeInfoWithDepth(AccessibilityNodeInfo node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }

    /**
     * @param node
     * @return Root node or null if incoming node is root.
     * <p> Note: It is a client responsibility to recycle the received info by calling recycle() to avoid creating of multiple instances.
     */
    public static AccessibilityNodeInfo getRoot(AccessibilityNodeInfo node) {
        if (node == null) {
            return null;
        }

        AccessibilityNodeInfo directParent = node.getParent();
        if (directParent == null) {
            return null;
        }

        return getRootRecursy(directParent, directParent.getParent());
    }

    private static AccessibilityNodeInfo getRootRecursy(AccessibilityNodeInfo node, AccessibilityNodeInfo parent) {
        if (parent == null) {
            return node;
        } else {
            node.recycle();
            return getRootRecursy(parent, parent.getParent());
        }
    }


    public static JsonObject treeToJson(AccessibilityNodeInfo root) {
        AccessibilityNodeJsonSerializer jsonSerrializer = new AccessibilityNodeJsonSerializer();
        JsonObject jsonObject = jsonSerrializer.getJsonObject();
        depthFirstSearchNodeProcessingWithJson(root, jsonSerrializer, 0, jsonObject);

        return jsonSerrializer.getJsonObject();
    }

    public static boolean depthFirstSearchNodeProcessingWithJson(AccessibilityNodeInfo nodeInfo, AccessibilityNodeJsonSerializer processor, int currentDepth, JsonObject parent) {
        if (nodeInfo == null) {
            return false;
        }

        JsonElement addedElement = processor.process(nodeInfo, currentDepth, parent);

        for (int i = 0; i < nodeInfo.getChildCount(); i++) {
            @Cleanup("recycle") AccessibilityNodeInfo child = nodeInfo.getChild(i);
            if (child == null) {
                continue;
            }
            depthFirstSearchNodeProcessingWithJson(child, processor, currentDepth + 1, addedElement.getAsJsonObject());
        }

        return false;
    }

    public static boolean isAccessibilityServiceEnabled(Context context) {
        Class<? extends AccessibilityService> accessibilityService = MyAccessibilityService.class;
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null || enabledServicesSetting.isEmpty())
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    public static void warnAccessibilityDisabled(Context context) {
        if (!isAccessibilityServiceEnabled(context)) {
            /* https://developer.android.com/reference/android/provider/Settings.Secure.html */
            Toast.makeText(context, "Enable accessibility: Settings -> Accessibility -> activity.minitor", Toast.LENGTH_LONG).show();
        }
    }

}
