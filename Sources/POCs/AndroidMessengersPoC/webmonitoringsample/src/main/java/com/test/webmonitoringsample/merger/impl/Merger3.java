package com.test.webmonitoringsample.merger.impl;

import com.test.webmonitoringsample.merger.IMerger;

import java.util.Collections;
import java.util.List;

public class Merger3 implements IMerger {
    private static final int DST_MERGE_INTERVAL = 50;

    /**
     *
     * @param dst -- where merge
     * @param src -- what merge
     * @return
     * <p> {@code true} -- if some elements from src is merged into dst.
     * <p> {@code false} if none elements were merged because all of them had been already in dst.
     */
    @Override
    public <T> boolean merge(List<T> dst, List<T> src) {
        return merge(dst, src, DST_MERGE_INTERVAL);
    }

    /**
     *
     * @param dst -- where merge
     * @param src -- what merge
     * @param dstElementsCountToMergeIn -- specify how many elements from the end of dst should be used as dst subcollection for merging. -1 if full dst collection should be used..
     * @return
     * <p> {@code true} -- if some elements from src is merged into dst.
     * <p> {@code false} if none elements were merged because all of them had been already in dst.
     */
    @Override
    public <T> boolean merge(List<T> dst, List<T> src, int dstElementsCountToMergeIn) {
        if (src == null || dst == null) {
            return false;
        }

        if (src.isEmpty()) {
            return false;
        }

        if (dst.isEmpty()) {
            return dst.addAll(src);
        }

        List<T> dstEndingMergeInterval = dst;
        if (dstElementsCountToMergeIn > 0 && dst.size() > dstElementsCountToMergeIn) {
            dstEndingMergeInterval = dst.subList(dst.size() - dstElementsCountToMergeIn, dst.size());
        }

        // try to find full interception
        int indexOfSublist = Collections.indexOfSubList(dstEndingMergeInterval, src);
        if (indexOfSublist >= 0) {
            return false;
        }

//        // try to find partial intersection of src with the dst beginning (with edge element)
//        indexOfSublist = src.lastIndexOf(dst.get(0));
//        if (indexOfSublist >= 0) {
//            List<T> beginningSublist = src.subList(0, indexOfSublist);
//            return dst.addAll(0, beginningSublist);
//        }
//
//        // try to find partial intersection of src with the dst ending (with edge element)
//        indexOfSublist = src.indexOf(dst.get(dst.size() - 1));
//        if (indexOfSublist >= 0) {
//            List<T> endingSublist = src.subList(indexOfSublist + 1, src.size());
//            return dst.addAll(endingSublist);
//        }


        int middleSrcIndex = src.size() / 2;
        boolean isMergedInBeginning = merge3PartialBeginningRecursy(dstEndingMergeInterval, src, middleSrcIndex);
        if (isMergedInBeginning) {
            return true;
        }

        boolean isMergedInEnding = merge3PartialEndingRecursy(dstEndingMergeInterval, src, middleSrcIndex);
        if (isMergedInEnding) {
            return true;
        }

        return dstEndingMergeInterval.addAll(src);
    }

    /**
     * <p> find intersection of src beginning with dst using recursion
     * <p> Assume
     * <p> src is [0,1,2,3,4,5,6,7,8]
     * <p> and dst is [8,9,10,11,12,13,14,15,16,17,18]
     * <p> We should try to find intersection from beginning of dst using next src substrings
     * <p> [4,5,6,7,8]
     * <p> [6,7,8]
     * <p> [7,8]
     * <p> [8]
     *
     * @param dst
     * @param src
     * @param <T>
     * @return
     */
    private static <T> boolean merge3PartialBeginningRecursy(List<T> dst, List<T> src, int middleSrcIndex) {
        if (middleSrcIndex != 0) {
            List<T> endingSrcSublist = src.subList(middleSrcIndex, src.size());
            int indexOfSublist = Collections.indexOfSubList(dst, endingSrcSublist);
            if (indexOfSublist < 0) {
                int middleSrcSubstringIndex = endingSrcSublist.size() / 2;
                if (middleSrcSubstringIndex == 0) {
                    return false;
                }

                return merge3PartialBeginningRecursy(dst, src, middleSrcIndex + middleSrcSubstringIndex);
            } else {
                // intersection was found -- merge it

                //////////////////////////////////
                ///////////skip duplicates before merging (clarify sublist!)
                int elementsToCheckDuplicates = endingSrcSublist.size() - 1;
                for (int i = 0; i < elementsToCheckDuplicates; ++i) {

                    int srcIndex = middleSrcIndex;
                    int dstIndex = indexOfSublist + middleSrcIndex;

                    // verify collection bounds
                    if ((middleSrcIndex > src.size() - 1) || (dstIndex > dst.size() - 1)) {
                        break;
                    }

                    T srcValue = src.get(srcIndex);
                    T dstValue = dst.get(dstIndex);
                    if (srcValue.equals(dstValue)) {
                        ++middleSrcIndex;
                    } else {
                        break;
                    }
                }
                //////////////////////////////////

                List<T> sublistToMerge = src.subList(0, middleSrcIndex);
                dst.addAll(indexOfSublist, sublistToMerge);
                return true;
            }
        }

        return false;
    }

    /**
     * <p> find intersection of src beginning with dst using recursion
     * <p> Assume
     * <p> src is [8,9,10,11,12,13,14,15,16,17,18]
     * <p> and dst is [0,1,2,3,4,5,6,7,8]
     * <p> We should try to find intersection from beginning of dst using next src substrings
     * <p> [8,9,10,11,12]
     * <p> [8,9,10]
     * <p> [8,9]
     * <p> [8]
     *
     * @param dst
     * @param src
     * @param <T>
     * @return
     */
    private static <T> boolean merge3PartialEndingRecursy(List<T> dst, List<T> src, int middleSrcIndex) {
        if (middleSrcIndex != 0) {
            List<T> startingSrcSublist = src.subList(0, middleSrcIndex);
            int indexOfSublist = Collections.lastIndexOfSubList(dst, startingSrcSublist);
            if (indexOfSublist < 0) {
                int middleSrcSubstringIndex = startingSrcSublist.size() / 2;
                if (middleSrcSubstringIndex == 0) {
                    return false;
                }

                return merge3PartialEndingRecursy(dst, src, middleSrcIndex - middleSrcSubstringIndex);
            } else {
                // intersection was found -- merge it

                //////////////////////////////////
                ///////////skip duplicates before merging (clarify sublist!)
                int elementsToCheckDuplicates = src.size() - middleSrcIndex;
                for (int i = 0; i < elementsToCheckDuplicates; ++i) {

                    int srcIndex = middleSrcIndex;
                    int dstIndex = indexOfSublist + middleSrcIndex;

                    // verify collection bounds
                    if ((middleSrcIndex > src.size() - 1) || (dstIndex > dst.size() - 1)) {
                        break;
                    }

                    T srcValue = src.get(srcIndex);
                    T dstValue = dst.get(dstIndex);
                    if (srcValue.equals(dstValue)) {
                        ++middleSrcIndex;
                    } else {
                        break;
                    }
                }
                //////////////////////////////////

                List<T> sublistToMerge = src.subList(middleSrcIndex, src.size());
                dst.addAll(indexOfSublist + middleSrcIndex, sublistToMerge);
                return true;
            }
        }

        return false;
    }
}
