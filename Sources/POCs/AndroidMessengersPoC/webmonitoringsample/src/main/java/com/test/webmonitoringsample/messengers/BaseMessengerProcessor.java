package com.test.webmonitoringsample.messengers;

import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.test.webmonitoringsample.App;
import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.ScreenshotService;
import com.test.webmonitoringsample.merger.IMerger;
import com.test.webmonitoringsample.merger.impl.Merger4;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.FileInfo;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.IMessengerProcessor;
import com.test.webmonitoringsample.messengers.model.Message;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Cleanup;

import static com.test.webmonitoringsample.MyAccessibilityService.g_executor;
import static com.test.webmonitoringsample.messengers.MessengerProcessor.g_messengersInfo;

public abstract class BaseMessengerProcessor implements IMessengerProcessor {

    public static final String recycle = "recycle";
    private static final IMerger merger = new Merger4();

    public void process(final AccessibilityEvent event) {
        @Cleanup(recycle)
        AccessibilityNodeInfo root = HelperMethods.getRootNode(event);
        if (root == null) {
            return;
        }

//        final long startTime = HelperMethods.getTime();
        final String rootNode = HelperMethods.treeToJson(root).toString();
//        final long endTime = HelperMethods.getTime();
//        HelperMethods.printExecutionTime("HelperMethods.treeToJson", startTime, endTime);
//
        HelperMethods.printJson(rootNode);
        HelperMethods.printEventInfo(event);
        HelperMethods.printNodeTreeDepth(root, HelperMethods.readableEventName(event));

        g_executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
//                    final long startTime = HelperMethods.getTime();
                    MessengerInfo messengerInfo = MessengerTools.getMessengerInfo(g_messengersInfo, getCurrentMessenger());
                    Conversation currentConversation = getPartialConversationFromRootJsonNode(rootNode);
                    if (currentConversation == null) {
                        return;
                    }

                    Conversation globalConversation = MessengerTools.getConversation(messengerInfo.conversations, currentConversation.title);
                    if (globalConversation == null) {
                        messengerInfo.conversations.add(currentConversation);
                        globalConversation = MessengerTools.getConversation(messengerInfo.conversations, currentConversation.title);
                    } else {
                        boolean isMerged = mergeMessages(globalConversation.messages, currentConversation.messages);
                        if (!isMerged) {
                            return;
                        }
                    }

//                    boolean isFullDateResolved = resolveFullDate(globalConversation.messages);
//                    if (!isFullDateResolved) {
//                        return;
//                    }

//                    boolean hasImage = hasImage(currentConversation.messages);
//                    if (hasImage) {
//                        FilesInfo availableSdCardImages = getSdCardImages();
//
//                        resolveImagesPath(globalConversation.messages, messengerInfo.alreadyUsedImagesPaths, availableSdCardImages);
//                    }

//                    final long endTime = HelperMethods.getTime();
//                    HelperMethods.printExecutionTime("g_executor.execute", startTime, endTime);
                } catch (Exception e) {
                    // create dump
                    Log.d("XXX", "CRASH");
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * <p> Merges source into destination.
     * <p> Default implementation uses {@link com.test.webmonitoringsample.merger.impl.Merger3} implementation
     *
     * @param dst -- destination
     * @param src -- source
     * @return {@code true} if source has been merged. Otherwise {@code false} (i.e. no enough info for merging was provided or we've already has this protion of data in dst collection)
     */
    public boolean mergeMessages(List<Message> dst, List<Message> src) {
        return merger.merge(dst, src);
    }

    String buffArray = "";
    long millisSinceEpoch = 0;
    boolean scrollStopped = false;

    public Conversation getPartialConversationFromRootJsonNode(String rootJson) {
        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(rootJson);

        JsonArray messagesJsonArray = getMessagesJsonArray(entireScreen);
        Integer messagesCount = getMessagesCount(messagesJsonArray);
        if (messagesCount == null || messagesCount.equals(0)) {
            return null;
        }

        if (millisSinceEpoch != 0 && (System.currentTimeMillis() - millisSinceEpoch < 1000)) {
            return null;
        }

        String stringArray = messagesJsonArray.toString();
        if (stringArray.equals(buffArray))
            return null;

        millisSinceEpoch = System.currentTimeMillis();
        buffArray = stringArray;

        //TODO make for every messengers
        String title = "KiK messenger";
        /*String title = getTitle(entireScreen);
        if (title == null || title.isEmpty()) {
            return null;
        }*/

        // skip for simplification
        //List<String> participants = getParticipants(entireScreen, LIST_STRTING_TYPE);
        List<String> participants = Collections.emptyList();

        // skip for simplification
        //Boolean isGroup = getIsGroup(entireScreen, LIST_INTEGER_TYPE);
        Boolean isGroup = false;

        List<Message> messages = new ArrayList<>();

        //the first element of KiK's array is avatar and contact info

        Log.d("TAGG", "getPartialConversationFromRootJsonNode: ");

        Intent intent = new Intent(App.getAppContext(), ScreenshotService.class)
                .putExtra(ScreenshotService.EVENT, "22")
                .setAction("dasdasdasd");

        boolean madeScreenshot = false;

        for (int i = 1; i < messagesCount; ++i) {
            String visibleDate = getVisibleDate(messagesJsonArray, i);

            Boolean isImage = getIsImage(messagesJsonArray, i);
            if (isImage && !madeScreenshot) {
                App.getAppContext().startService(intent);
                madeScreenshot = true;
            }

            String text = getText(messagesJsonArray, i, isImage);

            String time = getTime(messagesJsonArray, i);

            Boolean isIncomming = getIsIncomming(messagesJsonArray, i);

            String sender = getSender(messagesJsonArray, i);

            // TODO: getMessagesJsonArray MUST return corresponding count of messages and we need to remove this verification
//            if (visibleDate == null && isImage == false && text == null && time == null && sender == null) {
//                continue;
//            }

           /* if (visibleDate == null && text == null) {
                continue;
            }*/

            Message actualMessage = new Message(text, isIncomming, time, visibleDate, sender, isImage, null);
            messages.add(actualMessage);
        }

        Conversation actualConversation = new Conversation(title, participants, isGroup, messages);
        return actualConversation;
    }

    private static boolean hasImage(List<Message> messages) {
        boolean hasAtLeastOneImage = false;
        for (Message m : messages) {
            if (m.isImage) {
                hasAtLeastOneImage = true;
                break;
            }
        }
        return hasAtLeastOneImage;
    }

    static String getFirstStringOrNull(List<String> src) {
        return (src != null && !src.isEmpty()) ? src.get(0) : null;
    }

    static Integer getFirstIntOrNull(List<Integer> src) {
        return (src != null && !src.isEmpty()) ? src.get(0) : null;
    }

    /**
     * @param messages
     * @return Must return true if at least one date was resolved.
     * <p> Default implementaion returns false.
     */
    public boolean resolveFullDate(List<Message> messages) {
        return false;
    }

    public void resolveImagesPath(List<Message> messages, List<FileInfo> alreadyUsedImages, FilesInfo availableSdCardImages) {
        // default implementation does nothing...
    }

    public Integer getMessagesCount(JsonArray messagesJsonArray) {
        Integer messagesCount = JsonPath.read(messagesJsonArray, "$.length()");
        return messagesCount;
    }

    public abstract String normalizeVisibleDate(String visibleDate);


}
