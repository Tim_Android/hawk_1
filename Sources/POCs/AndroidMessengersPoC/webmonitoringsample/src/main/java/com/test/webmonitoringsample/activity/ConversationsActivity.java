package com.test.webmonitoringsample.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.adapter.ConversationsAdapter;
import com.test.webmonitoringsample.messengers.MessengerTools;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import static com.test.webmonitoringsample.messengers.MessengerProcessor.g_messengersInfo;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.FB_MESSENGER;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.FB_MESSENGER_LITE;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.INSTAGRAM;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.KiK;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.SNAPCHAT;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.WHATS_APP;

public class ConversationsActivity extends Activity {

    public static final String MESSENGER = "MESSENGER";
    private RecyclerView.Adapter adapter;
    private MessengerInfo messengerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversations);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.conversationsRecycleView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        MessengerInfo.Messenger messenger = (MessengerInfo.Messenger) getIntent().getSerializableExtra(MESSENGER);
        messengerInfo = MessengerTools.getMessengerInfo(g_messengersInfo, messenger);

        setTitle(messenger.name() + " conversations");

        adapter = new ConversationsAdapter(this, messengerInfo);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HelperMethods.warnAccessibilityDisabled(this);
        adapter.notifyDataSetChanged();
    }


    public static void startWhatsApp(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, WHATS_APP));
    }

    public void clearAll(View view) {
        messengerInfo.clear();
        adapter.notifyDataSetChanged();
    }

    public static void startFbMsgr(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, FB_MESSENGER));
    }

    public static void startKiK(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, KiK));
    }

    public static void startFbMsgrLite(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, FB_MESSENGER_LITE));
    }

    public static void startSnapchat(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, SNAPCHAT));
    }

    public static void startInstagram(Context context) {
        context.startActivity(new Intent(context, ConversationsActivity.class).putExtra(MESSENGER, INSTAGRAM));
    }
}
