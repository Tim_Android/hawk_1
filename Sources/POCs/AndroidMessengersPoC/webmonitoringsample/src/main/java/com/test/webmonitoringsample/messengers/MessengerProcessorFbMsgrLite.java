package com.test.webmonitoringsample.messengers;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.util.List;

import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_OBJECT_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_STRTING_TYPE;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.FB_MESSENGER_LITE;

public class MessengerProcessorFbMsgrLite extends BaseMessengerProcessor {

    public static final CharSequence PACKAGE_NAME = "com.facebook.mlite";
    public static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/rvMessageList')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')]";

    @Override
    public MessengerInfo.Messenger getCurrentMessenger() {
        return FB_MESSENGER_LITE;
    }

    @Override
    public Boolean getIsImage(JsonArray messagesJsonArray, int i) {
        String isImagePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/preview_image')]", i);
        JsonArray isImageJsonArray = JsonPath.read(messagesJsonArray, isImagePath);
        List<Object> isImageList = AccessibilityNodeJsonSerializer.gson.fromJson(isImageJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isImage = isImageList != null && !isImageList.isEmpty();
        return isImage;
    }

    @Override
    public String getSender(JsonArray messagesJsonArray, int i) {
        return null;
    }

    @Override
    public Boolean getIsIncomming(JsonArray messagesJsonArray, int i) {
        String inText = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/inbound_text')]", i);
        JsonArray inTextJsonArray = JsonPath.read(messagesJsonArray, inText);
        List<Object> inTextList = AccessibilityNodeJsonSerializer.gson.fromJson(inTextJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isIncomming = inTextList != null && !inTextList.isEmpty();
        if(isIncomming) {
            return isIncomming;
        }

        String outText = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/outbound_text')]", i);
        JsonArray outTextJsonArray = JsonPath.read(messagesJsonArray, outText);
        List<Object> outTextList = AccessibilityNodeJsonSerializer.gson.fromJson(outTextJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isOutgoing = outTextList != null && !outTextList.isEmpty();
        if(isOutgoing) {
            return !isOutgoing;
        }

        // in case this is image...
        // TODO: we can pass isImage parameter to this function to make logic more simple
        String inImage = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/right_share_button')]", i);
        JsonArray inImageJsonArray = JsonPath.read(messagesJsonArray, inImage);
        List<Object> inImageList = AccessibilityNodeJsonSerializer.gson.fromJson(inImageJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isIncomingImage = inImageList != null && !inImageList.isEmpty();
        if(isIncomingImage) {
            return isIncomingImage;
        }

        String outImage = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/left_share_button')]", i);
        JsonArray outImageJsonArray = JsonPath.read(messagesJsonArray, outImage);
        List<Object> outImageList = AccessibilityNodeJsonSerializer.gson.fromJson(outImageJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isOutgoingImage = outImageList != null && !outImageList.isEmpty();
        if(isOutgoingImage) {
            return !isOutgoingImage;
        }

        // don't know what is direction
        // TODO: fix this later for all the data categories...
        return null;
    }

    @Override
    public String getTime(JsonArray messagesJsonArray, int i) {
        // same as visible date
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/timestamp')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String visibleDate = getFirstStringOrNull(dateList);

        String visibleTime = null;
        if (visibleDate != null) {
            int visibleDateLength = visibleDate.length();
            visibleTime = visibleDate.subSequence(visibleDateLength - 5, visibleDateLength).toString();
        }

        return visibleTime;
    }

    @Override
    public String getText(JsonArray messagesJsonArray, int i, boolean isImage) {
        String textPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/outbound_text' || @.mViewIdResourceName=='com.facebook.mlite:id/inbound_text')].mText.mText", i);

        JsonArray textJsonArray = JsonPath.read(messagesJsonArray, textPath);
        List<String> messageList = AccessibilityNodeJsonSerializer.gson.fromJson(textJsonArray, LIST_STRTING_TYPE.getType());
        String text = getFirstStringOrNull(messageList);
        return text;
    }

    @Override
    public String getVisibleDate(JsonArray messagesJsonArray, int i) {
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/timestamp')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String date = getFirstStringOrNull(dateList);

        if (date != null) {
            date = normalizeVisibleDate(date.toUpperCase());
        }

        return date;
    }

    @Override
    public Boolean getIsGroup(Object entireScreen) {
        return false;
    }

    @Override
    public List<String> getParticipants(Object entireScreen) {
        return null;
    }

    @Override
    public JsonArray getMessagesJsonArray(Object entireScreen) {
        JsonArray messagesJsonArray = JsonPath.read(entireScreen, MESSAGES_ROOT_PATH);
        return messagesJsonArray;
    }

    @Override
    public String getTitle(Object entireScreen) {
        String titlePath = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.facebook.mlite:id/my_toolbar')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText";
        List<String> titles = JsonPath.parse(entireScreen).read(titlePath, LIST_STRTING_TYPE);
        String title = getFirstStringOrNull(titles);
        return title;
    }

    @Override
    public List<String> resolveParticipants(String participantsAsString) {
        return null;
    }

    @Override
    public List<String> getParticipantsAsListList(String participantsAsString) {
        return null;
    }

    @Override
    public FilesInfo getSdCardImages() {
        return null;
    }

    @Override
    public String normalizeVisibleDate(String visibleDate) {
        return MessengerProcessorFbMsgr.normalizeVisibleDateForFb(visibleDate);
    }
}
