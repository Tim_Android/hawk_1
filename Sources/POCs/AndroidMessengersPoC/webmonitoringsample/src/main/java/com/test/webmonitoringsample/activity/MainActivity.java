package com.test.webmonitoringsample.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.ScreenshotService;
import com.test.webmonitoringsample.messengers.MessengerProcessor;
import com.test.webmonitoringsample.messengers.MessengerTools;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.Message;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    public static final String TAG = "ZZZ";
    private static final int PERMISSIONS_REQUEST_CODE = 1;
    public static final int REQUEST_SCREENSHOT = 59706;
    private MediaProjectionManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mgr = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        startActivityForResult(mgr.createScreenCaptureIntent(),
                REQUEST_SCREENSHOT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SCREENSHOT) {
            if (resultCode == RESULT_OK) {
                Intent i = new Intent(this, ScreenshotService.class)
                        .putExtra(ScreenshotService.EXTRA_RESULT_CODE, resultCode)
                        .putExtra(ScreenshotService.EXTRA_RESULT_INTENT, data);

                // saveIntent(i, resultCode);

                File externalFilesDir = getExternalFilesDir(null);
                if (externalFilesDir != null) {
                    String STORE_DIRECTORY = externalFilesDir.getAbsolutePath() + "/screenshots/";
                    File storeDirectory = new File(STORE_DIRECTORY);
                    if (!storeDirectory.exists()) {
                        boolean success = storeDirectory.mkdirs();
                        if (!success) {
                            Log.e(TAG, "failed to create file storage directory.");
                            return;
                        }
                    }
                } else {
                    Log.e(TAG, "failed to create file storage directory, getExternalFilesDir is null.");
                    return;
                }

                startService(i);
            }
        }
    }

    public static final String DATA_PERMISSION_STRING = "data_permission_string";
    public static final String DESULT_CODE = "resultCode";

    public void saveIntent(Intent intent, int resultCode) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(DATA_PERMISSION_STRING, intent.toUri(0))
                .putInt(DESULT_CODE, resultCode)
                .commit();
    }


    public void onBrowsersClick(View view) {
        startActivity(new Intent(this, BrowsersActivity.class));
    }

//    public void onViberClick(View view) {
//        startActivity(new Intent(this, ConversationsActivity.class));
//    }

    public void onWhatsAppClick(View view) {
        ConversationsActivity.startWhatsApp(this);

//        File[] inImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_IN_DIR);
//        List<FileInfo> inImagesFileInfo = getFileInfo(inImages);
//
//        File[] outImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_OUT_DIR);
//        List<FileInfo> outImagesFileInfo = getFileInfo(outImages);
    }

    public void onFbMsgrClick(View view) {
        ConversationsActivity.startFbMsgr(this);

//        File[] inImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_IN_DIR);
//        List<FileInfo> inImagesFileInfo = getFileInfo(inImages);
//
//        File[] outImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_OUT_DIR);
//        List<FileInfo> outImagesFileInfo = getFileInfo(outImages);
    }

    public void onKiKClick(View view) {
        ConversationsActivity.startKiK(this);
    }

    public void onSnapchatClick(View view) {
        ConversationsActivity.startSnapchat(this);
    }

    public void onFbMsgrLiteClick(View view) {
        ConversationsActivity.startFbMsgrLite(this);
    }


    public void onInstagramClick(View view) {
        ConversationsActivity.startInstagram(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestRuntimePermissions();
        HelperMethods.warnAccessibilityDisabled(this);
    }

    private void requestRuntimePermissions() {
        final String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showPermissionExplanation(permission);
            } else {
                requestPermission(permission);
            }
        }
    }

    private void showPermissionExplanation(final String permission) {
        new AlertDialog.Builder(this).
                setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission(permission);
                    }
                }).
                setTitle("Need permissions").
                setMessage("Please provide permissions to read images from SD card").create().show();
    }

    private void requestPermission(String permission) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 0) {
                    Toast.makeText(this, "Requesting of permissions was canceled by system", Toast.LENGTH_SHORT).show();
                } else if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permissions were not granted...", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void testAccesiibility(View view) {
        ConversationsActivity.startWhatsApp(this);
        MessengerInfo messengerInfo = MessengerTools.getMessengerInfo(MessengerProcessor.g_messengersInfo, MessengerInfo.Messenger.WHATS_APP);
        List<String> participants = Collections.emptyList();
        List<Message> messages = new ArrayList<>();
        messages.add(new Message("first message", true, "13:25", null, "some sender", false, null));
        messages.add(new Message("second message", false, "13:26", null, null, false, null));
        Conversation conversation1 = new Conversation("first", participants, false, messages);
        Conversation conversation2 = new Conversation("second", participants, false, messages);
        messengerInfo.conversations.add(conversation1);
        messengerInfo.conversations.add(conversation2);
    }
}
