package com.test.webmonitoringsample.browsers;

import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.INodeProcessor;

import lombok.Cleanup;

class UrlProcessorChrome implements INodeProcessor {
    private static final String CHROME_URL_WIDGET_CLASS = "android.widget.EditText";
    private static final String CHROME_FRAME_LAYOUT_WIDGET_CLASS = "android.widget.FrameLayout";
    private static final String CHROME_URL_WIDGET_ID = "com.android.chrome:id/url_bar";
    static final String CHROME_PACKAGE_NAME = "com.android.chrome";

    // id of close icon when url bar is in suggestion mode
    private static final String CLOSE_ICON_VIEW_ID = "com.android.chrome:id/delete_button";

    private AccessibilityEvent event;

    UrlProcessorChrome(AccessibilityEvent event) {
        this.event = event;
    }

    @Override
    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
        if (isViewWithUrlEmpric(nodeInfo, depth)
                && HelperMethods.isUrlValid(nodeInfo.getText())
                && !isGarbage(nodeInfo)) {
            return HelperMethods.addUrlIfNeed(nodeInfo, event);
        }

//        if (isChromeUrlBar(nodeInfo)
//                && HelperMethods.isUrlValid(nodeInfo.getText())) {
//            // if url is in edit mode then don't save url because user doesn't follow it
//            return isUrlInEditMode(nodeInfo) || HelperMethods.addUrlIfNeed(nodeInfo, event);
//        }

        return false;
    }

    /**
     * Compare getTextSelectionStart and getTextSelectionEnd positions
     * to filter out url that was typed by user manually in url bar
     * and then discarded without actual going to that link
     * <p>
     * Also verify whether this link is from suggestions list (when user switch between those suggestions)
     */
    private boolean isGarbage(AccessibilityNodeInfo nodeInfo) {
        if (nodeInfo.getTextSelectionStart() != nodeInfo.getTextSelectionEnd()) {
            return true;
        }

        @Cleanup("recycle") AccessibilityNodeInfo parent = nodeInfo.getParent();
        return parent != null && HelperMethods.hasAccessibilityNodeInfoWithId(parent, CLOSE_ICON_VIEW_ID);
    }

    /**
     * Verify whether close button icon in url bar is visible
     *
     * @param nodeInfo -- node info
     * @return True if close button icon was found. False otherwise.
     */
    private boolean isUrlInEditMode(AccessibilityNodeInfo nodeInfo) {
        @Cleanup("recycle") AccessibilityNodeInfo parent = nodeInfo.getParent();
        return parent != null && HelperMethods.hasAccessibilityNodeInfoWithId(parent, CLOSE_ICON_VIEW_ID);
    }

    /**
     * Node with URL but not isChromeUrlBar
     */
    private boolean isViewWithUrlEmpric(AccessibilityNodeInfo nodeInfo, int depth) {
        return event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
                && depth == 0
                && event.getRecordCount() == 0 // RecordCount > 0 represents suggestions listView in url bar during typing
                && nodeInfo.getViewIdResourceName() == null;
    }

    private boolean isChromeUrlBar(AccessibilityNodeInfo nodeInfo) {
        // try to avoid of using ID logic (APA-396)
        //return CHROME_URL_WIDGET_CLASS.equals(nodeInfo.getClassName()) && CHROME_URL_WIDGET_ID.equals(nodeInfo.getViewIdResourceName());

        if (CHROME_URL_WIDGET_CLASS.equals(nodeInfo.getClassName())) {
            @Cleanup("recycle") AccessibilityNodeInfo parent = nodeInfo.getParent();
            if(parent != null && CHROME_FRAME_LAYOUT_WIDGET_CLASS.equals(parent.getClassName())) {
                @Cleanup("recycle") AccessibilityNodeInfo grandParent = parent.getParent();
                if(grandParent != null && CHROME_FRAME_LAYOUT_WIDGET_CLASS.equals(grandParent.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }
}
