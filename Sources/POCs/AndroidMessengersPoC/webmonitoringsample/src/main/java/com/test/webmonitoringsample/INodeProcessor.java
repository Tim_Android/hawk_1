package com.test.webmonitoringsample;

import android.view.accessibility.AccessibilityNodeInfo;


public interface INodeProcessor {

    /**
     * @param nodeInfo -- node for processing
     * @return true if need to stop processing of nodes in hierarchy. Otherwise return false.
     */
    boolean process(AccessibilityNodeInfo nodeInfo, int depth);
}
