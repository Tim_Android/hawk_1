package com.test.webmonitoringsample.merger;

import java.util.List;

public interface IMerger {
    <T> boolean merge(List<T> dst, List<T> src);
    <T> boolean merge(List<T> dst, List<T> src, int dstElementsCountToMergeIn);
}
