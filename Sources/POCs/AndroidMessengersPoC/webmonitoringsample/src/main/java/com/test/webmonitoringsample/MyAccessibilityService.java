package com.test.webmonitoringsample;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.test.webmonitoringsample.messengers.MessengerProcessor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyAccessibilityService extends AccessibilityService {

    public static ExecutorService g_executor;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("VVV", "MyAccessibilityService.onCreate");
        g_executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        g_executor.shutdownNow();
        Log.d("VVV", "MyAccessibilityService.onDestroy");
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        CharSequence eventPackageName = event.getPackageName();
        if (eventPackageName == null) {
            return;
        }

        //BrowserProcessor.process(event);
        //MessengerProcessor.processViber(event);
        MessengerProcessor.process(event);
    }

    @Override
    public void onInterrupt() {
    }
}
