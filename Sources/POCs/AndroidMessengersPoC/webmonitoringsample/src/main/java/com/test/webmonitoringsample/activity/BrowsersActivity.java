package com.test.webmonitoringsample.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.browsers.UrlInfo;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.test.webmonitoringsample.HelperMethods.urls;

public class BrowsersActivity extends Activity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browsers);
        textView = (TextView) findViewById(R.id.urls);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }

    public void clearUrls(View view) {
        urls.clear();
        displayUrls();
    }

    private void displayUrls() {
        textView.setText(getReadableUrlsInfo(urls));
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayUrls();
    }

    private String getReadableUrlsInfo(List<UrlInfo> urls) {
        String result = "";
        if (urls.isEmpty()) {
            result = "NO ANY URLS...";
        } else {
            for (UrlInfo url : urls) {
                result += new SimpleDateFormat("hh:mm:ss.SSS", Locale.getDefault()).format(url.time) + "  " + url.browserName + "  " + url.name
                        + "\r\n"
                        + "\r\n";
            }
        }

        return result;
    }
}
