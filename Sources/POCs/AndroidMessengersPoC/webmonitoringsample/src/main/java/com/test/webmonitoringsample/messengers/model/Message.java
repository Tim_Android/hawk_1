package com.test.webmonitoringsample.messengers.model;

import static com.test.webmonitoringsample.messengers.MessengerTools.areEqual;

public class Message {
    public String text;
    public Boolean isIncoming; // null if don't know
    public String time;
    public String visibleDate;
    public String sender;
    public Boolean isImage;
    public  FileInfo imageFileInfo;

    // in format YYYYMMDD (like part of image path 20170609)
    public String resolvedDate;

    public Message(String text, Boolean isIncoming, String time, String visibleDate, String sender, Boolean isImage, FileInfo imageFileInfo) {
        this.text = text;
        this.isIncoming = isIncoming;
        this.time = time;
        this.visibleDate = visibleDate;
        this.sender = sender;
        this.isImage = isImage;
        this.imageFileInfo = imageFileInfo;
    }

    //////////////////////////////////////////////////////////

    /**
     * @return {@code true} if two messages are fully equals. See {@link #equalsMinimalistic} for comparison
     */
    @Override
    public boolean equals(Object obj) {
        boolean isMininmalisticEquals = equalsMinimalistic(obj);
        if (!isMininmalisticEquals) {
            return false;
        }

        Message m = (Message) obj;
        return areEqual(m.text, this.text)
                && areEqual(m.time, this.time)
                && areEqual(m.visibleDate, this.visibleDate)
                && areEqual(m.sender, this.sender)
                && areEqual(m.isIncoming, this.isIncoming)
                && areEqual(m.isImage, this.isImage)
//                && areEqual(m.imageName, this.imageName) // this shouldn't be used in merger comparison
//                && areEqual(m.imageDir, this.imageDir) // this shouldn't be used in merger comparison
//                && areEqual(m.resolvedDate, this.resolvedDate) // this shouldn't be used in merger comparison
                ;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + text.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + visibleDate.hashCode();
        result = 31 * result + sender.hashCode();
        result = 31 * result + isIncoming.hashCode();
        result = 31 * result + isImage.hashCode();
//        result = 31 * result + resolvedDate.hashCode(); // see equals method
//        result = 31 * result + imageName.hashCode();  // see equals method
//        result = 31 * result + imageDir.hashCode();  // see equals method
        return result;
    }
    //////////////////////////////////////////////////////////

    /**
     * This comparison doesn't use fields comparison
     * <p> To compare with fields use {@link #equals}
     */
    public boolean equalsMinimalistic(Object objRight) {
        if (this == objRight)
            return true;
        if (objRight == null)
            return false;
        if (this.getClass() != objRight.getClass())
            return false;
        return true;
    }
}
