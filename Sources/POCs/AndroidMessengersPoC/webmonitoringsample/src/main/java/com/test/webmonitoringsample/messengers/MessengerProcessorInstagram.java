package com.test.webmonitoringsample.messengers;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.jayway.jsonpath.JsonPath;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_INTEGER_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_STRTING_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerTools.getCommonDateAsStringFromDate;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.INSTAGRAM;

public class MessengerProcessorInstagram extends BaseMessengerProcessor {
    public static final CharSequence PACKAGE_NAME = "com.instagram.android";
    // private static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_list')].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/row_root_container')]";
    private static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_list')].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')]";
    // private static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_list')].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content_container')]";

    //2017
    private static final SimpleDateFormat YEAR_DATE_FORMAT = new SimpleDateFormat("yyyy");

    //MON AT 13:51
    private static SimpleDateFormat DAY_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("EEE hh:mm a");

    // Oct 7, 9:51 PM
    private static SimpleDateFormat MONTH_DATE_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("MMM dd',' hh:mm a");

    // 2017 Oct 7, 9:51 PM
    private static SimpleDateFormat TMP_YEAR_MONTH_DATE_TIME_VISIBLE_DATE_FORMAT = new SimpleDateFormat("yyyy MMM dd',' hh:mm a");

    @Override
    public MessengerInfo.Messenger getCurrentMessenger() {
        return INSTAGRAM;
    }

    @Override
    public Boolean getIsImage(JsonArray messagesJsonArray, int i) {
        return false;
    }

    @Override
    public String getSender(JsonArray messagesJsonArray, int i) {
        String senderPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText.mText", i);

        JsonArray sendersJsonArray = JsonPath.read(messagesJsonArray, senderPath);
        List<String> sendersList = AccessibilityNodeJsonSerializer.gson.fromJson(sendersJsonArray, LIST_STRTING_TYPE.getType());
        String sender = getFirstStringOrNull(sendersList);
        return sender;
    }

    @Override
    public Boolean getIsIncomming(JsonArray messagesJsonArray, int i) {
//        // we don't know how to find this in easy way
//        // if we still need this we should try to use info about message balloon control position on screen
//        // Nevertheless mark all messages we know as incomming

//        String senderAvatarPath = String.format("$.[%1$d].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/sender_avatar')]", i);
//        JsonArray senderAvatarJsonArray = JsonPath.read(messagesJsonArray, senderAvatarPath);
//        List<Object> senderAvatarList = AccessibilityNodeJsonSerializer.gson.fromJson(senderAvatarJsonArray, LIST_OBJECT_TYPE.getType());
//        Boolean isIncomming = senderAvatarList != null && !senderAvatarList.isEmpty();
//        return isIncomming;

        int screenWidth = getScreenWidth(messagesJsonArray, i);

        //String screenRightBoundBalloonPath = String.format("$.[%1$d].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')].CHILDS[?(@.mClassName=='android.widget.TextView' && !@.mViewIdResourceName)].mBoundsInScreen.right", i);
        //String screenRightBoundBalloonPath = String.format("$.[%1$d].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')].CHILDS[?(@.mClassName=='android.widget.LinearLayout' && !@.mViewIdResourceName)].mBoundsInScreen.right", i);
        String screenRightBoundBalloonPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mBoundsInScreen.right", i);
        JsonArray screenRightBoundBalloonJsonArray = JsonPath.read(messagesJsonArray, screenRightBoundBalloonPath);
        List<Integer> screenRightBoundBalloonList = AccessibilityNodeJsonSerializer.gson.fromJson(screenRightBoundBalloonJsonArray, LIST_INTEGER_TYPE.getType());
        Integer screenRightBoundBalloon = getFirstIntOrNull(screenRightBoundBalloonList);


        // Asssume IN balloon is shifted not closer then 10% of the screen width from the right screen bound
        int tenPercentScreenWidth = screenWidth / 10;

        boolean isIncomming = (screenRightBoundBalloon + tenPercentScreenWidth) < screenWidth;

        return isIncomming;
    }

    ////////////////////////////////
    // TODO: copypaste!
    private int getScreenWidth(JsonArray messagesJsonArray, int i) {
        String screenWidthPath = String.format("$.[%1$d].mBoundsInParent.right", i);
        JsonPrimitive screenWidthJsonPrimitive = JsonPath.read(messagesJsonArray, screenWidthPath);
        int screenWidth = screenWidthJsonPrimitive.getAsInt();
        return screenWidth;
    }

    private int getMessageBalloonWidth(JsonArray messagesJsonArray, int i) {
        String screenWidthPath = String.format("$.[%1$d].mBoundsInScreen.right", i);
        JsonPrimitive screenWidthJsonPrimitive = JsonPath.read(messagesJsonArray, screenWidthPath);
        int screenWidth = screenWidthJsonPrimitive.getAsInt();
        return screenWidth;
    }
    ////////////////////////////////

    @Override
    public String getTime(JsonArray messagesJsonArray, int i) {
        String timePath = String.format("$.[%1$d].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_status')].mText", i);

        JsonArray timeJsonArray = JsonPath.read(messagesJsonArray, timePath);
        List<String> timesList = AccessibilityNodeJsonSerializer.gson.fromJson(timeJsonArray, LIST_STRTING_TYPE.getType());
        String time = getFirstStringOrNull(timesList);
        return time;
    }

    @Override
    public String getText(JsonArray messagesJsonArray, int i, boolean isImage) {
        String textPath = String.format("$.[%1$d].CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/message_content')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText.mText", i);

        JsonArray textJsonArray = JsonPath.read(messagesJsonArray, textPath);
        List<String> messageList = AccessibilityNodeJsonSerializer.gson.fromJson(textJsonArray, LIST_STRTING_TYPE.getType());
        String text = getFirstStringOrNull(messageList);
        return text;
    }

    @Override
    public String getVisibleDate(JsonArray messagesJsonArray, int i) {
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/timestamp_separator')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String date = getFirstStringOrNull(dateList);

        if (date != null) {
            date = normalizeVisibleDate(date.toUpperCase());
        }

        return date;
    }

    @Override
    public Boolean getIsGroup(Object entireScreen) {
        return null;
    }

    @Override
    public List<String> getParticipants(Object entireScreen) {
        return null;
    }

    @Override
    public JsonArray getMessagesJsonArray(Object entireScreen) {
        JsonArray messagesJsonArray = JsonPath.read(entireScreen, MESSAGES_ROOT_PATH);
        if(messagesJsonArray == null || messagesJsonArray.size() == 0) {
            return null;
        }

        // TODO: WORKAROUND!
        Boolean isScreenShifted = isScreenShifted(messagesJsonArray, 0);
        if(isScreenShifted) {
            return null;
        }

        return messagesJsonArray;
    }

    /**
     * Skip parsing if screen is shifted (i.e. user wants to see the time of Instagram message)
     * Because we can't find message direction in this "shifted" screen for Instagram messenger..
     * Usually message balloon grab full screen
     * See  {@link #getIsIncomming} implementation for details
     * @param messagesJsonArray
     * @return true if screen with messages is shifted to some side (usually to the left for instagram app to see message time)
     */
    public boolean isScreenShifted(JsonArray messagesJsonArray, int i) {
        int screenWidth = getScreenWidth(messagesJsonArray, i);
        int messageBalloonWidth = getMessageBalloonWidth(messagesJsonArray, i);
        return messageBalloonWidth < screenWidth;
    }

    @Override
    public String getTitle(Object entireScreen) {
        String titlePath = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.instagram.android:id/action_bar_textview_title')].mText";
        List<String> titles = JsonPath.parse(entireScreen).read(titlePath, LIST_STRTING_TYPE);
        String title = getFirstStringOrNull(titles);
        return title;
    }

    @Override
    public List<String> resolveParticipants(String participantsAsString) {
        return null;
    }

    @Override
    public List<String> getParticipantsAsListList(String participantsAsString) {
        return null;
    }

    @Override
    public FilesInfo getSdCardImages() {
        return null;
    }

    @Override
    public String normalizeVisibleDate(String visibleDate) {
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        return normalizedDate;
    }

    public static String normalizeVisibleDateForInstagram(String origDate) {
        if (origDate != null) {
            String date = origDate.toUpperCase();

            if(date.startsWith("TODAY")) {
                Date todayDate = new Date();
                date = getCommonDateAsStringFromDate(todayDate);
                return date;
            } else if (date.startsWith("YESTERDAY")) {
                Date yesterdayDate = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
                date = getCommonDateAsStringFromDate(yesterdayDate);
                return date;
            }

            // try to process other date formats...
            Date currentDate = new Date();
            String normalizedDateAsString = null;

            Date normalizedDate = getNormalizedDate(DAY_TIME_VISIBLE_DATE_FORMAT, date);
            if (normalizedDate != null) {
                // https://stackoverflow.com/a/12783806
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - normalizedDate.getDay() - 1));
                normalizedDateAsString = getCommonDateAsStringFromDate(cal.getTime());
                return normalizedDateAsString;
            }

            normalizedDate = getNormalizedDate(MONTH_DATE_TIME_VISIBLE_DATE_FORMAT, date);
            if (normalizedDate != null) {
                String year = YEAR_DATE_FORMAT.format(currentDate);
                normalizedDate = getNormalizedDate(TMP_YEAR_MONTH_DATE_TIME_VISIBLE_DATE_FORMAT, year + " " + date);
                normalizedDateAsString = getCommonDateAsStringFromDate(normalizedDate);
                return normalizedDateAsString;
            }

            return date;
        }

        return origDate;
    }

    private static Date getNormalizedDate(SimpleDateFormat potentialDateFormat, String date) {
        Date result = null;
        try {
            result = potentialDateFormat.parse(date);
        } catch (ParseException e) {
            //Log.d("ERROR", e.getMessage());
            //e.printStackTrace();
        }
        return result;
    }
}
