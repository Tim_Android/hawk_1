package com.test.webmonitoringsample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.activity.MessagesActivity;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {
    private Context context;
    private MessengerInfo messengerInfo;

    public ConversationsAdapter(Context context, MessengerInfo messengerInfo) {
        this.context = context;
        this.messengerInfo = messengerInfo;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conversation_element_layot, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        final TextView conversationTitle = (TextView) view.findViewById(R.id.conversationTitle);
        conversationTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TextView conversationTitle = (TextView) view.findViewById(R.id.conversationTitle);
                MessagesActivity.start(context, viewType, messengerInfo.messenger);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.conversationTitle.setText(messengerInfo.conversations.get(position).title);
    }

    @Override
    public int getItemCount() {
        return messengerInfo.conversations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView conversationTitle;

        public ViewHolder(View v) {
            super(v);
            conversationTitle = (TextView) v.findViewById(R.id.conversationTitle);
        }
    }
}
