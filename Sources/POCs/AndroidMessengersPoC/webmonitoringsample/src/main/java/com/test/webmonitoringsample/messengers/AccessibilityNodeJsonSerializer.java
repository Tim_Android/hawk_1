package com.test.webmonitoringsample.messengers;

import android.view.accessibility.AccessibilityNodeInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.test.webmonitoringsample.tools.JsonSerrializationExclusionStrategy;

public class AccessibilityNodeJsonSerializer {
    private static final String CHILDS = "CHILDS";
    private static final String ROOT = "ROOT";
    private JsonObject jsonObject;
    public static Gson gson = new GsonBuilder().serializeNulls().addSerializationExclusionStrategy(JsonSerrializationExclusionStrategy.getCommonStrategy()).create();

    public AccessibilityNodeJsonSerializer() {
        jsonObject = new JsonObject();
    }

    public JsonElement process(AccessibilityNodeInfo nodeInfo, int depth, JsonObject parent) {
        JsonElement nodeAsJson = gson.toJsonTree(nodeInfo);

        if (depth == 0) {
            parent.add(ROOT, nodeAsJson);
        } else {
            if (!parent.has(CHILDS)) {
                JsonArray childs = new JsonArray();
                parent.add(CHILDS, childs);
            }
            JsonArray childsArray = parent.getAsJsonArray(CHILDS);
            childsArray.add(nodeAsJson);
        }

        return nodeAsJson;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }
}
