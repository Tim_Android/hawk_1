/***
 Copyright (c) 2015 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.

 Covered in detail in the book _The Busy Coder's Guide to Android Development_
 https://commonsware.com/Android
 */

package com.test.webmonitoringsample;

import android.app.Service;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaScannerConnection;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class ScreenshotService extends Service {
    public static final String EXTRA_RESULT_CODE = "resultCode";
    public static final String EXTRA_RESULT_INTENT = "resultIntent";
    public static final String EVENT = "event";
    private Handler mHandler;
    private MediaProjectionManager mgr;
    private WindowManager wmgr;
    private ImageTransmogrifier it;
    private int mDensity;
    private Display mDisplay;
    private VirtualDisplay mVirtualDisplay;
    private static MediaProjection sMediaProjection;
    private static final String SCREENCAP_NAME = "screencap";
    private static final int VIRTUAL_DISPLAY_FLAGS = DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;

    final private HandlerThread handlerThread =
            new HandlerThread(getClass().getSimpleName(),
                    android.os.Process.THREAD_PRIORITY_BACKGROUND);
    private Handler handler;
    int resultCode;
    Intent resultData;

    int newFileName;

    @Override
    public void onCreate() {
        super.onCreate();

        mgr = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        wmgr = (WindowManager) getSystemService(WINDOW_SERVICE);

        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public int onStartCommand(Intent data, int flags, int startId) {
        if (data != null) {
            if (data.getAction() == null) {
                resultCode = data.getIntExtra(EXTRA_RESULT_CODE, -1);
                resultData = data.getParcelableExtra(EXTRA_RESULT_INTENT);
            }
            if ("22".equals(data.getStringExtra(ScreenshotService.EVENT))) {
                if (resultData != null) {

                    int min = 0;
                    int max = 999;
                    Random r = new Random();
                    newFileName = r.nextInt(max - min + 1) + min;

                    Log.d("asdasdsadasd", "run: " + newFileName);

                    if (sMediaProjection == null)
                        sMediaProjection = mgr.getMediaProjection(resultCode, resultData);

                    startCapture(newFileName);
                } else {
                    Intent ui = new Intent(this, EmptyActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(ui);
                }
            }
        }

        return (START_STICKY);
    }

    @Override
    public void onDestroy() {
        stopCapture();
        super.onDestroy();
    }

    void processImage(final byte[] png, final int fileName) {
        new Thread() {
            @Override
            public void run() {
                File output = new File(getExternalFilesDir(null), fileName + ".png");

                if (output.exists())
                    return;

                try {
                    FileOutputStream fos = new FileOutputStream(output);

                    fos.write(png);
                    fos.flush();
                    fos.getFD().sync();
                    fos.close();

                    MediaScannerConnection.scanFile(ScreenshotService.this,
                            new String[]{output.getAbsolutePath()},
                            new String[]{"image/png"},
                            null);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "Exception writing out screenshot", e);
                }
            }
        }.start();

        if (newFileName == fileName)
            stopCapture();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new IllegalStateException("Binding not supported. Go away.");
    }

    WindowManager getWindowManager() {
        return (wmgr);
    }

    Handler getHandler() {
        return (handler);
    }

    private void startCapture(int fileName) {

        it = new ImageTransmogrifier(this, fileName);

        MediaProjection.Callback cb = new MediaProjection.Callback() {
            @Override
            public void onStop() {
                mVirtualDisplay.release();
            }
        };

        mVirtualDisplay = sMediaProjection.createVirtualDisplay("andshooter",
                it.getWidth(), it.getHeight(),
                getResources().getDisplayMetrics().densityDpi,
                VIRTUAL_DISPLAY_FLAGS, it.getSurface(), null, handler);
        sMediaProjection.registerCallback(cb, handler);
    }

    private void stopCapture() {
        if (sMediaProjection != null) {
            sMediaProjection.stop();
            mVirtualDisplay.release();
            sMediaProjection = null;
        }
    }
}