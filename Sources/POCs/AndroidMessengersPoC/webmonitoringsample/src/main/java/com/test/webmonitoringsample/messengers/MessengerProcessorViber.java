package com.test.webmonitoringsample.messengers;

import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.test.webmonitoringsample.AccessibilityNodeInfoGuard;
import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.INodeProcessor;

import java.util.List;

import lombok.Cleanup;

import static com.test.webmonitoringsample.AccessibilityNodeInfoGuard.SAFE_RECYCLE;
import static com.test.webmonitoringsample.messengers.MessengerProcessor.g_viberInfo;

class MessengerProcessorViber implements INodeProcessor {
    public static final CharSequence VIBER_PACKAGE_NAME = "com.viber.voip";

    public MessengerProcessorViber() {
    }

    @Override
    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
        return false;
    }

    public static void process(AccessibilityEvent event) {
        AccessibilityNodeInfo root = HelperMethods.getRootNode(event);
        @Cleanup(SAFE_RECYCLE)
        AccessibilityNodeInfoGuard rootGuard = new AccessibilityNodeInfoGuard(root);
        if (root == null) {
            return;
        }

        HelperMethods.printNodeTreeDepth(root, "");

        FirstSearchResult decorParentNode = HelperMethods.depthFirstSearchNodeProcessingBest(
                root
                , new INodeProcessor() {
                    @Override
                    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
                        return "android.view.View".equals(nodeInfo.getClassName())
                                && "com.viber.voip:id/decor_content_parent".equals(nodeInfo.getViewIdResourceName());
                    }
                }
                , 0);
        if (decorParentNode.nodeInfo == null) {
            return;
        }
        @Cleanup(SAFE_RECYCLE)
        AccessibilityNodeInfoGuard decorParentNodeGuard = new AccessibilityNodeInfoGuard(decorParentNode.nodeInfo);
        if (decorParentNode.isIncomingParameter) {
            decorParentNodeGuard.dontRecycle();
        }

        final ViberInfo.Conversation conversation = new ViberInfo.Conversation();
        boolean found = fillConversationInfo(decorParentNode.nodeInfo, conversation);
        if (!found) {
            return;
        }

        fillMessages(decorParentNode.nodeInfo, conversation);

        resolveMessagesDirection(conversation);

        mergeToGlobalViberInfo(conversation);
    }

    private static boolean mergeToGlobalViberInfo(ViberInfo.Conversation conversation) {
        int conversationIndex = -1;
        for (int i = 0; i < g_viberInfo.conversations.size(); ++i) {
            if (g_viberInfo.conversations.get(i).equals(conversation)) {
                conversationIndex = i;
                break;
            }
        }

        if (conversationIndex == -1) {
            // new conversation. Just add it...
            g_viberInfo.conversations.add(conversation);
            return true;
        }

        ViberInfo.Conversation globalConversation = g_viberInfo.conversations.get(conversationIndex);
        if (globalConversation.messages.isEmpty()) {
            // conversation is empty. Just add new messages
            globalConversation.messages.addAll(conversation.messages);
            return true;
        }

        // try to add into beginning
        ViberInfo.Message firstGlobalMessage = globalConversation.messages.get(0);
        int firstCrossIndexWithGlobalBeginMessages = -1;
        for (int i = 0; i < conversation.messages.size(); ++i) {
            if (conversation.messages.get(i).equals(firstGlobalMessage)) {
                firstCrossIndexWithGlobalBeginMessages = i;
                break;
            }
        }
        if (firstCrossIndexWithGlobalBeginMessages > 0) {
            globalConversation.messages.addAll(0, conversation.messages.subList(0, firstCrossIndexWithGlobalBeginMessages));
            return true;
        }

        // try to add into the end
        ViberInfo.Message lastGlobalMessage = globalConversation.messages.get(globalConversation.messages.size() - 1);
        int firstCrossIndexWithGlobalEndMessages = -1;
        for (int i = 0; i < conversation.messages.size(); ++i) {
            if (conversation.messages.get(i).equals(lastGlobalMessage)) {
                firstCrossIndexWithGlobalEndMessages = i;
                break;
            }
        }
        if (firstCrossIndexWithGlobalEndMessages != -1
                && firstCrossIndexWithGlobalEndMessages != conversation.messages.size() - 1) {
            globalConversation.messages.addAll(globalConversation.messages.size(), conversation.messages.subList(firstCrossIndexWithGlobalEndMessages + 1, conversation.messages.size()));
            return true;
        }

        return false;
    }

    /**
     * @param conversation
     * @return true if resolve succeed. Otherwise false
     */
    private static boolean resolveMessagesDirection(ViberInfo.Conversation conversation) {

        // getFirstKnownDirection
        int firstKnownDirectionIndex = -1;
        boolean firstKnownDirectionIsIncoming = false;
        for (int i = 0; i < conversation.messages.size(); ++i) {
            if (conversation.messages.get(i).isIncoming != null) {
                firstKnownDirectionIndex = i;
                firstKnownDirectionIsIncoming = conversation.messages.get(i).isIncoming;
                break;
            }
        }

        if (firstKnownDirectionIndex == -1) {
            return false;
        }

        boolean lastKnownDirectionIsIncoming = !firstKnownDirectionIsIncoming;
        for (int i = 0; i < conversation.messages.size(); ++i) {
            if (conversation.messages.get(i).isIncoming == null) {
                conversation.messages.get(i).isIncoming = lastKnownDirectionIsIncoming;
            } else {
                lastKnownDirectionIsIncoming = conversation.messages.get(i).isIncoming;
            }
        }
        return true;
    }

    private static void fillMessages(AccessibilityNodeInfo decorParentNode, ViberInfo.Conversation conversation) {
        List<AccessibilityNodeInfo> messages = decorParentNode.findAccessibilityNodeInfosByViewId("com.viber.voip:id/message_container");
        try {
            for (int i = 0; i < messages.size(); ++i) {
                AccessibilityNodeInfo messageNode = messages.get(i);
                ViberInfo.Message message = getMessage(messageNode);
                if (message != null) {
                    conversation.messages.add(message);
                }
            }
        } finally {
            for (AccessibilityNodeInfo node : messages) {
                node.recycle();
            }
        }
    }

    /**
     * @param node -- com.viber.voip:id/message_container
     * @return valid message or null
     */
    private static ViberInfo.Message getMessage(AccessibilityNodeInfo node) {
        if (node.getChildCount() == 0) {
            return null;
        }

        String text = null;
        List<AccessibilityNodeInfo> texts = node.findAccessibilityNodeInfosByViewId("com.viber.voip:id/message");
        try {
            if (!texts.isEmpty()) {
                CharSequence cs = texts.get(0).getText();
                if (cs != null) {
                    text = cs.toString();
                }
            }
        } finally {
            for (AccessibilityNodeInfo tmp : texts) {
                tmp.recycle();
            }
        }
        if (text == null) {
            return null;
        }


        String timestamp = null;
        List<AccessibilityNodeInfo> timestamps = node.findAccessibilityNodeInfosByViewId("com.viber.voip:id/time_stamp");
        try {
            if (!timestamps.isEmpty()) {
                CharSequence cs = timestamps.get(0).getText();
                if (cs != null) {
                    timestamp = cs.toString();
                }
            }
        } finally {
            for (AccessibilityNodeInfo tmp : timestamps) {
                tmp.recycle();
            }
        }
        if (timestamp == null) {
            return null;
        }

        Boolean isIncoming = null;
        int childCount = node.getChildCount();
        if (childCount > 2) {
            @Cleanup("recycle")
            AccessibilityNodeInfo firstChild = node.getChild(0);

            if ("com.viber.voip:id/avatar".equals(firstChild.getViewIdResourceName())) {
                isIncoming = true;
            } else {
                isIncoming = false;
            }
        }

        return new ViberInfo.Message(text, isIncoming, timestamp);
    }

    private static boolean fillConversationInfo(AccessibilityNodeInfo decorParentNode, final ViberInfo.Conversation conversation) {
        return HelperMethods.breadthFirstSearchNodeProcessing(
                decorParentNode
                , new INodeProcessor() {
                    @Override
                    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
                        if ("android.widget.TextView".equals(nodeInfo.getClassName())) {
                            conversation.title = nodeInfo.getText() == null ? null : nodeInfo.getText().toString();
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
    }
}
