package com.test.webmonitoringsample.messengers.model;

import com.google.gson.JsonArray;

import java.text.SimpleDateFormat;
import java.util.List;

public interface IMessengerProcessor {

    // MAY 22, 2017
    // JUNE 9, 2017
    SimpleDateFormat COMMON_VISIBLE_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy");

    Conversation getPartialConversationFromRootJsonNode(String rootJson);

    MessengerInfo.Messenger getCurrentMessenger();

    /**
     * @param messagesJsonArray
     * @param i
     * @return true if image, false otherwise, null if we don't know...
     */
    Boolean getIsImage(JsonArray messagesJsonArray, int i);

    String getSender(JsonArray messagesJsonArray, int i);

    Boolean getIsIncomming(JsonArray messagesJsonArray, int i);

    String getTime(JsonArray messagesJsonArray, int i);

    String getText(JsonArray messagesJsonArray, int i, boolean isImage);

    /**
     *
     * @param messagesJsonArray
     * @param i
     * @return date in format MMM dd, yyyy (i.e. MAY 26, 2017)
     */
    String getVisibleDate(JsonArray messagesJsonArray, int i);

    Boolean getIsGroup(Object entireScreen);

    List<String> getParticipants(Object entireScreen);

    JsonArray getMessagesJsonArray(Object entireScreen);

    Integer getMessagesCount(JsonArray messagesJsonArray);

    String getTitle(Object entireScreen);


    List<String> resolveParticipants(String participantsAsString);

    List<String> getParticipantsAsListList(String participantsAsString);

    FilesInfo getSdCardImages();
}
