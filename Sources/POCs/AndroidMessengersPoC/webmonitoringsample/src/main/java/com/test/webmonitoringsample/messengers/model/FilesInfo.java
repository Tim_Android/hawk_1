package com.test.webmonitoringsample.messengers.model;

import java.util.List;

public class FilesInfo {
    public List<FileInfo> inFilesInfo;
    public List<FileInfo> outFilesInfo;
    public List<FileInfo> unknownDirectionFilesInfo;

    public FilesInfo(List<FileInfo> inFilesInfo, List<FileInfo> outFilesInfo) {
        this.inFilesInfo = inFilesInfo;
        this.outFilesInfo = outFilesInfo;
    }
}
