package com.test.webmonitoringsample;

import android.view.accessibility.AccessibilityNodeInfo;

public class AccessibilityNodeInfoGuard {
    public static final String SAFE_RECYCLE = "SAFE_RECYCLE";

    private AccessibilityNodeInfo nodeInfo;
    private boolean needRecycle;

    public AccessibilityNodeInfoGuard(AccessibilityNodeInfo nodeInfo) {
        this.nodeInfo = nodeInfo;
        this.needRecycle = true;
    }

    public void dontRecycle() {
        this.needRecycle = false;
    }

    public void SAFE_RECYCLE() {
        if(nodeInfo != null && needRecycle) {
            nodeInfo.recycle();
        }
    }
}
