package com.test.webmonitoringsample.messengers.model;

import java.util.List;

import static com.test.webmonitoringsample.messengers.MessengerTools.areEqual;

public class Conversation {
    public String title;
    public List<String> participants;
    public Boolean isGroup;
    public List<Message> messages;

    public Conversation(String title, List<String> participants, Boolean isGroup, List<Message> messages) {
        this.title = title;
        this.participants = participants;
        this.isGroup = isGroup;
        this.messages = messages;
    }

    //////////////////////////////////////////////////////////

    /**
     * @return {@code true} if two conversations are fully equals. See {@link #equalsMinimalistic} for comparison
     */
    @Override
    public boolean equals(Object obj) {
        boolean isMininmalisticEquals = equalsMinimalistic(obj);
        if (!isMininmalisticEquals) {
            return false;
        }

        Conversation c = (Conversation) obj;
        return areEqual(c.title, this.title)
                && areEqual(c.participants, this.participants)
                && areEqual(c.isGroup, this.isGroup)
                && areEqual(c.messages, this.messages);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + title.hashCode();
        result = 31 * result + participants.hashCode();
        return result;
    }
    //////////////////////////////////////////////////////////

    /**
     * This comparison doesn't use fields comparison
     * <p> To compare with fields use {@link #equals}
     */
    public boolean equalsMinimalistic(Object objRight) {
        if (this == objRight)
            return true;
        if (objRight == null)
            return false;
        if (this.getClass() != objRight.getClass())
            return false;
        return true;
    }
}
