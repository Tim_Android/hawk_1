package com.test.webmonitoringsample.merger.impl;

import com.test.webmonitoringsample.merger.IMerger;

import java.util.Collections;
import java.util.List;

/**
 * This merger expects either
 * <p> 0) Full intersection. Then don't merge to prevent duplicates!
 * <p> 1) At least 2 sequential elements intersection in src and dst (i.e. if dst=[1,2,3,4,5] and src=[7,8,3,4,9,0] then dst after merging will be dst=[1,2,7,8,3,4,9,0,5])
 * <p> 2) or intersection of src and dst by boundary elements (i.e. if dst=[1,2,3,4,5] and src=[5,6,7,8,9] then dst after merging will be dst=[1,2,3,4,5,6,7,8,9]). Similar logic with boundary intersection in the beginning.
 * <p> If no intersection found algorithm appends src to the end of dst.
 */
public class Merger4 implements IMerger {
    private static final int DST_MERGE_INTERVAL = 50;
    private static final int ELEMENTS_IN_SRC_SUBLISTS = 2;

    /**
     * <p> This merger assumes that there are no big amount of elements in src (every time usually there are about 10 elements to merge)
     * <p> So logic can be next:
     * <p> Try to find full intersection src with dst
     * <p> If no intersection found  then divide src into subcollections with 2 elements and try to find intersection of any src subcollections with dst. If intersections found then try to merge remaining src sublist/sublists into dst
     * <p> If no intersections found then try to find intersection edged elements in src and dst to merge into beginning or ending (by 1 element intersection).
     * <p> If no intersections found for this point then just add entire src to the end of dst.
     */
    @Override
    public <T> boolean merge(List<T> dst, List<T> src) {
        return merge(dst, src, DST_MERGE_INTERVAL);
    }

    @Override
    public <T> boolean merge(List<T> fullDst, List<T> src, int dstElementsCountToMergeIn) {
        if (src == null || fullDst == null) {
            return false;
        }

        if (src.isEmpty()) {
            return false;
        }

        if (fullDst.isEmpty()) {
            return fullDst.addAll(src);
        }

        List<T> dst = fullDst;
        if (dstElementsCountToMergeIn > 0 && fullDst.size() > dstElementsCountToMergeIn) {
            dst = fullDst.subList(fullDst.size() - dstElementsCountToMergeIn, fullDst.size());
        }

        // try to find full interception
        int indexOfSublist = Collections.indexOfSubList(dst, src);
        if (indexOfSublist >= 0) {
            return false;
        }

        if (src.size() >= 2) {
            boolean merged = mergeWithSrcSublists(dst, src, 0);
            if (merged) {
                return true;
            }
        }

        // try to shift src subcollections because if only 2 elements intersects with dst then we have 50% probability to fail merging
        // lets assume dst=[1,2,3,4,5] and src=[8,3,4,9] then we'll have [8,3] and [4,9] src subcollections that don't have intersections
        // so we should shift src subcollections to 1 and will have only one [3,4] subcollection that will have intersection
        if (src.size() >= 3) {
            boolean merged = mergeWithSrcSublists(dst, src, 1);
            if (merged) {
                return true;
            }
        }

        // try to merge by edged elements (compare dstFirst with srcLast and vise vera)
        if (src.size() > 1) {
            // 1.try to merge into beginning
            T lastSrcElement = src.get(src.size() - 1);
            T firstDstElement = dst.get(0);
            if (lastSrcElement.equals(firstDstElement)) {
                List<T> sublistToMerge = src.subList(0, src.size() - 1);
                dst.addAll(0, sublistToMerge);
                return true;
            }

            // 2. try to merge into the end
            T firstSrcElement = src.get(0);
            T lastDstElement = dst.get(dst.size() - 1);
            if (firstSrcElement.equals(lastDstElement)) {
                List<T> sublistToMerge = src.subList(1, src.size());
                dst.addAll(sublistToMerge);
                return true;
            }
        }


        return dst.addAll(src);
    }

    private <T> boolean mergeWithSrcSublists(List<T> dst, List<T> src, int srcSubcollectionsOffset) {
        // no full intersection found
        // try to find intersection of src subcollections with 2 elements..
        // Sample1: if src=[0,1,2,3,4,5,6,7] then try to use next subcollections [0,1], [2,3], [4,5], [6,7]
        // Sample2: if src=[0,1,2,3,4,5,6,7,8] then try to use next subcollections [0,1], [2,3], [4,5], [6,7]

        for (int i = 0; i + ELEMENTS_IN_SRC_SUBLISTS + srcSubcollectionsOffset <= src.size(); i = i + ELEMENTS_IN_SRC_SUBLISTS) {
            int srcSublistIndexBegin = i + srcSubcollectionsOffset;
            int srcSublistIndexAfterEnd = i + ELEMENTS_IN_SRC_SUBLISTS + srcSubcollectionsOffset;

            List<T> srcSublist = src.subList(srcSublistIndexBegin, srcSublistIndexAfterEnd);

            // try to find intersection of srcSublist with dst
            int intersectionIndex = Collections.indexOfSubList(dst, srcSublist);
            if (intersectionIndex == -1) {
                continue;
            }

            // intersection was found. Merge..
            int insertedIntoBeginningElements = 0;

            // 1. Merge src beginning
            int dstIndexToInsertSrcElement = intersectionIndex;
            int offset = 0;
            for (int j = srcSublistIndexBegin - 1; j >= 0; --j) {
                // skipping duplicates
                int dstIndexPreviousElement = dstIndexToInsertSrcElement - offset - 1;
                if (dstIndexPreviousElement >= 0) {
                    T dstPreviousElement = dst.get(dstIndexPreviousElement);
                    T srcElementToInsert = src.get(j);
                    if (!dstPreviousElement.equals(srcElementToInsert)) {
                        dst.add(dstIndexToInsertSrcElement, srcElementToInsert);
                        ++insertedIntoBeginningElements;
                    } else {
                        --dstIndexToInsertSrcElement;
                    }
                } else {
                    T srcElementToInsert = src.get(j);
                    dst.add(dstIndexToInsertSrcElement, srcElementToInsert);
                    ++insertedIntoBeginningElements;
                }
                ++offset;
            }

            // 2. Merge src ending
            dstIndexToInsertSrcElement = intersectionIndex + ELEMENTS_IN_SRC_SUBLISTS + insertedIntoBeginningElements;
            offset = 0;
            int shiftWithoutInserting = 0;
            for (int j = srcSublistIndexAfterEnd; j < src.size(); ++j) {
                // skipping duplicates
                int dstIndexNextElement = dstIndexToInsertSrcElement + offset - shiftWithoutInserting;
                if (dstIndexNextElement < dst.size()) {
                    T dstNextElement = dst.get(dstIndexNextElement);
                    T srcElementToInsert = src.get(j);
                    if (!dstNextElement.equals(srcElementToInsert)) {
                        dst.add(dstIndexToInsertSrcElement, srcElementToInsert);
                    } else {
                        ++shiftWithoutInserting;
                    }
                } else {
                    T srcElementToInsert = src.get(j);
                    dst.add(dstIndexToInsertSrcElement, srcElementToInsert);
                }

                ++offset;
                ++dstIndexToInsertSrcElement;
            }

            return true;
        }

        return false;
    }
}
