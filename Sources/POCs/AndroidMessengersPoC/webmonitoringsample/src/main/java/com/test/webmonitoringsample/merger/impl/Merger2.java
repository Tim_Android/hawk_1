package com.test.webmonitoringsample.merger.impl;

import com.test.webmonitoringsample.merger.IMerger;

import java.util.List;

import static java.util.Collections.indexOfSubList;

/**
 * <p> This merging tried to use {@link com.test.webmonitoringsample.merger.impl.Merger1} algorithm firstly.
 * <p> If mergeMessages1 return true -- exit. Otherwise try to find <s>full</s> <b>partial<b/> intersection (see the reason below}) src with dst. Return false if found. If not found then just add src messages to the end of the dst because usually snapchat removes previous messages in chat after some time so we can't find intersection between partial conversation and entire historical conversation
 * <p> This merging algorithm works for Snapchat
 * <p>
 * <p> Reason why do we use partial interseption instead of full:
 * <p>Sometimes scrolling in Snapchat chat leads to duplications in our monitoring results
 * <p>The reason is complex:
 * <p> 1) We can't merge results with "intersection condition" only (when next messages portion intersects with history data in edges)
 * This is becuase Snapchat removes history messages so we need to deal when no intersection available
 * <p>2) So we should try to find intersection inside the history to prevent duplications.
 * But for some reason Snapchat monitoring sometimes misses one or two inner messages in the messages snippet (despite user see them on screen)
 * <p>Actually I don't know what is the reason.
 * One solution I can suggest is to compare not full regular messages snippet with history but some sub-snippet (like 2 or 3 messages)
 * This way we can much more decrease duplications in the full history data
 *
 * @return See {@link com.test.webmonitoringsample.messengers.BaseMessengerProcessor#mergeMessages} for details about return value
 */
public class Merger2 implements IMerger {
    @Override
    public <T> boolean merge(List<T> dst, List<T> src) {
        return merge(dst, src);
    }

    @Override
    public <T> boolean merge(List<T> dst, List<T> src, int dstElementsCountToMergeIn) {
        IMerger merger1 = new Merger1();


        boolean isMergedWithClassicAlgorythm = merger1.merge(dst, src);
        if (isMergedWithClassicAlgorythm) {
            return true;
        }

        // try to find partial intersection src with dst
        int srcSize = src.size();
        if (srcSize > 2 && isIntersectionFound(dst, src, 0, 2)) {
            return false;
        }

        // try to do it couple of times with different sublist to decrease the probability of hitting into the "missed" messages (see function description)
        if (srcSize > 3 && isIntersectionFound(dst, src, 1, 3)) {
            return false;
        }

        return dst.addAll(src);
    }

    /**
     * Uses {@link java.util.Collections#indexOfSubList} to find intersection of dst with some src sublist(based on begin and end index)
     *
     * @param dst
     * @param src
     * @param srcStartIndex
     * @param srcEndIndex
     * @return {@code true} if intersection was found. Oherwise {@code false}
     */
    private static <T> boolean isIntersectionFound(List<T> dst, List<T> src, int srcStartIndex, int srcEndIndex) {
        return indexOfSubList(dst, src.subList(srcStartIndex, srcEndIndex)) != -1;
    }

}
