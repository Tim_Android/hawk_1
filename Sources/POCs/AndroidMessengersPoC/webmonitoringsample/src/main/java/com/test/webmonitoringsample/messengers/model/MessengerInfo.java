package com.test.webmonitoringsample.messengers.model;

import java.util.List;

public class MessengerInfo {
    public enum Messenger {
        VIBER, WHATS_APP, FB_MESSENGER, FB_MESSENGER_LITE, SNAPCHAT, INSTAGRAM, KiK
    }

    public final Messenger messenger;
    public final List<Conversation> conversations;
    public final List<FileInfo> alreadyUsedImagesPaths;

    public MessengerInfo(Messenger messageInfo, List<Conversation> conversations, List<FileInfo> alreadyUsedImagesPaths) {
        this.messenger = messageInfo;
        this.conversations = conversations;
        this.alreadyUsedImagesPaths = alreadyUsedImagesPaths;
    }

    //////////////////////////////////////////////////////////

    /**
     * @return {@code true} if
     * {@link MessengerInfo} are equals
     * <b>AND</b>
     * {@link Conversation#equals} method returns true.
     * <p>
     * <p> See {@link Conversation#equals} method for understanding behaviour.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MessengerInfo c = (MessengerInfo) obj;
        return
                c.messenger.equals(this.messenger)
                && c.conversations.equals(this.conversations)
                && c.alreadyUsedImagesPaths.equals(this.alreadyUsedImagesPaths);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + messenger.hashCode();
        result = 31 * result + conversations.hashCode();
        result = 31 * result + alreadyUsedImagesPaths.hashCode();
        return result;
    }
    //////////////////////////////////////////////////////////

    public void clear() {
        conversations.clear();
        alreadyUsedImagesPaths.clear();
    }

    public void clear(Conversation conversation) {
        for(Message m : conversation.messages) {
            FileInfo usedFile = m.imageFileInfo;
            alreadyUsedImagesPaths.remove(usedFile);
        }

        conversation.messages.clear();
    }

}
