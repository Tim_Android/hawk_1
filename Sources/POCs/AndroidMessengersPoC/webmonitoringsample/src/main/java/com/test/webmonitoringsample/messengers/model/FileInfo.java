package com.test.webmonitoringsample.messengers.model;

import static com.test.webmonitoringsample.messengers.MessengerTools.areEqual;

public class FileInfo {
    public final String fileDir;
    /**
     * fileName in format "IMG-21061122-WA0003.jpg"
     */
    public final String fileName;
    /**
     * lastModifiedTime in format "17:26"
     */
    public final String lastModifiedTime;

    public FileInfo(String fileName, String lastModifiedTime, String fileDir) {
        this.fileName = fileName;
        this.lastModifiedTime = lastModifiedTime;
        this.fileDir = fileDir;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isMininmalisticEquals = equalsMinimalistic(obj);
        if (!isMininmalisticEquals) {
            return false;
        }

        FileInfo fi = (FileInfo) obj;
        return areEqual(fi.fileName, this.fileName)
                && areEqual(fi.fileDir, this.fileDir)
                && areEqual(fi.lastModifiedTime, this.lastModifiedTime)
                ;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + fileName.hashCode();
        result = 31 * result + fileDir.hashCode();
        result = 31 * result + lastModifiedTime.hashCode();
        return result;
    }
    //////////////////////////////////////////////////////////

    /**
     * This comparison doesn't use fields comparison
     * <p> To compare with fields use {@link #equals}
     */
    public boolean equalsMinimalistic(Object objRight) {
        if (this == objRight)
            return true;
        if (objRight == null)
            return false;
        if (this.getClass() != objRight.getClass())
            return false;
        return true;
    }
}
