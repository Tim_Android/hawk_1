package com.test.webmonitoringsample.messengers;

import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.Message;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.test.webmonitoringsample.messengers.model.IMessengerProcessor.COMMON_VISIBLE_DATE_FORMAT;
import static java.util.Collections.indexOfSubList;

public class MessengerTools {

    public static MessengerInfo getMessengerInfo(List<MessengerInfo> mi, MessengerInfo.Messenger m) {
        for (MessengerInfo tmp : mi) {
            if (tmp.messenger.equals(m)) {
                return tmp;
            }
        }
        return null;
    }

    public static Conversation getConversation(List<Conversation> conversations, String title) {
        for (Conversation conversation : conversations) {
            if (conversation.title.equals(title)) {
                return conversation;
            }
        }
        return null;
    }

    public static boolean areEqual(Object left, Object right) {
        return (left == null && right == null) || (left != null && left.equals(right));
    }

    public static String getCommonDateAsStringFromDate(Date date) {
        return COMMON_VISIBLE_DATE_FORMAT.format(date).toUpperCase();
    }
}
