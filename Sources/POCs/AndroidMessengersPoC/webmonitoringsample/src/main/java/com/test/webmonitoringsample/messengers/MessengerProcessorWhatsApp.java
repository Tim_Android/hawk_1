package com.test.webmonitoringsample.messengers;

import android.util.Log;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.TypeRef;
import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.FileInfo;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.IMessengerProcessor;
import com.test.webmonitoringsample.messengers.model.Message;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.jayway.jsonpath.Criteria.where;
import static com.jayway.jsonpath.Filter.filter;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.WHATS_APP;

public class MessengerProcessorWhatsApp extends BaseMessengerProcessor {
    public static final CharSequence PACKAGE_NAME = "com.whatsapp";
    public static final String WHATS_APP_IMAGES_IN_DIR = "/WhatsApp/Media/WhatsApp Images";
    public static final String WHATS_APP_IMAGES_OUT_DIR = WHATS_APP_IMAGES_IN_DIR + "/Sent";
    public static final String WHATS_APP_IMAGES_FILE_NAME_PREFIX = "IMG-";
    public static final String WHATS_APP_IMAGES_FILE_NAME_FORMAT = "IMG-%s-WA%s.jpg";
    public static final SimpleDateFormat WHATSAPP_VISIBLE_TIME_FORMAT = new SimpleDateFormat("HH:mm");


    @Override
    public FilesInfo getSdCardImages() {
        File[] inImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_IN_DIR);
        List<FileInfo> inImagesFileInfo = getFileInfo(inImages);

        File[] outImages = HelperMethods.getFilesOnFilesystem(WHATS_APP_IMAGES_OUT_DIR);
        List<FileInfo> outImagesFileInfo = getFileInfo(outImages);

        inImagesFileInfo.addAll(outImagesFileInfo);

        return new FilesInfo(inImagesFileInfo, outImagesFileInfo);
    }

    /**
     * @param files
     * @return
     */
    public static List<FileInfo> getFileInfo(File[] files) {
        List<FileInfo> result = new ArrayList<>();

        for (File f : files) {
            if (f.isDirectory()) {
                // skip dirs...
                continue;
            }

            String fileName = f.getName();
            if (!fileName.startsWith(WHATS_APP_IMAGES_FILE_NAME_PREFIX)) {
                // skip trash...
                continue;
            }

            long lastModifiedTime = f.lastModified();
            Date modified = new Date(lastModifiedTime);

            String lastModifiedString = WHATSAPP_VISIBLE_TIME_FORMAT.format(modified);
            FileInfo fileInfo = new FileInfo(f.getName(), lastModifiedString, f.getParent());

            result.add(fileInfo);
        }

        return result;
    }

    //public static final  String MESSAGES_ROOT_PATH = "$..[?(@.mViewIdResourceName=='android:id/list')].CHILDS";
    public static final String LINEAR_LAYOUT_LEVEL_5 = "$.ROOT[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')]";
    public static final String MESSAGES_ROOT_PATH = LINEAR_LAYOUT_LEVEL_5 + ".CHILDS[?(@.mClassName=='android.widget.RelativeLayout')].CHILDS[?(@.mClassName=='android.widget.ListView')].CHILDS[?(@.mClassName=='android.view.ViewGroup')]";

    public static final TypeRef<List<String>> LIST_STRTING_TYPE = new TypeRef<List<String>>() {
    };
    public static final TypeRef<List<Object>> LIST_OBJECT_TYPE = new TypeRef<List<Object>>() {
    };
    public static final TypeRef<List<Integer>> LIST_INTEGER_TYPE = new TypeRef<List<Integer>>() {
    };
    public static final TypeRef<Integer> INTEGER_TYPE = new TypeRef<Integer>() {
    };

    public static final Filter TEXT_TYPE_STRING_FILTER = filter(where("mText").type(String.class));
    public static final Filter CLASSNAME_IS_TEXTVIEW_FILTER = filter(where("mClassName").is("android.widget.TextView"));

    @Override
    public Conversation getPartialConversationFromRootJsonNode(String rootJson) {
        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(rootJson);

        JsonArray messagesJsonArray = getMessagesJsonArray(entireScreen);
        Integer messagesCount = getMessagesCount(messagesJsonArray);
        if (messagesCount == null || messagesCount.equals(0)) {
            return null;
        }

        String title = getTitle(entireScreen);
        if (title == null || title.isEmpty()) {
            return null;
        }

        // skip for simplification
        //List<String> participants = getParticipants(entireScreen, LIST_STRTING_TYPE);
        List<String> participants = Collections.emptyList();

        // skip for simplification
        //Boolean isGroup = getIsGroup(entireScreen, LIST_INTEGER_TYPE);
        Boolean isGroup = false;

        List<Message> messages = new ArrayList<>();
        for (int i = 0; i < messagesCount; ++i) {
            String visibleDate = getVisibleDate(messagesJsonArray, i);

            Boolean isImage = getIsImage(messagesJsonArray, i);

            String text = getText(messagesJsonArray, i, isImage);

            String time = getTime(messagesJsonArray, i);

            Boolean isIncomming = getIsIncomming(messagesJsonArray, i);

            String sender = getSender(messagesJsonArray, i);

            Message actualMessage = new Message(text, isIncomming, time, visibleDate, sender, isImage, null);
            messages.add(actualMessage);
        }

        Conversation actualConversation = new Conversation(title, participants, isGroup, messages);
        return actualConversation;
    }

    @Override
    public MessengerInfo.Messenger getCurrentMessenger() {
        return WHATS_APP;
    }

    /**
     * @param messagesJsonArray
     * @param i
     * @return true if image, false otherwise, null if we don't know...
     */
    @Override
    public Boolean getIsImage(JsonArray messagesJsonArray, int i) {
        String isImagePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/image')]", i);
        JsonArray isImageJsonArray = JsonPath.read(messagesJsonArray, isImagePath);
        List<Object> isImageList = AccessibilityNodeJsonSerializer.gson.fromJson(isImageJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isImage = isImageList != null && !isImageList.isEmpty();
        return isImage;
    }

    @Override
    public String getSender(JsonArray messagesJsonArray, int i) {
        //String senderPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText", i);
        String senderPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/name_in_group_tv')].mText", i);
        JsonArray senderJsonArray = JsonPath.read(messagesJsonArray, senderPath);
        List<String> senderList = AccessibilityNodeJsonSerializer.gson.fromJson(senderJsonArray, LIST_STRTING_TYPE.getType());
        String sender = getFirstStringOrNull(senderList);
        return sender;
    }

    @Override
    public Boolean getIsIncomming(JsonArray messagesJsonArray, int i) {
        //String statusPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.ImageView')]", i);
        String statusPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/status')]", i);
        JsonArray statusJsonArray = JsonPath.read(messagesJsonArray, statusPath);
        List<Object> statusList = AccessibilityNodeJsonSerializer.gson.fromJson(statusJsonArray, LIST_OBJECT_TYPE.getType());
        Boolean isIncomming = statusList == null || statusList.isEmpty();
        return isIncomming;
    }

    @Override
    public String getTime(JsonArray messagesJsonArray, int i) {
        //String timePath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText", i);
        String timePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/date')].mText", i);
        JsonArray timeJsonArray = JsonPath.read(messagesJsonArray, timePath);
        List<String> timeList = AccessibilityNodeJsonSerializer.gson.fromJson(timeJsonArray, LIST_STRTING_TYPE.getType());
        String time = getFirstStringOrNull(timeList);
        return time;
    }

    @Override
    public String getText(JsonArray messagesJsonArray, int i, boolean isImage) {
        //String textPath = String.format("$.[%1$d].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.FrameLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText.mText", i);
        String textPath;
        if (isImage) {
            textPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/caption')].mText.mText", i);
        } else {
            textPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/message_text')].mText.mText", i);
        }


        JsonArray textJsonArray = JsonPath.read(messagesJsonArray, textPath);
        List<String> messageList = AccessibilityNodeJsonSerializer.gson.fromJson(textJsonArray, LIST_STRTING_TYPE.getType());
        String text = getFirstStringOrNull(messageList);
        return text;
    }

    @Override
    public String getVisibleDate(JsonArray messagesJsonArray, int i) {
        String datePath = String.format("$.[%1$d].CHILDS[?, ?]['mText']", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath, CLASSNAME_IS_TEXTVIEW_FILTER, TEXT_TYPE_STRING_FILTER);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String date = getFirstStringOrNull(dateList);
        date = normalizeVisibleDate(date);

        return date;
    }

    @Override
    public String normalizeVisibleDate(String date) {
        return MessengerProcessorSnapchat.normalizeVisibleDateForSnapchat(date);
    }

    @Override
    public Boolean getIsGroup(Object entireScreen) {
        //TODO: DON'T USE ".." for search -- this works too long on devices
        String topRightIconsPath = "$..[?(@.mClassName=='android.support.v7.widget.al')].CHILDS.length()";
        List<Integer> topRightIconsList = JsonPath.parse(entireScreen).read(topRightIconsPath, LIST_INTEGER_TYPE);
        // there are 3 icons in top right corner of private conversation and just one icon in the group one
        Boolean isGroup = topRightIconsList == null || topRightIconsList.isEmpty() || topRightIconsList.get(0) < 3;
        return isGroup;
    }

    @Override
    public List<String> getParticipants(Object entireScreen) {
        //TODO: DON'T USE ".." for search -- this works too long on devices
        String participantsPath = "$..CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/conversation_contact_status')].mText";
        List<String> participantsList = JsonPath.parse(entireScreen).read(participantsPath, LIST_STRTING_TYPE);
        String participantsAsString = getFirstStringOrNull(participantsList);
        List<String> participants = resolveParticipants(participantsAsString);
        return participants;
    }

    @Override
    public JsonArray getMessagesJsonArray(Object entireScreen) {
        JsonArray messagesJsonArray = JsonPath.read(entireScreen, MESSAGES_ROOT_PATH);
        return messagesJsonArray;
    }


    @Override
    public String getTitle(Object entireScreen) {
        //String titlePath = "$..[?(@.mViewIdResourceName=='com.whatsapp:id/conversation_contact')].CHILDS[?(@.mViewIdResourceName=='com.whatsapp:id/conversation_contact_name')].mText";
        String titlePath = LINEAR_LAYOUT_LEVEL_5 + ".CHILDS[?(@.mClassName=='android.view.ViewGroup')].CHILDS[?(@.mClassName=='android.widget.RelativeLayout')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')].CHILDS[?(@.mClassName=='android.widget.TextView')].mText";

        List<String> titles = JsonPath.parse(entireScreen).read(titlePath, LIST_STRTING_TYPE);
        String title = getFirstStringOrNull(titles);
        return title;
    }

    @Override
    public List<String> resolveParticipants(String participantsAsString) {

        // "tap here for group info" couple of seconds after chat is shown
        // "online"(for private chat)

        if (participantsAsString == null
                || participantsAsString.equals("tap here for group info")
                || participantsAsString.equals("online")) {
            return Collections.emptyList();
        }

        return getParticipantsAsListList(participantsAsString);
    }

    @Override
    public List<String> getParticipantsAsListList(String participantsAsString) {
        return Arrays.asList(participantsAsString.split(", "));
    }

    /**
     * Resolves full date based on sibling's fulldate.
     *
     * @return {@code true} if at least one date has been resolved. Otherwise {@code false} (i.e. no enough info for resolving)
     */
    @Override
    public boolean resolveFullDate(List<Message> messages) {
        // resolve from the beginning
        boolean isResolvedFromBeginning = resolveForward(messages, 0);

        // resolve from the ending
        int lastResolvedIndex = -1;
        for (int i = messages.size() - 1; i >= 0; --i) {
            Message current = messages.get(i);
            if (current.resolvedDate != null) {
                lastResolvedIndex = i;
                break;
            }
        }

        boolean isResolvedFromEnd = false;
        if (lastResolvedIndex > 0) {
            isResolvedFromEnd = resolveForward(messages, lastResolvedIndex);
        }

        return isResolvedFromBeginning || isResolvedFromEnd;
    }

    private boolean resolveForward(List<Message> messages, int startIndex) {
        boolean isResolved = false;

        // resolve from the beginning
        for (int i = startIndex; i < messages.size(); ++i) {
            //isResolved = isResolved ? isResolved : resolveForwardByIndex(messages, i, needBreakOutParameter, needContinueOutParameter);
            try {
                Message current = messages.get(i);
                Message previous = i == startIndex ? null : messages.get(i - 1);
                Message next = i == messages.size() - 1 ? null : messages.get(i + 1);

                if (current.resolvedDate != null
                        && next != null && next.visibleDate == null && next.resolvedDate != null) {
                    // all dates from the beginning already resolved
                    break;
                }

                if (current.resolvedDate == null
                        && current.visibleDate != null) {
                    // resolve by visible date from the beginning
                    //MAY 24, 2017
                    String visibleDate = current.visibleDate;

                    SimpleDateFormat visible = IMessengerProcessor.COMMON_VISIBLE_DATE_FORMAT;
                    SimpleDateFormat resolved = new SimpleDateFormat("yyyyMMdd");
                    java.util.Date dDate = visible.parse(visibleDate);
                    String resolvedDate = resolved.format(dDate);
                    current.resolvedDate = resolvedDate;
                    isResolved = true;
                    continue;
                }

                if (previous == null) {
                    // this is first message. We can't find previous for it..
                    continue;
                }

                // resolve by resolvedDate in previous message
                if (current.resolvedDate == null && previous.resolvedDate != null) {
                    // resolve by prev resolved date from the beginning
                    current.resolvedDate = previous.resolvedDate;
                    isResolved = true;
                }

            } catch (ParseException e) {
                Log.d("ERROR", "Parsing error:" + e.getMessage());
                e.printStackTrace();
                continue;
            }
        }

        return isResolved;
    }

    /**
     * Try to resolve full image path based on
     * <p> 1) message date
     * <p> 2) info about already used images
     * <p> 3) info about images available on filesystem
     *
     * @return {@code true} if paths were resolved. Otherwise {@code false} (i.e. no enough info for resolving)
     */
    @Override
    public void resolveImagesPath(List<Message> messages, List<FileInfo> alreadyUsedImages, FilesInfo availableSdCardImages) {
        for (Message message : messages) {
            if (!message.isImage) {
                continue;
            }

            if (message.imageFileInfo != null) {
                continue;
            }

            if (message.resolvedDate == null) {
                continue;
            }

            if (message.time == null) {
                continue;
            }

            List<FileInfo> availableFiles = message.isIncoming ? availableSdCardImages.inFilesInfo : availableSdCardImages.outFilesInfo;
            if (availableFiles.isEmpty()) {
                continue;
            }

            // for simplicity, because all files are in one directory
            String fileDir = availableFiles.get(0).fileDir;

            for (int i = 0; i < 100; ++i) {
                NumberFormat nf = new DecimalFormat("0000");
                String potentialFileNum = nf.format(i);

                String potentialImageName = String.format(MessengerProcessorWhatsApp.WHATS_APP_IMAGES_FILE_NAME_FORMAT, message.resolvedDate, potentialFileNum);
                FileInfo potentialImageFileInfo = new FileInfo(potentialImageName, message.time, fileDir);
                if (alreadyUsedImages.contains(potentialImageFileInfo)) {
                    continue;
                }

                if (availableFiles.contains(potentialImageFileInfo)) {
                    alreadyUsedImages.add(potentialImageFileInfo);
                    message.imageFileInfo = potentialImageFileInfo;
                    break;
                }
            }
        }
    }
}
