package com.test.webmonitoringsample.browsers;

import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.test.webmonitoringsample.HelperMethods;

import lombok.Cleanup;

public class BrowserProcessor {
    public static void process(AccessibilityEvent event) {
        CharSequence eventPackageName = event.getPackageName();
        if (eventPackageName == null) {
            return;
        }

        if (UrlProcessorChrome.CHROME_PACKAGE_NAME.equals(eventPackageName)
//                || UrlProcessorFirefox.FIREFOX_PACKAGE_NAME.equals(eventPackageName)
//                || UrlProcessorAndroidBrowser.NATIVE_BROWSER_PACKAGE_NAME.equals(eventPackageName)
                ) {
            HelperMethods.printEventInfo(event);

            @Cleanup("recycle") AccessibilityNodeInfo initialNodeInfo = event.getSource();

//            @Cleanup("recycle") AccessibilityNodeInfo rootNodeInfo = HelperMethods.getRoot(initialNodeInfo);
//            AccessibilityNodeInfo root = rootNodeInfo == null ? initialNodeInfo : rootNodeInfo;

            HelperMethods.printNodeTreeDepth(initialNodeInfo, AccessibilityEvent.eventTypeToString(event.getEventType()));
            HelperMethods.printNodeTreeBreadth(initialNodeInfo);
        }

//        int eventType = event.getEventType();
//
//        if (eventPackageName.equals(UrlProcessorChrome.CHROME_PACKAGE_NAME)) {
//            if (eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
//                    || eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
//                breadthFirstSearchNodeProcessing(event.getSource(), new UrlProcessorChrome(event));
//            }
//        }
//
//        else if (eventPackageName.equals(UrlProcessorFirefox.FIREFOX_PACKAGE_NAME)) {
//            if (eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
//                    || eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
//                breadthFirstSearchNodeProcessing(event.getSource(), new UrlProcessorFirefox());
//            }
//        } else if (eventPackageName.equals(UrlProcessorAndroidBrowser.NATIVE_BROWSER_PACKAGE_NAME)) {
//            if (eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
//                    || eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
//                breadthFirstSearchNodeProcessing(event.getSource(), new UrlProcessorAndroidBrowser());
//            }
//        }
    }
}
