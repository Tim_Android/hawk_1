package com.test.webmonitoringsample.messengers;

import android.util.Log;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.test.webmonitoringsample.messengers.model.FilesInfo;
import com.test.webmonitoringsample.messengers.model.Message;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp.LIST_STRTING_TYPE;
import static com.test.webmonitoringsample.messengers.MessengerTools.getCommonDateAsStringFromDate;
import static com.test.webmonitoringsample.messengers.model.MessengerInfo.Messenger.SNAPCHAT;

public class MessengerProcessorSnapchat extends BaseMessengerProcessor {
    public static final CharSequence PACKAGE_NAME = "com.snapchat.android";
    private static final String MESSAGES_ROOT_PATH = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/chat_message_list')].CHILDS[?(@.mClassName=='android.widget.LinearLayout')]";

    @Override
    public MessengerInfo.Messenger getCurrentMessenger() {
        return SNAPCHAT;
    }

    @Override
    public Boolean getIsImage(JsonArray messagesJsonArray, int i) {
        return false;
    }

    @Override
    public String getSender(JsonArray messagesJsonArray, int i) {
        String senderPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/chat_message_header_title')].mText", i);

        JsonArray sendersJsonArray = JsonPath.read(messagesJsonArray, senderPath);
        List<String> sendersList = AccessibilityNodeJsonSerializer.gson.fromJson(sendersJsonArray, LIST_STRTING_TYPE.getType());
        String sender = getFirstStringOrNull(sendersList);
        return sender;
    }

    @Override
    public Boolean getIsIncomming(JsonArray messagesJsonArray, int i) {
        String sender = getSender(messagesJsonArray, i);
        return !"ME".equals(sender);
    }

    @Override
    public String getTime(JsonArray messagesJsonArray, int i) {
        String timePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/chat_message_time')].mText", i);
        JsonArray timeJsonArray = JsonPath.read(messagesJsonArray, timePath);
        List<String> timeList = AccessibilityNodeJsonSerializer.gson.fromJson(timeJsonArray, LIST_STRTING_TYPE.getType());
        String time = getFirstStringOrNull(timeList);
        return time;
    }

    @Override
    public String getText(JsonArray messagesJsonArray, int i, boolean isImage) {
        String textPath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/chat_message_user_text')].mText.mText", i);

        JsonArray textJsonArray = JsonPath.read(messagesJsonArray, textPath);
        List<String> messageList = AccessibilityNodeJsonSerializer.gson.fromJson(textJsonArray, LIST_STRTING_TYPE.getType());
        String text = getFirstStringOrNull(messageList);
        return text;
    }

    @Override
    public String getVisibleDate(JsonArray messagesJsonArray, int i) {
        String datePath = String.format("$.[%1$d]..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/conversation_date')].mText", i);
        JsonArray dateJsonArray = JsonPath.read(messagesJsonArray, datePath);
        List<String> dateList = AccessibilityNodeJsonSerializer.gson.fromJson(dateJsonArray, LIST_STRTING_TYPE.getType());
        String date = getFirstStringOrNull(dateList);

        if (date != null) {
            date = normalizeVisibleDate(date.toUpperCase());
        }

        return date;
    }

    @Override
    public Boolean getIsGroup(Object entireScreen) {
        return null;
    }

    @Override
    public List<String> getParticipants(Object entireScreen) {
        return null;
    }

    @Override
    public JsonArray getMessagesJsonArray(Object entireScreen) {
        JsonArray messagesJsonArray = JsonPath.read(entireScreen, MESSAGES_ROOT_PATH);
        return messagesJsonArray;
    }

    @Override
    public String getTitle(Object entireScreen) {
        String titlePath = "$.ROOT..CHILDS[?(@.mViewIdResourceName=='com.snapchat.android:id/chat_friends_name')].mText";
        List<String> titles = JsonPath.parse(entireScreen).read(titlePath, LIST_STRTING_TYPE);
        String title = getFirstStringOrNull(titles);
        return title;
    }

    @Override
    public List<String> resolveParticipants(String participantsAsString) {
        return null;
    }

    @Override
    public List<String> getParticipantsAsListList(String participantsAsString) {
        return null;
    }

    @Override
    public FilesInfo getSdCardImages() {
        return null;
    }

    @Override
    public String normalizeVisibleDate(String visibleDate) {
        return normalizeVisibleDateForSnapchat(visibleDate);
    }

    public static String normalizeVisibleDateForSnapchat(String date) {
        if (date != null) {
            // normalizing...
            if (date.equals("TODAY")) {
                Date todayDate = new Date();
                date = getCommonDateAsStringFromDate(todayDate);
            } else if (date.equals("YESTERDAY")) {
                Date yesterdayDate = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
                date = getCommonDateAsStringFromDate(yesterdayDate);
            }

            // check trash like "XXX added you", "XXX created group ..." messages
            try {
                Date date1 = COMMON_VISIBLE_DATE_FORMAT.parse(date);
            } catch (ParseException e) {
                //Log.d("ERROR", e.getMessage());
                e.printStackTrace();
                date = null;
            }
        }

        return date;
    }
}
