package com.test.webmonitoringsample.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.webmonitoringsample.R;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.Message;

import java.io.File;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    private static final int INCOMMING_MESSAGE = 0;
    private static final int OUTGOING_MESSAGE = 1;
    private static final int OUTGOING_WITH_DATE_MESSAGE = 2;
    private static final int INCOMMINGWITH_DATE_MESSAGE = 3;
    private Conversation conversation;

    public MessagesAdapter(Conversation conversation) {
        this.conversation = conversation;
    }

    @Override
    public int getItemViewType(int position) {
        return isIncoming(position) ? INCOMMING_MESSAGE : OUTGOING_MESSAGE;
    }

    private boolean isIncoming(int position) {
        Boolean isInoming = conversation.messages.get(position).isIncoming;
        return isInoming != null && isInoming;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        boolean isIncoming = viewType == INCOMMING_MESSAGE;
        int layoutId = isIncoming ? R.layout.in_message_element_layout : R.layout.out_message_element_layout;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = conversation.messages.get(position);

        // TODO: Need remove this in production!
        // Don't take space for message if for some reason we have empty message...
        if (message.time == null
                && message.text == null
                && message.sender == null
                && message.visibleDate == null
                && message.isImage == false) {
            holder.full_messageView.setVisibility(View.GONE);
        } else {
            holder.full_messageView.setVisibility(View.VISIBLE);
        }

        // TODO: Some messengers have visible date in message entity (like WA) but others have visible date as separate entity (like FB)
        // So show visible date in corresponding manner...
        if (message.text == null
                && message.sender == null
                && message.isImage == false
                && message.visibleDate != null) {
            holder.messageView.setVisibility(View.GONE);

            holder.visibleDateView.setVisibility(View.VISIBLE);
            holder.date.setVisibility(View.VISIBLE);

            String date = message.visibleDate;
            String time = message.time != null ? message.time : "";

            String visibleDate = date + " " + time;
            holder.date.setText(visibleDate);
            return;
        } else {
            holder.messageView.setVisibility(View.VISIBLE);
        }

        /////////////////////////////////////////////////
        if (holder.date != null) {
            if (message.visibleDate == null) {
                holder.visibleDateView.setVisibility(View.GONE);
                holder.date.setVisibility(View.GONE);
            } else {
                String date = message.visibleDate;
                holder.visibleDateView.setVisibility(View.VISIBLE);
                holder.date.setVisibility(View.VISIBLE);
                holder.date.setText(date);
            }
        }

        if (holder.time != null) {
            if (message.time == null) {
                holder.time.setVisibility(View.GONE);
            } else {
                String time = message.time;
                holder.time.setVisibility(View.VISIBLE);
                holder.time.setText(time);
            }
        }

        if (holder.text != null) {
            if (message.text == null) {
                holder.text.setVisibility(View.GONE);
            } else {
                String text = message.text;
                holder.text.setVisibility(View.VISIBLE);
                holder.text.setText(text);
            }
        }

        if (holder.sender != null) {
            if (message.sender == null) {
                holder.sender.setVisibility(View.GONE);
            } else {
                String sender = message.sender;
                holder.sender.setVisibility(View.VISIBLE);
                holder.sender.setText(sender);
            }
        }

        if (holder.image != null) {
            if (message.isImage == false) {
                holder.image.setVisibility(View.GONE);
            } else {
                holder.image.setVisibility(View.VISIBLE);

                String imagePath = "";
                if (message.imageFileInfo != null) {
                    imagePath = message.imageFileInfo.fileDir + "/" + message.imageFileInfo.fileName;
                }

                File imgFile = new File(imagePath);

                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.image.setImageBitmap(myBitmap);
                } else {
                    holder.image.setImageResource(R.drawable.placeholder);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return conversation.messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View full_messageView;
        public View visibleDateView;
        public TextView date;
        public View messageView;
        public TextView text;
        public TextView time;
        public TextView sender;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);
            full_messageView = v.findViewById(R.id.full_message_view);
            visibleDateView = v.findViewById(R.id.visible_date);
            messageView = v.findViewById(R.id.message_view);
            text = (TextView) v.findViewById(R.id.message_text);
            time = (TextView) v.findViewById(R.id.message_time);
            sender = (TextView) v.findViewById(R.id.message_sender);
            date = (TextView) v.findViewById(R.id.message_date);
            image = (ImageView) v.findViewById(R.id.message_image);

//            full_messageView.setAccessibilityDelegate(new View.AccessibilityDelegate() {
//                  @Override
//                  public void onInitializeAccessibilityEvent(View host, AccessibilityEvent event) {
//                      //super.onInitializeAccessibilityEvent(host, event);
//                  }
//              }
//            );

//            full_messageView.setAccessibilityDelegate(new View.AccessibilityDelegate() {
//                @Override
//                public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
//                    //super.onInitializeAccessibilityNodeInfo(host, info);
//                    int a = 6;
//                    int b = 8;
//                }
//            });

            text.setAccessibilityDelegate(new View.AccessibilityDelegate() {
                @Override
                public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
                    //super.onInitializeAccessibilityNodeInfo(host, info);
                    int a = 6;
                    int b = 8;
                }
            });

            if (sender != null) {
                sender.setAccessibilityDelegate(new View.AccessibilityDelegate() {
                    @Override
                    public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
                        //super.onInitializeAccessibilityNodeInfo(host, info);
                        int a = 6;
                        int b = 8;
                    }
                });
            }

            //full_messageView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS);
//            time.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
            time.setAccessibilityDelegate(new View.AccessibilityDelegate() {
//                @Override
//                public boolean dispatchPopulateAccessibilityEvent(View host, AccessibilityEvent event) {
//                    //return true;
//                    return super.dispatchPopulateAccessibilityEvent(host, event);
//                }
//
//                @Override
//                public boolean onRequestSendAccessibilityEvent(ViewGroup host, View child, AccessibilityEvent event) {
//                    //return false;
//                    return super.onRequestSendAccessibilityEvent(host, child, event);
//                }

                @Override
                public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
                    //super.onInitializeAccessibilityNodeInfo(host, info);
                    int a = 6;
                    int b = 8;
                }
            });

        }
    }
}
