package com.test.webmonitoringsample.browsers;

import android.view.accessibility.AccessibilityNodeInfo;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.INodeProcessor;

class UrlProcessorFirefox implements INodeProcessor {
    private static final String FIREFOX_URL_WIDGET_CLASS = "android.widget.TextView";
    private static final String FIREFOX_URL_WIDGET_ID = "org.mozilla.firefox:id/url_bar_title";
    static final String FIREFOX_PACKAGE_NAME = "org.mozilla.firefox";

    @Override
    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
        if (isMozillaUrlBar(nodeInfo) && HelperMethods.isUrlValid(nodeInfo.getText())) {
            HelperMethods.addUrlIfNeed(nodeInfo, null);
            return true;
        } else {
            return false;
        }
    }

    private boolean isMozillaUrlBar(AccessibilityNodeInfo nodeInfo) {
        return FIREFOX_URL_WIDGET_CLASS.equals(nodeInfo.getClassName()) && FIREFOX_URL_WIDGET_ID.equals(nodeInfo.getViewIdResourceName());
    }
}
