package com.test.webmonitoringsample.tools;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class JsonSerrializationExclusionStrategy {

    public static ExclusionStrategy getCommonStrategy() {
        return new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return !
                        ("mText".equals(f.getName())
                                || "mClassName".equals(f.getName())
                                || "mViewIdResourceName".equals(f.getName())
                                || "mContentDescription".equals(f.getName())

                                // Need to understand of Instagram message direction
                                || "mBoundsInParent".equals(f.getName())
                                || "mBoundsInScreen".equals(f.getName())
                                || "left".equals(f.getName())
                                || "right".equals(f.getName())
                        );
//                return false;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        };
    }
}
