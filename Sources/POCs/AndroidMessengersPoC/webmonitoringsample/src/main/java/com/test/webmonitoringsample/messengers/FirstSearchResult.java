package com.test.webmonitoringsample.messengers;

import android.view.accessibility.AccessibilityNodeInfo;

public class FirstSearchResult {
    public final AccessibilityNodeInfo nodeInfo;
    public final boolean isIncomingParameter;
    public final int depth;

    public FirstSearchResult(AccessibilityNodeInfo nodeInfo, boolean isIncomingParameter, int depth) {
        this.nodeInfo = nodeInfo;
        this.isIncomingParameter = isIncomingParameter;
        this.depth = depth;
    }

    public FirstSearchResult() {
        this.nodeInfo = null;
        this.isIncomingParameter = false;
        this.depth = -1;
    }
}
