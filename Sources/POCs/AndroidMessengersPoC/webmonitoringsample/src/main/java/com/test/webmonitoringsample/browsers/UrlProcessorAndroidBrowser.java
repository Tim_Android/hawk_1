package com.test.webmonitoringsample.browsers;

import android.view.accessibility.AccessibilityNodeInfo;

import com.test.webmonitoringsample.HelperMethods;
import com.test.webmonitoringsample.INodeProcessor;

class UrlProcessorAndroidBrowser implements INodeProcessor {
    private static final String NATIVE_BROWSER_URL_WIDGET_CLASS = "android.widget.EditText";
    private static final String NATIVE_BROWSER_URL_WIDGET_ID = "com.android.browser:id/url";
    static final String NATIVE_BROWSER_PACKAGE_NAME = "com.android.browser";

    @Override
    public boolean process(AccessibilityNodeInfo nodeInfo, int depth) {
        if (isNativeBrowserBar(nodeInfo) && HelperMethods.isUrlValid(nodeInfo.getText())) {
            HelperMethods.addUrlIfNeed(nodeInfo, null);
            return true;
        } else {
            return false;
        }
    }

    private boolean isNativeBrowserBar(AccessibilityNodeInfo nodeInfo) {
        return NATIVE_BROWSER_URL_WIDGET_CLASS.equals(nodeInfo.getClassName()) && NATIVE_BROWSER_URL_WIDGET_ID.equals(nodeInfo.getViewIdResourceName());
    }
}
