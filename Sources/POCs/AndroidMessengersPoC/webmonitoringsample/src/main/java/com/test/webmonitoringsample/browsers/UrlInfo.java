package com.test.webmonitoringsample.browsers;

public class UrlInfo {
    public String browserName;
    public String name;
    public long time;

    public UrlInfo(String browserName, String url, long time) {
        this.browserName = browserName;
        this.name = url;
        this.time = time;
    }
}
