package com.test.webmonitoringsample;

import com.test.webmonitoringsample.merger.IMerger;
import com.test.webmonitoringsample.merger.impl.Merger4;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergerTests {

    private static final IMerger merger = new Merger4();

    @Test
    public void nullSrcTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = null;
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void nullDstTest() {
        List<Integer> dst = null;
        List<Integer> src = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> exp = null;

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void nullSrcAndDstTest() {
        List<Integer> dst = null;
        List<Integer> src = null;
        List<Integer> exp = null;

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void emptySrcTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = new ArrayList<>();
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void emptyDstTest() {
        List<Integer> dst = new ArrayList<>();
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void emptySrcAndDstTest() {
        List<Integer> dst = new ArrayList<>();
        List<Integer> src = new ArrayList<>();
        List<Integer> exp = new ArrayList<>();

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void fullSublistExistsInsideTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        List<Integer> src = new ArrayList<>(Arrays.asList(4, 5));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }


    @Test
    public void fullSublistExistsBeginTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        List<Integer> src = new ArrayList<>(Arrays.asList(3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void fullSublistExistsEndTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        List<Integer> src = new ArrayList<>(Arrays.asList(5, 6));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertFalse(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void noSublistExistsTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 1, 2));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside1Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(4, 5, 8));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 8, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside2Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(8, 4, 5));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 8, 4, 5, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside3Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(2, 8, 4, 5));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 2, 8, 4, 5, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside4Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(4, 5, 2, 8));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 2, 8, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside5Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 4, 5, 1));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 1, 4, 5, 1, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside6Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 4, 5, 1));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 1, 2, 4, 5, 1, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside7Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 4, 5, 1, 2));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 1, 4, 5, 1, 2, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside8Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(2, 1, 4, 5, 1, 2));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 2, 1, 4, 5, 1, 2, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside9Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(2, 1, 4, 5, 6, 1, 2));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 2, 1, 4, 5, 6, 1, 2, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside10Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7, 8, 9, 10));
        List<Integer> src = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4, 9, 10));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside11Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(77, 88, 4, 5, 6, 7, 8, 9, 10));
        List<Integer> src = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(77, 88, 12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4, 9, 10));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside12Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(77, 88, 99, 4, 5, 6, 7, 8, 9, 10));
        List<Integer> src = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(77, 88, 99, 12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4, 9, 10));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside13Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> src = new ArrayList<>(Arrays.asList(88, 99, 4, 5, 6, 7, 8, 9, 10));
        List<Integer> exp = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 88, 99, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside14Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> src = new ArrayList<>(Arrays.asList(77, 88, 99, 4, 5, 6, 7, 8, 9, 10));
        List<Integer> exp = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 77, 88, 99, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void sublistInside15Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 4, 5, 6, 7, 8, 1, 2, 3, 4));
        List<Integer> src = new ArrayList<>(Arrays.asList(99, 4, 5, 6, 7, 8, 9));
        List<Integer> exp = new ArrayList<>(Arrays.asList(12, 13, 11, 10, 99, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void singleElementBeginIntersectionTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(5, 6, 7, 8, 9));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void singleElementEndIntersectionTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(5, 6, 7, 8, 9));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void singleElementMiddleIntersectionTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(22, 33, 3, 44, 55));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 22, 33, 3, 44, 55));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }


    @Test
    public void mergeInTheMiddleTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(11, 12, 4, 5, 13, 14));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 11, 12, 4, 5, 13, 14, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void intersectionAtBeginningTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void intersectionAtEndingTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(4, 5, 6, 7));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void duplicatesInSrcBeginningMessgesTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(4, 5, 4, 5, 6, 7));
        List<Integer> exp = new ArrayList<>(Arrays.asList(3, 4, 5, 4, 5, 6, 7));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void duplicatesInSrcEndingMessgesTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 3, 4, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void recursyMergerBeginningTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18));
        List<Integer> src = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void recursyMergerEndingTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
        List<Integer> src = new ArrayList<>(Arrays.asList(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void recursyMergerEnding2Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 6));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void oneElementIntersectionInsideTest1() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 6, 7, 1, 2, 3, 4));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void oneElementIntersectionInsideTest2() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 6, 7, 1, 2, 3, 4, 5, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void oneElementIntersectionInsideTest3() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 6, 7));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 6, 7, 1, 2, 3, 4, 5, 6));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void skipBeginningOfLongDestinationList1Test() {
        List<Integer> dst = new ArrayList<>();
        for (int i = 0; i < 100; ++i) {
            dst.add(i);
        }

        List<Integer> diff = new ArrayList<>(Arrays.asList(1000, 1001, 1002));

        List<Integer> src = new ArrayList<>(Arrays.asList(50, 51, 52));
        src.addAll(diff);

        List<Integer> exp = new ArrayList<>(dst);
        exp.addAll(src);

        int COUNTS_TO_MERGE_IN = 40;
        boolean isMerged = merger.merge(dst, src, COUNTS_TO_MERGE_IN);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void skipBeginningOfLongDestinationList2Test() {
        List<Integer> diff = new ArrayList<>(Arrays.asList(1000, 1001, 1002));

        List<Integer> dst = new ArrayList<>();
        List<Integer> exp = new ArrayList<>();
        for (int i = 0; i < 100; ++i) {
            dst.add(i);
            exp.add(i);

            if (i == 52) {
                exp.addAll(diff);
            }
        }

        List<Integer> src = new ArrayList<>(Arrays.asList(50, 51, 52));
        src.addAll(diff);

        int COUNTS_TO_MERGE_IN = 50; // use range [50...99] of dst for merging
        boolean isMerged = merger.merge(dst, src, COUNTS_TO_MERGE_IN);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void skipBeginningOfLongDestinationList3Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));
        List<Integer> src = new ArrayList<>(Arrays.asList(3, 4, 998, 999));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 3, 4, 998, 999));

        int COUNTS_TO_MERGE_IN = 10;
        boolean isMerged = merger.merge(dst, src, COUNTS_TO_MERGE_IN);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void skipBeginningOfLongDestinationList4Test() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));
        List<Integer> src = new ArrayList<>(Arrays.asList(1, 3, 4, 998, 999));
        List<Integer> exp = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 1, 3, 4, 998, 999));

        int COUNTS_TO_MERGE_IN = 10;
        boolean isMerged = merger.merge(dst, src, COUNTS_TO_MERGE_IN);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void mergeInsideTwoElementsIntersectionTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(7, 8, 3, 4, 9, 0));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 7, 8, 3, 4, 9, 0, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }

    @Test
    public void mergeInsideTwoElementsIntersectionShiftedTest() {
        List<Integer> dst = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> src = new ArrayList<>(Arrays.asList(0, 7, 8, 3, 4, 9, 0));
        List<Integer> exp = new ArrayList<>(Arrays.asList(1, 2, 0, 7, 8, 3, 4, 9, 0, 5));

        boolean isMerged = merger.merge(dst, src);

        Assert.assertTrue(isMerged);
        Assert.assertEquals(exp, dst);
    }
}
