package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.test.webmonitoringsample.messengers.MessengerProcessorFbMsgr;
import com.test.webmonitoringsample.messengers.model.Conversation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Date;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;
import static com.test.webmonitoringsample.messengers.MessengerTools.getCommonDateAsStringFromDate;

public class FbMsgrTests {

    private Gson gson;
    private MessengerProcessorFbMsgr messengerProcessorFbMsgr;

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
        messengerProcessorFbMsgr = new MessengerProcessorFbMsgr();
    }

    @Test
    public void privateChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgr/private_messages_actual.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgr/private_messages_expected.json");

        Conversation actual = messengerProcessorFbMsgr.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void groupChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgr/group_messages_actual.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgr/group_messages_expected.json");

        Conversation actual = messengerProcessorFbMsgr.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void groupChatSwyping() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgr/group_swiping.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgr/group_swiping_expected.json");

        Conversation actual = messengerProcessorFbMsgr.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void privateMessagesWithImages() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgr/image_gif_message_date.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgr/image_gif_message_date_expected.json");

        Conversation actual = messengerProcessorFbMsgr.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void privateMessagesWithImagesAndCaption() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgr/image_with_caption.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgr/image_with_caption_expected.json");

        Conversation actual = messengerProcessorFbMsgr.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void dateNormalizerOnlyTime() throws Exception {
        String actual = messengerProcessorFbMsgr.normalizeVisibleDate("14:55");
        String expected = getCommonDateAsStringFromDate(new Date());
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Ignore
    // Useful test but hard to test without tricks.
    // SO use it just in case of some problems during parsing...
    public void dateNormalizerDayAndTime() throws Exception {
        String actual = messengerProcessorFbMsgr.normalizeVisibleDate("MON AT 13:51");
        String expected = "JUN 19, 2017";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void dateNormalizerMonthDateTime() throws Exception {
        String actual = messengerProcessorFbMsgr.normalizeVisibleDate("JUN 7 AT 09:51");
        String expected = "JUN 07, 2017";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void dateNormalizerYearMonthDateTime() throws Exception {
        String actual = messengerProcessorFbMsgr.normalizeVisibleDate("JUNE 23, 2016 AT 22:24");
        String expected = "JUN 23, 2016";
        Assert.assertEquals(expected, actual);
    }
}
