package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.Configuration;
import com.test.webmonitoringsample.messengers.MessengerProcessorInstagram;
import com.test.webmonitoringsample.messengers.model.Conversation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;

public class InstagramTests {
    private Gson gson;
    private MessengerProcessorInstagram messengerProcessorInstagram;

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
        messengerProcessorInstagram = new MessengerProcessorInstagram();
    }

    @Test
    public void privateChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "instagram/private_messages_actual.json");
        String expectedSerrialized = TestTools.testFileToString(this, "instagram/private_messages_expected.json");

        Conversation actual = messengerProcessorInstagram.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void skipShiftedScreenTest() throws Exception {
        String shiftedScreen = TestTools.testFileToString(this, "instagram/shiftedScreen.json");
        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(shiftedScreen);
        JsonArray messagesJsonArray = messengerProcessorInstagram.getMessagesJsonArray(entireScreen);
        Assert.assertNull(messagesJsonArray);
    }

    @Test
    public void notShiftedScreenTest() throws Exception {
        String shiftedScreen = TestTools.testFileToString(this, "instagram/private_messages_actual.json");
        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(shiftedScreen);
        JsonArray messagesJsonArray = messengerProcessorInstagram.getMessagesJsonArray(entireScreen);
        Assert.assertNotNull(messagesJsonArray.size() > 0);
    }

    @Test
    public void dateResolver1Test() throws Exception {
        String visibleDate = "Jul 22, 2:20 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "JUL 22, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }

    @Test
    @Ignore
    public void dateResolver2Test() throws Exception {
        String visibleDate = "Today 1:15 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "JUL 28, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }

    @Test
    @Ignore
    public void dateResolver3Test() throws Exception {
        String visibleDate = "Yesterday 1:15 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "JUL 27, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }

    @Test
    @Ignore
    public void dateResolver4Test() throws Exception {
        String visibleDate = "Yesterday 1:15 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "JUL 27, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }

    @Test
    @Ignore
    public void dateResolver5Test() throws Exception {
        String visibleDate = "Wed 1:15 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "JUL 26, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }

    @Test
    public void dateResolver6Test() throws Exception {
        String visibleDate = "Oct 7, 9:51 PM";
        String normalizedDate = MessengerProcessorInstagram.normalizeVisibleDateForInstagram(visibleDate);
        String expected = "OCT 07, 2017";

        Assert.assertEquals(expected, normalizedDate);
    }
}
