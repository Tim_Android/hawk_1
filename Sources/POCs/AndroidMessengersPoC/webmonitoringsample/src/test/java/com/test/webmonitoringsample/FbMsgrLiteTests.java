package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.test.webmonitoringsample.messengers.MessengerProcessorFbMsgr;
import com.test.webmonitoringsample.messengers.MessengerProcessorFbMsgrLite;
import com.test.webmonitoringsample.messengers.model.Conversation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;

public class FbMsgrLiteTests {
    private Gson gson;
    private MessengerProcessorFbMsgrLite messengerProcessorFbMsgrLite;

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
        messengerProcessorFbMsgrLite = new MessengerProcessorFbMsgrLite();
    }

    @Test
    public void privateChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgrLite/group_messages_actual.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgrLite/group_messages_expected.json");

        Conversation actual = messengerProcessorFbMsgrLite.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void imagesInGroupTest() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "fbMsgrLite/group_images.json");
        String expectedSerrialized = TestTools.testFileToString(this, "fbMsgrLite/group_images_expected.json");

        Conversation actual = messengerProcessorFbMsgrLite.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }
}
