package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.TypeRef;
import com.test.webmonitoringsample.messengers.MessengerProcessorWhatsApp;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.Message;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;
import static com.test.webmonitoringsample.messengers.MessengerTools.getCommonDateAsStringFromDate;
import static com.test.webmonitoringsample.messengers.model.IMessengerProcessor.COMMON_VISIBLE_DATE_FORMAT;

public class WhatsAppTests {
    private Gson gson;
    private MessengerProcessorWhatsApp messengerProcessorWhatsApp;

    private static Type listMessageType = new TypeRef<List<Message>>() {
    }.getType();
    private static Type conversationType = new TypeRef<Conversation>() {
    }.getType();

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
        messengerProcessorWhatsApp = new MessengerProcessorWhatsApp();
    }

    @Test
    public void privateChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "whatsApp/InAndOutMessage.json");
        String expectedSerrialized = TestTools.testFileToString(this, "whatsApp/InAndOutMessage_expected.json");

        Conversation actual = messengerProcessorWhatsApp.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void groupChatMessages() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "whatsApp/GroupMessages.json");
        String expectedSerrialized = TestTools.testFileToString(this, "whatsApp/GroupMessages_expected.json");

        Conversation actual = messengerProcessorWhatsApp.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void groupChat_7Messages() throws Exception {
        String testFile = TestTools.testFileToString(this, "whatsApp/Group_7Messages.json");

        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(testFile);

        JsonArray messagesJsonArray = messengerProcessorWhatsApp.getMessagesJsonArray(entireScreen);
        Integer actualMessagesCount = messengerProcessorWhatsApp.getMessagesCount(messagesJsonArray);
        Integer expectedMessagesCount = 7;
        Assert.assertEquals(expectedMessagesCount, actualMessagesCount);

        String actualTitle = messengerProcessorWhatsApp.getTitle(entireScreen);
        String expectedTitle = "Test group";
        Assert.assertEquals(expectedTitle, actualTitle);


        List<String> actualParticipants = messengerProcessorWhatsApp.getParticipants(entireScreen);
        List<String> expectedParticipants = Arrays.asList("Andrey", "Iphone", "You");
        Assert.assertEquals(expectedParticipants, actualParticipants);

        Boolean actualIsGroup = messengerProcessorWhatsApp.getIsGroup(entireScreen);
        Boolean expectedIsGroup = true;
        Assert.assertEquals(expectedIsGroup, actualIsGroup);

        /////////////////////////////////////////
        int messageNumOut = 3;

        boolean actualIsImageOut = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        boolean expectedIsImageOut = false;
        Assert.assertEquals(expectedIsImageOut, actualIsImageOut);

        String actualMessageOut = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumOut, expectedIsImageOut);
        String expectedMessageOut = "Out 1";
        Assert.assertEquals(expectedMessageOut, actualMessageOut);

        Boolean actualIsIncommingOut = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumOut);
        Boolean expectedIsIncommingOut = false;
        Assert.assertEquals(expectedIsIncommingOut, actualIsIncommingOut);

        String actualDateOut = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumOut);
        //String expectedDateOut = "YESTERDAY";
        Date yesterday = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
        String expectedDateOut = getCommonDateAsStringFromDate(yesterday);
        Assert.assertEquals(expectedDateOut, actualDateOut);

        String actualSenderOut = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumOut);
        String expectedSenderOut = null;
        Assert.assertEquals(expectedSenderOut, actualSenderOut);

        String actualTimeOut = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumOut);
        String expectedTimeOut = "18:01";
        Assert.assertEquals(expectedTimeOut, actualTimeOut);

        /////////////////////////////////////////
        int messageNumIn = 4;

        Boolean actualIsImageIn = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        Boolean expectedIsImageIn = false;
        Assert.assertEquals(expectedIsImageIn, actualIsImageIn);

        String actualMessageIn = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumIn, expectedIsImageIn);
        String expectedMessageIn = "In 1";
        Assert.assertEquals(expectedMessageIn, actualMessageIn);

        Boolean actualIsIncommingIn = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumIn);
        Boolean expectedIsIncommingIn = true;
        Assert.assertEquals(expectedIsIncommingIn, actualIsIncommingIn);

        String actualDateIn = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumIn);
        String expectedDateIn = null;
        Assert.assertEquals(expectedDateIn, actualDateIn);

        String actualSenderIn = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumIn);
        String expectedSenderIn = "Andrey Petrov";
        Assert.assertEquals(expectedSenderIn, actualSenderIn);

        String actualTimeIn = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumIn);
        String expectedTimeIn = "18:59";
        Assert.assertEquals(expectedTimeIn, actualTimeIn);
    }

    @Test
    public void privateChat_12Messages() throws Exception {
        String testFile = TestTools.testFileToString(this, "whatsApp/Private_12Messages.json");

        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(testFile);

        JsonArray messagesJsonArray = messengerProcessorWhatsApp.getMessagesJsonArray(entireScreen);
        Integer actualMessagesCount = messengerProcessorWhatsApp.getMessagesCount(messagesJsonArray);
        Integer expectedMessagesCount = 12;
        Assert.assertEquals(expectedMessagesCount, actualMessagesCount);

        String actualTitle = messengerProcessorWhatsApp.getTitle(entireScreen);
        String expectedTitle = "Andrey Petrov";
        Assert.assertEquals(expectedTitle, actualTitle);


        List<String> actualParticipants = messengerProcessorWhatsApp.getParticipants(entireScreen);
        List<String> expectedParticipants = Arrays.asList();
        Assert.assertEquals(expectedParticipants, actualParticipants);

        Boolean actualIsGroup = messengerProcessorWhatsApp.getIsGroup(entireScreen);
        Boolean expectedIsGroup = false;
        Assert.assertEquals(expectedIsGroup, actualIsGroup);

        /////////////////////////////////////////
        int messageNumOut = 10;

        boolean actualIsImageOut = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        boolean expectedIsImageOut = false;
        Assert.assertEquals(expectedIsImageOut, actualIsImageOut);

        String actualMessageOut = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumOut, expectedIsImageOut);
        String expectedMessageOut = "Seems Airplane mode on";
        Assert.assertEquals(expectedMessageOut, actualMessageOut);

        Boolean actualIsIncommingOut = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumOut);
        Boolean expectedIsIncommingOut = false;
        Assert.assertEquals(expectedIsIncommingOut, actualIsIncommingOut);

        String actualDateOut = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumOut);
        String expectedDateOut = null;
        Assert.assertEquals(expectedDateOut, actualDateOut);

        String actualSenderOut = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumOut);
        String expectedSenderOut = null;
        Assert.assertEquals(expectedSenderOut, actualSenderOut);

        String actualTimeOut = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumOut);
        String expectedTimeOut = "14:27";
        Assert.assertEquals(expectedTimeOut, actualTimeOut);

        /////////////////////////////////////////
        int messageNumIn = 11;

        boolean actualIsImageIn = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        boolean expectedIsImageIn = false;
        Assert.assertEquals(expectedIsImageIn, actualIsImageIn);

        String actualMessageIn = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumIn, expectedIsImageIn);
        String expectedMessageIn = "Hi from Andrey";
        Assert.assertEquals(expectedMessageIn, actualMessageIn);

        Boolean actualIsIncommingIn = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumIn);
        Boolean expectedIsIncommingIn = true;
        Assert.assertEquals(expectedIsIncommingIn, actualIsIncommingIn);

        String actualDateIn = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumIn);
        String expectedDateIn = "MAY 19, 2017";
        Assert.assertEquals(expectedDateIn, actualDateIn);

        String actualSenderIn = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumIn);
        String expectedSenderIn = null;
        Assert.assertEquals(expectedSenderIn, actualSenderIn);

        String actualTimeIn = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumIn);
        String expectedTimeIn = "17:18";
        Assert.assertEquals(expectedTimeIn, actualTimeIn);
    }

    @Test
    public void pixelPrivateChatMessages() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "whatsApp/Pixel_Private.json");
        String expectedSerrialized = TestTools.testFileToString(this, "whatsApp/Pixel_Private_expected.json");

        Conversation actual = messengerProcessorWhatsApp.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void nexusGroupMessagesImagesDates() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "whatsApp/Nexus_GroupWithImagesAndDates.json");
        String expectedSerrialized = TestTools.testFileToString(this, "whatsApp/Nexus_GroupWithImagesAndDates_expected.json");

        Conversation actual = messengerProcessorWhatsApp.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Pixel_privateChat_8Messages() throws Exception {
        String testFile = TestTools.testFileToString(this, "whatsApp/Pixel_Private.json");

        Object entireScreen = Configuration.defaultConfiguration().jsonProvider().parse(testFile);

        JsonArray messagesJsonArray = messengerProcessorWhatsApp.getMessagesJsonArray(entireScreen);
        Integer actualMessagesCount = messengerProcessorWhatsApp.getMessagesCount(messagesJsonArray);
        Integer expectedMessagesCount = 12;

        Assert.assertEquals(expectedMessagesCount, actualMessagesCount);

        String actualTitle = messengerProcessorWhatsApp.getTitle(entireScreen);
        String expectedTitle = "Andrey Petrov";
        Assert.assertEquals(expectedTitle, actualTitle);


        // TODO: test fails. Need recearch
//        List<String> actualParticipants = messengerProcessorWhatsApp.getParticipants(entireScreen, listStringType);
//        List<String>  expectedParticipants = Arrays.asList();
//        Assert.assertEquals(expectedParticipants, actualParticipants);

        Boolean actualIsGroup = messengerProcessorWhatsApp.getIsGroup(entireScreen);
        Boolean expectedIsGroup = false;
        Assert.assertEquals(expectedIsGroup, actualIsGroup);

        /////////////////////////////////////////
        int messageNumOut = 4;

        Boolean actualIsImageOut = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        Boolean expectedIsImageOut = false;
        Assert.assertEquals(expectedIsImageOut, actualIsImageOut);

        String actualMessageOut = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumOut, expectedIsImageOut);
        String expectedMessageOut = "Test 123";
        Assert.assertEquals(expectedMessageOut, actualMessageOut);

        Boolean actualIsIncommingOut = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumOut);
        Boolean expectedIsIncommingOut = false;
        Assert.assertEquals(expectedIsIncommingOut, actualIsIncommingOut);

        String actualDateOut = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumOut);
        String expectedDateOut = null;
        Assert.assertEquals(expectedDateOut, actualDateOut);

        String actualSenderOut = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumOut);
        String expectedSenderOut = null;
        Assert.assertEquals(expectedSenderOut, actualSenderOut);

        String actualTimeOut = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumOut);
        String expectedTimeOut = "16:59";
        Assert.assertEquals(expectedTimeOut, actualTimeOut);

        /////////////////////////////////////////
        int messageNumIn = 10;

        Boolean actualIsImageIn = messengerProcessorWhatsApp.getIsImage(messagesJsonArray, messageNumOut);
        Boolean expectedIsImageIn = false;
        Assert.assertEquals(expectedIsImageIn, actualIsImageIn);

        String actualMessageIn = messengerProcessorWhatsApp.getText(messagesJsonArray, messageNumIn, expectedIsImageIn);
        String expectedMessageIn = "I see..";
        Assert.assertEquals(expectedMessageIn, actualMessageIn);

        Boolean actualIsIncommingIn = messengerProcessorWhatsApp.getIsIncomming(messagesJsonArray, messageNumIn);
        Boolean expectedIsIncommingIn = true;
        Assert.assertEquals(expectedIsIncommingIn, actualIsIncommingIn);

        String actualDateIn = messengerProcessorWhatsApp.getVisibleDate(messagesJsonArray, messageNumIn);
        String expectedDateIn = "MAY 26, 2017";
        Assert.assertEquals(expectedDateIn, actualDateIn);

        String actualSenderIn = messengerProcessorWhatsApp.getSender(messagesJsonArray, messageNumIn);
        String expectedSenderIn = null;
        Assert.assertEquals(expectedSenderIn, actualSenderIn);

        String actualTimeIn = messengerProcessorWhatsApp.getTime(messagesJsonArray, messageNumIn);
        String expectedTimeIn = "10:55";
        Assert.assertEquals(expectedTimeIn, actualTimeIn);
    }

    @Test
    public void dateResolverTest() throws Exception {
        String messagesWithDateSomewhereSerrialized = TestTools.testFileToString(this, "whatsApp/DateMergerInitial.json");
        List<Message> actual = gson.fromJson(messagesWithDateSomewhereSerrialized, listMessageType);

        String expectedSerrialized = TestTools.testFileToString(this, "whatsApp/DateMergerExpected.json");
        List<Message> expected = gson.fromJson(expectedSerrialized, listMessageType);

        boolean isResolved = messengerProcessorWhatsApp.resolveFullDate(actual);
        Assert.assertTrue(isResolved);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }
}