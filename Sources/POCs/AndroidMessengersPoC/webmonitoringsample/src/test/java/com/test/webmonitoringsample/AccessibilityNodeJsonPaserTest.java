package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.TypeRef;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AccessibilityNodeJsonPaserTest {

    private Gson gson;

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
    }


    @Test
    @Ignore
    /**
     * See {@link https://github.com/json-path/JsonPath}
     */
    public void testFromJsonPathSite() throws Exception {
        String json = TestTools.testFileToString(this, "jsonPathTest.json");

        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);

        final TypeRef<List<Book>> LIST_BOOKS = new TypeRef<List<Book>>() {
        };

        String path = "$.store.book[?(@.category=='fiction')]";
        //String path = "$.store.book[?(@.category=='fiction' && @.child.b=='two')]";
        JsonArray jsonArray = JsonPath.read(document, path);
        List<Book> books = gson.fromJson(jsonArray, LIST_BOOKS.getType());

        Integer booksCount = JsonPath.read(jsonArray, "$.length()");
        JsonObject jsonObject = JsonPath.read(jsonArray, "$.[0]");
        Book book = gson.fromJson(jsonObject, Book.class);

        assertEquals(Integer.valueOf(3), booksCount);
    }

    static class Book {
        public String category;

        @SerializedName("title333")
        public String title222;

        @SerializedName("author")
        public String author222;

        public String price;
    }
}