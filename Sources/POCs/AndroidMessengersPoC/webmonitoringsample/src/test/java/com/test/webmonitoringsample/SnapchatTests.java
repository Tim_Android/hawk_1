package com.test.webmonitoringsample;

import com.google.gson.Gson;
import com.jayway.jsonpath.TypeRef;
import com.test.webmonitoringsample.merger.IMerger;
import com.test.webmonitoringsample.merger.impl.Merger4;
import com.test.webmonitoringsample.messengers.BaseMessengerProcessor;
import com.test.webmonitoringsample.messengers.MessengerProcessorSnapchat;
import com.test.webmonitoringsample.messengers.model.Conversation;
import com.test.webmonitoringsample.messengers.model.Message;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import static com.test.webmonitoringsample.TestTools.configureGson;
import static com.test.webmonitoringsample.TestTools.getGson;

public class SnapchatTests {
    private Gson gson;
    private BaseMessengerProcessor messengerProcessor;
    private IMerger merger = new Merger4();
    private static Type listMessageType = new TypeRef<List<Message>>() {
    }.getType();

    @Before
    public void setup() {
        configureGson();
        gson = getGson();
        messengerProcessor = new MessengerProcessorSnapchat();
    }

    @Test
    public void privateChatMessagesAndDate() throws Exception {
        String accessibilityNodeAsString = TestTools.testFileToString(this, "snapchat/private_text.json");
        String expectedSerrialized = TestTools.testFileToString(this, "snapchat/private_text_expected.json");

        Conversation actual = messengerProcessor.getPartialConversationFromRootJsonNode(accessibilityNodeAsString);
        Conversation expected = gson.fromJson(expectedSerrialized, Conversation.class);

        String expectedAsString = gson.toJson(expected);
        String actualAsString = gson.toJson(actual);
        Assert.assertEquals(expectedAsString, actualAsString);
        Assert.assertEquals(expected, actual);
    }

    /**
     * <p> See javadoc explanation {@link com.test.webmonitoringsample.merger.impl.Merger2 here}
     */
    @Test
    @Ignore
    public void duplicationsDuringMergingTest() throws Exception {
        String globalConversationJson = TestTools.testFileToString(this, "snapchat/merger2_global_full_intersection.json");
        String partialConversationJson = TestTools.testFileToString(this, "snapchat/merger2_partial_full_intersection.json");

        List<Message> globalMessages = gson.fromJson(globalConversationJson, listMessageType);
        List<Message> partialMessages = gson.fromJson(partialConversationJson, listMessageType);

        boolean isIntersectedFully = Collections.indexOfSubList(globalMessages, partialMessages) != -1;
        Assert.assertFalse(isIntersectedFully);

        boolean isMerged = merger.merge(globalMessages, partialMessages);
        Assert.assertFalse(isMerged);
    }
}
