﻿using System;

namespace Domain.Entities
{
    public struct UserInfo
    {
        public int Id { get; set; }
        public Guid AspUserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
