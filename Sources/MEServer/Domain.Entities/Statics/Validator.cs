﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Domain.Entities.Statics
{
    public static class Validator
    {
        private static readonly string _dateFormatInvariantCulture = "MM/dd/yyyy";

        public static bool ValidateDateFormat(string date)
        {
            DateTime dateTemp;

            if (DateTime.TryParseExact(date, _dateFormatInvariantCulture, null, DateTimeStyles.None, out dateTemp))
            {
                return true;
            }
            return false;
        }
    }
}