﻿using Domain.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class BrowserHistorySearchCondition: SearchCondition
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public int? VisitsCount { get; set; }

        public BrowserHistorySearchCondition(int agentId)
            : base(agentId)
        {
            VisitsCount = null;
        }

        public BrowserHistorySearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
            : base(agentId, dateFrom, dateTo)
        {
            VisitsCount = null;
        }

        public BrowserHistorySearchCondition(int agentId, DateTime dateFrom, DateTime dateTo,
            string url, string title, int? visitsCount)
            : base(agentId, dateFrom, dateTo)
        {
            Url = url;
            Title = title;
            VisitsCount = visitsCount;
        }

        public override string GetStoredProcedureName()
        {
            return "GET_BROWSER_HISTORY";
        }

        public override void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            base.FillSQLCommandParameters(parameters);

            if (Url != string.Empty)
            {
                parameters.AddWithValue("@URL_FLTR", Url);
            }
            if (Title != string.Empty)
            {
                parameters.AddWithValue("@TITLE_FLTR", Title);
            }
            if (VisitsCount.HasValue)
            {
                parameters.AddWithValue("@VISIT_COUNT_FLTR", VisitsCount.Value);
            }
        }
    }
}
