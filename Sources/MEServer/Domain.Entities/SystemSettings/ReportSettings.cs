﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.SystemSettings
{
    public class ReportSettings
    {
        public float LeftDocumentMargin
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LeftDocumentMargin"]); }
        }
        public float RightDocumentMargin
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.RightDocumentMargin"]); }
        }
        public float TopDocumentMargin
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.TopDocumentMargin"]); }
        }
        public float BottomDocumentMargin
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.BottomDocumentMargin"]); }
        }
        public float TitleFontSize
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.TitleFontSize"]); }
        }
        public float SubTitleFontSize
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.SubTitleFontSize"]); }
        }
        public float BodyFontSize
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.BodyFontSize"]); }
        }
        public float TableWidthPercentage
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.TableWidthPercentage"]); }
        }
        public float TableSpacingBefore
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.TableSpacingBefore"]); }
        }
        public float TableSpacingAfter
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.TableSpacingAfter"]); }
        }
        public float FilterSpacingBefore
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.FilterSpacingBefore"]); }
        }
        public float LineSeparatorWidth
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LineSeparatorWidth"]); }
        }
        public float LineSeparatorPercentage
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LineSeparatorPercentage"]); }
        }
        public float LineSeparatorOffset
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LineSeparatorOffset"]); }
        }
        public float HeaderSpacingAfter
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.HeaderSpacingAfter"]); }
        }
        public float LogoScalePercent
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LogoScalePercent"]); }
        }
        public float LogoSpacingAfter
        {
            get { return float.Parse(ConfigurationManager.AppSettings["PDFReport.LogoSpacingAfter"]); }
        }
        public int TableHeaderRows
        {
            get { return int.Parse(ConfigurationManager.AppSettings["PDFReport.TableHeaderRows"]); }
        }

    }
}
