﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class BannedEmailFilter
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "banDateFrom")]
        public DateTime? BanDateFrom { get; set; }

        [JsonProperty(PropertyName = "banDateTo")]
        public DateTime? BanDateTo{ get; set; }
    }
}
