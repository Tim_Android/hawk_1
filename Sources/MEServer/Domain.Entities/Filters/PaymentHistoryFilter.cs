﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class PaymentHistoryFilter
    {
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public string email { get; set; }
        public string transactionId { get; set; }

        public PaymentHistoryFilter()
        {
            Reset();
        }

        public void Reset()
        {
            dateFrom = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0).AddMonths(-1);
            dateTo = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 23, 59, 59);
            email = null;
            transactionId = null;
        }
    }
}
