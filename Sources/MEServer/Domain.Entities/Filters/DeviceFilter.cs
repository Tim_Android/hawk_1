﻿using System;
using Domain.Entities.Enums;
using Newtonsoft.Json;

namespace Domain.Entities.Filters
{
    public class DeviceFilter
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public int? UserId { get; set; }

        [JsonProperty(PropertyName = "agentWasActivated")]
        public bool? AgentWasActivated { get; set; }

        [JsonProperty(PropertyName = "lastUpdatedFrom")]
        public DateTime? LastUpdatedFrom { get; set; }

        [JsonProperty(PropertyName = "lastUpdatedTo")]
        public DateTime? LastUpdatedTo { get; set; }

        [JsonProperty(PropertyName = "device")]
        public string Device { get; set; }

        [JsonProperty(PropertyName = "deviceId")]
        public string DeviceId { get; set; }

        [JsonProperty(PropertyName = "licId")]
        public int? LicId { get; set; }

        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [JsonProperty(PropertyName = "os")]
        public string Os { get; set; }

        [JsonProperty(PropertyName = "sdk")]
        public string Sdk { get; set; }

        [JsonProperty(PropertyName = "firmwareId")]
        public string FirmwareId { get; set; }

        [JsonProperty(PropertyName = "agent")]
        public string Agent { get; set; }

        [JsonProperty(PropertyName = "svn")]
        public string Svn { get; set; }

        [JsonProperty(PropertyName = "timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "licActDateFrom")]
        public DateTime? LicActDateFrom { get; set; }

        [JsonProperty(PropertyName = "licActDateTo")]
        public DateTime? LicActDateTo { get; set; }

        [JsonProperty(PropertyName = "operSysType")]
        public OperatingSystemType? OperSysType { get; set; }

        [JsonProperty(PropertyName = "creationDateFrom")]
        public DateTime? CreationDateFrom { get; set; }

        [JsonProperty(PropertyName = "creationDateTo")]
        public DateTime? CreationDateTo { get; set; }

        [JsonProperty(PropertyName = "showUnassignedAgents")]
        public bool? ShowUnassignedAgents { get; set; }
    }
}
