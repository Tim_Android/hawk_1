﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class UserManagementFilter
    {
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "aspUserId")]
        public Guid? AspUserId { get; set; }

        [JsonProperty(PropertyName = "registeredFrom")]
        public DateTime? RegisteredFrom { get; set; }

        [JsonProperty(PropertyName = "registeredTo")]
        public DateTime? RegisteredTo { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "isEmailVerified")]
        public bool? IsEmailVerified { get; set; }

        [JsonProperty(PropertyName = "hasPurchasedLicenses")]
        public bool? HasPurchasedLicenses { get; set; }

        [JsonProperty(PropertyName = "hasNotUpdatedSubscr")]
        public bool? HasNotUpdatedSubscr { get; set; }

        [JsonProperty(PropertyName = "isBanned")]
        public bool? IsBanned { get; set; }
    }
}
