﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class LicenseFilter
    {
        [JsonProperty(PropertyName = "Id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public int? UserId { get; set; }

        [JsonProperty(PropertyName = "subscriptionEndFrom")]
        public DateTime? SubscriptionEndFrom { get; set; }

        [JsonProperty(PropertyName = "subscriptionEndTo")]
        public DateTime? SubscriptionEndTo { get; set; }

        [JsonProperty(PropertyName = "deviceCnt")]
        public short? DeviceCnt { get; set; }

        [JsonProperty(PropertyName = "paySysType")]
        public int? PaySysType { get; set; }

        [JsonProperty(PropertyName = "subscriptionId")]
        public string SubscriptionId { get; set; }

        [JsonProperty(PropertyName = "creationDateFrom")]
        public DateTime? CreationDateFrom { get; set; }

        [JsonProperty(PropertyName = "creationDateTo")]
        public DateTime? CreationDateTo { get; set; }

    }
}
