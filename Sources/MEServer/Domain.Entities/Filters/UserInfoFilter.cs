﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class UserInfoFilter
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? AspUserId { get; set; }
        public string Email { get; set; }
    }
}
