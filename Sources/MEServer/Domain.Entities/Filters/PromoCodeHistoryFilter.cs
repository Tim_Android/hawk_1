﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Filters
{
    public class PromoCodeHistoryFilter
    {
        [JsonProperty(PropertyName = "promoCode")]
        public string PromoCode { get; set; }

        [JsonProperty(PropertyName = "generatedFrom")]
        public DateTime? GeneratedFrom { get; set; }

        [JsonProperty(PropertyName = "generatedTo")]
        public DateTime? GeneratedTo { get; set; }

        [JsonProperty(PropertyName = "subscriptionPeriodId")]
        public int? SubscriptionPeriodId { get; set; }

        [JsonProperty(PropertyName = "state")]
        public int? State { get; set; }

        [JsonProperty(PropertyName = "isExpired")]
        public bool? IsExpired { get; set; }
    }
}
