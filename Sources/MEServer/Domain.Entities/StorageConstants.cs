﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class StorageConstants
    {
        public const string USER_SESSION_STORAGE = "account";
        public const string DEVICE_LIST_STORAGE = "deviceList";
        public const string CURRENT_DEVICE_STORAGE = "curentDevice";
        public const string CURRENT_TAB_STORAGE = "currentTab";
        public const string RECORDS_PER_PAGE_STORAGE = "recordsPerPage";
        public const string USER_TZ_OFFSET_STORAGE = "userTimeZoneOffset";
        public const string SELECTED_START_ROW = "selectedStartRow";
        public const string SELECTED_PAGE_INDEX = "selectedPageIndex";
        public const string SELECTED_VIEW = "selectedView";
        public const string DATE_VARIANT = "dateVariant";
        public const string SELECTED_CONTACT = "selectedContact";
        public const string DEVICE_NAME = "deviceName";
        public const string GET_SMS_TOTAL_COUNT = "GET_SMS_TOTAL_COUNT";
    }
}
