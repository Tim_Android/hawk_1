﻿using System;
using System.Data.SqlClient;
using Domain.Entities.Abstract;
using Domain.Entities.Enums;
namespace Domain.Entities
{
    public class GPSSearchCondition : SearchCondition
    {
        public GPSProviderMaskType MaskProvider { get; set; }

        public GPSSearchCondition(int agentId)
            : base(agentId)
        {
            MaskProvider = GPSProviderMaskType.NoMask;
        }

        public GPSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
            : base(agentId, dateFrom, dateTo)
        {
            MaskProvider = GPSProviderMaskType.NoMask;
        }

        public GPSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo, GPSProviderMaskType maskProvider)
            : base(agentId, dateFrom, dateTo)
        {
            MaskProvider = maskProvider;
        }

        public override string GetStoredProcedureName()
        {
            return "GET_COORDINATES";
        }

        public override void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            base.FillSQLCommandParameters(parameters);

            if (GPSProviderMaskType.NoMask != MaskProvider)
            {
                parameters.AddWithValue("@PROVIDER_MASK", (int)MaskProvider);
            }
        }
    }
}
