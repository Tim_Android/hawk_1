﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Entities.Enums;

namespace Domain.Entities
{
    public class NotifyMessage
    {
        public NotifyMessageType MessType { get; set; }

        public string MessText { get; set; }

        public NotifyMessage()
        {
        }

        public NotifyMessage(NotifyMessageType type, string message)
        {
            MessType = type;
            MessText = message;
        }
    }
}
