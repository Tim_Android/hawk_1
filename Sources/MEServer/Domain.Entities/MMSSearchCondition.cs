﻿using System;
namespace Domain.Entities
{
    public class MMSSearchCondition : SMSSearchCondition
    {
        public MMSSearchCondition(int agentId)
            : base(agentId)
        {
        }

        public MMSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
            : base(agentId, dateFrom, dateTo)
        {
        }

        public MMSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo,
            int messageType, string address, string content)
            : base(agentId, dateFrom, dateTo, messageType, address, content)
        {
        }

        public override string GetStoredProcedureName()
        {
            return "GET_MMS";
        }
    }
}
