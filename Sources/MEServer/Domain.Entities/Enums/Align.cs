﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Enums
{
    public enum Align: int
    {
        Left = 0,
        Center = 1,
        Right = 2
    }
}
