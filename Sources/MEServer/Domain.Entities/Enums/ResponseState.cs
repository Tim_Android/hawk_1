﻿namespace Domain.Entities.Enums
{
    public enum ResponseState
    {
        NotAuthenticated = 0,
        Authenticated = 1,
        InternalError = 2
    }
}
