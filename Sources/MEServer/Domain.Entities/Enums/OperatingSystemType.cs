﻿namespace Domain.Entities.Enums
{
    public enum OperatingSystemType : short
    {
        Ios = 1,
        Android = 2
    }
}
