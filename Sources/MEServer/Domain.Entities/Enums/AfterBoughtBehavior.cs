﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Enums
{
    public enum AfterBoughtBehavior
    {
        ActivateSubscriptionOn = 1,
        DoNotActivateSubscriptionNow = 2
    }
}
