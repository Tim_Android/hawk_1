﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Enums
{
    public enum NotifyMessageType : short
    {
        Success = 0,
        Warning = 1,
        Error = 2,
        Info = 3
    }
}
