﻿namespace Domain.Entities.Enums
{
    public enum ResponseStatus
    {
        UnknownError = 0,
        Success = 1,
        NotValidData = 2,
        AccountExistsActivated = 3,
        AccountExistsNotVerifiedYet = 4,
        UserNotExists = 5,
        UserBanned = 6
    }
}
