﻿namespace Domain.Entities.Enums
{
    public enum Font:short
    {
        ArialNormal = 1,
        ArialBold = 2,
        ArialItalic = 3
    }
}
