﻿namespace Domain.Entities.Enums
{
    public enum PhoneCallType
    {
        Unknown = 0,
        Incoming = 1,
        Outgoing = 2,
        Missed = 3
    }
}
