﻿namespace Domain.Entities.Enums
{
    public enum GPSProviderMaskType
    {
        NoMask = 0,
        GPSOnly = 1,
        WiFiOnly = 2,
        BothGPSAndWiFi = 3
    }
}
