﻿using Domain.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class BookmarkHistorySearchCondition : SearchCondition
    {
        public string Url { get; set; }
        public string Title { get; set; }

        public BookmarkHistorySearchCondition(int agentId)
            : base(agentId)
        {
            Url = string.Empty;
            Title = string.Empty;
        }

        public BookmarkHistorySearchCondition(int agentId, string url, string title)
            : base(agentId)
        {
            Url = url;
            Title = title;
        }

        public override string GetStoredProcedureName()
        {
            return "GET_BOOKMARK_HISTORY";
        }

        public override void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            //we only need to initialize agent ID parameter as bookmarks have no date and time filter options
            parameters.AddWithValue("@AGENT_ID", AgentId);

            if (Url != string.Empty)
            {
                parameters.AddWithValue("@URL_FLTR", Url);
            }
            if (Title != string.Empty)
            {
                parameters.AddWithValue("@TITLE_FLTR", Title);
            }
        }
    }
}
