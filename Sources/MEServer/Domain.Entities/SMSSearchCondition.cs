﻿using System;
using System.Data.SqlClient;
using Domain.Entities.Abstract;

namespace Domain.Entities
{
    public class SMSSearchCondition : SearchCondition
    {
        public int MessageType { get; set; }
        public string Address { get; set; }
        public string Content { get; set; }

        public SMSSearchCondition(int agentId)
            : base(agentId)
        {
            MessageType = 0;
            Address = string.Empty;
            Content = string.Empty;
        }

        public SMSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
            : base(agentId, dateFrom, dateTo)
        {
            Address = string.Empty;
            Content = string.Empty;
        }

        public SMSSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo,
            int messageType, string address, string content)
            : base(agentId, dateFrom, dateTo)
        {
            MessageType = messageType;
            Address = address;
            Content = content;
        }

        public override string GetStoredProcedureName()
        {
            return "GET_SMS";
        }

        public override void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            base.FillSQLCommandParameters(parameters);

            if (0 != MessageType)
            {
                parameters.AddWithValue("@TYPE_FLTR", MessageType);
            }
            if(null != Address)
            {
                parameters.AddWithValue("@ADDR_FLTR", Address);
            }

            if (string.Empty != Content)
            {
                parameters.AddWithValue("@CONTENT_FLTR", Content);
            }
        }
    }
}
