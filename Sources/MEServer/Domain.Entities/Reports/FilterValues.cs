﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Reports
{
    public class FilterValues
    {
        public string UrlFilter { get; set; }
        public string TitleFilter { get; set; }
        public DateTime DateTimeFromFilter { get; set; }
        public DateTime DateTimeToFilter { get; set; }
        public int? VisitsCountFilter { get; set; }
        public int TypeFilter { get; set; }
        public string NumberFilter { get; set; }
        public string ContentFilter { get; set; }
        public string ContactFilter { get; set; }
        public string BasePath { get; set; }
        public string Device { get; set; }
        public string GPSSelectedEntry { get; set; }
    }
}
