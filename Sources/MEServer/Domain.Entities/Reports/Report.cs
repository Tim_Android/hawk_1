﻿using Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Reports
{
    public abstract class Report
    {
        protected ReportSchemaEntity _schema;
        protected DataTable _data;
        protected string _fileName;
        protected string _fileExtension;
        protected Report(ReportSchemaEntity schema, DataTable data)
        {
            _schema = schema;
            _data = data;
            _fileName = schema.Name;
        }
    }
}
