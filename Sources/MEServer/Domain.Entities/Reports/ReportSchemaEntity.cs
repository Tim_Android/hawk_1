﻿using Domain.Entities.Enums;
using System.Collections.Generic;

namespace Domain.Entities.Reports
{
    public class ReportSchemaEntity
    {
        
        public string Header { get; private set; }
        public string Name { get; private set; }
        public string LogoPath { get; private set; }
        public Dictionary <Font,string> FontPaths { get; private set; }
        public Dictionary<string, string> Filters { get; private set; }
        public List<ColumnEntity> TableColumns { get; private set; }

        public ReportSchemaEntity AddHeader(string header)
        {
            Header = header;
            return this;
        }

        public ReportSchemaEntity AddColumns(List<ColumnEntity> columns)
        {
            TableColumns = columns;
            return this;
        }

        public ReportSchemaEntity AddName(string name)
        {
            Name = name;
            return this;
        }

        public ReportSchemaEntity AddLogoPath(string logoPath)
        {
            LogoPath = logoPath;
            return this;
        }
        public ReportSchemaEntity AddFontPaths(Dictionary<Font,string> fontPathes)
        {
            FontPaths = fontPathes;
            return this;
        }

        public ReportSchemaEntity AddFilters(Dictionary<string, string> filters)
        {
            Filters = filters;
            return this;
        }
    }
}
