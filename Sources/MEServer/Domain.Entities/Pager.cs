﻿using Newtonsoft.Json;
namespace Domain.Entities
{
    public class Pager
    {
        [JsonProperty(PropertyName="recordsPerPage")]
        public int? RecordsPerPage { get; set; }

        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "totalRecords")]
        public int TotalRecords { get; set; }

        [JsonProperty(PropertyName = "totalPages")]
        public int TotalPages { get; set; }

        public Pager()
        {
            Reset();
        }

        public void Reset()
        {
            RecordsPerPage = null;
            Page = 1;
            TotalPages = 0;
            TotalRecords = 0;
        }
    }
}
