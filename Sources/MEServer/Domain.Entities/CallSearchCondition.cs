﻿using System;
using System.Data.SqlClient;
using Domain.Entities.Abstract;
using Domain.Entities.Enums;
namespace Domain.Entities
{
    public class CallSearchCondition : SearchCondition
    {
        public PhoneCallType CallType { get; set; }
        public string NumberMask { get; set; }

        public CallSearchCondition(int agentId)
            : base(agentId)
        {
            CallType = PhoneCallType.Unknown;
            NumberMask = string.Empty;
        }

        public CallSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
            : base(agentId, dateFrom, dateTo)
        {
            CallType = PhoneCallType.Unknown;
            NumberMask = string.Empty;
        }

        public CallSearchCondition(int agentId, DateTime dateFrom, DateTime dateTo, PhoneCallType callType, string numberMask)
            : base(agentId, dateFrom, dateTo)
        {
            CallType = callType;
            NumberMask = numberMask;
        }

        public override string GetStoredProcedureName()
        {
            return "GET_CALLS";
        }

        public override void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            base.FillSQLCommandParameters(parameters);

            if (!string.IsNullOrEmpty(NumberMask))
            {
                parameters.AddWithValue("@NUMBER_FLTR", NumberMask);
            }
            if (PhoneCallType.Unknown != CallType)
            {
                parameters.AddWithValue("@CTYPE_FLTR", (int)CallType);
            }
        }
    }
}
