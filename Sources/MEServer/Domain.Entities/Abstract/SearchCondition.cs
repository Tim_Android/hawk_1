﻿using System;
using System.Data.SqlClient;
namespace Domain.Entities.Abstract
{
    public abstract class SearchCondition
    {
        public int AgentId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int UserTimeZoneOffset { get; set; }
        private const string MIN_DATE_SQL = "1/1/1753 12:00:00 AM";

        public DateTime MinDateSql
        {
            get
            {
                return DateTime.Parse(MIN_DATE_SQL);
            }
        }

        public SearchCondition(int agentId)
        {
            AgentId = agentId;
            DateFrom = DefaultDateFrom;
            DateTo = DefaultDateTo;
            UserTimeZoneOffset = 0;
        }

        public SearchCondition(int agentId, DateTime dateFrom, DateTime dateTo)
        {
            AgentId = agentId;

            DateFrom = dateFrom < MinDateSql ? MinDateSql : dateFrom;
            DateTo = dateTo;
            UserTimeZoneOffset = 0;
        }

        public abstract string GetStoredProcedureName();

        public virtual void FillSQLCommandParameters(SqlParameterCollection parameters)
        {
            parameters.AddWithValue("@AGENT_ID", AgentId);
            parameters.AddWithValue("@TZ_USER_OFFSET", UserTimeZoneOffset);
            parameters.AddWithValue("@BEG_DATE_FLTR", DateFrom);
            parameters.AddWithValue("@END_DATE_FLTR", DateTo);
        }

        public static DateTime DefaultDateFrom
        {
            get
            {
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddMonths(-1);
            }
        }
        public static DateTime DefaultDateTo
        {
            get { return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59).AddDays(1); }
        }
    }
}
