﻿using System;
using Domain.Entities.Enums;
using Newtonsoft.Json;

namespace Domain.Entities
{
    public class Device
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public int UserId { get; set; }

        [JsonProperty(PropertyName = "agentName")]
        public string AgentName { get; set; }

        [JsonProperty(PropertyName = "agentWasActivated")]
        public bool AgentWasActivated { get; set; }

        [JsonProperty(PropertyName = "lastUpdate")]
        public DateTime? LastUpdate { get; set; }

        [JsonProperty(PropertyName = "deviceId")]
        public string DeviceId { get; set; }

        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [JsonProperty(PropertyName = "os")]
        public string Os { get; set; }

        [JsonProperty(PropertyName = "sdk")]
        public string Sdk { get; set; }

        [JsonProperty(PropertyName = "firmwareId")]
        public string FirmwareId { get; set; }

        [JsonProperty(PropertyName = "svnVersion")]
        public string SvnVersion { get; set; }

        [JsonProperty(PropertyName = "timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty(PropertyName = "deviceName")]
        public string DeviceName { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "licId")]
        public int? LicId { get; set; }

        [JsonProperty(PropertyName = "licActDate")]
        public DateTime? LicActDate { get; set; }

        [JsonProperty(PropertyName = "creationDate")]
        public DateTime CreationDate { get; set; }

        [JsonProperty(PropertyName = "operSysType")]
        public OperatingSystemType OperSysType { get; set; }
    }
}
