﻿namespace Domain.Entities
{
    public class MessageType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
