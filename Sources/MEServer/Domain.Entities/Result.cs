﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Result
    {
        public NotifyMessage Message { get; set; }
        public int? Code { get; set; }

        public Result(NotifyMessage message, int? code = null)
        {
            Message = message;
            Code = code;
        }
    }
}
