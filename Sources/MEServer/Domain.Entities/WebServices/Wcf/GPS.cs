﻿using System;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class GPS
    {
        [DataMember(IsRequired = true)]
        public double latitude;
        [DataMember(IsRequired = true)]
        public double longitude;
        [DataMember(IsRequired = true)]
        public double altitude;
        [DataMember(IsRequired = true)]
        public float accuracy;
        [DataMember(IsRequired = true)]
        public float bearing;
        [DataMember(IsRequired = true)]
        public string provider;
        [DataMember(IsRequired = true)]
        public float speed;
        [DataMember(IsRequired = true)]
        public DateTime time;

        public static bool operator ==(GPS pointOne, GPS pointTwo)
        {
            return (((object)pointOne != null) && ((object)pointTwo != null)
                    && (pointOne.latitude == pointTwo.latitude)
                    && (pointOne.longitude == pointTwo.longitude)
                    && (pointOne.altitude == pointTwo.altitude)
                    && (pointOne.accuracy == pointTwo.accuracy)
                    && (pointOne.bearing == pointTwo.bearing)
                    && (pointOne.provider == pointTwo.provider)
                    && (pointOne.speed == pointTwo.speed)
                    && (pointOne.time == pointTwo.time));
        }

        public static bool operator !=(GPS pointOne, GPS pointTwo)
        {
            return !(pointOne == pointTwo);
        }

        public override bool Equals(object other)
        {
            bool result = false;
            if (other is GPS)
            {
                result = Equals((GPS)other);
            }
            return result;
        }

        public bool Equals(GPS that)
        {
            return (this == that);
        }

        public override int GetHashCode()
        {
            return (int)Math.Sqrt(Math.Pow(latitude, 2) + Math.Pow(longitude, 2));
        }

        public static GPS ChoicePointBestAccuracy(GPS pointOne, GPS pointTwo)
        {
            GPS retPoint = null;

            if (pointOne.accuracy < pointTwo.accuracy)
            {
                retPoint = pointOne;
            }
            else
            {
                retPoint = pointTwo;
            }

            return retPoint;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Latitude: \"{0}\"", latitude);
            sb.AppendFormat(" Longitude: \"{0}\"", longitude);
            sb.AppendFormat(" Altitude: \"{0}\"", altitude);
            sb.AppendFormat(" Accuracy: \"{0}\"", accuracy);
            sb.AppendFormat(" Bearing: \"{0}\"", bearing);
            sb.AppendFormat(" Provider: \"{0}\"", provider);
            sb.AppendFormat(" Speed: \"{0}\"", speed);
            sb.AppendFormat(" Date/Time: \"{0}\"", time);

            return sb.ToString();
        }
    }
}
