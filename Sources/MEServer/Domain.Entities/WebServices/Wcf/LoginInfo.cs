﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class LoginInfo
    {
        [DataMember(Name = "login", IsRequired = true)]
        public string Login { get; set; }
    }
}
