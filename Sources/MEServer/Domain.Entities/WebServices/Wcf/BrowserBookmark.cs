﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class BrowserBookmark
    {
        [DataMember(IsRequired = true)]
        public string url { get; set; }

        [DataMember(IsRequired = true)]
        public string title { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("url: \"{0}\"", url);
            sb.AppendFormat(" title: \"{0}\"", title);

            return sb.ToString();
        }
    }
}