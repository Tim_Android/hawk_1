﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class RegisterInfo
    {
        [DataMember( Name = "firstName", IsRequired = true)]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName", IsRequired = true)]
        public string LastName { get; set; }

        [DataMember(Name = "email", IsRequired = true)]
        public string Email { get; set; }

        [DataMember(Name = "password", IsRequired = true)]
        public string Password { get; set; }

        [DataMember(Name = "token", IsRequired = true)]
        public string Token { get; set; }
    }
}
