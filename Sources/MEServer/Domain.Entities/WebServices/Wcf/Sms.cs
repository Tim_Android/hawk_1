﻿using System;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{

    [DataContract]
    public class Sms
    {
        [DataMember(IsRequired = true)]
        public string address;

        [DataMember]
        public string contactName;

        [DataMember(IsRequired = true)]
        public string body;

        [DataMember(IsRequired = true)]
        public string type;

        [DataMember(IsRequired = true)]
        public DateTime date;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Address: \"{0}\"", address);
            sb.AppendFormat(" Body: \"{0}\"", body);
            sb.AppendFormat(" Type: \"{0}\"", type);
            sb.AppendFormat(" Date: \"{0}\"", date);
            sb.AppendFormat(" Contact Name: \"{0}\"", contactName);
            return sb.ToString();
        }
    }
}
