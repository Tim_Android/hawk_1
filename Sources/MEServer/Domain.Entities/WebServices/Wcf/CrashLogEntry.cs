﻿using System;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class CrashLogEntry
    {
        [DataMember(IsRequired = true)]
        public DateTime date;

        [DataMember(IsRequired = true)]
        public string data;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(" date: \"{0}\"", date);
            sb.AppendFormat(" data: \"{0}\"", data);

            return sb.ToString();
        }
    }
}
