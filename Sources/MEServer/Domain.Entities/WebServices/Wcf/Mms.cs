﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class Mms : Sms
    {
        [DataMember(IsRequired = true)]
        public String sub; // MMS subject
        [DataMember]
        public List<MMSAttachment> attachments;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base.ToString());
            sb.AppendFormat(" Subject: \"{0}\"", sub);
            sb.Append(" Attachments: [");
            foreach (MMSAttachment attachment in attachments)
            {
                sb.AppendFormat("-\"{0}\"", attachment.ToString());
            }
            sb.Append("-]");

            return sb.ToString();
        }
    }
}
