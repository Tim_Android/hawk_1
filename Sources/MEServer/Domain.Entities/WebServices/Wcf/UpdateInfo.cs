﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class UpdateInfo
    {
        [DataMember]
        public DeviceInfo deviceInfo;

        [DataMember]
        public List<Call> callLog;

        [DataMember]
        public List<Sms> smsLog;

        [DataMember]
        public List<Mms> mmsLog;

        [DataMember]
        public List<GPS> gpsLog;

        [DataMember]
        public List<CrashLogEntry> crashLog;

        [DataMember]
        public List<BrowserBookmark> browserBookmarksLog;

        [DataMember]
        public List<BrowserHistoryItem> browserHistoryLog;
    }
}
