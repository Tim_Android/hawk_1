﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class BrowserHistoryItem
    {
        [DataMember(IsRequired = true)]
        public DateTime date { get; set; }

        [DataMember(IsRequired = true)]
        public string url { get; set; }

        [DataMember(IsRequired = true)]
        public string title { get; set; }

        [DataMember(IsRequired = true)]
        public int visits { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("date: \"{0}\"", date.ToString());
            sb.AppendFormat(" url: \"{0}\"", url);
            sb.AppendFormat(" title: \"{0}\"", title);
            sb.AppendFormat(" visits: \"{0}\"", visits);

            return sb.ToString();
        }
    }
}
