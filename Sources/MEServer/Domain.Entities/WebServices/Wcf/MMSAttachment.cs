﻿using System;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class MMSAttachment
    {
        [DataMember(IsRequired = true)]
        public int _id; // MMS attachment ID (from agent database)
        [DataMember(IsRequired = true)]
        public String type;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Type: \"{0}\"", type);
            return sb.ToString();
        }
    }
}
