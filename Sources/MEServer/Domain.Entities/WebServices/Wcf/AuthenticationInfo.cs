﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class AuthenticationInfo
    {
        [DataMember(Name = "password", IsRequired = true)]
        public string Password { get; set; }

        [DataMember(Name = "login", IsRequired = true)]
        public string Login { get; set; }

        [DataMember(Name = "agentId", IsRequired = true)]
        public string AgentId { get; set; }
    }
}
