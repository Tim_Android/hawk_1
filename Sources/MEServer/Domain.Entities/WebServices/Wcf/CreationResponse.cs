﻿using System.Runtime.Serialization;
using Domain.Entities.Enums;
using Domain.Entities.WebServices.Wcf.Base;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class CreationResponse : Response
    {
        [DataMember(Name = "agentId")]
        public int? AgentId { get; set; }
    }
}
