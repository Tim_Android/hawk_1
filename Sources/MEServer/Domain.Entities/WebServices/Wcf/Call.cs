﻿using System;
using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class Call
    {
        [DataMember(IsRequired = true)]
        public string fromNumber;

        [DataMember(IsRequired = true)]
        public string fromNumberName;

        [DataMember(IsRequired = true)]
        public string fromNumberType;

        [DataMember(IsRequired = true)]
        public string toNumber;

        [DataMember(IsRequired = true)]
        public string toNumberName;

        [DataMember(IsRequired = true)]
        public string toNumberType;

        [DataMember(IsRequired = true)]
        public int duration;

        [DataMember(IsRequired = true)]
        public DateTime date;

        // todo: It's better to use enum here, but there is a bug with enums in json serializer
        [DataMember(IsRequired = true)]
        public string type;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("fromNumber: \"{0}\"", fromNumber);
            sb.AppendFormat(" fromNumberName: \"{0}\"", fromNumberName);
            sb.AppendFormat(" fromNumberType: \"{0}\"", fromNumberType);
            sb.AppendFormat(" toNumber: \"{0}\"", toNumber);
            sb.AppendFormat(" toNumberName: \"{0}\"", toNumberName);
            sb.AppendFormat(" toNumberType: \"{0}\"", toNumberType);
            sb.AppendFormat(" duration: \"{0}\"", duration);
            sb.AppendFormat(" date: \"{0}\"", date);
            sb.AppendFormat(" type: \"{0}\"", type);

            return sb.ToString();
        }
    }
}
