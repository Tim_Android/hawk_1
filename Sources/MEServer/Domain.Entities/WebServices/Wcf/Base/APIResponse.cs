﻿using Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.WebServices.Wcf.Base
{
    public class APIResponse
    {
        [DataMember(Name = "status", IsRequired = true)]
        public ResponseStatus Status { get; set; }
    }
}
