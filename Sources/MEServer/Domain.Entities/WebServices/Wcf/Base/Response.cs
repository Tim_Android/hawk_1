﻿using System.Runtime.Serialization;
using Domain.Entities.Enums;
namespace Domain.Entities.WebServices.Wcf.Base
{
    [DataContract]
    public class Response
    {
        [DataMember(Name = "state", IsRequired = true)]
        public ResponseState State { get; set; }
    }
}
