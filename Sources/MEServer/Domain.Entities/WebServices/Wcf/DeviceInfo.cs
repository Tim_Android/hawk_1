﻿using System.Runtime.Serialization;
using System.Text;
namespace Domain.Entities.WebServices.Wcf
{
    [DataContract]
    public class DeviceInfo
    {
        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_DEVICE_ID;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_PHONE_NUMBER;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_BRAND;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_MODEL_NAME;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_OS_VERSION;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_SDK_VERSION;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_FIRMWARE_ID;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_SVN_VERSION;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_AGENT_NAME;

        [DataMember(IsRequired = true)]
        public string DEVICE_INFO_TIME_ZONE_NAME;

        [DataMember(IsRequired = true)]
        public int DEVICE_INFO_TIME_ZONE_RAW_OFFSET;

        [DataMember(Name = "systemType", IsRequired = true)]
        public string SystemType { get; set; }

        [DataMember(Name = "password", IsRequired = true)]
        public string Password { get; set; }

        [DataMember(Name = "login", IsRequired = true)]
        public string Login { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Device ID: \"{0}\"", DEVICE_INFO_DEVICE_ID);
            sb.AppendFormat(" Phone number: \"{0}\"", DEVICE_INFO_PHONE_NUMBER);
            sb.AppendFormat(" Brand: \"{0}\"", DEVICE_INFO_BRAND);
            sb.AppendFormat(" Model: \"{0}\"", DEVICE_INFO_MODEL_NAME);
            sb.AppendFormat(" OS: \"{0}\"", DEVICE_INFO_OS_VERSION);
            sb.AppendFormat(" SDK: \"{0}\"", DEVICE_INFO_SDK_VERSION);
            sb.AppendFormat(" Firmware ID: \"{0}\"", DEVICE_INFO_FIRMWARE_ID);
            sb.AppendFormat(" SVN: \"{0}\"", DEVICE_INFO_SVN_VERSION);
            sb.AppendFormat(" Agent: \"{0}\"", DEVICE_INFO_AGENT_NAME);
            sb.AppendFormat(" Time Zone: \"{0}\"", DEVICE_INFO_TIME_ZONE_NAME);
            sb.AppendFormat(" TZ Offset: \"{0}\"", DEVICE_INFO_TIME_ZONE_RAW_OFFSET);

            return sb.ToString();
        }
    }
}
