﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ColumnEntity
    {
        public string Name { get; set; }
        public string Field { get; set; }
        public List<string> Arguments { get; set; }
    }
}
