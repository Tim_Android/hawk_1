﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace Settings
{
    public static class TrialSettings
    {
        public static int DeviceCount
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["trialDeviceCount"]);
            }
        }

        public static int ActivationCount
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["trialActivationCount"]);
            }
        }

        public static int TrialPromocodeDuration
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["trialPromocodeDuration"]);
            }
        }

        public static int TrialValidityPeriod
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["trialValidityPeriod"]);
            }
        }
    }
}
