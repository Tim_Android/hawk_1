﻿/*==============================================================*/
/* Table: CRASH_LOGS                                            */
/*==============================================================*/
create table CRASH_LOGS (
   ID                   int                  identity(1, 1),
   AGENT_ID             int                  not null,
   CRASH_DATA           CRASH_DATA_TYPE      not null,
   CRASH_DATETIME       datetime             not null,
   constraint PK_AgentErrorLog primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"


GO
/*==============================================================*/
/* Index: IND_CRASH_LOG_DATE                                    */
/*==============================================================*/
create index IND_CRASH_LOG_DATE on CRASH_LOGS (
AGENT_ID DESC,
CRASH_DATETIME DESC
)


GO
alter table CRASH_LOGS
   add constraint FK_AgentErrorLog_Agent foreign key (AGENT_ID)
      references DEVICES (ID)