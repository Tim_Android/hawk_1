﻿/*==============================================================*/
/* Table: PAYMENTS                                              */
/*==============================================================*/
create table PAYMENTS (
   ID                   int                  identity(1, 1),
   LIC_ID               int                  not null,
   PRICE_LIST_ID        int                  not null,
   PAY_DATE             datetime             null,
   TRANS_ID             SUBSCR_ID_TYPE       not null,
   AMOUNT               money                not null,
   DEV_CNT              smallint             not null default 0,
   SUBSCR_END           datetime             null,
   PREV_SUBSCR_END      datetime             null,
   PAY_COMMENT          COMMENT_TYPE         null,
   constraint PK_PAYMENTS primary key (ID)
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Summa', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PAYMENTS', @level2type = N'COLUMN', @level2name = N'AMOUNT';


GO
alter table PAYMENTS
   add constraint FK_PAYMENTS_FK_PRICE__PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade
GO
alter table PAYMENTS
   add constraint FK_PAYMENTS_FK_DEVS_P_LICENSES foreign key (LIC_ID)
      references LICENSES (ID)