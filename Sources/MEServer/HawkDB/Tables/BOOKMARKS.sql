﻿CREATE TABLE [dbo].[BOOKMARKS] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [AGENT_ID] INT             NOT NULL,
    [URL]      [dbo].[BROWSER_URL_TYPE]   NOT NULL,
    [TITLE]    [dbo].[BROWSER_TITLE_TYPE] NOT NULL
);
go
CREATE INDEX IND_BOOKMARKS_AGENT_ID ON BOOKMARKS                 ([AGENT_ID]) INCLUDE ([ID], [URL], [TITLE])
