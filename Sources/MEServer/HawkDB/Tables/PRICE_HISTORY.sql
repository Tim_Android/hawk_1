﻿/*==============================================================*/
/* Table: PRICE_HISTORY                                         */
/*==============================================================*/
create table PRICE_HISTORY (
   PRICE_LIST_ID        int                  null,
   DATE_FROM            datetime             not null,
   DATE_TO              datetime             null,
   PRICE                money                not null,
   PRICE_OTHER          money                null,
   [RETAIL_PRICE]         money                not null default 0.00
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Active Price: (DATE_TO is NULL)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PRICE_HISTORY', @level2type = N'COLUMN', @level2name = N'DATE_TO';


GO
alter table PRICE_HISTORY
   add constraint FK_PRICE_HI_FK_PRICE__PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade