﻿/*==============================================================*/
/* Table: CALLS                                                 */
/*==============================================================*/
create table CALLS (
   ID                   int                  identity(1, 1),
   AGENT_ID             int                  not null,
   DURATION             int                  not null,
   DATE                 datetime             not null,
   CTYPE                CALL_TYPE            not null,
   FROM_PHONE_ID        int                  not null,
   TO_PHONE_ID          int                  not null,
   TO_NUM_TYPE_ID       int                  not null,
   FROM_NUM_TYPE_ID     int                  not null,
   constraint PK_Call primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"


GO
/*==============================================================*/
/* Index: IND_CALL_AGENT_ID                                     */
/*==============================================================*/
create index IND_CALL_AGENT_ID on CALLS (
AGENT_ID DESC,
TO_PHONE_ID DESC,
TO_NUM_TYPE_ID ASC,
FROM_PHONE_ID DESC,
FROM_NUM_TYPE_ID ASC
)


GO
/*==============================================================*/
/* Index: IND_CALL_TO_PHONE                                     */
/*==============================================================*/
create index IND_CALL_TO_PHONE on CALLS (
TO_PHONE_ID DESC
)


GO
/*==============================================================*/
/* Index: IND_CALL_FROM_PHONE                                   */
/*==============================================================*/
create index IND_CALL_FROM_PHONE on CALLS (
FROM_PHONE_ID DESC
)


GO
alter table CALLS
   add constraint FK_CALLS_TO_PHONE foreign key (TO_PHONE_ID)
      references PHONES (ID)
GO
alter table CALLS
   add constraint FK_CALLS_FROM_PHONE foreign key (FROM_PHONE_ID)
      references PHONES (ID)
GO
alter table CALLS
   add constraint FK_CALLS_TO_NUM_TYPE foreign key (TO_NUM_TYPE_ID)
      references NUMBERS_TYPES (ID)
GO
alter table CALLS
   add constraint FK_CALLS_FROM_NUM_TYPE foreign key (FROM_NUM_TYPE_ID)
      references NUMBERS_TYPES (ID)
GO
alter table CALLS
   add constraint FK_Calls_Agents foreign key (AGENT_ID)
      references DEVICES (ID)