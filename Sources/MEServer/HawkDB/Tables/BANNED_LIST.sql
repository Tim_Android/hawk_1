﻿CREATE TABLE [dbo].[BANNED_LIST]
(
	[ID] INT NOT NULL PRIMARY KEY identity, 
    [EMAIL] [dbo].[EMAIL_TYPE] NOT NULL unique, 
    [BAN_DATE] DATETIME NOT NULL, 
    [NOTE] NVARCHAR(MAX) NULL, 
    
)
GO

/*==============================================================*/
/* Index: IND_BAN_EMAIL                                     */
/*==============================================================*/
create index IND_BAN_EMAIL on BANNED_LIST (
EMAIL ASC
)

GO