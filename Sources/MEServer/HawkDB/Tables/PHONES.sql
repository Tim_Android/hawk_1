﻿/*==============================================================*/
/* Table: PHONES                                                */
/*==============================================================*/
create table PHONES (
   ID                   int                  identity(1, 1),
   DEVICE_ID             int                  null,
   NUMBER               PHONE_TYPE           null,
   PHONE_NAME           PNAME_TYPE           null,
   constraint PK_PHONES primary key (ID)
)


GO
/*==============================================================*/
/* Index: IND_PHONE_NUM_NAME                                    */
/*==============================================================*/
create index IND_PHONE_NUM_NAME on PHONES (
NUMBER ASC
)
GO
/*==============================================================*/
/* Index: IND_PHONE_DEVICE_ID                                    */
/*==============================================================*/
CREATE NONCLUSTERED INDEX IND_PHONE_DEVICE_ID on PHONES (
DEVICE_ID 
)
GO
/*==============================================================*/
/* Index: IND_PHONE_PHONE_NAME                                    */
/*==============================================================*/
CREATE NONCLUSTERED INDEX IND_PHONE_PHONE_NAME on PHONES (
PHONE_NAME 
)

