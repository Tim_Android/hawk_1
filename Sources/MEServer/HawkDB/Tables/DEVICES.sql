﻿/*==============================================================*/
/* Table: DEVICES                                               */
/*==============================================================*/
create table DEVICES (
   ID                   int                  identity(1, 1),
   USER_ID              int                  not null,
   AGENT_NAME           AGENT_NAME_TYPE      not null,
   IS_ACTIVED           bit                  not null,
   LAST_UPDATED         datetime             null,
   PHONE_NUMBER         PHONE_TYPE           null,
   BRAND                BRAND_TYPE           null,
   MODEL                MODEL_TYPE           null,
   OS                   OS_TYPE              null,
   SDK                  SDK_TYPE             null,
   FIRMWARE_ID          FIRMWARE_ID_TYPE     null,
   SVN_VERSION          SVN_TYPE             null,
   TIME_ZONE            TIME_ZONE_TYPE       null,
   TZ_OFFSET            int                  null default 0,
   DEVICE_NAME          DEV_NAME_TYPE        null,
   NOTE                 NOTE_TYPE            null,
   LIC_ID               int                  null,
   LIC_ACT_DATE         datetime             null,
   CREATION_DATE        datetime             not null,
   OPER_SYS_TYPE        int                  not null DEFAULT 2,
   DEVICE_ID            DEVICE_ID_TYPE       null,
   constraint PK_Agent primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"


GO
/*==============================================================*/
/* Index: IND_DEVICE_ID                                         */
/*==============================================================*/
create index IND_DEVICE_ID on DEVICES (
DEVICE_ID DESC
)


GO
alter table DEVICES
   add constraint FK_DEVICES_FK_DEVS_L_LICENSES foreign key (LIC_ID)
      references LICENSES (ID)
         on update cascade
GO
alter table DEVICES
   add constraint FK_Agents_Users foreign key (USER_ID)
      references USERS_INFO (ID)
go
CREATE INDEX IND_DEVICES_LIC_ID ON DEVICES                 (LIC_ID)
go
CREATE INDEX IND_DEVICES_USER_ID_IS_ACTIVATED ON DEVICES                 ([USER_ID], [IS_ACTIVED]) INCLUDE ([ID], [AGENT_NAME], [LAST_UPDATED], [DEVICE_NAME], [LIC_ID], [DEVICE_ID])
go
CREATE INDEX IND_DEVICES_USER_ID ON DEVICES                 ([USER_ID])
go
CREATE INDEX IND_DEVICES_IS_ACTIVATED ON DEVICES                 ([IS_ACTIVED]) INCLUDE ([ID], [USER_ID], [AGENT_NAME], [LAST_UPDATED], [DEVICE_NAME], [LIC_ID], [DEVICE_ID])