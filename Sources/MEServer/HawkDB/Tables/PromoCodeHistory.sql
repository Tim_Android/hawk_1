﻿/*==============================================================*/
/* Table: PromoCodeHistory                                      */
/*==============================================================*/
create table PromoCodeHistory (
   ID                   int                  identity,
   LICENSE_ID           int                  not null,
   PROMO_CODE_ID        int                  not null,
   USER_ID              int                  not null,
   constraint PK_PROMOCODEHISTORY primary key (ID)
)


GO
alter table PromoCodeHistory
   add constraint FK_PROMOCOD_FK_PROCOD_USERS_IN foreign key (USER_ID)
      references USERS_INFO (ID)
GO
alter table PromoCodeHistory
   add constraint FK_PROMOCOD_FK_PROCOD_PROMOCOD foreign key (PROMO_CODE_ID)
      references PromoCode (ID)
GO
alter table PromoCodeHistory
   add constraint FK_PROMOCOD_FK_PRCODE_LICENSES foreign key (LICENSE_ID)
      references LICENSES (ID)