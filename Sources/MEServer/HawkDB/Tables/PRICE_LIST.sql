﻿/*==============================================================*/
/* Table: PRICE_LIST                                            */
/*==============================================================*/
create table PRICE_LIST (
   ID                   int                  identity(1, 1),
   LABEL                NAME_TYPE            null,
   DURATION             smallint             null,
   PAY_TYPE             smallint             not null,
   constraint PK_PRICE_LIST primary key (ID)
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'in days', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PRICE_LIST', @level2type = N'COLUMN', @level2name = N'DURATION';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '1 - Fee (Signup Fee),
2 - Recurring (regular payment)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PRICE_LIST', @level2type = N'COLUMN', @level2name = N'PAY_TYPE';

