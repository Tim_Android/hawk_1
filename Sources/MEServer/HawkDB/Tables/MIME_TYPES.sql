﻿/*==============================================================*/
/* Table: MIME_TYPES                                            */
/*==============================================================*/
create table MIME_TYPES (
   ID_MIME              int                  identity(1, 1),
   EXT                  nchar(10)            null,
   MIME_NAME            nchar(100)            not null,
   constraint PK_MIME_TYPES primary key (ID_MIME)
)


GO
/*==============================================================*/
/* Index: IND_MIME_TYPES_NAME                                   */
/*==============================================================*/
create index IND_MIME_TYPES_NAME on MIME_TYPES (
MIME_NAME ASC
)

