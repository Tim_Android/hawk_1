﻿/*==============================================================*/
/* Table: EMAIL                                                 */
/*==============================================================*/
create table EMAIL (
   ID                   int                  identity(1, 1),
   EMAIL_NAME           nvarchar(64)         not null,
   EMAIL_TYPE           int                  not null,
   EMAIL_TEXT_HTML      EMAIL_CONTENT_TYPE   not null,
   ALLOWED_VARS         EMAIL_CONTENT_TYPE   null,
   constraint PK_EMAIL primary key (ID)
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '1 - registration validation,
2 - registration validation confirm,
3 - registration,
4 - password reset confirmation,
5 - new password', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EMAIL', @level2type = N'COLUMN', @level2name = N'EMAIL_TYPE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'This field contains list of variables that are available for this template', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EMAIL', @level2type = N'COLUMN', @level2name = N'ALLOWED_VARS';

