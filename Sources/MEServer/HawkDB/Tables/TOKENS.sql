﻿/*==============================================================*/
/* Table: TOKENS                                                */
/*==============================================================*/
create table TOKENS (
   EMAIL                EMAIL_TYPE           not null,
   TOKEN                VARCHAR(100)         null,
   EXPIRATION_DATE      DATETIME             not null default DATEADD(d, 1, GETUTCDATE())
)


GO
/*==============================================================*/
/* Index: IND_TOKENS_TOKEN                                      */
/*==============================================================*/
create index IND_TOKENS_TOKEN on TOKENS (
TOKEN ASC
)

