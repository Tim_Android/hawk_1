﻿/*==============================================================*/
/* Table: LICENSES                                              */
/*==============================================================*/
create table LICENSES (
   ID                   int                  identity(1, 1),
   USER_ID              int                  not null,
   PRICE_LIST_ID        int                  null,
   SUBSCR_END           datetime             null,
   DEV_CNT              smallint             not null default 0,
   PAY_SYS_TYPE         smallint             null,
   SUBSCR_ID            SUBSCR_ID_TYPE       null,
   CREATION_DATE        datetime             null,
   constraint PK_LICENSES primary key (ID)
)


GO
/*==============================================================*/
/* Index: IND_LIC_SUBSCR_ID                                     */
/*==============================================================*/
create index IND_LIC_SUBSCR_ID on LICENSES (
SUBSCR_ID ASC
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NULL if it wasn''t licensed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LICENSES', @level2type = N'COLUMN', @level2name = N'SUBSCR_END';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Payment Systems:
    1 - AutorizeNet,
    2 - PayPal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LICENSES', @level2type = N'COLUMN', @level2name = N'PAY_SYS_TYPE';


GO
alter table LICENSES
   add constraint FK_LICENSES_REFERENCE_USERS_IN foreign key (USER_ID)
      references USERS_INFO (ID)
GO
alter table LICENSES
   add constraint FK_LICENSES_FK_LICS_P_PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade