﻿/*==============================================================*/
/* Table: USERS_INFO                                            */
/*==============================================================*/
create table USERS_INFO (
   ID                   int                  identity(1, 1),
   FIRST_NAME           NAME_TYPE            null,
   LAST_NAME            NAME_TYPE            null,
   ASP_NET_USER_ID      uniqueidentifier     null,
   EMAIL                EMAIL_TYPE           not null,
   constraint PK_User primary key (ID) on "PRIMARY",
   constraint [FK_USERS_INFO_aspnet_Users] foreign key ([ASP_NET_USER_ID]) references [dbo].[aspnet_Users] ([UserId]) on delete set NULL on update cascade
)
on "PRIMARY"

