﻿/*==============================================================*/
/* Table: MESSAGES                                              */
/*==============================================================*/
create table MESSAGES (
   ID_MSG               int                  identity(1,1),
   AGENT_ID_MSG         int                  not null,
   TYPE_ID              int                  not null,
   MSG_ADDR             ADDRESS_TYPE         null,
   PHONE_ID             int                  null,
   MSG_DT               datetime             null,
   MSG_BODY             MSG_BODY_TYPE        null,
   MSG_SUBJ             MSG_SUBJ_TYPE        null,
   constraint PK_MESSAGES primary key (ID_MSG)
)
GO
/*==============================================================*/
/* Index: IND_SMS_ID_AGENT                                      */
/*==============================================================*/
create index IND_SMS_ID_AGENT on MESSAGES (
AGENT_ID_MSG DESC
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'SMS and MMS messages', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MESSAGES';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Identity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MESSAGES', @level2type = N'COLUMN', @level2name = N'ID_MSG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Message subject (for MMS)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MESSAGES', @level2type = N'COLUMN', @level2name = N'MSG_SUBJ';


GO
alter table MESSAGES
   add constraint FK_MESSAGES_FK_SMS_TY_MSG_TYPE foreign key (TYPE_ID)
      references MSG_TYPES (ID_TYPE)
         on update cascade
GO
alter table MESSAGES
   add constraint FK_MESSAGES_FK_SMS_AG_DEVICES foreign key (AGENT_ID_MSG)
      references DEVICES (ID)
         on update cascade
GO

CREATE NONCLUSTERED INDEX [IND_MESSAGES_AGENT_ID_DATE_SUBJ_TYPE] ON [dbo].[MESSAGES]
(
	[AGENT_ID_MSG] DESC,
	[MSG_DT] DESC,
	[MSG_SUBJ] ASC,
	[TYPE_ID] ASC
)
INCLUDE ( 	[ID_MSG],
	[MSG_ADDR],
	[MSG_BODY]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IND_MESSAGES_AGENT_ID_SUBJ_TYPE_DATE] ON [dbo].[MESSAGES]
(
	[AGENT_ID_MSG] ASC,
	[MSG_SUBJ] ASC,
	[TYPE_ID] ASC,
	[MSG_DT] ASC
)
INCLUDE ([ID_MSG],
	[MSG_ADDR],
	[MSG_BODY]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

GO
