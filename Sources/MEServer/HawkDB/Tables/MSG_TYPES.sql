﻿/*==============================================================*/
/* Table: MSG_TYPES                                             */
/*==============================================================*/
create table MSG_TYPES (
   ID_TYPE              int                  identity(1,1),
   TYPE_NAME            nchar(10)            null,
   constraint PK_MSG_TYPES primary key (ID_TYPE)
)


GO
/*==============================================================*/
/* Index: IND_TYPE_NAME                                         */
/*==============================================================*/
create index IND_TYPE_NAME on MSG_TYPES (
TYPE_NAME ASC
)

