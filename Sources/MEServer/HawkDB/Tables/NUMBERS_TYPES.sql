﻿/*==============================================================*/
/* Table: NUMBERS_TYPES                                         */
/*==============================================================*/
create table NUMBERS_TYPES (
   ID                   int                  identity(1, 1),
   NUMBER_TYPE          NUM_NAME_TYPE        null,
   constraint PK_NUMBERS_TYPES primary key (ID)
)


GO
/*==============================================================*/
/* Index: IND_NUM_TYPE                                          */
/*==============================================================*/
create index IND_NUM_TYPE on NUMBERS_TYPES (
NUMBER_TYPE ASC
)

