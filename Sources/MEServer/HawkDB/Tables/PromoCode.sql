﻿/*==============================================================*/
/* Table: PromoCode                                             */
/*==============================================================*/
create table PromoCode (
   ID                   int                  identity,
   PROMO_CODE           varchar(32)          not null,
   PRICELIST_ID         int                  not null,
   VALID_FROM           datetime             not null,
   VALID_TO             datetime             not null,
   DATE_GENERATED       datetime             not null,
   DEVICE_COUNT         int                  not null,
   ACTIVATION_COUNT     int                  not null,
   ACTIVE               bit                  not null,
   constraint PK_PROMOCODE primary key (ID)
)


GO
alter table PromoCode
   add constraint FK_PROMOCOD_FK_PROCOD_PRICE_LI foreign key (PRICELIST_ID)
      references PRICE_LIST (ID)