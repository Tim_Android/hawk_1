﻿CREATE TABLE [dbo].[BROWSER_HISTORY] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [AGENT_ID] INT             NOT NULL,
    [URL]      [dbo].[BROWSER_URL_TYPE]   NOT NULL,
    [TITLE]    [dbo].[BROWSER_TITLE_TYPE] NOT NULL,
    [VISITS]   INT             NOT NULL,
    [DATE]     DATETIME        NOT NULL
);
go
CREATE INDEX IND_BROWSER_HISTORY_AGENT_ID ON BROWSER_HISTORY                 ([AGENT_ID]) INCLUDE ([ID], [URL], [TITLE], [VISITS], [DATE])

