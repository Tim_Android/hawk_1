﻿/*==============================================================*/
/* Table: MMS_ATTACH                                            */
/*==============================================================*/
create table MMS_ATTACH (
   ID_ATTACH            int                  identity(1,1),
   MSG_ID               int                  null,
   ID_MIME              int                  null,
   ATTACH_DATA          ATTACH_DATA_TYPE     null,
   AGENT_ATTACH_ID      INT                  null,
   constraint PK_MMS_ATTACH primary key (ID_ATTACH)
)


GO



GO
/*==============================================================*/
/* Index: IND_MMS                                               */
/*==============================================================*/
create index IND_MMS on MMS_ATTACH (
MSG_ID DESC
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'The MMS messages attachments', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MMS_ATTACH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Identity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MMS_ATTACH', @level2type = N'COLUMN', @level2name = N'MSG_ID';


GO
alter table MMS_ATTACH
   add constraint FK_MMS_ATTA_REFERENCE_MIME_TYP foreign key (ID_MIME)
      references MIME_TYPES (ID_MIME)
GO
alter table MMS_ATTACH
   add constraint FK_MMS_ATTA_FK_MMS_AT_MESSAGES foreign key (MSG_ID)
      references MESSAGES (ID_MSG)
         on update cascade on delete cascade
GO
/*==============================================================*/
/* Index: IND_MMS_ATTA_ID_MIME                                    */
/*==============================================================*/
create NONCLUSTERED index IND_MMS_ATTA_ID_MIME on MMS_ATTACH (
ID_MIME 
)
