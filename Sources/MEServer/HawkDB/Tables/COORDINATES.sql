﻿/*==============================================================*/
/* Table: COORDINATES                                           */
/*==============================================================*/
create table COORDINATES (
   ID                   int                  identity(1,1),
   AGENT_ID             int                  not null,
   LATITUDE             double precision     null,
   LONGITUDE            double precision     null,
   ALTITUDE             double precision     null,
   ACCURACY             float                null,
   BEARING              float                null,
   PROVIDER             PROVIDER_TYPE        null default 0,
   SPEED                float                null,
   ARRIVAL_TIME         datetime             null,
   constraint PK_COORDINATES primary key (ID)
)


GO
/*==============================================================*/
/* Index: IND_GPS_ID_AGENT                                      */
/*==============================================================*/
create index IND_GPS_ID_AGENT on COORDINATES (
AGENT_ID DESC
)


GO
alter table COORDINATES
   add constraint FK_COORDINA_REFERENCE_DEVICES foreign key (AGENT_ID)
      references DEVICES (ID)
         on update cascade