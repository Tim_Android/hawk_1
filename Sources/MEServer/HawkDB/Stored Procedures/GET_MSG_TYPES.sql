﻿create procedure GET_MSG_TYPES 
with encryption
as
begin
    select ID_TYPE as ID, TYPE_NAME as TNAME
    from MSG_TYPES
    order by ID_TYPE
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_MSG_TYPES] TO [HAWKWebSiteUser]
    AS [dbo];

