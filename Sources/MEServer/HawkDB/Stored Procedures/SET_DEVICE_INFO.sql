﻿create procedure SET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE = null,
    @DEVICE_NAME DEV_NAME_TYPE = null,
    @NOTE NOTE_TYPE = null,
    @PHONE_NUMBER PHONE_TYPE = null,
    @BRAND BRAND_TYPE  = null,
    @MODEL MODEL_TYPE = null,
    @OS OS_TYPE = null,
    @SDK SDK_TYPE = null,
	@FIRMWARE_ID FIRMWARE_ID_TYPE = null,
    @SVN_VERSION SVN_TYPE = NULL,
    @TIME_ZONE TIME_ZONE_TYPE = NULL,
    @TZ_OFFSET int = NULL
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        declare @BRAND_OLD_VAL BRAND_TYPE;
        declare @MODEL_OLD_VAL MODEL_TYPE;
        
        select @BRAND_OLD_VAL = D.BRAND, @MODEL_OLD_VAL = D.MODEL
        from DEVICES D
        where D.ID = @AGENT_ID;
              
        update DEVICES
        set
            DEVICES.AGENT_NAME = isnull(@AGENT_NAME, DEVICES.AGENT_NAME),
            DEVICES.DEVICE_NAME = isnull(@DEVICE_NAME, DEVICES.DEVICE_NAME),
            DEVICES.NOTE = isnull(@NOTE, DEVICES.NOTE),
            DEVICES.PHONE_NUMBER = isnull(@PHONE_NUMBER, DEVICES.PHONE_NUMBER),
            DEVICES.BRAND = isnull(@BRAND, DEVICES.BRAND),
            DEVICES.MODEL = isnull(@MODEL, DEVICES.MODEL),
            DEVICES.OS = isnull(@OS, DEVICES.OS),
            DEVICES.SDK = isnull(@SDK, DEVICES.SDK),
            DEVICES.FIRMWARE_ID = isnull(@FIRMWARE_ID, DEVICES.FIRMWARE_ID),
            DEVICES.SVN_VERSION = isnull(@SVN_VERSION, DEVICES.SVN_VERSION),
            DEVICES.TIME_ZONE = isnull(@TIME_ZONE, DEVICES.TIME_ZONE),
            DEVICES.TZ_OFFSET = isnull(@TZ_OFFSET, DEVICES.TZ_OFFSET)
        where DEVICES.ID = @AGENT_ID
        
        if ((@BRAND_OLD_VAL is null) and (@MODEL_OLD_VAL is null))
        begin
            UPDATE DEVICES
            SET 
              DEVICES.DEVICE_NAME = isnull(cast((rtrim(DEVICES.BRAND) + ' ' + rtrim(DEVICES.MODEL)) as nchar(20)), DEVICES.AGENT_NAME)
            where (DEVICES.ID = @AGENT_ID) and (DEVICES.AGENT_NAME = DEVICES.DEVICE_NAME)
        end        
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[SET_DEVICE_INFO] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[SET_DEVICE_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

