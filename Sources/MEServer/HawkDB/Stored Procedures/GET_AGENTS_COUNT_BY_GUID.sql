﻿create procedure GET_AGENTS_COUNT_BY_GUID
    @USER_GUID uniqueidentifier = null,
    @AGENT_ID int = null,
    @AGENTS_COUNT int output
with encryption
as
begin
    set nocount on;

    set @AGENTS_COUNT = 0
    if ((@USER_GUID is not null) and (@AGENT_ID is not null))
    begin
        select @AGENTS_COUNT = isnull(count(D.ID), 0) 
        from USERS_INFO U 
            left join DEVICES D on U.ID = D.USER_ID
        where U.ASP_NET_USER_ID = @USER_GUID 
            and D.ID = @AGENT_ID 
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_AGENTS_COUNT_BY_GUID] TO [HAWKWebSiteUser]
    AS [dbo];

