﻿create procedure GET_MMS_ATTACH @MSG_ID int
with encryption
as
begin
    select A.ID_ATTACH,
           MT.MIME_NAME,
           MT.EXT,
           case when (A.ATTACH_DATA is not NULL) then 'Ok' else 'Error' end as STATUS
    from MMS_ATTACH A
        left join MIME_TYPES MT on MT.ID_MIME=A.ID_MIME    
    where A.MSG_ID = @MSG_ID
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_MMS_ATTACH] TO [HAWKWebSiteUser]
    AS [dbo];

