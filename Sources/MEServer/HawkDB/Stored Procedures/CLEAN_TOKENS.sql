﻿create procedure CLEAN_TOKENS
with encryption
as
begin
    delete from TOKENS
    where EXPIRATION_DATE < GETUTCDATE()
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[CLEAN_TOKENS] TO [HAWKWebSiteUser]
    AS [dbo];

