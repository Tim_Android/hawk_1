﻿create procedure GET_PRICE_LIST_ITEM
    @PL_ID int,
    @DURATION smallint output,
    @PRICE money output,
    @PRICE_OTHER money output
with encryption    
as
begin
    set nocount on;

    select @DURATION=DURATION, @PRICE=PRICE, @PRICE_OTHER=isnull(PRICE_OTHER, 0.0)
    from ACTIVE_PRICE_LIST    
    where (ID = @PL_ID)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PRICE_LIST_ITEM] TO [HAWKWebSiteUser]
    AS [dbo];

