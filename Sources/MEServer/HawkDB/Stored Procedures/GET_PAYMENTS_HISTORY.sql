﻿create procedure GET_PAYMENTS_HISTORY 
    @LIC_ID int = null,
    @USER_ID int = null,
    @DATE_FROM datetime = null,    
    @DATE_TO datetime = null,
    @EMAIL EMAIL_TYPE = null,
    @TRANS_ID SUBSCR_ID_TYPE = null,
    @ID_PAYMENT int = null
with encryption
as
begin
    select P.ID_PAYMENT, P.PAY_DATE, UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME, P.LABEL,
        P.DEV_CNT, P.PRICE, P.PRICE_OTHER, P.AMOUNT, P.TRANS_ID,
        P.START_DATE, P.FINISH_DATE, P.PAY_COMMENT
    from PAYMENTS_HISTORY P
        left join LICENSES L on L.ID = P.LIC_ID
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where ((@LIC_ID is null) or (LIC_ID = @LIC_ID))
        and ((@USER_ID is null) or (LIC_ID in (select distinct ID 
                                               from LICENSES
                                               where USER_ID = @USER_ID)))
        and ((@EMAIL is null) or (UI.EMAIL like '%' + @EMAIL + '%'))
        and ((@TRANS_ID is null) or (P.TRANS_ID like '%' + @TRANS_ID + '%'))
        and ((@DATE_FROM is null) or (@DATE_FROM <= PAY_DATE))
        and ((@DATE_TO is null) or (PAY_DATE <= @DATE_TO))
        and ((@ID_PAYMENT is null) or (P.ID_PAYMENT = @ID_PAYMENT))
    order by PAY_DATE desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PAYMENTS_HISTORY] TO [HAWKWebSiteUser]
    AS [dbo];

