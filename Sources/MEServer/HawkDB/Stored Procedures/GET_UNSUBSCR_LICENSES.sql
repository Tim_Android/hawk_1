﻿create procedure GET_UNSUBSCR_LICENSES
    @PL_ID int
with encryption    
as
begin
    select U.UserName as EMAIL, UI.FIRST_NAME, UI.LAST_NAME, L.ID as LIC_ID, L.SUBSCR_ID, L.PAY_SYS_TYPE
    from ACTIVE_PRICE_LIST APL
        left join LICENSES L on L.PRICE_LIST_ID = APL.ID
        left join USERS_INFO UI on UI.ID = L.USER_ID
        left join aspnet_Users U on U.UserId = UI.ASP_NET_USER_ID
    where (APL.ID = @PL_ID) and (L.SUBSCR_ID is not null)
    order by LIC_ID desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_UNSUBSCR_LICENSES] TO [HAWKWebSiteUser]
    AS [dbo];

