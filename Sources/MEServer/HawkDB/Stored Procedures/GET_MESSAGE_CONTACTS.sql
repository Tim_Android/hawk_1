﻿CREATE PROCEDURE [dbo].[GET_MESSAGE_CONTACTS]
@AGENT_ID int
with encryption
as
begin
    select distinct CASE WHEN PHONES.PHONE_NAME is null 
						 THEN 
							CASE WHEN (select top 1 PHONES.PHONE_NAME
									  from PHONES
									  where PHONES.DEVICE_ID = @AGENT_ID 
										    and SMS.SMS_ADDR = PHONES.NUMBER
											and PHONES.PHONE_NAME is not null) IS NULL
								 THEN SMS.SMS_ADDR
								 ELSE (select top 1 PHONES.PHONE_NAME
									  from PHONES
									  where PHONES.DEVICE_ID = @AGENT_ID 
										    and SMS.SMS_ADDR = PHONES.NUMBER
											and PHONES.PHONE_NAME is not null)
							END
						ELSE PHONES.PHONE_NAME
					END AS MSG_CONTACT
    from SMS
		left join PHONES on PHONES.ID = SMS.PHONE_ID
	where @AGENT_ID = AGENT_ID_SMS
    order by MSG_CONTACT
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_MESSAGE_CONTACTS] TO [HAWKWebSiteUser]
    AS [dbo];
