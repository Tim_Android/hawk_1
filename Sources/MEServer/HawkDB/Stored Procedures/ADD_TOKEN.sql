﻿create procedure ADD_TOKEN
    @EMAIL EMAIL_TYPE,
    @TOKEN varchar(100)
with encryption
as
begin
    insert into TOKENS (EMAIL, TOKEN)
    values (@EMAIL, @TOKEN)        
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_TOKEN] TO [HAWKWebSiteUser]
    AS [dbo];

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_TOKEN] TO [HAWKWebServiceUser]
    AS [dbo];