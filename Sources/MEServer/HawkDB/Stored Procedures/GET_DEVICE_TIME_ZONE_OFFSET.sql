﻿create procedure GET_DEVICE_TIME_ZONE_OFFSET
    @AGENT_ID int = null,
    @TZ_DEV_OFFSET datetime output
with encryption
as
begin
    set @TZ_DEV_OFFSET = null
    if (@AGENT_ID is not null)
    begin 
        select @TZ_DEV_OFFSET = dateadd(ms, isnull(D.TZ_OFFSET, 0),  0)
        from DEVICES D 
        where D.ID = @AGENT_ID;
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_DEVICE_TIME_ZONE_OFFSET] TO [HAWKWebSiteUser]
    AS [dbo];

