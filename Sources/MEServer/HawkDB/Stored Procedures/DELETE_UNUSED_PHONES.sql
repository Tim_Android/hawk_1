﻿create procedure DELETE_UNUSED_PHONES
as
begin
    delete 
    from PHONES 
    where ID not in (select distinct FROM_PHONE_ID
                     from CALLS
                     union
                     select distinct TO_PHONE_ID
                     from CALLS)
end
