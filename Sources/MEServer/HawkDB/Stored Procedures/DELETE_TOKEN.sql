﻿create procedure DELETE_TOKEN
    @TOKEN VARCHAR(100) = null
with encryption    
as
begin
    if (@TOKEN is not null)
    begin
        delete from TOKENS
        where TOKEN = @TOKEN
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_TOKEN] TO [HAWKWebSiteUser]
    AS [dbo];

