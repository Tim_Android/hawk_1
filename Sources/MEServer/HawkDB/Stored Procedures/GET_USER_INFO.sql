﻿create procedure GET_USER_INFO
    @ASP_USER_ID uniqueidentifier = null,
    @SUBSCR_ID SUBSCR_ID_TYPE =  null
with encryption
as
begin
    if ((@ASP_USER_ID is null) and (@SUBSCR_ID is null))
    begin
        return;
    end

    select ID, FIRST_NAME, LAST_NAME, EMAIL
    from USERS_INFO
    where ((@ASP_USER_ID is null) or (ASP_NET_USER_ID = @ASP_USER_ID))
        and ((@SUBSCR_ID is null) or (ID in (select L.USER_ID
                                             from LICENSES L
                                             where L.SUBSCR_ID = @SUBSCR_ID)))
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USER_INFO] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USER_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

