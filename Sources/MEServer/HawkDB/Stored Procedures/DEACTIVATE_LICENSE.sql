﻿create procedure DEACTIVATE_LICENSE
    @LICENSE_ID int
with encryption
as
begin
    set nocount on;

    update DEVICES
    set LIC_ID = null,
        LIC_ACT_DATE = null,
        CREATION_DATE = getdate()
    where LIC_ID = @LICENSE_ID
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DEACTIVATE_LICENSE] TO [HAWKPeriodicEventsUser]
    AS [dbo];

