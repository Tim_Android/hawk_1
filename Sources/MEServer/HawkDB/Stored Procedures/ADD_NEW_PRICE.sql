﻿create procedure ADD_NEW_PRICE
    @PL_ID int,
    @NEW_PRICE money,
    @NEW_PRICE_OTHER money = null,
	@NEW_RETAIL_PRICE money = 0.00,
    @DATE_FROM datetime = null
with encryption    
as
begin
    set nocount on;

    if (@DATE_FROM is null)
    begin
        set @DATE_FROM = getutcdate();
    end

    update PRICE_HISTORY
    set
        DATE_TO = @DATE_FROM
    where (PRICE_LIST_ID = @PL_ID) and (DATE_TO is null)

    insert into PRICE_HISTORY(PRICE_LIST_ID, DATE_FROM, PRICE, PRICE_OTHER, RETAIL_PRICE)
    values (@PL_ID, @DATE_FROM, @NEW_PRICE, @NEW_PRICE_OTHER, @NEW_RETAIL_PRICE)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_NEW_PRICE] TO [HAWKWebSiteUser]
    AS [dbo];

