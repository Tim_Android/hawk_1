﻿create procedure GET_NOT_ACTIVATED_ACCOUNTS
    @START_TIME_HRS int,
    @FINISH_TIME_HRS int = null
with encryption
as
begin
    select UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME
    from USERS_INFO UI
        right join aspnet_Membership M on UI.ASP_NET_USER_ID = M.UserId
    where not exists (select L.ID from LICENSES L where L.USER_ID = UI.ID)
        and (@START_TIME_HRS < datediff(hour, CreateDate, getdate()))
        and ((@FINISH_TIME_HRS is null) or (datediff(hour, CreateDate, getdate()) <= @FINISH_TIME_HRS))
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_NOT_ACTIVATED_ACCOUNTS] TO [HAWKPeriodicEventsUser]
    AS [dbo];

