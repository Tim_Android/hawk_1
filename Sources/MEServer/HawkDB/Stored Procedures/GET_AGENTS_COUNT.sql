﻿create procedure GET_AGENTS_COUNT
    @USER_ID int = null,
    @AGENT_ID int = null,
    @AGENTS_COUNT int output
with encryption
as
begin
    set nocount on;

	set @AGENTS_COUNT = 0
    if ((@USER_ID is not null) and (@AGENT_ID is not null))
    begin
        select @AGENTS_COUNT = count(*) 
        from DEVICES 
        where ID = @AGENT_ID 
            and USER_ID = @USER_ID

        if (@AGENTS_COUNT is null)
        begin
            set @AGENTS_COUNT = 0
        end
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_AGENTS_COUNT] TO [HAWKWebSiteUser]
    AS [dbo];

