﻿create procedure ADD_CRASH_LOG
    @AGENT_ID int, 
    @CRASH_DATETIME datetime, 
    @CRASH_DATA CRASH_DATA_TYPE
with encryption
as
begin
    set nocount on;

    insert into CRASH_LOGS(AGENT_ID, CRASH_DATETIME, CRASH_DATA)
    values (@AGENT_ID, @CRASH_DATETIME, @CRASH_DATA);
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_CRASH_LOG] TO [HAWKWebServiceUser]
    AS [dbo];

