﻿create procedure DISABLE_PROMO_CODE
 @PROMO_CODE_ID int,
 @STATUS_ID int output
with encryption
as
begin
	declare @STATE bit
	 
	select @STATE = STATE
	from FullPromoCodeHistory
	where ID = @PROMO_CODE_ID
	
	--when Admin click "Disable button", need check state. If at this time any user activated the promo code and changed 
	--status (full activated), should show the error = "the activation status of the selected Promo Code has changed"
	
	if @STATE = 1 --code of "partly activated"
	begin
		update  PromoCode
		set ACTIVE = 0
		where ID = @PROMO_CODE_ID
	end
	else begin
		select @STATUS_ID = 301
		return;
	end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DISABLE_PROMO_CODE] TO [HAWKWebSiteUser]
    AS [dbo];

