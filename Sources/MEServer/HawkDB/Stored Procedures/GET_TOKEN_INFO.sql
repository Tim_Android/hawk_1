﻿CREATE procedure GET_TOKEN_INFO
    @TOKEN VARCHAR(100) = null
with encryption    
as
begin
    if (@TOKEN is not null)
    begin
        select EMAIL
        from TOKENS
        where TOKEN = @TOKEN AND EXPIRATION_DATE > GETUTCDATE()
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_TOKEN_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

