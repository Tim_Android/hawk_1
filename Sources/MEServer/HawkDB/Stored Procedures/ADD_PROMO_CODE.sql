﻿create procedure ADD_PROMO_CODE
	@PROMO_CODE nvarchar(32) = null,
	@PRICE_LIST_ID int,
	@VALID_FROM datetime,
	@VALID_TO datetime,
	@DEVICE_COUNT int,
	@ACTIVATION_COUNT int,
	@ACTIVE bit,
	@PROMO_CODE_COUNT int = 0,
	@STATUS_ID  int output
with encryption
as
begin
	set nocount on;
	
	declare @DATE datetime = getutcdate()
	declare @COUNT int
	
	--if promo code entered manually, @PROMO_CODE_COUNT should be set as 0
	--check exists promode
	if @PROMO_CODE_COUNT = 0
	begin			
		select @COUNT = count(PROMO_CODE)
		from PromoCode
		where PROMO_CODE = @PROMO_CODE

		if @COUNT > 0
		begin
			select @STATUS_ID = 401; --the promo code already exists
			return;
		end
	end
	
	begin transaction INSERT_PROMO_CODE
	begin try
		if @PROMO_CODE_COUNT = 0
			begin
			-- the user entered a promo code manually, INSERT will be done one time
				insert into PromoCode(PROMO_CODE, PRICELIST_ID, VALID_FROM, VALID_TO, DATE_GENERATED, DEVICE_COUNT, ACTIVATION_COUNT, ACTIVE)
				values(@PROMO_CODE, @PRICE_LIST_ID, @VALID_FROM,@VALID_TO, @DATE, @DEVICE_COUNT, @ACTIVATION_COUNT, @ACTIVE)
			end	
		else
			begin
				declare @i int = 0
				while @i < @PROMO_CODE_COUNT 
				begin
				-- the user prefer to generate a promo code automatically, INSERT will be done @PROMO_CODE_COUNT times
					set @i = @i+1
					declare @id nvarchar(32) =  replace(newid(), '-', '') 
					insert into PromoCode(PROMO_CODE, PRICELIST_ID, VALID_FROM, VALID_TO, DATE_GENERATED, DEVICE_COUNT, ACTIVATION_COUNT, ACTIVE)
					values(@id, @PRICE_LIST_ID, @VALID_FROM,@VALID_TO, @DATE, @DEVICE_COUNT, @ACTIVATION_COUNT, @ACTIVE)
				end
			end
		commit transaction INSERT_PROMO_CODE
	end try
	begin catch
		rollback transaction INSERT_PROMO_CODE
		select @STATUS_ID = 100; -- insert error
		return;
	end catch
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_PROMO_CODE] TO [HAWKWebSiteUser]
    AS [dbo];

