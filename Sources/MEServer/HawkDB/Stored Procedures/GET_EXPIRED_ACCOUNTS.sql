﻿create procedure GET_EXPIRED_ACCOUNTS
    @PERIOD_DAYS int = 365
with encryption
as
begin
    select UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME
    from USERS_INFO UI
        left join LICENSES L on L.USER_ID = UI.ID
    where ASP_NET_USER_ID is not null
    group by UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME      
    having datediff(day, max(L.SUBSCR_END), getdate()) > @PERIOD_DAYS
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_EXPIRED_ACCOUNTS] TO [HAWKPeriodicEventsUser]
    AS [dbo];

