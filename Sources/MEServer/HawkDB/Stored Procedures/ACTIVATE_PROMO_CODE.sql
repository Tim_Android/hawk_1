﻿create procedure ACTIVATE_PROMO_CODE
	@PROMO_CODE nvarchar(32),
	@USER_ID int,
	@STATUS_ID int output
with encryption
as
begin
	set nocount on;

	declare @Date datetime = GETUTCDATE()
	declare @LIC_ID int
	declare @PAY_ID int
	declare @PRICE_LIST_ID int
	declare @DEV_COUNT int
	declare @PROMO_CODE_ID int
	declare @ACTIVE bit
	declare @STATE int
	declare @EXPIRED bit
	declare @PROMO_CODE_HISTORY_ID int


	select @ACTIVE = ACTIVE, @PROMO_CODE_ID = ID
	from PromoCode
	where PROMO_CODE = @PROMO_CODE
	if @PROMO_CODE_ID is  null or @ACTIVE = 0
	begin
    --@ERROR_ID = 201 the user activates an invalid or disabled promo code
		select @STATUS_ID = 201
		return;
	end
	
	
	select @STATE = STATE, @EXPIRED = EXPIRED
	from FullPromoCodeHistory
	where PROMO_CODE = @PROMO_CODE
	if @STATE = 2 --code of "Full Activated" state
	begin
    -- @ERROR_ID = 202 the user activates a promo code whose available activations have already been used up
		select @STATUS_ID = 202
		return;
	end
		
	select @PROMO_CODE_HISTORY_ID = ID
	from PromoCodeHistory 
	where PROMO_CODE_ID = @PROMO_CODE_ID and USER_ID = @USER_ID
	if @EXPIRED = 1 or @PROMO_CODE_HISTORY_ID is not null
	begin
    -- @ERROR_ID = 203 the user activates a promo code that is not valid any more
		select @STATUS_ID = 203
		return;
	end 
	
	--if everithing ok, activate promo code
	select @PROMO_CODE_ID = ID, @PRICE_LIST_ID = PRICELIST_ID, @DEV_COUNT = DEVICE_COUNT
	from PromoCode
	where PROMO_CODE = @PROMO_CODE
	
    --should minus 1
    set @DEV_COUNT = @DEV_COUNT - 1;
    
	begin transaction ACTIVATE_PROMOCODE
	exec FINALIZE_PAYMENT 
		@USER_ID, 
		3, -- PAYMENT SYSTEM TYPE OF "PROMO CODE"
		null, 
		@PRICE_LIST_ID, 
		'PROMOCODE', 
		@DEV_COUNT, 
		null, 
		@Date, 
		null,
		@LIC_ID output,
		@PAY_ID output
		
	
	if @LIC_ID is null or @PAY_ID is null
	begin
		select @STATUS_ID = 102 -- creation of license was broken
		rollback transaction ACTIVATE_PROMOCODE
		return;
	end
	else begin
			begin try
				insert into PromoCodeHistory(LICENSE_ID, PROMO_CODE_ID, USER_ID)
				values(@LIC_ID, @PROMO_CODE_ID, @USER_ID)
				commit transaction  ACTIVATE_PROMOCODE
			end try
			begin catch
				rollback transaction ACTIVATE_PROMOCODE
				SELECT @STATUS_ID = 100; -- insert error
				return;
			end catch
		end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ACTIVATE_PROMO_CODE] TO [HAWKWebSiteUser]
    AS [dbo];

