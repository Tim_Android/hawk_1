﻿create procedure GET_SMS
    @AGENT_ID int = null,
    @TYPE_FLTR int = null,
    @ADDR_FLTR ADDRESS_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @CONTENT_FLTR nvarchar(4000) = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if( @CONTENT_FLTR is not null)
    begin
        set @CONTENT_FLTR='%' + ltrim(@CONTENT_FLTR)
        set @CONTENT_FLTR=rtrim(@CONTENT_FLTR) + '%'
    end


    declare @TZ_DEV_OFFSET datetime

    exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output;
	with Cte
	as

    (select distinct S.SMS_ADDR, 
					CASE WHEN PHONES.PHONE_NAME is null 
						 THEN (select top 1 PHONES.PHONE_NAME 
							   from PHONES
							   where PHONES.DEVICE_ID = @AGENT_ID 
									and S.SMS_ADDR = PHONES.NUMBER
									and PHONES.PHONE_NAME is not null)
						ELSE PHONES.PHONE_NAME
					END AS PHONE_NAME,

					(S.SMS_DT + @TZ_DEV_OFFSET) as DEV_DATE, 
					dateadd(mi, isnull(@TZ_USER_OFFSET, 0), 
					S.SMS_DT) as USER_DATE, 
					S.SMS_BODY, rtrim(T.TYPE_NAME) as TYPE_NAME
    from SMS S
        left join MSG_TYPES T on T.ID_TYPE=S.TYPE_ID
		left join PHONES on S.PHONE_ID = PHONES.ID
												
    where ((@AGENT_ID is not null) and (S.AGENT_ID_SMS = @AGENT_ID))
        and ((@TYPE_FLTR is null) or (S.TYPE_ID = @TYPE_FLTR))
        and ((@BEG_DATE_FLTR is null) or ((S.SMS_DT + @TZ_DEV_OFFSET) >= @BEG_DATE_FLTR))
        and ((@END_DATE_FLTR is null) or ((S.SMS_DT + @TZ_DEV_OFFSET) <= @END_DATE_FLTR))
        and ((@CONTENT_FLTR is null) or (S.SMS_BODY like @CONTENT_FLTR))
    
	)
	select * from Cte
	where (@ADDR_FLTR is null) or (Cte.SMS_ADDR = @ADDR_FLTR or Cte.PHONE_NAME = @ADDR_FLTR)
	order by DEV_DATE desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_SMS] TO [HAWKWebSiteUser]
    AS [dbo];

