﻿create procedure GET_CALLS
    @AGENT_ID int = null,
    @CTYPE_FLTR smallint = null,
    @NUMBER_FLTR PNAME_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    declare @NUM_FLTR PNAME_TYPE;
    set @NUM_FLTR = @NUMBER_FLTR
    if (@NUM_FLTR is not null)
    begin
        set @NUM_FLTR='%' + ltrim(@NUM_FLTR);
        set @NUM_FLTR=rtrim(@NUM_FLTR) + '%';
    end

    if (@AGENT_ID is not null)
    begin 
		declare @TZ_DEV_OFFSET datetime

        exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

                select  distinct DATE, 
                (DATE + @TZ_DEV_OFFSET) as DEV_DATE, 
                dateadd(mi, isnull(@TZ_USER_OFFSET, 0), DATE) as USER_DATE,
                case C.CTYPE
                    when 1 THEN 'Incoming'
                    when 2 THEN 'Outgoing'
                    when 3 THEN 'Missed'
                    else 'unknown'
                    end as CALL_TYPE,
                case when (C.DURATION / 3600 < 10) then '0' else '' end + cast (C.DURATION/3600 as varchar(2)) + ':' 
                    + case when (C.DURATION % 3600 / 60 < 10) then '0' else '' end + cast (C.DURATION%3600/60 as varchar(2)) + ':' 
                    + case when (C.DURATION % 3600 % 60 < 10) then '0' else '' end + cast (C.DURATION%3600%60 as varchar(2)) 
                    as DURATION,
                case C.CTYPE
                    when 1 THEN P_FROM.NUMBER -- 'Incoming'
                    when 2 THEN P_TO.NUMBER -- 'Outgoing'
                    when 3 THEN P_FROM.NUMBER -- 'Missed'
                    else 'unknown'
                    END AS PHONE_NUMBER,
                case C.CTYPE
                    when 1 THEN P_FROM.PHONE_NAME -- 'Incoming'
                    when 2 THEN P_TO.PHONE_NAME -- 'Outgoing'
                    when 3 THEN P_FROM.PHONE_NAME -- 'Missed'
                    else 'unknown'
                    END AS CONTACT_NAME,
                case C.CTYPE
                    when 1 THEN T_FROM.NUMBER_TYPE -- 'Incoming'
                    when 2 THEN T_TO.NUMBER_TYPE-- 'Outgoing'
                    when 3 THEN T_FROM.NUMBER_TYPE -- 'Missed'
                    else 'unknown'
                    END AS NUMBER_TYPE
            from CALLS C         
            left join PHONES P_TO on P_TO.ID = C.TO_PHONE_ID
            left join NUMBERS_TYPES T_TO on T_TO.ID = C.TO_NUM_TYPE_ID                                    
            left join PHONES P_FROM on P_FROM.ID = C.FROM_PHONE_ID
            left join NUMBERS_TYPES T_FROM on T_FROM.ID = C.FROM_NUM_TYPE_ID
        where (C.AGENT_ID = @AGENT_ID)            
            and ((@CTYPE_FLTR is null) or (C.CTYPE = @CTYPE_FLTR))
            and ((@BEG_DATE_FLTR is null) or ((DATE + @TZ_DEV_OFFSET) >= @BEG_DATE_FLTR))
            and ((@END_DATE_FLTR is null) or ((DATE + @TZ_DEV_OFFSET) <= @END_DATE_FLTR))
            and ((@NUMBER_FLTR is null) or (P_FROM.NUMBER like @NUM_FLTR) or (P_TO.NUMBER like @NUM_FLTR)
                or (P_FROM.PHONE_NAME like @NUM_FLTR) or (P_TO.PHONE_NAME like @NUM_FLTR))         
        order by DATE desc            
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_CALLS] TO [HAWKWebSiteUser]
    AS [dbo];

