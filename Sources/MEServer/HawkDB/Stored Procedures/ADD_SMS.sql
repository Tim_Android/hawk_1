﻿create procedure ADD_SMS 
  @AGENT_ID int, 
  @MSG_TYPE nchar(10), 
  @SMS_ADDR ADDRESS_TYPE,
  @CONTACT_NAME ADDRESS_TYPE = null,
  @SMS_DT datetime, 
  @SMS_BODY nchar(4000)
with encryption
as
declare @PHONE_ID int
begin
    set nocount on;

    declare @TYPE_ID int;
    
    select @TYPE_ID=ID_TYPE
    from MSG_TYPES
    where TYPE_NAME = @MSG_TYPE;

	exec GET_PHONE_ID @AGENT_ID, @SMS_ADDR, @CONTACT_NAME, @PHONE_ID output

    insert into SMS(AGENT_ID_SMS, TYPE_ID, SMS_ADDR, PHONE_ID, SMS_DT, SMS_BODY)
    values(@AGENT_ID, @TYPE_ID, @SMS_ADDR, @PHONE_ID, @SMS_DT, @SMS_BODY);
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_SMS] TO [HAWKWebServiceUser]
    AS [dbo];

