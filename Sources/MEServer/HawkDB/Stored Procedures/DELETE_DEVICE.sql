﻿create procedure DELETE_DEVICE
    @AGENT_ID int = null
with encryption
as
begin  
    if (@AGENT_ID is not null)
    begin
        -- prepare agent to delete
        exec DELETE_CALLS @AGENT_ID
        
        delete 
        from MESSAGES
        where AGENT_ID_MSG = @AGENT_ID
        
        delete 
        from COORDINATES
        where AGENT_ID = @AGENT_ID
        
		delete 
        from CRASH_LOGS
        where AGENT_ID = @AGENT_ID        
        
        -- delete agent
        delete 
        from DEVICES
        where ID = @AGENT_ID
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_DEVICE] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_DEVICE] TO [HAWKWebSiteUser]
    AS [dbo];

