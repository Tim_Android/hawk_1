﻿create procedure GET_NUMBER_TYPE_ID
 	@NTYPE NUM_NAME_TYPE,
    @NUMBER_TYPE_ID int output
with encryption
as
begin
    set @NUMBER_TYPE_ID = null
    if (@NTYPE is not null)
    begin
        select @NUMBER_TYPE_ID=T.ID
        from NUMBERS_TYPES T
        where T.NUMBER_TYPE = @NTYPE

        if (@NUMBER_TYPE_ID is null)
        begin
    	    insert into NUMBERS_TYPES(NUMBER_TYPE)
            values(@NTYPE)
        
            set @NUMBER_TYPE_ID = CAST(SCOPE_IDENTITY() as int)
        end
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_NUMBER_TYPE_ID] TO [HAWKWebSiteUser]
    AS [dbo];

