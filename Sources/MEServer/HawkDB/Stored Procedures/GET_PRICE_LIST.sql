﻿create procedure GET_PRICE_LIST 
    @PAYMENT_DATE datetime = null,
    @PAY_TYPE smallint = null
with encryption    
as
begin    
    if(@PAYMENT_DATE is null)
    begin
        set @PAYMENT_DATE = getdate()
    end

    select ID, LABEL, DURATION, PAY_TYPE, PRICE, PRICE_OTHER, RETAIL_PRICE,
        dateadd(day, DURATION, @PAYMENT_DATE) as SUBSCR_END, DATE_FROM
    from ACTIVE_PRICE_LIST    
    where ((@PAY_TYPE is null) or (PAY_TYPE=@PAY_TYPE))
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PRICE_LIST] TO [HAWKWebSiteUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PRICE_LIST] TO [HAWKPeriodicEventsUser]
    AS [dbo];

