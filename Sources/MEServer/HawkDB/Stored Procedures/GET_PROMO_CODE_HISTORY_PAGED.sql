﻿create procedure GET_PROMO_CODE_HISTORY_PAGED
	@RECORDS_PER_PAGE int,

	--search
	@PROMO_CODE nvarchar(32) = null,
	@GENERATED_FROM date = null,
	@GENERATED_TO date = null,
	@SUBSCRIPTION_PERIOD_ID int = null,
	@STATE int = null,
	@EXPIRED bit = null,
	--end search
			
	@PAGE int output,
	@TOTAL_PAGES int output,
    @TOTAL_RECORDS int output
with encryption
as
begin
    declare @START int
    declare @END int

	--show all records - should set @RECORDS_PER_PAGE and @PAGE, another values can be null
	;With Cte As
	(
		select ID 
		from FullPromoCodeHistory
		where(
			    ( @SUBSCRIPTION_PERIOD_ID is null or PRICE_LIST_ID = @SUBSCRIPTION_PERIOD_ID )
				and (@STATE is null or STATE = @STATE )
				and (@EXPIRED is null or EXPIRED = @EXPIRED)
			    and ((@GENERATED_FROM is null) or (convert(date, DATE_GENERATED) >= @GENERATED_FROM))
                and ((@GENERATED_TO is null) or (convert(date, DATE_GENERATED) <= @GENERATED_TO))
                and (@PROMO_CODE is null or PROMO_CODE like '%'+@PROMO_CODE+'%')  
              )
	)--With Cte As			
	select @TOTAL_RECORDS = count(ID)
	from Cte;

	set @TOTAL_PAGES = (@TOTAL_RECORDS + @RECORDS_PER_PAGE - 1) / @RECORDS_PER_PAGE;
	
	if @PAGE > @TOTAL_PAGES and @TOTAL_PAGES != 0
		set @PAGE = @TOTAL_PAGES 
	
	set @START = ((@PAGE - 1) * @RECORDS_PER_PAGE)+1;
	set @END = @PAGE * @RECORDS_PER_PAGE;
	
	;With Cte As
	(
		select
			rn = row_number() Over(Order by (ID) DESC), 
			ID, 
			DATE_GENERATED ,
			PROMO_CODE,
			DEVICE_COUNT,
			LABEL,
			TOTAL_ACTIVATIONS_AVAILABLE,
			ACTIVATION_USED,
			ACTIVATIONS_LEFT,
			VALID_FROM,
			VALID_TO,
			STATE,
			ACTIVE,
			EXPIRED
		from FullPromoCodeHistory
		where(
			    ( @SUBSCRIPTION_PERIOD_ID is null or PRICE_LIST_ID = @SUBSCRIPTION_PERIOD_ID )
				and (@STATE is null or STATE = @STATE )
				and (@EXPIRED is null or EXPIRED = @EXPIRED)
			    and ((@GENERATED_FROM is null) or (convert(date, DATE_GENERATED) >= @GENERATED_FROM))
                and ((@GENERATED_TO is null) or (convert(date, DATE_GENERATED) <= @GENERATED_TO))
                and (@PROMO_CODE is null or PROMO_CODE like '%'+@PROMO_CODE+'%')  
              )
	)--With Cte As
	
	select  ID, 
			DATE_GENERATED ,
			PROMO_CODE,
			DEVICE_COUNT,
			LABEL,
			TOTAL_ACTIVATIONS_AVAILABLE,
			ACTIVATION_USED,
			ACTIVATIONS_LEFT,
			VALID_FROM,
			VALID_TO,
			STATE,
			ACTIVE,
			EXPIRED
	from Cte
	where rn between  @START and @END
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PROMO_CODE_HISTORY_PAGED] TO [HAWKWebSiteUser]
    AS [dbo];

