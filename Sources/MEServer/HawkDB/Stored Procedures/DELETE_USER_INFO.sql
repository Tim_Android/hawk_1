﻿create procedure DELETE_USER_INFO
    @ASP_USER_ID uniqueidentifier = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        -- delete the user's devices
        declare @AGENT_ID int
              
        declare @DEVICES_CURSOR cursor
        set @DEVICES_CURSOR = cursor scroll for
          select D.ID
          from USERS_INFO U
              left join DEVICES D on D.USER_ID = U.ID
          where U.ASP_NET_USER_ID = @ASP_USER_ID
          
        open @DEVICES_CURSOR
        
        fetch next from @DEVICES_CURSOR into @AGENT_ID
        while @@FETCH_STATUS = 0
        begin
            exec DELETE_DEVICE @AGENT_ID
            fetch next from @DEVICES_CURSOR into @AGENT_ID
        end
        
        close @DEVICES_CURSOR
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_USER_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

