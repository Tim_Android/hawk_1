﻿CREATE PROCEDURE [dbo].[UPDATE_LICENSE]
	@Id int,
	@SubscriptionEndDate datetime,
	@DeviceCount int

	with encryption
AS
begin
	update LICENSES
		set
			SUBSCR_END = @SubscriptionEndDate,
			DEV_CNT = @DeviceCount

		where ID = @Id
end

GO

GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_LICENSE] TO [HAWKWebSiteUser]
    AS [dbo];
	GO 

GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_LICENSE] TO [HAWKWebServiceUser]
    AS [dbo];
	GO
