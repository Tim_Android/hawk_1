﻿CREATE PROCEDURE [dbo].[UPDATE_BANNED_EMAIL]

  @EMAIL EMAIL_TYPE,
  @NOTE nvarchar(MAX)


with encryption
as
begin
    set nocount on;
	UPDATE BANNED_LIST SET NOTE = @NOTE WHERE EMAIL = @EMAIL;
end

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_BANNED_EMAIL] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_BANNED_EMAIL] TO [HAWKWebSiteUser]
    AS [dbo];
