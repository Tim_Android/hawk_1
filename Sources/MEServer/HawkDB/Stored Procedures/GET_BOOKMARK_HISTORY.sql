﻿create procedure GET_BOOKMARK_HISTORY
    @AGENT_ID int = null,
    @URL_FLTR BROWSER_URL_TYPE = null,
	@TITLE_FLTR BROWSER_TITLE_TYPE = null
with encryption
as
begin
    if( @URL_FLTR is not null)
    begin
        set @URL_FLTR='%' + ltrim(@URL_FLTR)
        set @URL_FLTR=rtrim(@URL_FLTR) + '%'
    end

    if( @TITLE_FLTR is not null)
    begin
        set @TITLE_FLTR='%' + ltrim(@TITLE_FLTR)
        set @TITLE_FLTR=rtrim(@TITLE_FLTR) + '%'
    end

    select distinct B.ID, B.AGENT_ID, B.TITLE, B.URL
    from BOOKMARKS B
    where ((@AGENT_ID is not null) and (B.AGENT_ID = @AGENT_ID))
        and ((@TITLE_FLTR is null) or (B.TITLE like @TITLE_FLTR))
        and ((@URL_FLTR is null) or (B.URL like @URL_FLTR))
    order by ID desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_BOOKMARK_HISTORY] TO [HAWKWebSiteUser]
    AS [dbo];