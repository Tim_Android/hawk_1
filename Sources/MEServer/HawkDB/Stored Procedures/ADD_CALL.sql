﻿create procedure ADD_CALL
    @AGENT_ID int,
    @CALL_DATE datetime,
    @CALL_TYPE nchar(15),
    @FROM_NUMBER PHONE_TYPE,
    @FROM_PHONE_NAME PNAME_TYPE,
    @FROM_NUM_TYPE NUM_NAME_TYPE,
    @TO_NUMBER PHONE_TYPE,
    @TO_PHONE_NAME PNAME_TYPE,
    @TO_NUM_TYPE NUM_NAME_TYPE,
    @DURATION int
with encryption
as
    declare @FROM_PHONE_ID int
    declare @TO_PHONE_ID int
    declare @FROM_NUM_TYPE_ID int
    declare @TO_NUM_TYPE_ID int
begin
    set nocount on;

    declare @CTYPE smallint    
    set @CTYPE = 0
    if (@CALL_TYPE like '%incoming%')
    begin
        set @CTYPE = 1
    end
    if (@CALL_TYPE like '%outgoing%')
    begin
        set @CTYPE = 2
    end     
    if (@CALL_TYPE like '%missed%')
    begin
        set @CTYPE = 3
    end

    exec GET_PHONE_ID @AGENT_ID, @FROM_NUMBER, @FROM_PHONE_NAME, @FROM_PHONE_ID output
    exec GET_PHONE_ID @AGENT_ID, @TO_NUMBER, @TO_PHONE_NAME, @TO_PHONE_ID output
    exec GET_NUMBER_TYPE_ID @FROM_NUM_TYPE, @FROM_NUM_TYPE_ID output
    exec GET_NUMBER_TYPE_ID @TO_NUM_TYPE, @TO_NUM_TYPE_ID output

    insert into CALLS(AGENT_ID, DATE, CTYPE, DURATION, FROM_PHONE_ID, TO_PHONE_ID, FROM_NUM_TYPE_ID, TO_NUM_TYPE_ID)
    values (@AGENT_ID, @CALL_DATE, @CTYPE, @DURATION, @FROM_PHONE_ID, @TO_PHONE_ID, @FROM_NUM_TYPE_ID, @TO_NUM_TYPE_ID);
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_CALL] TO [HAWKWebServiceUser]
    AS [dbo];

