﻿create procedure ADD_EMAIL_TO_BANNED_LIST 
    @EMAIL EMAIL_TYPE,
 	@NOTE nvarchar(MAX),
 	
    @ID int output
with encryption
as
begin
    set nocount on;

	insert into BANNED_LIST (EMAIL, BAN_DATE, NOTE)
    values(@EMAIL, getdate(),@NOTE)
    
	set @ID=CAST(SCOPE_IDENTITY() as int)
end




GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_EMAIL_TO_BANNED_LIST] TO [HAWKWebSiteUser]
    AS [dbo];
