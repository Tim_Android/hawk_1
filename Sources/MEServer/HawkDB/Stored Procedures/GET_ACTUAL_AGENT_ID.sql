﻿create procedure GET_ACTUAL_AGENT_ID
    @AGENT_ID int,    
    @DEVICE_ID DEVICE_ID_TYPE = null,
    @ACTUAL_AGENT_ID int output
with encryption
as
begin
    set nocount on;

    set @ACTUAL_AGENT_ID = null;
  
    if (@AGENT_ID is null)
    begin
        return;
    end

    declare @USER_ID int;
    declare @AGENT_NAME nvarchar(50)
    declare @OPER_SYS_TYPE int
    

    select @USER_ID=D.USER_ID, @AGENT_NAME=D.AGENT_NAME, @OPER_SYS_TYPE = d.OPER_SYS_TYPE
    from DEVICES D
    where D.ID = @AGENT_ID;
  
    -- agent is exist for this device
    select @ACTUAL_AGENT_ID = D.ID
    from DEVICES D
    where (D.DEVICE_ID = @DEVICE_ID) and (D.USER_ID = @USER_ID);
    
    if (@ACTUAL_AGENT_ID is not null)
    begin
        return;
    end    

    -- else  
    select @ACTUAL_AGENT_ID = D.ID
    from DEVICES D
    where (D.ID = @AGENT_ID) and (D.DEVICE_ID is null)

    if (@ACTUAL_AGENT_ID is not null)
    begin
        -- first agent's start
        update DEVICES
        set DEVICE_ID = @DEVICE_ID
        where ID = @AGENT_ID;
        return;
    end    
    else begin
        -- doubling agent (new device for AGENT)     
        exec ADD_AGENT @USER_ID, @AGENT_NAME, @DEVICE_ID, @OPER_SYS_TYPE, @ACTUAL_AGENT_ID output
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_ACTUAL_AGENT_ID] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_ACTUAL_AGENT_ID] TO [HAWKWebSiteUser]
    AS [dbo];

