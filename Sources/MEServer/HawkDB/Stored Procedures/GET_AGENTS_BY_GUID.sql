﻿create procedure GET_AGENTS_BY_GUID
    @USER_GUID uniqueidentifier = null,
    @IS_ACTIVED bit = 1
with encryption
as
begin
    if (@USER_GUID is not null)
    begin
        select D.ID, isnull(D.DEVICE_NAME, D.AGENT_NAME) as DEVICE_NAME, 
            D.LAST_UPDATED, isnull(D.DEVICE_ID, '<no data>') as DEVICE_ID, D.LIC_ID
        from DEVICES D 
            join USERS_INFO U on U.ID = D.USER_ID
        where (U.ASP_NET_USER_ID = @USER_GUID) and (IS_ACTIVED = @IS_ACTIVED)
        order by D.DEVICE_NAME
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_AGENTS_BY_GUID] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_AGENTS_BY_GUID] TO [HAWKWebSiteUser]
    AS [dbo];

