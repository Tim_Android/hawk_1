﻿create procedure FINALIZE_AUTO_PAYMENT
    @PAYMENT_DATE datetime = null,    
    @SUBSCR_ID SUBSCR_ID_TYPE,
    @TRANS_ID SUBSCR_ID_TYPE,
    @PAY_COMMENT COMMENT_TYPE = null,
    @PAY_ID int output
with encryption
as
    declare @LIC_ID int
    declare @USER_ID int
    declare @PAY_SYS_TYPE smallint
    declare @OUT_LIC_ID int
    declare @PRICE_LIST_ID int
begin
    set nocount on;

	set @PAY_ID = null

    select @LIC_ID=L.ID, @PRICE_LIST_ID=PRICE_LIST_ID, 
        @USER_ID=L.USER_ID, @PAY_SYS_TYPE=L.PAY_SYS_TYPE
    from LICENSES L
    where L.SUBSCR_ID = @SUBSCR_ID

	if( @LIC_ID is not null )
	begin
		exec FINALIZE_PAYMENT @USER_ID, @PAY_SYS_TYPE, @SUBSCR_ID, @PRICE_LIST_ID, 
			@TRANS_ID, 0, @LIC_ID, @PAYMENT_DATE, @PAY_COMMENT, 
			@OUT_LIC_ID output, @PAY_ID output
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[FINALIZE_AUTO_PAYMENT] TO [HAWKWebSiteUser]
    AS [dbo];

