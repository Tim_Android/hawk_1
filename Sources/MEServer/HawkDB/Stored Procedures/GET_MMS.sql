﻿create procedure GET_MMS
    @AGENT_ID int = null,
    @TYPE_FLTR int = null,    
    @ADDR_FLTR ADDRESS_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @CONTENT_FLTR nvarchar(4000) = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if( @CONTENT_FLTR is not null)
    begin
        set @CONTENT_FLTR='%' + ltrim(@CONTENT_FLTR)
        set @CONTENT_FLTR=rtrim(@CONTENT_FLTR) + '%'
    end

    if( @ADDR_FLTR is not null)
    begin
        set @ADDR_FLTR='%' + ltrim(@ADDR_FLTR)
        set @ADDR_FLTR=rtrim(@ADDR_FLTR) + '%'
    end

    declare @TZ_DEV_OFFSET datetime

    exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output;

	with Cte
	as
    (select distinct 
		M.ID_MSG, 
		M.MSG_ADDR, 
		CASE WHEN PHONES.PHONE_NAME is null 
			THEN (select top 1 PHONES.PHONE_NAME 
				from PHONES
				where PHONES.DEVICE_ID = @AGENT_ID 
					and M.MSG_ADDR = PHONES.NUMBER
					and PHONES.PHONE_NAME is not null)
			ELSE PHONES.PHONE_NAME
		END AS PHONE_NAME,
		(M.MSG_DT + @TZ_DEV_OFFSET) as DEV_DATE, 
		dateadd(mi, isnull(@TZ_USER_OFFSET, 0), 
		M.MSG_DT) as USER_DATE, 
		M.MSG_SUBJ, 
		M.MSG_BODY, 
		T.TYPE_NAME,
		stuff(
			(select ', ' + 'attachment' + cast(MMS_ATTACH.ID_ATTACH as nvarchar(max)) + rtrim(MIME_TYPES.EXT)
			from MMS_ATTACH
				join MIME_TYPES on MMS_ATTACH.ID_MIME = MIME_TYPES.ID_MIME
			where M.ID_MSG = MMS_ATTACH.MSG_ID
			for xml path('')
			),1,2,'') as ATTACHMENTS
    from MESSAGES M
        left join MSG_TYPES T on T.ID_TYPE=M.TYPE_ID
		left join PHONES on PHONES.ID = M.PHONE_ID
    where ((@AGENT_ID is not null) and (M.AGENT_ID_MSG = @AGENT_ID))
        and ((@TYPE_FLTR is null) or (M.TYPE_ID = @TYPE_FLTR))
        and ((M.MSG_SUBJ is not null) or exists(select A.MSG_ID 
                                                from MMS_ATTACH A 
                                                where A.MSG_ID = M.ID_MSG))
        and ((@BEG_DATE_FLTR is null) or ((M.MSG_DT + @TZ_DEV_OFFSET) >= @BEG_DATE_FLTR))
        and ((@END_DATE_FLTR is null) or ((M.MSG_DT + @TZ_DEV_OFFSET) <= @END_DATE_FLTR))
        and ((@CONTENT_FLTR is null) or (M.MSG_BODY like @CONTENT_FLTR)
            or (M.MSG_SUBJ like @CONTENT_FLTR))
	)
	select * from Cte
	where (@ADDR_FLTR is null) or (Cte.MSG_ADDR like @ADDR_FLTR or Cte.PHONE_NAME like @ADDR_FLTR)
	order by DEV_DATE desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_MMS] TO [HAWKWebSiteUser]
    AS [dbo];

