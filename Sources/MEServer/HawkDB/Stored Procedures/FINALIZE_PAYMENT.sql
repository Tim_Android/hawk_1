﻿create procedure FINALIZE_PAYMENT
    @USER_ID int,
    @PAY_SYS_TYPE smallint,
    @SUBSCR_ID SUBSCR_ID_TYPE = null,    -- <null> if don't subscribe
    @PL_ID int = null,                   -- is null if LICENSE extend
    @TRANS_ID SUBSCR_ID_TYPE,
    @ADD_DEV_COUNT int = 0,
    @IN_LIC_ID int = null,   
    @PAYMENT_DATE datetime = null,
    @PAY_COMMENT COMMENT_TYPE = null,
    @OUT_LIC_ID int output,              -- <Ok> if is not null
    @PAY_ID int output                   -- <Ok> if is not null
with encryption
as
    declare @PREV_PRICE_LIST_ID int
    declare @PREV_SUBSCR_END datetime
    declare @PREV_DEV_COUNT int
    declare @NEW_SUBSCR_END datetime    
    declare @SUBSCR_END datetime
    declare @AMOUNT_OLD_PERIOD_NEW_DEV_EACH money
    declare @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL money 
    declare @AMOUNT_NEW_PERIOD_FIRST money
    declare @AMOUNT_NEW_PERIOD_OTHER money
    declare @AMOUNT_NEW_PERIOD_TOTAL money
    declare @AMOUNT_TOTAL money
    declare @PRICE_LIST_ID_OLD_PERIOD int
begin
    set nocount on;

    set @OUT_LIC_ID = null;
    set @PAY_ID = null;

    if ((@IN_LIC_ID is null) and (@PL_ID is null))
    begin
        return
    end

    if (@PAYMENT_DATE is null)
    begin
      set @PAYMENT_DATE = getutcdate();
    end

	if (@IN_LIC_ID is not null)
	begin
        select @PREV_SUBSCR_END = SUBSCR_END, 
               @PREV_PRICE_LIST_ID = PRICE_LIST_ID, @PREV_DEV_COUNT = DEV_CNT
        from LICENSES
        where ID = @IN_LIC_ID
        
        set @OUT_LIC_ID = @IN_LIC_ID;
    end
    else begin
        set @PREV_SUBSCR_END = @PAYMENT_DATE;
        set @PREV_DEV_COUNT = 1;
    end

    exec CALCULATE_PAYMENT @IN_LIC_ID, @ADD_DEV_COUNT, @PAYMENT_DATE, @PL_ID,
        @AMOUNT_OLD_PERIOD_NEW_DEV_EACH output, 
        @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL output, 
        @AMOUNT_NEW_PERIOD_FIRST output, @AMOUNT_NEW_PERIOD_OTHER output, 
        @AMOUNT_TOTAL output, @PRICE_LIST_ID_OLD_PERIOD output

    set @AMOUNT_NEW_PERIOD_TOTAL = @AMOUNT_TOTAL - isnull(@AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL, 0);

    exec GET_NEW_SUBSCR_END @PL_ID, @IN_LIC_ID, @PREV_SUBSCR_END, @NEW_SUBSCR_END output

    set @ADD_DEV_COUNT = isnull(@ADD_DEV_COUNT, 0);

    begin transaction FINAL_PAY
    
	if (@IN_LIC_ID is null)
	begin
        exec ADD_LICENSE @USER_ID, @PL_ID, @NEW_SUBSCR_END, @ADD_DEV_COUNT, 
            @PAY_SYS_TYPE, @SUBSCR_ID, @OUT_LIC_ID output
	end
	else begin
		update LICENSES
		set  PRICE_LIST_ID = isnull(@PL_ID, PRICE_LIST_ID),
			 SUBSCR_END = @NEW_SUBSCR_END,
			 DEV_CNT = DEV_CNT + @ADD_DEV_COUNT,
			 PAY_SYS_TYPE = @PAY_SYS_TYPE,
			 SUBSCR_ID = @SUBSCR_ID
		where ID = @IN_LIC_ID
    end

    -- ADD NEW PAYMENT ------------------------------------------------------
	if( (@ADD_DEV_COUNT > 0) and (@IN_LIC_ID is not null) )
    begin    -- Added devices to current period -----------------------------
		declare @PriceListId int = isnull(@PRICE_LIST_ID_OLD_PERIOD, @PL_ID);

		exec ADD_PAYMENT_HISTORY_ENTRY 
				@OUT_LIC_ID OUTPUT,
				@PriceListId,
				@PAYMENT_DATE,
				@TRANS_ID,
				@AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL,
				@ADD_DEV_COUNT,
				@PREV_SUBSCR_END,
				@PREV_SUBSCR_END,
				@PAY_COMMENT,
				@PAY_ID OUTPUT
	end

	if( @NEW_SUBSCR_END > @PREV_SUBSCR_END )
    begin    -- New period --------------------------------------------------
		declare @DeviceCount int = @PREV_DEV_COUNT + @ADD_DEV_COUNT;
		declare @Price_List_Id int = isnull(@PL_ID, @PREV_PRICE_LIST_ID);

		exec ADD_PAYMENT_HISTORY_ENTRY
				@OUT_LIC_ID OUTPUT,
				@Price_List_Id,
				@PAYMENT_DATE,
				@TRANS_ID,
				@AMOUNT_NEW_PERIOD_TOTAL,
				@DeviceCount,
				@NEW_SUBSCR_END,
				@PREV_SUBSCR_END,
				@PAY_COMMENT,
				@PAY_ID OUTPUT

    end

    commit transaction FINAL_PAY
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[FINALIZE_PAYMENT] TO [HAWKWebSiteUser]
    AS [dbo];

