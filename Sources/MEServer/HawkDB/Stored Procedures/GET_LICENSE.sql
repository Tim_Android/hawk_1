﻿create procedure GET_LICENSE
    @LIC_ID int,
    @REPORT_DATE datetime = null,    
    @DEVICE_COUNT int output,
    @RESIDUAL_DAYS int output,
    @SUBSCR_END datetime output,
    @PRICE_LIST_ID int output,
    @SUBSCR_ID SUBSCR_ID_TYPE output,
    @PAY_SYS_TYPE smallint output,
    @FREE_DEV_COUNT int output
with encryption    
as
declare @ACT_DEV_COUNT int
begin
    set nocount on;
    
    select @SUBSCR_END = SUBSCR_END, @DEVICE_COUNT = isnull(DEV_CNT, 0), 
        @PRICE_LIST_ID = PRICE_LIST_ID, @SUBSCR_ID = SUBSCR_ID, @PAY_SYS_TYPE = PAY_SYS_TYPE 
    from LICENSES
    where ID = @LIC_ID

    select @ACT_DEV_COUNT = isnull(count(D.ID), 0)
    from DEVICES D
    where D.LIC_ID = @LIC_ID

    set @FREE_DEV_COUNT = @DEVICE_COUNT - @ACT_DEV_COUNT

    if(@REPORT_DATE is null)
    begin
        set @REPORT_DATE = getdate()
    end

    set @RESIDUAL_DAYS = datediff(day, @REPORT_DATE, @SUBSCR_END);
    if((@RESIDUAL_DAYS < 0) or (@RESIDUAL_DAYS is null))
    begin
        set @RESIDUAL_DAYS = 0
    end

    if(@DEVICE_COUNT is null)
    begin
        set @DEVICE_COUNT = 0 
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_LICENSE] TO [HAWKWebSiteUser]
    AS [dbo];

