﻿create procedure SET_LICENSE_SUBSCR_ID
    @LIC_ID int = null,
    @OLD_SUBSCR_ID SUBSCR_ID_TYPE = null,
    @NEW_SUBSCR_ID SUBSCR_ID_TYPE = null  -- null if cancelling subscription
with encryption    
as    
begin
    set nocount on;

    if((@LIC_ID is null) and (@OLD_SUBSCR_ID is null))
    begin
        return
    end

	update LICENSES
	set SUBSCR_ID = @NEW_SUBSCR_ID
	where ((ID = @LIC_ID) or (@LIC_ID is null))
        and ((SUBSCR_ID = @OLD_SUBSCR_ID) or (@OLD_SUBSCR_ID is null))
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[SET_LICENSE_SUBSCR_ID] TO [HAWKWebSiteUser]
    AS [dbo];

