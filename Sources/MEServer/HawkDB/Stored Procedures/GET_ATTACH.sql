﻿create procedure GET_ATTACH @ID_ATTACH int
with encryption
as
begin
    select ATTACH_DATA 
    from MMS_ATTACH
    where ID_ATTACH = @ID_ATTACH
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_ATTACH] TO [HAWKWebSiteUser]
    AS [dbo];

