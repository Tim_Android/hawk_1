﻿create procedure ADD_MMS_ATTACH 
    @AGENT_ID int, 
    @AGENT_ATTACH_ID int, 
    @ATTACH_DATA ATTACH_DATA_TYPE
with encryption
as
declare @MESSAGE_ID int
begin
    set nocount on;
    
    declare @ID_ATTACH int
    
    select @ID_ATTACH=A.ID_ATTACH
    from MESSAGES M
        left join MMS_ATTACH A on M.ID_MSG = A.MSG_ID
    where M.AGENT_ID_MSG = @AGENT_ID and A.AGENT_ATTACH_ID = @AGENT_ATTACH_ID
    
    update MMS_ATTACH 
    set ATTACH_DATA = @ATTACH_DATA
    where ID_ATTACH = @ID_ATTACH
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_MMS_ATTACH] TO [HAWKWebServiceUser]
    AS [dbo];

