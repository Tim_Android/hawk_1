﻿create procedure IS_LICENSED_DEVICE
    @AGENT_ID int = null,
    @IS_LICENSED bit output
with encryption    
as
    declare @LIC_ID int
    declare @SUBSCR_END datetime
begin
    set nocount on;

    set @IS_LICENSED = 0;

    if (@AGENT_ID is not null)
    begin
        select @LIC_ID = D.LIC_ID
        from DEVICES D
        where D.ID = @AGENT_ID;
        
        if( @LIC_ID is not null )
        begin
            select @SUBSCR_END = L.SUBSCR_END
            from LICENSES L
            where L.ID = @LIC_ID

            if( (@SUBSCR_END is not null) and (@SUBSCR_END >= getdate()) )
            begin
                set @IS_LICENSED = 1;
            end
        end
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[IS_LICENSED_DEVICE] TO [HAWKWebServiceUser]
    AS [dbo];

