﻿create procedure CALCULATE_SUPPORT_PAYMENT
    @LIC_ID int = null,  -- is null if new LICENSE
    @PAYMENT_DATE datetime = null,
    @PL_ID int = null,  -- = (to existing subscription)
    @AMOUNT_NEW_PERIOD_FIRST money output,
    @AMOUNT_NEW_PERIOD_OTHER money output,
    @AMOUNT_TOTAL money output,
    @PRICE_LIST_ID_OLD_PERIOD int output
with encryption   
as
    declare @PREV_DEV_COUNT int
    declare @PREV_SUBSCR_END datetime
    declare @RESIDUAL_DAYS int
    declare @PREV_DURATION int
    declare @PREV_PL_ID int
    declare @PREV_SUBSCR_ID SUBSCR_ID_TYPE
    declare @PREV_PAY_SYS_TYPE smallint
    declare @FREE_DEV_COUNT int
begin    
    set @AMOUNT_NEW_PERIOD_FIRST = 0.0
    set @AMOUNT_NEW_PERIOD_OTHER = 0.0
    set @AMOUNT_TOTAL = 0.0
    set @PRICE_LIST_ID_OLD_PERIOD = null

    if( (@LIC_ID is null) and (@PL_ID is null) )
    begin
        return
    end

    if( @LIC_ID is not null )
    begin
        exec GET_LICENSE @LIC_ID, @PAYMENT_DATE, @PREV_DEV_COUNT output, 
            @RESIDUAL_DAYS output, @PREV_SUBSCR_END output, 
            @PREV_PL_ID output, @PREV_SUBSCR_ID output, @PREV_PAY_SYS_TYPE output,
            @FREE_DEV_COUNT output
		
    end
    else begin
        set @PREV_DEV_COUNT = 0;
        set @RESIDUAL_DAYS = 0;
        set @PREV_SUBSCR_END = null;
        set @PREV_PL_ID = null;
    end

    if( @PL_ID is not null )
    begin
		exec GET_PRICE_LIST_ITEM @PL_ID, @PREV_DURATION output, @AMOUNT_NEW_PERIOD_FIRST output, @AMOUNT_NEW_PERIOD_OTHER output

		set @AMOUNT_TOTAL = @AMOUNT_TOTAL + @AMOUNT_NEW_PERIOD_FIRST
		
		set @AMOUNT_NEW_PERIOD_OTHER = 0.0;

    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].CALCULATE_SUPPORT_PAYMENT TO [HAWKWebSiteUser]
    AS [dbo];

