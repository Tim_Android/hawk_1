﻿create procedure CLEAN_CRASH_LOGS
    @MAX_DAY_COUNT int = 45,
    @MAX_ROW_COUNT int = 100
with encryption
as
begin
    set nocount on;

    delete 
    from CRASH_LOGS
    where datediff(day, CRASH_DATETIME, getdate()) > @MAX_DAY_COUNT

    delete from CRASH_LOGS
    where ID in ( select CL.ID
                  from CRASH_LOGS CL
                  where CL.AGENT_ID in ( select CL1.AGENT_ID
                                         from CRASH_LOGS CL1
                                         group by CL1.AGENT_ID
                                         having count(*) > @MAX_ROW_COUNT
                                       )
                        and CL.ID not in ( select top(@MAX_ROW_COUNT) CL2.ID
                                           from CRASH_LOGS CL2
                                           where CL2.AGENT_ID = CL.AGENT_ID
                                           order by CL2.ID desc
                                         )
                )
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[CLEAN_CRASH_LOGS] TO [HAWKPeriodicEventsUser]
    AS [dbo];

