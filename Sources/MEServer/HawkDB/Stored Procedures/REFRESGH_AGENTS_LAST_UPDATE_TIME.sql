﻿create procedure REFRESGH_AGENTS_LAST_UPDATE_TIME
    @AGENT_ID int = null
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        update DEVICES
        set
            IS_ACTIVED = 1,
            LAST_UPDATED = getdate()
        where ID = @AGENT_ID
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[REFRESGH_AGENTS_LAST_UPDATE_TIME] TO [HAWKWebServiceUser]
    AS [dbo];

