﻿create procedure GET_AGENTS_LAST_UPDATE_TIME 
    @AGENT_ID int = null,
    @LAST_UPDATED_TIME datetime output
with encryption
as
begin
    set nocount on;

	set @LAST_UPDATED_TIME = null
    if (@AGENT_ID is not null)
    begin
        select @LAST_UPDATED_TIME=LAST_UPDATED
        from DEVICES
        where ID = @AGENT_ID
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_AGENTS_LAST_UPDATE_TIME] TO [HAWKWebSiteUser]
    AS [dbo];

