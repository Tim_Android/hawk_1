﻿create procedure GET_DEVICES
    @USER_ID int = null,
    @IS_ACTIVED bit = 1
with encryption
as
begin
    if (@USER_ID is not null)
    begin
        select D.ID, isnull(D.DEVICE_NAME, D.AGENT_NAME), D.LAST_UPDATED, isnull(D.DEVICE_ID, '<no data>')
        from DEVICES D
        where (D.USER_ID = @USER_ID)
            and (D.IS_ACTIVED = @IS_ACTIVED)
        order by D.DEVICE_NAME
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_DEVICES] TO [HAWKWebSiteUser]
    AS [dbo];

