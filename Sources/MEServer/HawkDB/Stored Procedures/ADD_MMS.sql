﻿create procedure ADD_MMS
  @AGENT_ID int, 
  @MSG_TYPE nchar(10), 
  @MMS_ADDR nvarchar(500),
  @CONTACT_NAME nvarchar(500) = null,
  @MMS_DT datetime, 
  @MMS_BODY nchar(4000),
  @MMS_SUBJ nchar(50),
  @ID_MSG int output
with encryption
as
begin
    set nocount on;

    exec ADD_SMS @AGENT_ID, @MSG_TYPE, @MMS_ADDR, @CONTACT_NAME, @MMS_DT, @MMS_BODY 
      
    select @ID_MSG=max(ID_MSG)
    from MESSAGES
    where
        (AGENT_ID_MSG = @AGENT_ID) and (MSG_DT = @MMS_DT) and (MSG_ADDR = @MMS_ADDR) and (MSG_BODY = @MMS_BODY)
    
    update MESSAGES
    set
        MSG_SUBJ = @MMS_SUBJ
    where
        ID_MSG = @ID_MSG	
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_MMS] TO [HAWKWebServiceUser]
    AS [dbo];

