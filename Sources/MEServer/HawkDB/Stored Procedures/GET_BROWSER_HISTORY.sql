﻿create procedure GET_BROWSER_HISTORY
    @AGENT_ID int = null,
    @URL_FLTR BROWSER_URL_TYPE = null,
	@TITLE_FLTR BROWSER_TITLE_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @VISIT_COUNT_FLTR int = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if( @URL_FLTR is not null)
    begin
        set @URL_FLTR='%' + ltrim(@URL_FLTR)
        set @URL_FLTR=rtrim(@URL_FLTR) + '%'
    end

    if( @TITLE_FLTR is not null)
    begin
        set @TITLE_FLTR='%' + ltrim(@TITLE_FLTR)
        set @TITLE_FLTR=rtrim(@TITLE_FLTR) + '%'
    end

    declare @TZ_DEV_OFFSET datetime

    exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

    select distinct B.ID, B.AGENT_ID, B.TITLE, B.URL, B.VISITS, (B.DATE + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), B.DATE) as USER_DATE
    from BROWSER_HISTORY B
    where ((@AGENT_ID is not null) and (B.AGENT_ID = @AGENT_ID))
        and ((@TITLE_FLTR is null) or (B.TITLE like @TITLE_FLTR))
        and ((@BEG_DATE_FLTR is null) or ((B.DATE + @TZ_DEV_OFFSET) >= @BEG_DATE_FLTR))
        and ((@END_DATE_FLTR is null) or ((B.DATE + @TZ_DEV_OFFSET) <= @END_DATE_FLTR))
        and ((@URL_FLTR is null) or (B.URL like @URL_FLTR))
		and ((@VISIT_COUNT_FLTR is null) or (B.VISITS > @VISIT_COUNT_FLTR))
    order by DEV_DATE desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_BROWSER_HISTORY] TO [HAWKWebSiteUser]
    AS [dbo];