﻿create procedure GET_BANNED_LIST_PAGED
    @EMAIL EMAIL_TYPE = null,    
    @BAN_DATE_FROM datetime = null,   
    @BAN_DATE_TO datetime = null,
    
    @RecordsPerPage INT = NULL, -- if null or 0 - all records
    @Page INT = NULL output, -- if null or 0 - set first page
    @TotalRecords int output, -- this is output parameter
    @TotalPages int output-- this is output parameter

with encryption    
as
begin
    set nocount on;

	declare @Start int
	declare @End int

    select @TotalRecords = COUNT(ID)
    from BANNED_LIST
    where (
			(@EMAIL is null or EMAIL LIKE '%' + @EMAIL + '%') and
			(@BAN_DATE_FROM is null or BAN_DATE >= @BAN_DATE_FROM) and
			(@BAN_DATE_TO is null or BAN_DATE <= @BAN_DATE_TO)
			
		) 
	--DON`T FORGET MAKE CHANGES IN FILTER "WHERE" IN Cte
    
    EXEC PAGER @RecordsPerPage, @Page OUTPUT, @TotalRecords OUTPUT, @Start OUTPUT, @End OUTPUT,  @TotalPages OUTPUT
    
	;With Cte As
	(
        SELECT
            rn = ROW_NUMBER() Over(Order by BAN_DATE desc),
            ID,
            EMAIL,
            BAN_DATE,
            NOTE
                  
        from BANNED_LIST
        where (
                (@EMAIL is null or EMAIL LIKE '%' + @EMAIL + '%') and
				(@BAN_DATE_FROM is null or BAN_DATE >= @BAN_DATE_FROM) and
				(@BAN_DATE_TO is null or BAN_DATE <= @BAN_DATE_TO)
               ) 
     )
	SELECT 
            ID,
            EMAIL,
            BAN_DATE,
            NOTE
	FROM Cte
	WHERE rn between  @Start and @End
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_BANNED_LIST_PAGED] TO [HAWKWebSiteUser]
    AS [dbo];

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_BANNED_LIST_PAGED] TO [HAWKWebServiceUser]
    AS [dbo];
