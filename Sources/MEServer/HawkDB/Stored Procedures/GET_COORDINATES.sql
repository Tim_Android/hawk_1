﻿create procedure GET_COORDINATES
    @AGENT_ID int = null,
    @PROVIDER_MASK int = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if (@AGENT_ID is not null)
    begin 
        declare @TZ_DEV_OFFSET datetime

        exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

        select distinct (ARRIVAL_TIME + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), ARRIVAL_TIME) as USER_DATE, LATITUDE, LONGITUDE, ALTITUDE, ACCURACY, BEARING, SPEED,
            case PROVIDER 
                when 1 THEN 'GPS'
                when 2 THEN 'GSM/Wi-Fi'
                ELSE 'unknown' 
            end as PROVIDER
        from COORDINATES
        where (AGENT_ID = @AGENT_ID)
            and ((@PROVIDER_MASK is null) or (@PROVIDER_MASK = 0) or (PROVIDER & @PROVIDER_MASK > 0))
            and ((@BEG_DATE_FLTR is null) or ((ARRIVAL_TIME + @TZ_DEV_OFFSET) >= @BEG_DATE_FLTR))
            and ((@END_DATE_FLTR is null) or ((ARRIVAL_TIME + @TZ_DEV_OFFSET) <= @END_DATE_FLTR))
        order by DEV_DATE desc
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_COORDINATES] TO [HAWKWebSiteUser]
    AS [dbo];

