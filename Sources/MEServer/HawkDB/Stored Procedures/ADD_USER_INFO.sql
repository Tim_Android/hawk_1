﻿create procedure ADD_USER_INFO
    @ASP_USER_ID uniqueidentifier,
    @FIRST_NAME nvarchar(50),
    @LAST_NAME nvarchar(50),
    @EMAIL EMAIL_TYPE,
    @ID int output
with encryption
as
begin
    set nocount on;

    insert into USERS_INFO(ASP_NET_USER_ID, FIRST_NAME, LAST_NAME, EMAIL)
    values (@ASP_USER_ID, @FIRST_NAME, @LAST_NAME, @EMAIL)

	select @ID=CAST(SCOPE_IDENTITY() as int)      
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_USER_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_USER_INFO] TO [HAWKWebServiceUser]
    AS [dbo];

