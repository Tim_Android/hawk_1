﻿create procedure IS_DEACTIVATION_ALLOWED
    @ID_DEVICE int,
    @CAN_DEACTIVATE bit output
with encryption
as
    declare @LIC_ID int
    declare @LIC_ACT_DATE datetime
begin
    set nocount on;
 
    set @CAN_DEACTIVATE = 0;

    select @LIC_ID = LIC_ID, @LIC_ACT_DATE = LIC_ACT_DATE
    from DEVICES D
    where D.ID = @ID_DEVICE

    if( (@LIC_ID is not null) and (@LIC_ACT_DATE is not null) and (datediff(day, @LIC_ACT_DATE, getdate()) > 30) )
    begin
        set @CAN_DEACTIVATE = 1;
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[IS_DEACTIVATION_ALLOWED] TO [HAWKWebSiteUser]
    AS [dbo];

