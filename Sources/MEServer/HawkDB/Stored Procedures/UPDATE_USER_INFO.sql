﻿create procedure UPDATE_USER_INFO
    @ASP_USER_ID uniqueidentifier = null,
    @FIRST_NAME nvarchar(50) = null,
    @LAST_NAME nvarchar(50) = null,
    @EMAIL EMAIL_TYPE = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        if ((@FIRST_NAME is not null) and (@LAST_NAME is not null))
        begin
			update USERS_INFO
			set FIRST_NAME = @FIRST_NAME,
				LAST_NAME = @LAST_NAME
			where ASP_NET_USER_ID = @ASP_USER_ID
	    end
        if (@EMAIL is not null)
        begin
			update USERS_INFO
			set EMAIL = @EMAIL
			where ASP_NET_USER_ID = @ASP_USER_ID
        end
    end    
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_USER_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

