﻿create procedure CLEAN_USERS_DATA
    @MAX_DAY_COUNT int = 45
with encryption
as
begin
    set nocount on;

    delete from MESSAGES
    where datediff(day, MSG_DT, getutcdate()) > @MAX_DAY_COUNT

    delete from COORDINATES
    where datediff(day, ARRIVAL_TIME, getutcdate()) > @MAX_DAY_COUNT

    delete 
    from CALLS
    where datediff(day, DATE, getutcdate()) > @MAX_DAY_COUNT

    --exec DELETE_UNUSED_PHONES
    
    delete 
    from DEVICES
    where (LIC_ID is null) 
        and (datediff(day, CREATION_DATE, getdate()) > @MAX_DAY_COUNT)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[CLEAN_USERS_DATA] TO [HAWKPeriodicEventsUser]
    AS [dbo];

