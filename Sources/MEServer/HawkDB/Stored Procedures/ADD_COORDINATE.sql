﻿create procedure ADD_COORDINATE
    @AGENT_ID      int,
    @LATITUDE      double precision,
    @LONGITUDE     double precision,
    @ALTITUDE      double precision,
    @ACCURACY      float,
    @BEARING       float,
    @PROVIDER_NAME nchar(50),
    @SPEED         float,
    @ARRIVAL_TIME  datetime = null
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        if (@ARRIVAL_TIME is null)
        begin
            set @ARRIVAL_TIME = getdate()
        end

        declare @PROVIDER PROVIDER_TYPE
        set @PROVIDER = 0
        if (@PROVIDER_NAME like '%gps%')
        begin
            set @PROVIDER = 1
        end
        if (@PROVIDER_NAME like '%network%')
        begin
            set @PROVIDER = 2
        end        
        
        insert into COORDINATES(AGENT_ID, LATITUDE, LONGITUDE, ALTITUDE, 
            ACCURACY, BEARING, PROVIDER, SPEED, ARRIVAL_TIME)
        values(@AGENT_ID, @LATITUDE, @LONGITUDE, @ALTITUDE, @ACCURACY, @BEARING,
            @PROVIDER, @SPEED, @ARRIVAL_TIME)
    end        
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_COORDINATE] TO [HAWKWebServiceUser]
    AS [dbo];

