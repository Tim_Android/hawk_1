﻿create procedure ADD_MMS_ATTACH_TEMPL @MMS_ID int, @MIME_TYPE nchar(100), @AGENT_ATTACH_ID int 
with encryption
as
begin
    set nocount on;

    declare @ID_MIME int
    
    select @ID_MIME=ID_MIME 
    from MIME_TYPES
    where MIME_NAME=@MIME_TYPE
    
    if (@ID_MIME is NULL)
    begin
        insert into MIME_TYPES(MIME_NAME)
        values (@MIME_TYPE)

        select @ID_MIME=ID_MIME 
        from MIME_TYPES
        where MIME_NAME=@MIME_TYPE         
    end

    insert into MMS_ATTACH(MSG_ID, ID_MIME, AGENT_ATTACH_ID)
    values (@MMS_ID, @ID_MIME, @AGENT_ATTACH_ID)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_MMS_ATTACH_TEMPL] TO [HAWKWebServiceUser]
    AS [dbo];

