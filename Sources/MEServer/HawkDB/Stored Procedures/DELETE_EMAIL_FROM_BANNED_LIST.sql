﻿create procedure DELETE_EMAIL_FROM_BANNED_LIST
    @EMAIL EMAIL_TYPE = null
with encryption
as
begin  
    if (@EMAIL is not null)
    begin
         
        delete 
        from BANNED_LIST
        where EMAIL = @EMAIL
    end
end




GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_EMAIL_FROM_BANNED_LIST] TO [HAWKWebSiteUser]
    AS [dbo];
