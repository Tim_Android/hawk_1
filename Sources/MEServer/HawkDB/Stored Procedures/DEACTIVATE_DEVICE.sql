﻿create procedure DEACTIVATE_DEVICE
    @ID_DEVICE int
with encryption
as
    declare @CAN_DEACTIVATE bit
begin
    set nocount on;

    exec  IS_DEACTIVATION_ALLOWED @ID_DEVICE, @CAN_DEACTIVATE output

    if( @CAN_DEACTIVATE = 1 )
    begin
        update DEVICES
        set LIC_ID = null,
            LIC_ACT_DATE = null,
            CREATION_DATE = getdate()
        where ID = @ID_DEVICE
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DEACTIVATE_DEVICE] TO [HAWKWebSiteUser]
    AS [dbo];

