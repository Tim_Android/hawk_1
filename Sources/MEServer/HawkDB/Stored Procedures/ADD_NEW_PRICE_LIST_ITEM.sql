﻿create procedure ADD_NEW_PRICE_LIST_ITEM
    @LABEL NAME_TYPE,
    @DURATION smallint,
    @PAY_TYPE smallint,
    @PL_ID int output
with encryption    
as
begin
    set nocount on;

    insert into PRICE_LIST(LABEL, DURATION, PAY_TYPE)
    values (@LABEL, @DURATION, @PAY_TYPE)

    set @PL_ID=CAST(SCOPE_IDENTITY() as int)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_NEW_PRICE_LIST_ITEM] TO [HAWKWebSiteUser]
    AS [dbo];

