﻿create procedure ADD_AGENT 
    @USER_ID int, 
 	@AGENT_NAME AGENT_NAME_TYPE,
    @DEVICE_ID DEVICE_ID_TYPE = null,
    @OPER_SYS_TYPE int,
    @AGENT_ID int output
with encryption
as
begin
    set nocount on;

	insert into DEVICES(USER_ID, AGENT_NAME, DEVICE_NAME, DEVICE_ID, IS_ACTIVED, LAST_UPDATED, CREATION_DATE, OPER_SYS_TYPE)
    values(@USER_ID, @AGENT_NAME, SUBSTRING ( @AGENT_NAME , 1 , 20 ), @DEVICE_ID, 'FALSE', null, getdate(), @OPER_SYS_TYPE)
    
	set @AGENT_ID=CAST(SCOPE_IDENTITY() as int)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_AGENT] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ADD_AGENT] TO [HAWKWebSiteUser]
    AS [dbo];

