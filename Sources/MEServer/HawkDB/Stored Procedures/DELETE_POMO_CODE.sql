﻿create procedure DELETE_POMO_CODE
 @PROMO_CODE_ID int,
 @STATUS_ID int output
with encryption
as
begin
	declare @STATE bit
	 
	select @STATE = STATE
	from FullPromoCodeHistory
	where ID = @PROMO_CODE_ID
	
	if @STATE = 0 --code of "NotActivated" state
	begin
		delete from PromoCode
		where ID = @PROMO_CODE_ID
	end
	else begin
		select @STATUS_ID = 301 --the activation status of the selected Promo Code
        -- has changed
		return;
	end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_POMO_CODE] TO [HAWKWebSiteUser]
    AS [dbo];

