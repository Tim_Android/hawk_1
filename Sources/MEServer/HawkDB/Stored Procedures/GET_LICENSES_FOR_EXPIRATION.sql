﻿create procedure GET_LICENSES_FOR_EXPIRATION
with encryption
as
begin
    select UI.FIRST_NAME, 
	UI.LAST_NAME, 
	UI.EMAIL, 
	L.ID, 
	L.SUBSCR_END, 
	L.DEV_CNT,
	L.PAY_SYS_TYPE,
	L.PRICE_LIST_ID
    from LICENSES L
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where (SUBSCR_END <= getdate()) 
        and exists (select LIC_ID 
                    from DEVICES D
                    where D.LIC_ID = L.ID)
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_LICENSES_FOR_EXPIRATION] TO [HAWKPeriodicEventsUser]
    AS [dbo];

