﻿create procedure DELETE_CALLS
    @AGENT_ID int = null
with encryption
as
begin  
    if (@AGENT_ID is not null)
    begin
        delete 
        from CALLS
        where AGENT_ID = @AGENT_ID
        
        exec DELETE_UNUSED_PHONES
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[DELETE_CALLS] TO [HAWKWebSiteUser]
    AS [dbo];

