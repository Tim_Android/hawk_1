﻿create procedure ADD_LICENSE
    @USER_ID int,
    @PRICE_LIST_ID int,
    @SUBSCR_END datetime,
    @ADD_DEV_COUNT smallint,
    @PAY_SYS_TYPE smallint,
    @SUBSCR_ID SUBSCR_ID_TYPE,    
    @OUT_LIC_ID int output
with encryption
as
    declare @PAY_TYPE smallint
begin
	select @PAY_TYPE = PAY_TYPE
	from ACTIVE_PRICE_LIST
	where ID = @PRICE_LIST_ID

    if( @PAY_TYPE = 1 )  -- Signup Fee Payment (for first license)
    begin
        -- Please note that signup fee license is created as if user bought
        -- smallest period license. That's why we use search for price list
        -- if in different PAY_TYPE category. 
		select @PRICE_LIST_ID = APL.ID
		from ACTIVE_PRICE_LIST APL
		where APL.DURATION = ( select min(DURATION)
		                       from ACTIVE_PRICE_LIST
		                       where PAY_TYPE = 2 )
    end

	insert into LICENSES(USER_ID, PRICE_LIST_ID, SUBSCR_END, DEV_CNT, 
	    PAY_SYS_TYPE, SUBSCR_ID, CREATION_DATE)
	values(@USER_ID, @PRICE_LIST_ID, @SUBSCR_END, isnull(@ADD_DEV_COUNT, 0) + 1,
	    @PAY_SYS_TYPE, @SUBSCR_ID, getdate())

	set @OUT_LIC_ID=CAST(SCOPE_IDENTITY() as int);
end


