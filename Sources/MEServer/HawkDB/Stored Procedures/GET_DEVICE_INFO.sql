﻿create procedure GET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE output,
    @DEVICE_ID DEVICE_ID_TYPE output,
    @PHONE_NUMBER PHONE_TYPE output,
    @BRAND BRAND_TYPE output,
    @MODEL MODEL_TYPE output,
    @OS OS_TYPE output,
    @SDK SDK_TYPE output,
	@FIRMWARE_ID FIRMWARE_ID_TYPE output,
    @SVN_VERSION SVN_TYPE output,
    @TIME_ZONE TIME_ZONE_TYPE output,
    @DEVICE_NAME DEV_NAME_TYPE output,
    @NOTE NOTE_TYPE output,
    @LIC_ACT_DATE datetime output,
    @CAN_DEACTIVATE bit output,
    @LIC_ID int output,
    @OPER_SYS_TYPE int output
with encryption    
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin        
        select
            @DEVICE_NAME = isnull(D.DEVICE_NAME, D.AGENT_NAME),
            @AGENT_NAME = D.AGENT_NAME,
            @DEVICE_ID = isnull(D.DEVICE_ID, 'unknown'),
            @PHONE_NUMBER = isnull(D.PHONE_NUMBER, 'unknown'),
            @BRAND = isnull(D.BRAND, 'unknown'),
            @MODEL = isnull(D.MODEL, 'unknown'),
            @OS = isnull(D.OS, 'unknown'),
            @SDK = isnull(D.SDK, 'unknown'),
            @FIRMWARE_ID = isnull(D.FIRMWARE_ID, 'unknown'),
            @SVN_VERSION = isnull(D.SVN_VERSION, 'unknown'),
            @TIME_ZONE = isnull(D.TIME_ZONE, 'unknown'),
            @NOTE = isnull(D.NOTE, ''),
            @LIC_ACT_DATE = LIC_ACT_DATE,
            @LIC_ID = LIC_ID,
            @OPER_SYS_TYPE = OPER_SYS_TYPE 
        from DEVICES D
        where D.ID = @AGENT_ID
    end
    
    exec  IS_DEACTIVATION_ALLOWED @AGENT_ID, @CAN_DEACTIVATE output    
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_DEVICE_INFO] TO [HAWKWebSiteUser]
    AS [dbo];

