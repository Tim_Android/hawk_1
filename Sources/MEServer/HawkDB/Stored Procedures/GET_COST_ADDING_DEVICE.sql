﻿create procedure GET_COST_ADDING_DEVICE
    @RESIDUAL_DAYS int,
    @COST money output,
    @PRICE_LIST_ID int output
with encryption
as
    declare @DURATION int
    declare @NEXT_PRICE_LIST_ID int
    declare @NEXT_DURATION int
    declare @NEXT_COST money 
    declare @CURSOR cursor    
begin
    set nocount on;

    set @CURSOR  = cursor SCROLL for    
		select ID, PRICE_OTHER, DURATION
		from ACTIVE_PRICE_LIST
		where (PAY_TYPE=2) and (PRICE_OTHER is not null)
		order by DURATION
    open @CURSOR

    fetch next from @CURSOR into @PRICE_LIST_ID, @COST, @DURATION

	while @@FETCH_STATUS = 0
	begin
		fetch next from @CURSOR into @NEXT_PRICE_LIST_ID, @NEXT_COST, @NEXT_DURATION
		
		if( @NEXT_DURATION > @RESIDUAL_DAYS )
        begin
            break
        end
        else begin
            set @PRICE_LIST_ID = @NEXT_PRICE_LIST_ID;
			set @COST = @NEXT_COST;
			set @DURATION = @NEXT_DURATION;
        end
	end
	close @CURSOR

    set @COST = isnull(round(@COST * @RESIDUAL_DAYS / @DURATION, 2), 0.0);
end


