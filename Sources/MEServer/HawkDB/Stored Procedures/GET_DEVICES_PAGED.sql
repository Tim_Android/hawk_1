﻿create procedure GET_DEVICES_PAGED
    @AGENT_ID int = null,
    @USER_ID INT = null,
    @IS_ACTIVED bit = null,
    @LAST_UPDATED_FROM datetime = null,
    @LAST_UPDATED_TO datetime = null,
    @DEVICE_ID DEVICE_ID_TYPE = null,
    @AGENT_NAME AGENT_NAME_TYPE = null,
    @PHONE_NUMBER PHONE_TYPE = null,
    @BRAND BRAND_TYPE = null,
    @MODEL MODEL_TYPE = null,
    @OS OS_TYPE = null,
    @SDK SDK_TYPE = null,
    @FIRMWARE_ID FIRMWARE_ID_TYPE = null,
    @SVN_VERSION SVN_TYPE = null,
    @TIME_ZONE TIME_ZONE_TYPE = null,
    @DEVICE_NAME DEV_NAME_TYPE = null,
    @NOTE NOTE_TYPE = null,
    @LIC_ACT_DATE_FROM datetime = null,
    @LIC_ACT_DATE_TO datetime = null,
    @LIC_ID int = null,
    @OPER_SYS_TYPE int = null,
    @CREATION_DATE_FROM datetime = null,
    @CREATION_DATE_TO datetime = null,
    @SHOW_UNASSIGNED_AGENTS bit = 1,
    
    @RecordsPerPage INT = NULL, -- if null or 0 - all records
    @Page INT = NULL output, -- if null or 0 - set first page
    @TotalRecords int output, -- this is output parameter
    @TotalPages int output-- this is output parameter

with encryption    
as
begin
    set nocount on;

	declare @Start int
	declare @End int

    select @TotalRecords = COUNT(ID)
    from DEVICES
    where (
			(@AGENT_ID is null or ID = @AGENT_ID) and
			(@USER_ID is null or USER_ID = @USER_ID) and
			(@IS_ACTIVED is null or IS_ACTIVED = @IS_ACTIVED) and
			(@LAST_UPDATED_FROM is null or LAST_UPDATED >= @LAST_UPDATED_FROM) and
			(@LAST_UPDATED_TO is null or LAST_UPDATED <= @LAST_UPDATED_TO) and
			(@DEVICE_ID is null or DEVICE_ID = @DEVICE_ID) and
			(@AGENT_NAME is null or AGENT_NAME = @AGENT_NAME) and
			(@PHONE_NUMBER is null or PHONE_NUMBER = @PHONE_NUMBER) and
			(@BRAND is null or BRAND = @BRAND) and
			(@MODEL is null or MODEL = @MODEL) and
			(@OS is null or OS = @OS) and
			(@SDK is null or SDK = @SDK) and
			(@FIRMWARE_ID is null or FIRMWARE_ID = @FIRMWARE_ID) and
			(@SVN_VERSION is null or SVN_VERSION = @SVN_VERSION) and
			(@TIME_ZONE is null or TIME_ZONE = @TIME_ZONE) and
			(@DEVICE_NAME is null or DEVICE_NAME = @DEVICE_NAME) and
			(@NOTE is null or NOTE = @NOTE) and
			(@LIC_ID is null or LIC_ID = @LIC_ID) and
			(@LIC_ACT_DATE_FROM is null or LIC_ACT_DATE >= @LIC_ACT_DATE_FROM) and
			(@LIC_ACT_DATE_TO is null or LIC_ACT_DATE <= @LIC_ACT_DATE_TO) and
			(@OPER_SYS_TYPE is null or OPER_SYS_TYPE = @OPER_SYS_TYPE) and
			(@CREATION_DATE_FROM is null or CREATION_DATE >= @CREATION_DATE_FROM) and
			(@CREATION_DATE_TO is null or CREATION_DATE <= @CREATION_DATE_TO) and
			(@SHOW_UNASSIGNED_AGENTS = 1 or DEVICE_ID is not null)
		) 
	--DON`T FORGET MAKE CHANGES IN FILTER "WHERE" IN Cte
    
    EXEC PAGER @RecordsPerPage, @Page OUTPUT, @TotalRecords OUTPUT, @Start OUTPUT, @End OUTPUT,  @TotalPages OUTPUT
    
	;With Cte As
	(
        SELECT
            rn = ROW_NUMBER() Over(Order by CREATION_DATE desc),
            ID,
            USER_ID,
            AGENT_NAME,
            IS_ACTIVED,
            LAST_UPDATED,
            DEVICE_ID,
            PHONE_NUMBER,
            BRAND,
            MODEL,
            OS,
            SDK,
            FIRMWARE_ID,
            SVN_VERSION, 
            TIME_ZONE,
            DEVICE_NAME,
            NOTE, LIC_ID,
            LIC_ACT_DATE,
            CREATION_DATE,
            OPER_SYS_TYPE         
        from DEVICES
        where (
                (@AGENT_ID is null or ID = @AGENT_ID) and
                (@USER_ID is null or USER_ID = @USER_ID) and
                (@IS_ACTIVED is null or IS_ACTIVED = @IS_ACTIVED) and
                (@LAST_UPDATED_FROM is null or LAST_UPDATED >= @LAST_UPDATED_FROM) and
                (@LAST_UPDATED_TO is null or LAST_UPDATED <= @LAST_UPDATED_TO) and
                (@DEVICE_ID is null or DEVICE_ID = @DEVICE_ID) and
                (@AGENT_NAME is null or AGENT_NAME = @AGENT_NAME) and
                (@PHONE_NUMBER is null or PHONE_NUMBER = @PHONE_NUMBER) and
                (@BRAND is null or BRAND = @BRAND) and
                (@MODEL is null or MODEL = @MODEL) and
                (@OS is null or OS = @OS) and
                (@SDK is null or SDK = @SDK) and
                (@FIRMWARE_ID is null or FIRMWARE_ID = @FIRMWARE_ID) and
                (@SVN_VERSION is null or SVN_VERSION = @SVN_VERSION) and
                (@TIME_ZONE is null or TIME_ZONE = @TIME_ZONE) and
                (@DEVICE_NAME is null or DEVICE_NAME = @DEVICE_NAME) and
                (@NOTE is null or NOTE = @NOTE) and
				(@LIC_ID is null or LIC_ID = @LIC_ID) and
                (@LIC_ACT_DATE_FROM is null or LIC_ACT_DATE >= @LIC_ACT_DATE_FROM) and
                (@LIC_ACT_DATE_TO is null or LIC_ACT_DATE <= @LIC_ACT_DATE_TO) and
                (@OPER_SYS_TYPE is null or OPER_SYS_TYPE = @OPER_SYS_TYPE) and
                (@CREATION_DATE_FROM is null or CREATION_DATE >= @CREATION_DATE_FROM) and
				(@CREATION_DATE_TO is null or CREATION_DATE <= @CREATION_DATE_TO) and
				(@SHOW_UNASSIGNED_AGENTS = 1 or DEVICE_ID is not null)
               ) 
     )
	SELECT 
            ID,
            USER_ID,
            AGENT_NAME,
            IS_ACTIVED,
            LAST_UPDATED,
            DEVICE_ID,
            PHONE_NUMBER,
            BRAND,
            MODEL,
            OS,
            SDK,
            FIRMWARE_ID,
            SVN_VERSION, 
            TIME_ZONE,
            DEVICE_NAME,
            NOTE, LIC_ID, 
            LIC_ACT_DATE,
            CREATION_DATE,
            OPER_SYS_TYPE
	FROM Cte
	WHERE rn between  @Start and @End
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_DEVICES_PAGED] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_DEVICES_PAGED] TO [HAWKWebSiteUser]
    AS [dbo];

