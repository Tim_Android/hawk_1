﻿create procedure IS_ACTIVATION_ALLOWED
    @LIC_ID int,
    @CAN_ACTIVATE bit output
with encryption
as
    declare @DEVICE_COUNT int
    declare @ACT_DEV_COUNT int
begin
    set nocount on;
 
    set @CAN_ACTIVATE = 0;
 
    select @DEVICE_COUNT = DEV_CNT
    from LICENSES
    where ID = @LIC_ID
 
    select @ACT_DEV_COUNT = isnull(count(ID), 0)
    from DEVICES
    where LIC_ID = @LIC_ID

    if( @DEVICE_COUNT - @ACT_DEV_COUNT > 0 )
    begin
        set @CAN_ACTIVATE = 1;
    end
end


