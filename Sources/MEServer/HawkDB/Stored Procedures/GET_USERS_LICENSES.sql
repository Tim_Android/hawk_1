﻿create procedure GET_USERS_LICENSES
    @USER_ID int = null,
    @EXPIRE_DAYS int = null
with encryption
as
begin
    select UI.FIRST_NAME, UI.LAST_NAME, UI.EMAIL, L.CREATION_DATE, L.ID, 
        L.PRICE_LIST_ID, L.SUBSCR_END, L.DEV_CNT, L.PAY_SYS_TYPE, 
        L.SUBSCR_ID, (L.DEV_CNT - (select count(D.ID) 
                                   from DEVICES D
                                   where D.LIC_ID = L.ID)) as FREE_DEV_COUNT, L.USER_ID
    from LICENSES L
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where (((L.USER_ID = @USER_ID) or (@USER_ID is null))
        and ((datediff(day, getdate(), L.SUBSCR_END) <= @EXPIRE_DAYS) or (@EXPIRE_DAYS is null)))
    group by L.ID, L.PRICE_LIST_ID, L.SUBSCR_END, L.DEV_CNT, L.PAY_SYS_TYPE, 
        L.SUBSCR_ID, UI.FIRST_NAME, UI.LAST_NAME, UI.EMAIL, L.CREATION_DATE, L.USER_ID
    order by L.SUBSCR_END desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USERS_LICENSES] TO [HAWKWebSiteUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USERS_LICENSES] TO [HAWKPeriodicEventsUser]
    AS [dbo];

