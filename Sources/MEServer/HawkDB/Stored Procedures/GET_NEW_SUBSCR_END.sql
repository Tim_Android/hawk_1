﻿create procedure GET_NEW_SUBSCR_END
    @PL_ID int,
    @LIC_ID int = null,
    @PAYMENT_DATE datetime = null, 
    @NEW_SUBSCR_END datetime output
with encryption    
as
    declare @PREV_SUBSCR_END datetime
    declare @PREV_PL_ID int
    declare @DURATION int
begin
    if (@PAYMENT_DATE is null)
    begin
        set @PAYMENT_DATE = getdate()
    end
    
    set @PREV_SUBSCR_END = @PAYMENT_DATE;    

	if( @LIC_ID is not null )  -- Get previous SUBSCR_END
	begin
		select @PREV_SUBSCR_END = L.SUBSCR_END, @PREV_PL_ID = PRICE_LIST_ID
		from LICENSES L
		where L.ID = @LIC_ID
	end
	
    set @NEW_SUBSCR_END = @PREV_SUBSCR_END;
	
    if (@PL_ID is null)
    begin
		return
	end

    if (@PREV_SUBSCR_END < @PAYMENT_DATE)
    begin
        set @PREV_SUBSCR_END = @PAYMENT_DATE  -- fail to pay on time
    end

    select @DURATION = DURATION
    from ACTIVE_PRICE_LIST
    where ID = @PL_ID

    set @NEW_SUBSCR_END = dateadd(day, @DURATION, @PREV_SUBSCR_END);
end


