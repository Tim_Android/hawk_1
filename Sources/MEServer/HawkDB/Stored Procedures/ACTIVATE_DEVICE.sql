﻿create procedure ACTIVATE_DEVICE
    @ID_DEVICE int,
    @ID_LICENSE int,
    @LIC_ACT_DATE datetime
with encryption    
as
    declare @CAN_ACTIVATE bit
begin
    set nocount on;
    
    exec  IS_ACTIVATION_ALLOWED @ID_LICENSE, @CAN_ACTIVATE output

    if( @CAN_ACTIVATE = 1 )
    begin    
        update DEVICES
        set LIC_ID = @ID_LICENSE,
            LIC_ACT_DATE = isnull(@LIC_ACT_DATE, getdate())    
        where ID = @ID_DEVICE
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ACTIVATE_DEVICE] TO [HAWKWebSiteUser]
    AS [dbo];

