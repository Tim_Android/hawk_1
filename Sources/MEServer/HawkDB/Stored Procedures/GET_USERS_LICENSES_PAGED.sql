﻿create procedure GET_USERS_LICENSES_PAGED
	@RECORDS_PER_PAGE int = null,
--search
	@ID int = null,
    @USER_ID int = null,    
    @SUBSCRIPTION_END_FROM datetime = null,
    @SUBSCRIPTION_END_TO datetime = null,
    @DEVICE_CNT smallint = null,
    @PAY_SYS_TYPE int = null,
    @SUBSCR_ID nvarchar(32)= null,
    @CREATION_DATE_FROM datetime = null,
    @CREATION_DATE_TO datetime = null,
    --end search
    
    @PAGE int output,
	@TOTAL_PAGES int output,
    @TOTAL_RECORDS int output
with encryption
as
begin
	set nocount on;

	declare @Start int
	declare @End int

    select @TOTAL_RECORDS = COUNT(L.ID)
   
    from LICENSES L
    join PRICE_LIST on L.PRICE_LIST_ID = PRICE_LIST.ID
    where
        (
			(@ID is null or L.ID = @ID) and
			(@USER_ID is null or USER_ID = @USER_ID) and                
			(@SUBSCRIPTION_END_FROM is null or SUBSCR_END >= @SUBSCRIPTION_END_FROM) and 
			(@SUBSCRIPTION_END_TO is null or SUBSCR_END <= @SUBSCRIPTION_END_TO) and 
			(@DEVICE_CNT is null or DEV_CNT = @DEVICE_CNT) and 
			(@PAY_SYS_TYPE is null or PAY_SYS_TYPE = @PAY_SYS_TYPE) and 
			(@SUBSCR_ID is null or SUBSCR_ID = @SUBSCR_ID) and
			(@CREATION_DATE_FROM is null or CREATION_DATE >= @CREATION_DATE_FROM) and 
			(@CREATION_DATE_TO is null or CREATION_DATE <= @CREATION_DATE_TO)
		)

    	 
    EXEC PAGER @RECORDS_PER_PAGE, @Page OUTPUT, @TOTAL_RECORDS OUTPUT, @Start OUTPUT, @End OUTPUT,  @TOTAL_PAGES OUTPUT
	  
	  
	;With Cte As
    (select 
			rn = row_number()Over(Order by (CREATION_DATE) DESC), 
			L.USER_ID,
			L.PRICE_LIST_ID,
			L.SUBSCR_END,
			L.DEV_CNT,
			L.PAY_SYS_TYPE,
			L.SUBSCR_ID,
			L.CREATION_DATE,
			L.ID,
			LABEL as PERIOD_NAME
			
	 
       
    from LICENSES L
    join PRICE_LIST on L.PRICE_LIST_ID = PRICE_LIST.ID
    where
        (
			(@ID is null or L.ID = @ID) and
			(@USER_ID is null or USER_ID = @USER_ID) and                
			(@SUBSCRIPTION_END_FROM is null or SUBSCR_END >= @SUBSCRIPTION_END_FROM) and 
			(@SUBSCRIPTION_END_TO is null or SUBSCR_END <= @SUBSCRIPTION_END_TO) and 
			(@DEVICE_CNT is null or DEV_CNT = @DEVICE_CNT) and 
			(@PAY_SYS_TYPE is null or PAY_SYS_TYPE = @PAY_SYS_TYPE) and 
			(@SUBSCR_ID is null or SUBSCR_ID = @SUBSCR_ID) and
			(@CREATION_DATE_FROM is null or CREATION_DATE >= @CREATION_DATE_FROM) and 
			(@CREATION_DATE_TO is null or CREATION_DATE <= @CREATION_DATE_TO)
		)
    )--With Cte As
    
	 select  
			USER_ID,
			PRICE_LIST_ID,
			SUBSCR_END,
			DEV_CNT,
			PAY_SYS_TYPE,
			SUBSCR_ID,
			CREATION_DATE,
			ID,
			PERIOD_NAME
	from Cte
	where rn between  @START and @END
end   

GO


GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USERS_LICENSES_PAGED] TO [HAWKWebSiteUser]
    AS [dbo];


GO