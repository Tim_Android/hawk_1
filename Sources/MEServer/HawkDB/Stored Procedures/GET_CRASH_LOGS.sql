﻿create procedure GET_CRASH_LOGS
    @AGENT_ID int = null
with encryption
as
begin
    select distinct CRASH_DATETIME, CRASH_DATA
    from CRASH_LOGS CL
    where (@AGENT_ID is not null) and (CL.AGENT_ID = @AGENT_ID)
    order by CRASH_DATETIME desc
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_CRASH_LOGS] TO [HAWKWebSiteUser]
    AS [dbo];

