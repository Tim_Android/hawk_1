﻿create procedure PAGER 
    @RecordsPerPage INT = NULL, -- if null or 0 - all records
	
	--SHOULD BE SET PARAMETERS
    @Page INT = NULL OUTPUT, -- if null or 0 - set first page
    @TotalRecords INT OUTPUT, -- this is output parameter

    -- NOT SHOULD BE SET PARAMETERS
    @Start INT OUTPUT, 
    @End INT OUTPUT,
    @TotalPages INT OUTPUT-- this is output parameter
with encryption   
as
begin
    set nocount on;

    IF (@RecordsPerPage IS NULL)
    BEGIN
        IF (@TotalRecords > 0)
           BEGIN
               SELECT @RecordsPerPage = @TotalRecords;
           END
        ELSE
           BEGIN -- if no records found, then we assume that we have 100 record per page to avoid division by zero
               SELECT @RecordsPerPage = 100;
           END
    END

    SET @TotalPages = (@TotalRecords + @RecordsPerPage - 1) / @RecordsPerPage;

    IF (@Page > @TotalPages)
    BEGIN
        SET @Page = @TotalPages;
    END

    IF @Page < 1 OR @Page IS NULL	
    BEGIN
        SET @Page = 1
    END

	SET @Start = ((@Page - 1) * @RecordsPerPage)+1;
	SET @End = @Page * @RecordsPerPage;
	--END PAGER
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[PAGER] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[PAGER] TO [HAWKWebSiteUser]
    AS [dbo];

