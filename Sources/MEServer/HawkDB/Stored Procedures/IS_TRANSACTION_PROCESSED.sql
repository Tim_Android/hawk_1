﻿create procedure IS_TRANSACTION_PROCESSED
    @TRANSACTION_ID SUBSCR_ID_TYPE,
    @PROCESSED bit output
with encryption
as
    declare @TRANSACTION_COUNT int
begin
    set nocount on;
 
    set @PROCESSED = 0;

    select @TRANSACTION_COUNT = count(*) 
    from PAYMENTS_HISTORY
    where TRANS_ID = @TRANSACTION_ID

    if( @TRANSACTION_COUNT > 0 )
    begin
        set @PROCESSED = 1;
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[IS_TRANSACTION_PROCESSED] TO [HAWKWebSiteUser]
    AS [dbo];

