﻿create procedure GET_USER_LIST_PAGED
	@RECORDS_PER_PAGE int = null,
    @LICENSE_CHECK_PERIOD int,
    
	--search
    @ID INT = null,
	@ASP_NET_USER_ID uniqueidentifier = null,
    @REGISTERED_FROM datetime = null,
    @REGISTERED_TO datetime = null,
	@EMAIL EMAIL_TYPE = null,
    @EMAIL_IS_VERIFIED bit = null,
    @HAS_PURCHASED_LICENSES bit = null,
    @HAS_NOT_UPDATED_SUBSCR bit = null,
    @IS_BANNED bit = null,
	--end search
			
	@PAGE int output,
	@TOTAL_PAGES int output,
    @TOTAL_RECORDS int output
with encryption
as
begin
    declare @START int
    declare @END int
    declare @CUR_DATE datetime
    
    
    select @CUR_DATE = GETUTCDATE();
    
	--show all records - should set @RECORDS_PER_PAGE and @PAGE, another values can be null
	;With Cte As
	(
        select  UI.ID,
                UI.ASP_NET_USER_ID,
                CreateDate as REGISTRATION_DATE,
                FIRST_NAME,
                LAST_NAME,
                UI.EMAIL,
                EMAIL_IS_VERIFIED,
                HAS_PURCHASED_LICENSES,
                HAS_NOT_UPDATED_SUBSCR,
                IS_BANNED
        from USERS_INFO UI
           join aspnet_Membership on UI.ASP_NET_USER_ID = aspnet_Membership.UserId
           OUTER APPLY
		   (
			   SELECT
			   (
				   SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
				   FROM aspnet_UsersInRoles
				       JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId
				   WHERE  aspnet_UsersInRoles.UserId = UI.ASP_NET_USER_ID AND aspnet_Roles.LoweredRoleName = 'confirmed'
			   ) AS EMAIL_IS_VERIFIED,
			   (
			       SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
			       FROM LICENSES 
			           JOIN USERS_INFO ON LICENSES.USER_ID = USERS_INFO.ID
			           JOIN aspnet_Membership ON USERS_INFO.ASP_NET_USER_ID = aspnet_Membership.UserId
			       WHERE aspnet_Membership.UserId = UI.ASP_NET_USER_ID AND LICENSES.PAY_SYS_TYPE IN (1, 2) -- we want to get licenses that have been bought with Authorize.net or PayPal
			   ) AS HAS_PURCHASED_LICENSES,
			   (
				   SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
				   FROM LICENSES
				      JOIN USERS_INFO ON LICENSES.USER_ID = USERS_INFO.ID
				   WHERE SUBSCR_END < DATEADD(DAY, @LICENSE_CHECK_PERIOD, @CUR_DATE) AND SUBSCR_END > @CUR_DATE
				      AND SUBSCR_ID IS NOT NULL AND USERS_INFO.ASP_NET_USER_ID = UI.ASP_NET_USER_ID
			   ) AS HAS_NOT_UPDATED_SUBSCR,
			   (
				   SELECT CASE COUNT(*) 
								WHEN 0 THEN 0
								ELSE 1
							  END 
							  from BANNED_LIST where BANNED_LIST.EMAIL = UI.EMAIL
			   
			   ) AS IS_BANNED
			  
		    ) AccountInfo
        where(
                (@ID is null or ID = @ID) and
                (@ASP_NET_USER_ID is null or ASP_NET_USER_ID = @ASP_NET_USER_ID) and
                (@EMAIL is null or UI.EMAIL LIKE '%' + @EMAIL + '%') and 
                (@REGISTERED_FROM is null or CreateDate >= @REGISTERED_FROM) and 
                (@REGISTERED_TO is null or CreateDate <= @REGISTERED_TO) and 
                (@EMAIL_IS_VERIFIED is null or EMAIL_IS_VERIFIED = @EMAIL_IS_VERIFIED) and 
                (@HAS_PURCHASED_LICENSES is null or HAS_PURCHASED_LICENSES = @HAS_PURCHASED_LICENSES) and 
                (@HAS_NOT_UPDATED_SUBSCR is null or HAS_NOT_UPDATED_SUBSCR = @HAS_NOT_UPDATED_SUBSCR) and
                (@IS_BANNED is null or IS_BANNED = @IS_BANNED)
              )
	)--With Cte As		
    	
	select @TOTAL_RECORDS = count(ID)
	from Cte;

	 EXEC PAGER @RECORDS_PER_PAGE, @Page OUTPUT, @TOTAL_RECORDS OUTPUT, @Start OUTPUT, @End OUTPUT,  @TOTAL_PAGES OUTPUT

	;With Cte As
	(
		select
			rn = row_number() Over(Order by (CreateDate) DESC), 
			UI.ID,
            UI.ASP_NET_USER_ID,
            CreateDate as REGISTRATION_DATE,
            FIRST_NAME,
            LAST_NAME,
            UI.EMAIL,
            EMAIL_IS_VERIFIED,
            HAS_PURCHASED_LICENSES,
            HAS_NOT_UPDATED_SUBSCR,
            IS_BANNED
        from USERS_INFO UI
           join aspnet_Membership on UI.ASP_NET_USER_ID = aspnet_Membership.UserId
           OUTER APPLY
		   (
			   SELECT
			   (
				   SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
				   FROM aspnet_UsersInRoles
				       JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId
				   WHERE  aspnet_UsersInRoles.UserId = UI.ASP_NET_USER_ID AND aspnet_Roles.LoweredRoleName = 'confirmed'
			   ) AS EMAIL_IS_VERIFIED,
			   (
			       SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
			       FROM LICENSES 
			           JOIN USERS_INFO ON LICENSES.USER_ID = USERS_INFO.ID
			           JOIN aspnet_Membership ON USERS_INFO.ASP_NET_USER_ID = aspnet_Membership.UserId
			       WHERE aspnet_Membership.UserId = UI.ASP_NET_USER_ID AND LICENSES.PAY_SYS_TYPE IN (1, 2) -- we want to get licenses that have been bought with Authorize.net or PayPal
			   ) AS HAS_PURCHASED_LICENSES,
			   (
				   SELECT CASE COUNT(*) 
			                WHEN 0 THEN 0
			                ELSE 1
			              END 
				   FROM LICENSES
				      JOIN USERS_INFO ON LICENSES.USER_ID = USERS_INFO.ID
				   WHERE SUBSCR_END < DATEADD(DAY, @LICENSE_CHECK_PERIOD, @CUR_DATE) AND SUBSCR_END > @CUR_DATE
				      AND SUBSCR_ID IS NOT NULL AND USERS_INFO.ASP_NET_USER_ID = UI.ASP_NET_USER_ID
			   ) AS HAS_NOT_UPDATED_SUBSCR,
			   (
				   SELECT CASE COUNT(*) 
								WHEN 0 THEN 0
								ELSE 1
							  END 
							  from BANNED_LIST where BANNED_LIST.EMAIL = UI.EMAIL
			  
			   ) AS IS_BANNED
		    ) AccountInfo
        where(
                (@ID is null or ID = @ID) and
                (@ASP_NET_USER_ID is null or ASP_NET_USER_ID = @ASP_NET_USER_ID) and
                (@EMAIL is null or UI.EMAIL LIKE '%' + @EMAIL + '%') and 
                (@REGISTERED_FROM is null or CreateDate >= @REGISTERED_FROM) and 
                (@REGISTERED_TO is null or CreateDate <= @REGISTERED_TO) and 
                (@EMAIL_IS_VERIFIED is null or EMAIL_IS_VERIFIED = @EMAIL_IS_VERIFIED) and 
                (@HAS_PURCHASED_LICENSES is null or HAS_PURCHASED_LICENSES = @HAS_PURCHASED_LICENSES) and 
                (@HAS_NOT_UPDATED_SUBSCR is null or HAS_NOT_UPDATED_SUBSCR = @HAS_NOT_UPDATED_SUBSCR) and
                (@IS_BANNED is null or IS_BANNED = @IS_BANNED)
              )
	)--With Cte As
	
	select  ID,
            ASP_NET_USER_ID,
            REGISTRATION_DATE,
            FIRST_NAME,
            LAST_NAME,
            EMAIL,
            EMAIL_IS_VERIFIED,
            HAS_PURCHASED_LICENSES,
            HAS_NOT_UPDATED_SUBSCR,
            IS_BANNED
	from Cte
	where rn between  @START and @END
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USER_LIST_PAGED] TO [HAWKWebSiteUser]
    AS [dbo];