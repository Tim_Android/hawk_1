﻿create procedure GET_PHONE_ID
    @DEVICE_ID int,
    @NUMBER PHONE_TYPE,
    @PHONE_NAME PNAME_TYPE,    
    @PHONE_ID int output
with encryption
as
begin
    set nocount on;

    if( @NUMBER = '-1' )
    begin
        set @NUMBER = 'Unknown'
    end
    if( @NUMBER = '-2' )
    begin
        set @NUMBER = 'Private'
    end
    if( @NUMBER = '-3' )
    begin
        set @NUMBER = 'Payphone'
    end

    set @PHONE_ID = null
    if (@NUMBER is not null)
    begin
        if (@PHONE_NAME is not null)
        begin
            select @PHONE_ID=P.ID
            from PHONES P
            where P.DEVICE_ID = @DEVICE_ID 
				  and (P.NUMBER = @NUMBER) 
				  and (P.PHONE_NAME = @PHONE_NAME)

			if(@PHONE_ID is null)
			begin
				select @PHONE_ID=P.ID
				from PHONES P
				where (P.NUMBER = @NUMBER) 
					  and (P.PHONE_NAME = @PHONE_NAME)
			end
        end
        else begin
            select @PHONE_ID=P.ID
            from PHONES P
            where P.DEVICE_ID = @DEVICE_ID 
				  and (P.NUMBER = @NUMBER) 
				  and (P.PHONE_NAME is not null)
			
			if(@PHONE_ID is null)
			begin
				select @PHONE_ID=P.ID
				from PHONES P
				where (P.NUMBER = @NUMBER) 
					  and (P.PHONE_NAME is null)
			end
        end

        if (@PHONE_ID is null)
        begin
    	    insert into PHONES(DEVICE_ID, NUMBER, PHONE_NAME)
            values(@DEVICE_ID, @NUMBER, @PHONE_NAME)

            set @PHONE_ID = CAST(SCOPE_IDENTITY() as int)
        end
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_PHONE_ID] TO [HAWKWebSiteUser]
    AS [dbo];

