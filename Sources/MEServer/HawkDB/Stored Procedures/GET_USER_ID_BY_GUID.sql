﻿create procedure GET_USER_ID_BY_GUID
    @ASP_USER_ID uniqueidentifier = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        select ID
        from USERS_INFO
        where ASP_NET_USER_ID = @ASP_USER_ID
    end
end



GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USER_ID_BY_GUID] TO [HAWKWebServiceUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_USER_ID_BY_GUID] TO [HAWKWebSiteUser]
    AS [dbo];

