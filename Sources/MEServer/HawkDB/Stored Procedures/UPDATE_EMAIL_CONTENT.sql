﻿create procedure UPDATE_EMAIL_CONTENT
    @EMAIL_TYPE int,
    @EMAIL_TEXT_HTML nvarchar(4000)
as
begin
    update EMAIL
    set EMAIL_TEXT_HTML = @EMAIL_TEXT_HTML
    where EMAIL_TYPE = @EMAIL_TYPE
end

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UPDATE_EMAIL_CONTENT] TO [HAWKWebSiteUser]
    AS [dbo];

