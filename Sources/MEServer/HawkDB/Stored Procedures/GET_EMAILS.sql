﻿create procedure GET_EMAILS
    @EMAIL_TYPE int = null
as
begin
    select ID, EMAIL_NAME, EMAIL_TYPE, EMAIL_TEXT_HTML, ALLOWED_VARS
    from EMAIL
    where (@EMAIL_TYPE is null) or (EMAIL_TYPE = @EMAIL_TYPE)
end

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_EMAILS] TO [HAWKWebSiteUser]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_EMAILS] TO [HAWKPeriodicEventsUser]
    AS [dbo];

GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GET_EMAILS] TO [HAWKWebServiceUser]
    AS [dbo];

