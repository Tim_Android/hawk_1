﻿/*==============================================================*/
/* View: ACTIVE_PRICE_LIST                                      */
/*==============================================================*/
create view ACTIVE_PRICE_LIST as
select L.ID,
L.LABEL,
L.DURATION,
L.PAY_TYPE,
H.PRICE,
H.PRICE_OTHER,
H.RETAIL_PRICE,
H.DATE_FROM
from PRICE_LIST L
    left join PRICE_HISTORY H on H.PRICE_LIST_ID = L.ID
where H.DATE_TO is null
