﻿/*==============================================================*/
/* View: FullPromoCodeHistory                                   */
/*==============================================================*/
create view FullPromoCodeHistory as
select  
		PC.ID,
		PROMO_CODE, 
		VALID_FROM, 
		VALID_TO, 
		DATE_GENERATED, 
		DEVICE_COUNT, 
		ACTIVE,
		ACTIVATION_COUNT as TOTAL_ACTIVATIONS_AVAILABLE,
		COUNT(PH.PROMO_CODE_ID) as ACTIVATION_USED,
		(ACTIVATION_COUNT - COUNT(PH.PROMO_CODE_ID)) as ACTIVATIONS_LEFT,
		LABEL,
        DURATION,
        PL.ID as PRICE_LIST_ID,
		(case when (CONVERT (DATE, GETUTCDATE()) BETWEEN CONVERT (DATE, VALID_FROM) and  CONVERT(DATE, VALID_TO)) 
						OR (CONVERT (DATE, GETUTCDATE()) < CONVERT (DATE, VALID_FROM))
			  then 0 else 1 
		 end
		) as EXPIRED,
		(case when COUNT(PH.PROMO_CODE_ID) = 0 then 0 
					when ((ACTIVATION_COUNT - COUNT(PH.PROMO_CODE_ID)) > 0)	then 1 else 2
	     end
	    ) as STATE  -- 0 NOT ACTIVATED, 1 PARTLY ACTIVATED,  2 FULL ACTIVATED
from PromoCode PC 
		left outer join PromoCodeHistory PH on PC.ID = PH.PROMO_CODE_ID
		left outer join PRICE_LIST PL on PL.ID= PC.PRICELIST_ID
		left outer join LICENSES L on L.ID = PH.LICENSE_ID 
group by	PC.ID,
			PROMO_CODE, 
			PRICELIST_ID, 
			VALID_FROM, 
			VALID_TO, 
			DATE_GENERATED,
			DEVICE_COUNT, 
			ACTIVATION_COUNT, 
			ACTIVE,
			LABEL,
            DURATION,
            PL.ID
