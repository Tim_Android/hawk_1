﻿
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
  
GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_User] TO [aspnet_Personalization_ReportingAccess]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_User] TO [HAWKWebSiteUser]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_User] TO [HAWKPeriodicEventsUser]
    AS [dbo];

GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_User] TO [HAWKWebServiceUser]
    AS [dbo];
