﻿/*==============================================================*/
/* View: PAYMENTS_HISTORY                                       */
/*==============================================================*/
create view PAYMENTS_HISTORY as
select P.ID as ID_PAYMENT, P.LIC_ID, P.PAY_DATE, PL.LABEL, P.DEV_CNT, PH.PRICE, PH.PRICE_OTHER, P.AMOUNT, 
    P.TRANS_ID, P.PREV_SUBSCR_END as START_DATE, P.SUBSCR_END as FINISH_DATE, P.PAY_COMMENT
from  PAYMENTS P
	left join PRICE_LIST PL on PL.ID = P.PRICE_LIST_ID
	left join PRICE_HISTORY PH on PH.PRICE_LIST_ID = PL.ID
where (PH.DATE_FROM <= P.PAY_DATE ) 
    and (( P.PAY_DATE <= PH.DATE_TO ) or ( PH.DATE_TO is null ))
