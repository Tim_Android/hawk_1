﻿/*==============================================================*/
/* View: SMS                                                    */
/*==============================================================*/
create view SMS  as
select M.AGENT_ID_MSG as AGENT_ID_SMS, 
       M.TYPE_ID,
       M.MSG_ADDR as SMS_ADDR,
	   M.PHONE_ID,
       M.MSG_DT as SMS_DT,
       M.MSG_BODY as SMS_BODY
from MESSAGES M
where M.MSG_SUBJ is null 
    and not exists(select A.MSG_ID 
                   from MMS_ATTACH A 
                   where A.MSG_ID = M.ID_MSG)
with check option
