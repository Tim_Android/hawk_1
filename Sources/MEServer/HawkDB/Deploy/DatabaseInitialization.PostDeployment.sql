﻿--Initialize Membership data and creation of admin account
declare @ApplicationId uniqueidentifier

select @ApplicationId=ApplicationId
from aspnet_Applications
where (ApplicationName = '/') and (LoweredApplicationName = '/');

if (@ApplicationId is null)
begin
    set @ApplicationId = 'D292C95E-365A-411F-9D7F-EFC76165DEFA';
    insert into aspnet_Applications (ApplicationName, LoweredApplicationName, ApplicationId, Description)
    values ('/', '/', @ApplicationId, NULL);
end

-- -=#  Roles  #=-
if (not exists(select * from aspnet_Roles where (RoleName='admin') and (LoweredRoleName='admin')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'3358285A-340C-4C6B-BC1D-EA901F969ABC','admin','admin',NULL);
end 

if (not exists(select * from aspnet_Roles where (RoleName='paid') and (LoweredRoleName='paid')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'3E4630EF-E94C-4470-80E9-1E8FD1A48327','paid','paid',NULL);
end

if (not exists(select * from aspnet_Roles where (RoleName='user') and (LoweredRoleName='user')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'8362EF01-5A09-47FC-88BA-A2658A4C8BBA','user','user',NULL);
end

if (not exists(select * from aspnet_Roles where (RoleName='confirmed') and (LoweredRoleName='confirmed')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'0F3338D3-A6AC-4C00-B203-FF22D4E3BD83','confirmed','confirmed',NULL);
end

declare @SchemaVersion int;
select @SchemaVersion = count(*) from aspnet_SchemaVersions;
if (@SchemaVersion = 0)
begin 
    insert into aspnet_SchemaVersions (Feature, CompatibleSchemaVersion, IsCurrentVersion)
	values ('common', 1, 1),
	       ('health monitoring', 1, 1),
		   ('membership', 1, 1),
		   ('personalization', 1, 1),
		   ('profile', 1, 1),
		   ('role manager', 1, 1);
end
go

-- -=#  Admin user  #=-
declare @ADMIN_USER_UID uniqueidentifier;
declare @UserName nvarchar(256);
declare @UserEmail nvarchar(256);
declare @CURRENT_TIME datetime;
declare @CURRENT_TIME_UTC datetime;

set @UserName = 'admin';
set @UserEmail = 'admin@paraben.com';
set @CURRENT_TIME = getdate();
set @CURRENT_TIME_UTC = getutcdate();



if (not exists(select * from USERS_INFO where (EMAIL = @UserEmail)))
begin
exec dbo.aspnet_Membership_CreateUser '/', @UserName, 
    'XGOrHsipLyVKDWo1Rlcl0XCfIuw=', 'O08G7KnXON6qBwhjWD2bTA==', -- DefaultPassword: Admin$321_456!#
    @UserEmail, NULL, NULL, 1, @CURRENT_TIME_UTC, @CURRENT_TIME, 0, 1, 
    @ADMIN_USER_UID output;
exec aspnet_UsersInRoles_AddUsersToRoles '/', @UserName, 'Admin', @CURRENT_TIME_UTC;
declare @ID int;

exec ADD_USER_INFO @ADMIN_USER_UID, 'Site', 'Admin', @UserEmail, @ID output;
end
go


--Initialize Email Table
DECLARE @EmailInit int;
select @EmailInit = COUNT(*) from EMAIL;
if (@EmailInit = 0)
begin
    SET IDENTITY_INSERT [dbo].[EMAIL] ON;

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (1, N'Registration Validation                                         ', 1, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

	<p>Thank you for signing up for the HAWK Mobile Monitor system.</p>
	<p>We hope that this helps you gain a better understanding of your child’s activities in their smartphone. Your trial period is 7-days and you will receive an email after that time to remind you to sign up for a full subscription.</p>
	<p>Thank you for signing up for the HAWK Mobile Monitor system.</p>
	<p>Please confirm your email address by clicking on this <a href="@ValidationLink@">link</a></p>
	<p>You will receive confirmation email shortly after visiting the link</p>
	<br/>
	<p>If you haven&#39;t signed up for the HAWK Mobile Monitoring System, please ignore the message.</p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
	<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
	<p><i><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></i></p>
	<a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>
	', N'@HawkSiteLink@|Link to the HAWK site||@ValidationLink@|Link to validate registration. Ensure that it is included in E-Mail text (or link) or otherwise user won''t be able to confirm his account||@FirstName@|User''s first name||@LastName@|User''s last name');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (2, N'Registration Validation Confirm                                 ', 2, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

	<p>You have successfully validated your HAWK Mobile Monitor account email address.</p>
	<p>You can now log into your account and follow the instructions to start monitoring.</p>
	<p>If you require technical support, we encourage you to go to our online support system at:</p>
	<a href="https://www.hawk-monitoring.com/Public/Support.aspx">https://www.hawk-monitoring.com/Public/Support.aspx</a>

	<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
	<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
	<p><i><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></i></p>
	<a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (3, N'Registration                                                    ', 3, N'<p><span style="color:#000080"><strong>Hello, @FirstName@ @LastName@</strong></span></p>

	<p>Thank you for your registration. Here are your account details:</p>

	<table>
		<tbody>
			<tr>
				<td>Login:</td>
				<td>@Login@</td>
			</tr>
			<tr>
				<td>Password:</td>
				<td>@Password@</td>
			</tr>
		</tbody>
	</table>

	<p>You can use credentials above to log in the site.</p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
	<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
	', N'|Currently notification is not used');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (4, N'New Password                                                    ', 5, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

	<p>Congratulations!<br />
	You have successfully completed password reset procedure.<br />
	Your new password is: @Password@</p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
	<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
	', N'@Password@|User''s new password||@FirstName@|User''s first name||@LastName@|User''s last name');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (5, N'Confirm Password Reset                                          ', 4, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>You or somebody impersonated as you requested to reset your HAWK account password.<br />
	Your password reset link will expire in 24 hours from request submission.<br />
	Please follow the <a href="@ResetLink@">link</a> to reset your password.<br />
	You will receive email with your new password shortly after visiting the link</p>

	<p>If you haven&#39;t requested password reset you can ignore the message.</p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
	<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
	', N'@ResetLink@|Password reset link. Ensure that it is included in E-Mail text (or link) or otherwise user won''t be able to reset password||@FirstName@|User''s first name||@LastName@|User''s last name');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (6, N'Price Edit (Subscription cancel notice)', 6, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>Please note the subscription prices were changed. The auto-charge for the next time period cannot be completed for License #@LicId@. Please check the new prices and click on the Buy HAWK Subscription page and verify your acceptance of the price changes.</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
	<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|License Id');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (7, N'Autocharge Transaction Error (Admn''s)', 7, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear Administrator,</strong></span></p>

	<p>Error occured during transaction processing on payment processor side. Here is an additional info:</p>

	<table border="0" cellpadding="1" cellspacing="1">
		<tbody>
			<tr>
				<td>Error Information:</td>
				<td>@ErrorInfo@</td>
			</tr>
			<tr>
				<td>SubscriptionId: &nbsp;</td>
				<td>@SubscrId@</td>
			</tr>
		</tbody>
	</table>

	<p>Please pay attention to user&#39;s account: @Email@ (@LastName@, @FirstName@ ).&nbsp;Additional information is stored in site&#39;s log files</p>

	<p>&nbsp;</p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@Email@|User''s Email||@ErrorInfo@|Error details||@SubscrId@|Subscription Id');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (8, N'Autocharge Processing Error (Admin''s)', 8, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear Administrator,</strong></span></p>

	<p>Error occured during transaction processing. Here is an additional info:</p>

	<table border="0" cellpadding="1" cellspacing="1">
		<tbody>
			<tr>
				<td>TransactionId:</td>
				<td>@TransId@</td>
			</tr>
			<tr>
				<td>SubscriptionId: &nbsp;</td>
				<td>@SubscrId@</td>
			</tr>
		</tbody>
	</table>

	<p>Please pay attention to user&#39;s account &nbsp;@Email@ (@LastName@, @FirstName@ ). Additional information is stored in site&#39;s log files</p>

	<p>&nbsp;</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@Email@|User''s Email||@TransId@|Transaction Id||@SubscrId@|Subscription Id');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (11, N'Autocharge Fail (Admin''s)', 9, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>Auto-charge failed and your Subscription #@LicId@&nbsp;canot be updated automatically</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
	<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|User''s Subscription Id');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (12, N'License Expiration Notice', 10, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>One or more of your HAWK Mobile Monitor subscriptions is about to expire.</p>
	<p>Please check the following subscriptions:</p>

	<p>@LicensesList@</p>

	<p>You must activate your subscription to continue your monitoring activities. Activation is easy, login to your account and select your name or account in the upper right corner. You then can select to Buy Subscription to activate your monitoring account.</p>
	<p>Or just follow this link: <a href="@RedirectUrl@">Buy Subscription</a>.</p>

	<p>Remember you save the longer you activate your subscription so please review all your options.</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
	<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
		<p><i><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></i></p>
	<a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicensesList@|List of licenses to check. Please note that list of licenses is rendered as unordered list||@RedirectUrl@|Current server url');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (13, N'License Expired Notice', 11, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>Your HAWK Mobile Monitor Subscription has ended. The device associated with:</p>

	<p>Subscription #@LicId@&nbsp;</p>

	<p>Is no longer being monitored. If you would like to continue monitoring or add a new device for monitoring you can do so by updating your subscription. To update your subscription please login to your account at <a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>.</p>

	<p>Once your account is renewed you can continue monitoring without having to setup the device again.</p>

	<p><em>To receive a 10% discount on a 6 month or 1 year monitoring package please help us with a quick 3 question survey <a href="https://goo.gl/forms/unLU1FRhKlOnEHRL2">here</a>.</em></p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>

	<p><em><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></em></p>
	<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
		<p><i><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></i></p>
	<a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|License Id');


	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (14, N'Account Deleted Notice', 12, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>This message confirms that your account at HAWK has been deleted.</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
	<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (16, N'Not Activated Account Notice', 13, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

	<p>You have received this message, because you have signed up for HAWK Mobile Monitoring System. However, your account has not been activated yet. To activate your account, you must purchase a subscription within the next @AccountActivationDeadline@ days. Otherwise your account will be deleted and deactivated. &nbsp;</p>

	<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@SignupFeeAmount@|Signup Fee cost||@AccountActivationDeadline@|Number of days the we wait for account activation prior deleting the account');

	INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (17, N'Trial License Expired Notice', 14, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>
	<p>Your HAWK Mobile Monitor Trial has ended. The device associated with:</p>

	<p>Subscription #@LicId@&nbsp;</p>

	<p>Is no longer being monitored. If you would like to continue monitoring or add a new device for monitoring you can do so by updating your subscription. To update your subscription please login to your account at <a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a>.</p>

	<p>Once your account is renewed you can continue monitoring without having to setup the device again.</p>

	<p><em>To receive a 10% discount on a 6 month or 1 year monitoring package please help us with a quick 3 question survey <a href="https://goo.gl/forms/unLU1FRhKlOnEHRL2">here</a>.</em></p>

	<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

	<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>

	<p><em><span style="color:gray">Provider of HAWK Mobile Monitoring System</span></em></p>

	<p><a href="https://www.hawk-monitoring.com">www.hawk-monitoring.com</a></p>
	', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|License Id');
	SET IDENTITY_INSERT [dbo].[EMAIL] OFF;
end
GO

--Initialize MIME types
DECLARE @MimeInit int;
select @MimeInit = COUNT(*) from MIME_TYPES;
if (@MimeInit = 0)
begin
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.asp', 'text/asp');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.au', 'audio/basic');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.avi', 'video/avi');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.bmp', 'image/bmp');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.boo', 'application/book');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.book', 'application/book');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.c', 'text/plain');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.c', 'text/x-c');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.c++', 'text/plain');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.cc', 'text/plain');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.cc', 'text/x-c');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.class', 'application/java');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.class', 'application/java-byte-code');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.class', 'application/x-java-class');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.css', 'application/x-pointplus');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.css', 'text/css');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.doc', 'application/msword');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.dot', 'application/msword');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.exe', 'application/octet-stream');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.gif', 'image/gif');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.h', 'text/plain');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.h', 'text/x-h');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.html', 'text/html');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ico', 'image/x-icon');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.java', 'text/plain');
	
	INSERT INTO MIME_TYPES( EXT, MIME_NAME)
	VALUES ('.java', 'text/x-java-source');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.jpg', 'image/jpeg');
	
	INSERT INTO MIME_TYPES( EXT, MIME_NAME)
	VALUES ('.jpg', 'image/pjpeg');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.js', 'application/x-javascript');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.m3u', 'audio/x-mpequrl');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'application/x-midi');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'audio/midi');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'audio/x-mid');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'audio/x-midi');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'music/crescendo');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mid', 'x-music/x-midi');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mp2', 'audio/mpeg');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mp2', 'video/x-mpeg');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mp3', 'audio/mpeg3');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mp3', 'audio/x-mpeg-3');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mpg', 'audio/mpeg');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.mpg', 'video/mpeg');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.pas', 'text/pascal');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.pic', 'image/pict');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.png', 'image/png');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.pps', 'application/vnd.ms-powerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ppt', 'application/mspowerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ppt', 'application/powerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ppt', 'application/vnd.ms-powerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ppt', 'application/x-mspowerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.ppz', 'application/mspowerpoint');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.qif', 'image/x-quicktime');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.qt', 'video/quicktime');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.rmi', 'audio/mid');
	
	INSERT INTO MIME_TYPES( EXT, MIME_NAME)
	VALUES ('.rtf', 'application/rtf');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.rtf', 'application/x-rtf');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.rtf', 'text/richtext');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.tif', 'image/tiff');

	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.tif', 'image/x-tiff');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.txt', 'text/plain');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.wav', 'audio/wav');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.wav', 'audio/x-wav');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xls', 'application/excel');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xls', 'application/vnd.ms-excel');

	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xls', 'application/x-excel');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xls', 'application/x-msexcel');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xml', 'application/xml');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.xml', 'text/xml');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.zip', 'application/x-compressed');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.zip', 'application/x-zip-compressed');
	
	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.zip', 'application/zip');

	INSERT INTO MIME_TYPES(EXT, MIME_NAME)
	VALUES ('.zip', 'multipart/x-zip');
end
go

--Initialize message statuses
DECLARE @MessageTypeInit int;
select @MessageTypeInit = COUNT(*) from MSG_TYPES;
if (@MessageTypeInit = 0)
begin
	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Inbox');

	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Sent');

	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Draft');

	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Outbox');

	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Failed');

	INSERT INTO MSG_TYPES(TYPE_NAME)
	VALUES('Queued');
end
GO


--Initialize Price List
exec INIT_PRICE_LIST;
GO

ALTER DATABASE [$(DatabaseName)] 
SET AUTO_UPDATE_STATISTICS_ASYNC ON;
GO
