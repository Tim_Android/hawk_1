﻿/*==============================================================*/
/* Domain: AGENT_NAME_TYPE                                      */
/*==============================================================*/
create type AGENT_NAME_TYPE
   from nvarchar(50)


GO
GRANT EXECUTE
    ON TYPE::[dbo].[AGENT_NAME_TYPE] TO [HAWKWebSiteUser];

