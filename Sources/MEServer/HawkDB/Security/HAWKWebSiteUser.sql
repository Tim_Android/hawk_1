﻿CREATE LOGIN [HAWKWebSiteUser]
    WITH PASSWORD = N'MEVeryStrongPasswordWith12345And_', DEFAULT_DATABASE = [HAWK], DEFAULT_LANGUAGE = [us_english], check_expiration=OFF, CHECK_POLICY = OFF;
go;

CREATE USER [HAWKWebSiteUser] FOR LOGIN [HAWKWebSiteUser];
go;
