﻿CREATE LOGIN [HAWKPeriodicEventsUser]
    WITH PASSWORD = N'MEVeryStrongPasswordWith12345And_', DEFAULT_DATABASE = [HAWK], DEFAULT_LANGUAGE = [us_english], check_expiration=OFF, CHECK_POLICY = OFF;
go;

CREATE USER [HAWKPeriodicEventsUser] FOR LOGIN [HAWKPeriodicEventsUser];
go

