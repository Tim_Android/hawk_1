﻿using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Hawk.Services
{
    public class CaptchaValidatorService
    {
        public static bool IsValid(string encodedResponse, string privateKey)
        {
            var client = new WebClient();

            string googleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", privateKey, encodedResponse));

            RecaptchaResponse captchaResponse = JsonConvert.DeserializeObject<RecaptchaResponse>(googleReply);
            if (captchaResponse != null && captchaResponse.Success == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
