﻿using Domain.Entities;
using Domain.Entities.Enums;
using Domain.Entities.Filters;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkCommonResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hawk.Infrastructure.AppLogger;

namespace Hawk.Services
{
    public class SubscriptionManagementService
    {
        public static Result UpdateLicense(LicenseModel license)
        {
            try
            {
                var pager = new Pager();

                LicenseModel oldLicense = LicenseModel.GetLicenseListPaged(new LicenseFilter
                {
                    Id = license.id
                }, ref pager).Single();

                PriceListItemModel[] priceList = PriceListItemModel.GetPriceList(null);

                if (license.devices != oldLicense.devices)
                {
                    PaymentHistoryItemModel historyItem = new PaymentHistoryItemModel
                    {
                        id = license.id,
                        paymentDate = DateTime.UtcNow,
                        transactionId = Common.Admin,
                        amount = 0,
                        deviceCount = license.devices,
                        afterPaymentSubscrEnd = oldLicense.subscrEndDate,// for monatomic changes
                        originalSubscrEnd = oldLicense.subscrEndDate
                    };

                    if (license.devices > oldLicense.devices)
                    {
                        PriceListItemModel price = priceList.Where(x => x.type == PriceListItemType.DeviceAdd).Single();
                        historyItem.priceId = price.id;
                        historyItem.paymentComment = string.Format(SubscriptionManagement.AddedDevices, license.devices - oldLicense.devices);
                    }
                    else
                    {
                        PriceListItemModel price = priceList.Where(x => x.type == PriceListItemType.DeviceRemoval).Single();
                        historyItem.priceId = price.id;
                        historyItem.paymentComment = string.Format(SubscriptionManagement.RemovedDevices, oldLicense.devices - license.devices);
                    }

                    LicenseModel.UpdateLicense(historyItem);
                }

                if (license.subscrEndDate.Date != oldLicense.subscrEndDate.Date)
                {
                    PaymentHistoryItemModel historyItem = new PaymentHistoryItemModel
                    {
                        id = license.id,
                        paymentDate = DateTime.UtcNow,
                        transactionId = Common.Admin,
                        amount = 0,
                        deviceCount = license.devices,
                        afterPaymentSubscrEnd = license.subscrEndDate,
                        originalSubscrEnd = oldLicense.subscrEndDate,
                    };

                    if (license.subscrEndDate.Date > oldLicense.subscrEndDate.Date)
                    {
                        PriceListItemModel price = priceList.Where(x => x.type == PriceListItemType.SubscriptionExtend).Single();
                        historyItem.priceId = price.id;
                        historyItem.paymentComment = string.Format(SubscriptionManagement.SubscriptionExtended, (license.subscrEndDate - oldLicense.subscrEndDate).Days);
                    }
                    else
                    {
                        PriceListItemModel price = priceList.Where(x => x.type == PriceListItemType.SubscriptionDecrease).Single();
                        historyItem.priceId = price.id;
                        historyItem.paymentComment = string.Format(SubscriptionManagement.SubscriptionDecreased, (oldLicense.subscrEndDate - license.subscrEndDate).Days);
                    }

                    LicenseModel.UpdateLicense(historyItem);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(String.Format(SubscriptionManagement.ErrorUpdateLogger, license.id, ex.Message));

                return new Result(new NotifyMessage
                {
                    MessType = NotifyMessageType.Error,
                    MessText = SubscriptionManagement.ErrorLicenseUpdate
                });
            }
            

            return new Result(new NotifyMessage
            {
                MessType = NotifyMessageType.Success,
                MessText = SubscriptionManagement.LicenseUpdated
            });
        }
    }
}
