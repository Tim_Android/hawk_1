﻿using EmailManagement;
using EmailManagement.Types;
using Hawk.Infrastructure.BLL;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Security;

namespace Hawk.Services
{
    public class AccountService
    {
        public static void SendValidationEmail(string email, int tokenLength, string registrationLink, UserInfoModel userInfo)
        {
            string token = Utils.GetRandomString(tokenLength);
            TokenManagement.AddToken(email, token);
            string port = Utils.GetPortParameter();
            string validationLink = Utils.ConvertRelativeUrlToAbsoluteUrl(registrationLink + token, port);

            //Send registration confirmation email
            Email emailData = EmailComposer.InitGeneralinfo(email, EmailManagement.EmailResources.RegistrationValidationSubj);

            emailData.bodyParams.Add("@HawkSiteLink@", Utils.ConvertRelativeUrlToAbsoluteUrl(String.Empty, port));
            emailData.bodyParams.Add("@ValidationLink@", validationLink);
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);
            EmailComposer.SendEmailNotification(EmailTypes.RegistrationValidation, emailData);
        }

        public static void DeleteCustomer(bool testMode, string email)
        {

            //TODO: use transactionscope for these operations in the future
            //currently using it results in error as 2 databse connections are used in 1 scope
            //which requires enabling of distributed network transactions first

            MembershipUser user = Membership.GetUser(email);
            UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);
            UserInfoModel.DeleteUserInfo((Guid)user.ProviderUserKey);
            AccountService.CancelCustomerSubscriptions(userInfo.id, testMode);
            Membership.DeleteUser(email);

            Email emailData = EmailComposer.InitGeneralinfo(userInfo.email, EmailManagement.EmailResources.AccountDeletionSubj);
            EmailManager.SendAccountDeletionNotice(userInfo, emailData);
        }

        public static void CancelCustomerSubscriptions(int userId, bool testMode)
        {
            LicenseModel[] licensesAffected = LicenseModel.GetUserLicensesWithSubscriptions(userId);
            if (licensesAffected.Length > 0)
            {//we have users that require subscription cancellation 
                foreach (LicenseModel license in licensesAffected)
                {
                    IPaymentProcessor processor = null;
                    switch (license.type)
                    {
                        case PaymentProcessors.AuthorizeNet:
                            processor = new AuthorizeNetPaymentProcessor(testMode);
                            break;
                        case PaymentProcessors.PayPal:
                            processor = new PaypalPaymentProcessor(testMode);
                            break;
                    }
                    //cancel subscription
                    var subscriptionResult = processor.CancelSubscription(license.subscriptionId);
                    if (subscriptionResult.operResult.result == PaymentResult.Success)
                    {
                        //commit subscription change to DB
                        LicenseModel.UpdateLicenseSubscrId(license.id, null);
                    }
                }
            }
        }
    }
}
