﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Hawk.Services
{
    public static class CookieStorageService
    {
        public static void SetCookie(string value, string name)
        {
            HttpCookie cookie = new HttpCookie(name);

            cookie.Expires.AddYears(1);
            cookie.Value = value;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static string GetCookie(string name)
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(name))
            {
                return HttpContext.Current.Request.Cookies[name].Value;
            }
            else
            {
                return null;
            }
        }
    }
}
