﻿using HawkDataAccess.Models;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hawk.Services
{
    public class PromoCodeService
    {
        
        /// <summary>
        /// Method for generation of promocode for trial period
        /// </summary>
        /// <param name="trialDeviceCount"></param>
        /// <param name="trialActivationCount"></param>
        /// <param name="trialPromocodeDuration"></param>
        /// <param name="trialValidityPeriod">Number of days the code is valid</param>
        /// <returns></returns>
        public static string GenerateTrialPromocode(int trialDeviceCount, int trialActivationCount, int trialPromocodeDuration, int trialValidityPeriod)
        {
            PriceListItemModel promoCodeEntry = PriceListItemModel.GetPriceList(PriceListItemType.Promocode).Where(res => res.duration == trialPromocodeDuration).Single();
            string trialCode = Guid.NewGuid().ToString().Replace("-", string.Empty).ToUpper();
            PromoCodeModel.GenerateManuallyPromoCode(promoCodeEntry.id, trialDeviceCount, trialActivationCount, DateTime.UtcNow, DateTime.UtcNow.AddDays(trialValidityPeriod), trialCode);
            return trialCode;
        }

        //TODO: Move here business logic from PromoCodeModel (need to create promoCodeRepository as well)
    }
}
