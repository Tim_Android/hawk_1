﻿using Domain.Entities;
using Domain.Entities.Enums;
using Domain.Entities.Reports;
using Domain.Interfaces;
using Hawk.Infrastructure.BLL;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReportResources = HawkCommonResources.Report;


namespace Hawk.Services
{
    public class ReportService
    {
        private ReportSchemaEntity _schema = null;
        private FilterValues _dataTransfer = null;

        public PdfReport GetReport(DataTable data, TabsType type, FilterValues dataTransfer)
        {
            _dataTransfer = dataTransfer;
            _schema = GetReportSchema(type);
            return new PdfReport(_schema, data);
        }
        private ReportSchemaEntity GetReportSchema(TabsType type)
        {
            switch (type)
            {
                case TabsType.CallLog:
                    {
                        return BuildCallSchema();
                    }
                case TabsType.SMSLog:
                    {
                        return BuildSMSSchema();
                    }
                case TabsType.MMSLog:
                    {
                        return BuildMMSSchema();
                    }
                case TabsType.GPSLog:
                    {
                        return BuildGPSSchema();
                    }
                case TabsType.BrowserLog:
                    {
                        return BuildBrowserHistorySchema();
                    }
                case TabsType.BookmarkLog:
                    {
                        return BuildBookmarkSchema();
                    }
                default:
                    break;
            }
            return null;
        }

        private ReportSchemaEntity BuildBookmarkSchema()
        {
            return new ReportSchemaEntity()
                 .AddName(ReportResources.FileNameBrowserBookmark)
                 .AddHeader(ReportResources.ReportHeaderBrowserBookmark + _dataTransfer.Device)
                 .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                 .AddFontPaths(GetFontPathes())
                 .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderUrl,
                                    Field = "{0}",
                                    Arguments = new List<string> { "URL" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderTitle,
                                    Field = "{0}",
                                    Arguments = new List<string> { "TITLE" }
                                }
                                })
                .AddFilters(new Dictionary<string, string>
                                {
                                    {ReportResources.FilterUrl, string.IsNullOrEmpty(_dataTransfer.UrlFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.UrlFilter},
                                    {ReportResources.FilterTitle, string.IsNullOrEmpty(_dataTransfer.TitleFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.TitleFilter}
                                });
        }

        private ReportSchemaEntity BuildBrowserHistorySchema()
        {
            return new ReportSchemaEntity()
                 .AddName(ReportResources.FileNameBrowserHistory)
                 .AddHeader(ReportResources.ReportHeaderBrowserHistory + _dataTransfer.Device)
                 .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                 .AddFontPaths(GetFontPathes())
                 .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDeviceDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DEV_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLocalDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "USER_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderUrl,
                                    Field = "{0}",
                                    Arguments = new List<string> { "URL" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderTitle,
                                    Field = "{0}",
                                    Arguments = new List<string> { "TITLE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderVisitsCount,
                                    Field = "{0}",
                                    Arguments = new List<string> { "VISITS" }
                                }
                                })
                .AddFilters(new Dictionary<string, string>
                            {
                                {ReportResources.FilterDateFrom, _dataTransfer.DateTimeFromFilter.ToString() },
                                {ReportResources.FilterDateTo, _dataTransfer.DateTimeToFilter.ToString() },
                                {ReportResources.FilterUrl, string.IsNullOrEmpty(_dataTransfer.UrlFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.UrlFilter},
                                {ReportResources.FilterTitle, string.IsNullOrEmpty(_dataTransfer.TitleFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.TitleFilter},
                                {ReportResources.FilterVisitCount, _dataTransfer.VisitsCountFilter == null ? HawkCommonResources.Common.Empty : _dataTransfer.VisitsCountFilter.ToString()},
                            });
        }

        private ReportSchemaEntity BuildGPSSchema()
        {
            return new ReportSchemaEntity()
                .AddName(ReportResources.FileNameGPSHistory)
                .AddHeader(ReportResources.ReportHeaderGPSHistory + _dataTransfer.Device)
                .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                .AddFontPaths(GetFontPathes())
                .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDeviceDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DEV_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLocalDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "USER_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderType,
                                    Field = "{0}",
                                    Arguments = new List<string> { "PROVIDER" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLatitude,
                                    Field = "{0}",
                                    Arguments = new List<string> { "LATITUDE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLongitude,
                                    Field = "{0}",
                                    Arguments = new List<string> { "LONGITUDE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderAccuracy,
                                    Field = "{0}",
                                    Arguments = new List<string> { "ACCURACY" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderSpeed,
                                    Field = "{0}",
                                    Arguments = new List<string> { "SPEED" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderAltitude,
                                    Field = "{0}",
                                    Arguments = new List<string> { "ALTITUDE"}
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderBearing,
                                    Field = "{0}",
                                    Arguments = new List<string> { "BEARING" }
                                }
                                })
                    .AddFilters(new Dictionary<string, string>
                                    {
                                        {ReportResources.FilterDateFrom, _dataTransfer.DateTimeFromFilter.ToString() },
                                        {ReportResources.FilterDateTo, _dataTransfer.DateTimeToFilter.ToString() },
                                        {ReportResources.FilterType, string.IsNullOrEmpty(_dataTransfer.GPSSelectedEntry) ? HawkCommonResources.Common.Empty : _dataTransfer.GPSSelectedEntry}
                                    });
        }

        private ReportSchemaEntity BuildMMSSchema()
        {
            return new ReportSchemaEntity()
                .AddName(ReportResources.FileNameMMSMessages)
                .AddHeader(ReportResources.ReportHeaderMMSMessages + _dataTransfer.Device)
                .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                .AddFontPaths(GetFontPathes())
                .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderPhoneNumberEmail,
                                    Field = "{0}",
                                    Arguments = new List<string> { "MSG_ADDR" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderPhoneName,
                                    Field = "{0}",
                                    Arguments = new List<string> { "PHONE_NAME" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDeviceDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DEV_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLocalDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "USER_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderMessageBody,
                                    Field = "{0}",
                                    Arguments = new List<string> { "MSG_BODY" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderType,
                                    Field = "{0}",
                                    Arguments = new List<string> { "TYPE_NAME" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderSubject,
                                    Field = "{0}",
                                    Arguments = new List<string> { "MSG_SUBJ" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderAttachments,
                                    Field = "{0}",
                                    Arguments = new List<string> { "ATTACHMENTS" }
                                }
                                })
            .AddFilters(new Dictionary<string, string>
                        {
                            {ReportResources.FilterDateFrom, _dataTransfer.DateTimeFromFilter.ToString() },
                            {ReportResources.FilterDateTo, _dataTransfer.DateTimeToFilter.ToString() },
                            {ReportResources.FilterType, _dataTransfer.TypeFilter == (int)PhoneCallType.Unknown ? HawkCommonResources.Common.Any : ((PhoneCallType)_dataTransfer.TypeFilter).ToString()},
                            {ReportResources.FilterAddressBookEntry, _dataTransfer.NumberFilter == null ? HawkCommonResources.Common.Empty : _dataTransfer.NumberFilter },
                            {ReportResources.FilterContent, string.IsNullOrEmpty(_dataTransfer.ContentFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.ContentFilter}
                        });
        }

        private ReportSchemaEntity BuildSMSSchema()
        {
            return new ReportSchemaEntity()
                 .AddName(ReportResources.FileNameSMSMessages)
                 .AddHeader(ReportResources.ReportHeaderSMSMessages + _dataTransfer.Device)
                 .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                 .AddFontPaths(GetFontPathes())
                 .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderPhoneNumberEmail,
                                    Field = "{0}",
                                    Arguments = new List<string> { "SMS_ADDR" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderPhoneName,
                                    Field = "{0}",
                                    Arguments = new List<string> { "PHONE_NAME" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDeviceDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DEV_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLocalDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "USER_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderMessageBody,
                                    Field = "{0}",
                                    Arguments = new List<string> { "SMS_BODY" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderType,
                                    Field = "{0}",
                                    Arguments = new List<string> { "TYPE_NAME" }
                                }
                                })
                .AddFilters(new Dictionary<string, string>
                            {
                                {ReportResources.FilterDateFrom, _dataTransfer.DateTimeFromFilter.ToString() },
                                {ReportResources.FilterDateTo, _dataTransfer.DateTimeToFilter.ToString() },
                                {ReportResources.FilterContact, _dataTransfer.ContactFilter == null ? HawkCommonResources.Common.AnyContact : (_dataTransfer.ContactFilter == string.Empty ? HawkCommonResources.Common.UnknownNumber : _dataTransfer.ContactFilter)},
                                {ReportResources.FilterType, _dataTransfer.TypeFilter == (int)PhoneCallType.Unknown ? HawkCommonResources.Common.Any : ((PhoneCallType)_dataTransfer.TypeFilter).ToString()},
                                {ReportResources.FilterContent, string.IsNullOrEmpty(_dataTransfer.ContentFilter) ? HawkCommonResources.Common.Empty : _dataTransfer.ContentFilter}
                            });
        }

        private ReportSchemaEntity BuildCallSchema()
        {
            return new ReportSchemaEntity()
                 .AddName(ReportResources.FileNameCallHistory)
                 .AddHeader(ReportResources.ReportHeaderCallHistory + _dataTransfer.Device)
                 .AddLogoPath(_dataTransfer.BasePath + ReportResources.LogoPath)
                 .AddFontPaths(GetFontPathes())
                 .AddColumns(new List<ColumnEntity> {
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDeviceDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DEV_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderLocalDate,
                                    Field = "{0}",
                                    Arguments = new List<string> { "USER_DATE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderPhoneNumber,
                                    Field = "{0}",
                                    Arguments = new List<string> { "PHONE_NUMBER" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderContactName,
                                    Field = "{0}",
                                    Arguments = new List<string> { "CONTACT_NAME" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderNumberType,
                                    Field = "{0}",
                                    Arguments = new List<string> { "NUMBER_TYPE" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderDuration,
                                    Field = "{0}",
                                    Arguments = new List<string> { "DURATION" }
                                },
                                new ColumnEntity
                                {
                                    Name = ReportResources.HeaderType,
                                    Field = "{0}",
                                    Arguments = new List<string> { "CALL_TYPE" }
                                }
                                })
                    .AddFilters(new Dictionary<string, string>
                                {
                                    {ReportResources.FilterDateFrom, _dataTransfer.DateTimeFromFilter.ToString() },
                                    {ReportResources.FilterDateTo, _dataTransfer.DateTimeToFilter.ToString() },
                                    {ReportResources.FilterType, _dataTransfer.TypeFilter == (int)PhoneCallType.Unknown ? HawkCommonResources.Common.Any : ((PhoneCallType)_dataTransfer.TypeFilter).ToString()},
                                    {ReportResources.FilterAddressBookEntry, _dataTransfer.NumberFilter == null ? HawkCommonResources.Common.Empty : _dataTransfer.NumberFilter }
                                });
        }
        private Dictionary<Font, string> GetFontPathes()
        {
            return new Dictionary<Font, string> {
                                    { Font.ArialNormal, _dataTransfer.BasePath + ReportResources.FontPathArialNormal },
                                    { Font.ArialBold, _dataTransfer.BasePath + ReportResources.FontPathArialBold },
                                    { Font.ArialItalic, _dataTransfer.BasePath + ReportResources.FontPathArialItalic },
                                };
        }
    }

}
