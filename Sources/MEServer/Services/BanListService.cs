﻿using Domain.Entities;
using Domain.Entities.Filters;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hawk.Services
{
    public class BanListService
    {
        public static bool AddEmailToBannedList(BannedEmailModel data)
        {
            if (!isBanned(data.email))
            {
                BannedListManagement.AddEmailToBannedList(data);
                return true;
            }
            return false;
        }

        public static bool isBanned(string email)
        {
            var filter = new BannedEmailFilter();
            filter.Email = email;
            var pager = new Pager();
            var bannedList = BannedListManagement.GetBannedList(filter, ref pager);

            if (bannedList.Count == 0)
                return false;
            else
                return true;
        }

        public static IList<BannedEmailModel> GetBannedList(BannedEmailFilter filter, ref Pager pager)
        {
            return BannedListManagement.GetBannedList(filter, ref pager);
        }

        public static bool UpdateBannedEmail(BannedEmailModel data)
        {
            return BannedListManagement.UpdateBannedEmail(data);
        }

        public static bool DeleteEmailFromBannedList(string email)
        {
            return BannedListManagement.DeleteEmailFromBannedList(email);
        }
    }
}
