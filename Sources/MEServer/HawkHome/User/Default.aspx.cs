﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using HawkHome.Code;
using HawkHome.Common.Page;

namespace HawkHome
{
    public partial class Default : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //to redirect customer to payment if he hasn't paid yet
            if (!HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.PAID))
            {
                Response.Redirect("~/User/Billing/Billing.aspx", true);
            }

            //to redirect straightforward to device info
            if (DeviceList.Count > 0)
            {
                Response.Redirect("~/User/AgentInfo.aspx", true);
            }
            else
            {
                Response.Redirect("~/User/CreateAgent.aspx", true);
            }
            UserUI main = Page.Master as UserUI;
            main.HideHeader();
            main.HideMonitoringTabs();
            MonitoringUi.HideRecordSetDetails();
        }
    }
}
