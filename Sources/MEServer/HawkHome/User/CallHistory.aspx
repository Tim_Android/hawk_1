<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="CallHistory.aspx.cs" Inherits="HawkHome.CallHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var page = 'call_list';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <asp:GridView ID="gridCallHistory" runat="server" AllowPaging="True"
        AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover long_word_fix"
        OnPageIndexChanging="gridCallHistory_PageIndexChanging" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="DEV_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Device Date/Time" SortExpression="DEV_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="USER_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Local Date/Time" SortExpression="USER_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="PHONE_NUMBER" HeaderText="Phone Number/Email" SortExpression="FROM_NUMBER" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="CONTACT_NAME" HeaderText="Contact Name" SortExpression="FROM_PHONE_NAME" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="NUMBER_TYPE" HeaderText="Number Type" SortExpression="FROM_NUMBER_TYPE" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="DURATION" HeaderText="Duration (hh:mm:ss)" SortExpression="DURATION" ReadOnly="true">
                <HeaderStyle Width="80px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="CALL_TYPE" HeaderText="Type" SortExpression="CALL_TYPE" ReadOnly="true">
                <HeaderStyle Width="70px"></HeaderStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EmptyDataTemplate>
            No Data Found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
