﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Security;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkCommonResources;
using HawkHome.Common.Page;

namespace HawkHome
{
    public partial class CallHistory : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.CallLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.CallLog, CurrentDeviceInfo.device));

            GridDataLog = gridCallHistory;
        }

        protected void gridCallHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridCallHistory.PageIndex = e.NewPageIndex;
            gridCallHistory.DataBind();
        }
    }
}