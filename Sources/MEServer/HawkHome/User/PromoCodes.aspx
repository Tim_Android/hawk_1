﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PromoCodes.aspx.cs" Inherits="HawkHome.User.PromoCodes" MasterPageFile="~/Common/BaseUI.Master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/basic_styles.css") %>" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>License Codes</h4>
                <hr class="edit_account_hr" />
                <div class="empty_div_height_large">&nbsp;</div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="activatePromoCode" class="col-sm-2 control-label label_color_black">Enter your License Code:</label>
                    <div class="col-sm-5">
                        <asp:TextBox ID="activatePromoCode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-5">
                        <asp:Button ID="ActivateButton" runat="server" Text="Activate"
                            CssClass="btn btn-sm btn-warning label_color_black mtop2" ValidationGroup="PromoCode" OnClick="btnActivate_Click" type="button" />
                        <asp:RequiredFieldValidator ID="activatePromoCodeValidator" runat="server" Display="Dynamic"
                            ControlToValidate="activatePromoCode" ValidationGroup="PromoCode">
                                    <span class="alert-danger inline_alert">Please enter a License Code</span>
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
