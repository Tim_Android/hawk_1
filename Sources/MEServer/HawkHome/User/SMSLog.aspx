﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="SMSLog.aspx.cs" Inherits="HawkHome.SMSLog" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">var page = 'sms';</script>
    <script src="<%=ResolveUrl("~/JS/readmore.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/initializeReadMoreLink.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/Pages/sms_log.js") %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <div class="row">
        <asp:ListView ID="listViewSMSLog" runat="server" ItemPlaceholderID="listItem" OnPagePropertiesChanging="OnPagePropertiesChanging" GroupPlaceholderID="listItemGroup">
            <LayoutTemplate>
                <div class="col-sm-offset-3 col-sm-6">
                    <asp:PlaceHolder runat="server" ID="listItemGroup"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <GroupTemplate>
                <asp:PlaceHolder runat="server" ID="listItem"></asp:PlaceHolder>
            </GroupTemplate>
            <ItemTemplate>
                <div class="bubble-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <p class="<%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "" : "text-right"%> menu_bold">
                               <%# Session["selectedContact"] != null ? string.Empty : (
                                                                Eval("PHONE_NAME") != DBNull.Value ? Eval("PHONE_NAME") : (
                                                                                        (string)Eval("SMS_ADDR") == string.Empty ? HawkCommonResources.Common.UnknownNumber : Eval("SMS_ADDR")))%>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class='<%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "bubble me" : "bubble you"%>'>
                            <%if ((string)Session["dateVariant"] == "0")
                                {%>
                            <p class="small <%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "" : "text-right"%>">
                                <span class="small"><%# Eval("DEV_DATE")%></span>

                            </p>
                            <% }
                                else
                                {%>
                            <p class="small <%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "" : "text-right"%>">
                                <span class="small"><%# Eval("USER_DATE")%></span>
                            </p>
                            <% }%>

                            <p class="text-black">
                                <b>
                                    <%# Eval("SMS_BODY")%>
                                </b>
                            </p>
                            <p class="small <%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "" : "text-right"%>">
                                <span class="glyphicon <%# (string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.InboxMessage ? "glyphicon-import" : 
                                                           ((string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.QueuedMessage ? "glyphicon-inbox" : 
                                                           ((string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.FailedMessage ? "glyphicon-remove" : 
                                                           ((string)DataBinder.Eval(Container.DataItem, "TYPE_NAME") == HawkCommonResources.Common.DraftMessage ? "glyphicon-pencil" : "glyphicon-export")))%>" aria-hidden="true"></span>
                                <%# Eval("TYPE_NAME")%>
                            </p>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <%--  Pager   --%>
        <div class="text-center" id="divPager" runat="server">
            <asp:DataPager ID="dataPager" runat="server" PagedControlID="listViewSMSLog" OnLoad="DataPager_Load">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Button" ButtonCssClass="btn btn-sm background_color_gray label_color_black" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                        ShowNextPageButton="false" />
                    <asp:NumericPagerField ButtonType="Button" CurrentPageLabelCssClass="btn btn-sm btn-warning label_color_black" NextPreviousButtonCssClass="btn btn-sm background_color_gray label_color_black" NumericButtonCssClass="btn btn-sm background_color_gray label_color_black" />
                    <asp:NextPreviousPagerField ButtonType="Button" ButtonCssClass="btn btn-sm background_color_gray label_color_black" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                </Fields>
            </asp:DataPager>
        </div>
    <%--  Pager  End --%>
     <asp:GridView ID="gridSMSLog" runat="server"
        AllowPaging="True" CssClass="table table-bordered table-striped table-condensed table-hover long_word_fix"
        OnPageIndexChanging="gridSMSLog_PageIndexChanging" GridLines="Horizontal" Height="100%" Width="100%"
        AutoGenerateColumns="False" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="SMS_ADDR" HeaderText="Phone Number/Email" SortExpression="SMS_ADDR" ReadOnly="true">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="PHONE_NAME" HeaderText="Phone Name" SortExpression="PHONE_NAME" ReadOnly="true">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="DEV_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Device Date/Time" SortExpression="DEV_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="USER_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Local Date/Time" SortExpression="USER_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="SMS_BODY" HeaderText="Message Body" SortExpression="SMS_BODY" ReadOnly="true">
                <HeaderStyle Width="150px"></HeaderStyle>
                <ItemStyle CssClass="article"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" SortExpression="TYPE_NAME" ReadOnly="true">
                <HeaderStyle Width="50px"></HeaderStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" CssClass="pager" />
        <SelectedRowStyle CssClass="table_row_selected" Font-Bold="True" ForeColor="Black" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EmptyDataTemplate>
            No Data Found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
