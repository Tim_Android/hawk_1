﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="AgentInfo.aspx.cs" Inherits="HawkHome.User.AgentInfo" %>

<%@ Import Namespace="Domain.Entities.Enums" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <script type="text/javascript">
        var page = 'device_info';
    </script>
    <script src="<%=ResolveUrl("~/JS/blockUI.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/Pages/device_info.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <div id="device_info" class="pure-skin-paraben elements_margin">
        <div class="row">
            <div class="col-sm-6">
                <table class="pure-table pure-table-horizontal full_width">
                    <thead>
                        <tr>
                            <th colspan="2" class="text_align_center"><span class="text-bold">Your device information </span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Device name: </td>
                            <td>
                                <asp:TextBox ID="edtDeviceName" runat="server" MaxLength="32" CssClass="form-control">1_2_3_4_..._18_19_20</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Activation status: </td>
                            <td>
                                <div class="mtop10">
                                    <span class="badge <% if (CurrentDeviceInfo.licId == null)
                                                          { %> 
                                                        progress-bar-danger                          
                                                   <% }
                                                          else
                                                          {%>
                                                        progress-bar-success
                                                   <%} %>">
                                        <asp:Label ID="lblActivation" runat="server"></asp:Label></span>
                                </div>
                                <div class="mtop10 form-horizontal">
                                    <div class="form-group">
                                        <asp:Label ID="lblLicense" runat="server" Visible="false" CssClass="col-sm-5 control-label label_color_black text-left-important"></asp:Label>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlLicenses" runat="server" AutoPostBack="false" CssClass="zero_margin_bottom form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:LinkButton ID="btnActivate" runat="server" CssClass="btn btn-sm btn-success label_color_black monitored_devices_button" OnClick="btnActivate_Click">
                                    <i class="icon-ok"></i> Activate
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeactivate" runat="server" CssClass="btn btn-sm btn-warning label_color_black monitored_devices_button" OnClick="btnDeactivate_Click">
                                    <i class="icon-off"></i> Deactivate
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="mtop10">
                                </div>
                            </td>
                        </tr>
                        <tr id="activationDate" runat="server" visible="false">
                            <td>Activation date: </td>
                            <td>
                                <asp:Label ID="lblActivationDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Phone number: </td>
                            <td>
                                <asp:Label ID="lblPhoneNumber" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Brand: </td>
                            <td>
                                <asp:Label ID="lblBrand" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Model: </td>
                            <td>
                                <asp:Label ID="lblModel" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>System: </td>
                            <td>
                                <asp:Label ID="lblOS" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <% if (CurrentDeviceInfo.operSysType == OperatingSystemType.Ios)
                               {%>
                            <td>Unique ID: </td>
                            <%}
                               else
                               {%>
                            <td>IMEI: </td>
                            <%}%>

                            <td>
                                <asp:Label ID="lblIMEI" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Time Zone: </td>
                            <td>
                                <asp:Label ID="lblTimeZone" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>SDK: </td>
                            <td>
                                <asp:Label ID="lblSDK" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <% if (CurrentDeviceInfo.operSysType != OperatingSystemType.Ios)
                               { %>
                            <td>Firmware ID: </td>
                            <td>
                                <asp:Label ID="lblFirmware" runat="server" Text="no data"></asp:Label>
                            </td>
                            <% }%>
                        </tr>
                        <tr>
                            <td>Agent Name: </td>
                            <td>
                                <asp:Label ID="lblAgent" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Agent Version: </td>
                            <td>
                                <asp:Label ID="lblVersion" runat="server" Text="no data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Activity: </td>
                            <td>
                                <asp:Label ID="lblLastActivity" runat="server" Text="Last agent activity: "></asp:Label>
                                ago
                            </td>
                        </tr>
                        <tr>
                            <td>Note: </td>
                            <td>
                                <asp:TextBox ID="edtDeviceNote" runat="server" TextMode="MultiLine" CssClass="form-control mtop10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnDeviceInfoUpdate" runat="server"
                                    Text="Save Changes" CssClass="btn btn-sm btn-warning label_color_black"
                                    OnClick="btnDeviceInfoUpdate_Click" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <div class="elements_margin">
                    <asp:LinkButton ID="btnBuySubscription" runat="server" CssClass="btn btn-sm btn-success label_color_black device_info_button"
                        OnClick="btnBuySubscription_Click">
                                  <i class="glyphicon glyphicon-shopping-cart"></i> Buy Subscription
                    </asp:LinkButton>
                </div>
                 <div class="elements_margin">
                    <asp:LinkButton ID="btnLicenseCodes" runat="server" CssClass="btn btn-sm btn-success label_color_black device_info_button"
                        OnClick="btnLicenseCodes_Click">
                                   License Codes
                    </asp:LinkButton>
                </div>
                <div class="elements_margin">

                    <asp:Button ID="btnRebuild" runat="server" CssClass="btn btn-sm btn-warning label_color_black device_info_button"
                        OnClick="btnRebuild_Click" data-toggle="modal" data-target="#downloadAgent" Text="Build Agent" />

                         <asp:LinkButton ID="getAgent" runat="server" Visible="false" CssClass="btn btn-sm btn-success label_color_black device_info_button"
                        OnClick="btnGetAgent_Click">
                                  <i class="icon-download"></i> Download Agent
                    </asp:LinkButton>
                </div>
                
                <div class="elements_margin">
                    <asp:LinkButton ID="btnRemoveAgent" runat="server" Text="Remove Agent" CssClass="pure-button pure-button-error pure-button-small label_color_white device_info_button deleteAgent hide"
                        OnClick="btnRemoveAgent_Click">
                        <i class="icon-trash"></i> Remove Agent
                    </asp:LinkButton>

                    <a id="deleteAgent" class="btn btn-danger btn-sm label_color_white device_info_button"><i class="glyphicon glyphicon-trash"></i>&nbsp;Remove Agent</a>

                    <div id="popup" class="hide popup_padding">
                        <div>
                            All agent data will be permanently deleted from the site. Do you want to continue?
                        </div>
                        <div class="elements_margin">
                            <input type="button" id="yes" value="Yes" class="btn btn-sm btn-warning  label_color_black" />
                            <input type="button" id="no" value="No" class="btn btn-sm label_color_black" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!--Modals-->
    <div class="modal fade" id="downloadAgent" tabindex="-1" role="dialog" aria-labelledby="downloadAgent">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Building agent...
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="full_width progress-bar progress-bar-warning progress-bar-striped active" role="progressbar"
                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
