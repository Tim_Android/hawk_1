﻿using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkHome.Code;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace HawkHome.User.Ajax
{
    /// <summary>
    /// Summary description for CancelSubscription
    /// </summary>
    public class SubscriptionManager : IHttpHandler
    {
        private Logger logger;

        public SubscriptionManager()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public void ProcessRequest(HttpContext context)
        {
            string response = String.Empty;
            OperationResultModel result = new OperationResultModel();
            bool subscriptionCancelErrors = false;
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.USER) && context.Request.Params["licId"] != null)
            {
                Guid aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
                UserInfoModel userInfo = UserInfoModel.GetUserInfo(aspUserId);
                LicenseModel[] userLicenses = LicenseModel.GetLicenseList(userInfo.id, null);
                LicenseModel license = userLicenses.Where(res => res.id == Convert.ToInt32(context.Request.Params["licId"])).FirstOrDefault();

                if (license != null)
                {

                    IPaymentProcessor processor = null;
                    bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
                    switch (license.type)
                    {
                        case PaymentProcessors.AuthorizeNet:
                            processor = new AuthorizeNetPaymentProcessor(testMode);
                            break;
                        case PaymentProcessors.PayPal:
                            processor = new PaypalPaymentProcessor(testMode);
                            break;
                    }
                    //cancel subscription
                    
                    var subscriptionResult = processor.CancelSubscription(license.subscriptionId.Trim());
                    if (subscriptionResult.operResult.result == PaymentResult.Success)
                    {
                        //commit subscription change to DB
                        LicenseModel.UpdateLicenseSubscrId(license.id, null);
                    }
                    else
                    {
                        logger.Warn(String.Format(Resources.Error.SubscriptionCancelUser, license.id, license.subscriptionId));
                        subscriptionCancelErrors = true;
                    }
                }
                else
                {
                    logger.Info(String.Format(Resources.Error.UnauthorizedSubscriptionCancelAttempt, context.User.Identity.Name, context.Request.Params["licId"]));
                    subscriptionCancelErrors = true;
                }
            }
            else
            {
                logger.Info(String.Format(Resources.Error.UnauthorizedAccessAttempt));
                subscriptionCancelErrors = true;
            }
            if (!subscriptionCancelErrors)
            {
                result.code = OperationResultModel.CODE_SUCCESS;
                result.message = Resources.Success.AutochargeTurnedOff;
            }
            else
            {
                result.code = OperationResultModel.CODE_ERROR;
                result.message = Resources.Error.SubscriptionCancel;
            }
            response = JSSerializer.GetInstance().Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}