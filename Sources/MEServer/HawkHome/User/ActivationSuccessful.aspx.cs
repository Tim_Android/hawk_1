﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User
{
    public partial class ActivationSuccessful : System.Web.UI.Page
    {
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/Default.aspx", true);
        }
    }
}