﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HawkHome.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <div>
        <h4><asp:Label ID="lblHello" runat="server" Text="Welcome!" Font-Size="Large"></asp:Label></h4>
        <hr class="edit_account_hr">
    </div>
    <div class="ipsum">
        Follow these instructions to start using HAWK:
        <ol>
            <li>Click the device icon to the left and click New Agent on the Monitored Devices pane to create a new Agent.</li>
            <li>Install the Agent on the monitored device (make sure the Internet connection or Wi-Fi connection is enabled).</li>
            <li>After your device appears in the list of monitored devices, click it.</li>
            <li>On the Device Info tab of the selected device, click Activate to activate the Agent on the device.</li>
            <li>The Agent starts sending data from the device. As soon as data is received, you can view it on the Call History, SMS Messages, MMS Messages, and GPS History tabs.</li>
        </ol>
    </div>
</asp:Content>
