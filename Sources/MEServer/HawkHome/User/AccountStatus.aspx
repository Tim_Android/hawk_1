﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="AccountStatus.aspx.cs" Inherits="HawkHome.User.AccountStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/overlay.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.nimble.loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/subscription_details.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>Subscription Details</h4>
                <hr class="edit_account_hr" />
            </div>
            <div class="row-fluid">
                <% if (licenseDetails.Length > 0)
                   {%>
                <div class="col-sm-3">
                    <select id="licenseList" size="20" class="full_width form-control" autocomplete="off">
                        <% for (int i = 0; i < licenseDetails.Length; i++)
                           {
                               if (i == 0)
                               {%>
                        <option selected value="<%= licenseDetails[i].licenseInfo.id %>"><%= licenseDetails[i].licenseInfo.licenseName %></option>
                        <%
                                   }
                                   else
                                   {
                        %>
                        <option value="<%= licenseDetails[i].licenseInfo.id %>"><%= licenseDetails[i].licenseInfo.licenseName %></option>
                        <%
                                   }
                               }
                        %>
                    </select>
                </div>
                <div class="col-sm-9">
                    <div class="well">
                        <% for (int i = 0; i < licenseDetails.Length; i++)
                           {
                        %>
                        <div class="<% if (i != 0)
                                       {%> hide <% } %>"
                            id="license<%= licenseDetails[i].licenseInfo.id %>">

                            <div>
                                <h5>Details for <%= licenseDetails[i].licenseInfo.licenseName %></h5>
                                <hr class="edit_account_hr" />
                                <div class="elements_margin">
                                    <table class="elements_margin">
                                        <tr>
                                            <td class="text-bold">Expiration Date:</td>
                                            <td>&nbsp;&nbsp;&nbsp;<%= licenseDetails[i].licenseInfo.subscrEndDate.ToString("g") %></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">The number of devices in the subscription:</td>
                                            <td>&nbsp;&nbsp;&nbsp;<%= licenseDetails[i].licenseInfo.devices %></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">The number of devices with activated Agents:</td>
                                            <td>&nbsp;&nbsp;&nbsp;<%= licenseDetails[i].licenseInfo.devices - licenseDetails[i].licenseInfo.freeDevices %></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">The number of available devices:</td>
                                            <td>&nbsp;&nbsp;&nbsp;<%= licenseDetails[i].licenseInfo.freeDevices %></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <div class="small_top_margin">
                                    <h5>Upcoming Payment for <%= licenseDetails[i].licenseInfo.licenseName %></h5>
                                    <hr class="edit_account_hr" />
                                    <div>
                                        <% if (licenseDetails[i].autochargeInfo != null)
                                           { %>
                                        <div id="scheduled_payment<%=licenseDetails[i].licenseInfo.id %>">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" checked class="autocharge" data-license="<%= licenseDetails[i].licenseInfo.id %>" />Charge automatically each time period
                                                </label>
                                                <div id="overlay<%=licenseDetails[i].licenseInfo.id %>" class="overlay">
                                                    <img src="/Images/loading.gif" id="img-load<%=licenseDetails[i].licenseInfo.id %>" class="img-load" />
                                                </div>
                                            </div>
                                            <div>
                                                <table class="pure-table pure-table-horizontal upcoming_payment_width">
                                                    <thead>
                                                        <tr>
                                                            <th class="text_align_center">Time period</th>
                                                            <th class="text_align_center">Number of devices</th>
                                                            <th class="text_align_center">Price per device</th>
                                                            <th class="text_align_center">Total</th>
                                                            <th class="text_align_center">Next payment</th>
                                                        </tr>
                                                    </thead>
                                                    <tr>
                                                        <td class="text_align_center"><%= licenseDetails[i].autochargeInfo.label %></td>
                                                        <td class="text_align_center"><%= licenseDetails[i].licenseInfo.devices %></td>
                                                        <td class="text_align_center">$<%= licenseDetails[i].autochargeInfo.price.ToString("F") %></td>
                                                        <td class="text_align_center">$<%= licenseDetails[i].autochargeInfo.total.ToString("F") %></td>
                                                        <td class="text_align_center"><%= licenseDetails[i].autochargeInfo.paymentDate.ToString("g") %></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <% }
                                           else
                                           { %>
                                        <div>This subscription has no upcoming payments</div>
                                        <% } %>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <%
                               }
                        %>
                    </div>
                </div>
                <% }
                   else
                   {%>
                <div class="span15">
                    <div>Currently you have no active subscriptions</div>
                    <div class="elements_margin"><a href="/User/Billing/Billing.aspx" class="pure-button pure-button-small pure-button-warning label_color_black">Buy subscription</a></div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
