﻿using System;
using System.Web.UI.WebControls;
using HawkDataAccess.Types;
using HawkCommonResources;
using HawkHome.Common.Page;
using Domain.Entities;

namespace HawkHome
{
    public partial class SMSLog : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.SMSLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.SmsLog, CurrentDeviceInfo.device));
            if (Session[StorageConstants.SELECTED_VIEW] == null || (byte)Session[StorageConstants.SELECTED_VIEW] == (byte)Domain.Entities.Enums.ViewDisplaying.BubbleStyle)
            {
                ListViewDataLog = listViewSMSLog;
                listViewSMSLog.Visible = true;
                gridSMSLog.Visible = false;
            }
            if ((byte)Session[StorageConstants.SELECTED_VIEW] == (byte)Domain.Entities.Enums.ViewDisplaying.Grid)
            {
                gridSMSLog.Visible = true;
                listViewSMSLog.Visible = false;
                divPager.Visible = false;

                GridDataLog = gridSMSLog;
                if (IsPostBack)
                {
                    gridSMSLog.PageIndex = GetCurrentIndex();
                }
                gridSMSLog.DataBind();
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (Session[StorageConstants.SELECTED_VIEW] == null || (byte)Session[StorageConstants.SELECTED_VIEW] == (byte)Domain.Entities.Enums.ViewDisplaying.BubbleStyle)
            {
                BindDataListViewLog(MonitoringUi.GetSearchCondition());
                divPager.Visible = MonitoringUi.TotalRecords > 0;
            }
            if ((byte)Session[StorageConstants.SELECTED_VIEW] == (byte)Domain.Entities.Enums.ViewDisplaying.Grid)
            {
                base.OnLoadComplete(e);
            }
        }

        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            SaveCurrentStartRow(e.StartRowIndex);
            dataPager.SetPageProperties(GetCurrentStartRowIndex(), e.MaximumRows, false);
        }

        protected void DataPager_Load(object sender, EventArgs e)
        {
            int total = GetSMSTotalCount(MonitoringUi.GetSearchCondition(), StorageConstants.GET_SMS_TOTAL_COUNT);
            int startRow = CalculateStartRowIndex(total);
            SaveCurrentStartRow(startRow);

            dataPager.SetPageProperties(startRow, MonitoringUi.RecordsPerPage, true);
            dataPager.PageSize = MonitoringUi.RecordsPerPage;
        }

        private int CalculateStartRowIndex(int total)
        {
            int currentRowIndex = GetCurrentStartRowIndex();
            return currentRowIndex > total ? CalculateCurrentRowIndex(total) : CalculateCurrentRowIndex(currentRowIndex);
        }

        private int CalculateCurrentRowIndex(int total)
        {
            return (total / MonitoringUi.RecordsPerPage) * MonitoringUi.RecordsPerPage;
        }

        protected void gridSMSLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SaveCurrentIndex(e.NewPageIndex);
            gridSMSLog.PageIndex = e.NewPageIndex;
            gridSMSLog.DataBind();
        }

        private void SaveCurrentIndex(int newSelectedIndex)
        {
            // page synchronization between listView and GridView
            Session[StorageConstants.SELECTED_START_ROW] = newSelectedIndex * MonitoringUi.RecordsPerPage;
            Session[StorageConstants.SELECTED_PAGE_INDEX] = newSelectedIndex;
        }
        private void SaveCurrentStartRow(int newStartRow)
        {
            Session[StorageConstants.SELECTED_START_ROW] = newStartRow;
        }
        private int GetCurrentIndex()
        {
            // for switch between Grid and Listview properly. Cause: GridView doesn't know anything about page count changes
            return (int)Session[StorageConstants.SELECTED_START_ROW] / MonitoringUi.RecordsPerPage;
        }
        private int GetCurrentStartRowIndex()
        {
            return Session[StorageConstants.SELECTED_START_ROW] == null ? 0 : (int)Session[StorageConstants.SELECTED_START_ROW];
        }
    }
}
