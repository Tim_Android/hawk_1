﻿#region namespaces

using System;
using System.Web.UI.WebControls;
using Domain.Entities.Enums;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Types;
using HawkHome.Code;
using System.Configuration;
using HawkHome.Common.Page;
using Hawk.Infrastructure.AppLogger;

#endregion

namespace HawkHome
{
    public partial class CreateAgent : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.Inactive, CurrentDeviceInfo);
            MonitoringUi.HideMonitoringTabs();
            MonitoringUi.HideRecordSetDetails();

            MonitoringUi.SetHeader(Resources.General.labelAgentInstInstructions);
        }

        protected void btnCreateAgent_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime timestamp = DateTime.UtcNow;
                string agentName = Resources.General.labelProductName + " " + timestamp.ToString(ConfigurationManager.AppSettings["agentNameDateTimeFormat"]);

                int id = DataAccess.AddAgent(Account.Value.Id, agentName, (int)OperatingSystemType.Android);
                string path = AgentBuilder.BuildAgentInstaller(id.ToString(), agentName);

                Session.Add("agentPath", path);
                Session.Add("agentName", agentName);

                getAgent.Visible = true;
                btnCreateAgent.Visible = false;
            }
            catch (Exception ex)
            {
                Logger.LogError("Android agent's building. Error during build");
                Logger.LogError(ex);
            }
        }

        protected void btnGetAgent_Click(object sender, EventArgs e)
        {
            try
            {
                string contentType = "application/vnd.android.package-archive";
                string path = (string)Session["agentPath"];
                string agentName = (string)Session["agentName"];

                Response.Clear();
                Response.Buffer = false;
                Response.ContentType = contentType;
                Response.AddHeader("Content-Disposition", string.Format(" attachment; filename=\"{0}\"", agentName + ".apk"));
                Response.TransmitFile(path);
                Response.End();
            }
            catch (Exception ex)
            {
                Logger.LogError("Android agent's downloading. Error during download");
                Logger.LogError(ex);
            }
        }
    }
}
