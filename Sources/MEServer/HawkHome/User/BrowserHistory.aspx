﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="BrowserHistory.aspx.cs" Inherits="HawkHome.User.BrowserHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var page = 'browser_history';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <asp:GridView ID="gridBrowserHistory" runat="server" AllowPaging="True"
        AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover long_word_fix"
        OnPageIndexChanging="gridBrowserHistory_PageIndexChanging" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="DEV_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Device Date/Time" SortExpression="DEV_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="USER_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Local Date/Time" SortExpression="USER_DATE" ReadOnly="true">
                <HeaderStyle Width="160px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TITLE" HeaderText="Title" SortExpression="TITLE" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="VISITS" HeaderText="Visits Count" SortExpression="VISITS" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EmptyDataTemplate>
            No Data Found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
