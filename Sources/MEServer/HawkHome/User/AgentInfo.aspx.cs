﻿#region namespaces

using HawkDataAccess;
using HawkDataAccess.Exceptions;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using System;
using System.Web.UI.WebControls;
using Domain.Entities.Enums;
using HawkHome.Common.Page;
using Domain.Entities;

#endregion

namespace HawkHome.User
{
    public partial class AgentInfo : MonitoringPage
    {
        private BaseUI main
        {
            get
            {
                return Page.Master.Master as BaseUI;
            }
        }

        private UserUI userTemplate
        {
            get
            {
                return Page.Master as UserUI;
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            if (!DataAccess.ValidateAgentId(CurrentDeviceInfo.id, Account.Value.AspUserId))
            {
                Server.Transfer("~/User/CreateAgent.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            main.ResetMessages();
            MonitoringUi.HideRecordSetDetails();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            MonitoringUi.SetHeader(string.Format(Resources.General.labelDeviceHeader, CurrentDeviceInfo.device));
            MonitoringUi.SetPageType(TabsType.AgentInfo, CurrentDeviceInfo);

            InitBasicDeviceInfoUI();
            InitActivationUI();
        }

        private void InitActivationUI()
        {
            lblActivation.Text = CurrentDeviceInfo.licId != null ? Resources.General.labelActivated : HawkCommonResources.Common.labelNotActivated;

            if (CurrentDeviceInfo.licId != null && CurrentDeviceInfo.licActDate != null)
            {//adding activation date label
                activationDate.Visible = true;
                lblActivationDate.Text = CurrentDeviceInfo.licActDate.Value.ToString("g");
            }

            btnDeactivate.Visible = CurrentDeviceInfo.canBeDeactivated;
            if (CurrentDeviceInfo.licId == null)
            {// if device not activated, we try to get list of valid licenses for activation
                InitAvailableLicenses(ddlLicenses, main.userInfo.id);
                if (ddlLicenses.Items.Count == 0)
                {//if there is not valid licenses, then hide activation option
                    btnActivate.Visible = false;
                    ddlLicenses.Visible = false;
                }
                lblLicense.Visible = false;
            }
            else
            {//if device is activated, there is no need in displaying activation options
                btnActivate.Visible = false;
                ddlLicenses.Visible = false;
                lblLicense.Visible = true;
                lblLicense.Text = String.Format(Resources.General.labelLicense, CurrentDeviceInfo.licId);
            }
        }

        private void InitAvailableLicenses(DropDownList ddlLicenses, int userId)
        {
            LicenseModel[] licenses = LicenseModel.GetUserLicListForActivation(userId);
            if (licenses != null)
            {
                ddlLicenses.Items.Clear();
                foreach (LicenseModel license in licenses)
                {
                    ListItem option = new ListItem(license.licenseName, license.id.ToString());
                    ddlLicenses.Items.Add(option);
                }
            }
        }

        private void InitBasicDeviceInfoUI()
        {
            DateTime lastActivity = DataAccess.GetAgentLastUpdateTime(CurrentDeviceInfo.id);
            TimeSpan diff = DateTime.Now - lastActivity;

            lblLastActivity.Text = string.Format(Resources.General.labelTimeSinceLastActivity, diff.Days, diff.Hours, diff.Minutes, diff.Seconds);
            edtDeviceName.Text = CurrentDeviceInfo.device;
            edtDeviceNote.Text = CurrentDeviceInfo.note;
            lblAgent.Text = CurrentDeviceInfo.agent;
            lblBrand.Text = CurrentDeviceInfo.brand;
            lblIMEI.Text = CurrentDeviceInfo.deviceId;
            lblModel.Text = CurrentDeviceInfo.model;
            lblOS.Text = CurrentDeviceInfo.os;
            lblSDK.Text = CurrentDeviceInfo.sdk;
            lblFirmware.Text = CurrentDeviceInfo.firmware;
            lblTimeZone.Text = CurrentDeviceInfo.timeZone;
            lblVersion.Text = CurrentDeviceInfo.svn;
            lblPhoneNumber.Text = CurrentDeviceInfo.phoneNumber;
        }

        protected void btnRemoveAgent_Click(object sender, EventArgs e)
        {
            DataAccess.RemoveAgent(CurrentDeviceInfo.id);
            InitializeDeviceList(true);
            Response.Redirect("~/User/Default.aspx");
        }

        protected void btnRebuild_Click(object sender, EventArgs e)
        {
            try
            {
                string agentName = string.Empty;

                string path = default(string);
                string contentType = default(string);

                switch (CurrentDeviceInfo.operSysType)
                {
                    case OperatingSystemType.Android:
                        path = AgentBuilder.BuildAgentInstaller(CurrentDeviceInfo.id.ToString(), CurrentDeviceInfo.device);
                        contentType = "application/vnd.android.package-archive";
                        agentName = string.Format("HAWK ({0}).apk", CurrentDeviceInfo.device);
                        break;
                    case OperatingSystemType.Ios:
                        path = IosAgentBuild.GetAgentPath();
                        contentType = "application/octet-stream";
                        agentName = string.Format("HAWK ({0}).ipa", CurrentDeviceInfo.device);
                        break;
                    default:
                        throw new Exception("Invalid Agent type");
                }

                Session.Add("agentPath", path);
                Session.Add("agentName", agentName);
                Session.Add("contentType", contentType);

                getAgent.Visible = true;
                btnRebuild.Visible = false;


            }
            catch (SimultaniousBuildException ex)
            {
                main.SetErrorMessage(ex.Message);
            }
            catch (Exception ex)
            {
                main.SetErrorMessage(ex.Message);
            }
        }

        protected void btnDeviceInfoUpdate_Click(object sender, EventArgs e)
        {
            CurrentDeviceInfo.device = edtDeviceName.Text;
            CurrentDeviceInfo.note = edtDeviceNote.Text;
            DataAccess.UpdateDeviceInfo(CurrentDeviceInfo);
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {
            if (DeviceManagement.ActivateDevice(CurrentDeviceInfo.id, Convert.ToInt32(ddlLicenses.SelectedValue)))
            {
                main.SetSuccessMessage(Resources.Success.DeviceActivation);
            }
            else
            {
                main.SetErrorMessage(Resources.Error.DeviceActivation);
            }

            InitializeDeviceList(true);
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            if (DeviceManagement.DeactivateDevice(CurrentDeviceInfo.id))
            {
                main.SetSuccessMessage(Resources.Success.DeviceDeactivation);
            }
            else
            {
                main.SetErrorMessage(Resources.Error.DeviceDeactivation);
            }

            InitializeDeviceList(true);
        }

        protected void btnBuySubscription_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/Billing/Billing.aspx");
        }
        protected void btnLicenseCodes_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/PromoCodes.aspx");
        }
        #region PushNotifications draft
        /*
        protected void btnPushTest_Click(object sender, EventArgs e)
        {
            string developerKey = "AIzaSyCXQ0pHi07wfiUoeJxa2-jPcTU79UVhBjY";
            string deviceRegId = "APA91bER1uwOhfSMnJZ7X6hsrV_403eIf4SBVDBLs6rquFi78NVjiGFUrQ9VEKLyg-Z7WjJTpiCCUBFVGJpBmuQcHUGP1vIxIAvZoRqngcuWRum0kzoB2j-_r-33ZLcyfvUNvStr5FhsAHEjPeAj1wxyMSTIO0TTaQ";
            PushMessageModel message = new PushMessageModel(PushType.WipeDevice, edtDeviceNote.Text);
            AndroidPushHandler.SendNotification(developerKey, deviceRegId, message);
        } 
         * */
        #endregion

        protected void btnGetAgent_Click(object sender, EventArgs e)
        {
            try
            {
                string contentType = (string)Session["contentType"];
                string path = (string)Session["agentPath"];
                string agentName = (string)Session["agentName"];

                Response.Clear();
                Response.Buffer = false;
                Response.ContentType = contentType;
                Response.AddHeader("Content-Disposition", string.Format(" attachment; filename=\"{0}\"", agentName));
                Response.TransmitFile(path);
                Response.End();

            }
            catch (SimultaniousBuildException ex)
            {
                main.SetErrorMessage(ex.Message);
                getAgent.Visible = false;
                btnRebuild.Visible = true;
            }
            catch (Exception ex)
            {
                main.SetErrorMessage(ex.Message);
                getAgent.Visible = false;
                btnRebuild.Visible = true;
            }
        }
    }
}

