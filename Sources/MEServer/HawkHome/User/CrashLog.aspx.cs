﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkCommonResources;
using HawkHome.Common.Page;

namespace HawkHome
{
    public partial class CrashLog : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           MonitoringUi.SetPageType(TabsType.CrashLog, CurrentDeviceInfo);
           agentId = Convert.ToInt32(CurrentDeviceInfo.id);

            //todo: store agentName in cache/session

           Guid userId = (Guid)(Membership.GetUser(HttpContext.Current.User.Identity.Name)).ProviderUserKey;
            if (!DataAccess.ValidateAgentId(agentId, userId))
            {
                Server.Transfer("Default.aspx");
            }

            if (Master is IGridDisplayable)
            {
                IGridDisplayable masterPageGridDisplayable = Master as IGridDisplayable;
               // masterPageGridDisplayable.DataLogHeader = String.Format(PageHeader.CrashLog, CurrentDeviceInfo.device);
            }

            if (!IsPostBack)
            {
                BindCrashLogs();
            }
        }

        protected void gridCrashLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridCrashLog.PageIndex = e.NewPageIndex;
            BindCrashLogs();
        }

        private void BindCrashLogs()
        {
            DataSet ds = DataAccess.GetCrashLogs(agentId);
            gridCrashLog.DataSource = ds;
            gridCrashLog.DataBind();
        }

        int agentId;
    }
}