﻿using EmailManagement;
using Hawk.Services;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkDataAccess;
using HawkDataAccess.Const;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User
{
    public partial class AccountEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.IsInRole(AvailableRoles.ADMIN))
            {
                deleteAccSection.Visible = false;
            }
            if (!IsPostBack)
            {
                BaseUI main = Page.Master as BaseUI;
                FirstName.Text = main.userInfo.firstName;
                LastName.Text = main.userInfo.lastName;

            }
        }

        protected void GeneralUserDataEdit_Click(object sender, EventArgs e)
        {
            bool updateResult = UserInfoModel.UpdateUserInfo((Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey, new UserInfoModel
            {
                firstName = FirstName.Text,
                lastName = LastName.Text
            });
            this.form_edit.Visible = false;
            this.result_form.Visible = true;
            if (updateResult == true)
            {
                resultCaption.Text = Resources.Success.Label;
                resultDescription.Text = Resources.Success.AccountInformationSaved;
            }
            else
            {
                resultCaption.Text = Resources.Error.Label;
                resultDescription.Text = Resources.Error.Unknown;
            }
        }

        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            if (Membership.ValidateUser(User.Identity.Name, CurrentPassword.Text))
            {
                if (NewPassword.Text.Length >= Convert.ToInt32(ConfigurationManager.AppSettings["minimumPasswordLength"]))
                {
                    if (NewPassword.Text == ConfirmNewPassword.Text)
                    {
                        MembershipUser user = Membership.GetUser();
                        this.form_edit.Visible = false;
                        this.result_form.Visible = true;
                        if (user.ChangePassword(CurrentPassword.Text, NewPassword.Text))
                        {
                            resultCaption.Text = Resources.Success.Label;
                            resultDescription.Text = Resources.Success.PasswordChange;
                        }
                        else
                        {
                            resultCaption.Text = Resources.Error.Label;
                            resultDescription.Text = Resources.Error.Unknown;
                        }
                    }
                    else
                    {
                        FailureText.Text = Resources.Error.PasswordFieldsDontMatch;
                    }
                }
                else
                {
                    FailureText.Text = String.Format(Resources.Error.PasswordLengthInsufficient, ConfigurationManager.AppSettings["minimumPasswordLength"]);
                }
            }
            else
            {
                FailureText.Text = Resources.Error.CurrentPasswordInvalid;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            BaseUI main = Page.Master as BaseUI;

            AccountService.DeleteCustomer(testMode, main.userInfo.email);
            FormsAuthentication.SignOut();
            Roles.DeleteCookie();
            Session.Abandon();
            Response.Redirect("~/Public/Default.aspx?success=profile_deleted");
        }
    }
}