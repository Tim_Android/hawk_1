﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="CreateAgent.aspx.cs" Inherits="HawkHome.CreateAgent" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/jquery.lazyload.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/Pages/create_agent.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <div>
        <div class="mbottom10">
            Welcome to HAWK Mobile Monitor. To start monitoring, you will need to install the HAWK Agent onto your device. Click on the Android icon for Android devices or the Apple icon for iOS devices and follow the installation steps below. 
        </div>
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="row" data-bind="foreach: osNavigation">
                    <div class="col-sm-12">
                        <div>
                            <a href="#" class="thumbnail" data-bind="click: $root.ChangeOs, css: { os_not_selected: $root.selectedOsType() != osType }">
                                <img data-bind="attr: { src: imagePath }" class="os-button">
                            </a>
                        </div>
                        <div class="text-center">
                            <span data-bind="text: name"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="androidSubmenu" class="mtop20 text-center" data-bind="visible: selectedOsType() == 1">
            <asp:Button ID="btnCreateAgent" runat="server" Text="Build Agent" ValidationGroup="Agentinfo"
                CssClass="btn btn-sm btn-warning label_color_black mleft5"
                OnClick="btnCreateAgent_Click" data-toggle="modal" data-target="#downloadAgent" />

                                <asp:Button ID="getAgent" runat="server" Text="Download Agent" Visible="false" ValidationGroup="Agentinfo"
                CssClass="btn btn-sm btn-success label_color_black device_info_button"
                OnClick="btnGetAgent_Click" />
        </div>
<%--        <div id="iosSubmenu" class="mtop20 text-center" data-bind="visible: selectedOsType() == 2">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeOsVersion.bind($data, 2), css: { active: selectedOsVersion() == 2 }">iOS 6</button>
                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeOsVersion.bind($data, 3), css: { active: selectedOsVersion() == 3 }">iOS 7-9</button>
            </div>
        </div>--%>

        <div id="androidNav" class="mtop20 mbottom20 text-center" data-bind="visible: selectedOsVersion() == 1">
            <div class="btn-toolbar inline-block" role="toolbar">
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.PrevSlide, visible: currentSlide() > 1"><span class="glyphicon glyphicon-arrow-left"></span></button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Configuring&nbsp;Device">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 1), css: { active: currentSlide() == 1 }">1</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 2), css: { active: currentSlide() == 2 }">2</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 3), css: { active: currentSlide() == 3 }">3</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 4), css: { active: currentSlide() == 4 }">4</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Downloading&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 5), css: { active: currentSlide() == 5 }">5</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 6), css: { active: currentSlide() == 6 }">6</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 7), css: { active: currentSlide() == 7 }">7</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 8), css: { active: currentSlide() == 8 }">8</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 9), css: { active: currentSlide() == 9 }">9</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 10), css: { active: currentSlide() == 10 }">10</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 11), css: { active: currentSlide() == 11 }">11</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 12), css: { active: currentSlide() == 12 }">12</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 13), css: { active: currentSlide() == 13 }">13</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 14), css: { active: currentSlide() == 14 }">14</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 15), css: { active: currentSlide() == 15 }">15</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 16), css: { active: currentSlide() == 16 }">16</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Installing&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 17), css: { active: currentSlide() == 17 }">17</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 18), css: { active: currentSlide() == 18 }">18</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 19), css: { active: currentSlide() == 19 }">19</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 20), css: { active: currentSlide() == 20 }">20</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 21), css: { active: currentSlide() == 21 }">21</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 22), css: { active: currentSlide() == 22 }">22</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Finishing&nbsp;Installation">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 23), css: { active: currentSlide() == 23 }">23</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.NextSlide, visible: currentSlide() < 23"><span class="glyphicon glyphicon-arrow-right"></span></button>
                </div>
            </div>
        </div>

        <div id="ios6Nav" class="mtop20 mbottom20 text-center" data-bind="visible: selectedOsVersion() == 2">
            <div class="btn-toolbar inline-block" role="toolbar">
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.PrevSlide, visible: currentSlide() > 1"><span class="glyphicon glyphicon-arrow-left"></span></button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Installing&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 1), css: { active: currentSlide() == 1 }">1</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 2), css: { active: currentSlide() == 2 }">2</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 3), css: { active: currentSlide() == 3 }">3</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 4), css: { active: currentSlide() == 4 }">4</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 5), css: { active: currentSlide() == 5 }">5</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 6), css: { active: currentSlide() == 6 }">6</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 7), css: { active: currentSlide() == 7 }">7</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 8), css: { active: currentSlide() == 8 }">8</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 9), css: { active: currentSlide() == 9 }">9</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 10), css: { active: currentSlide() == 10 }">10</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 11), css: { active: currentSlide() == 11 }">11</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 12), css: { active: currentSlide() == 12 }">12</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Connecting&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 13), css: { active: currentSlide() == 13 }">13</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 14), css: { active: currentSlide() == 14 }">14</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 15), css: { active: currentSlide() == 15 }">15</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 16), css: { active: currentSlide() == 16 }">16</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Hiding&nbsp;Cydia&nbsp;and&nbsp;Poof">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 17), css: { active: currentSlide() == 17 }">17</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 18), css: { active: currentSlide() == 18 }">18</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 19), css: { active: currentSlide() == 19 }">19</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 20), css: { active: currentSlide() == 20 }">20</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 21), css: { active: currentSlide() == 21 }">21</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 22), css: { active: currentSlide() == 22 }">22</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 23), css: { active: currentSlide() == 23 }">23</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 24), css: { active: currentSlide() == 24 }">24</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 25), css: { active: currentSlide() == 25 }">25</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 26), css: { active: currentSlide() == 26 }">26</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 27), css: { active: currentSlide() == 27 }">27</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 28), css: { active: currentSlide() == 28 }">28</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 29), css: { active: currentSlide() == 29 }">29</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Finishing&nbsp;Installation">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 30), css: { active: currentSlide() == 30 }">30</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.NextSlide, visible: currentSlide() < 30"><span class="glyphicon glyphicon-arrow-right"></span></button>
                </div>
            </div>
        </div>

        <div id="ios7_8Nav" class="mtop20 mbottom20 text-center" data-bind="visible: selectedOsVersion() == 3">
            <div class="btn-toolbar inline-block" role="toolbar">
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.PrevSlide, visible: currentSlide() > 1"><span class="glyphicon glyphicon-arrow-left"></span></button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Installing&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 1), css: { active: currentSlide() == 1 }">1</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 2), css: { active: currentSlide() == 2 }">2</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 3), css: { active: currentSlide() == 3 }">3</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 4), css: { active: currentSlide() == 4 }">4</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 5), css: { active: currentSlide() == 5 }">5</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 6), css: { active: currentSlide() == 6 }">6</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 7), css: { active: currentSlide() == 7 }">7</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 8), css: { active: currentSlide() == 8 }">8</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 9), css: { active: currentSlide() == 9 }">9</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 10), css: { active: currentSlide() == 10 }">10</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 11), css: { active: currentSlide() == 11 }">11</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 12), css: { active: currentSlide() == 12 }">12</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Connecting&nbsp;Agent">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 13), css: { active: currentSlide() == 13 }">13</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 14), css: { active: currentSlide() == 14 }">14</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 15), css: { active: currentSlide() == 15 }">15</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 16), css: { active: currentSlide() == 16 }">16</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 17), css: { active: currentSlide() == 17 }">17</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Locking&nbsp;Cydia">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 18), css: { active: currentSlide() == 18 }">18</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 19), css: { active: currentSlide() == 19 }">19</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 20), css: { active: currentSlide() == 20 }">20</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 21), css: { active: currentSlide() == 21 }">21</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 22), css: { active: currentSlide() == 22 }">22</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 23), css: { active: currentSlide() == 23 }">23</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 24), css: { active: currentSlide() == 24 }">24</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 25), css: { active: currentSlide() == 25 }">25</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 26), css: { active: currentSlide() == 26 }">26</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 27), css: { active: currentSlide() == 27 }">27</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 28), css: { active: currentSlide() == 28 }">28</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 29), css: { active: currentSlide() == 29 }">29</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 30), css: { active: currentSlide() == 30 }">30</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 31), css: { active: currentSlide() == 31 }">31</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group" data-toggle="tooltip" data-placement="top" title="Finishing&nbsp;Installation">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 32), css: { active: currentSlide() == 32 }">32</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 33), css: { active: currentSlide() == 33 }">33</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.ChangeSlide.bind($data, 34), css: { active: currentSlide() == 34 }">34</button>
                </div>
                <div class="btn-group mtop5 no-float" role="group">
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.NextSlide, visible: currentSlide() < 34"><span class="glyphicon glyphicon-arrow-right"></span></button>
                </div>
            </div>
        </div>

        <div id="androidSlides" data-bind="visible: selectedOsVersion() == 1" class="text-black">
            <div data-bind="visible: currentSlide() == 1">
                <div>
                    <p>
                        To install the Agent, you need to enable the installation from unknown sources on the device.
                    </p>
                    <p>
                        To enable the installation from unknown sources, first, go to the device <span class="text-bold">Settings</span>.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/1.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 2">
                <div>
                    <p>
                        For Android 4.0 and lower, tap <span class="text-bold">Applications</span>.
                    </p>
                    <p>
                        For Android 4.1 and higher, tap <span class="text-bold">Security</span>.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/2.png")%>" class="lazy tutorial-slide-android-2x center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 3">
                <div>
                    Find and tap the <span class="text-bold">Unknown Sources</span> option if its checkbox is not selected yet.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/3.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 4">
                <div>
                    Tap <span class="text-bold">OK</span> in the confirmation message.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/4.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 5">
                <div>
                    Open a mobile browser on your device and enter <span class="text-bold">hawk-monitoring.com</span> in the address bar. If you have issues using the web-site with a built-in browser, install Google Chrome or Mozilla Firefox on your device.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/5.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 6">
                <div>
                    Tap the web-site menu button in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/6.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 7">
                <div>
                    Tap <span class="text-bold">Log In</span> in the web-site menu.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/7.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 8">
                <div>
                    On the <span class="text-bold">Log In</span> page, enter the credentials of your HAWK Mobile Monitor account and tap <span class="text-bold">Log In</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/8.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 9">
                <div>
                    Once logged in, tap the web-site menu button in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/9.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 10">
                <div>
                    Tap <span class="text-bold">Account</span> in the web-site menu.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/10.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 11">
                <div>
                    Tap <span class="text-bold">Device Monitoring</span> in the <span class="text-bold">Account</span> sub-menu.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/11.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 12">
                <div>
                    On the <span class="text-bold">Device Monitoring</span> page, tap the device icon to the left to display the <span class="text-bold">Monitored Devices</span> pane.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/12.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 13">
                <div>
                    On the <span class="text-bold">Monitored Devices</span> pane, tap <span class="text-bold">New Agent</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/13.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 14">
                <div>
                    The <span class="text-bold">Agent Installation Instruction</span> page is displayed. Tap <span class="text-bold">Android Agent</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/14.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 15">
                <div>
                    Tap <span class="text-bold">Build Agent</span> to buid an Agent installation file.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/15.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 16">
                <div>
                    Tap <span class="text-bold">Download Agent</span> to download an Agent installation file to the device.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/16.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 17">
                <div>
                    After the Agent installation file is downloaded, find the folder with downloads on the device and tap the Agent installation file.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/17.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 18">
                <div>
                    If the <span class="text-bold">Complete action using</span> dialogue appears, select <span class="text-bold">Verify and install</span> and tap <span class="text-bold">Just once</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/18.png")%>" class="lazy tutorial-slide-ios-8-truncated center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 19">
                <div>
                    Scroll down through the description of the permissions required by the Agent and tap <span class="text-bold">Install</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/19.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 20">
                <div>
                    The Agent installation starts. After the installation finishes, tap <span class="text-bold">Open</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/20.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 21">
                <div>
                    Define a 4-digit password (make sure it contains different digits to be secure) to prevent the Agent from being uninstalled without your authorization and tap <span class="text-bold">Start agent</span>. 
                    Please note that you won’t be able to uninstall the Agent without this password, so please make sure you remember it.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/21.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 22">
                <div>
                    After the Agent is started, the <span class="text-bold">Activate device administrator</span> screen appears. Tap <span class="text-bold">Activate</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/22.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 23">
                <div>
                    <p>
                        Return to the web-site and tap Refresh on the Monitored Devices pane. The Agent will appear in the list of monitored devices under the device manufacturer and model name. 
                        For more information on the work with the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Content/HAWK_UserManual.pdf")%>">help file</a>.
                    </p>

                    <p>
                        If you have problems installing the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Public/Support.aspx")%>">Support page</a>.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/androidGeneric/23.png")%>" class="lazy tutorial-slide-android center-block" />
                </div>
            </div>
            <div>
                <div class="pull-right text-right">
                    <div>
                        <a target="_blank" href="<%=ResolveUrl("~/Content/hawk-android.pdf") %>" class="explicit-link">PDF Instructions</a>
                    </div>
                    <div>
                        <a target="_blank" href="https://youtu.be/6D2WsqkboG4" class="explicit-link">Video Instructions</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="ios6Slides" data-bind="visible: selectedOsVersion() == 2" class="text-black">
            <div data-bind="visible: currentSlide() == 1">
                <div>
                    Tap the <span class="text-bold">Cydia</span> icon on the home screen of your device. Cydia should already be installed on a jailbroken device by default. 
                    If Cydia is not installed, please contact our support at <a href="mailto:hawksupport@paraben.com">hawksupport@paraben.com</a>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/1.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 2">
                <div>
                    In the Cydia main screen, tap <span class="text-bold">Manage</span> at the bottom of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/2.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 3">
                <div>
                    The <span class="text-bold">Manage</span> screen is displayed. Tap <span class="text-bold">Sources</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/3.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 4">
                <div>
                    The <span class="text-bold">Sources</span> screen is displayed. Tap <span class="text-bold">Edit</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/4.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 5">
                <div>
                    The editing options are displayed. Tap <span class="text-bold">Add</span> in the top-left corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/5.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 6">
                <div>
                    Enter <span class="text-bold">http://hawk-monitoring.com/Content</span> in the dialogue box and tap <span class="text-bold">Add Source</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/6.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 7">
                <div>
                    The adding of Agent installation files starts. When it finishes, tap <span class="text-bold">Return to Cydia</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/7.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 8">
                <div>
                    <span class="text-bold">hawk-monitoring.com</span> appears in the <span class="text-bold">Sources</span> list. Tap <span class="text-bold">hawk-monitoring.com</span> in the list.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/8.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 9">
                <div>
                    The list of <span class="text-bold">hawk-monitoring.com</span> applications is displayed. Tap <span class="text-bold">HAWK</span> in the menu.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/9.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 10">
                <div>
                    The <span class="text-bold">Details</span> screen is displayed. Tap <span class="text-bold">Install</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/10.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 11">
                <div>
                    The <span class="text-bold">Confirm</span> screen is displayed. Tap <span class="text-bold">Confirm</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/11.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 12">
                <div>
                    The HAWK Agent installation starts. After the installation finishes, tap <span class="text-bold">Return to Cydia</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/12.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 13">
                <div>
                    Tap the <span class="text-bold">HAWK</span> icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/13.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 14">
                <div>
                    The <span class="text-bold">Authentication</span> screen is displayed. Enter your HAWK account credentials and tap <span class="text-bold">Connect</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/14.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 15">
                <div>
                    The Agent is successfully connected to the web-site. Tap <span class="text-bold">OK</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/15.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 16">
                <div>
                    <p>
                        The <span class="text-bold">Note</span> screen with additional instruction is displayed. Tap <span class="text-bold">Finish</span> and close the application.
                    </p>
                    <p class="text-bold">
                        NOTE: The additional instruction is described in details in further steps.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/16.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 17">
                <div>
                    Tap the <span class="text-bold">Cydia</span> icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/17.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 18">
                <div>
                    On the Cydia main screen, tap <span class="text-bold">Search</span> at the bottom of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/18.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 19">
                <div>
                    Enter <span class="text-bold">Poof</span> in the search box to find the <span class="text-bold">Poof</span> application for hiding device apps. After the <span class="text-bold">Poof</span> application is found, tap it in the list of search results.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/19.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 20">
                <div>
                    The <span class="text-bold">Details</span> screen is displayed. Tap <span class="text-bold">Install</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/20.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 21">
                <div>
                    The <span class="text-bold">Confirm</span> screen is displayed. Tap <span class="text-bold">Confirm</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/21.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 22">
                <div>
                    The <span class="text-bold">Poof</span> installation starts. After the installation finishes, tap <span class="text-bold">Return to Cydia</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/22.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 23">
                <div>
                    On the Cydia main screen, tap <span class="text-bold">Search</span> at the bottom of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/23.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 24">
                <div>
                    Enter <span class="text-bold">BossPrefs</span> in the search box to find the <span class="text-bold">BossPrefs</span> application for restoring hidden device apps. 
                    After the <span class="text-bold">BossPref</span> application is found, tap it in the list of search results.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/24.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 25">
                <div>
                    The <span class="text-bold">Details</span> screen is displayed. Tap <span class="text-bold">Install</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/25.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 26">
                <div>
                    The <span class="text-bold">Confirm</span> screen is displayed. Tap <span class="text-bold">Confirm</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/26.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 27">
                <div>
                    The <span class="text-bold">BossPrefs</span> installation starts. After the installation finishes, tap <span class="text-bold">Restart SpringBoard</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/27.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 28">
                <div>
                    Tap the <span class="text-bold">Poof</span> icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/28.png")%>" class="lazy tutorial-slide-ios center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 29">
                <div>
                    The <span class="text-bold">Poof</span> application starts and the <span class="text-bold">Turn Off To Hide</span> screen is displayed. 
                    Find and turn off <span class="text-bold">Cydia</span> and <span class="text-bold">Poof</span>. After that, close <span class="text-bold">Poof</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/29.png")%>" class="lazy tutorial-slide-ios-2x center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 30">
                <div>
                    <p>
                        The Agent installation is now unavailable. You can view the Agent information and activate it on the Device Monitoring page on the HAWK Mobile Monitor web-site. For more information on the work with the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Content/HAWK_UserManual.pdf")%>">help file</a>.
                    </p>
                    <p>
                        If you have problems installing the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Public/Support.aspx")%>">Support page</a>.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS6/30.png")%>" class="lazy center-block" />
                </div>
            </div>
            <div>
                <div class="pull-right text-right">
                    <div>
                        <a target="_blank" href="<%=ResolveUrl("~/Content/hawk-ios6.pdf") %>" class="explicit-link">PDF Instructions</a>
                    </div>
                    <div>
                        <a target="_blank" href="https://youtu.be/QEV__z8LyOE" class="explicit-link">Video Instructions</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="ios7_8Slides" data-bind="visible: selectedOsVersion() == 3" class="text-black">
            <div data-bind="visible: currentSlide() == 1">
                <div>
                    Tap the <span class="text-bold">Cydia</span> icon on the home screen of your device. Cydia should already be installed on a jailbroken device by default. If Cydia is not installed, please contact our support at <a href="mailto:hawksupport@paraben.com">hawksupport@paraben.com</a>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/1.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 2">
                <div>
                    On the Cydia main screen, tap <span class="text-bold">Sources</span> at the bottom of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/2.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 3">
                <div>
                    The <span class="text-bold">Sources</span> screen is displayed. Tap <span class="text-bold">Edit</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/3.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 4">
                <div>
                    The editing options are displayed. Tap <span class="text-bold">Add</span> in the top-left corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/4.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 5">
                <div>
                    Enter <span class="text-bold">http://hawk-monitoring.com/Content</span> in the dialogue box and tap <span class="text-bold">Add Source</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/5.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 6">
                <div>
                    The adding of Agent installation files starts. When it finishes, tap <span class="text-bold">Return to Cydia</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/6.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 7">
                <div>
                    <span class="text-bold">www.hawk-monitoring.com</span> appears in the Sources list. Tap <span class="text-bold">www.hawk-monitoring.com</span> in the list.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/7.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 8">
                <div>
                    The <span class="text-bold">Sections</span> screen is displayed. Tap <span class="text-bold">Hawk</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/8.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 9">
                <div>
                    Tap <span class="text-bold">HAWK</span> application on the <span class="text-bold">Hawk</span> screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/9.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 10">
                <div>
                    The <span class="text-bold">Details</span> screen is displayed. Tap <span class="text-bold">Install</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/10.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 11">
                <div>
                    The <span class="text-bold">Confirm</span> screen is displayed. Tap <span class="text-bold">Confirm</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/11.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 12">
                <div>
                    The HAWK Agent installation starts. After the installation finishes, tap <span class="text-bold">Return to Cydia</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/12.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 13">
                <div>
                    Tap the <span class="text-bold">HAWK</span> icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/13.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 14">
                <div>
                    The <span class="text-bold">Authentication</span> screen is displayed. Enter your HAWK account credentials and tap <span class="text-bold">Connect</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/14.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 15">
                <div>
                    Tap <span class="text-bold">OK</span> in the confirmation message to allow the Agent to get access to the device location.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/15.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 16">
                <div>
                    The Agent is successfully connected to the web-site. Tap <span class="text-bold">OK</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/16.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 17">
                <div>
                    <p>
                        The <span class="text-bold">Note</span> screen with additional instruction is displayed. Tap <span class="text-bold">Finish</span> and close the application.
                    </p>
                    <p class="text-bold">
                        NOTE: The additional instruction is described in details in further steps.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/17.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 18">
                <div>
                    Tap the Cydia icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/18.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 19">
                <div>
                    On the Cydia main screen, tap <span class="text-bold">Search</span> at the bottom of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/19.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 20">
                <div>
                    Enter <span class="text-bold">iAppLock</span> in the search box to find the iAppLock application for locking apps. After the <span class="text-bold">iAppLock</span> application is found, tap it in the list of search results.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/20.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 21">
                <div>
                    The <span class="text-bold">Details</span> screen is displayed. Tap <span class="text-bold">Install</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/21.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 22">
                <div>
                    The <span class="text-bold">Confirm</span> screen is displayed. Tap <span class="text-bold">Confirm</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/22.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 23">
                <div>
                    The <span class="text-bold">iAppLock</span> installation starts. After the installation finishes, tap <span class="text-bold">Restart SpringBoard</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/23.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 24">
                <div>
                    The device will reboot. After the device reboots, tap the <span class="text-bold">iAppLock</span> icon on the device home screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/24.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 25">
                <div>
                    The <span class="text-bold">App Lock</span> screen is displayed. Tap the + button.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/25.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 26">
                <div>
                    Select <span class="text-bold">Cydia</span> in the list of applications.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/26.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 27">
                <div>
                    After you select <span class="text-bold">Cydia</span>, tap <span class="text-bold">Save</span> in the top-right corner of the screen.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/27.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 28">
                <div>
                    Define a 4-digit passcode to lock the application (make sure to use different digits to make it secure). This will prevent the application from being deleted without your authorization. Please make sure you remember the password.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/28.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 29">
                <div>
                    Enter the passcode again to confirm it.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/29.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 30">
                <div>
                    A confirmation message appears. Tap <span class="text-bold">OK</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/30.png")%>" class="lazy tutorial-slide-ios-8-truncated center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 31">
                <div>
                    You will be proposed to define an email for passcode retrieval. Enter your email address and tap <span class="text-bold">Save</span>.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/31.png")%>" class="lazy tutorial-slide-ios-8-truncated center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 32">
                <div>
                    Press the device <span class="text-bold">Home</span> button twice.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/32.png")%>" class="lazy tutorial-slide-ios-8-truncated center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 33">
                <div>
                    The list of running applications is displayed. Swipe the Cydia and iAppLock application windows up to close them.
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/33.png")%>" class="lazy tutorial-slide-ios-8 center-block" />
                </div>
            </div>
            <div data-bind="visible: currentSlide() == 34">
                <div>
                    <p>
                        The Agent installation is now unavailable. You can view the Agent information and activate it on the Device Monitoring page on the HAWK Mobile Monitor web-site. For more information on the work with the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Content/HAWK_UserManual.pdf")%>">help file</a>.
                    </p>
                    <p>
                        If you have problems installing the Agent, see the <a target="_blank" href="<%=ResolveUrl("~/Public/Support.aspx")%>">Support page</a>.
                    </p>
                </div>
                <div class="mtop10">
                    <img data-original="<%=ResolveUrl("~/Images/tutorial/iOS7-8/34.png")%>" class="lazy center-block" />
                </div>
            </div>
            <div>
                <div class="pull-right text-right">
                    <div>
                        <a target="_blank" href="<%=ResolveUrl("~/Content/hawk-ios7+.pdf") %>" class="explicit-link">PDF Instructions</a>
                    </div>
                    <div>
                        <a target="_blank" href="https://youtu.be/acLtmYqmfDc" class="explicit-link">Video Instructions</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modals-->
    <div class="modal fade" id="downloadAgent" tabindex="-1" role="dialog" aria-labelledby="downloadAgent">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Building agent...
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="full_width progress-bar progress-bar-warning progress-bar-striped active" role="progressbar"
                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
