﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="True" CodeBehind="AccountEdit.aspx.cs" Inherits="HawkHome.User.AccountEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css") %>" />
    <script src="<%=ResolveUrl("~/JS/blockUI.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/edit_account.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div class="center_margin" runat="server" id="form_edit" visible="true">
                <div>
                    <div class="control-group">
                        <asp:Label ID="FailureText" runat="server" EnableViewState="False" CssClass="label label-important"></asp:Label>
                    </div>
                </div>
                <div class="well">
                    <div class="form-horizontal">
                        <% if (!HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
                           { %>
                        <div class="control-group">
                            <h4>Edit General Information</h4>
                            <hr class="edit_account_hr" />
                        </div>

                        <div class="form-group">
                            <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName" for="FirstName" CssClass="col-sm-2 control-label">First Name:</asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="FirstName" runat="server" placeholder="First name" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-2 mtop3">
                                <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" Display="Dynamic"
                                    ControlToValidate="FirstName" ValidationGroup="Generalinfo">
                                        <span class="alert-danger inline_alert">First name is required</span>
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName" CssClass="col-sm-2 control-label">Last Name:</asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="LastName" runat="server" placeholder="Last Name" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-2 mtop3">
                                <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" Display="Dynamic"
                                    ControlToValidate="LastName" ValidationGroup="Generalinfo">
                                             <span class="alert-danger inline_alert">Last name is required</span>
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="GeneralUserDataEdit_Click" Text="Save Changes"
                                    CssClass="btn btn-warning btn-sm label_color_black" ValidationGroup="Generalinfo">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <% } %>

                        <div class="control-group">
                            <h4>Change Password</h4>
                            <hr class="edit_account_hr" />
                        </div>
                        <div class="form-group">
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server"
                                ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="Dynamic"
                                ValidationGroup="ChangePassword1" ForeColor="White">
                                    <div class="col-sm-10 mtop3">
                                        <span class="alert-danger inline_alert" style="width:90%;">New Password and Confirm New Password fields must match</span>
                                    </div>
                            </asp:CompareValidator>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="CurrentPasswordLabel" runat="server" CssClass="col-sm-2 control-label"
                                AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" placeholder="Old Password" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-5 mtop3">
                                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" Display="Dynamic"
                                    ControlToValidate="CurrentPassword" ValidationGroup="ChangePassword1">
                                            <span class="alert-danger inline_alert">Current password is required</span>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="CurrentPasswordLength" runat="server" Display="Dynamic"
                                    ControlToValidate="CurrentPassword" ValidationGroup="ChangePassword1"
                                    ValidationExpression="^.{8,32}$">
                                            <span class="alert-danger inline_alert" style="width:auto">Your password must be 8 to 32 characters long</span>
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="NewPasswordLabel" runat="server" CssClass="col-sm-2 control-label"
                                AssociatedControlID="NewPassword">New Password:</asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" placeholder="New Password" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-5 mtop3">
                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" Display="Dynamic"
                                    ControlToValidate="NewPassword" ValidationGroup="ChangePassword1">
                                <span class="alert-danger inline_alert">New password is required</span>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="NewPasswordLength" runat="server" Display="Dynamic"
                                    ControlToValidate="NewPassword" ValidationGroup="ChangePassword1"
                                    ValidationExpression="^.{8,32}$">
                                <span class="alert-danger inline_alert" style="width:auto">Your password must be 8 to 32 characters long</span>
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>


                        <div class="form-group">
                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" CssClass="col-sm-2 control-label"
                                AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" placeholder="Repeat New Password" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-sm-5 mtop3">
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" Display="Dynamic"
                                    ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword1">
                                <span class="alert-danger inline_alert">New password confirmation is required</span>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="ConfirmNewPasswordLength" runat="server" Display="Dynamic"
                                    ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword1"
                                    ValidationExpression="^.{5,32}$">
                                <span class="alert-danger inline_alert" style="width:auto">Your password must be 8 to 32 characters long</span>
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <asp:Button ID="ChangePassword" runat="server" CssClass="btn btn-warning btn-sm label_color_black"
                                    Text="Save Changes" OnClick="ChangePassword_Click"
                                    ValidationGroup="ChangePassword1" />
                            </div>
                        </div>
                        <div id="deleteAccSection" runat="server">
                            <div>
                                <h4>Delete Account</h4>
                                <hr class="edit_account_hr" />
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="Delete" CssClass="hide pure-button pure-button-small pure-button-error label_color_black deleteAccount" />
                                    <button id="deleteAccount" type="button" class="btn btn-danger btn-sm label_color_white">Delete</button>
                                </div>
                            </div>
                            <div id="popup" class="popup_padding label_color_black popup hide">
                                <div>
                                    Warning! If you delete your account, you won’t be able to restore it. Are you sure you want delete your account?
                                </div>
                                <div class="elements_margin">
                                    <input type="button" id="yes" value="Yes" class="btn btn-sm btn-warning label_color_black" />
                                    <input type="button" id="no" value="No" class="btn btn-sm pure-button label_color_black" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="half mbottom50" runat="server" id="result_form" visible="false">
            <div class="hero-unit" runat="server" id="operation_result">
                <h4>
                    <asp:Label runat="server" ID="resultCaption">Congratulations!</asp:Label>
                </h4>
                <p>
                    <asp:Label runat="server" ID="resultDescription">Your account info has been successfully updated.</asp:Label>
                </p>
                <p class="mtop25">
                    <a id="A1" href="~/User/AccountEdit.aspx" class="btn btn-sm btn-warning label_color_black" runat="server">Go Back</a>
                    <% if (!HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
                       { %>
                    <a id="A2" href="~/User/Default.aspx" class="btn btn-sm btn-warning label_color_black mleft10" runat="server">Go to Device Monitoring</a>
                    <% } %>
                </p>
            </div>
        </div>
    </div>
</asp:Content>

