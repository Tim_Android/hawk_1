﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="MMSLog.aspx.cs" Inherits="HawkHome.MmsLogForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var page = 'mms';
    </script>
    <script src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/readmore.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/initializeReadMoreLink.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/Pages/mms_log.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <asp:GridView ID="gridMMSLog" runat="server" CssClass="table table-bordered table-striped table-condensed table-hover long_word_fix"
        AllowPaging="True" PageSize="15"
        OnPageIndexChanging="gridMMSLog_PageIndexChanging" GridLines="Horizontal"
        AutoGenerateColumns="False" DataKeyNames="ID_MSG"
        OnSelectedIndexChanged="gridMMSLog_SelectedIndexChanged" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="MSG_ADDR" HeaderText="Phone Number/Email" SortExpression="MSG_ADDR" ReadOnly="true">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="PHONE_NAME" HeaderText="Phone Name" SortExpression="PHONE_NAME" ReadOnly="true">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="DEV_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Device Date/Time" SortExpression="DEV_DATE" ReadOnly="true">
                <HeaderStyle Width="120"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="USER_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Local Date/Time" SortExpression="USER_DATE" ReadOnly="true">
                <HeaderStyle Width="120px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="MSG_BODY" HeaderText="Message Body" SortExpression="MSG_BODY" ReadOnly="true">
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle CssClass="article"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" SortExpression="TYPE_NAME" ReadOnly="true">
                <HeaderStyle Width="50px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="MSG_SUBJ" HeaderText="Subject" SortExpression="MSG_SUBJ" ReadOnly="true">
                <HeaderStyle Width="120px"></HeaderStyle>
            </asp:BoundField>
              <asp:CommandField ShowSelectButton="True" HeaderText="Attachment" ButtonType="Button" SelectText="View" ControlStyle-CssClass="btn btn-sm background_color_gray label_color_black">
                <HeaderStyle Width="80px"></HeaderStyle>
            </asp:CommandField>
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EmptyDataTemplate>
            No Data Found.
        </EmptyDataTemplate>
    </asp:GridView>

    <asp:GridView ID="gridMMSAttachments" runat="server"
        PageSize="5" CssClass="table table-bordered table-striped table-condensed table-hover shown-attachments"
        AutoGenerateColumns="False" Visible="False" Width="50%"
        OnRowCreated="gridMMSAttachments_RowCreated"
        OnSelectedIndexChanged="gridMMSAttachments_SelectedIndexChanged"
        DataKeyNames="ID_ATTACH">
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Button" SelectText="Open" ControlStyle-CssClass="btn btn-sm label_color_black">
                <HeaderStyle Width="65px"></HeaderStyle>
            </asp:CommandField>
            <asp:BoundField DataField="MIME_NAME" HeaderText="Attachment Type" SortExpression="MIME_NAME" ReadOnly="true" />
            <asp:BoundField DataField="EXT" HeaderText="Extension" SortExpression="EXT" ReadOnly="true" />
            <asp:BoundField DataField="STATUS" HeaderText="Status" SortExpression="STATUS" ReadOnly="true" />
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <div id="attachments-bottom"></div>
</asp:Content>
