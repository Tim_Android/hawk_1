﻿using HawkBilling.Core.Type;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User.Billing
{
    public partial class Receipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["trans_id"] != null)
            {
                BaseUI main = Page.Master as BaseUI;
                PaymentHistoryItemModel[] historyEntries = PaymentHistoryItemModel.GetPaymentHistoryEntry(main.userInfo.id, Request.Params["trans_id"]);
                if (historyEntries.Length > 0)
                {
                    blockReceipt.Visible = true;
                    blockNoDataMessage.Visible = false;

                    //we select any first payment entry as transaction ID and payment date will be identical 
                    //for both current and new period entries
                    PaymentHistoryItemModel entry = historyEntries.First();

                    //initializing current period data [if applicable]
                    InitCurrentPeriodData(historyEntries);
                    //initializing new period data [if applicable]
                    InitNewPeriodData(historyEntries);

                    //general info initialization
                    lblTransactionDate.InnerText = entry.paymentDate.Value.ToString(ConfigurationManager.AppSettings["defaultDateFormat"]);
                    lblTransactionId.InnerText = entry.transactionId;
                    lblUserName.InnerText = entry.firstName + " " + entry.lastName;
                    lblTotalAmount.InnerText = historyEntries.Sum(res => res.amount).ToString("F");

                    string[] comment = entry.paymentComment.Split(Convert.ToChar(ConfigurationManager.AppSettings["allowedVarsDescriptionSeparator"]));

                    if (comment.Length == 2)
                    {
                        lblAutochargeStatus.InnerText = comment[0];
                        lblPaymentSystem.InnerText = comment[1];
                    }
                }
            }
        }

        private void InitCurrentPeriodData(PaymentHistoryItemModel[] entries)
        {
            //checking if there is an entry for current period
            PaymentHistoryItemModel entry = entries.Where(res => res.originalSubscrEnd == res.afterPaymentSubscrEnd).FirstOrDefault();
            if (entry != null)
            {//filling out parts that are related to current period
                currentPeriodRow.Visible = true;
                lblCurPeriodFrom.InnerText = entry.paymentDate.Value.ToString(ConfigurationManager.AppSettings["defaultDateFormat"]);
                lblCurPeriodTo.InnerText = entry.originalSubscrEnd.Value.ToString(ConfigurationManager.AppSettings["defaultDateFormat"]);
                lblCurPeriodNewDeviceCount.InnerText = entry.deviceCount.ToString();
                lblCurPeriodTransactionAmount.InnerText = entry.amount.ToString("F");
            }
        }

        private void InitNewPeriodData(PaymentHistoryItemModel[] entries)
        {
            //checking if there is an entry for new period
            PaymentHistoryItemModel entry = entries.Where(res => res.originalSubscrEnd < res.afterPaymentSubscrEnd).FirstOrDefault();
            if (entry != null)
            {//filling out parts that are related to new period
                newPeriodRow.Visible = true;
                lblNewPeriodName.InnerText = entry.label;
                lblNewPeriodFrom.InnerText = entry.originalSubscrEnd.Value.ToString(ConfigurationManager.AppSettings["defaultDateFormat"]);
                lblNewPeriodTo.InnerText = entry.afterPaymentSubscrEnd.Value.ToString(ConfigurationManager.AppSettings["defaultDateFormat"]);
                lblNewPeriodDeviceCount.InnerText = entry.deviceCount.ToString();
                lblNewPeriodTransactionAmount.InnerText = entry.amount.ToString("F");
            }
        }
    }
}