﻿using Domain.Entities.Enums;
using Hawk.Infrastructure.BLL;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User.Billing
{
    public partial class OrderConfirm : System.Web.UI.Page
    {
        protected TotalOrderModel order;

        protected string totalsEncoded = default(string);
        protected string orderEncoded = default(string);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[ProductModel.SESSION_NAME] == null)
            {
                Server.Transfer("~/User/Billing/Billing.aspx");
            }

            order = Session[ProductModel.SESSION_NAME] as TotalOrderModel;
            order.CalculateTotals();

            //preparing serialized data for totals display
            orderEncoded = Utils.Base64Encode(JSSerializer.GetInstance().Serialize(order));

            if (!IsPostBack)
            {
                InitHelper.InitPaymentSystemsRadioGroup(this.rblPaymentSystem);
                rblPaymentSystem.SelectedIndex = 0;
            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            order = Session[ProductModel.SESSION_NAME] as TotalOrderModel;
            foreach (var item in order.Products)
            {
                if (item.productType == ProductType.PhoneSubscription)
                {
                    item.isRecurringBillingEnabled = recurringBillingPhone.Checked; //turning recurring billing on or off
                }
                if (item.productType == ProductType.SupportSubscription && rblPaymentSystem.SelectedValue == PaymentProcessors.AuthorizeNet.ToString())
                {
                    item.isRecurringBillingEnabled = recurringBillingSupport.Checked; //turning recurring billing on or off
                }
            }
             
            if (rblPaymentSystem.SelectedValue == PaymentProcessors.AuthorizeNet.ToString())
            {
                Server.Transfer("~/User/Billing/PaymentDetails.aspx");
            }
            if (rblPaymentSystem.SelectedValue == PaymentProcessors.PayPal.ToString())
            {
                BaseUI main = Page.Master as BaseUI;

                CustomerDetails custInfo = new CustomerDetails();

                custInfo.customerId = main.userInfo.id;
                custInfo.firstName = main.userInfo.firstName;
                custInfo.lastName = main.userInfo.lastName;
                custInfo.email = main.userInfo.email;

                PaymentDetails paymInfo = new PaymentDetails();
                order.CalculateTotals();
                paymInfo.amount = order.total;

                foreach (var item in order.Products)
                {
                    var separator = item != order.Products[order.Products.Count - 1] ? ", " : string.Empty;
                    paymInfo.description += item.GetOrderDescription() + separator;
                }

                PayPalDetails paypalInfo = new PayPalDetails();
                paypalInfo.returnURL = ConfigurationManager.AppSettings["PayPalReturnURL"];
                paypalInfo.cancelURL = ConfigurationManager.AppSettings["PayPalCancelURL"];
                paypalInfo.productName = Resources.Billing.ProductName;

                List<SubscriptionDetails> subscrInfoList = null;

                if (order.Products.Any(x=>x.isRecurringBillingEnabled == true))
                {
                    subscrInfoList = new List<SubscriptionDetails>();
                    foreach (var reccurringProduct in order.Products)
                    {
                        if (reccurringProduct.isRecurringBillingEnabled)
                        {
                            subscrInfoList.Add(
                           new SubscriptionDetails
                           {
                               subscriptionStartDate = reccurringProduct.GetSubscriptionStartDate(Convert.ToInt16(ConfigurationManager.AppSettings["daysPriorAutochargePayment"])),
                               billingInterval = (short)reccurringProduct.priceListItem.duration,
                               amount = reccurringProduct.CalculateSubscriptionTotals(),
                               productType = reccurringProduct.productType
                           });
                        }
                    }
                }
            

            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);

            PaypalPaymentProcessor processor = new PaypalPaymentProcessor(custInfo, paymInfo, paypalInfo, testMode, subscrInfoList);

            try
            {
                string returnUrl = Utils.ConvertRelativeUrlToAbsoluteUrl(System.Web.VirtualPathUtility.ToAbsolute(processor.payPalInfo.returnURL), HttpContext.Current.Request.Url.Port.ToString());
                string cancelUrl = Utils.ConvertRelativeUrlToAbsoluteUrl(System.Web.VirtualPathUtility.ToAbsolute(processor.payPalInfo.cancelURL), HttpContext.Current.Request.Url.Port.ToString());

                string redirectURL = processor.RequestRedirectUrl(returnUrl, cancelUrl);
                if (redirectURL != String.Empty)
                {
                    Session[PaymentProcessor.SESSION_NAME] = processor;
                    Response.Redirect(redirectURL, true);
                }
                else
                {
                    throw new PaypalInitException(Resources.Billing.PayPalInitError);
                }
            }
            catch (PaypalInitException ex)
            {
                main.SetWarningMessage(Resources.Billing.PayPalInitError);
            }
        }
    }
}
}