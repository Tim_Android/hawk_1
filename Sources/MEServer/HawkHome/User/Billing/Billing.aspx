﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="Billing.aspx.cs" Inherits="HawkHome.Public.Billing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <script src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/billing.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div id="order" class="container pure-skin-paraben ">
        <div class="mtop20">
            <div>
                <h4>Buy Subscription</h4>
                <hr class="edit_account_hr" />
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div>
                        <div id="step1" class="form-inline elements_margin">
                            Please select subscription option:
                    <select id="selectedLicense" name="selectedLicense" class="form-control input-sm">
                        <%foreach (KeyValuePair<string, string> pair in licensesSelect)
                            { %>
                        <option value="<%= pair.Key %>"><%= pair.Value %></option>
                        <%} %>
                    </select>
                        </div>
                        <div id="step2" data-bind="visible: !additionalServicesOnly()">
                            <table class="pure-table pure-table-horizontal full_width">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text_align_center text_vert_align_middle">Term</th>
                                        <th class="text_align_center text_vert_align_middle">Retail Price</th>
                                        <th class="text_align_center">Online Special<br />
                                            Price Per Device in<br />
                                            U.S. Dollars</th>
                                        <th class="text_align_center text_vert_align_middle">Subscription End</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: { data: plans, afterAdd: showFade }">
                                    <tr>
                                        <td>
                                            <input type="radio" name="priceListId" data-bind="value: id, checked: $root.selectedPlanId" class="radio" />
                                        </td>
                                        <td class="text_align_center"><span class="label_color_black" data-bind="text: label"></span></td>
                                        <td class="text_align_center">$<span data-bind="text: retailPrice"></span> ($<span data-bind="text: (retailPrice - price).toFixed(2)"></span> Savings)</td>
                                        <td class="text_align_center">$<span data-bind="text: price"></span></td>
                                        <td class="text_align_center"><span data-bind="text: subscriptionEnd"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-inline elements_margin">
                                Add devices to subscription:
                            <input name="devCount" type="number" data-bind="value: devices, valueUpdate: 'afterkeydown'" class="form-control input-sm" min="0" max="9999" step="1" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="step3" class="form-inline elements_margin">
                            Additional services:
                        </div>
                        <div id="step4">
                            <table class="pure-table pure-table-horizontal full_width">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text_align_center text_vert_align_middle">Term</th>
                                        <th class="text_align_center">Price in U.S. Dollars</th>
                                        <th class="text_align_center text_vert_align_middle">Subscription End</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: { data: phoneSupportPlans, afterAdd: showFade }">
                                    <tr>
                                        <td>
                                            <input type="radio" name="supportPriceListId" data-bind="value: id, checked: $root.selectedSupportPlanId" class="radio" />
                                        </td>
                                        <td class="text_align_center"><span class="label_color_black" data-bind="text: label"></span></td>
                                        <td class="text_align_center">$<span data-bind="text: price"></span></td>
                                        <td class="text_align_center"><span data-bind="text: subscriptionEnd"></span></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="well">
                        <div class="elements_margin">
                            <h5>Shopping Cart</h5>
                            <hr class="edit_account_hr" />
                            <span data-bind="visible: (!order().oldPeriodDisplay() && !order().newPeriodDisplay() && order().supportTotal() == 0)">Your shopping cart is empty</span>
                            <table class="pure-table pure-table-horizontal full_width" data-bind="visible: order().oldPeriodDisplay()">
                                <thead>
                                    <tr>
                                        <th colspan="4">Price for new devices in current period</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="totals_col_width">Device Count</td>
                                    <td class="totals_col_width">Period</td>
                                    <td class="totals_col_width">Price per device</td>
                                    <td class="text-right totals_width">Total:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <span data-bind="text: order().deviceNumber"></span>&nbsp;device<span data-bind="visible: order().deviceNumber() > 1">s</span>
                                    </td>
                                    <td>
                                        <span data-bind="text: order().residualDays"></span>&nbsp;day(s) left
                                    </td>
                                    <td>$<span data-bind="text: order().oldPeriodCostEachFormatted"></span>
                                    </td>
                                    <td class="text-right totals_width">$<span data-bind="text: order().oldPeriodCostTotal"></span>
                                    </td>
                                </tr>
                            </table>
                            <table class="pure-table pure-table-horizontal full_width" data-bind="visible: order().newPeriodDisplay() && !additionalServicesOnly()">
                                <thead>
                                    <tr>
                                        <th colspan="4">Price for all devices in new period</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="totals_col_width">Device Count</td>
                                    <td class="totals_col_width">Period</td>
                                    <td class="totals_col_width">Price per device</td>
                                    <td class="text-right totals_width">Total:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <span data-bind="visible: (order().oldPeriodDisplay() && order().newPeriodDisplay())">
                                            <span data-bind="text: order().existingDevNumber() + order().deviceNumber()"></span>&nbsp;device<span data-bind="    visible: (order().existingDevNumber() + order().deviceNumber()) > 1">s</span><br />
                                            (<span data-bind="text: order().existingDevNumber()"></span>&nbsp;existing, <span data-bind="    text: order().deviceNumber()"></span>&nbsp;new)
                                        </span>
                                        <span data-bind="visible: (!order().oldPeriodDisplay() && order().newPeriodDisplay())">
                                            <span data-bind="visible: showNewDevices()"><span data-bind="text: order().deviceNumber()"></span>&nbsp;device<span data-bind="    visible: order().deviceNumber() > 1">s</span></span>
                                            <span data-bind="visible: showExistingDevices()"><span data-bind="text: order().existingDevNumber()"></span>&nbsp;device<span data-bind="    visible: order().existingDevNumber() > 1">s</span></span>
                                        </span>
                                    </td>
                                    <td><span data-bind="text: selectedPlan().label"></span></td>
                                    <td>$<span data-bind="text: selectedPlan().price"></span></td>
                                    <td class="text-right">
                                        <span data-bind="visible: (order().oldPeriodDisplay() && order().newPeriodDisplay())">$<span data-bind="text: order().newPeriodCostTotalFormatted()"></span>
                                        </span>
                                        <span data-bind="visible: (!order().oldPeriodDisplay() && order().newPeriodDisplay())">$<span data-bind="    text: order().deviceTotal()"></span>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <table class="pure-table pure-table-horizontal full_width" data-bind="visible: order().supportTotal() > 0">
                                <thead>
                                    <tr>
                                        <th colspan="4">Price for Additional services</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="totals_col_width">Service</td>
                                    <td class="totals_col_width">Period</td>
                                    <td class="totals_col_width">Price</td>
                                    <td class="text-right totals_width">Total:</td>
                                </tr>
                                <tr>
                                    <td>Phone Support
                                    </td>
                                    <td><span data-bind="text: selectedSupportPlan().label"></span></td>
                                    <td>$<span data-bind="text: selectedSupportPlan().price"></span></td>
                                    <td class="text-right">
                                        <span data-bind="visible: order().supportTotal() > 0">$<span data-bind="    text: order().supportTotal()"></span>
                                        </span>

                                    </td>
                                </tr>
                            </table>
                            <table class="pure-table pure-table-horizontal full_width subscription-details-content" data-bind="visible: order().newPeriodDisplay() || order().supportTotal() > 0">
                                <tr>
                                    <td class="text-bold text-right" colspan="3">Order Total:
                                    </td>
                                    <td class="text-bold text-right totals_width">$<span data-bind="text: order().total"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix">
                        <hr />
                        <asp:LinkButton ID="btnCheckout" runat="server" class="btn btn-sm btn-success label_color_black checkout_button pull-right" OnClick="btnCheckout_Click" data-bind="disableClick: !order().continueEnabled()">Continue <span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
