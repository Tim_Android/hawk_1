﻿using HawkBilling.Core;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.Public
{
    public partial class Billing : System.Web.UI.Page
    {
        protected Dictionary<string, string> licensesSelect;

        protected void Page_Init(object sender, EventArgs e)
        {
            licensesSelect = new Dictionary<string, string>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Remove(ProductModel.SESSION_NAME);
            Session.Remove(PaymentProcessor.SESSION_NAME);

            BaseUI main = Page.Master as BaseUI;
            var phoneSupportIds = GetSupportPrices();

            LicenseModel[] licenses = LicenseModel.GetLicenseList(main.userInfo.id, null)
                                                        .Where(x => !phoneSupportIds.Contains((int)x.priceListId))
                                                        .ToArray();
            licenses.OrderBy(res => res.subscrEndDate); // ordering by subscription end date

            licensesSelect.Add(ProductModel.NEW_LICENSE_VALUE, Resources.Billing.CreateNewLic);
            licensesSelect.Add(ProductModel.NO_LICENSE_VALUE, Resources.Billing.NoSubscription);
            string licenseNameTemplate = Resources.Billing.LicenseNameTemplate;
            foreach (LicenseModel license in licenses)
            {
                string licenseName = String.Format(licenseNameTemplate, license.licenseName, license.devices);
                licensesSelect.Add(license.id.ToString(), licenseName);
            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            var orderCase = PrepareOrderCase();

            if (orderCase.Products.Count > 0)
            {
                Session[ProductModel.SESSION_NAME] = orderCase;
                Response.Redirect("~/User/Billing/OrderConfirm.aspx", true);
            }
            else
            {
                BaseUI main = Page.Master as BaseUI;
                main.SetWarningMessage(Resources.Billing.EmptyCart);
            }

        }

        private bool CheckTotal(ProductModel order)
        {
            return order.CalculateTotals().total != 0;
        }

        private TotalOrderModel PrepareOrderCase()
        {
            var order = new TotalOrderModel();
            var licenseProduct = PrepareLicenseProduct();
            var supportProduct = PrepareSupportProduct();
            if (licenseProduct != null)
            {
                AddOrder(order, licenseProduct);
            }
            if (supportProduct != null)
            {
                AddOrder(order, supportProduct);
            }
            return order;
        }

        private void AddOrder(TotalOrderModel orderCase, ProductModel licenseOrder)
        {
            if (licenseOrder.isValid && CheckTotal(licenseOrder))
            {
                orderCase.Products.Add(licenseOrder);
            }
        }

        private ProductModel PrepareLicenseProduct()
        {
            DateTime? subscrEndDate = null;
            if (Request.Params[ProductModel.SELECTED_LICENSE] == ProductModel.NO_LICENSE_VALUE)
            {
                return null;
            }
            if (Request.Params[ProductModel.SELECTED_LICENSE] != ProductModel.NEW_LICENSE_VALUE)
            {//for old subscriptions we use their subscrEnd date as base date for price list calculation
                LicenseModel licInfo = LicenseModel.GetLicenseInfo(Convert.ToInt32(Request.Params[ProductModel.SELECTED_LICENSE]));
                subscrEndDate = licInfo.subscrEndDate;
            }

            int? priceListId = Convert.ToInt32(Request.Params[ProductModel.PRICE_LIST_ID]);
            if (Request.Params[ProductModel.PRICE_LIST_ID] == ProductModel.CURRENT_PERIOD_PRICE_LIST_VALUE)
            {//getting old price list id if we add only new devices
                LicenseModel licInfo = LicenseModel.GetLicenseInfo(Convert.ToInt32(Request.Params[ProductModel.SELECTED_LICENSE]));
                priceListId = licInfo.priceListId;
            }

            PriceListItemModel priceListItem = PriceListItemModel.GetPriceListForSubscrExtend(PriceListItemType.SubscriptionItem, 
                                                                                              subscrEndDate, 
                                                                                              Resources.Billing.SubscriptionExtendLabel, 
                                                                                              priceListId)
                                                                 .FirstOrDefault(res => res.id == Convert.ToInt32(Request.Params[ProductModel.PRICE_LIST_ID]));

            ProductModel order = new ProductModel(priceListItem, 
                                              Convert.ToInt32(Request.Params[ProductModel.DEV_COUNT]), 
                                              Convert.ToInt32(Request.Params[ProductModel.SELECTED_LICENSE]));

            InitHelper.InitOrderMessages(order);

            order.Validate();
            return order;
        }
        private ProductModel PrepareSupportProduct()
        {
            DateTime? subscrEndDate = null;
            

            int? priceListId = Convert.ToInt32(Request.Params[ProductModel.SUPPORT_PRICE_LIST_ID]);
            if (Request.Params[ProductModel.SUPPORT_PRICE_LIST_ID] == ProductModel.NEW_LICENSE_VALUE)
            {
                return null;
            }

            PriceListItemModel priceListItem = PriceListItemModel.GetPriceList(PriceListItemType.PhoneSupport, DateTime.UtcNow)
                                            .First(x => x.id == priceListId);

            var licenseId = GetSupportLicenseId();
            licenseId = licenseId ?? 0;
            var order = new SupportProductModel(priceListItem, (int)licenseId);

            InitHelper.InitOrderMessages(order);

            order.Validate();
            return order;
        }

        private static List<int> GetSupportPrices()
        {
            return PriceListItemModel.GetPriceList(PriceListItemType.PhoneSupport, DateTime.UtcNow)
                                            .Where(x => x.type == PriceListItemType.PhoneSupport)
                                            .Select(x => x.id)
                                            .ToList();
        }
        private static int? GetSupportLicenseId()
        {
            int? supportLicense = null;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var license = LicenseModel.GetLicenseList(GetUserInfo().id, null)
                                                        .FirstOrDefault(x => GetSupportPrices()
                                                        .Contains((int)x.priceListId));
                if (license != null)
                {
                    supportLicense = license.id;
                }
            }

            return supportLicense;
        }
        private static UserInfoModel GetUserInfo()
        {
            var aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
            var userInfo = UserInfoModel.GetUserInfo(aspUserId);
            return userInfo;
        }

    }
}