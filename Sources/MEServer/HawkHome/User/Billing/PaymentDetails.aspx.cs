﻿using Domain.Entities.Enums;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.PaymentProcessors;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User.Billing
{
    public partial class AuthorizeNet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[ProductModel.SESSION_NAME] != null)
            {
                if (!IsPostBack)
                {
                    PopulateMonth(this.ddlMonth);
                    PopulateYear(this.ddlYear);
                    ddlMonth.SelectedIndex = DateTime.UtcNow.Month - 1; //indexes are zero-based
                }
            }
            else
            {
                Server.Transfer("~/Public/Error/NotAuthorized.aspx");
            }
        }

        protected void PopulateMonth(DropDownList month)
        {
            for (int i = 1; i < 13; i++)
            {
                DateTime monthItem = new DateTime(DateTime.UtcNow.Year, i, 1);
                ListItem li = new ListItem(monthItem.ToString("MM"), monthItem.ToString("MM"));
                month.Items.Add(li);
            }
        }

        protected void PopulateYear(DropDownList year)
        {
            for (int i = 0; i < 25; i++)
            {
                string yearItem = (DateTime.Today.Year + i).ToString();
                ListItem li = new ListItem(yearItem, yearItem);
                year.Items.Add(li);
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);

            BaseUI main = Page.Master as BaseUI;


            CustomerDetails custInfo = new CustomerDetails
            {
                customerId = main.userInfo.id,
                firstName = main.userInfo.firstName,
                lastName = main.userInfo.lastName,
                email = main.userInfo.email
            };

            var order = Session[ProductModel.SESSION_NAME] as TotalOrderModel;
            order.CalculateTotals();

            PaymentDetails paymInfo = new PaymentDetails
            {
                CC = tbCreditCard.Text,
                CVV = tbCVV.Text,
                amount = order.total,
                expirationMonth = Convert.ToInt32(ddlMonth.SelectedValue),
                expirationYear = Convert.ToInt32(ddlYear.SelectedValue)
            };

            foreach (var item in order.Products)
            {
                var separator = item != order.Products[order.Products.Count - 1] ? ", " : string.Empty;
                paymInfo.description += item.GetOrderDescription() + separator;
            }


            List<SubscriptionDetails> subscrInfoList = null;
            if (order.Products.Any(x => x.isRecurringBillingEnabled == true))
            {
                subscrInfoList = new List<SubscriptionDetails>();
                foreach (var reccurringProduct in order.Products)
                {
                    if (reccurringProduct.isRecurringBillingEnabled)
                    {
                        subscrInfoList.Add(
                       new SubscriptionDetails
                       {
                           subscriptionStartDate = reccurringProduct.GetSubscriptionStartDate(Convert.ToInt16(ConfigurationManager.AppSettings["daysPriorAutochargePayment"])),
                           billingInterval = (short)reccurringProduct.priceListItem.duration,
                           amount = reccurringProduct.CalculateSubscriptionTotals(),
                           productType = reccurringProduct.productType
                       });
                    }
                }
            }

            AuthorizeNetPaymentProcessor processor = new AuthorizeNetPaymentProcessor(custInfo, paymInfo, testMode, subscrInfoList);
            if (processor.AuthorizePayment())
            {
                Session[PaymentProcessor.SESSION_NAME] = processor;
                Response.Redirect(ConfigurationManager.AppSettings["PayPalReturnURL"], true);
            }
            else
            {
                main.SetErrorMessage(processor.paymentInfo.operResult.message);
            }
        }
    }
}