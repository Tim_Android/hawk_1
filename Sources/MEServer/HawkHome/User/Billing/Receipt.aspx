﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="True" CodeBehind="Receipt.aspx.cs" Inherits="HawkHome.User.Billing.Receipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/print.css") %>" media="print" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div id="blockReceipt" runat="server" visible="false">
                <table class="full_width">
                    <tr>
                        <td class="half">
                            <img src="/Images/footer-logo.png" />

                        </td>
                        <td>
                            <div>
                                <h3>Invoice Acknowledgement</h3>
                            </div>
                            <div>
                                <h5>Transaction Information</h5>
                                Date <span id="lblTransactionDate" runat="server"></span>
                                <br>
                                ID <span id="lblTransactionId" runat="server"></span>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="valign_top">
                            <div class="elements_margin">
                                Sold by:
                    <br>
                                Paraben Corporation<br>
                                39344 John Mosby Hwy Ste 277<br>
                                Aldie VA 20105-2000<br>
                                Tel: 1.801.796.0944<br>
                                USA<br>
                            </div>
                        </td>
                        <td class="valign_top">
                            <div class="elements_margin">
                                Sold To: 
                    <span id="lblUserName" runat="server"></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="full_width small_top_margin">
                    <tr>
                        <th class="table_cell_solid_border">Description</th>
                        <th class="table_cell_solid_border">Total Price USD</th>
                    </tr>
                    <tr id="currentPeriodRow" runat="server" visible="false">
                        <td class="table_cell_solid_border valign_top">Current Period (<span id="lblCurPeriodFrom" runat="server"></span> - <span id="lblCurPeriodTo" runat="server"></span>)
                    <br>
                            Added devices: <span id="lblCurPeriodNewDeviceCount" runat="server"></span>
                        </td>
                        <td class="table_cell_solid_border text_align_center">$<span id="lblCurPeriodTransactionAmount" runat="server"></span>
                        </td>
                    </tr>
                    <tr id="newPeriodRow" runat="server" visible="false">
                        <td class="table_cell_solid_border valign_top">New Period - <span id="lblNewPeriodName" runat="server"></span>(<span id="lblNewPeriodFrom" runat="server"></span> - <span id="lblNewPeriodTo" runat="server"></span>)
                    <br>
                            Total devices: <span id="lblNewPeriodDeviceCount" runat="server"></span>
                        </td>
                        <td class="table_cell_solid_border text_align_center">$<span id="lblNewPeriodTransactionAmount" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="table_cell_solid_border text_align_center">$<span id="lblTotalAmount" runat="server"></span>
                        </td>
                    </tr>
                </table>
                <div>
                    <h4 class="text_align_center">Autocharge feature is <span id="lblAutochargeStatus" runat="server"></span></h4>
                    <h4 class="text_align_center">PAID in FULL by <span id="lblPaymentSystem" runat="server"></span></h4>
                </div>
                <a id="print_button" href="#" onclick="window.print();" class="btn btn-sm btn-warning label_color_black monitored_devices_button"><span class="glyphicon glyphicon-print"></span>Print</a>
            </div>
            <div id="blockNoDataMessage" runat="server">
                There is no receipt data to display
            </div>
        </div>
    </div>
</asp:Content>
