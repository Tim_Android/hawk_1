﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="OrderConfirm.aspx.cs" Inherits="HawkHome.User.Billing.OrderConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <script src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/jquery.base64.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/order_confirm.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>Confirm Order</h4>
                <hr class="edit_account_hr" />
            </div>
             <input id="order" type="hidden" value="<%= orderEncoded %>" />
            <div class="col-sm-7 well">
                 <div data-bind="foreach: order().products">
                <div class="row" data-bind="visible: $data.productType() == PHONE_SUBSCRIPTION">
                <div class="col-sm-12">
                    <div id="orderTotals" class="elements_margin">
                       
                        <table class="pure-table pure-table-horizontal full_width" data-bind="visible: $data.oldPeriodDisplay()">
                            <thead>
                                <tr>
                                    <th colspan="4">Price for new devices in current period</th>
                                </tr>
                            </thead>
                            <tr>
                                <td class="totals_col_width">Device Count</td>
                                <td class="totals_col_width">Period</td>
                                <td class="totals_col_width">Price per device</td>
                                <td class="text-right totals_width">Total:</td>
                            </tr>
                            <tr>
                                <td>
                                    <span data-bind="text: deviceNumber"></span>&nbsp;device<span data-bind="visible: $data.deviceNumber > 1">s</span>
                                </td>
                                <td>
                                    <span data-bind="text: residualDays"></span>&nbsp;day(s) left
                                </td>
                                <td>$<span data-bind="text: oldPeriodCostEachFormatted"></span>
                                </td>
                                <td class="text-right totals_width">$<span data-bind="text: oldPeriodCostTotal"></span>
                                </td>
                            </tr>
                        </table>
                        <table class="pure-table pure-table-horizontal full_width" data-bind="visible: $data.newPeriodDisplay()">
                            <thead>
                                <tr>
                                    <th colspan="4">Price for all devices in new period</th>
                                </tr>
                            </thead>
                            <tr>
                                <td class="totals_col_width">Device Count</td>
                                <td class="totals_col_width">Period</td>
                                <td class="totals_col_width">Price per device</td>
                                <td class="text-right totals_width">Total:</td>
                            </tr>
                            <tr>
                                <td>
                                    <span data-bind="visible: ($data.oldPeriodDisplay() && $data.newPeriodDisplay())">
                                        <span data-bind="text: existingDevNumber + deviceNumber"></span>&nbsp;device<span data-bind="visible: ($data.existingDevNumber + $data.deviceNumber) > 1">s</span><br />
                                        (<span data-bind="text: existingDevNumber"></span>&nbsp;existing, <span data-bind="text: deviceNumber"></span>&nbsp;new)
                                    </span>
                                    <span data-bind="visible: (!$data.oldPeriodDisplay() && $data.newPeriodDisplay())">
                                        <span data-bind="visible: $data.showNewDevices()"><span data-bind="text: deviceNumber"></span>&nbsp;device<span data-bind="visible: deviceNumber > 1">s</span></span>
                                        <span data-bind="visible: $data.showExistingDevices()"><span data-bind="text: existingDevNumber"></span>&nbsp;device<span data-bind="visible: existingDevNumber > 1">s</span></span>
                                    </span>
                                </td>
                                <td><span data-bind="text: selectedPlan().label"></span></td>
                                <td>$<span data-bind="text: selectedPlan().price"></span></td>
                                <td class="text-right">
                                    <span data-bind="visible: ($data.oldPeriodDisplay() && $data.newPeriodDisplay())">$<span data-bind="text: newPeriodCostTotalFormatted()"></span>
                                    </span>
                                    <span data-bind="visible: (!$data.oldPeriodDisplay() && $data.newPeriodDisplay())">$<span data-bind="text: total"></span>
                                    </span>
                                </td>
                            </tr>
                        </table>
                       
                    </div>
                   
                </div>
            </div>
             
                    <div class="row" data-bind="visible: $data.productType() == PHONE_SUBSCRIPTION">
                <div class="col-sm-12">
                                <div class="checkbox">
                                    <label>
                                        <input id="recurringBillingPhone" runat="server" type="checkbox" name="recurringBilling" />
                                        Automatically charge my payment method every billing period for device subscription only 
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                 <div class="row" data-bind="visible: $data.productType() == SUPPORT_SUBSCRIPTION">
                <div class="col-sm-12">
                    <div id="supportProduct" class="elements_margin">
                       
                        <table class="pure-table pure-table-horizontal full_width" data-bind="visible: $data.oldPeriodDisplay()">
                            <thead>
                                <tr>
                                    <th colspan="4">Price of Phone Support subscription for current period</th>
                                </tr>
                            </thead>
                            <tr>
                                <td class="totals_col_width">Count</td>
                                <td class="totals_col_width">Period</td>
                                <td class="totals_col_width">Price</td>
                                <td class="text-right totals_width">Total:</td>
                            </tr>
                            <tr>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span data-bind="text: residualDays"></span>&nbsp;day(s) left
                                </td>
                                <td>
                                </td>
                                <td class="text-right totals_width">$<span data-bind="text: oldPeriodCostTotal"></span>
                                </td>
                            </tr>
                        </table>
                        <table class="pure-table pure-table-horizontal full_width" data-bind="visible: $data.newPeriodDisplay()">
                            <thead>
                                <tr>
                                    <th colspan="4">Price of Phone Support subscription for current period</th>
                                </tr>
                            </thead>
                            <tr>
                                <td class="totals_col_width">Count</td>
                                <td class="totals_col_width">Period</td>
                                <td class="totals_col_width">Price</td>
                                <td class="text-right totals_width">Total:</td>
                            </tr>
                            <tr>
                                <td>
                                    1
                                </td>
                                <td><span data-bind="text: selectedPlan().label"></span></td>
                                <td>$<span data-bind="text: selectedPlan().price"></span></td>
                                <td class="text-right">
                                   
                                    <span data-bind="visible: (!$data.oldPeriodDisplay() && $data.newPeriodDisplay())">$<span data-bind="text: total"></span>
                                    </span>
                                </td>
                            </tr>
                        </table>
                       
                    </div>
                   
                </div>
            </div>
                      <div class="row" data-bind="visible: $data.productType() == SUPPORT_SUBSCRIPTION && !$root.isPayPal()">
                            <div class="col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input id="recurringBillingSupport" runat="server" type="checkbox" name="recurringBilling" />
                                            Automatically charge my payment method every billing period
                                        </label>
                                    </div>
                             </div>
                       </div>
                  </div>
          
                 <div class="row">
                      <div class="col-sm-12">
                         <table class="pure-table-horizontal full_width subscription-details-content">
                            <tr>
                                <td class="text-bold text-right" colspan="3">Order Total:
                                </td>
                                <td class="text-bold text-right totals_width">$<span data-bind="text: order().total"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                 </div>
       
                 <div class="row">
                <div class="col-sm-12">
                    <div id="paymentSystem" class="elements_margin">
                        <div class="payment_margin">
                            <div>
                                Please select a payment option:
                            </div>
                            <div class="payment_system_list_margin">
                                <asp:RadioButtonList runat="server" ID="rblPaymentSystem" CssClass="radio asp_radio_fix options"></asp:RadioButtonList>
                            </div>
                            
                        </div>
                        <div class="clearfix">
                            <hr />
                            <asp:LinkButton runat="server" ID="btnCheckout" CssClass="btn btn-sm btn-success label_color_black checkout_button pull-right" OnClick="btnCheckout_Click">Checkout <span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                        </div>
                    </div>

             </div>
            </div>
            </div>
        </div>
    </div>
</asp:Content>
