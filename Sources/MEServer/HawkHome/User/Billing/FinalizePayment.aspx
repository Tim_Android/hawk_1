﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="FinalizePayment.aspx.cs" Inherits="HawkHome.User.Billing.FinalizePayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%=ResolveUrl("~/JS/Pages/finalize_Payment.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4><span id="lblPaymentResult" runat="server"></span></h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div>
                    <span id="lblPaymentResultText" runat="server"></span><span id="lblLicenseName" runat="server"></span>
                </div>
                <div>
                    <div id="lblSubscriptionResult" runat="server" class="elements_margin" visible="false">

                        <div id="lblCreationResult" runat="server"></div>
                        <div id="divErrorDetails" runat="server" visible="false">Details: <span id="lblSubscrErrorDetails" runat="server" class="text-error"></span></div>
                    </div>
                    <div id="lblselectBehavior" runat="server" visible="false"></div>
                    <div id="lblGoToActivate" runat="server" visible="false"></div>
                </div>
                <div class="row" runat="server" id="behaviorDiv" visible="false">
                    <div class="payment_system_list_margin">
                        <asp:RadioButtonList runat="server" ID="afterBoughtBehavior" CssClass="radio asp_radio_fix"></asp:RadioButtonList>
                    </div>
                    <div>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="ddlDevices" runat="server" AutoPostBack="false" CssClass="zero_margin_bottom form-control"></asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div>
                    <asp:Label ID="lblNotExistFreeDevices" runat="server" Visible="false" CssClass="text-left-important"></asp:Label>
                    <input id="notExistFreeDevices" runat="server" type="hidden" value="false" />
                </div>
                <div>
                    <div class="elements_margin label_color_black">
                        <asp:LinkButton runat="server" ID="btnContinue" CssClass="btn btn-sm btn-warning label_color_black monitored_devices_button">Continue</asp:LinkButton>
                        <a id="lnkRedirect" visible="false" runat="server" href="~/User/Billing/FinalizePayment.aspx" class="btn btn-sm btn-warning label_color_black monitored_devices_button">Continue</a>
                        <a id="lnkReceipt" runat="server" href="" target="_blank" class="btn btn-sm label_color_black monitored_devices_button" visible="false">Print Receipt</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
