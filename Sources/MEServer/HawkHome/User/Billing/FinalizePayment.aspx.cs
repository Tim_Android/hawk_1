﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities.Enums;
using Domain.Entities.Filters;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkCommonResources;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User.Billing
{
    public partial class FinalizePayment : System.Web.UI.Page
    {
        protected bool paymentSuccess = false;
        private bool? _isPaid;
        private BaseUI _main;
        private int? _licenseId;

        public int? LicenseId
        {
            get
            {
                if (_licenseId != null)
                {
                    return _licenseId;
                }
                else
                {
                    return (int?)Session["licenseId"];
                }
            }
            set
            {
                _licenseId = value;
                Session["licenseId"] = _licenseId;
            }
        }
        public bool? IsPayed
        {
            get
            {
                if (_licenseId != null)
                {
                    return _isPaid;
                }
                else
                {
                    return (bool?)Session["isPayed"];
                }
            }
            set
            {
                _isPaid = value;
                Session["isPayed"] = _isPaid;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _main = (Page.Master as BaseUI);


            if (IsPayed != null && IsPayed == true)
            {
                if (afterBoughtBehavior.SelectedValue == AfterBoughtBehavior.ActivateSubscriptionOn.ToString())
                {
                    ActivateDevice(int.Parse(ddlDevices.SelectedValue), (int)LicenseId);
                    IsPayed = null;
                    LicenseId = null;
                }
            }

            if (Session[PaymentProcessor.SESSION_NAME] != null && Session[ProductModel.SESSION_NAME] != null)
            {
                PaymentProcessor processor = Session["paymentProcessor"] as PaymentProcessor;
                var order = Session[ProductModel.SESSION_NAME] as TotalOrderModel;

                Dictionary<string, string> additionalParams = null;

                if (HttpContext.Current.Request.Params["token"] != null && HttpContext.Current.Request.Params["PayerID"] != null)
                {//then this is PayPal response and we should initialize additional parameters to Processpayment
                    additionalParams = new Dictionary<string, string>();
                    additionalParams.Add("token", HttpContext.Current.Request.Params["token"]);
                    additionalParams.Add("PayerID", HttpContext.Current.Request.Params["PayerID"]);
                }

                try
                {
                    foreach (var item in order.Products)
                    {
                        if (item.licenseId > 0 && item.licenseId != null)
                        {//we need to check whether we should cancel previous subscription
                            LicenseModel license = LicenseModel.GetLicenseInfo((int)item.licenseId);
                            if (license.isAutochargeEnabled)
                            {//if subscription exist we have to cancel old and create new one
                                if (!ResolveSubscriptionCancellation(license.subscriptionId, license.type))
                                {
                                    lnkRedirect.Visible = true;
                                    lnkRedirect.HRef = ConfigurationManager.AppSettings["OrderConfirmURL"];
                                    lnkRedirect.InnerText = Resources.General.labelBack;
                                    lblPaymentResult.InnerText = Resources.Error.PaymentHeader;
                                    lblPaymentResultText.InnerText = processor.paymentInfo.operResult.message;
                                }
                                else
                                {
                                    LicenseModel.UpdateLicenseSubscrId(license.id, null);
                                }
                            }
                        }
                       
                    }
                    processor.ProcessPayment(additionalParams);

                }
                catch (Exception ex)
                {
                    lnkRedirect.Visible = true;
                    lnkRedirect.HRef = ConfigurationManager.AppSettings["OrderConfirmURL"];
                    lnkRedirect.InnerText = Resources.General.labelBack;
                    lblPaymentResult.InnerText = Resources.Error.PaymentHeader;
                    lblPaymentResultText.InnerText = ex.Message;
                }

                switch (processor.paymentInfo.operResult.result)
                {
                    case PaymentResult.Success:
                        paymentSuccess = true;
                        lblPaymentResult.InnerText = Resources.Success.PaymentHeader;
                        BaseUI main = Page.Master as BaseUI;

                        foreach (var product in order.Products)
                        {
                            int? licenseId = null;
                            int? paymentId = null;

                            string comment = PaymentHistoryItemModel.BuildCommentString(order.isRecurringBillingEnabled, processor.paymentProcessor);
                            string subscriptionId = null;
                            if (processor.subscrInfo != null)
                            {
                                foreach (var subscription in processor.subscrInfo)
                                {
                                    if (subscription.productType == product.productType)
                                    {
                                        subscriptionId = subscription.operResult.subscriptionId;
                                    }
                                }
                            }
                            
                            

                            if (AccountManagement.FinalizePayment(product, (int)main.userInfo.id, subscriptionId, processor, out licenseId, out paymentId, comment))
                            {
                                if (product as SupportProductModel == null)
                                {
                                    if (!HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.PAID))
                                    {
                                        //enabling user account
                                        Roles.AddUserToRole(Context.User.Identity.Name, HawkDataAccess.Const.AvailableRoles.PAID);
                                    }

                                    if (product.type == SubscriptionPaymentType.NewSubscription || product.type == SubscriptionPaymentType.SignupFee)
                                    {
                                        lblLicenseName.InnerText = String.Format(Resources.Billing.NewSubscrLabel, licenseId.Value.ToString());
                                    }

                                    if (!IsPostBack && product.type == SubscriptionPaymentType.NewSubscription)
                                    {

                                        IList<DeviceInfo> deviceList = DeviceManagement.GetAgentsForUser(_main.aspUserId, true).Where(x => x.licId == null)
                                                                                                                               .ToList();

                                        InitHelper.InitAfterBoughtBehavior(afterBoughtBehavior);
                                        afterBoughtBehavior.SelectedIndex = 0;
                                        LicenseId = licenseId;
                                        IsPayed = true;
                                        behaviorDiv.Visible = true;
                                        lblselectBehavior.Visible = true;
                                        lblselectBehavior.InnerText = Resources.Success.SelectBehavior;
                                        ddlDevices.Enabled = false;
                                        if (deviceList.Count > 0)
                                        {
                                            foreach (var deviceItem in deviceList)
                                            {
                                                ddlDevices.Items.Add(new ListItem(deviceItem.device, deviceItem.id.ToString()));
                                            }
                                        }
                                        else
                                        {
                                            afterBoughtBehavior.Items.FindByValue(AfterBoughtBehavior.ActivateSubscriptionOn.ToString()).Enabled = false;
                                            lblNotExistFreeDevices.Text = SubscriptionManagement.AllDevicesAssigned;
                                            lblNotExistFreeDevices.Visible = true;
                                            notExistFreeDevices.Value = "true";
                                        }
                                    }
                                    else
                                    {
                                        lblGoToActivate.Visible = true;
                                        lblGoToActivate.InnerText = Resources.Success.GoToMonitoringPage;
                                    }
                                }
                            }

                            lnkReceipt.Visible = true;
                            lnkReceipt.HRef = String.Format(ConfigurationManager.AppSettings["ReceiptURL"], processor.paymentInfo.operResult.transactionId);


                            Session.Remove(ProductModel.SESSION_NAME);
                            Session.Remove(PaymentProcessor.SESSION_NAME);
                        }
                        break;
                    case PaymentResult.Error:
                        lnkRedirect.Visible = true;
                        lblPaymentResult.InnerText = Resources.Error.PaymentHeader;
                        lnkRedirect.HRef = ConfigurationManager.AppSettings["OrderConfirmURL"];
                        lnkRedirect.InnerText = Resources.General.labelBack;
                        Session.Remove(PaymentProcessor.SESSION_NAME);
                        break;
                }

                lblPaymentResultText.InnerText = processor.paymentInfo.operResult.message;

                if (processor.subscrInfo != null)
                {//add subscription section to results 
                    lblSubscriptionResult.Visible = true;
                    foreach (var item in processor.subscrInfo)
                    {
                        var separator = item == processor.subscrInfo[processor.subscrInfo.Count - 1] ? " ;" : string.Empty;
                        switch (item.operResult.result)
                        {
                            case PaymentResult.Success:
                                lblCreationResult.InnerText += Resources.Success.SubscriptionCreation + separator;
                                break;
                            case PaymentResult.Error:
                                lblCreationResult.InnerText += Resources.Error.SubscriptionCreation + separator;
                                divErrorDetails.Visible = true;
                                lblSubscrErrorDetails.InnerText += item.operResult.message + separator;
                                break;
                        }
                    }
                    
                }
            }
            else
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Response.Redirect("~/User", true);
                }
                else
                {
                    Response.Redirect("~/", true);
                }
            }
        }

        private void ActivateDevice(int deviceId, int licenseId)
        {
            if (DeviceManagement.ActivateDevice(deviceId, licenseId))
            {
                _main.SetSuccessMessage(Resources.Success.DeviceActivation);
            }
            else
            {
                _main.SetErrorMessage(Resources.Error.DeviceActivation);
            }
        }

        protected bool ResolveSubscriptionCancellation(string subscrId, PaymentProcessors type)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);

            IPaymentProcessor processor = null;

            switch (type)
            {
                case PaymentProcessors.AuthorizeNet:
                    processor = new AuthorizeNetPaymentProcessor(null, null, testMode);
                    break;
                case PaymentProcessors.PayPal:
                    processor = new PaypalPaymentProcessor(null, null, null, testMode);
                    break;
                default:
                    return false;
            }

            var result = processor.CancelSubscription(subscrId);

            if (result.operResult.result == PaymentResult.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}