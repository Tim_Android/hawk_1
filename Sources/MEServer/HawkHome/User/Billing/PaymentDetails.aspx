﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="PaymentDetails.aspx.cs" Inherits="HawkHome.User.Billing.AuthorizeNet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>Payment Details</h4>
                <hr class="edit_account_hr" />
            </div>
            <div class="elements_margin">
                Please enter your credit card details:
            </div>
            <div class="form-horizontal payment_details">
                <div class="form-group">
                    <asp:Label ID="lblCreditCard" runat="server" AssociatedControlID="tbCreditCard" CssClass="col-sm-2 control-label">Credit Card Number:</asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="tbCreditCard" runat="server" placeholder="Credit Card Number" MaxLength="16" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCardHolderName" runat="server" AssociatedControlID="tbCardHolderName" CssClass="col-sm-2 control-label">Name on Card:</asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="tbCardHolderName" runat="server" placeholder="Name on Card" MaxLength="128" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCVV" runat="server" AssociatedControlID="tbCVV" CssClass="col-sm-2 control-label">CVC/CVV:</asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="tbCVV" runat="server" placeholder="XXX or XXXX" MaxLength="4" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblExpDate" runat="server" CssClass="col-sm-2 control-label">Expiration Date:</asp:Label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <hr />
                    <div class="col-sm-6">
                        <asp:LinkButton ID="btnPay" runat="server" CssClass="btn btn-sm btn-success label_color_black checkout_button pull-right" OnClick="btnPay_Click">Complete <span class="glyphicon glyphicon-shopping-cart"></span> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
