﻿using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using HawkHome.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.SessionState;

namespace HawkHome.Ajax
{
    /// <summary>
    /// Gets Price list with according subscription end date
    /// </summary>
    public class PriceList : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated && context.Request.Params[ProductModel.SELECTED_LICENSE] != null)
            {
                PriceListItemModel[] priceList = null;
                if (context.Request.Params[ProductModel.SELECTED_LICENSE] == ProductModel.NEW_LICENSE_VALUE)
                {
                    priceList = PriceListItemModel.GetPriceList(PriceListItemType.SubscriptionItem, DateTime.UtcNow);
                }
                else
                {
                    LicenseModel licInfo = LicenseModel.GetLicenseInfo(Convert.ToInt32(context.Request.Params[ProductModel.SELECTED_LICENSE]));

                    priceList = PriceListItemModel.GetPriceListForSubscrExtend(PriceListItemType.SubscriptionItem, licInfo.subscrEndDate, Resources.Billing.SubscriptionExtendLabel, (int)licInfo.priceListId);
                }

                string response = JSSerializer.GetInstance().Serialize(priceList);
                context.Response.ContentType = "application/json";
                context.Response.Write(response);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}