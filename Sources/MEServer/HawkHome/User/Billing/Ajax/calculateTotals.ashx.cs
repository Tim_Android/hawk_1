﻿using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using HawkHome.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace HawkHome.Ajax
{
    /// <summary>
    /// Calculates totals for input set of properties
    /// </summary>
    public class TotalsCalculator : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated &&
                context.Request.Params[ProductModel.SELECTED_LICENSE] != null &&
                context.Request.Params[ProductModel.DEV_COUNT] != null &&
                context.Request.Params[ProductModel.PRICE_LIST_ID] != null &&
                context.Request.Params[ProductModel.SUPPORT_PRICE_LIST_ID] != null)
            {
                CalculatedTotalsModel totals = null;
                CalculatedTotalsModel supportTotals = null;
                int? supportLicense = GetSupportLicenseId();

                if (context.Request.Params[ProductModel.SELECTED_LICENSE] != ProductModel.NO_LICENSE_VALUE)
                {
                    if (context.Request.Params[ProductModel.SELECTED_LICENSE] == ProductModel.NEW_LICENSE_VALUE)
                    {
                        totals = CalculatedTotalsModel.CalculatePayment(
                            null, //new license
                            Convert.ToInt32(context.Request.Params[ProductModel.DEV_COUNT]) - 1, //calculation method assumes that you have 1 device and submit additional number
                            Convert.ToInt32(context.Request.Params[ProductModel.PRICE_LIST_ID]));

                    }
                    else
                    {
                        int? priceListId = Convert.ToInt32(context.Request.Params[ProductModel.PRICE_LIST_ID]);
                        if (priceListId < 0)
                        {//we add devices only with no period extension
                            priceListId = null;
                        }
                        totals = CalculatedTotalsModel.CalculatePayment(Convert.ToInt32(context.Request.Params[ProductModel.SELECTED_LICENSE]), //existing license
                                                                   Convert.ToInt32(context.Request.Params[ProductModel.DEV_COUNT]),
                                                                   priceListId);

                    }
                }
                supportTotals = CalculatedTotalsModel.CalculateSupportPayment(supportLicense, //new license
                                                               Convert.ToInt32(context.Request.Params[ProductModel.SUPPORT_PRICE_LIST_ID]));
                if (totals == null)
                {
                    totals = supportTotals;
                }
                else
                {
                    totals.supportResidualDays = supportTotals.supportResidualDays;
                    totals.supportTotal = supportTotals.total;
                    totals.total += supportTotals.supportTotal;
                }
                



                string response = JSSerializer.GetInstance().Serialize(totals);
                context.Response.ContentType = "application/json";
                context.Response.Write(response);
            }
        }

        private static int? GetSupportLicenseId()
        {
            int? supportLicense = null;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var license = LicenseModel.GetLicenseList(GetUserInfo().id, null)
                                                        .FirstOrDefault(x => GetSupportPrices()
                                                        .Contains((int)x.priceListId));
                
                supportLicense = license?.id;
           
            }

            return supportLicense;
        }

        private static UserInfoModel GetUserInfo()
        {
            var aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
            var userInfo = UserInfoModel.GetUserInfo(aspUserId);
            return userInfo;
        }
        private static List<int> GetSupportPrices()
        {
            return PriceListItemModel.GetPriceList(PriceListItemType.PhoneSupport, DateTime.UtcNow)
                                            .Where(x => x.type == PriceListItemType.PhoneSupport)
                                            .Select(x => x.id)
                                            .ToList();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}