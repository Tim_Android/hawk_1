﻿using HawkDataAccess.Models;
using HawkDataAccess.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;

namespace HawkHome.User.Billing.Ajax
{
    #region Attributes
    /// <summary>
    /// Summary description for billingService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    #endregion
    public class billingService : System.Web.Services.WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public PriceListItemModel[] GetSubscriptionPriceList()
        {
            return GetPriceList(PriceListItemType.SubscriptionItem);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public PriceListItemModel[] GetPhoneSupportPriceList()
        {
            return GetPriceList(PriceListItemType.PhoneSupport);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetSupportSubscription()
        {
            return GetLicense();
        }

        #region private methods
        private string GetLicense()
        {
            var license = default(LicenseModel);
            string phone = ConfigurationManager.AppSettings["phone_support"];
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {

                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    license = LicenseModel.GetLicenseList(GetUserInfo().id, null)
                                                    .FirstOrDefault(x => GetSupportPrices().Contains((int)x.priceListId) 
                                                                    && x.subscrEndDate >= DateTime.UtcNow);
                }
            }
            return license != null ? phone : null;
        }
        private PriceListItemModel[] GetPriceList(PriceListItemType type)
        {
            PriceListItemModel[] priceList = default(PriceListItemModel[]);

            if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.Request.Params[ProductModel.SELECTED_LICENSE] != null)
            {
                var licenseId = HttpContext.Current.Request.Params[ProductModel.SELECTED_LICENSE];
                if (licenseId == ProductModel.NEW_LICENSE_VALUE && type == PriceListItemType.SubscriptionItem)
                {
                    priceList = PriceListItemModel.GetPriceList(type, DateTime.UtcNow);
                }
                else
                {
                    if (licenseId == ProductModel.NO_LICENSE_VALUE || type == PriceListItemType.PhoneSupport)
                    {
                        if (HttpContext.Current.User.Identity.IsAuthenticated)
                        {
                            var license = LicenseModel.GetLicenseList(GetUserInfo().id, null)
                                                            .FirstOrDefault(x => GetSupportPrices()
                                                            .Contains((int)x.priceListId));
                            if (license == null)
                            {
                                return PriceListItemModel.GetPriceList(PriceListItemType.PhoneSupport, DateTime.UtcNow);
                            }
                            else
                            {
                                licenseId = license.id.ToString();
                            }
                        }
                    }
                    LicenseModel licInfo = LicenseModel.GetLicenseInfo(Convert.ToInt32(licenseId));

                    priceList = PriceListItemModel.GetPriceListForSubscrExtend(type,
                                                                               licInfo.subscrEndDate,
                                                                               type == PriceListItemType.PhoneSupport ? Resources.Billing.PhoneSupportExtendLabel : Resources.Billing.SubscriptionExtendLabel,
                                                                               (int)licInfo.priceListId);
                }
            }
            return priceList;
        }

        private static UserInfoModel GetUserInfo()
        {
            var aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
            var userInfo = UserInfoModel.GetUserInfo(aspUserId);
            return userInfo;
        }

        private static List<int> GetSupportPrices()
        {
            return PriceListItemModel.GetPriceList(PriceListItemType.PhoneSupport, DateTime.UtcNow)
                                            .Where(x => x.type == PriceListItemType.PhoneSupport)
                                            .Select(x => x.id)
                                            .ToList();
        }

        #endregion
    }
}
