﻿using Domain.Interfaces;
using HawkCommonResources;
using HawkDataAccess;
using HawkDataAccess.Types;
using HawkHome.Common.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User
{
    public partial class BrowserHistory : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.BrowserLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.BrowserLog, CurrentDeviceInfo.device));

            GridDataLog = gridBrowserHistory;
        }

        protected void gridBrowserHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridBrowserHistory.PageIndex = e.NewPageIndex;
            gridBrowserHistory.DataBind();
        }
    }
}