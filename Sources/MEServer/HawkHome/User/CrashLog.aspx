﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="CrashLog.aspx.cs" Inherits="HawkHome.CrashLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script type="text/javascript">
      var page = 'crash_log';
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
     <asp:GridView ID="gridCrashLog" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover" 
        onpageindexchanging="gridCrashLog_PageIndexChanging" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom"/>
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="CRASH_DATETIME" HeaderText="Date/Time (UTC)" SortExpression="CRASH_DATETIME" ReadOnly="true" />
            <asp:BoundField DataField="CRASH_DATA" HeaderText="Crash Data" SortExpression="CRASH_DATA" ReadOnly="true" />
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />                                   
        <emptydatatemplate>
                No Data Found.
        </emptydatatemplate>        
    </asp:GridView>
</asp:Content>
