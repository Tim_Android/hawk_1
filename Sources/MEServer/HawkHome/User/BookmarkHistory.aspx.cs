﻿using Domain.Interfaces;
using HawkCommonResources;
using HawkDataAccess;
using HawkDataAccess.Types;
using HawkHome.Common.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User
{
    public partial class BookmarkHistory : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.BookmarkLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.BookmarkLog, CurrentDeviceInfo.device));

            GridDataLog = gridBookmarkHistory;
        }

        protected void gridCallHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridBookmarkHistory.PageIndex = e.NewPageIndex;
            gridBookmarkHistory.DataBind();
        }
    }
}