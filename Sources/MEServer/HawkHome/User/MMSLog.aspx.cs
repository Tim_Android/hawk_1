﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkCommonResources;
using HawkHome.Common.Page;

namespace HawkHome
{
    public partial class MmsLogForm : MonitoringPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MonitoringUi.SetPageType(TabsType.MMSLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.MmsLog, CurrentDeviceInfo.device));

            GridDataLog = gridMMSLog;

            if (IsPostBack)
            {
                gridMMSAttachments.Visible = false;
            }
        }

        protected void gridMMSLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridMMSLog.SelectedIndex = -1;
            gridMMSAttachments.Visible = false;
            gridMMSLog.PageIndex = e.NewPageIndex;
            gridMMSLog.DataBind();
        }

        protected void gridMMSLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = gridMMSLog.SelectedIndex;

            int mmsID = (int)gridMMSLog.SelectedDataKey.Values["ID_MSG"];

            DataSet ds = DataAccess.GetMMSAttachments(mmsID);

            gridMMSAttachments.DataSource = ds;
            gridMMSAttachments.DataBind();
            gridMMSAttachments.Visible = true;
        }

        protected void gridMMSAttachments_RowCreated(object sender, GridViewRowEventArgs e)
        {
            DataRowView r = (DataRowView)e.Row.DataItem;

            if (r != null)
            {
                string statusAtachment = r["STATUS"].ToString();
                e.Row.Cells[0].Enabled = statusAtachment.Equals("Ok");
            }
        }

        protected void gridMMSAttachments_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Download attachment
            int index = gridMMSAttachments.SelectedIndex;
            int attachmentID = (int)gridMMSAttachments.SelectedDataKey.Values["ID_ATTACH"];

            byte[] attachment = DataAccess.GetAttachment(attachmentID);
            if (null != attachment)
            {
                string attacmentExtension = HttpUtility.HtmlDecode(gridMMSAttachments.SelectedRow.Cells[2].Text);
                Response.Clear();
                Response.Buffer = false;
                Response.ContentType = HttpUtility.HtmlDecode(gridMMSAttachments.SelectedRow.Cells[1].Text);
                Response.AddHeader("Content-Disposition", "attachment; filename="
                    + "attachment" + attachmentID.ToString() + attacmentExtension);
                Response.BinaryWrite(attachment);
                Response.End();
            }
        }
    }
}
