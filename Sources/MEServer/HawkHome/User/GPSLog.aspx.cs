﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using Domain.Entities.Enums;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkCommonResources;
using HawkHome.Common.Page;

namespace HawkHome
{
    public partial class GPSLog : MonitoringPage
    {
        public string selectedTab;

        protected void Page_Load(object sender, EventArgs e)
        {
            selectedTab = "table";
            MonitoringUi.SetPageType(TabsType.GPSLog, CurrentDeviceInfo);
            MonitoringUi.SetHeader(string.Format(PageHeader.GpsLog, CurrentDeviceInfo.device));

            GridDataLog = gridGPSLog;
            //        userTemplate.isKmlExportEnabled = masterPageGridDisplayable.GridDataLog.Rows.Count == 0 ? false : true;
        }

        private void InitEmptyDataTemplateContent()
        {

            switch (CurrentDeviceInfo.operSysType)
            {
                case OperatingSystemType.Ios:
                    //hide android-specific empty table
                    Control adnroidEmptyHeader = gridGPSLog.Controls[0].Controls[0].FindControl("androidEmptyHeader");
                    if (adnroidEmptyHeader != null)
                    {
                        adnroidEmptyHeader.Visible = false;
                    }

                    Control androidEmptyContent = gridGPSLog.Controls[0].Controls[0].FindControl("androidEmptyContent");
                    if (androidEmptyContent != null)
                    {
                        androidEmptyContent.Visible = false;
                    }
                    break;
                case OperatingSystemType.Android:
                    //hide iOs-specific empty table
                    Control iOsEmptyHeader = gridGPSLog.Controls[0].Controls[0].FindControl("iOsEmptyHeader");
                    if (iOsEmptyHeader != null)
                    {
                        iOsEmptyHeader.Visible = false;
                    }

                    Control iOsEmptyContent = gridGPSLog.Controls[0].Controls[0].FindControl("iOsEmptyContent");
                    if (iOsEmptyContent != null)
                    {
                        iOsEmptyContent.Visible = false;
                    }
                    break;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            InitEmptyDataTemplateContent();
        }

        protected void gridGPSLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //hide "Type", "Speed", and "Bearing" columns for iOS agent
            if (CurrentDeviceInfo.operSysType == OperatingSystemType.Ios)
            {
                gridGPSLog.Columns[3].Visible = false;
                gridGPSLog.Columns[7].Visible = false;
                gridGPSLog.Columns[9].Visible = false;
            }
        }

        protected void gridGPSLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridGPSLog.PageIndex = e.NewPageIndex;
            gridGPSLog.DataBind();
        }

        protected void gridGPSLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedTab = "map";
        }
    }
}
