﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="BookmarkHistory.aspx.cs" Inherits="HawkHome.User.BookmarkHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var page = 'bookmark_history';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <asp:GridView ID="gridBookmarkHistory" runat="server" AllowPaging="True"
        AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover long_word_fix" 
        OnPageIndexChanging="gridCallHistory_PageIndexChanging" ShowHeaderWhenEmpty="true">
        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
        <RowStyle ForeColor="#333333"/>
        <Columns>
            <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TITLE" HeaderText="Title" SortExpression="TITLE" ReadOnly="true">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle ForeColor="Black" HorizontalAlign="Left" CssClass="pager" />
        <SelectedRowStyle Font-Bold="True" />
        <HeaderStyle Font-Bold="True" ForeColor="Black" />
        <EmptyDataTemplate>
            No Data Found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
