﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserUI.Master" AutoEventWireup="true" CodeBehind="GPSLog.aspx.cs" Inherits="HawkHome.GPSLog" %>

<%@ Import Namespace="Domain.Entities.Enums" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var page = 'gps';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" runat="server">
    <div id="divGPSContent" style="max-width: none;">
        <div id="divGPSPages">
            <div id="divGPSGgid" runat="server">
                <asp:GridView ID="gridGPSLog" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover gps_table long_word_fix"
                    OnPageIndexChanging="gridGPSLog_PageIndexChanging" Height="100%"
                    Width="100%" OnSelectedIndexChanged="gridGPSLog_SelectedIndexChanged" OnRowDataBound="gridGPSLog_RowDataBound"
                    ShowHeaderWhenEmpty="false">
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <RowStyle ForeColor="#333333"/>
                    <Columns>
                        <asp:TemplateField HeaderText="Show on Map">
                            <HeaderStyle Width="93px" />
                            <ItemTemplate>
                                <a target="_blank" href="https://maps.google.com/maps?q=<%# Eval("LATITUDE") %>,<%# Eval("LONGITUDE") %>" class="map_coordinate_center" title="Show on map">
                                    <i class="glyphicon glyphicon-map-marker paraben_color" aria-hidden="true"></i>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DEV_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Device Date/Time" SortExpression="DEV_DATE" ReadOnly="true">
                            <HeaderStyle Width="160px"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="USER_DATE" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Local Date/Time" SortExpression="USER_DATE" ReadOnly="true">
                            <HeaderStyle Width="160px"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="PROVIDER" HeaderText="Type" SortExpression="PROVIDER" ReadOnly="true" />
                        <asp:BoundField DataField="LATITUDE" HeaderText="Latitude" SortExpression="LATITUDE" ReadOnly="true" />
                        <asp:BoundField DataField="LONGITUDE" HeaderText="Longitude" SortExpression="LONGITUDE" ReadOnly="true" />
                        <asp:BoundField DataField="ACCURACY" HeaderText="Accuracy (m)" SortExpression="ACCURACY" ReadOnly="true" />
                        <asp:BoundField DataField="SPEED" HeaderText="Speed (m/s)" SortExpression="SPEED" ReadOnly="true" />
                        <asp:BoundField DataField="ALTITUDE" HeaderText="Altitude (m)" SortExpression="ALTITUDE" ReadOnly="true" />
                        <asp:BoundField DataField="BEARING" HeaderText="Bearing" SortExpression="BEARING" ReadOnly="true" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" CssClass="pager" />
                    <SelectedRowStyle CssClass="table_row_selected" Font-Bold="True" ForeColor="Black" />
                    <HeaderStyle Font-Bold="True" ForeColor="Black" />
                    <EmptyDataRowStyle CssClass="hide" />
                    <EmptyDataTemplate>
                        <%-- This row is required for proper row stripe color  --%>
                        <tr class="hide">
                        </tr>
                        <tr id="iOsEmptyHeader" runat="server">
                            <th scope="col" class="label_color_black">Show on Map</th>
                            <th scope="col" class="label_color_black">Device Date/Time</th>
                            <th scope="col" class="label_color_black">Local Date/Time</th>
                            <th scope="col" class="label_color_black">Latitude</th>
                            <th scope="col" class="label_color_black">Longitude</th>
                            <th scope="col" class="label_color_black">Accuracy (m)</th>
                            <th scope="col" class="label_color_black">Altitude (m)</th>
                        </tr>
                        <tr id="iOsEmptyContent" runat="server">
                            <td colspan="7">No Data Found.
                            </td>
                        </tr>
                        <tr id="androidEmptyHeader" runat="server">
                            <th scope="col" class="label_color_black">Show on Map</th>
                            <th scope="col" class="label_color_black">Device Date/Time</th>
                            <th scope="col" class="label_color_black">Local Date/Time</th>
                            <th scope="col" class="label_color_black">Type</th>
                            <th scope="col" class="label_color_black">Latitude</th>
                            <th scope="col" class="label_color_black">Longitude</th>
                            <th scope="col" class="label_color_black">Accuracy (m)</th>
                            <th scope="col" class="label_color_black">Speed (m/s)</th>
                            <th scope="col" class="label_color_black">Altitude (m)</th>
                            <th scope="col" class="label_color_black">Bearing</th>
                        </tr>
                        <tr id="androidEmptyContent" runat="server">
                            <td colspan="10">No Data Found.
                            </td>
                        </tr>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

