﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivationSuccessful.aspx.cs" Inherits="HawkHome.User.ActivationSuccessful" MasterPageFile="~/Common/BaseUI.Master" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/basic_styles.css") %>" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <h4>Activation Successful</h4>
            <hr class="edit_account_hr" />
            <div>&nbsp;</div>
            <label>
                Congratulations! Your License Code has been successfully processed. Your License Code is <%=Session["PromoCode"] %>
            </label>
            <div>&nbsp;</div>
            <div>
                <asp:Button ID="ContinueButton" runat="server" Text="Continue"
                    CssClass="btn btn-sm btn-warning label_color_black"
                    OnClick="btnContinue_Click" />
            </div>
        </div>
    </div>
</asp:Content>

