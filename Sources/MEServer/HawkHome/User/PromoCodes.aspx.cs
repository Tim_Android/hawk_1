﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HawkDataAccess.Models;
using System.Web.Security;
using HawkDataAccess;
using HawkDataAccess.Types;

#endregion

namespace HawkHome.User
{
    public partial class PromoCodes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                BaseUI main = (BaseUI)Page.Master;
                main.ResetMessages();
            }
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {            
            BaseUI main = (BaseUI)Page.Master;

            var aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;

            string promoCode = activatePromoCode.Text.Trim();

            var error = PromoCodeModel.Activate(promoCode, main.userInfo.id);
            if (!error.HasValue)
            {
                Session["PromoCode"] = promoCode;
                if (!HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.PAID))
                {
                    //enabling user account
                    Roles.AddUserToRole(Context.User.Identity.Name, HawkDataAccess.Const.AvailableRoles.PAID);
                }
                Response.Redirect("~/User/ActivationSuccessful.aspx", true);
            }
            else
            {
                switch (error.Value)
                {
                    case (int)ErrorType.ActivateInvalidOrDisabledPromoCode:
                        main.SetErrorMessage(Resources.Error.ActivateInvalidOrDisabledPromoCode);
                        break;
                    case (int)ErrorType.ActivationsHaveAlreadyBeenUsedUp:
                        main.SetErrorMessage(Resources.Error.ActivationsHaveAlreadyBeenUsedUp);
                        break;
                    case (int)ErrorType.ExpiredPromoCode:
                        main.SetErrorMessage(Resources.Error.ExpiredPromoCode);
                        break;
                    default:
                        main.SetErrorMessage(Resources.Error.Unknown);
                        break;
                }
            }
        }
    }
}