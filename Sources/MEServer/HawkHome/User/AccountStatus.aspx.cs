﻿using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.User
{
    public partial class AccountStatus : System.Web.UI.Page
    {
        protected AccountDetailsItemModel[] licenseDetails;

        protected void Page_Load(object sender, EventArgs e)
        {
            BaseUI main = Page.Master as BaseUI;
            LicenseModel[] userLicenses = LicenseModel.GetLicenseList(main.userInfo.id, null);
            PriceListItemModel[] priceList = PriceListItemModel.GetPriceList(PriceListItemType.SubscriptionItem);
            List<AccountDetailsItemModel> details = new List<AccountDetailsItemModel>();
            foreach (LicenseModel license in userLicenses)
            {
                AccountDetailsItemModel item = new AccountDetailsItemModel();
                item.licenseInfo = license;
                if (license.isAutochargeEnabled)
                {
                    AutochargeInfoModel autochargeInfo = new AutochargeInfoModel();
                    PriceListItemModel licensePriceListItem = priceList.Where(res => res.id == license.priceListId).FirstOrDefault();
                    if (licensePriceListItem == null)
                    {
                        //this is PromoCodeType, should get actual prices by SubscriptionItem type 
                        var promoCodeTypePriceList = PriceListItemModel.GetPriceList(PriceListItemType.Promocode);
                        var duration = promoCodeTypePriceList.Where(res => res.id == license.priceListId).FirstOrDefault().duration;
                        licensePriceListItem = priceList.Where(z => z.duration == duration).FirstOrDefault();
                    }
                    CalculatedTotalsModel totals = CalculatedTotalsModel.CalculatePayment(null, license.devices - 1, licensePriceListItem.id);
                    autochargeInfo.label = licensePriceListItem.label;
                    autochargeInfo.price = licensePriceListItem.price.Value;
                    autochargeInfo.priceOther = licensePriceListItem.priceOther.Value;
                    autochargeInfo.total = totals.total;
                    int daysPriorAutochargePayment = Convert.ToInt32(ConfigurationManager.AppSettings["daysPriorAutochargePayment"]);
                    autochargeInfo.paymentDate = license.subscrEndDate.Subtract(new TimeSpan(daysPriorAutochargePayment, 0, 0, 0));
                    item.autochargeInfo = autochargeInfo;
                }
                details.Add(item);
            }
            licenseDetails = details.ToArray();
        }
    }
}