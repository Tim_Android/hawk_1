﻿#region NameSpaces
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using HawkHome.Code;
using HawkDataAccess.Models;
using HawkDataAccess;
using System.Configuration;
using HawkDataAccess.Types;
using System.Data;
using System.Linq;
using Domain.Entities;
using Domain.Entities.Abstract;
using Domain.Entities.Enums;
using Domain.Interfaces;
using Hawk.Services;
#endregion
namespace HawkHome
{
    public partial class UserUI : System.Web.UI.MasterPage
    {
        private int _totalRecords = default(int);
        public int RecordsPerPage {
            get {
                return int.Parse(ddlRecordsPerPage.SelectedValue);
            }
            set {
                ddlRecordsPerPage.SelectedValue = value.ToString();
            }
        }

        public int TotalRecords {
            get { return _totalRecords; }
            set {
                _totalRecords = value;
                lblRecordsFound.InnerText = string.Format(Resources.General.labelTotalResults, value.ToString());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitRecordsPerPage();
        }


        public void SetPageType(TabsType tab, DeviceInfoEx deviceInfo)
        {
            Session[StorageConstants.CURRENT_TAB_STORAGE] = tab;
            DataFilters.SetFilterConfiguration(tab, deviceInfo);
            MonitoringMenu.SetMenuConfiguration(deviceInfo);
        }

        public SearchCondition GetSearchCondition()
        {
            return DataFilters.GetSearchCondition();
        }

        public void BindDeviceList(DeviceInfoEx currentDevice, IList<DeviceInfo> devices)
        {
            List<AgentInfoModel> agentDatabind = new List<AgentInfoModel>();
            foreach (DeviceInfo device in devices)
            {
                AgentInfoModel agentItem = new AgentInfoModel();
                agentItem.id = device.id.ToString();
                agentItem.name = device.device;
                string imageLink = string.Empty;
                if (!device.licId.HasValue)
                {//license is not activated
                    imageLink = ResolveUrl(ConfigurationManager.AppSettings["DeviceNotLicensedURL"]);
                }
                else
                {
                    imageLink = device.isOnline ? ResolveUrl(ConfigurationManager.AppSettings["DeviceOnlineURL"]) : ResolveUrl(ConfigurationManager.AppSettings["DeviceOfflineURL"]);
                }
                agentItem.imageLink = imageLink;
                agentItem.imei = device.deviceId;
                if (device.id == currentDevice.id)
                {
                    agentItem.active = true;
                }
                else
                {
                    agentItem.active = false;
                }
                agentDatabind.Add(agentItem);
            }
            ItemList.SetDataBind(agentDatabind);
        }

        public void BindDataLoadMethod(Func<SearchCondition, DataSet> method)
        {
            DataFilters.SetDataLoadDelegate(method);
        }

        public void SetHeader(string header)
        {
            lblHeader.Text = header;
        }

        public void HideHeader()
        {
            header.Visible = false;
        }

        public void HideMonitoringTabs()
        {
            MonitoringMenu.Visible = false;
        }

        public void HideRecordSetDetails()
        {
            divRecordsPerPage.Visible = false;
        }

        protected void btnNewAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/CreateAgent.aspx", true);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Session[StorageConstants.DEVICE_LIST_STORAGE] = null; //cleaning device list to be sure the most recent one has been loaded on refresh
            Response.Redirect(Request.RawUrl, true);
        }
        #region Private Methods

        private void InitRecordsPerPage()
        {
            if (!IsPostBack)
            {
                ddlRecordsPerPage.Items.Add(new ListItem("25", "25"));
                ddlRecordsPerPage.Items.Add(new ListItem("50", "50"));
                ddlRecordsPerPage.Items.Add(new ListItem("100", "100"));
                ddlRecordsPerPage.Items.Add(new ListItem("150", "150"));

                string existingRecordsPerPageSetting = CookieStorageService.GetCookie(StorageConstants.RECORDS_PER_PAGE_STORAGE);
                ListItem selectedDropdownItem = ddlRecordsPerPage.Items.FindByValue(existingRecordsPerPageSetting);

                //if setting exists and it is a valid setting
                if (!string.IsNullOrWhiteSpace(existingRecordsPerPageSetting) && selectedDropdownItem != null)
                {
                    ddlRecordsPerPage.SelectedValue = selectedDropdownItem.Value;
                }
                else
                {
                    ddlRecordsPerPage.SelectedIndex = 0;
                }
            }
                        
            CookieStorageService.SetCookie(ddlRecordsPerPage.SelectedValue, StorageConstants.RECORDS_PER_PAGE_STORAGE);
        }
        #endregion
    }
}
