﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="HawkHome.Public._404" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
<div class="container">
            <div class="mtop20 hero-unit">
                <h1>Page not found</h1>
                <p>
                    The page you requested does not exist.
                </p>
                <p class="mtop20">
                    <a href="<%= ResolveUrl("~/")%>" class="btn btn-sm btn-warning label_color_black">Continue</a>
                </p>
            </div>
    </div>
</asp:Content>
