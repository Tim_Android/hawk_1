﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="NotAuthorized.aspx.cs" Inherits="HawkHome.Public.Error.NotAuthorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            You are not authorized to view this content. Please contact our suport team if you experience any difficulties
        </div>
    </div>
</asp:Content>
