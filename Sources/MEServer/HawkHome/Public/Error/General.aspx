﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="General.aspx.cs" Inherits="HawkHome.Public.Error.General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
            <div class="mtop20 hero-unit">
                <h1>Oops…</h1>
                <p>
                    An error has occurred while submitting your request. We apologize for any inconvenience caused by this.<br>
                </p>
                <p class="mtop20">
                    <a href="<%= ResolveUrl("~/")%>" class="btn btn-sm btn-warning label_color_black">Continue</a>
                </p>
            </div>
    </div>
</asp:Content>
