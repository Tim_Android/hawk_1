﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace HawkHome.Ajax
{
    public class Helper : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public void SetTimezone()
        {
            if (HttpContext.Current.Session[StorageConstants.USER_TZ_OFFSET_STORAGE] == null)
            {
                HttpContext.Current.Session[StorageConstants.USER_TZ_OFFSET_STORAGE] = HttpContext.Current.Request["timezone"];
            }
        }
    }
}
