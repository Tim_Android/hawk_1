﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="HawkHome.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%=ResolveUrl("~/JS/Pages/home.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/hammer-v2.0.3.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/flickerplate.js")%>"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container-fluid">
        <div class="row mtop20">
            <div id="sliderContainer" runat="server" class="col-sm-9 hidden-xs">
                <div class="flicker-example">
                    <ul><li style="background-image: url(<%=ResolveUrl("~/Images/slideshow/HAWK-Website-Banners-Hero.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/HAWK-Website-Banners-Hero.png")%>"></li>
                        <li style="background-image: url(<%=ResolveUrl("~/Images/slideshow/HAWKBanner.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/HAWKBanner.png")%>"></li>
                        <li style="background-image: url(<%=ResolveUrl("~/Images/slideshow/monitor_page_banner-min.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/monitor_page_banner-min.png")%>"></li>

                    </ul>
                </div>
            </div>
            <div id="registrationContainer" runat="server" class="col-sm-3">
                <asp:CreateUserWizard ID="CreateUserWizard" runat="server"
                    LoginCreatedUser="false"
                    RequireEmail="False" CssClass="reg_form"
                    OnCreatedUser="RegisterUser_CreatedUser"
                    OnCreatingUser="RegisterUser_CreatingUser"
                    OnCreateUserError="CreateUserWizard_CreateUserError"
                    ContinueDestinationPageUrl="~/User/Default.aspx"
                    DuplicateUserNameErrorMessage="A user with such email is already registered" InvalidPasswordErrorMessage="Password length minimum: {0}.">
                    <CreateUserButtonStyle CssClass="pure-button pure-button-small pure-button-warning label_color_black" />
                    <WizardSteps>
                        <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                            <ContentTemplate>
                                <div class="mtop30">
                                    <div class="form-horizontal">
                                        <div>
                                            <img class="img-responsive" src="<%=ResolveUrl("~/Images/free-trial-button.png")%>" />
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:Label ID="ErrorMessage" runat="server" EnableViewState="False" CssClass="alert-danger inline_alert full_width"></asp:Label>
                                                <asp:CompareValidator ID="PasswordCompare" runat="server"
                                                    ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                                    Display="Dynamic" ValidationGroup="CreateUserWizard1">
                                                           <div class="alert-danger inline_alert full_width">Password fields do not match</div>
                                                </asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="FirstName" runat="server" placeholder="First Name" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Display="Dynamic"
                                                ControlToValidate="FirstName" ValidationGroup="CreateUserWizard1" CssClass="col-sm-12 mtop3">
                                                    <span class="alert-danger inline_alert">First Name is required</span>
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="LastName" runat="server" placeholder="Last Name" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" Display="Dynamic"
                                                ControlToValidate="LastName" ValidationGroup="CreateUserWizard1" CssClass="col-sm-12 mtop3">
                                                    <span class="alert-danger inline_alert">Last Name is required</span>
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="UserName" runat="server" placeholder="Email" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" Display="Dynamic"
                                                ControlToValidate="UserName" ValidationGroup="CreateUserWizard1" CssClass="col-sm-12 mtop3">
                                                    <span class="alert-danger inline_alert">Email is required</span>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator
                                                ID="EmailValidator" runat="server" ControlToValidate="UserName" Display="Dynamic"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ValidationGroup="CreateUserWizard1" CssClass="col-sm-4 mtop3">
                                                    <span class="alert-danger inline_alert">Wrong email format</span>
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="Password" runat="server" TextMode="Password" placeholder="Password" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" Display="Dynamic"
                                                ControlToValidate="Password" ValidationGroup="CreateUserWizard1" CssClass="col-sm-12 mtop3">
                                                    <span class="alert-danger inline_alert">Password is required</span>
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" placeholder="Confirm Password" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" Display="Dynamic"
                                                ControlToValidate="ConfirmPassword" ValidationGroup="CreateUserWizard1" CssClass="col-sm-12 mtop3">
                                                    <span class="alert-danger inline_alert">Confirm Password is required</span>
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="g-recaptcha registration-form-centered" data-sitekey="<%= ConfigurationManager.AppSettings["recaptchaPublicKey"]  %>"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="checkbox text-center">
                                                    <label>
                                                        <asp:CheckBox ID="chbLicenseAgreement" runat="server" />
                                                        I have read and accept <a target="_blank" href="<%= ResolveUrl("~/Public/LicenseAgreement.aspx") %>" class="explicit-link">license agreement</a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="registration-button-container mtop3">
                                                    <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" CssClass="btn btn-sm btn-warning label_color_black" Text="Start Your FREE 7-Day Trial" ValidationGroup="CreateUserWizard1" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <CustomNavigationTemplate>
                            </CustomNavigationTemplate>
                        </asp:CreateUserWizardStep>
                        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                            <ContentTemplate>
                                <div>
                                    <div>
                                        <h1>Congratulations!</h1>
                                        <p>
                                            Thank you for signing up for the HAWK Mobile Monitoring system. 
                                    We have sent you a verification email to <span id="lblEmail" runat="server" class="text-bold"></span>. 
                                    Please check your inbox and follow the instructions to complete the verification process.
                                        </p>
                                        <br />
                                        <p>
                                            If you do not see the confirmation email, please check your spam filter and allow all email from paraben.com.
                                        </p>
                                        <p>
                                            <asp:Button ID="ContinueButton" runat="server" CausesValidation="False" CssClass="btn btn-sm btn-warning label_color_black mtop5"
                                                CommandName="Continue" Text="Continue" ValidationGroup="CreateUserWizard1" />
                                        </p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:CompleteWizardStep>
                    </WizardSteps>
                </asp:CreateUserWizard>
            </div>
        </div>
    </div>
    <div id="content">
        <div id="services7">
            <div class="container">
                <div class="row">
                   
                    <div class="col-sm-3">
                        <img class="img-custom-thumbnail img-responsive" src="<%=ResolveUrl("~/Images/info-cards/android-info-card-min.png")%>" />
                    </div>
                    <div class="col-sm-3">
                        <img class="img-custom-thumbnail img-responsive" src="<%=ResolveUrl("~/Images/info-cards/Apple Device support.png")%>" />
                    </div>
                    <div class="col-sm-3">
                        <img class="img-custom-thumbnail img-responsive" src="<%=ResolveUrl("~/Images/info-cards/messaging-info-card-min.png")%>" />
                    </div>
                    <div class="col-sm-3">
                        <img class="img-custom-thumbnail img-responsive" src="<%=ResolveUrl("~/Images/info-cards/gps-info-card-min.png")%>" />
                    </div>
                </div>
            </div>
        </div>
        <!-- end services5 -->
        <div id="amazing" class="hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 mbottom25 amazing-left">
                        <h2 class="section-title2 right">Hawk Mobile Monitor</h2>
                        <p class="right">Here's what you can monitor:</p>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <br>
                        <img src="<%=ResolveUrl("~/images/hawk-monitoring.png")%>" alt="" class="img-responsive" />
                    </div>
                    <div class="col-sm-3 amazing-right">
                        <ul class="number-list reset-list">
                            <li>
                                <div class="numberCircle">1</div>
                                <h4><strong>Call History</strong></h4>
                            </li>
                            <li>
                                <div class="numberCircle">2</div>
                                <h4><strong>Texts (SMS & MMS)</strong></h4>
                            </li>
                            <li>
                                <div class="numberCircle">3</div>
                                <h4><strong>GPS - Device Location</strong></h4>
                            </li>
                            <li>
                                <div class="numberCircle">4</div>
                                <h4><strong>Web History</strong></h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end amazing section-->
        <div runat="server" class="hidden-xs">
            <div class="flicker-example">
                <ul>
                    <li class="mouseover-pointer" onclick='window.location.href="<%=ResolveUrl("~/Public/Story.aspx")%>"' style="background-image: url(<%=ResolveUrl("~/Images/slideshow/parents_page_banner-min.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/parents_page_banner-min.png")%>"></li>
                    <li style="background-image: url(<%=ResolveUrl("~/Images/slideshow/testimonials_page_banner-min.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/testimonials_page_banner-min.png")%>"></li>
                    <li class="mouseover-pointer" onclick='window.location.href="<%=ResolveUrl("~/Public/Story.aspx")%>"' style="background-image: url(<%=ResolveUrl("~/Images/slideshow/alternate-testimonials_page_banner-min.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/alternate-testimonials_page_banner-min.png")%>"></li>
                    <li style="background-image: url(<%=ResolveUrl("~/Images/slideshow/HAWK_banner.png")%>)" data-background="<%=ResolveUrl("~/Images/slideshow/HAWK_banner.png")%>"></li>
                </ul>
            </div>
        </div>
        <!-- end feedbacks -->
        <div id="services" class="ptop30">
            <div class="container">
                <div class="center feedbacks-header mbottom30">
                    <span>How to Videos </span>
                </div>
                <div class="row">
                     <div class="col-sm-6">
                        <iframe src="https://www.youtube.com/embed/cIDqc7b0MvY" frameborder="0" allowfullscreen class="embedded-video-home"></iframe>
                    </div>
                    <div class="col-sm-6">
                        <iframe src="https://www.youtube.com/embed/6D2WsqkboG4?rel=0" frameborder="0" allowfullscreen class="embedded-video-home"></iframe>
                    </div>
                   
                </div>
              
            </div>
        </div>
        <div class="container mtop20">
            <p class="gray mbottom10">Frequently Asked Questions</p>
            <div class="row mbottom30">
                <div class="col-sm-6">
                    <div class="panel-group" id="accordion3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="icon-question"></span>
                                <a data-toggle="collapse" href="#collapse1">
                                    <h4 class="panel-title faq-header-label">What is HAWK Mobile Monitor?</h4>
                                </a>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>HAWK Mobile Monitor is a complete monitoring solution for Android devices. The monitoring focuses on call logs, text messages and media messages, GPS location, and web browsing activities on the device.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="icon-question"></span>
                                <a data-toggle="collapse" href="#collapse22">
                                    <h4 class="panel-title faq-header-label">How does HAWK Monitoring work?</h4>
                                </a>
                            </div>
                            <div id="collapse22" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>With Android device an App is installed on the device to be monitoring or the target device. The App can be seen by the users of the device, but cannot be removed by that user without a special password that is set when the App is setup.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="icon-question"></span>
                                <a data-toggle="collapse" href="#collapse3">
                                    <h4 class="panel-title faq-header-label">Do you support my phone?</h4>
                                </a>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>HAWK Mobile Monitor was developed to support devices with Android OS version 2.2–6.0 .</p>
                                    <p>You can find your firmware typically in the About Phone section of the Settings on the device.</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="icon-question"></span>
                                <a data-toggle="collapse" href="#collapse4">
                                    <h4 class="panel-title faq-header-label">What can you monitor?</h4>
                                </a>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>HAWK Mobile Monitor allows you to monitor main communication activity, which includes call logs (incoming, outgoing, and missed) and SMS and MMS messages (incoming, outgoing, draft, and even messages whose sending failed), and track movements of the device user via GPS tracking and view them on the map. In addition, for Android devices, you can monitor Internet browser history and bookmarks.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="icon-question"></span>
                                <a data-toggle="collapse" href="#collapse6">
                                    <h4 class="panel-title faq-header-label">Will my child know HAWK is on their device?</h4>
                                </a>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>HAWK Mobile Monitor was developed to comply with the US privacy laws. The application doesn’t hide its presence on your kid’s device. If your kids are under 18, you don’t have to inform them they are being monitored. So the decision is up to you.</p>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>

                <!--row 2 sliders-->
                <div class="row mbottom30">
                    <div class="col-sm-6">
                        <div class="panel-group">
                           
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="icon-question"></span>
                                    <a data-toggle="collapse" href="#collapse10">
                                        <h4 class="panel-title faq-header-label">How is HAWK Monitor different?</h4>
                                    </a>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>HAWK Mobile Monitor uses verifiable ways of obtaining data directly from the target device. HAWK Mobile Monitor was made by forensic experts so that the data collected is always considered to be sound.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="icon-question"></span>
                                    <a data-toggle="collapse" href="#collapse11">
                                        <h4 class="panel-title faq-header-label">How can I stop the HAWK App from being deleted on the device?</h4>
                                    </a>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>HAWK Mobile Monitor provides special means to protect the HAWK Agent App on the device against uninstallation. After installing the app on the device, you can be sure it won’t be deleted without your knowing.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="icon-question"></span>
                                    <a data-toggle="collapse" href="#collapse12">
                                        <h4 class="panel-title faq-header-label">Can I monitor more than one device?</h4>
                                    </a>
                                </div>
                                <div id="collapse12" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>HAWK Mobile Monitor allows you to monitor as many devices as you want. You do need to install the HAWK Agent App on each device you want to monitor.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="icon-question"></span>
                                    <a data-toggle="collapse" href="#collapse17">
                                        <h4 class="panel-title faq-header-label">How much does it cost?</h4>
                                    </a>
                                </div>
                                <div id="collapse17" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>The price per monitored device depends on the length of time you purchase the subscription for.  The longer the subscription period, the lower the price. See the <a target="_blank" href="<%= ResolveUrl("~/Public/Prices.aspx") %>">Prices page</a> for more information.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="icon-question"></span>
                                    <a data-toggle="collapse" href="#collapse19">
                                        <h4 class="panel-title faq-header-label">How quickly do I see the data from the device?</h4>
                                    </a>
                                </div>
                                <div id="collapse19" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>The monitoring after the HAWK Agent App is installed starts right away and you are about 1 minute away from seeing data.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="designed">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 center">
                        <a href="#top">
                            <img class="img-responsive" src="<%=ResolveUrl("~/Images/HAWK_tagline_reminder_banner-min.png")%>" alt="Start Trial" />
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/readmore.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/initializeReadMoreLink.js") %>"></script>
    <!-- iCarousel -->
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/iCarousel/raphael-min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/iCarousel/jquery.mousewheel.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/iCarousel/jquery.easing.1.3.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/iCarousel/icarousel.packed.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#icarousel').iCarousel();
        });
    </script>
    <!-- End iCarousel -->
    <script>
        window.addEventListener('load', function () {

            if (jQuery('#MainWindow_registrationContainer:contains("Congratulations!")').is(":visible")) {
                jQuery('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/864980935/?label=x6cXCI7l_mwQx5-6nAM&amp;guid=ON&amp;script=0"/>');
            }

        });
    </script>
</asp:Content>
