﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="Story.aspx.cs" Inherits="HawkHome.Public.Story" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>Amber's Story</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <p>
                    I started working at a young age and found a passion that I didn’t know I had. Although I always wanted to be Wonder Woman growing up, I figured it had to do with the cool outfit and invisible plane, and like me she was tall. However, as I started in the field of digital forensics I found it was fighting for justice and truth that made me idolize Wonder Woman. I kept working hard and founded Paraben Corporation. We make tools to help law enforcement catch digital predators, fraud, and all other geek related crimes. I had found my calling and it was to make a difference through invention.
                </p>
                <p class="mtop20">
                    In my mid-twenties I found myself single with two kids and still fighting for the justice I knew I loved, but my kids were getting older things were changing. It is when my daughter came home from 6th grade crying because the girls at school were being mean I really saw the change. When I was a kid girls were mean and they called you names, but once you were not around them, the name calling and bullying stopped. For my daughter that was not the case. She is part of a new generation, the Connected Generation; she was always connected to the web through her phone, tablet, or computer. I didn’t have any of those things when I was growing up. However, this connection is also a curse because those mean girls could continue to be mean anytime day or night because of this connection.
                </p>
                <p class="mtop20">
                    I talked to my daughter and told her the best thing you could do when someone was mean was to kill them with kindness. She went back to school and tried her best, but came back a week later scared and in tears. I decided to go through her phone to see what was happening. This is when the shock set in. “I will kill you if you come near us.” is the first text message I saw from these girls and I knew we had a larger problem. I called the school and reported the issue and my concern over the bullying. The school said there was nothing they could do. This is when I knew I needed to step up and do something.
                </p>
                <p class="mtop20">
                    I started by creating tools like the iRecovery Stick and Phone Recovery Stick, which would allow a parent to download the data from a smartphone and look at what is happening. I figured this would at least allow everyone to have the same digital forensics super power I had and be able to print and take the data to schools or law enforcement. Schools have gotten better when it comes to cyber bullying, but the true difference starts at home.
                </p>
                <p class="mtop20">
                    My son was getting older too, and boys are very different on how they work on the internet. He was not coming home telling me there was a problem; it was silent, but sometimes silence can be even scarier. My son was online and connected, but what he was doing was not safe. Chatting in groups of people of all ages and talking about his questions on a boy maturing. I panicked. I knew the internet and communities of strangers was not where I wanted my son to learn, but I needed to know what he was talking about.  
                </p>
                <p class="mtop20">
                    That is where Paraben’s HAWK came in. I designed HAWK to be able to keep me informed of all of my kid’s online life and really focus on their internet safety. Their connection to the internet also became a connection to me through information. I didn’t look at it as monitoring; I looked at it as gathering information so I could do the best I could as a parent. Information is power and I needed more power when it came to being able to talk to my kids. 
                </p>
                <p class="mtop20">
                    Not everyone’s story is the same, but know that the information you find on your children’s mobile devices can be the information saves their life.
                </p>
                <img src="<%= ResolveUrl("~/Images/signature.png") %>" />
            </div>
        </div>
    </div>
</asp:Content>
