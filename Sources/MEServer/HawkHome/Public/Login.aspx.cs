﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using HawkDataAccess.Models;
using Domain.Entities.Filters;
using Domain.Entities;
using Hawk.Services;

namespace HawkHome
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/User/Default.aspx", true);
            }

            //login on enter - setting default button behavior for login form
            Button lbButton = Login1.FindControl("LoginButton") as Button;
            ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainWindow");
            contentPlaceHolder.Page.Form.DefaultButton = lbButton.UniqueID;

            //bind ResolveUrl expressions
            Page.DataBind();
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            if (BanListService.isBanned(Login1.UserName))
            {
                FormsAuthentication.SignOut();
                Roles.DeleteCookie();
                Session.Clear();
                Server.Transfer("~/Public/Default.aspx?warning=banned");
            }
            if (Roles.IsUserInRole(Login1.UserName, HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                Session.Clear();
                Response.Redirect("~/Admin/History.aspx", true);
            }
            else if (Roles.IsUserInRole(Login1.UserName, HawkDataAccess.Const.AvailableRoles.CONFIRMED))
            {
                Session.Clear();
                var url = FormsAuthentication.GetRedirectUrl(Login1.UserName, false);
                if (!string.IsNullOrEmpty(url) && url == "/User/Billing/Billing.aspx")
                {
                    FormsAuthentication.RedirectFromLoginPage(Login1.UserName, false);
                }
                else
                {
                    Response.Redirect("~/User/Default.aspx", true);
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Roles.DeleteCookie();
                Session.Clear();
                Server.Transfer("~/Public/Default.aspx?warning=not_confirmed");
            }
        }


        protected void Login1_Load(object sender, EventArgs e)
        {
            Login1.FindControl("failMessage").Visible = false;
        }

        protected void Login1_LoginError(object sender, EventArgs e)
        {
            Login1.FindControl("failMessage").Visible = true;
        }
    }
}
