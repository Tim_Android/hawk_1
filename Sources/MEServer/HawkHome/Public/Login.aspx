﻿<%@ Page Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="HawkHome.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css")%>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <asp:Login ID="Login1" runat="server" DestinationPageUrl="/User/Default.aspx" OnLoggedIn="Login1_LoggedIn" OnLoad="Login1_Load" OnLoginError="Login1_LoginError"
            RememberMeSet="True" CssClass="full_width mtop20">
            <LayoutTemplate>
                <div class="form-horizontal well">
                    <div>
                        <h4>Log In</h4>
                        <hr class="edit_account_hr" />
                    </div>
                    <div id="failMessage" runat="server" class="form-group">
                        <div class="col-sm-12">
                            <asp:Label ID="FailureText" runat="server" EnableViewState="False" CssClass="alert-danger inline_alert full_width"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="col-sm-2 control-label">Email:</asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" Display="Dynamic"
                            ControlToValidate="UserName" ValidationGroup="Login1" CssClass="col-sm-4 mtop3">
                                    <span class="alert-danger inline_alert">Email is required</span>
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="col-sm-2 control-label">Password:</asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" Display="Dynamic"
                            ControlToValidate="Password" ErrorMessage="Password is required"
                            ToolTip="Password is required" ValidationGroup="Login1" CssClass="col-sm-4 mtop3">
                                    <span class="alert-danger inline_alert">Password is required</span>
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <a href="<%#ResolveUrl("~/Public/PasswordReset.aspx")%>" class="btn btn-sm label_color_black">Reset Password</a>
                            <asp:Button ID="LoginButton" TabIndex="1" runat="server" CommandName="Login" Text="Log In" CssClass="btn btn-warning btn-sm label_color_black"
                                ValidationGroup="Login1" />
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
</asp:Content>
