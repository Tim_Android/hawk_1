﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="HawkHome.Public.PasswordReset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css")%>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <asp:Label ID="FailureText" runat="server" EnableViewState="False" CssClass="label label-important" ForeColor="White"></asp:Label>
            </div>
            <div class="form-horizontal well">
                <div class="control-group">
                    <h4>Reset Password</h4>
                    <hr class="edit_account_hr" />
                </div>
                <div id="resetForm" runat="server" visible="true" class="form-group">
                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="col-sm-2 control-label">Email:</asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="UserName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-1 mtop2">
                        <asp:Button ID="ResetButton" runat="server" Text="Confirm" CssClass="btn btn-warning btn-sm label_color_black" OnClick="ResetButton_Click"
                            ValidationGroup="Reset" />
                    </div>
                    <div class="col-sm-3 mtop3">
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server"
                            ControlToValidate="UserName" Display="Dynamic"
                            ValidationGroup="Reset">
                         <span class="alert-danger inline_alert">Email field is required.</span>
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="EmailValidator" runat="server" CssClass="" Display="Dynamic"
                            ControlToValidate="UserName" ValidationExpression="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
                            ValidationGroup="Reset">
                         <span class="alert-danger inline_alert">Email is invalid.</span>
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
                <div id="confirmation" runat="server" visible="false">
                    <div class="form-group">
                        <asp:Label ID="lblResult" runat="server" CssClass="col-sm-6"></asp:Label>
                    </div>
                    <div id="btnBack" class="control-group" runat="server" visible="false">
                        <button class="btn btn-warning btn-sm label_color_black" onclick="history.back();return false;">Back</button>
                    </div>
                    <div id="btnToLogin" class="control-group" runat="server" visible="false">
                        <a href="~/Public/Login.aspx" class="btn btn-sm background_color_gray label_color_black" runat="server">Take me to Log In page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
