﻿using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.Public
{
    public partial class Prices : System.Web.UI.Page
    {
        protected List<PriceListItemModel> prices;

        protected void Page_Init(object sender, EventArgs e)
        {
            prices = new List<PriceListItemModel>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            prices.AddRange(PriceListItemModel.GetPriceList(null));
        }
    }
}