﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="Features.aspx.cs" Inherits="HawkHome.Public.Features" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/stylesheet/prettyPhoto.css")%>">
    <script src="<%# ResolveUrl("~/JS/jquery.prettyPhoto.js") %>"></script>
    <script src="<%# ResolveUrl("~/JS/lightbox_init.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="span15 mtop20">
            <div>
                <h4>Features</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div>
                    <div id="user_manual" class="elements_margin">
                        HAWK Mobile Monitor provides the following main features. If you want to know more about HAWK Mobile Monitor, <a target="_blank" href="~/Content/HAWK_UserManual.pdf" runat="server" class="text-bold">click here</a> to download the user manual.
                    </div>
                    <div id="sms" class="mtop20">
                        <h5 ><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/monitor-sms.png") %>" /></span>Monitor SMS (Text Messages)</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    When you install a HAWK Mobile Monitor Agent application on a target device, it starts monitoring all SMS activity on the device. It includes all incoming and outgoing messages, draft messages, which have not been sent yet, messages awaiting sending, and even messages whose sending failed. When the information about a new SMS message is received by the HAWK Mobile Monitor Server, on the HAWK Mobile Monitor web-site, you can view the following information about a message: the sender of a message, the time a message was received, sent, or created, its type (draft, sent, etc.), and the content of an actual message.
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/sms-monitoring.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/sms-monitoring.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="mms" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/monitor-mms.png") %>" /></span>MMS Monitor (Multi-Media Messaging)</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    When you install a HAWK Mobile Monitor Agent application on a target device, it starts monitoring all MMS activity on the device. It includes all incoming and outgoing messages, draft messages, which have not been sent yet, messages awaiting sending, and even messages whose sending failed. When the information about a new MMS message is received by the HAWK Mobile Monitor Server, on the HAWK Mobile Monitor web-site, you can download and view the files sent in a message, and you can view the following information about a message: the sender of a message, the time a message was received, sent, or created, its subject, its type (sent, draft, etc.), and text sent along with a file. 
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/mms-monitoring.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/mms-monitoring.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="multiple_phones" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/monitor-phones.png") %>" /></span>Monitor Multiple Phones</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    HAWK Mobile Monitor allows you to install its Agent applications on multiple Android OS and iPhone/iPad/iPod Touch devices and monitor them independently. The devices are linked to one account and do not interfere with each other. You can define any number of devices to be monitored in a subscription when you purchase one. Moreover, you can group devices into a number of subscriptions, each with its own expiration date. If there is no need to monitor a device, you can unassign a license from it and assign it to another device. The monitored data sent from each device is limited only by the storage time (45 days). 
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/monitor-multiple-phones.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/monitor-multiple-phones.png") %>" class="features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="call_history" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/call-history.png") %>" /></span>Monitor Call History</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    When you install a HAWK Mobile Monitor Agent application on a target device, it starts monitoring all incoming and outgoing calls, even if there was no answer. On the HAWK Mobile Monitor web-site, you can view detailed information on each call, which includes the time a call was started, the phone number from which and to which the call was performed, its sender and recipient (if it exists in the device address book), its type (incoming, outgoing, etc.), and the duration of a call.
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/call-log-monitoring.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/call-log-monitoring.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="gps" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/gps-tracking.png") %>" /></span>GPS Tracking</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    When you install a HAWK Mobile Monitor Agent application on a target device and enable the GPS tracking on it, you can view the history of its movements on the HAWK Mobile Monitor web-site. Besides providing the information on a location and the speed of a device at a certain moment of time, HAWK Mobile Monitor allows you to download and view the GPS history data on an actual map so that you can see the device movements in a graphical representation. 
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/gps-monitoring.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/gps-monitoring.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="web_history" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/web-history.png") %>" /></span>Web History Tracking</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    When you install a HAWK Mobile Monitor Agent application on a target device you can see data associated with the default browsers that are installed on the smart device. This allows you to see where your child might be looking on the internet. Currently only the default browsers for Android are supported.
                                </p>
                            </div>
                              <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/browser-history.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/browser-history.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="android" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/android-devices.png") %>" /></span>Supports Android Phones</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    The HAWK Mobile Monitor service is developed to support all Android OS versions, starting with the very first stable version 2.2. All you need is to download a special Agent installation file and run it on a target device. The installation process doesn’t take much time and all actions that you need to perform on a device are described in details in the HAWK Mobile Monitor help file, so you won’t have any difficulties performing the installation.
                                </p>
                            </div>
                              <div class="col-sm-4">
                                <a href="<%=ResolveUrl("~/Images/Support/android.png") %>" rel="prettyPhoto">
                                    <img src="<%=ResolveUrl("~/Images/Support/android.png") %>" class="img-responsive features-screenshot" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="iOS" class="mtop20">
                        <h5><span><img class="img-right-margin text_vert_align_middle" src="<%=ResolveUrl("~/Images/feature_icons/ios-devices.png") %>" /></span>Supports iPhone/iPad/iPod Touch Devices</h5>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    iOS/iPhone support is coming soon. We have decided to change our methods for working with these devices to match changes that have happened to the operating system/firmware. We hope to offer a new HAWK agent for iOS in the coming months.
                                </p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
