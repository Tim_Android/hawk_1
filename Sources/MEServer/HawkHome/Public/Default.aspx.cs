﻿using EmailManagement;
using EmailManagement.Types;
using Hawk.Infrastructure.BLL;
using Hawk.Services;
using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HawkHome
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BaseUI main = (BaseUI)Page.Master;
            string message = String.Empty;

            if (Request["warning"] != null)
            {
                switch (Request["warning"])
                {
                    case "not_confirmed":
                        message = Resources.Error.AccountNotConfirmed;
                        break;
                    case "already_paid":
                        message = Resources.Error.AlreadyPaid;
                        break;
                    case "already_confirmed":
                        message = Resources.Error.AlreadyConfirmed;
                        break;
                    case "invalid_token":
                        message = Resources.Error.AccountAlreadyActivated;
                        break;
                    case "banned":
                        message = Resources.Error.UserExistInBannedList;
                        break;
                    default:
                        message = Resources.Error.Unknown;
                        break;
                }
                main.SetWarningMessage(message);
            }
            if (Request["success"] != null)
            {
                switch (Request["success"])
                {
                    case "profile_deleted":
                        message = Resources.Success.ProfileDeleted;
                        break;
                    case "activation_resent":
                        message = Resources.Success.VerificationEmailSent;
                        break;
                    default:
                        message = Resources.Error.Unknown;
                        break;
                }
                main.SetSuccessMessage(message);
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {//hiding registration form from logged in user
                sliderContainer.Attributes["class"] = "col-sm-12";
                registrationContainer.Attributes["class"] = "hide";
            }

        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            Guid membershipId = (Guid)(Membership.GetUser(CreateUserWizard.UserName)).ProviderUserKey;
            UserInfoModel userInfo = new UserInfoModel();
            userInfo.firstName = ((TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("FirstName")).Text;
            userInfo.lastName = ((TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("LastName")).Text;
            userInfo.email = CreateUserWizard.UserName;            

            if (!UserInfoModel.AddUserInfo(membershipId, userInfo))
            {//if we failed to add info, then try to remove created account
                Membership.DeleteUser(CreateUserWizard.UserName);
                Response.Redirect("~/Public/Error/General.aspx", true);
            }
            else
            {
                Roles.AddUserToRole(CreateUserWizard.UserName, HawkDataAccess.Const.AvailableRoles.USER);

                AccountService.SendValidationEmail(CreateUserWizard.Email,
                                                   Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]),
                                                   ConfigurationManager.AppSettings["RegistrationValidationUrl"],
                                                   userInfo);

                ((HtmlGenericControl)CompleteWizardStep1.ContentTemplateContainer.FindControl("lblEmail")).InnerText = CreateUserWizard.Email;
            }
        }

        protected void RegisterUser_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            if (BanListService.isBanned(CreateUserWizard.UserName))
            {
                FormsAuthentication.SignOut();
                Roles.DeleteCookie();
                Session.Clear();
                Server.Transfer("~/Public/Default.aspx?warning=banned");
            }

            BaseUI main = (BaseUI)Page.Master;
            main.ResetMessages();

            //checking license agreement
            CheckBox licAgreement = ((CheckBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("chbLicenseAgreement"));
            if (!licAgreement.Checked)
            {
                main.SetErrorMessage(Resources.Error.LicenseAgreementAccept);
                e.Cancel = true;
                return;
            }

            //validation captcha
            string encodedResponse = Request.Form["g-Recaptcha-Response"];
            if (!CaptchaValidatorService.IsValid(encodedResponse, ConfigurationManager.AppSettings["recaptchaPrivateKey"]))
            {
                main.SetErrorMessage(Resources.Error.CaptchaValidationFailed);
                e.Cancel = true;
                return;
            }

            //if we got this far - then everything is ok
            CreateUserWizard.Email = CreateUserWizard.UserName;
        }

        protected void CreateUserWizard_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            if (e.CreateUserError == MembershipCreateStatus.DuplicateUserName)
            {
                //overriding default error message
                CreateUserWizard.DuplicateUserNameErrorMessage = BuildDuplicateUserMessage(CreateUserWizard.UserName);
            }
        }

        #region Helpers
        private string BuildDuplicateUserMessage(string email)
        {
            string port = Utils.GetPortParameter();

            string loginLink = Utils.ConvertRelativeUrlToAbsoluteUrl(ConfigurationManager.AppSettings["LoginUrl"], port);
            string registrationValidationLink = Utils.ConvertRelativeUrlToAbsoluteUrl(ConfigurationManager.AppSettings["RegistrationValidationSendUrl"], port);
            registrationValidationLink += HttpUtility.UrlEncode(email);
            //string validationResendLink
            return string.Format(Resources.Error.UserNameTaken, loginLink, registrationValidationLink);
        }
        #endregion
    }
}