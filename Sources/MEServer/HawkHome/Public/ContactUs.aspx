﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="HawkHome.Public.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css")%>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>Contact Us</h4>
                <hr class="edit_account_hr" />
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <h5>About Us</h5>
                    <p class="text_justify">Paraben Corporation is a longtime leader in the field of digital forensics and investigations. Paraben’s technology is utilized by a variety of federal, state, and international law enforcement and government agencies to insure the protection of evidence. Paraben’s Hawk Mobile Monitoring system was designed with parents and companies in mind to be able to watch and secure the data on mobile devices. Paraben’s inspiration for this system came from their own BYOD policies and the active roles of the executives as parents of teenagers. Paraben has been a trusted source for securing, protecting, and monitoring mobile devices for over 10 years and continues to lead this field. </p>
                </div>
                <div class="col-sm-6">
                    <div class="elements_margin elements_margin_horiz">
                        <h5>Email</h5>
                        <p>Product Support: <a href="mailto:hawksupport@paraben.com">hawksupport@paraben.com</a></p>
                    </div>
                    <div class="elements_margin elements_margin_horiz">
                        <h5>Phone</h5>
                        <p>Tel: 1.801.796.0944</p>
                        <p>Fax: 1.571.918.4054</p>
                    </div>
                    <div class="elements_margin elements_margin_horiz">
                        <h5>Headquarters</h5>
                        <p>Paraben Corporation</p>
                        <p>39344 John Mosby Hwy Ste 277</p>
                        <p>Aldie, VA 20105-2000</p>
                        <p>USA</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
