﻿<%@ Page Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true"
    CodeBehind="Register.aspx.cs" Inherits="HawkHome.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css")%>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <div class="hero-unit">
                    <h1>Congratulations!</h1>
                    <p>
                        Your account has been successfully verified.
                        <br>
                        <% if (_usedTrial){ %>
                        You can now log into your account and enjoy the best mobile monitoring software.
                        <% } else { %>
                        As a first-time visitor to HAWK Mobile Monitoring you can try our service for 7 days absolutely free! You can now log into your account and enjoy the best mobile monitoring software.
                        <% }%>
                    </p>
                    <p>
                        <a href="<%= ResolveUrl("~/Public/Login.aspx") %>" class="btn btn-sm btn-warning label_color_black mtop5">Take me to Log In page</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
