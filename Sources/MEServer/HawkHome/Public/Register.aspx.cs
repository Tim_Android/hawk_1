﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using EmailManagement;
using System.Configuration;
using HawkDataAccess;
using System.Web.UI.HtmlControls;
using HawkDataAccess.Models;
using EmailManagement.Types;
using HawkDataAccess.Types;
using Hawk.Services;
using Settings;
using Domain.Entities;
using Domain.Entities.Filters;
using HawkDataAccess.Const;

namespace HawkHome
{
    public partial class Register : System.Web.UI.Page
    {
        protected bool _usedTrial = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
               Response.Redirect("~/User/Default.aspx", true);   
            }

            bool actionRequest = false;

            //resending validation email
            if (Request.QueryString["userAccount"] != null && !IsPostBack)
            {
                MembershipUser user = Membership.GetUser(Request.QueryString["userAccount"]);

                if (user != null)
                {
                    if (Roles.IsUserInRole(user.UserName, AvailableRoles.CONFIRMED))
                    {
                        Server.Transfer("~/Public/Default.aspx?warning=already_confirmed");
                    }
                    else
                    {
                        UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);

                        AccountService.SendValidationEmail(userInfo.email,
                                                       Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]),
                                                       ConfigurationManager.AppSettings["RegistrationValidationUrl"],
                                                       userInfo);

                        Server.Transfer("~/Public/Default.aspx?success=activation_resent");
                    }
                }
            }

            //confirming registration
            if (Request.QueryString["token"] != null && !IsPostBack)
            {
                actionRequest = true;
                string userEmail = TokenManagement.CheckToken(Request.QueryString["token"]);
                if (userEmail != null)
                {
                    Roles.AddUserToRole(userEmail, AvailableRoles.CONFIRMED);

                    MembershipUser user = Membership.GetUser(userEmail);
                    UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);

                    InitializeTrialPeriod(userInfo);

                    //sending validation confirmation email
                    SendConfirmationEmail(userEmail, userInfo);
                    TokenManagement.DeleteToken(Request.QueryString["token"]);
                }
                else
                {
                    Server.Transfer("~/Public/Default.aspx?warning=invalid_token");
                }
            }
            
            if (!actionRequest)
            {
                Response.Redirect("~/Public/Default.aspx", true);
            }
             
        }

        private void SendConfirmationEmail(string userEmail, UserInfoModel userInfo)
        {
            Email emailData = EmailComposer.InitGeneralinfo(userEmail, EmailManagement.EmailResources.RegistrationValidationSuccessSubj);
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);

            EmailComposer.SendEmailNotification(EmailTypes.RegistrationValidationConfirm, emailData);
        }

        /// <summary>
        /// Grants user trial period if he hasn't been registered before
        /// </summary>
        /// <param name="userInfo"></param>
        private void InitializeTrialPeriod(UserInfoModel userInfo)
        {
            Pager pager = new Pager();
            IList<UserInfoModel> userInfoList = UserInfoModel.GetUserInfoPaged(new UserInfoFilter
            {
                Email = userInfo.email
            }, ref pager);

            _usedTrial = userInfoList.Where(res => res.aspUserId == null).ToList().Count > 0;

            //if user hasn't been registered before we grant him with a free trial period
            if (!_usedTrial)
            {
                string promoCode = PromoCodeService.GenerateTrialPromocode(TrialSettings.DeviceCount, TrialSettings.ActivationCount,
                                                                           TrialSettings.TrialPromocodeDuration, TrialSettings.TrialValidityPeriod);
                PromoCodeModel.Activate(promoCode, userInfo.id);
                Roles.AddUserToRole(userInfo.email, HawkDataAccess.Const.AvailableRoles.PAID);
            }
        }

        
    }
}
