﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="ViewGPSData.aspx.cs" Inherits="HawkHome.Public.ViewGPSData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfa70j_QUN4Hv_wZL8ZWuhOSkLqm6iaw&sensor=true" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/JS/geoxml3.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/scrollTo.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/blockUI.js")%>"></script>
    <script src="<%=ResolveUrl("~/JS/kml_display.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>View GPS Data</h4>
                <hr class="edit_account_hr" />
            </div>
            <div id="notSupported" class="alert hide">
                Unfortunately your browser does not support features required for this page. Please update you browser version.
            </div>
            <div class="elements_margin">
                You can use this page to view your previously exported coordinates. Please note that only *.kml files are allowed
            </div>
            <div id="fileUploadDialog" class="elements_margin">
                <input type="file" id="file" name="file" class="hide_important" />
                <a id="browseFile" class="btn btn-sm btn-warning label_color_black">Select File</a>
                <span id="fileUploadName" class="elements_margin_horiz label_color_gray">Please select file to upload that contains GPS data</span>
            </div>
            <div id="popup" class="hide popup_padding">
                <div class="elements_margin">
                    Wrong file type detected. Please make sure you use file with kml extension
                </div>
                <div class="elements_margin">
                    <input type="button" id="yes" value="OK" class="pure-button pure-button-small pure-button-warning label_color_black" />
                </div>
            </div>
            <div id="map_page">
                <div id="divMapCanvas" runat="server">
                    <div id="map_canvas" style="width: 100%; height: 600px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
