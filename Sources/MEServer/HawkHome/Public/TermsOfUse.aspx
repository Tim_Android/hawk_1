﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="TermsOfUse.aspx.cs" Inherits="HawkHome.Public.TermsOfUse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="span15">
        <div>
            <h4>Terms of Use</h4>
            <hr class="edit_account_hr" />
        </div>
        <div class="row-fluid">
            <div class="span13 offset1">
                <p class="text_justify">Contact webmaster@paraben.com with questions or problems. </p>

                <p class="text_justify">The information contained on this website is protected by copyright and may not be distributed, modified or reproduced without prior written consent from Paraben. All of Paraben’s trademarks and service marks are protected by law and use of such marks is not permitted without prior written consent from Paraben. </p>

                <p class="text_justify">Permission to use documents (such as white papers, press releases, datasheets and FAQs) from this site (hereinafter referred to as "website") is granted, provided that use of such documents from this website is for informational use only and will not be copied or posted on any network computer or broadcast in any media and that no modifications of any documents are made. Use for any other purpose is expressly prohibited by law, and may result in severe civil and criminal penalties. Violators will be prosecuted to the maximum extent possible. </p>

                <p class="text_justify">Documents specified include the design or layout of the Paraben.com website. All elements of website are protected by trade dress and other laws and may not be copied or imitated in whole or in part. No logo, graphic, sound or image from any Paraben website may be copied or retransmitted unless expressly permitted by Paraben. </p>

                <p class="text_justify">PARABEN MAKES NO REPRESENTATIONS OR WARANTIES ABOUT THE INFORMATION CONTAINED IN THE DOCUMENTS AND RELATED GRAPHICS PUBLISHED ON THIS SERVER FOR ANY PURPOSE. ALL SUCH DOCUMENTS AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND. Paraben HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. </p>

                <p class="text_justify">IN NO EVENT SHALL PARABEN BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF INFORMATION AVAILABLE FROM THIS SITE. </p>

                <p class="text_justify">THE DOCUMENTS AND RELATED GRAPHICS PUBLISHED ON THIS SERVER COULD INCLUDE TECHNICAL INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. PARABEN MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE PRODUCT(S) AND/OR THE PROGRAM(S) DESCRIBED HEREIN AT ANY TIME. </p>

                <p class="text_justify">THE LINKS IN THIS AREA WILL LET YOU LEAVE PARABEN’S SITE. THE LINKED SITES ARE NOT UNDER THE CONTROL OF PARABEN AND PARABEN IS NOT RESPONSIBLE FOR THE CONTENTS OF ANY LINKED SITE OR ANY LINK CONTAINED IN A LINKED SITE, OR ANY CHANGES OR UPDATES TO SUCH SITES. </p>
            </div>
        </div>
    </div>
</asp:Content>
