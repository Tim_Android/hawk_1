﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailManagement;
using HawkDataAccess;
using EmailManagement.Types;
using HawkDataAccess.Models;
using Hawk.Infrastructure.BLL;
using System.Configuration;

namespace HawkHome.Public
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TokenManagement.CleanTokens();
            if (IsPostBack)
            {
                this.confirmation.Visible = true;
                this.resetForm.Visible = false;
            }
            else
            {
                if (Request.QueryString["token"] != null)
                {
                    string userEmail = TokenManagement.CheckToken(Request.QueryString["token"]);
                    if (userEmail != null)
                    {
                        MembershipUser user = Membership.GetUser(userEmail);
                        if (user != null)
                        {
                            string newPassword = user.ResetPassword();
                            UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);
                            Email emailData = EmailComposer.InitGeneralinfo(user.Email, EmailManagement.EmailResources.NewPasswordSubj);
                            emailData.bodyParams.Add("@Password@", newPassword);
                            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
                            emailData.bodyParams.Add("@LastName@", userInfo.lastName);
                            EmailComposer.SendEmailNotification(EmailTypes.NewPassword, emailData);
                            TokenManagement.DeleteToken(Request.QueryString["token"]);
                            this.confirmation.Visible = true;
                            this.resetForm.Visible = false;
                            lblResult.Text = Resources.General.labelPassResetNewPassSent;
                            btnToLogin.Visible = true;
                        }
                        else
                        {
                            Response.Redirect("~/Public/Error/General.aspx", true);
                        }
                    }
                    else
                    {
                        this.confirmation.Visible = true;
                        this.resetForm.Visible = false;
                        lblResult.Text = Resources.General.labelPassResetNoSuchToken;
                    }
                }
            }
        }

        protected void ResetButton_Click(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser(this.UserName.Text);
            if (user != null)
            {
                string token = Utils.GetRandomString(Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]));
                if (TokenManagement.AddToken(user.Email, token))
                {
                    UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);
                    Email emailData = EmailComposer.InitGeneralinfo(user.Email, EmailManagement.EmailResources.ConfirmPassResetSubj);

                    string port = Utils.GetPortParameter();
                    string passwordResetLink = Utils.ConvertRelativeUrlToAbsoluteUrl(ConfigurationManager.AppSettings["ResetPasswordUrl"] + token, port);

                    emailData.bodyParams.Add("@ResetLink@", passwordResetLink);
                    emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
                    emailData.bodyParams.Add("@LastName@", userInfo.lastName);
                    EmailComposer.SendEmailNotification(EmailTypes.ConfirmPasswordReset, emailData);
                    lblResult.Text = Resources.General.labelPassResetEmailSent;
                }
                else
                {
                    Response.Redirect("~/Public/Error/General.aspx", true);
                }
            }
            else
            {
                lblResult.Text = Resources.General.labelPassResetNoUser;
            }
            btnBack.Visible = true;
        }
    }
}