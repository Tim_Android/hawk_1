﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="Prices.aspx.cs" Inherits="HawkHome.Public.Prices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="span15 mtop20 pure-skin-paraben">
            <div>
                <h4>Prices</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div class="row">
                    <div class="col-sm-7">
                        <p class="text_justify">
                            The HAWK Mobile Monitoring System offers you a discounted prices system. The longer the period you select for subscription, the lower the price for each device in a subscription. You will be able to add devices to a subscription or extend it any time after the purchase. You can also create a recurring subscription which will be automatically extended before it expires.
                        </p>
                    </div>
                    <div class="col-sm-5">
                        <table class="pure-table pure-table-horizontal full_width">
                            <thead>
                                <tr>
                                    <th class="text_align_center text_vert_align_middle">Term</th>
                                    <th class="text_align_center text_vert_align_middle">Retail Price</th>
                                    <th class="text_align_center">Online Special<br />Price Per Device in<br />U.S. Dollars</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (HawkDataAccess.Models.PriceListItemModel price in prices.Where(res => res.type == HawkDataAccess.Types.PriceListItemType.SubscriptionItem))
                                   {  %>
                                <tr>
                                    <td class="text_align_center"><%= price.label %></td>
                                    <td class="text_align_center">$<%= price.retailPrice.Value.ToString("F") + " ($" + (price.retailPrice - price.price).Value.ToString("F") + " Savings)" %></td>
                                    <td class="text_align_center">$<%= price.price.Value.ToString("F") %></td>
                                </tr>
                                <% } %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="prices_note">
                            Please note: HAWK credits purchased can only be applied towards Android OS based devices and iOS jailbroken devices. 
                            Please test that the monitored device is supported by HAWK before purchasing credits. Credits are non-refundable and non-transferable.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
