﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="Support.aspx.cs" Inherits="HawkHome.Public.Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20 pure-skin-paraben">
            <div>
                <h4>FAQ</h4>
                <hr class="edit_account_hr" />
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h5 class="text-center">General Questions</h5>

                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse22">
                                    <h4 class="support-panel-title faq-header-label">I have registered to HAWK Mobile Monitor, but I have not received a free trial? Why?</h4>
                                </a>
                            </div>
                            <div id="collapse22" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The free 7 day trial period is available only to users who register to the service for the first time. If you have already registered to HAWK Mobile Monitor in the past, you will need to <a target="_blank" href="<%= ResolveUrl("~/User/Billing/Billing.aspx") %>">buy a subscription</a> to start using the service.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="text-center">Working with Devices (Common Issues)</h5>

                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse1">
                                    <h4 class="support-panel-title faq-header-label">How do I check that an Agent is installed?</h4>
                                </a>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If an Agent is installed on a device, you will see it as HAWK in the list of applications installed on a device.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse2">
                                    <h4 class="support-panel-title faq-header-label">I have installed an Agent, but the device on which it is installed does not appear in the list of monitored devices. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>To appear in the list, the device must be connected to Internet to allow an Agent to connect to the HAWK Mobile Monitor web-site. Also make sure the Agent is installed: there must be a HAWK application in the list of installed applications on the device.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse3">
                                    <h4 class="support-panel-title faq-header-label">I have deactivated an Agent, but it still consumes device traffic. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>An Agent sends data regardless of its state. To stop the sending of data, you need to uninstall an Agent.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse4">
                                    <h4 class="support-panel-title faq-header-label">There is no Deactivate button on the Device Info tab. Where is it?</h4>
                                </a>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If the Deactivate button is not present on the tab, it means that the 30 days period, during which an Agent cannot be deactivated, has not passed yet. Please wait till the 30 days period comes to an end.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse5">
                                    <h4 class="support-panel-title faq-header-label">How do I distinguish among devices with the same brand and model?</h4>
                                </a>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>You can differentiate such devices by viewing device information, such as phone number or IMEI. You can then define different names for devices on the Device Info tab of the Monitored Devices page.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse6">
                                    <h4 class="support-panel-title faq-header-label">How much traffic does an Agent consume daily?</h4>
                                </a>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>It depends on the user activity. Generally, such features as GPS tracking and MMS messages (2.5 MB per message maximum) consume the most volume of traffic.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse7">
                                    <h4 class="support-panel-title faq-header-label">An Agent does not send monitored data. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Please check the following:</p>
                                    <ul>
                                        <li>The device has no Internet connection</li>
                                        <li>The Agent on the device is activated</li>
                                        <li>There is no anti-virus software or firewall installed on a device</li>
                                        <li>If you still do not receive any monitored data, please, contact our support and send us Agent crash logs. To see the crash logs, log in to the HAWK Mobile Monitor web-site,
                                             select the required device on the <span class="text-bold">Monitored Devices</span> page, and then enter the following address in your Internet browser:
                                             <a target="_blank" href="<%= ResolveUrl("~/User/CrashLog.aspx") %>">https://www.hawk-monitoring.com/User/CrashLog.aspx</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse23">
                                    <h4 class="support-panel-title faq-header-label">The monitored data for some period of time is absent. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse23" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The Agent has its own storage of monitored data. The storage size is limited to 10 MB. If there is no Internet connection on the device and the limit is exceeded, the storage data is erased and the device starts monitoring data anew.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="text-center">Working with Android Device</h5>
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse33">
                                    <h4 class="support-panel-title faq-header-label">How to Uninstall Agent from Android OS Device</h4>
                                </a>
                            </div>
                            <div id="collapse33" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>To uninstall an Agent from Android OS Device:</p>
                                    <p>1. In the device Settings, select Security > Device administrators.</p>
                                    <img class="img-responsive supportPictureSize" src="<%=ResolveUrl("~/Images/Support/uninstall1.png")%>" />
                                    <p>2. In the Device administrators menu, select Device Admin.</p>
                                    <p>3. In the Device administrator menu, tap Deactivate.</p>
                                    <img class="img-responsive supportPictureSize" src="<%=ResolveUrl("~/Images/Support/uninstall2.png")%>" />
                                    <p>4. The device screen will turn blank and then you will be asked to enter the password defined during Agent installation. Enter the password and tap OK.</p>
                                    <p>5. In the device settings, select Apps.</p>
                                    <p>6. Select HAWK.</p>
                                    <img class="img-responsive supportPictureSize" src="<%=ResolveUrl("~/Images/Support/uninstall3.png")%>" />
                                    <p>7. In the App info menu, tap Uninstall.</p>
                                    <img class="img-responsive supportPictureSize" src="<%=ResolveUrl("~/Images/Support/uninstall4.png")%>" />
                                    <p>8. Tap OK in the confirmation message.</p>
                                    <img class="img-responsive supportPictureSize" src="<%=ResolveUrl("~/Images/Support/uninstall5.png")%>" />
                                    <p>9. The uninstallation process starts. </p>
                                    <p>10. When the uninstallation is finished, the application disappears from the list.</p>
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse8">
                                    <h4 class="support-panel-title faq-header-label">I have copied an Agent installation file from a computer to the device, but I cannot find it.
                                        <br />
                                        What do I do?</h4>
                                </a>
                            </div>
                            <div id="collapse8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>You can download the Agent to the device directly using mobile browser. This is the recommended way of Agent downloading.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse9">
                                    <h4 class="support-panel-title faq-header-label">I cannot install an Agent on an Android OS device. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Please make sure you have allowed the installation from unknown sources on your device. To allow the installation from unknown sources, do the following:</p>
                                    <ul>
                                        <li>For Android OS 4.x and higher, select <span class="text-bold">Settings > Security > Unknown Sources</span>.</li>
                                        <li>For Android OS up to 4.0, select <span class="text-bold">Settings > Applications > Unknown Sources</span>.</li>
                                    </ul>
                                    <p>If you are still unable to install an Agent, please, contact our support.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse10">
                                    <h4 class="support-panel-title faq-header-label">I cannot uninstall an Agent from an Android OS device. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If during the installation of an Agent you activated a device administrator, you will need to disable a device administrator. To disable a device administrator, do the following:</p>
                                    <ol>
                                        <li>In the device Settings, select <span class="text-bold">Security > Device administrators</span></li>
                                        <li>In the <span class="text-bold">Device administrators</span> menu, select <span class="text-bold">Device Admin</span>.</li>
                                        <li>In the <span class="text-bold">Device administrator</span> menu, tap <span class="text-bold">Deactivate</span>.</li>
                                        <li>The device screen will turn blank and then you will be asked to enter the password defined during Agent installation. Enter the password and tap <span class="text-bold">OK</span>.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse11">
                                    <h4 class="support-panel-title faq-header-label">Does an Agent use mobile Internet to send data?</h4>
                                </a>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If mobile Internet is enabled on your device, an Agent will be able to use it to send data.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse12">
                                    <h4 class="support-panel-title faq-header-label">The Agent does not send any GPS data. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse12" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Please check that the device GPS tracking is enabled on the monitored device.</p>
                                </div>
                            </div>
                        </div>
                    </div>



                    <h5 class="text-center">Working with Data</h5>

                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse17">
                                    <h4 class="support-panel-title faq-header-label">I have selected a downloaded KML file with GPS data, but after selecting it, nothing is displayed on the View GPS data page.</h4>
                                </a>
                            </div>
                            <div id="collapse17" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The file may contain no coordinates. If the file was downloaded from the GPS History tab of the Device Monitoring page, try downloading it again and check that the GPS history data is displayed in the grid before starting the download. </p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse18">
                                    <h4 class="support-panel-title faq-header-label">Some MMS message attachments are displayed with the Error status, why?</h4>
                                </a>
                            </div>
                            <div id="collapse18" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The HAWK Mobile Monitor Server uses the standard MMS message size limit in 2.5 MB. If an MMS message attachment is more than 2.5 MB, it will not be received by the Server.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse19">
                                    <h4 class="support-panel-title faq-header-label">The GPS history shows wrong data: the device was in one place, but the GPS history says it was in another place.
                                    <br />
                                        What is wrong?</h4>
                                </a>
                            </div>
                            <div id="collapse19" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Sometimes GPS history shows not the exact location of a device, but rather a location of a Wi-Fi spot which was used to send the monitored data. Please also check the Accuracy column in the GPS history, which indicates how accurate the recorded coordinates are.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse20">
                                    <h4 class="support-panel-title faq-header-label">The time in the Device Date/Time column has changed. Why?</h4>
                                </a>
                            </div>
                            <div id="collapse20" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The time displayed in the Device Date/Time column is displayed according to the current device time zone. If the time zone was changed, the time displayed in the column will change as well.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse31">
                                    <h4 class="support-panel-title faq-header-label">Does HAWK Mobile Monitor monitor calls/SMS/MMS from blacklisted contacts?</h4>
                                </a>
                            </div>
                            <div id="collapse31" class="panel-collapse collapse">
                                <div class="panel-body">
                                    For Android OS devices, only the incoming calls from contacts blacklisted with the help of the 3rd party applications are monitored.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#collapse32">
                                    <h4 class="support-panel-title faq-header-label">Can I view the SMS messages/MMS messages/call history for the period before the Agent installation on the device?</h4>
                                </a>
                            </div>
                            <div id="collapse32" class="panel-collapse collapse">
                                <div class="panel-body">
                                    No. Only messages created and calls performed after the Agent installation will be displayed on the web-site.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="chat-support">
        <div id="craftysyntax_1">
            <div id="cs-container"></div>
            <script type="text/javascript" src="https://localhost/livehelp_js.php?eo=0&amp;department=1&amp;serversession=1&amp;pingtimes=10&amp;secure=Y&amp;dynamic=Y&amp;creditline=L"></script>
        </div>
    </div>
</asp:Content>
