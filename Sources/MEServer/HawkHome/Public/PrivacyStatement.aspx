﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="PrivacyStatement.aspx.cs" Inherits="HawkHome.Public.PrivacyStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="span15">
        <div>
            <h4>Privacy Policy</h4>
            <hr class="edit_account_hr" />
        </div>
        <div class="row-fluid">
            <div class="span13 offset1">
                <p class="text_justify">Paraben respects your right to privacy and understands that visitors to hawkmonitoring.com want to be in control of their personal information. Paraben presents information on hawkmonitoring.com that is tailored to the needs of our visitors. For this reason, we may ask you to register or to provide personal information when you visit certain areas of hawkmonitoring.com, including our shopping cart, our product registration section, our download free software section, any contest or events section, or to subscribe to our newsletter. </p>

                <p class="text_justify">We will use the personal information you share with us to ensure the content, services, and advertising that we provide on hawkmonitoring.com are tailored to your current and future needs. If you provide hawkmonitoring.com with your mailing address or telephone number, Paraben and its partners may use this information to alert you to product upgrades, special offers, updated information, and new services. If you provide your e-mail address, Paraben may contact you via e-mail as well. If you do not want Paraben, our partners, or selected third parties to contact you, you may opt out when you provide personal information or contact Paraben directly at webmaster@Paraben.com </p>

                <p class="text_justify">If you choose not to register or provide personal information, you can still visit most of hawkmonitoring.com. However, you will not have access to areas that require personal identification. </p>
            </div>
        </div>
    </div>
</asp:Content>
