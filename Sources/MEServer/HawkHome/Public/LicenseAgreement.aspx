﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="LicenseAgreement.aspx.cs" Inherits="HawkHome.Public.LicenseAgreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div class="control-group">
                <h4>End User License Agreement</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <p>
                    The term “us” or “we” or “our” refers to the owners and operators of Hawk Mobile. The term “you” or “user” refers to the user or purchaser of the Hawk Mobile software and services (herein “Hawk Mobile”).
                    Important! These terms of service (TOS) govern your use of Hawk Mobile. By opening an account and accessing these services, you are indicating your acknowledgment and acceptance of these terms of use. These terms are subject to change at any time. Your use of Hawk Mobile after such changes are implemented constitutes your acknowledgment and acceptance of the changes. Please consult these terms of use prior to each and every use for any changes.
                </p>
                <h5 class="mtop20">LEGAL USE OF SERVICES</h5>
                <p>
                    It may be a federal and/or state offense to install monitoring/surveillance software onto a Phone/Computer which you do not own or have proper authorization to install. It may also be an offense in your jurisdiction to monitor the activities of other individuals. Check all state, federal and local laws before installing any Monitoring Software such as Hawk Mobile. You may also have to notify a person that they are being monitored if they are over age 18. Federal or local law governs the use of some types of software; it is responsibility of the user to follow such laws.”
                </p>
                <p>
                    The Computer Fraud and Abuse Act ("CFAA", 18 U.S.C. § 1030) In Section 1030(g), CFAA provides that "[a]ny person who suffers damage or loss by reason of a violation of this section may maintain a civil action against the violator to obtain compensatory damages and injunctive relief or other equitable relief." 18 U.S.C. 1030(g). CFAA defines "damage" as "any impairment to the integrity or availability of data, a program, a system, or information." Id. at § 1030(e)(8). CFAA defines "loss" as "any reasonable cost to any victim, including the cost of responding to an offense, conducting a damage assessment, and restoring the data, program, system, or information to its condition prior to the offense, and any revenue lost, cost incurred, or other consequential damages incurred because of interruption of service." Id. at § 1030(e)(11).
                </p>
                <p>
                    It is the responsibility of the user to determine, and obey, all applicable laws in their country and/or local jurisdiction regarding the use of the software and services. The software and service is intended to provide the Licensee with the ability to capture, store and control access to their own information.
                </p>
                <p>
                    By installing Hawk Mobile, you represent that Hawk Mobile will be used in only a lawful manner. Logging other people's Cell Phone or Computer data or installing Hawk Mobile on another person's Phone/Computer without their knowledge can be considered as an illegal activity in your country. Hawk Mobile assumes no liability and is not responsible for any misuse or damage. It is the final user's responsibility to obey all laws in their country and/or State.
                </p>
                <p>
                    Regardless of the state, it is usually illegal to record a conversation to which you either are not a party, do not have consent to tape, or could not naturally overhear. Federal law and most state laws also make it illegal to disclose the contents of an illegally intercepted call or communication. 
                </p>
                <p>
                    We do not manage the data, nor control distribution of data, nor access personal data captured or stored on servers and databases we provide. We cannot, and have no responsibility to, access, recover, retrieve or read any data or information captured by Licensee or other party use of the software and service. The publisher and vendor make no warranty, assume no liability, and are not responsible in any way for any misuse or damage caused by using the software or services. <span class="label_color_black">The software user must accept all risk and liability for use.</span> Use of the software and service constitutes acceptance of these terms & conditions and grants indemnification of the software supplier. All trademark, copyright images and word marks displayed on this website are property of their respective owners.
                </p>
                <h5 class="mtop20">TERM</h5>
                <p>
                    This Agreement is effective from the date of acceptance and shall be renewed on a monthly basis until either party terminates the agreement for any reason. The terms and conditions relating to our liability, intellectual property and confidential information shall survive termination of this Agreement for any reason.
                </p>
                <h5 class="mtop20">RIGHTS</h5>
                <p>
                    In return for your continued payment of the Fee when due, we grant you a non-exclusive right to use Hawk Mobile for the duration of the Term, subject to the terms and conditions contained within this Agreement. 
                </p>
                <h5 class="mtop20">RESTRICTIONS</h5>
                <p>
                    You are not permitted to use Hawk Mobile for the provision of any service for the benefit of third parties without written authorization from us.
                </p>
                <p>
                    You shall not modify or translate any part of Hawk Mobile, reverse engineer, disassemble or decompile the Software or any part of it, or otherwise attempt to derive or determine the source code or the logic in the Software, except to the extent and for the express purposes authorized by applicable law;
                </p>
                <p>
                    You shall not sub-license, rent, sell, lease, distribute or otherwise transfer Hawk Mobile (in whole or in part) except as permitted under this Agreement unless you obtain a separate license from us for such purpose (for example, you may not embed Hawk Mobile (in whole or in part) into another application and then distribute such application to third parties unless you first acquire a license from us);
                </p>
                <p>
                    You shall not use Hawk Mobile for the purposes of competing with us, including without limitation any gathering of competitive intelligence.
                </p>
                <p>
                    You must be eighteen (18) years or older to access Hawk Mobile services. If you are under eighteen years of age, you are not permitted to access these services for any reason.
                </p>
                <h5 class="mtop20">COPYRIGHT AND OWNERSHIP</h5>
                <p>
                    The Hawk Mobile Service including without limitation all know-how, concepts, logic and specifications are proprietary products belonging to Hawk Mobile and our licensors, and are protected throughout the world by copyright and other intellectual property rights.
                </p>
                <p>
                    No license, right or interest in our logos or trademarks is granted to you under this Agreement and you agree not to remove any product identification or notices of proprietary restrictions. This License does not convey to you an interest in or to the Software and Documentation, but only a limited right of use revocable in accordance with the terms of this License.
                </p>
                <h5 class="mtop20">DATA COLLECTION</h5>
                <p>
                    In the event it can be determined that you have collected or attempted to collect data from a smartphone or computer that you do not own without permission of the owner or users of said device, your access to said data shall cease. We will cooperate fully with state and federal authorities in such an event by providing them with copies of all data unlawfully provided or collected by you.
                </p>
                <p>
                    Any data provided or collected by you in accordance with state and federal law can be deleted upon receipt of a written request sent to:
                </p>
                <address>
                    Hawk Mobile<br>
                    469 East 1000 South<br>
                    Pleasant Grove, UT 84062<br>
                </address>
                <p>
                    Personal information collected by Hawk Mobile may be stored and processed in the United States or any other country or region in which Hawk Mobile or its affiliates, subsidiaries, or service providers maintain facilities. You consent to any such transfer of information outside of your country or region. Hawk Mobile abides by the safe harbor framework as set forth by the U.S. Department of Commerce regarding the collection, use, and retention of data from the European Union, European Economic Area and Switzerland.
                </p>
                <p>
                    Access to any data recorded using Hawk Mobile will be limited to 45 days. Any information remaining longer than 45 days shall be deleted or retained by Hawk Mobile in the event of an investigation by any applicable governmental authority.  
                </p>
                <h5 class="mtop20">BILLING</h5>
                <p>
                    By opening your Hawk Mobile account, you authorize us to charge you a monthly fee at the then current rate, and any other charges you may incur in connection with your use of the Hawk Mobile service to your debit or credit card. You acknowledge that the amount billed each month may vary from month to month for reasons that may include differing amounts due to promotional offers and/or changing or adding a plan, and you authorize us to charge your debit or credit card for such varying amounts, which may be billed monthly in one or more charges.
                </p>
                <p>
                    We reserve the right to adjust pricing for our service or any components thereof in any manner and at any time as we may determine in our sole and absolute discretion. Except as otherwise expressly provided for in these Terms of Use, any price changes to your service will take effect following a reasonable period of notice to you by email.
                </p>
                <p>
                    ALL PAYMENTS ARE NONREFUNDABLE AND THERE ARE NO REFUNDS OR CREDITS FOR PARTIALLY USED PERIODS. Following any cancellation, however, you will continue to have access to Hawk Mobile through the end of your current billing period. At any time, and for any reason, we may provide a refund, discount, or other consideration to some or all of our customers ("credits"). The amount and form of such credits, and the decision to provide them, are at our sole and absolute discretion. The provision of credits in one instance does not entitle you to credits in the future for similar instances, nor does it obligate us to provide credits in the future, under any circumstance.
                </p>
                <p>
                    You may cancel your Hawk Mobile account at any time, and you will continue to have access to Hawk Mobile through the end of your monthly billing period. WE DO NOT PROVIDE REFUNDS OR CREDITS FOR ANY PARTIAL-MONTH PERIODS.
                </p>
                <h5 class="mtop20">DISCLAIMER</h5>
                <p>
                    You acknowledge and understand that it is illegal in most countries to install monitoring / surveillance software onto a cell phone/computer which you do not own or have proper authorization to install. It may also be an offense in your area to monitor the activities of other individuals without their prior written consent.
                </p>
                <p>
                    You hereby agree to check all laws in your own respective country, and also the country where the monitoring / surveillance will take place if different from your own country, <span class="label_color_black">before installing any type of software or hardware which may be used for such purposes.</span> We absolutely do not condone the use of such software for illegal purposes. It is the responsibility of the user to follow all such laws that govern the use of some types of software. By continuing to access this site you agree to comply with such laws and agree that it is your sole responsibility to comply with such laws.
                </p>
                <p>
                    You also agree you will indemnify and hold the owners and operators of hawk mobile, its subsidiaries, affiliates, licensors, content providers, service providers, employees, agents, officers, directors, and contractors harmless from any and all damage, litigation, or legal predicaments that may arise out of your use of or abuse of such software.
                </p>
                <p>
                    You further agree and acknowledge that laws and procedures change frequently and are subject to different interpretations. The situations described on this site may not apply to your particular circumstances. Thus, no action recommended on this site should be taken without first obtaining advice and counsel of a trained legal professional.
                </p>
                <p>
                    You hereby agree that (i) you will use, disclose and/or transport the Service in accordance with any applicable export control laws and regulations; (ii) you will not re-export or re-transfer the Service to any destination subject to restrictive sanctions, measures or trade embargoes implemented at a national, regional or international level without the appropriate authorization; (iii) you are solely responsible for fulfilling any applicable governmental requirements in connection with your use, disclosure and/or transport of the Service. You agree to indemnify and hold us harmless from and against any claim, loss, liability or damage suffered or incurred by us resulting from or related to your breach of this Agreement.
                </p>
                <h5 class="mtop20">WARRANTIES</h5>
                <p>
                    Your use of Hawk Mobile is at your own risk. The content is provided as is and without warranties of any kind, either expressed or implied. Our company disclaims all warranties, including any implied warranties of merchantability, fitness for a particular purpose, title, or non-infringement. We do not warrant that the functions or content contained in Hawk Mobile will be uninterrupted or error-free, that defects will be corrected, or that Hawk Mobile is free of viruses or other harmful components. We do not warrant or make any representation regarding use, or the result of use, of the content in terms of accuracy, reliability, or otherwise. The content may include technical inaccuracies or typographical errors, and we may make changes or improvements at any time. You, and not the owners and operators of Hawk Mobile, assume the entire cost of all necessary servicing, repair or correction in the event of any loss or damage arising from the use of Hawk Mobile or its content. We make no warranties that your use of the content will not infringe the rights of others and assumes no liability or responsibility for errors or omissions in such content.
                </p>
                <h5>GOVERNING LAW</h5>
                <p>
                    This License shall be construed and governed in accordance with the laws of the State of Utah. Any dispute arising out of or with respect to this License between You and Licensor shall be solely adjudicated by the competent Federal or State court situated in Salt Lake City, Utah. You and Licensor consent to the venue and jurisdiction of such court for purposes of any such dispute.
                </p>
                <h5 class="mtop20">SEVERABILITY</h5>
                <p>
                    Should any term of this License be declared void or unenforceable by any court of competent jurisdiction, such declaration shall have no effect on the remaining terms hereof.
                </p>
                <h5 class="mtop20">NO WAIVER</h5>
                <p>
                    Our failure to enforce any rights hereunder or to take action against you in the event of any breach hereunder shall not be deemed a waiver as to our subsequent enforcement of rights or subsequent actions in the event of future breach.
                </p>
            </div>
        </div>
    </div>
</asp:Content>
