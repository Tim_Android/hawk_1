//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Billing {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Billing() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Billing", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to April.
        /// </summary>
        internal static string April {
            get {
                return ResourceManager.GetString("April", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to August.
        /// </summary>
        internal static string August {
            get {
                return ResourceManager.GetString("August", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Credit Card: Visa, MasterCard, American Express and Discover Card.
        /// </summary>
        internal static string AuthorizeNetLabel {
            get {
                return ResourceManager.GetString("AuthorizeNetLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Subscription.
        /// </summary>
        internal static string CreateNewLic {
            get {
                return ResourceManager.GetString("CreateNewLic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to December.
        /// </summary>
        internal static string December {
            get {
                return ResourceManager.GetString("December", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have to choose subscription extension period or additional devices to proceed.
        /// </summary>
        internal static string EmptyCart {
            get {
                return ResourceManager.GetString("EmptyCart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to February.
        /// </summary>
        internal static string February {
            get {
                return ResourceManager.GetString("February", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to January.
        /// </summary>
        internal static string January {
            get {
                return ResourceManager.GetString("January", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to July.
        /// </summary>
        internal static string July {
            get {
                return ResourceManager.GetString("July", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to June.
        /// </summary>
        internal static string June {
            get {
                return ResourceManager.GetString("June", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} - {1} device(s).
        /// </summary>
        internal static string LicenseNameTemplate {
            get {
                return ResourceManager.GetString("LicenseNameTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to March.
        /// </summary>
        internal static string March {
            get {
                return ResourceManager.GetString("March", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to May.
        /// </summary>
        internal static string May {
            get {
                return ResourceManager.GetString("May", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your subscription number is: {0}.
        /// </summary>
        internal static string NewSubscrLabel {
            get {
                return ResourceManager.GetString("NewSubscrLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No order information has been found. Please try again.
        /// </summary>
        internal static string NoOrderInfoFound {
            get {
                return ResourceManager.GetString("NoOrderInfoFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Subscription.
        /// </summary>
        internal static string NoSubscription {
            get {
                return ResourceManager.GetString("NoSubscription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to November.
        /// </summary>
        internal static string November {
            get {
                return ResourceManager.GetString("November", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to October.
        /// </summary>
        internal static string October {
            get {
                return ResourceManager.GetString("October", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Signup Fee: 1 month for 1 device Subscription.
        /// </summary>
        internal static string OrderDescriptionSignupFee {
            get {
                return ResourceManager.GetString("OrderDescriptionSignupFee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Subscription.
        /// </summary>
        internal static string OrderDescriptionTemplate1 {
            get {
                return ResourceManager.GetString("OrderDescriptionTemplate1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to , {0} additional devices.
        /// </summary>
        internal static string OrderDescriptionTemplate2 {
            get {
                return ResourceManager.GetString("OrderDescriptionTemplate2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional {0} Subscription.
        /// </summary>
        internal static string OrderDescriptionTemplateExt {
            get {
                return ResourceManager.GetString("OrderDescriptionTemplateExt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} new devices.
        /// </summary>
        internal static string OrderDevOnlyDescription {
            get {
                return ResourceManager.GetString("OrderDevOnlyDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured during PayPal payment initialization. Please use another payment method or retry payment later.
        /// </summary>
        internal static string PayPalInitError {
            get {
                return ResourceManager.GetString("PayPalInitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PayPal.
        /// </summary>
        internal static string PayPalLabel {
            get {
                return ResourceManager.GetString("PayPalLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone Support 0 days.
        /// </summary>
        internal static string PhoneSupportExtendLabel {
            get {
                return ResourceManager.GetString("PhoneSupportExtendLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HAWK Subscription.
        /// </summary>
        internal static string ProductName {
            get {
                return ResourceManager.GetString("ProductName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to September.
        /// </summary>
        internal static string September {
            get {
                return ResourceManager.GetString("September", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0 days.
        /// </summary>
        internal static string SubscriptionExtendLabel {
            get {
                return ResourceManager.GetString("SubscriptionExtendLabel", resourceCulture);
            }
        }
    }
}
