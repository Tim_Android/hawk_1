﻿#region namespaces 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

#endregion

namespace HawkHome.Code
{
    public class JsonConfigObject
    {
        public int agent_id { get; set; }
        public string agent_name { get; set; }
        public string crash_test_enabled { get; set; }
        public int log_level { get; set; }
        public int ls_max_sz { get; set; }
        public string service_url { get; set; }
        public string svn_revision { get; set; }
    }
}