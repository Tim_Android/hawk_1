﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace HawkHome.Code
{
    public class JSSerializer
    {
        private static JavaScriptSerializer serializer;
        private static readonly object mutex = new object();

        public static JavaScriptSerializer GetInstance()
        {
            lock (mutex)
            {
                if (serializer == null)
                {
                    serializer = new JavaScriptSerializer();
                }
            }
            return serializer;
        }
    }
}