﻿using Domain.Entities.Enums;
using HawkBilling.Core.Type;
using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using HawkCommonResources;


namespace HawkHome.Code
{
    public class InitHelper
    {
        public static void InitPaymentSystemsRadioGroup(RadioButtonList rblPaymentSystem)
        {
            ListItem anetOption = new ListItem(Resources.Billing.AuthorizeNetLabel, PaymentProcessors.AuthorizeNet.ToString());
            ListItem paypalOption = new ListItem(Resources.Billing.PayPalLabel, PaymentProcessors.PayPal.ToString());
            rblPaymentSystem.Items.Add(anetOption);
            rblPaymentSystem.Items.Add(paypalOption);
        }

        public static void InitOrderMessages(ProductModel order)
        {
            order.messages.Add(ProductModel.NEW_SUBSCRIPTION1, Resources.Billing.OrderDescriptionTemplate1);
            order.messages.Add(ProductModel.NEW_SUBSCRIPTION2, Resources.Billing.OrderDescriptionTemplate2);
            order.messages.Add(ProductModel.NEW_DEVICE, Resources.Billing.OrderDevOnlyDescription);
            order.messages.Add(ProductModel.SIGNUP_FEE, Resources.Billing.OrderDescriptionSignupFee);
            order.messages.Add(ProductModel.SUBSCRIPTION_EXT, Resources.Billing.OrderDescriptionTemplateExt);
        }
        public static void InitAfterBoughtBehavior(RadioButtonList afterBoughtBehavior)
        {
            afterBoughtBehavior.Items.Add(new ListItem(SubscriptionManagement.DoNotActivateSubscriptionNow, AfterBoughtBehavior.DoNotActivateSubscriptionNow.ToString()));
            afterBoughtBehavior.Items.Add(new ListItem(SubscriptionManagement.ActivateSubscriptionOn, AfterBoughtBehavior.ActivateSubscriptionOn.ToString()));
        }
    }
}