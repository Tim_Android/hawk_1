﻿#region namespaces

using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Diagnostics;
using HawkDataAccess.Exceptions;
using Hawk.Infrastructure.AppLogger;

#endregion

namespace HawkHome.Code
{
    public static class AgentBuilder
    {

        private static AgentBuilderSettings _settings;

        static AgentBuilder()
        {
            _settings = new AgentBuilderSettings();
        }

        public static string BuildAgentInstaller(string agentId, string agentName)
        {
            string outDir = Path.Combine(Path.Combine(AgentBuilderSettings.buildRoot, "installs"), agentId);
            string srcDir = Path.Combine(AgentBuilderSettings.buildRoot, "src");
            string agentSVNRevision = GetSVNRevision(srcDir);

            string outFilePath = Path.Combine(outDir, _settings.GetResultAgentApkName(agentSVNRevision));

            try
            {
                bool isAgentExists = File.Exists(outFilePath);

                if (!isAgentExists && Directory.Exists(outDir))
                {
                    Directory.Delete(outDir, true);    // Deleting old builds
                }

                if (!isAgentExists)
                {
                    string tmpFolderName = Path.Combine(AgentBuilderSettings.buildRoot, Guid.NewGuid().ToString());
                    DirectoryCopy(srcDir, tmpFolderName, true);

                    try
                    {
                        DateTime dt = DateTime.Now;
                        PatchAgentSettings(tmpFolderName, agentId, agentName,
                            dt.Year.ToString() + "." + dt.Month.ToString() + "." + dt.Day.ToString() + "." + agentSVNRevision,
                            _settings.agentConnectionString);

                        using (Process p = new Process())
                        {
                            p.StartInfo.EnvironmentVariables["PATH"] = AgentBuilderSettings.buildEnvironment + ";" + p.StartInfo.EnvironmentVariables["PATH"];
                            p.StartInfo.WorkingDirectory = tmpFolderName;
                            p.StartInfo.FileName = Environment.GetEnvironmentVariable("COMSPEC");
                            p.StartInfo.Arguments = String.Format("/c \"{0}\"", _settings.buildCommand);
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = true;
                            p.StartInfo.RedirectStandardError = true;
                            p.StartInfo.CreateNoWindow = true;

                            StringBuilder sb = new StringBuilder();
                            p.Start();
                            sb.Append(p.StandardOutput.ReadToEnd());
                            sb.Append(p.StandardError.ReadToEnd());
                            p.WaitForExit();

                            if (p.ExitCode != 0)
                            {
                                throw new Exception("Build failed:\n" + sb.ToString());
                            }

                            string apkPath = Path.Combine(tmpFolderName, Path.Combine("bin", _settings.agentApkName));

                            Directory.CreateDirectory(outDir);

                            File.Copy(apkPath, outFilePath);
                        }
                    }
                    finally
                    {
                        Directory.Delete(tmpFolderName, true);
                    }
                }
            }
            catch (System.IO.IOException ex)
            {
                Logger.LogError(ex);
                throw new SimultaniousBuildException(Resources.Error.SimultaniousBuild);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Agent build failed", ex);
            }

            return outFilePath;
        }

        private static string GetSVNRevision(string pathToSrc)
        {
            const string nameToolToGetSVNVersion = "svnversion";

            string svnRevision = "unknown";

            try
            {
                Process p = new Process();

                p.StartInfo.WorkingDirectory = pathToSrc;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = nameToolToGetSVNVersion;
                p.Start();
                svnRevision = p.StandardOutput.ReadLine();
                p.WaitForExit();
            }
            catch (Exception)
            {
                svnRevision = AgentBuilderSettings.agentSvnRevision;
            }

            return svnRevision;
        }

        private static void PatchAgentSettings(string pathToSrc, string agentId, string agentName, string agentVersion, string agentConnectionString)
        {
            string text;
            string path = Path.Combine(pathToSrc, ConfigurationManager.AppSettings["settingsPath"]);
            using (var reader = new StreamReader(path))
            {
                text = reader.ReadToEnd();
            }
            
            text = Regex.Replace(text, "AGENT_ID\\s+=\\s+\"([^\"]*)\"", String.Format("AGENT_ID = \"{0}\"", agentId));
            text = Regex.Replace(text, "DEVICE_INFO_AGENT_NAME\\s+=\\s+\"([^\"]*)\"", String.Format("DEVICE_INFO_AGENT_NAME = \"{0}\"", agentName));

            
            text = Regex.Replace(text, "DEVICE_INFO_SVN_VERSION\\s+=\\s+\"([^\"]*)\"", String.Format("DEVICE_INFO_SVN_VERSION = \"{0}\"", agentVersion));

            text = Regex.Replace(text, "OPT_WEBSERVER_DEF\\s+=\\s+\"([^\"]*)\"", String.Format("OPT_WEBSERVER_DEF = \"http://{0}\"", agentConnectionString));
            text = Regex.Replace(text, "OPT_WEBSERVER_SSL_DEF\\s+=\\s+\"([^\"]*)\"", String.Format("OPT_WEBSERVER_SSL_DEF = \"https://{0}\"", agentConnectionString));

            using (var writer = new StreamWriter(path))
            {
                writer.Write(text);
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private class AgentBuilderSettings
        {
            public bool testingMode
            {
                get
                {
                    return _testingMode;
                }
            }

            public string buildCommand
            {
                get
                {
                    return (testingMode) ? ConfigurationManager.AppSettings["buildCommandTest"]
                        : ConfigurationManager.AppSettings["buildCommandLive"];
                }
            }

            public string agentApkName
            {
                get
                {
                    return (testingMode) ? ConfigurationManager.AppSettings["apkNameTest"]
                        : ConfigurationManager.AppSettings["apkNameLive"];
                }
            }

            public string hostDataServiceConnection
            {
                get
                {
                    return (testingMode) ? ConfigurationManager.AppSettings["hostDataServiceConnectionTest"]
                        : ConfigurationManager.AppSettings["hostDataServiceConnectionLive"];
                }
            }

            public string portDataServiceConnection
            {
                get
                {
                    return (testingMode) ? ConfigurationManager.AppSettings["portDataServiceConnectionTest"]
                        : ConfigurationManager.AppSettings["portDataServiceConnectionLive"];
                }
            }

            public string agentConnectionString
            {
                get
                {
                    return hostDataServiceConnection + ":" + portDataServiceConnection;
                }
            }

            public static string buildRoot
            {
                get
                {
                    return ConfigurationManager.AppSettings["buildRoot"];
                }
            }

            public static string buildEnvironment
            {
                get
                {
                    return ConfigurationManager.AppSettings["buildEnv"];
                }
            }

            public static string agentSvnRevision
            {
                get
                {
                    return ConfigurationManager.AppSettings["agentSvnRevision"];
                }
            }

            private bool _testingMode;

            public AgentBuilderSettings()
            {
                _testingMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            }

            public string GetResultAgentApkName(string agentSvnRevision)
            {
                return string.Format(ConfigurationManager.AppSettings["resultApkName"], agentSvnRevision, testingMode.ToString());
            }
        }
    }
}