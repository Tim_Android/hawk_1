﻿#region namespaces

using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json;
using ConfigurationManager = System.Configuration.ConfigurationManager;

#endregion
namespace HawkHome.Code
{
    /// <summary>
    /// Deprecated. iOS agent is no longer being built as it is distributed through Cydia
    /// </summary>
    public static class IosAgentBuild
    {
        public static string GetAgentPath()
        {
            string currentPathToAgent = Path.Combine(AgentSettings.AgentRoot, AgentSettings.AgentName);

            bool agentExists = File.Exists(currentPathToAgent);
            if (!agentExists)
            {
                throw new DirectoryNotFoundException("Agent does not exist or could not be found: " + currentPathToAgent);
            }

            return currentPathToAgent;
        }

        private class AgentSettings
        {
            public static string AgentRoot
            {
                get { return ConfigurationManager.AppSettings["agentRoot"]; }
            }

            public static string AgentName
            {
                get { return ConfigurationManager.AppSettings["agentNameLive"]; }
            }
        }
    }
}