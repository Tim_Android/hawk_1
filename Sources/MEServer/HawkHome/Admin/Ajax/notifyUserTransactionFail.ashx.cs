﻿using EmailManagement;
using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkHome.Admin.Ajax
{
    /// <summary>
    /// Summary description for notifyUserTransactionFail
    /// </summary>
    public class TransactionFailNotifier : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            OperationResultModel result = new OperationResultModel();
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN) && context.Request["data"] != null)
            {
                int licId = Convert.ToInt32(context.Request["data"]);

                LicenseModel license = LicenseModel.GetLicenseInfo(licId);
                UserInfoModel userInfo = UserInfoModel.GetUserInfo(license.subscriptionId);

                if (license != null && userInfo != null)
                {
                    try
                    {
                        Email emailData = EmailComposer.InitGeneralinfo(userInfo.email, EmailManagement.EmailResources.AutochargeFailUserSubj);
                        EmailManager.SendAutochargeFailNotice(license, userInfo, emailData);

                        result.code = OperationResultModel.CODE_SUCCESS;
                        result.message = Resources.Success.EmailSent;
                    }
                    catch (Exception)
                    {
                        result.code = OperationResultModel.CODE_WARNING;
                        result.message = Resources.Error.EmailSent;
                    }
                }
            }
            string response = JSSerializer.GetInstance().Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}