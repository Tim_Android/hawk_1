﻿using Hawk.Infrastructure.BLL;
using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HawkHome.Admin.Ajax
{
    /// <summary>
    /// Summary description for getLicensesToCheck
    /// </summary>
    public class LicensesChecker : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN) && context.Request.Params["data"] != null)
            {
                LicenseModel[] licenses = LicenseModel.GetLicensesToCheck(Convert.ToInt32(ConfigurationManager.AppSettings["licenseCheckPeriod"]));   

                var convertedDatesLicenseList = licenses.Select(res => new
                {
                    id = res.id,
                    firstName = res.firstName,
                    lastName = res.lastName,
                    creationDate = res.creationDate.ToString("g"),
                    subscrEndDate = res.subscrEndDate.ToString("g"),
                    email = res.email,
                    subscriptionId = res.subscriptionId
                });

                int totalPages = 0;
                int currentPage = Convert.ToInt32(context.Request.Params["data"]);
                int recordsOnPage = Convert.ToInt32(ConfigurationManager.AppSettings["defaultRecordsPerPage"]);
                var pageContents = Utils.Paginate(convertedDatesLicenseList, recordsOnPage, convertedDatesLicenseList.Count(), ref currentPage, out totalPages);

                string response = JSSerializer.GetInstance().Serialize(new { 
                    page = currentPage,
                    licenses = pageContents,
                    totalPages = totalPages
                });
                context.Response.ContentType = "application/json";
                context.Response.Write(response);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}