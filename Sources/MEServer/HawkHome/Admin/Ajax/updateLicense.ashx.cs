﻿using HawkDataAccess;
using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkHome.Admin.Ajax
{
    /// <summary>
    /// Summary description for updateLicense
    /// </summary>
    public class LicenseUpdater : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            OperationResultModel result = new OperationResultModel();
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                AutochargeUpdateModel updateInfo = JSSerializer.GetInstance().Deserialize<AutochargeUpdateModel>(context.Request["data"]);

                LicenseModel license = LicenseModel.GetLicenseInfo(updateInfo.licId);

                if (updateInfo != null && license.isAutochargeEnabled) 
                {//license exists and autocharge is enabled
                    string comment = PaymentHistoryItemModel.BuildCommentString(true, license.type);

                    if (AccountManagement.ExtendSubscription(license.subscriptionId, updateInfo.transactionId, comment))
                    {
                        result.code = OperationResultModel.CODE_SUCCESS;
                        result.message = Resources.Success.ManualAutocharge;
                    }
                    else
                    {
                        result.code = OperationResultModel.CODE_WARNING;
                        result.message = Resources.Error.ManualAutocharge;
                    }
                }
            }
            string response = JSSerializer.GetInstance().Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}