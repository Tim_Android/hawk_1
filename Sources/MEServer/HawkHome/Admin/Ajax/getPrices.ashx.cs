﻿using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkHome.Admin.Ajax
{
    /// <summary>
    /// Summary description for getPrices
    /// </summary>
    public class PriceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                PriceListItemModel[] priceList = null;

                var isPromoCodePage = HttpContext.Current.Request.QueryString["isPromoCodePage"];
                if (isPromoCodePage != null && bool.Parse(isPromoCodePage))
                {
                    priceList = PriceListItemModel.GetPriceList(PriceListItemType.Promocode, DateTime.UtcNow);
                }
                else
                {
                    priceList = PriceListItemModel.GetPriceList(null, null);
                }

                var convertedDatesPriceList = priceList.OrderBy(res=>res.id).
                                                        Select(res => new
                {
                    id = res.id,
                    label = res.label,
                    price = res.price,
                    priceOther = res.priceOther,
                    retailPrice = res.retailPrice,
                    type = res.type,
                    startedOn = res.startedOn.Value.ToString("g")
                });
                string response = JSSerializer.GetInstance().Serialize(convertedDatesPriceList);
                context.Response.ContentType = "application/json";
                context.Response.Write(response);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}