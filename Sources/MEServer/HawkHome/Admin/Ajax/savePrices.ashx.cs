﻿using EmailManagement;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using HawkBilling.PaymentProcessors;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkHome.Code;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HawkHome.Admin.Ajax
{
    /// <summary>
    /// Summary description for savePrices
    /// </summary>
    public class PriceListSaver : IHttpHandler
    {
        private Logger logger;

        public PriceListSaver()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public void ProcessRequest(HttpContext context)
        {
            string response = String.Empty;
            OperationResultModel result = new OperationResultModel();
            bool subscriptionCancelErrors = false;
            if (context.User.Identity.IsAuthenticated && context.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                PriceListItemModel[] changedPrices = JSSerializer.GetInstance().Deserialize<PriceListItemModel[]>(context.Request["data"]);
                ProcessNewPrices(ref changedPrices);
                foreach (PriceListItemModel priceListItem in changedPrices)
                {
                    if (priceListItem.type == PriceListItemType.SubscriptionItem)
                    {//we have to get the list of users that use this price list id and subscription. Then we cancel it
                        LicenseModel[] licensesAffected = LicenseModel.GetCancelSubscriptionLicenseList(priceListItem.id);
                        if (licensesAffected.Length > 0)
                        {//we have users that require subscription cancellation 
                            foreach (LicenseModel license in licensesAffected)
                            {
                                IPaymentProcessor processor = null;
                                bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
                                switch (license.type)
                                {
                                    case PaymentProcessors.AuthorizeNet:
                                        processor = new AuthorizeNetPaymentProcessor(testMode);
                                        break;
                                    case PaymentProcessors.PayPal:
                                        processor = new PaypalPaymentProcessor(testMode);
                                        break;
                                }
                                //cancel subscription
                                var cancelResult = processor.CancelSubscription(license.subscriptionId);
                                if (cancelResult?.operResult.result == PaymentResult.Success)
                                {
                                    //commit subscription change to DB
                                    LicenseModel.UpdateLicenseSubscrId(license.id, null);
                                    //send email notification
                                    Email emailData = EmailComposer.InitGeneralinfo(license.email,EmailManagement.EmailResources.SubscriptionCancelledAsPriceChanged);

                                    EmailManager.SendSubscriptionCancellationNotice(license, emailData);
                                }
                                else
                                {
                                    logger.Warn(String.Format(Resources.Error.PriceEditUnableToChange, license.id, license.subscriptionId, license.email));
                                    subscriptionCancelErrors = true;
                                }
                            }
                        }
                    }
                    PriceListItemModel.AddNewPrice(priceListItem);
                }
                if (!subscriptionCancelErrors)
                {
                    result.code = OperationResultModel.CODE_SUCCESS;
                    result.message = Resources.Success.PriceEditSuccessful;
                }
                else
                {
                    result.code = OperationResultModel.CODE_WARNING;
                    result.message = Resources.Error.PriceEditSubscriptionCancel;
                }
            }
            else
            {
                result.code = OperationResultModel.CODE_ERROR;
                result.message = Resources.Error.AuthenticationRequired;

            }
            response = JSSerializer.GetInstance().Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(response);
        }

        /// <summary>
        /// Initializes second price value with changed one. This method is required since we transitioned to new billing model (fixed device price)
        /// </summary>
        /// <param name="changedPrices"></param>
        private void ProcessNewPrices(ref PriceListItemModel[] changedPrices)
        {
            for (int i = 0; i < changedPrices.Length; i++)
            {
                changedPrices[i].priceOther = changedPrices[i].price;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}