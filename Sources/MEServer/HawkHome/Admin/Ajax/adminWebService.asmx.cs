﻿#region

using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Services;
using HawkDataAccess.Types;
using Domain.Entities.Filters;
using Domain.Entities;
using Domain.Entities.Enums;
using System.Configuration;
using System.Web.Security;
using Hawk.Services;
using HawkDataAccess;

#endregion

namespace HawkHome.Admin.Ajax
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class adminWebService : System.Web.Services.WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string deletePromoCode(int id)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                int? res = PromoCodeModel.DeletePromoCode(id);
                return GetDeletedStatus(res);
            }
            return string.Empty;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string disablePromoCode(int id)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.IsInRole(HawkDataAccess.Const.AvailableRoles.ADMIN))
            {
                int? res = PromoCodeModel.DisablePromoCode(id);
                return GetDisabledStatus(res);
            }
            return string.Empty;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getPromoCodeHistory(PromoCodeHistoryFilter filter, Pager pager)
        {
            string response = null;

            IList<PromoCodeModel> promoCodeList = PromoCodeModel.GetPromoCodes(filter, ref pager);
            if (promoCodeList != null)
            {
                var convertedPromoCodeList = promoCodeList.Select(res => new
                {
                    id = res.id,
                    generatedDate = res.generatedDate.ToString("g"),
                    promoCode = res.promoCode,
                    deviceCount = res.deviceCount,
                    totalActivationsAvailable = res.totalActivationsAvailable,
                    activationUsed = res.activationsUsed,
                    activationsLeft = res.activationsLeft,
                    validFrom = res.validFrom.ToShortDateString(),
                    validTo = res.validTo.ToShortDateString(),
                    state = res.stateLabel,
                    expired = res.expired,
                    subscriptionPeriod = res.subscriptionPeriod,
                    toDelete = res.toDelete,
                    toDisable = res.toDisable
                });

                OperationResultModel error;
                if (promoCodeList.Count == 0)
                {
                    error = new OperationResultModel()
                    {
                        code = OperationResultModel.CODE_WARNING,
                        message = Resources.General.labelNoDataFound
                    };
                }
                else
                {
                    error = new OperationResultModel()
                    {
                        code = OperationResultModel.CODE_SUCCESS,
                        message = string.Empty
                    };
                }

                var pageState = new
                {
                    list = convertedPromoCodeList,
                    pager = pager,
                    error = error
                };
                response = JsonConvert.SerializeObject(pageState);
            }

            if (response == null)
            {
                var pageState = new
                {
                    pager = new
                    {
                        page = 1,
                        totalPages = 0,
                        totalRecords = 0,
                        recordsPerPage = 0,
                    },
                    error = new OperationResultModel
                    {
                        code = OperationResultModel.CODE_ERROR,
                        message = Resources.General.labelNoDataFound + ";" + Resources.Error.Unknown
                    }
                };
                response = JsonConvert.SerializeObject(pageState);
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getUserList(UserManagementFilter filter, Pager pager)
        {
            int licenseCheckPeriod = int.Parse(ConfigurationManager.AppSettings["licenseCheckPeriod"]);
            var userList = UserListEntryModel.GetUserList(filter, licenseCheckPeriod, ref pager);
            return JsonConvert.SerializeObject(new
            {
                userList = userList,
                pager = pager
            });
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void resendVerificationEmail(string email)
        {
            MembershipUser user = Membership.GetUser(email);

            UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);

            AccountService.SendValidationEmail(userInfo.email,
                                           Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]),
                                           ConfigurationManager.AppSettings["RegistrationValidationUrl"],
                                           userInfo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void deleteUser(string email)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            AccountService.DeleteCustomer(testMode, email);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getLicenseList(LicenseFilter filter, Pager pager)
        {

            var licenseList = LicenseModel.GetLicenseListPaged(filter, ref pager);
            return JsonConvert.SerializeObject(new
            {
                licenseList = licenseList,
                pager = pager
            });
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string updateLicense(LicenseModel license)
        {
            return JsonConvert.SerializeObject(SubscriptionManagementService.UpdateLicense(license));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getDevicesList(DeviceFilter filter, Pager pager)
        {

            var devicesList = DeviceManagement.GetDevices(filter, ref pager);
            return JsonConvert.SerializeObject(new
            {
                devicesList = devicesList,
                pager = pager
            });
        }

        #region Banned list


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string getBannedList(BannedEmailFilter filter, Pager pager)
        {

            var bannedList = BanListService.GetBannedList(filter, ref pager);
            return JsonConvert.SerializeObject(new
            {
                bannedList = bannedList,
                pager = pager
            });
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string addEmailToBannedList(BannedEmailModel data)
        {
            if (BanListService.AddEmailToBannedList(data))
            {
                return JsonConvert.SerializeObject(new Result(new NotifyMessage
                {
                    MessType = NotifyMessageType.Success,
                    MessText = Resources.Success.EmailAddedToBannedList
                }));
            }
            else
            {
                return JsonConvert.SerializeObject(new Result(new NotifyMessage
                {
                    MessType = NotifyMessageType.Error,
                    MessText = Resources.Error.EmailAlreadyExistInBannedList
                }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string updateBannedEmail(BannedEmailModel data)
        {
            BanListService.UpdateBannedEmail(data);
          
            return JsonConvert.SerializeObject(new Result(new NotifyMessage
            {
                MessType = NotifyMessageType.Success,
                MessText = Resources.Success.EmailUpdateInBannedList
            }));


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string deleteEmailFromBannedList(string email)
        {
            if (BanListService.DeleteEmailFromBannedList(email))
            {
                return JsonConvert.SerializeObject(new Result(new NotifyMessage
                {
                    MessType = NotifyMessageType.Success,
                    MessText = Resources.Success.UserlDeletedFromBannedList
                }));
            }
            else
            {
                return JsonConvert.SerializeObject(new Result(new NotifyMessage
                {
                    MessType = NotifyMessageType.Error,
                    MessText = Resources.Error.DeletingFromBannedList
                }));
            }

        }

        #endregion

        #region Private Methods

        private string GetDisabledStatus(int? res)
        {
            if (!res.HasValue)
            {
                //succsess
                var pageState = new
                {
                    state = new OperationResultModel()
                    {
                        code = OperationResultModel.CODE_SUCCESS,
                        message = Resources.Success.PromoCodeDisabled
                    }
                };

                return JsonConvert.SerializeObject(pageState);
            }

            return GetError(res);

        }

        private string GetDeletedStatus(int? res)
        {
            if (!res.HasValue)
            {
                //succsess
                var pageState = new
                {
                    state = new OperationResultModel()
                    {
                        code = OperationResultModel.CODE_SUCCESS,
                        message = Resources.Success.PromoCodeDeleted
                    }
                };

                return JsonConvert.SerializeObject(pageState);
            }

            return GetError(res);
        }

        private string GetError(int? res)
        {

            string message;

            if (res.Value == (int)ErrorType.StatusOfSelectedProCodeHasChanged)
            {
                message = Resources.Error.ActivationStatusHasChanged;
            }
            else
            {
                message = Resources.Error.Unknown;
            }

            //error
            var errorPageState = new
            {
                state = new OperationResultModel()
                {
                    code = OperationResultModel.CODE_ERROR,
                    message = message
                }
            };

            return JsonConvert.SerializeObject(errorPageState);
        }

        #endregion
    }


}
