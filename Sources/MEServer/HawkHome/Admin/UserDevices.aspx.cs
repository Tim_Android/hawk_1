﻿using Domain.Entities.Filters;
using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.Admin
{
    public partial class UserDevices : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           var userId = Request.QueryString["userId"];
            if (null != userId && string.Empty != userId)
            {
                HiddenUserId.Value = userId;
            }
           
        }

      
    }
}