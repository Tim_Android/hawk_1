﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="HawkHome.Admin.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pager.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/pagerHandler.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/spin.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/bootstrap.paginator.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/moment.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.filter_input.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/user_management.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/blockUI.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/Base/FormatUtils.js") %>"></script>
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>User Management</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div id="filter" class="row mbottom10">
                    <div class="col-sm-2">
                        <label class="control-label">Registered from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="1" data-bind="value: filter.registeredFrom" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Registered to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="1" data-bind="value: filter.registeredTo" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Email:</label>
                        <div class="controls">
                            <input class="form-control" data-bind="value: filter.email" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Email Verified:</label>
                        <div class="controls">
                            <select class="form-control" data-bind="value: filter.isEmailVerified">
                                <option value="-1">Please select an option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Has Purchased Subscriptions:</label>
                        <div class="controls">
                            <select class="form-control" data-bind="value: filter.hasPurchasedLicenses">
                                <option value="-1">Please select an option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mbottom10">
                    <button type="button" class="btn btn-sm label_color_black" data-bind="click: resetFilter">Clear filters</button>
                    <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: loadUserList">Filter Data</button>
                </div>
                <div id="loadedContent">
                    <table class="table table-bordered table-striped table-condensed table-hover long_word_fix">
                        <thead>
                            <tr>
                                <th class="text_align_center custom-header-style">Registered</th>
                                <th class="text_align_center custom-header-style">First Name</th>
                                <th class="text_align_center custom-header-style">Last Name</th>
                                <th class="text_align_center custom-header-style">Email</th>
                                <th class="text_align_center custom-header-style email-verified-col">Email Verified</th>
                                <th class="text_align_center custom-header-style">In Banned List</th>
                                <th class="text_align_center custom-header-style">Has Purchased Subscriptions</th>
                                <th class="text_align_center custom-header-style">Has Not Updated Subscriptions</th>
                                <th class="text_align_center custom-header-style">Details</th>
                                <th class="text_align_center custom-header-style tools-col">Tools</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: {
    data: userList,
    afterAdd: showEntry
}, visible: containsUsers">
                            <tr>
                                <td class="text_align_center valign_middle">
                                    <span data-bind="text: registrationDate"></span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span data-bind="text: firstName"></span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span data-bind="text: lastName"></span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span data-bind="text: email"></span>
                                </td>                               
                                <td class="text_align_center valign_middle">
                                    <span class="badge alert-success" data-bind="visible: isEmailVerified">Yes</span>
                                    <span class="badge alert-danger" data-bind="visible: !isEmailVerified">No</span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span class="badge alert-success" data-bind="visible: isBanned">Yes</span>
                                    <span class="badge alert-danger" data-bind="visible: !isBanned">No</span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span class="badge alert-success" data-bind="visible: hasPurchasedSubscr">Yes</span>
                                    <span class="badge alert-danger" data-bind="visible: !hasPurchasedSubscr">No</span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <span class="badge alert-success" data-bind="visible: hasNotUpdatedSubscr">Yes</span>
                                    <a class="explicit-link" href="<%= ResolveUrl("~/Admin/UserCheckList.aspx") %>" data-bind="visible: hasNotUpdatedSubscr">Update</a>
                                    <span class="badge alert-danger" data-bind="visible: !hasNotUpdatedSubscr">No</span>
                                </td>
                                <td class="text_align_center valign_middle">
                                    <div class="mtop5">
                                        <a class="explicit-link" data-bind="visible: isEmailVerified, attr: { 'href': '<%= ResolveUrl("~/Admin/History.aspx") %>' + '?email=' + email }">View Payment History</a><br />
                                    </div>
                                </td>
                                <td class="text_align_center valign_middle">
                                   
                                    <div class="mtop5">
                                        <div class="btn-group">
                                            <button class="btn btn-warning dropdown-toggle label_color_black" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                
                                                <li><a data-bind=" attr: { 'href': '<%= ResolveUrl("~/Admin/UserSubscriptions.aspx") %>' + '?userId=' + userId }"><span class="glyphicon glyphicon-list-alt"></span> View Subscriptions</a></li>
                                                <li><a data-bind=" attr: { 'href': '<%= ResolveUrl("~/Admin/UserDevices.aspx") %>' + '?userId=' + userId }"><span class="glyphicon glyphicon-phone"></span> View Devices</a></li>
                                                <li data-bind="visible: !isEmailVerified"><a href="#" data-bind="click: $root.resendVerificationEmail"><span class="glyphicon glyphicon-envelope"></span> Send Verification Email</a></li>
                                                <li data-bind="visible: !isBanned"><a href="#" data-bind="click: $root.clickAddToBannedList"><span class="glyphicon glyphicon-ban-circle"></span> Add To Banned List</a></li>
                                                <li data-bind="visible: isBanned"><a href="#" data-bind="click: $root.clickDeleteFromBannedList"><span class="glyphicon glyphicon-ok-circle"></span> Unban</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" data-bind="click: $root.clickDeleteUser"><span class="glyphicon glyphicon-remove-circle"></span> Delete User</a></li>
                                            </ul>
                                        </div>
                                    </div>                                 
                                </td>
                            </tr>
                        </tbody>
                        <tbody data-bind="visible: isPaginationVisible">
                            <tr>
                                <td colspan="10" class="pagination-custom text-center">
                                    <ul class="pagination pagination-lg">
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                        <tbody data-bind="visible: !containsUsers()">
                            <tr>
                                <td colspan="10">No Data Found.</td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="popupDeleteUser" class="hide popup_padding">
                        <div>
                            Do you want to permanently delete the account: <span data-bind="text: selectedUser().firstName"></span>&nbsp;<span data-bind="    text: selectedUser().lastName"></span> (<span data-bind="    text: selectedUser().email"></span>)?
                        </div>
                        <div class="elements_margin">
                            <input type="button" value="Yes" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.deleteUser" />
                            <input type="button" value="No" class="btn btn-sm label_color_black no" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
