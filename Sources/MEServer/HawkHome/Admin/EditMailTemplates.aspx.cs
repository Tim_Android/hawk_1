﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailManagement;
using EmailManagement.Models;
using System.Net;
using System.Configuration;
using EmailManagement.Types;

namespace HawkHome.Admin
{
    public partial class EditMailTemplates : System.Web.UI.Page
    {
        protected string emailContent;
        protected Dictionary<string, string> templateVars;
        protected EmailModel[] emails;

        protected const string ALLOWED_VARS_SEPARATOR = "allowedVarsSeparator";
        protected const string ALLOWED_VARS_DESCRIPTION_SEPARATOR = "allowedVarsDescriptionSeparator";

        protected void Page_Init(object sender, EventArgs e)
        {
            templateVars = new Dictionary<string, string>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            emails = EmailModel.GetEmailList();

            if (!IsPostBack)
            {
                PrepareEmailDropdown(ddlNotification, emails);
                ddlNotification.SelectedIndex = 0;
                emailContent = emails[0].content;
                PrepareEmailTemplateVars(emails[0].allowedVars);
            }
            else
            {
                BaseUI main = (BaseUI)Page.Master;
                main.ResetMessages(); //resetting messages to make sure they won't stay on every postback after successful template saving
            }
        }

        protected void ddlNotification_SelectedIndexChanged(object sender, EventArgs e)
        {
            EmailTypes selectedType = (EmailTypes)Convert.ToInt32(ddlNotification.SelectedValue);
            var emailInfo = EmailModel.GetEmailList().Where(res => res.type == selectedType).Select(res => new
            {
                content = res.content,
                allovedVars = res.allowedVars
            }).FirstOrDefault();

            emailContent = emailInfo.content;
            PrepareEmailTemplateVars(emailInfo.allovedVars);
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            emailContent = WebUtility.HtmlDecode(Request.Form["editor"]);
            EmailTypes selectedType = (EmailTypes)Convert.ToInt32(ddlNotification.SelectedValue);
            BaseUI main = (BaseUI)Page.Master;
            if (EmailModel.SaveEmailContent(emailContent, selectedType))
            {
                main.SetSuccessMessage(Resources.Success.NotificationEdit);
            }
            else
            {
                main.SetErrorMessage(Resources.Error.NotificationEdit);
            }
            string aVars = emails.Where(res => res.type == selectedType)
                                     .Select(res => res.allowedVars).FirstOrDefault();
            PrepareEmailTemplateVars(aVars);
        }

        #region HelperMethods

        protected void PrepareEmailDropdown(DropDownList ddl, EmailModel[] emails)
        {
            ddl.Items.Clear();
            foreach (EmailModel email in emails)
            {
                ListItem option = new ListItem(email.name, ((int)email.type).ToString());
                ddl.Items.Add(option);
            }
        }

        protected void PrepareEmailTemplateVars(string allowedVars)
        {
            string[] templateVarArray = allowedVars.Split(new string[] { ConfigurationManager.AppSettings[ALLOWED_VARS_SEPARATOR] }, StringSplitOptions.None);
            foreach (string variable in templateVarArray)
            {
                string[] definitionArray = variable.Split(new string[] { ConfigurationManager.AppSettings[ALLOWED_VARS_DESCRIPTION_SEPARATOR] }, StringSplitOptions.None);
                templateVars.Add(definitionArray[0], definitionArray[1]);
            }
        }

        #endregion
    }
}