﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="UserCheckList.aspx.cs" Inherits="HawkHome.Admin.UserCheckList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/bootstrap.paginator.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/user_check_list.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>User Subscriptions</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div>
                    If for some reason the subscription of users, who have autocharge feature enabled, wasn’t updated in 
                    <%= ConfigurationManager.AppSettings["licenseCheckPeriod"] %> days before the subscription expires, those users are added to this list
                </div>
                <div class="elements_margin">
                    <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>
                            <tr >
                                <th class="text_align_center custom-header-style">Subscription ID</th>
                                <th class="text_align_center custom-header-style">Email</th>
                                <th class="text_align_center custom-header-style">Name</th>
                                <th class="text_align_center custom-header-style">Registration Date</th>
                                <th class="text_align_center custom-header-style">Subscription End</th>
                                <th class="text_align_center custom-header-style">Tools</th>
                            </tr>
                        </thead>
                        <tbody data-bind=" foreach: {
                                                        data: licenseList,
                                                        afterAdd: showLicenseEntry
                                                    }">
                            <tr>
                                <td class="text_align_center" data-bind="text: subscriptionId"></td>
                                <td class="text_align_center" data-bind="text: email"></td>
                                <td class="text_align_center" data-bind="text: fullName"></td>
                                <td class="text_align_center" data-bind="text: creationDate"></td>
                                <td class="text_align_center" data-bind="text: subscrEndDate"></td>
                                <td class="text_align_center">
                                    <div>
                                        <a href="#" class="btn btn-sm btn-warning label_color_black full_width updateAccount" data-bind="click: $root.updateSubscriptionShowUI"><span class="glyphicon glyphicon-plus">&nbsp</span>Update</a>
                                    </div>
                                    <div class="mtop5">
                                        <a href="#" class="btn btn-sm btn-warning label_color_black full_width" data-bind="click: $root.notifyUserTransactionFail"><span class="glyphicon glyphicon-envelope">&nbsp</span>Transaction Failed</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tr class="pager">
                            <td colspan="6" class="pagination-footer">
                                <div id="bottomPagination" style="margin:0px;"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="updatePopup" class="hide popup_padding">
                    <div>
                        <div>Please enter the Transaction Id in the box below:</div>
                        <div id="transactionIdContainer" class="control-group elements_margin">
                            <div class="controls">
                                <input id="transactionId" type="text" class="form-control" data-bind="value: transactionId, valueUpdate: 'afterkeydown'" />
                            </div>
                        </div>
                    </div>
                    <div class="elements_margin">
                        <input type="button" id="yes" value="OK" class="btn btn-sm btn-warning label_color_black popup-btn-width" data-bind="disable: !$root.isUpdateAvailable()" />
                        <input type="button" id="no" value="Cancel" class="btn btn-sm label_color_black popup-btn-width" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
