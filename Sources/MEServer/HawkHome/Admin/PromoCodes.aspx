﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PromoCodes.aspx.cs" Inherits="HawkHome.Admin.PromoCodes" MasterPageFile="~/Common/BaseUI.Master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/Stylesheet/datepicker.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />

    <script type="text/javascript" src="<%= ResolveUrl("~/JS/moment.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/jquery.filter_input.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/jquery.nimble.loader.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/knockout.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/knockout.validation.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/bootstrap-datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/initializeDataPicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/loader.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/pager.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/Base/pagerHandler.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Admin/JS/promo_code_page_init.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/bootstrap-paginator.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/blockUI.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/pnotify.min.js")%>"></script>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container" id="PageMainContent">
        <div class="mtop20">
            <h4>License Codes</h4>
            <hr class="edit_account_hr" />

            <div>
                <div data-bind="click: showContent" class="span3 chevron">
                    <span class="glyphicon glyphicon-chevron-right" data-bind="visible: !show()">&nbsp;</span>
                    <span class="glyphicon glyphicon-chevron-down" data-bind="visible: show">&nbsp;</span>
                    Create new License Code  
                </div>
                <div class="mtop20 mbottom20" data-bind="visible: show">
                    <div class="row mbottom20">
                        <div class="col-sm-11 col-sm-offset-1">
                            <div class="radio">
                                <label class="radio cursor_pointer">
                                    <input type="radio" name="generateAutomatically" runat="server" id="generateAutomatically" data-bind="value: generateAutomatically, checked: selectedCodeGenType" />
                                    Generate a random code
                                </label>
                            </div>
                            <div class="radio">
                                <label class="radio cursor_pointer">
                                    <input type="radio" name="generateManually" runat="server" id="generateManually" data-bind="value: generateManually, checked: selectedCodeGenType" />
                                    Enter the code manually
                                </label>
                            </div>

                            <div class="form-horizontal" data-bind="visible: isGenerateAutomaticallyVisible, validationOptions: { insertMessages: false }">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Number of codes to be generated:</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox runat="server" name="codeCount" ID="codeCount" type="text" data-bind="value: codeCount,  valueUpdate: 'afterkeydown'" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="alert-danger inline_alert mtop3" data-bind="validationMessage: codeCount"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal" data-bind="visible: isGenerateManuallyVisible, validationOptions: { insertMessages: false }">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Enter the code to be generated:</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox runat="server" ID="code" name="code" type="text" CssClass="form-control" placeholder="Promo Code" data-bind="value: promoCode, valueUpdate: 'afterkeydown'"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="alert-danger inline_alert mtop3" style="width: auto;" data-bind="validationMessage: promoCode"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mtop20 mbottom20">
                        <div class="col-sm-2 col-sm-offset-1 pure-skin-paraben">
                            <table class="pure-table pure-table-horizontal full_width">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text_align_center label_color_black">
                                            <span>Term</span>
                                        </th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="    foreach: termsList">
                                    <tr class="text_align_center">
                                        <td>
                                            <input type="radio" name="term" data-bind="value: id, checked: $root.termCheckId">
                                        </td>
                                        <td>
                                            <span data-bind="text: label"></span>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Number of devices:</label>
                                    <div class="col-sm-2">
                                        <asp:TextBox runat="server" name="deviceCount" ID="deviceCount" type="text" data-bind="value: deviceCount" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-7">
                                        <asp:RequiredFieldValidator ID="DeviceCountValidator" runat="server" Display="Dynamic"
                                            ControlToValidate="deviceCount" ValidationGroup="PromoCodeinfo">
                                        <span class="alert-danger inline_alert mtop3">Set the value from 1 to 9999.</span>
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                                            ControlToValidate="deviceCount" ValidationGroup="PromoCodeinfo"
                                            ValidationExpression="^([1-9]|[1-9][0-9]|[1-9][0-9][0-9]||[1-9][0-9][0-9][0-9])$">
                                        <span class="alert-danger inline_alert mtop3">Set the value from 1 to 9999.</span>
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Number of activations:</label>
                                    <div class="col-sm-2">
                                        <asp:TextBox runat="server" name="activationCount" ID="activationCount" type="text" data-bind="value: activationCount" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-7">
                                        <asp:RequiredFieldValidator ID="ActivationCountValidator" runat="server" Display="Dynamic"
                                            ControlToValidate="activationCount" ValidationGroup="PromoCodeinfo">
                                        <span class="alert-danger inline_alert mtop3">Set the value from 1 to 9999.</span>
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                                            ControlToValidate="activationCount" ValidationGroup="PromoCodeinfo"
                                            ValidationExpression="^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$">
                                        <span class="alert-danger inline_alert mtop3">Set the value from 1 to 999</span>
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="validationOptions: { insertMessages: false }">
                                    <label class="col-sm-3 control-label">Code valid from:</label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtDateFrom" name="txtDateFrom" date-group="2" runat="server" CssClass="filter_date_from form-control" placeholder="Valid From"
                                            data-bind="value: dateFromFormat, valueUpdate: 'keyup'"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-7">
                                        <span class="alert alert-error inline_alert" style="width: auto;" data-bind="validationMessage: dateFromFormat"></span>
                                        <asp:RequiredFieldValidator ID="DateFromValidator" runat="server" Display="Dynamic"
                                            ControlToValidate="txtDateFrom" ValidationGroup="PromoCodeinfo">
                                        <span class="alert-danger inline_alert mtop3">Date is required.</span>
                                        </asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="ValidFrom" runat="server"
                                            ControlToValidate="txtDateFrom"
                                            ValidationGroup="PromoCodeinfo"
                                            Display="Dynamic"
                                            OnServerValidate="DateFormat_Validate">
                                         <span class="alert-danger inline_alert mtop3">The date format is not valid.</span>
                                        </asp:CustomValidator>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="validationOptions: { insertMessages: false }">
                                    <label class="col-sm-3 control-label">Code valid to:</label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtDateTo" runat="server" date-group="2" CssClass="filter_date_to form-control" placeholder="Valid To"
                                            data-bind="value: dateToFormat, valueUpdate: 'keyup'"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-7">
                                        <span class="alert alert-error inline_alert" style="width: auto;" data-bind="validationMessage: dateToFormat"></span>
                                        <asp:RequiredFieldValidator ID="DateToValidator" runat="server" Display="Dynamic"
                                            ControlToValidate="txtDateTo" ValidationGroup="PromoCodeinfo">
                                        <span class="alert-danger inline_alert mtop3">Date is required.</span>
                                        </asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="ValidTo" runat="server"
                                            Display="Dynamic"
                                            ControlToValidate="txtDateTo"
                                            ValidationGroup="PromoCodeinfo"
                                            OnServerValidate="DateFormat_Validate">
                                        <span class="alert-danger inline_alert mtop3">The date format is not valid.</span>
                                        </asp:CustomValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-3">
                                        <asp:Button ID="Generate" runat="server" Text="Generate" OnClick="btnGenerate_Click"
                                            CssClass="btn btn-sm btn-warning label_color_black"
                                            ValidationGroup="PromoCodeinfo" data-bind="click: validation" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div data-bind="click: showContent" class="span3 chevron">
                        <span class="glyphicon glyphicon-chevron-right" data-bind="visible: show">&nbsp;</span>
                        <span class="glyphicon glyphicon-chevron-down" data-bind="visible: !show()">&nbsp;</span>
                        License Codes History
                    </div>
                    <div data-bind="visible: show() != true">
                        <div id="filters">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="filter_field input-large">Code Number:</label>
                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" data-bind="value: promoCodeFilter().promoCode"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <label class="filter_field input-large" for="txtGenerateFrom">Generated from:</label>
                                    <asp:TextBox ID="txtGenerateFrom" date-group="1" name="txtGenerateFrom" runat="server" CssClass="filter_date_from form-control" data-bind="value: promoCodeFilter().generatedFrom"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <label class="filter_field input-large">Generated to:</label>
                                    <asp:TextBox ID="txtGeneratedTo" date-group="1" runat="server" CssClass="filter_date_to form-control" data-bind="value: promoCodeFilter().generatedTo"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <label class="filter_field input-large">Subscription Period:</label>
                                    <asp:DropDownList runat="server" ID="ddlSubscriptionPeriod" AutoPostBack="false" CssClass="form-control" data-bind="value: promoCodeFilter().subscriptionPeriod"></asp:DropDownList>
                                </div>
                                <div class="col-sm-2">
                                    <label class="filter_field input-large">Activation State:</label>
                                    <asp:DropDownList runat="server" ID="ddlActivationState" AutoPostBack="false" CssClass="form-control" data-bind="value: promoCodeFilter().state"></asp:DropDownList>
                                </div>
                                <div class="col-sm-2">
                                    <label class="filter_field input-large">Expiration State:</label>
                                    <asp:DropDownList runat="server" ID="ddlExpirationState" AutoPostBack="false" CssClass="form-control" data-bind="value: promoCodeFilter().expiration"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row mtop10">
                                <div class="col-sm-6">
                                    <asp:Button ID="ClearFilters" runat="server" Text="Clear Filters"
                                        CssClass="btn btn-sm btn-warning label_color_black" data-bind="click: clearFilters " />
                                    <asp:Button ID="FilterData" runat="server" Text="Filter Data"
                                        CssClass="btn btn-sm btn-warning label_color_black" data-bind="click: getPromoCodeHistoryList" />
                                </div>
                            </div>
                        </div>
                        <div class="mtop20">
                            <div class="form-inline">
                                <div class="page_counter">
                                    < Page <span data-bind="text: page"></span>&nbsp;of <span data-bind="    text: totalPages"></span>>
                                </div>
                                <label>Records per page:</label>
                                <asp:DropDownList runat="server" ID="recordsPerPage" AutoPostBack="false" CssClass="form-control" data-bind="value: recordsPerPage, event: {change:updatePromoCodeTable}"></asp:DropDownList>
                            </div>
                            <table class="table table-bordered table-striped table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th class="text_align_center custom-header-style">Date Generated</th>
                                        <th class="text_align_center custom-header-style">Code Number</th>
                                        <th class="text_align_center custom-header-style">Device Count</th>
                                        <th class="text_align_center custom-header-style">Subscription Period</th>
                                        <th class="text_align_center custom-header-style">Total Activations Available</th>
                                        <th class="text_align_center custom-header-style">Activations Used</th>
                                        <th class="text_align_center custom-header-style">Activations Left</th>
                                        <th class="text_align_center custom-header-style">Valid From</th>
                                        <th class="text_align_center custom-header-style">Valid To</th>
                                        <th class="text_align_center custom-header-style">State</th>
                                        <th class="text_align_center custom-header-style">Expired</th>
                                        <th class="text_align_center custom-header-style">Available Action</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: promoCodeList">
                                    <tr>
                                        <td class="text_align_center" data-bind="text: generatedDate"></td>
                                        <td class="text_align_center" data-bind="text: promoCode"></td>
                                        <td class="text_align_center" data-bind="text: deviceCount"></td>
                                        <td class="text_align_center" data-bind="text: subscriptionPeriod"></td>
                                        <td class="text_align_center" data-bind="text: totalActivationsAvailable"></td>
                                        <td class="text_align_center" data-bind="text: activationUsed"></td>
                                        <td class="text_align_center" data-bind="text: activationsLeft"></td>
                                        <td class="text_align_center" data-bind="text: validFrom"></td>
                                        <td class="text_align_center" data-bind="text: validTo"></td>
                                        <td class="text_align_center" data-bind="text: state"></td>
                                        <td class="text_align_center" data-bind="text: expired">&nbsp</td>
                                        <td>
                                            <a href="#" id="deletePromoCode" class="btn btn-sm btn-warning label_color_black checkout_button " data-bind="visible: toDelete, click: $parent.clickDeletePromoCode">Delete</a>
                                            <a href="#" class="btn btn-sm btn-warning label_color_black checkout_button" data-bind="visible: toDisable, click: $parent.clickDisablePromoCode">Disable</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="pagination-custom text-center">
                                <ul class="pagination" data-bind="visible: isPaginationVisible">
                                </ul>
                                <span data-bind="text: message" class="error"></span>
                            </div>
                            <div id="popupDeletePromoCode" class="hide popup_padding">
                                <div>
                                    The code will be deleted and the users will not be able to use it anymore. Do you want to continue?
                                </div>
                                <div class="elements_margin">
                                    <input type="button" value="Yes" class="btn btn-sm btn-warning label_color_black" data-bind="click: deletePromoCode" />
                                    <input type="button" value="No" class="btn btn-sm label_color_black no" />
                                </div>
                            </div>
                            <div id="popupDisablePromoCode" class="hide popup_padding">
                                <div>
                                    The users will not be able to activate this code anymore. Do you want to continue?
                                </div>
                                <div class="elements_margin">
                                    <input type="button" value="Yes" class="btn btn-sm btn-warning label_color_black" data-bind="click: disablePromoCode" />
                                    <input type="button" value="No" class="btn btn-sm label_color_black no" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
