﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="HawkHome.Admin.History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var page = '';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.filter_input.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/initializeDataPicker.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/user_page_init.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <div>
                <h4>Payments History</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div class="elements_margin">
                    <asp:Panel DefaultButton="btnFilter" runat="server">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Date from:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="filter_date_from form-control" date-group="1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">
                                    Date to:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="filter_date_to form-control" date-group="1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">
                                    <asp:Label ID="lblType" runat="server" Text="Email:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtUser" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">
                                    <asp:Label ID="lblTransactionId" runat="server" Text="Transaction ID:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtTransaction" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="mtop10">
                                <asp:Button ID="btnClear" runat="server" Text="Clear filters" OnClick="btnClear_Click"
                                    CssClass="btn btn-sm label_color_black" />
                                <asp:Button ID="btnFilter" runat="server" Text="Filter Data" OnClick="btnFilter_Click"
                                    CssClass="btn btn-sm btn-warning label_color_black" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="elements_margin">
                    <asp:GridView ID="gridPaymentHistory" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-condensed table-hover" Height="100%"
                        Width="100%" OnPageIndexChanging="gridPaymentHistory_PageIndexChanging"
                        PageSize="50" ShowHeaderWhenEmpty="true">
                        <PagerSettings Mode="NumericFirstLast" />
                        <Columns>
                            <asp:BoundField DataField="paymentDate" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}" HeaderText="Payment Date" SortExpression="paymentDate" ReadOnly="true">
                                <HeaderStyle Width="160px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" ReadOnly="true" />
                            <asp:BoundField DataField="firstName" HeaderText="First Name" SortExpression="firstName" ReadOnly="true" />
                            <asp:BoundField DataField="lastName" HeaderText="Last Name" SortExpression="lastName" ReadOnly="true" />
                            <asp:BoundField DataField="label" HeaderText="Used plan" SortExpression="label" ReadOnly="true">
                                <HeaderStyle Width="160px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="deviceCount" HeaderText="Device Count" SortExpression="deviceCount" ReadOnly="true" />
                            <asp:BoundField DataField="price" DataFormatString="${0:F2}" HeaderText="Price per device" SortExpression="price" ReadOnly="true" />
                            <asp:BoundField DataField="amount" DataFormatString="${0:F2}" HeaderText="Amount paid" SortExpression="amount" ReadOnly="true" />
                            <asp:BoundField DataField="transactionId" HeaderText="Transaction ID" SortExpression="transactionId" ReadOnly="true" />
                            <asp:BoundField DataField="originalSubscrEnd" DataFormatString="{0:d}" HeaderText="Orig. Subscription End" SortExpression="originalSubscrEnd" ReadOnly="true" />
                            <asp:BoundField DataField="afterPaymentSubscrEnd" DataFormatString="{0:d}" HeaderText="New Subscription End" SortExpression="afterPaymentSubscrEnd" ReadOnly="true" />
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" CssClass="pager" />
                        <SelectedRowStyle CssClass="table_row_selected" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="Black" />
                        <EmptyDataTemplate>
                            No Data Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
