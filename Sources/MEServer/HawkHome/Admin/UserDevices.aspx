﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="UserDevices.aspx.cs" Inherits="HawkHome.Admin.UserDevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pager.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/pagerHandler.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/spin.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/bootstrap.paginator.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/moment.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.filter_input.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/user_devices.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script src="../JS/Base/FormatUtils.js"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/JS/blockUI.js") %>"></script>
    <script src="../JS/knockout.validation.js"></script>
    <script src="../JS/web.config.js"></script>
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <asp:HiddenField ID="HiddenUserId" runat="server" />
            <div>
                <h4>Devices</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div class="elements_margin">
                </div>
                <div id="filter" class="row mbottom10">

                    <div class="col-sm-2">
                        <label class="control-label">Creation Date from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="3" data-bind="value: filter.creationDateFrom" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Creation Date to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="3" data-bind="value: filter.creationDateTo" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">License Activated from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="2" data-bind="value: filter.licActDateFrom" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">License Activated to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="2" data-bind="value: filter.licActDateTo" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Last Updated from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="1" data-bind="value: filter.lastUpdatedFrom" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Last Updated to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="1" data-bind="value: filter.lastUpdatedTo" />
                        </div>
                    </div>

                    <div class="col-sm-2 ">
                        <label class="control-label">Device ID</label>
                        <div class="controls">
                            <input class="form-control" data-bind="value: filter.deviceId" />
                        </div>
                    </div>
                    <div class="col-sm-2 ">
                        <label class="control-label">License ID</label>
                        <div class="controls" data-bind="validationElement: filter.licId, value: filter.licId">
                             <input class="form-control" data-bind="value: filter.licId " />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">Agent Was Activated:</label>
                        <div class="controls">
                            <select class="form-control" data-bind="value: filter.agentWasActivated">
                                <option value="-1">Select an option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">&nbsp;</label>
                        <div class="controls">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked="checked" data-bind="checked: filter.showUnassignedAgents">Show Unassigned Agents</label>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="mbottom10">
                <button type="button" class="btn btn-sm label_color_black" data-bind="click: resetFilter">Clear filters</button>
                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: loadDevicesList">Filter Data</button>
            </div>
            <div class="elements_margin">

                <table class="table table-bordered table-striped table-condensed table-hover long_word_fix">
                    <thead>
                        <tr>
                            <th class="text_align_center custom-header-style adjust_header_column">Device Name</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Creation Date</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Operation System</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Agent Was Activated</th>
                            <th class="text_align_center custom-header-style adjust_header_column">License Activation Date</th>
                            <th class="text_align_center custom-header-style adjust_header_column">License ID</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Last Updated</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Device ID</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Phone Number</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Brand</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Model</th>
                            <th class="text_align_center custom-header-style adjust_header_column">OS</th>
                            <th class="text_align_center custom-header-style adjust_header_column">SDK</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Firmware ID</th>
                            <th class="text_align_center custom-header-style adjust_header_column">SVN Version</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Time Zone</th>
                            <th class="text_align_center custom-header-style adjust_header_column">Note</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: {
                                        data: devicesList,
                                        afterAdd: showEntry
                                    }, visible: containsDevices">
                        <tr>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: deviceName"></span>
                            </td>
                             <td class="text_align_center valign_middle">
                                <span data-bind="text: creationDate"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span class="badge" data-bind="text: operSysType"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span class="badge alert-success" data-bind="visible: agentWasActivated">Yes</span>
                                <span class="badge alert-danger" data-bind="visible: !agentWasActivated">No</span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: licActDate"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: licId"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: lastUpdate"></span>
                            </td>

                            <td class="text_align_center valign_middle">
                                <span data-bind="text: deviceId"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: phoneNumber"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: brand"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: model"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: os"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: sdk"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: firmwareId"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: svnVersion"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: timeZone"></span>
                            </td>

                            <td class="text_align_center valign_middle">
                                <span data-bind="text: note"></span>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: isPaginationVisible">
                        <tr>
                            <td colspan="17" class="pagination-custom text-center">
                                <ul class="pagination pagination-lg">
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: !containsDevices()">
                        <tr>
                            <td colspan="17">No Data Found.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
