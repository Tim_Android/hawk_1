/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.htmlEncodeOutput = true;
    config.autoGrow_onStartup = true;
    
    config.toolbar = [    
    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
    { name: 'align', groups: ['align'], items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
    { name: 'list', groups: ['list'], items: ['NumberedList', 'BulletedList'] },
    { name: 'indent', groups: ['indent'], items: ['Outdent', 'Indent'] },
    { name: 'links', items: ['Link', 'Unlink'] },
    { name: 'insert', items: ['Table', 'Image', 'HorizontalRule', 'SpecialChar'] },
    '/',
    { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
    { name: 'colors', items: ['TextColor', 'BGColor'] },
    { name: 'editing', groups: ['find', 'selection'], items: ['Find', 'Replace', 'SelectAll', '-', 'Scayt'] },
    { name: 'tools', items: ['Maximize', 'ShowBlocks'] },
    '/',
    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
    { name: 'document', groups: ['mode'], items: ['Source'] },
    ];
};
