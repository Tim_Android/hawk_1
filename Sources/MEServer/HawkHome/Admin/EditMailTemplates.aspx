﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="EditMailTemplates.aspx.cs" Inherits="HawkHome.Admin.EditMailTemplates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/bootstrap.wells.css") %>" />
    <script src="<%=ResolveUrl("~/Admin/CKEditor/ckeditor.js")%>"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('editor');
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>Edit Notification Template</h4>
                <hr class="edit_account_hr" />
            </div>
            <div class="row form-horizontal">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="ddlNotification" class="col-sm-5 control-label">Please select a template you want to edit:</label>
                        <div class="col-sm-7">
                            <asp:DropDownList runat="server" ID="ddlNotification" AutoPostBack="true" OnSelectedIndexChanged="ddlNotification_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-sm-offset-1">
                    <div class=" float_inline_height_fix">
                        <asp:Button ID="btnSaveChanges" runat="server" CssClass="btn btn-sm btn-warning label_color_black" OnClick="btnSaveChanges_Click" Text="Save Template" />
                    </div>
                </div>
            </div>
            <div class="row elements_margin">
                <div class="col-sm-7 mail_template_edit_height">
                    <div>
                        <textarea name="editor"><%= this.emailContent %></textarea>
                    </div>
                </div>
                <div class="col-sm-4  col-sm-offset-1 mail_template_edit_height">
                    <div class="well mail_template_edit_height">
                        List of available variables for template:
                        <dl>
                            <%foreach (KeyValuePair<string, string> pair in templateVars)
                              { %>
                            <dt><%= pair.Key %></dt>
                            <dd><%= pair.Value %></dd>
                            <%} %>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
