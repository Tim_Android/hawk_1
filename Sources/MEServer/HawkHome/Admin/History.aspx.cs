﻿using Domain.Entities.Filters;
using HawkDataAccess;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.Admin
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PaymentHistoryFilter filter = new PaymentHistoryFilter();

            if (!IsPostBack)
            {
                if (!string.IsNullOrWhiteSpace(Request["email"]))
                {//if we received email parameter we get all time payments history for this user
                    filter.email = Request["email"];
                    filter.dateFrom = null;
                    filter.dateTo = null;
                }
            }
            else
            {
                filter = GetFilter();
            }

            InitFilterUI(filter);
            BindData(filter);
        }

        protected void gridPaymentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridPaymentHistory.PageIndex = e.NewPageIndex;
            BindData(GetFilter());
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            //resetting filter values to default ones
            PaymentHistoryFilter filter = new PaymentHistoryFilter();
            InitFilterUI(filter);
            BindData(filter);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindData(GetFilter());
        }

        protected void BindData(PaymentHistoryFilter filter)
        {
            gridPaymentHistory.DataSource = PaymentHistoryItemModel.GetPaymentHistory(null, null, filter);
            gridPaymentHistory.DataBind();
        }


        /// <summary>
        /// Method for applying filter values to controls
        /// </summary>
        protected void InitFilterUI(PaymentHistoryFilter filter)
        {
            string dateFormat = ConfigurationManager.AppSettings["defaultDateFormat"];

            txtDateFrom.Text = filter.dateFrom.HasValue ? filter.dateFrom.Value.ToString(dateFormat) : string.Empty;
            txtDateTo.Text = filter.dateTo.HasValue ? filter.dateTo.Value.ToString(dateFormat) : string.Empty;
            txtUser.Text = (filter.email == null) ? String.Empty : filter.email;
            txtTransaction.Text = (filter.transactionId == null) ? String.Empty : filter.transactionId;
        }

        protected PaymentHistoryFilter GetFilter()
        {
            PaymentHistoryFilter filter = new PaymentHistoryFilter();
            filter.dateFrom =  string.IsNullOrWhiteSpace(txtDateFrom.Text) ? (DateTime?)null : DateTime.Parse(txtDateFrom.Text);
            filter.dateTo = string.IsNullOrWhiteSpace(txtDateTo.Text) ? (DateTime?)null : DateTime.Parse(txtDateTo.Text).AddHours(23).AddMinutes(59).AddSeconds(59);
            filter.email = string.IsNullOrWhiteSpace(txtUser.Text) ? null : txtUser.Text;
            filter.transactionId = string.IsNullOrWhiteSpace(txtTransaction.Text) ? null : txtTransaction.Text;

            return filter;
        }
    }
}