﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="UserSubscriptions.aspx.cs" Inherits="HawkHome.Admin.UserSubscriptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pager.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/pagerHandler.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/spin.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/bootstrap.paginator.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/moment.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.filter_input.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/user_subscriptions.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/knockout.binders.js")%>"></script>
    <script src="../JS/web.config.js"></script>
    <script src="../JS/Base/FormatUtils.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/blockUI.js")%>"></script>
    <script src="../JS/knockout.validation.js"></script>
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">
            <asp:HiddenField ID="HiddenUserId" runat="server" />
            <div>
                <h4>Subscriptions</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div class="elements_margin">
                </div>
                <div id="filter" class="row mbottom10">

                    <div class="col-sm-3">
                        <label class="control-label">Creation Date from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="3" data-bind="value: filter.creationDateFrom" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Creation Date to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="3" data-bind="value: filter.creationDateTo" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Subscription End from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="2" data-bind="value: filter.subscriptionEndFrom" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Subscription End to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="2" data-bind="value: filter.subscriptionEndTo" />
                        </div>
                    </div>
                    <div class="col-sm-3 ">
                        <label class="control-label">Devices Count:</label>
                        <div class="controls" data-bind="validationElement: filter.deviceCnt, popover: filter.deviceCnt">
                            <input class="form-control" data-bind="value: filter.deviceCnt " />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Payment System</label>
                        <div class="controls">
                            <select class="form-control" data-bind="value: filter.paySysType">
                                <option value="-1">Select an option</option>
                                <option value="1">AuthorizeNet</option>
                                <option value="2">PayPal</option>
                                <option value="3">Promocode</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Subscription ID</label>
                        <div class="controls">
                            <input class="form-control" data-bind="value: filter.subscriptionId" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="mbottom10">
                <button type="button" class="btn btn-sm label_color_black" data-bind="click: resetFilter">Clear filters</button>
                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: loadLicenseList">Filter Data</button>
            </div>
            <div class="elements_margin">

                <table class="table table-bordered table-striped table-condensed table-hover long_word_fix">
                    <thead>
                        <tr>
                            <th class="text_align_center custom-header-style">Date End</th>
                            <th class="text_align_center custom-header-style">Period</th>
                            <th class="text_align_center custom-header-style">Devices Count</th>
                            <th class="text_align_center custom-header-style">Pay System</th>
                            <th class="text_align_center custom-header-style">Subscriptions ID</th>
                            <th class="text_align_center custom-header-style">Creation Date</th>
                            <th class="text_align_center custom-header-style">Tools</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: {
    data: licenseList,
    afterAdd: showEntry
}, visible: containsLicensies">
                        <tr>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: subscrEndDate"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: periodName"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: devices"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: paySys"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: subscriptionId"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <span data-bind="text: creationDate"></span>
                            </td>
                            <td class="text_align_center valign_middle">
                                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: $root.addCurrentLicense" data-toggle="modal" data-target="#editSubscription""><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: isPaginationVisible">
                        <tr>
                            <td colspan="6" class="pagination-custom text-center">
                                <ul class="pagination pagination-lg">
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: !containsLicensies()">
                        <tr>
                            <td colspan="7">No Data Found.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--Modals-->
        <div class="modal fade" id="editSubscription" tabindex="-1" role="dialog" aria-labelledby="editSubscription" data-bind="with: currentLicense">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Subscription</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Device Count:</label>
                                <div class="col-sm-5" data-bind="validationElement: devices, popover: devices">
                                    <input type="text" class="form-control" data-bind="value: devices">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Subscription End Date:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-bind="dateTimePicker: subscrEndDate, datepickerOptions: { format: DATETIME_FORMAT }">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning" data-bind="click: $root.updateLicense">Update Subscription</button>
                        <button type="button" class="btn-link btn btn-sm blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
