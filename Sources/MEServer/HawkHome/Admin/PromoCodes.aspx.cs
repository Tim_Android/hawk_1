﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using Domain.Entities.Statics;
namespace HawkHome.Admin
{
    public partial class PromoCodes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                BaseUI main = (BaseUI)Page.Master;
                main.ResetMessages();
            }

            FillSubscriptionPeriodDdl();
            FillActivationStateDdl();
            FillExpirationStateDdl();
            FillRecoredsPerPageDdl();
        }


        protected void DateFormat_Validate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = Validator.ValidateDateFormat(e.Value);
        }

        #region Buttons

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                int? error = default(int?);
                var termVaule = Request.Form["term"];
                BaseUI main = (BaseUI)Page.Master;
                int term;

                if (termVaule == null || !int.TryParse(termVaule, out term))
                {
                    // first argument - any value to get a dafault error
                    SetErrorMessage(1, main);
                }
                else
                {
                    if (generateAutomatically.Checked)
                    {
                        error = PromoCodeModel.GenerateAutomaticallyPromoCode(term, Convert.ToInt32(deviceCount.Text), Convert.ToInt32(activationCount.Text), Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), Convert.ToInt32(codeCount.Text));
                    }
                    if (generateManually.Checked)
                    {
                        error = PromoCodeModel.GenerateManuallyPromoCode(term, Convert.ToInt32(deviceCount.Text), Convert.ToInt32(activationCount.Text), Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), code.Text);
                    }
                }

                if (!error.HasValue)
                {
                    main.SetSuccessMessage(Resources.Success.PromoCodeGeneration);
                }
                else
                {
                    SetErrorMessage(error.Value, main);
                }
            }
        }


        #endregion

        #region PrivateMethods

        private void FillRecoredsPerPageDdl()
        {
            recordsPerPage.Items.Clear();

            for (int i = 20; i <= 50; i += 10)
            {
                recordsPerPage.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        private void SetErrorMessage(int error, BaseUI main)
        {
            switch (error)
            {
                case (int)ErrorType.PromoCodeEnteredManuallyAlreadyExists:
                    main.SetErrorMessage(Resources.Error.PromoCodeAlreadyExists);
                    break;
                default:
                    main.SetErrorMessage(Resources.Error.Unknown);
                    break;
            }
        }

        private void FillSubscriptionPeriodDdl()
        {
            ddlSubscriptionPeriod.Items.Clear();
            ddlSubscriptionPeriod.Items.Add(new ListItem(Resources.General.labelSelectValue, null));

            PriceListItemModel[] priceList = PriceListItemModel.GetPriceList(PriceListItemType.Promocode, DateTime.UtcNow);

            foreach (var obj in priceList)
            {
                ListItem option = new ListItem(obj.label, obj.id.ToString());
                ddlSubscriptionPeriod.Items.Add(option);
            }
        }

        private void FillActivationStateDdl()
        {
            ddlActivationState.Items.Clear();

            ddlActivationState.Items.Add(new ListItem(Resources.General.labelSelectValue, null));
            ddlActivationState.Items.Add(new ListItem(HawkCommonResources.Common.labelFullyActivated, ((int)ActivationStateType.FullyActivated).ToString()));
            ddlActivationState.Items.Add(new ListItem(HawkCommonResources.Common.labelPartlyActivatedOrDisabled, ((int)ActivationStateType.PartlyActivated).ToString()));
            ddlActivationState.Items.Add(new ListItem(HawkCommonResources.Common.labelNotActivated, ((int)ActivationStateType.NotActivated).ToString()));
        }

        private void FillExpirationStateDdl()
        {
            ddlExpirationState.Items.Clear();

            ddlExpirationState.Items.Add(new ListItem(Resources.General.labelSelectValue, null));
            ddlExpirationState.Items.Add(new ListItem(HawkCommonResources.Common.labelExpired, ((int)ExpirationStateType.Expired).ToString()));
            ddlExpirationState.Items.Add(new ListItem(HawkCommonResources.Common.labelNotExpired, ((int)ExpirationStateType.NotExpired).ToString()));
        }

        #endregion
    }
}