﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="EditPrices.aspx.cs" Inherits="HawkHome.Admin.EditPrices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/yui.tables.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.nimble.loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/edit_prices.js")%>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container pure-skin-paraben">
        <div class="mtop20">
            <div>
                <h4>Edit Prices</h4>
                <hr class="edit_account_hr" />
            </div>
            <div id="prices_area">
                <div class="prices_width elements_margin">
                    <table class="pure-table pure-table-horizontal full_width form-inline">
                        <thead>
                            <tr>
                                <th class="text_align_center">Price List Name</th>
                                <th class="text_align_center">Price per device</th>
                                <th class="text_align_center">Retail Price</th>
                                <th class="text_align_center">Last changed</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: priceList">
                            <tr data-bind="visible: type == 2 || type == 8 ">
                                <td class="text_align_center"><span class="label_color_black" data-bind="text: label"></span></td>
                                <td class="text_align_center">$<input class="input-sm zero_margin_bottom form-control" type="text" data-bind="value: price" /></td>
                                <td class="text_align_center">$<input class="input-sm zero_margin_bottom form-control" type="text" data-bind="value: retailPrice" /></td>
                                <td class="text_align_center"><span class="label_color_black" data-bind="text: startedOn"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="elements_margin">
                    <button type="button" class="btn  background_color_gray label_color_black" data-bind="click: restoreOriginal">Reset</button>
                    <button type="button" class="btn btn-success label_color_black" data-bind="click: savePrices"><i class="glyphicon glyphicon-floppy-disk"></i>Save</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
