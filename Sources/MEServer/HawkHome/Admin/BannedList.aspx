﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/BaseUI.Master" AutoEventWireup="true" CodeBehind="BannedList.aspx.cs" Inherits="HawkHome.Admin.BannedList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pager.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/pagerHandler.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/spin.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/loader.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/bootstrap.paginator.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/moment.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/jquery.filter_input.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.validation.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/knockout.editables.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Admin/JS/banned_list.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/pnotify.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Base/FormatUtils.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/web.config.js")%>"></script>


    <link rel="stylesheet" href="<%=ResolveUrl("~/Stylesheet/pnotify.min.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainWindow" runat="server">
    <div class="container">
        <div class="mtop20">

            <div>
                <h4>Banned List</h4>
                <hr class="edit_account_hr" />
            </div>
            <div>
                <div class="elements_margin">
                </div>
                <div id="filter" class="row mbottom10">

                    <div class="col-sm-4">
                        <label class="control-label">Ban Date from:</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="3" data-bind="value: filter.banDateFrom" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Ban Date to:</label>
                        <div class="controls">
                            <input class="filter_date_to form-control" date-group="3" data-bind="value: filter.banDateTo" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Email</label>
                        <div class="controls">
                            <input class="filter_date_from form-control" date-group="2" data-bind="value: filter.email" />
                        </div>
                    </div>


                </div>
            </div>
            <div class="mbottom10">
                <button type="button" class="btn btn-sm label_color_black" data-bind="click: resetFilter">Clear filters</button>
                <button type="button" class="btn btn-sm btn-warning label_color_black" data-bind="click: loadBannedList">Filter Data</button>
            </div>
            <div class="mbottom10">
                <button type="button" class="btn btn-sm btn-success" data-bind="click: $root.beginEdtNewEmail" data-toggle="modal" data-target="#addEmail">Add Email</button>
            </div>
            <div class="elements_margin">

                <table class="table table-bordered table-condensed table-striped table-hover long_word_fix">
                    <thead>
                        <tr>
                            <th class="text_align_center custom-header-style">Email</th>
                            <th class="text_align_center custom-header-style">Ban Date</th>
                            <th class="text_align_center custom-header-style">Note</th>
                            <th class="text_align_center custom-header-style">Tools</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: { data: bannedList, afterAdd: showEntry }, visible: containsBannedEmails">
                        <tr>
                            <td class="text-center">
                                <span data-bind="text: email"></span>
                            </td>
                            <td class="text-center">
                                <span data-bind="text: banDate"></span>
                            </td>
                            <td class="text-center">

                                <span data-bind="text: note, visible: !isBeingEdited()"></span>
                                <textarea class="form-control resize_none" data-bind="value: note, visible: isBeingEdited"></textarea>

                                <div data-bind="visible: isBeingEdited">
                                    <a href="#" class="btn btn-sm btn-success" data-bind="click: $root.clickUpdateBannedEmail">Save</a>
                                    <a href="#" class="btn btn-sm btn-warning" data-bind="click: $root.cancelNoteChanges">Cancel</a>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="mtop5">
                                    <div class="btn-group">
                                        <button class="btn btn-warning dropdown-toggle label_color_black" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-bind="click: $root.clickDeleteFromBannedList"><span class="glyphicon glyphicon-ok-circle"></span>Unban</a></li>
                                            <li><a href="#" data-bind="click: $root.beginEdit"><span class="glyphicon glyphicon-pencil"></span>Edit</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                    <tbody data-bind="visible: isPaginationVisible">
                        <tr>
                            <td colspan="4" class="pagination-custom text-center">
                                <ul class="pagination pagination-lg">
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: !containsBannedEmails()">
                        <tr>
                            <td colspan="4">No Data Found.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!--Modals-->
        <div class="modal fade" id="addEmail" tabindex="-1" role="dialog" aria-labelledby="addEmail" data-bind="with: newBannedEmail">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Email</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" data-bind="value: email">
                                    <p data-bind="validationMessage: email"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Note:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" data-bind="value: note">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-warning" data-bind="click: $root.addEmail">Add Email</button>
                        <button type="button" class="btn-link btn btn-sm blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
