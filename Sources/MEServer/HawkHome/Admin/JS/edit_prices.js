﻿/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="../../JS/loader.js" />
/// <reference path="/JS/intellisense.knockout.js" />

$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.loader.InitLoader();
    viewModel.initPrices();
})


function PriceListItem(priceListId, label, price, priceOther, retailPrice, startedOn, type) {
    var self = this;

    self.priceListId = priceListId;
    self.label = label;
    self.price = ko.observable(price).extend({ numeric: 2 }).extend({ positive: true });
    self.priceOther = ko.observable(priceOther).extend({ numeric: 2 }).extend({ positive: true });
    self.retailPrice = ko.observable(retailPrice).extend({ numeric: 2 }).extend({ positive: true });
    self.startedOn = startedOn;
    self.type = type;
}

function ViewModel() {
    var self = this;

    self.originalPriceList = [];
    self.priceList = ko.observableArray([]);
    self.loader = new Loader();

    self.initPrices = function () {
        self.loader.ShowLoading("#prices_area");
        self.priceList.removeAll();
        self.originalPriceList = [];
        $.getJSON("/Admin/Ajax/getPrices.ashx", null, function (list) {
            self.loader.HideLoading("#prices_area");
            $.map(list, function (item) {
                self.originalPriceList.push({ id: item.id, label: item.label, price: item.price, priceOther: item.priceOther, retailPrice: item.retailPrice, startedOn: item.startedOn, type: item.type });
                self.priceList.push(new PriceListItem(item.id, item.label, item.price, item.priceOther, item.retailPrice, item.startedOn, item.type));
            });
        });
    };

    self.restoreOriginal = function () {
        self.priceList.removeAll();
        $.map(self.originalPriceList, function (item) {
            self.priceList.push(new PriceListItem(item.id, item.label, item.price, item.priceOther, item.retailPrice, item.startedOn, item.type));
        });
    }

    self.isChanged = function () {
        for (var i = 0, count = self.priceList().length; i < count; i++) {
            if (self.priceList()[i].price() != self.originalPriceList[i].price ||
                self.priceList()[i].priceOther() != self.originalPriceList[i].priceOther ||
                self.priceList()[i].retailPrice() != self.originalPriceList[i].retailPrice) {
                return true;
            }
        }
        return false;
    }

    self.changedPriceListItems = function () {
        var result = [];
        for (var i = 0, count = self.priceList().length; i < count; i++) {
            if (self.priceList()[i].price() != self.originalPriceList[i].price ||
                self.priceList()[i].priceOther() != self.originalPriceList[i].priceOther ||
                self.priceList()[i].retailPrice() != self.originalPriceList[i].retailPrice) {
                result.push({
                    id: self.priceList()[i].priceListId,
                    type: self.priceList()[i].type,
                    price: self.priceList()[i].price(),
                    priceOther: self.priceList()[i].priceOther(),
                    retailPrice: self.priceList()[i].retailPrice()
                });
            }
        }
        return result;
    }

    self.savePrices = function () {
        if (self.isChanged() == true) {
            self.loader.ShowLoading("#prices_area");
            $.getJSON(
                "/Admin/Ajax/savePrices.ashx", {
                    data: JSON.stringify(self.changedPriceListItems())
                },
                function (result) {
                    self.loader.HideLoading("#prices_area");
                    switch (result.code) {
                        case 0:
                            notificationHandler.showErrorMessage(result.message, 'Error');
                            break;
                        case 1:
                            self.updateOriginalPriceList();
                            notificationHandler.showSuccessMessage(result.message);
                            break;
                        case 2:
                            self.updateOriginalPriceList();
                            notificationHandler.showWarningMessage(result.message);
                            break;
                        default:
                            notificationHandler.showWarningMessage("Unknown result");
                            break;
                    }
                }
                );
        }
        else {
            notificationHandler.showWarningMessage("No changes detected");
        }
    }

    self.updateOriginalPriceList = function () {
        self.originalPriceList = [];
        $.map(self.priceList(), function (item) {
            self.originalPriceList.push({ id: item.priceListId, label: item.label, price: item.price(), priceOther: item.priceOther(), retailPrice: item.retailPrice(), startedOn: item.startedOn, type: item.type });
        });
    }
}

ko.extenders.numeric = function (target, precision) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: target,  //always return the original observables value
        write: function (newValue) {
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? 0 : parseFloat(+newValue),
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });

    //initialize with current value to make sure it is rounded appropriately
    result(target());

    //return the new computed observable
    return result;
};

ko.extenders.positive = function (target) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed(
        {
            read: target,  //always return the original observables value
            write: function (newValue) {
                var current = target();
                var valueToWrite;

                if (newValue < 0) {
                    valueToWrite = newValue * (-1);
                }
                else {
                    valueToWrite = newValue;
                }

                //only write if it changed
                if (valueToWrite !== current) {
                    target(valueToWrite);
                } else {
                    //if the rounded value is the same, but a different value was written, force a notification for the current field
                    if (newValue !== current) {
                        target.notifySubscribers(valueToWrite);
                    }
                }
            }
        }
  );

    result(target());

    //return the new computed observable
    return result;
};