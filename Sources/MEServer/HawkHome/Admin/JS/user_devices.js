﻿//                 User Devices
$(document).ready(function () {
    var viewModel = new DeviceViewModel();
    viewModel.loadDevicesList();
    ko.validation.init({
        insertMessages: false,
        errorElementClass: 'has-error'
    });

    ko.bindingHandlers.popover = {
        update: function (element, valueAccessor, allBindingsAccessor) {
            var value = valueAccessor(),
                valueUnwrap = ko.unwrap(value);
            if ($(element).hasClass('has-error')) {
                $(element).children('textarea, input').popover({
                    content: $(element).attr('title'),
                    placement: "top",
                    trigger: "hover"
                });
            } else {
                $(element).children('textarea, input').popover("destroy");
            }
        }
    };
    ko.applyBindings(viewModel);

    // datepicker init
    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, true); //from js file
                }
            });
        });
    }




});


//   class DeviceViewModel

function DeviceViewModel() {
    var self = this;

    self.filter = new Filter({
        lastUpdatedFrom: '',
        lastUpdatedTo: '',
        creationDateFrom: '',
        creationDateTo: '',
        licActDateFrom: '',
        licActDateTo: '',
        licId: null,
        agentWasActivated: '',
        deviceId: null,
        showUnassignedAgents: true,
        userId: $('#MainWindow_HiddenUserId').val()
    });

    self.devicesList = ko.observableArray([]);

    self.isPaginationVisible = ko.observable(false);

    self.containsDevices = ko.computed(function () {
        return (self.devicesList().length > 0);
    });

    self.loadDevicesList = function () {
        var validator = ko.validatedObservable(self.filter);

        if (validator.isValid()) {
            ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/getDevicesList", {
                pager: pagerHandler.getPageState(),
                filter: self.filter.getFilter(),
                onSuccess: self.setDataGrid
            });
        }
        else {
            validator.errors.showAllMessages();
        }

    }

    pagerHandler.setDataloadCallBack(self.loadDevicesList);

    self.setDataGrid = function (data) {
        var response = $.parseJSON(data.d);

        self.devicesList.removeAll();
        $.each(response.devicesList, function (index, value) {
            self.devicesList.push(new Device(value));
        });
        pagerHandler.updatePager(response.pager);
        if (response.pager.totalPages > 1) {
            self.isPaginationVisible(true);
        }
        else {
            self.isPaginationVisible(false);
        }
    };

    self.resetFilter = function () {
        self.filter.resetFilter();
        self.loadDevicesList();
    };

    self.showEntry = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }


}


// class Filter

function Filter(data) {
    var self = this;

    self.id = ko.observable(data.id);
    self.userId = ko.observable(data.userId);
    self.agentWasActivated = ko.observable(data.agentWasActivated);
    self.lastUpdatedFrom = ko.observable(data.lastUpdatedFrom);
    self.lastUpdatedTo = ko.observable(data.lastUpdatedTo);
    self.device = ko.observable(data.device);
    self.deviceId = ko.observable(data.deviceId);
    self.licId = ko.observable(data.licId).extend({ digit: true });
    self.phoneNumber = ko.observable(data.phoneNumber);
    self.brand = ko.observable(data.brand);
    self.model = ko.observable(data.model);
    self.os = ko.observable(data.os);
    self.sdk = ko.observable(data.sdk);
    self.firmwareId = ko.observable(data.firmwareId);
    self.agent = ko.observable(data.agent);
    self.svn = ko.observable(data.svn);
    self.timeZone = ko.observable(data.timeZone);
    self.note = ko.observable(data.note);
    self.svn = ko.observable(data.svn);
    self.licActDateFrom = ko.observable(data.licActDateFrom);
    self.licActDateTo = ko.observable(data.licActDateTo);
    self.operSysType = ko.observable(data.operSysType);
    self.creationDateFrom = ko.observable(data.creationDateFrom);
    self.creationDateTo = ko.observable(data.creationDateTo);
    self.showUnassignedAgents = ko.observable(data.showUnassignedAgents);


    self.getFilter = function () {
        return new Filter({
            lastUpdatedFrom: self.lastUpdatedFrom(),
            lastUpdatedTo: self.lastUpdatedTo(),
            creationDateFrom: self.creationDateFrom(),
            creationDateTo: self.creationDateTo(),
            licActDateFrom: self.licActDateFrom(),
            licActDateTo: self.licActDateTo(),
            licId: self.licId(),
            agentWasActivated: self.agentWasActivatedToBool(),
            deviceId: self.deviceId(),
            showUnassignedAgents: self.showUnassignedAgents(),
            userId: self.userId(),
        });
    }


    self.resetFilter = function () {
        self.lastUpdatedFrom('');
        self.lastUpdatedTo('');
        self.creationDateFrom('');
        self.creationDateTo('');
        self.licActDateFrom('');
        self.licActDateTo('');
        self.licId(null);
        self.agentWasActivated(-1);
        self.deviceId(null);
        self.showUnassignedAgents(true);

        self.resetUiDates();
    }

    self.agentWasActivatedToBool = function () {
        var agentWasActivated = null;
        switch (self.agentWasActivated()) {
            case '0':
                agentWasActivated = false;
                break;
            case '1':
                agentWasActivated = true;
                break;
            default: break;
        }
        return agentWasActivated;
    };

    self.resetUiDates = function () {
        //update UI state
        if ($(".filter_date_from").length && $(".filter_date_to").length) {
            $(".filter_date_from").each(function (index, value) {
                var dateFrom = $(this);
                var groupId = $(this).attr("date-group");

                $(".filter_date_to").each(function (index, value) {
                    var dateTo = $(this);
                    if (dateTo.attr("date-group") == groupId) {
                        syncDateState(dateFrom, dateTo); //from js file
                    }
                });
            });
        }
    }
}


// class Device

function Device(data) {
    var self = this;

    self.id = data.id;
    self.userId = data.userId;
    self.agentName = data.agentName;
    self.agentWasActivated = data.agentWasActivated;
    self.lastUpdate = formatDate(data.lastUpdate);
    self.deviceId = data.deviceId == null ? NOT_AVAILABLE_LABEL : data.deviceId;
    self.phoneNumber = data.phoneNumber == null ? NOT_AVAILABLE_LABEL : data.phoneNumber;
    self.brand = data.brand == null ? NOT_AVAILABLE_LABEL : data.brand;
    self.model = data.model == null ? NOT_AVAILABLE_LABEL : data.model;
    self.os = data.os == null ? NOT_AVAILABLE_LABEL : data.os;
    self.sdk = data.sdk == null ? NOT_AVAILABLE_LABEL : data.sdk;
    self.firmwareId = data.firmwareId == null ? NOT_AVAILABLE_LABEL : data.firmwareId;
    self.svnVersion = data.svnVersion == null ? NOT_AVAILABLE_LABEL : data.svnVersion;
    self.timeZone = data.timeZone == null ? NOT_AVAILABLE_LABEL : data.timeZone;
    self.deviceName = data.deviceName == null ? NOT_AVAILABLE_LABEL : data.deviceName;
    self.note = data.note == null ? NOT_AVAILABLE_LABEL : data.note;
    self.licId = data.licId == null ? NOT_AVAILABLE_LABEL : data.licId;
    self.licActDate = formatDate(data.licActDate);
    self.creationDate = formatDate(data.creationDate);
    self.operSysType = convertOperationSystemToName(data.operSysType);
}

// Helping methods

function convertOperationSystemToName(data) {
    var result = null;
    switch (data) {
        case 1: result = "iOS";
            break;
        case 2: result = "Android";
            break;
        default: result = 'unknown';
            break;
    }
    return result;

}