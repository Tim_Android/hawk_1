﻿//                 Banned List
$(document).ready(function () {
    ko.validation.init({
        insertMessages: false,
        errorElementClass: 'has-error'
    });

    var viewModel = new BannedViewModel();
    viewModel.loadBannedList();

    ko.applyBindings(viewModel);

    

    // datepicker init
    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, true); //from js file
                }
            });
        });
    }

});


//   class BannedViewModel
function BannedViewModel() {
    var self = this;

    self.filter = new Filter({
        banDateFrom: '',
        banDateTo: '',
        email: null
        
    });

    self.newBannedEmail = ko.validatedObservable(new BannedEmail(emailData));
    self.bannedList = ko.observableArray([]);
    self.isPaginationVisible = ko.observable(false);

    pagerHandler.setDataloadCallBack(self.loadBannedList);

    self.containsBannedEmails = ko.computed(function () {
        return (self.bannedList().length > 0);
    });

    self.loadBannedList = function () {
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/getBannedList", {
            pager: pagerHandler.getPageState(),
            filter: self.filter.getFilter(),
            onSuccess: self.setDataGrid
        });

    }

    self.setDataGrid = function (data) {
        var response = $.parseJSON(data.d);

        self.bannedList.removeAll();
        $.each(response.bannedList, function (index, value) {
            self.bannedList.push(new BannedEmail(value));
        });

        pagerHandler.updatePager(response.pager);
        if (response.pager.totalPages > 1) {
            self.isPaginationVisible(true);
        }
        else {
            self.isPaginationVisible(false);
        }
    };

    self.resetFilter = function () {
        self.filter.resetFilter();
        self.loadBannedList();
    };

    self.showEntry = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }

    self.clickUpdateBannedEmail = function (data) {
        data.note.commit();
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/updateBannedEmail", {
            data: data,
            onSuccess: function (result) {
                notifyHandleResult(result);
                self.loadBannedList();
              
            }
        });
    }

    self.clickDeleteFromBannedList = function (data) {
        
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/deleteEmailFromBannedList", {
            email: data.email,
            onSuccess: function (result) {
                self.loadBannedList();
                notifyHandleResult(result);
            }
        });
    }

    self.addEmail = function () {

        self.newBannedEmail.errors.showAllMessages();
        if (self.newBannedEmail.isValid()) {

            ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/addEmailToBannedList", {
                data: ko.toJS(self.newBannedEmail()),
                onSuccess: function (result) {
                    notifyHandleResult(result);

                    self.loadBannedList();
                    self.newBannedEmail(new BannedEmail(emailData));
                    $('#addEmail').modal('hide');
                }
            });
        }
    }

    self.beginEdit = function (data) {
        data.isBeingEdited(true);
        data.note.beginEdit();
    }

    self.cancelNoteChanges = function (data) {
        data.isBeingEdited(false);
        data.note.rollback();        
    }    
}

// class Filter
function Filter(data) {
    var self = this;

    self.banDateFrom = ko.observable(data.banDateFrom);
    self.banDateTo = ko.observable(data.banDateTo);
    self.email = ko.observable(data.email);    

    self.getFilter = function () {
        return new Filter({
            banDateFrom: self.banDateFrom(),
            banDateTo: self.banDateTo(),
            email: self.email(),
        });
    }

    self.resetFilter = function () {
        self.banDateFrom('');
        self.banDateTo('');
        self.email(null);

        self.resetUiDates();
    }

    self.resetUiDates = function () {
        //update UI state
        if ($(".filter_date_from").length && $(".filter_date_to").length) {
            $(".filter_date_from").each(function (index, value) {
                var dateFrom = $(this);
                var groupId = $(this).attr("date-group");

                $(".filter_date_to").each(function (index, value) {
                    var dateTo = $(this);
                    if (dateTo.attr("date-group") == groupId) {
                        syncDateState(dateFrom, dateTo); //from js file
                    }
                });
            });
        }
    }
}


// class BannedEmail
function BannedEmail(data) {
    var self = this;

    self.banDate = formatDate(data.banDate);
    self.email = ko.observable(data.email).extend({ email: true, required: true });
    self.note = ko.observable(data.note).extend({
        editable: true
    });

    self.isBeingEdited = ko.observable(data.isBeingEdited);
}

// default initialising BannedEmail
var emailData = {
    email: null,
    banDate: new Date(),
    note: null,    
    isBeingEdited: false
};

