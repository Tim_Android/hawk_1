﻿//                 Subscriptions
$(document).ready(function () {
    var viewModel = new LicenseViewModel();
    viewModel.loadLicenseList();
    ko.validation.init({
        insertMessages: false,
        errorElementClass: 'has-error'
    });

    ko.applyBindings(viewModel);
    
    
    // datepicker init
    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, true); //from js file
                }
            });
        });
    }
    
});


//   class LicenseViewModel

function LicenseViewModel() {
    var self = this;
    

    self.filter = new  Filter({
        subscriptionEndFrom: '',
        subscriptionEndTo:   '',
        deviceCnt:           null,
        paySysType:          '',
        subscriptionId:      null,
        userId:              $('#MainWindow_HiddenUserId').val(),
        creationDateFrom:    '',
        creationDateTo:      ''
    });

    self.currentLicense = ko.observable(new EditableLicense(defaultLicense));

    self.licenseList = ko.observableArray([]);

    self.isPaginationVisible = ko.observable(false);

    self.addCurrentLicense = function (data) {
        self.currentLicense(new EditableLicense(data));
    }

    self.containsLicensies = ko.computed(function () {
        return (self.licenseList().length > 0);
    });

    self.loadLicenseList = function () {
        var validator = ko.validatedObservable(self.filter);

        if (validator.isValid()) {
            ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/getLicenseList", {
                pager: pagerHandler.getPageState(),
                filter: self.filter.getFilter(),
                onSuccess: self.setLicenseDataGrid
            });
        }
        else {
            validator.errors.showAllMessages();
        }                
    }

    self.updateLicense = function () {
        var validator = ko.validatedObservable(self.currentLicense());

        if (validator.isValid()) {
            ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/updateLicense", {
                pager: pagerHandler.getPageState(),
                license: self.currentLicense,
                onSuccess: function (result) {
                    var data = JSON.parse(result.d);
                    if (data.Message.MessType == 0) {
                        self.loadLicenseList();
                        self.currentLicense(new EditableLicense(defaultLicense));
                        $('#editSubscription').modal('hide');
                    }
                    notifyHandleResult(result);
                }
            });
        }
        else {
            validator.errors.showAllMessages();
        }
    }
   
    self.setLicenseDataGrid = function (data) {
        var response = $.parseJSON(data.d);

        self.licenseList.removeAll();
        $.each(response.licenseList, function (index, value) {
            self.licenseList.push(new License(value));
        });
        pagerHandler.updatePager(response.pager);
        if (response.pager.totalPages > 1) {
            self.isPaginationVisible(true);
        }
        else {
            self.isPaginationVisible(false);
        }
    };

    self.resetFilter = function () {
        self.filter.resetFilter();
        self.loadLicenseList();
    };

    self.showEntry = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }

    pagerHandler.setDataloadCallBack(self.loadLicenseList);
}


// class Filter

function Filter(data) {
    var self = this;

    self.subscriptionEndFrom = ko.observable(data.subscriptionEndFrom);
    self.subscriptionEndTo =   ko.observable(data.subscriptionEndTo);
    self.deviceCnt = ko.observable(data.deviceCnt).extend({ min: 1, max: 9999, digit: true });
    self.paySysType =          ko.observable(data.paySysType);
    self.subscriptionId =      ko.observable(data.subscriptionId);
    self.userId =              ko.observable(data.userId);
    self.creationDateFrom =    ko.observable(data.creationDateFrom);
    self.creationDateTo =      ko.observable(data.creationDateTo);

    self.getFilter = function () {
        return {
            subscriptionEndFrom: self.subscriptionEndFrom(),
            subscriptionEndTo:   self.subscriptionEndTo(),
            deviceCnt: self.deviceCnt(),
            paySysType:          self.convertToPaySys(),
            subscriptionId:      self.subscriptionId(),
            userId:              self.userId(),
            creationDateFrom:    self.creationDateFrom(),
            creationDateTo:      self.creationDateTo()
        };
    }


    
    self.convertToPaySys = function () {
        var data = self.paySysType();
        if (data == '-1') {
            data = null;
        }
        return data;
    }


    self.resetFilter = function () {
        self.subscriptionEndFrom('');
        self.subscriptionEndTo('');
        self.deviceCnt(null);
        self.paySysType(-1);
        self.subscriptionId(null);
        self.creationDateFrom('');
        self.creationDateTo('');

        self.resetUiDates();
    }


    self.resetUiDates = function () {
        //update UI state
        if ($(".filter_date_from").length && $(".filter_date_to").length) {
            $(".filter_date_from").each(function (index, value) {
                var dateFrom = $(this);
                var groupId = $(this).attr("date-group");

                $(".filter_date_to").each(function (index, value) {
                    var dateTo = $(this);
                    if (dateTo.attr("date-group") == groupId) {
                        syncDateState(dateFrom, dateTo); //from js file
                    }
                });
            });
        }
    }
}



// class License

function License(data) {
    var self = this;

    self.id =             data.id;
    self.subscrEndDate =  formatDate(data.subscrEndDate);
    self.devices = data.devices;
    self.paySys =         convertPaySysEnumToPaySysName(data.type);
    self.subscriptionId = data.subscriptionId == null ? NOT_AVAILABLE_LABEL : data.subscriptionId;
    self.creationDate =   formatDate(data.creationDate);
    self.userId =         data.userId;
    self.priceListId =    data.priceListId;
    self.periodName =     data.periodName;

}

function EditableLicense(data) {
    var self = this;

    self.id = ko.observable(data.id);
    self.subscrEndDate = ko.observable(formatDate(data.subscrEndDate));
    self.devices = ko.observable(data.devices).extend({ min: 1, max: 9999, digit: true });
    self.paySys = ko.observable(convertPaySysEnumToPaySysName(data.type));
    self.subscriptionId = ko.observable(data.subscriptionId == null ? NOT_AVAILABLE_LABEL : data.subscriptionId);
    self.creationDate = ko.observable(formatDate(data.creationDate));
    self.userId = ko.observable(data.userId);
    self.priceListId = ko.observable(data.priceListId);
    self.periodName = ko.observable(data.periodName);

}
var defaultLicense = {
    id: null,
    subscrEndDate: '',
    devices: null,
    paySys: null,
    subscriptionId: null,
    creationDate: null,
    userId: null,
    priceListId: null,
    periodName: null
}
// Helping methods

function convertPaySysEnumToPaySysName(data) {
    var result = null;
    switch(data)
    {
        case 1: result = "AuthorizeNet";
            break;
        case 2: result = "PayPal";
            break;
        case 3: result = "Promocode";
            break;
        default: break;
    }
    return result;
    
}

