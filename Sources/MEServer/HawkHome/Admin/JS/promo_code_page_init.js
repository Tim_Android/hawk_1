﻿
$(document).ready(function () {
    var viewModel = new PromoCodeViewModel();

    viewModel.getTerm();
    pagerHandler.setRecordsPerPage(20);
    ko.applyBindings(viewModel);

    $('div.form-inline input[type=submit], div.chevron').on('click', function () {
        $('#messages_wrapper button.close').click();
    });

    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, false);
                }
            });
        });
    }

    $.blockUI.defaults.css.border = '3px solid #f89406';

    $('.no').click(function () {
        $.unblockUI();
        return false;
    });
});

function PromoCodeFilter()
{
    var self = this;

    self.promoCode = ko.observable('');
    self.generatedFrom = ko.observable('');
    self.generatedTo = ko.observable('');
    self.subscriptionPeriod = ko.observable();
    self.state = ko.observable();
    self.expiration = ko.observable();

    self.subscriptionPeriodInt = function () {
        if (self.subscriptionPeriod() == parseInt(self.subscriptionPeriod())) {
            return parseInt(self.subscriptionPeriod());
        }
        else {
            return null;
        }
    }

    self.stateInt = function () {
        if (self.state() == parseInt(self.state())) {
            return parseInt(self.state());
        }
        else {
            return null;
        }
    }

    self.expiredBool = function () {
        if (self.expiration() == parseInt(self.expiration())) {
            var isExpired = null;
            switch (parseInt(self.expiration()))
            {
                case 0:
                    isExpired = false;
                    break;
                case 1:
                    isExpired = true;
                    break;
            }
            return isExpired;
        }
        else {
            return null;
        }
    }

    self.getFilter = function ()
    {
        return {
            promoCode: self.promoCode(),
            generatedFrom: self.generatedFrom(),
            generatedTo: self.generatedTo(),
            subscriptionPeriodId: self.subscriptionPeriodInt(),
            state: self.stateInt(),
            isExpired: self.expiredBool()
        };
    }
}

function PromoCodeViewModel() {
    var self = this;

    self.promoCodeId;

    //default values
    self.deviceCount = 1;
    self.activationCount = 1;
    self.generateAutomatically = 'generateAutomatically';
    self.generateManually = 'generateManually';

    self.page = ko.observable();
    self.totalPages = ko.observable();
    self.recordsPerPage = ko.observable();
    self.selectedCodeGenType = ko.observable(self.generateAutomatically); //can be two values: generateAutomatically or generateManually

    self.show = ko.observable(true); //flag to show generation part or history part of promo code
    self.termsList = ko.observableArray([]);
    self.promoCodeList = ko.observableArray([]);
    self.isPaginationVisible = ko.observable(false);
    self.message = ko.observable(''); //error or warning message

    self.codeCount = ko.observable(1)
        .extend({
            validation: {
                validator:
                    function (value) {
                        switch (self.selectedCodeGenType()) {
                            case self.generateAutomatically:
                                if (!$.isNumeric(value) || value < 1 || value > 999 || value == '') {
                                    return false;
                                }
                                else {
                                    return true;
                                }
                        }
                        return false;
                    },
                message: "Set the value from 1 to 999."
            }
        });

    //value of user promo code
    self.promoCode = ko.observable('')
        .extend({
            validation: {
                validator:
                    function (value) {
                        switch (self.selectedCodeGenType()) {
                            case self.generateManually:
                                var filter = /^[a-zA-Z0-9].{15,32}$/;
                                if (!filter.test(value)) {
                                    return false;
                                } else {
                                    self.codeCount(1);
                                    return true;
                                }
                            default:
                                return false;
                        }
                    },
                message: "Check that License code length is 16-32 symbols and that you have used both letters (A-Z, a-z) and numbers (0-9) in the Promo code."
            }
        });

    self.dateFromFormat = ko.observable('').extend({ date: { params: true, message: 'The date format is not valid.' } });
    self.dateToFormat = ko.observable('').extend({ date: { params: true, message: 'The date format is not valid.' } });

    self.promoCodeFilter = ko.observable(new PromoCodeFilter())

    self.isGenerateAutomaticallyVisible = ko.computed(function () {
        switch (self.selectedCodeGenType()) {
            case self.generateAutomatically:
                return true;
            case self.generateManually:
                return false;
        }
    });

    self.isGenerateManuallyVisible = ko.computed(function () {
        switch (self.selectedCodeGenType()) {
            case self.generateAutomatically:
                return false;
            case self.generateManually:
                return true;
        }
    });

    self.getTerm = function () {
        $.getJSON("/Admin/Ajax/getPrices.ashx", { isPromoCodePage: true }, function (list) {
            $.map(list, function (obj, index) {
                if (index == 0) {
                    // the first radion button will be checked
                    self.termCheckId = obj.id + '';
                }
                self.termsList.push({ id: obj.id, label: obj.label, type: obj.type });
            });
        });
    };

    self.showContent = function () {
        if (self.show() == true) {
            self.show(false);
            self.getPromoCodeHistoryList();
        } else {
            self.show(true);
        }
    };

    self.clearFilters = function () {
        self.promoCodeFilter().promoCode('');
        self.promoCodeFilter().generatedFrom('');
        self.promoCodeFilter().generatedTo('');
        self.promoCodeFilter().subscriptionPeriod(null);
        self.promoCodeFilter().state(null);
        self.promoCodeFilter().expiration(null);

        self.resetUiDates();

        self.getPromoCodeHistoryList();
    };

    self.onSuccessGetPromoCodeHistoryList = function (data) {
        var res = JSON.parse(data.d);
        if (res.error.code == 0) {
            var errors = res.error.message.split(';');
            self.message(errors[0]);
            notificationHandler.showErrorMessage(errors[1], 'Error');
        }
        else {
            self.message(res.error.message);
        }
        self.page(res.pager.page);
        self.totalPages(res.pager.totalPages);

        self.promoCodeList(res.list);
        pagerHandler.updatePager(res.pager);
    };

    self.getPromoCodeHistoryList = function () {
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/getPromoCodeHistory", {
            pager: pagerHandler.getPageState(),
            filter: self.promoCodeFilter().getFilter(),
            onSuccess: self.onSuccessGetPromoCodeHistoryList
        });
    };

    self.validation = function (data, event) {
        if (!self.dateToFormat.isValid() && !self.dateToFormat.isValid()) {
            return false;
        }
        switch (self.selectedCodeGenType()) {
            case self.generateAutomatically:
                return self.codeCount.isValid();
            case self.generateManually:
                return self.promoCode.isValid();
        }
        return false;
    };

    self.clickDeletePromoCode = function (data, event) {
        modalHandler.blockUI('popupDeletePromoCode');
        self.promoCodeId = data.id;
    };

    self.clickDisablePromoCode = function (data, event) {
        modalHandler.blockUI('popupDisablePromoCode');
        self.promoCodeId = data.id;
    };

    self.deletePromoCode = function (data, event) {
        self.ChangePromoCodeState("/Admin/Ajax/adminWebService.asmx/deletePromoCode");
    };

    self.disablePromoCode = function () {
        self.ChangePromoCodeState("/Admin/Ajax/adminWebService.asmx/disablePromoCode");
    };

    self.ChangePromoCodeState = function (url) {
        $.unblockUI();
        var requestData = {
            id: self.promoCodeId,
            onSuccess: self.onSuccessChangePromoCodeState
        }
        ajaxHandler.sendAjaxPost(url, requestData);
    };

    self.onSuccessChangePromoCodeState = function (data) {
        var res = JSON.parse(data.d);
        switch (res.state.code) {
            case 1:
                notificationHandler.showSuccessMessage(res.state.message);
                break;
            case 0:
                notificationHandler.showErrorMessage(res.state.message, 'Error');
                break;
        }
        self.getPromoCodeHistoryList();
    }

    self.updatePromoCodeTable = function () {
        var pager = pagerHandler.getPageState();
        pager.recordsPerPage = self.recordsPerPage();
        pagerHandler.updatePager(pager);
        self.getPromoCodeHistoryList();
    };

    self.setDataGrid = function (data) {
        self.userList(data.userList);
        pagerHandler.updatePager(data.pager);

        if (response.pager.totalPages > 1) {
            self.isPaginationVisible(true);
        }
        else {
            self.isPaginationVisible(false);
        }
    };

    self.resetUiDates = function () {
        //update UI state
        if ($(".filter_date_from").length && $(".filter_date_to").length) {
            $(".filter_date_from").each(function (index, value) {
                var dateFrom = $(this);
                var groupId = $(this).attr("date-group");

                $(".filter_date_to").each(function (index, value) {
                    var dateTo = $(this);
                    if (dateTo.attr("date-group") == groupId) {
                        syncDateState(dateFrom, dateTo);
                    }
                });
            });
        }
    }

    pagerHandler.setDataloadCallBack(self.getPromoCodeHistoryList);
};