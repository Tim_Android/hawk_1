﻿/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="/JS/intellisense.knockout.js" />
/// 
$(document).ready(function () {
    var viewModel = new CheckListViewModel();
    ko.applyBindings(viewModel);
    viewModel.loadLicenses(1);

    $('#yes').click(function () {
        var updateInfo = {
            licId: $('#updatePopup').attr('data-licid'),
            transactionId: $('#transactionId').val()
        }
        viewModel.updateLicense(updateInfo);
        $.unblockUI();

        var pagesInfo = $('#bottomPagination').bootstrapPaginator("getPages");
        viewModel.loadLicenses(pagesInfo['current']);
    });

    $('#no').click(function () {
        $.unblockUI();
    });

});

function License(data) {
    var self = this;

    self.id = data.id;
    self.email = data.email;
    self.firstName = data.firstName;
    self.lastName = data.lastName;
    self.fullName = ko.computed(function () {
        return self.lastName + ", " + self.firstName;
    });
    self.creationDate = data.creationDate;
    self.subscrEndDate = data.subscrEndDate;
    self.subscriptionId = data.subscriptionId;

    self.isLoading = ko.observable(false);
}

function CheckListViewModel() {
    var self = this;

    self.licenseList = ko.observableArray([]);

    self.transactionId = ko.observable('');

    self.isUpdateAvailable = ko.computed(function () {
        if ($.trim(self.transactionId()).length > 0) {
            return true;
        }
        else {
            return false;
        }
    });

    self.loadLicenses = function (page) {
        $.getJSON("/Admin/Ajax/getLicensesToCheck.ashx",
                  {
                      data: page
                  },
                  function (res) {
                      self.licenseList.removeAll();
                      $.map(res.licenses, function (item) {
                          self.licenseList.push(new License(item));
                      });
                      if (res.page != page) {
                          page = res.page;
                      }
                      var options = {
                          leaps: false,
                          currentPage: res.page,
                          totalPages: res.totalPages,
                          bootstrapMajorVersion: 2,
                          size: 'small',
                          alignment: 'left',
                          onPageClicked: function (e, originalEvent, type, page) {
                              self.loadLicenses(page);
                          },
                          itemContainerClass: function (type, page, current) {
                              return (page === current) ? "active" : "accordion-toggle";
                          }
                      };

                      if (res.totalPages > 1) {
                          $('#bottomPagination').bootstrapPaginator(options);
                          $('#bottomPagination').show();
                          $('.pager').removeClass('hide');
                      }
                      else {
                          $('.pager').addClass('hide');
                      }
                  });
    }

    self.updateSubscriptionShowUI = function (license) {
        $.blockUI.defaults.css.cursor = 'default';
        $.blockUI.defaults.overlayCSS.cursor = 'default';
        $.blockUI.defaults.css.border = '3px solid #f89406';
        $.blockUI({ message: $('#updatePopup') });
        if ($('#updatePopup').attr('data-licid')) {
            $('#updatePopup').removeAttr('data-licid')
        }
        $('#updatePopup').attr('data-licid', license.id);
        $('#transactionId').val('');
    }

    self.updateLicense = function (updateInfo) {
        $.getJSON(
                "/Admin/Ajax/updateLicense.ashx", {
                    data: JSON.stringify(updateInfo)
                },
                function (result) {
                    switch (result.code) {
                        case 0:
                            notificationHandler.showErrorMessage(result.message, 'Error');
                            break;
                        case 1:
                            notificationHandler.showSuccessMessage(result.message);
                            var pagesInfo = $('#bottomPagination').bootstrapPaginator("getPages");
                            self.loadLicenses(pagesInfo['current'])
                            break;
                        default:
                            notificationHandler.showWarningMessage("Unknown result");
                            break;
                    }
                }
                );
    }

    self.notifyUserTransactionFail = function (license) {
        license.isLoading(true);
        $.getJSON(
                "/Admin/Ajax/notifyUserTransactionFail.ashx", {
                    data: license.id
                },
                function (result) {
                    license.isLoading(false);
                    switch (result.code) {
                        case 0:
                            notificationHandler.showErrorMessage(result.message, 'Error');
                            break;
                        case 1:
                            notificationHandler.showSuccessMessage(result.message);
                            break;
                        default:
                            notificationHandler.showWarningMessage("Unknown result");
                            break;
                    }
                }
                );
    }

    self.showLicenseEntry = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }
}

