﻿/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="/JS/intellisense.knockout.js" />

$(document).ready(function () {
    var viewModel = new UserManagementViewModel();
    viewModel.loadUserList();
    ko.applyBindings(viewModel);

    // datepicker init
    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, true); //from js file
                }
            });
        });
    }

    $.blockUI.defaults.css.border = '3px solid #f89406';

    $('.no').click(function () {
        $.unblockUI();
        return false;
    });
});


function UserManagementViewModel() {
    var self = this;

    self.selectedUser = ko.observable(new User({
        registrationDate: null
    }));
    self.filter = new UserFilter({
        registeredFrom: '',
        registeredTo: '',
        email: '',
        hasPurchasedLicenses: null,
        isEmailVerified: null
    });
    self.userList = ko.observableArray([]);
    self.isPaginationVisible = ko.observable(false);
    self.containsUsers = ko.computed(function () {
        return (self.userList().length > 0);
    });

    self.loadUserList = function () {
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/getUserList", {
            pager: pagerHandler.getPageState(),
            filter: self.filter.getFilter(),
            onSuccess: self.setDataGrid
        });
    }

    self.resetFilter = function () {
        self.filter.resetFilter();
        self.loadUserList();
    }

    self.resendVerificationEmail = function (userListEntry) {
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/resendVerificationEmail", {
            email: userListEntry.email,
        });
    }

    self.deleteUser = function () {
        $.unblockUI();
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/deleteUser", {
            email: self.selectedUser().email,
            onSuccess: function () {
                self.loadUserList();
                notificationHandler.showSuccessMessage('The user account has been successfully deleted.');
            }
        });
    }

    pagerHandler.setDataloadCallBack(self.loadUserList);

    self.setDataGrid = function (data) {
        var response = $.parseJSON(data.d);

        self.userList.removeAll();
        $.each(response.userList, function (index, value) {
            self.userList.push(new User(value));
        });
        pagerHandler.updatePager(response.pager);
        if (response.pager.totalPages > 1)
        {
            self.isPaginationVisible(true);
        }
        else {
            self.isPaginationVisible(false);
        }
    };

    self.clickDeleteUser = function (data, event) {
        modalHandler.blockUI('popupDeleteUser');
        self.selectedUser(data);
    };

    self.clickAddToBannedList = function (data) {
        
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/addEmailToBannedList", {
            data: data,
            onSuccess: function (result) {
                notifyHandleResult(result);
                self.loadUserList();
            }
        });
    }

    self.clickDeleteFromBannedList = function (data) {
        
        ajaxHandler.sendAjaxPost("/Admin/Ajax/adminWebService.asmx/deleteEmailFromBannedList", {
            email: data.email,
            onSuccess: function (result) {
                notifyHandleResult(result);
                self.loadUserList();
            }
        });
    }

    self.showEntry = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }
}

function UserFilter(data) {
    var self = this;

    self.registeredFrom = ko.observable(data.registeredFrom);
    self.registeredTo = ko.observable(data.registeredTo);
    self.email = ko.observable(data.email);
    self.hasPurchasedLicenses = ko.observable(data.hasPurchasedLicenses);
    self.isEmailVerified = ko.observable(data.isEmailVerified);

    //these methods required to convert data properly bind filter values on server
    self.hasPurchasedLicensesBool = function () {
        var hasPurchasedLicenses = null;
        switch (self.hasPurchasedLicenses()) {
            case '0':
                hasPurchasedLicenses = false;
                break;
            case '1':
                hasPurchasedLicenses = true;
                break;
        }
        return hasPurchasedLicenses;
    };
    self.isEmailVerifiedBool = function () {
        var isEmailVerified = null;
        switch (self.isEmailVerified()) {
            case '0':
                isEmailVerified = false;
                break;
            case '1':
                isEmailVerified = true;
                break;
        }
        return isEmailVerified;
    };

    self.getFilter = function () {
        return {
            registeredFrom: self.registeredFrom(),
            registeredTo: self.registeredTo(),
            email: self.email(),
            hasPurchasedLicenses: self.hasPurchasedLicensesBool(),
            isEmailVerified: self.isEmailVerifiedBool()
        };
    }

    self.resetFilter = function () {
        self.registeredFrom('');
        self.registeredTo('');
        self.email('');
        self.hasPurchasedLicenses(-1);
        self.isEmailVerified(-1);

        self.resetUiDates();
    }

    self.resetUiDates = function ()
    {
        //update UI state
        if ($(".filter_date_from").length && $(".filter_date_to").length) {
            $(".filter_date_from").each(function (index, value) {
                var dateFrom = $(this);
                var groupId = $(this).attr("date-group");

                $(".filter_date_to").each(function (index, value) {
                    var dateTo = $(this);
                    if (dateTo.attr("date-group") == groupId) {
                        syncDateState(dateFrom, dateTo); //from js file
                    }
                });
            });
        }
    }
}

function User(data) {
    var self = this;

    var day = moment(data.registrationDate);
    self.registrationDate = day.format("MM/DD/YYYY hh:mm:ss A");
    self.firstName = data.firstName;
    self.lastName = data.lastName;
    self.email = data.email;
    self.isEmailVerified = data.isEmailVerified;
    self.hasPurchasedSubscr = data.hasPurchasedLicenses;
    self.hasNotUpdatedSubscr = data.hasNotUpdatedSubscr;
    self.userId = data.id;
    self.isBanned = data.isBanned;
}

