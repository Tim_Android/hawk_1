/*Author:  http://themeforest.net/user/lorthemes // Bogdan Laurentiu */
/*Main Scripts*/
(function(e) {
    "use strict";

    function n(e) {
        e.q.value = "site:" + t + " " + e.qfront.value
    }

    function u(t) {
        e.ajax({
            type: t.attr("method"),
            url: t.attr("action"),
            data: t.serialize(),
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function(t) {
                var n = e('<span class="alert alert-danger"><button type="button" class="close icon-close" data-dismiss="alert" aria-hidden="true"></button>Could not connect to server. Please try again later.</span>');
                e("#notification_container").html(n);
                setTimeout(function() {
                    n.addClass("animate")
                }, 300)
            },
            success: function(t) {
                if (t.result != "success") {
                    var n = t.msg.substring(4),
                        r = e('<span class="alert alert-warning"><button type="button" class="close icon-close" data-dismiss="alert" aria-hidden="true"></button>' + n + "</span>");
                    e("#notification_container").html(r);
                    setTimeout(function() {
                        r.addClass("animate")
                    }, 300)
                } else {
                    var n = t.msg,
                        r = e('<span class="success alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>' + n + "</span>");
                    e("#notification_container").html(r);
                    setTimeout(function() {
                        r.addClass("animate")
                    }, 300)
                }
            }
        })
    }
    var t = "site:www.yourwebsite.com";
    e(".btn-slide").click(function () {
        if (e(this).children().hasClass("icon-angle-up")) {
            e(this).children().addClass("icon-angle-down");
            e(this).children().removeClass("icon-angle-up");
        }
        else {
            e(this).children().addClass("icon-angle-up");
            e(this).children().removeClass("icon-angle-down");
        }
        e("#panel").slideToggle("fast", "linear");
        e(this).toggleClass("active-panel");
        return false
    });
    if (e.fn.dlmenu != "undefined") {
        var r = e("#dl-menu");
        if (r.length > 0) {
            e("#main-menu > ul").clone().addClass("dl-menu").appendTo(r);
            r.find("ul li ul").addClass("dl-submenu");
            r.dlmenu()
        }
    }
    e(document).click(function() {
        e(".searchForm").removeClass("active")
    });
    e(".searchForm").click(function(e) {
        e.stopPropagation()
    });
    e(".searchPanel span").click(function(t) {
        t.stopPropagation();
        if (e(".searchForm").hasClass("active")) {
            e(".searchForm").removeClass("active")
        } else {
            e(".searchForm").addClass("active")
        }
    });
    var i = e("#page-wrapper > header"),
        s = e("#back-top");
    s.hide();
    e(function() {
        e(window).scroll(function() {
            if (e(this).scrollTop() > 100) {
                s.fadeIn();
                i.addClass("fillbg")
            } else {
                s.fadeOut();
                i.removeClass("fillbg")
            }
        });
        s.children("a").click(function() {
            e("body,html").animate({
                scrollTop: 0
            }, 800);
            return false
        })
    });
    var o = e(".price-range-slider");
    e.each(o, function(t, n) {
        var r = e(this),
            i = r.parent().find(".price-result"),
            s = i.data("currency");
        r.slider({
            range: true,
            min: 0,
            max: 500,
            values: [75, 300],
            slide: function(e, t) {
                i.val(s + t.values[0] + " - " + s + t.values[1])
            }
        });
        i.val(s + r.slider("values", 0) + " - " + s + r.slider("values", 1))
    });
    e(".cart-qty").each(function(t, n) {
        var r = e(n),
            i = r.children("input"),
            s = {
                "+": function(e) {
                    return parseFloat(e) + 1
                },
                "-": function(e) {
                    return parseFloat(e) - 1
                }
            },
            o;
        r.children("button").on("click", function(t) {
            t.preventDefault;
            o = s[e(this).data("operator")](i.val());
            if (o >= 1) i.attr("value", o)
        })
    });
    var a = e("#mc-embedded-subscribe-form");
    e("#mc-embedded-subscribe").on("click", function(e) {
        if (e) e.preventDefault();
        u(a)
    });
    e(document).ready(function() {
        function s() {
            var t = e(window).scrollTop();
            if (Math.abs(n - t) <= r) return;
            if (t > n && t > i) {
                e("#header").removeClass("nav-down").addClass("nav-up")
            } else {
                if (t + e(window).height() < e(document).height()) {
                    e("#header").removeClass("nav-up").addClass("nav-down")
                }
            }
            n = t
        }
        var t, n = 0,
            r = 5,
            i = e("#header").outerHeight();
        e(window).scroll(function(e) {
            t = true
        });
        setInterval(function() {
            if (t) {
                s();
                t = false
            }
        }, 250)
    });
    e(window).load(function() {
        if (e.fn.isotope != "undefined") {
            var t = e(".portfolio-wrapper");
            if (t.length > 0) {
                t.isotope({
                    filter: "*",
                    animationOptions: {
                        duration: 750,
                        easing: "linear",
                        queue: false
                    }
                });
                var n = {
                    numberGreaterThan50: function() {
                        var t = e(this).find(".number").text();
                        return parseInt(t, 10) > 50
                    },
                    ium: function() {
                        var t = e(this).find(".name").text();
                        return t.match(/ium$/)
                    }
                };
                e("#portfolio-filters").on("click", "li", function() {
                    var r = e(this).attr("data-filter");
                    r = n[r] || r;
                    t.isotope({
                        filter: r
                    })
                });
                e(".filters-nav").each(function(t, n) {
                    var r = e(n);
                    r.on("click", "li", function() {
                        r.find(".is-active").removeClass("is-active");
                        e(this).addClass("is-active")
                    })
                })
            }
        }
    })
})(jQuery)