﻿/// <reference path="/JS/intellisense.jquery.js" />
/// 
$(document).ready(function () {
    $('#yes').click(function () {
        $.unblockUI();
        $(".deleteAccount").click();
    });

    $('#no').click(function () {
        $.unblockUI();
        return false;
    });

    $('#deleteAccount').on("click", function (event) {
        $.blockUI.defaults.css.cursor = 'default';
        $.blockUI.defaults.overlayCSS.cursor = 'default';
        $.blockUI.defaults.css.border = '3px solid #f89406';
        $.blockUI({ message: $('#popup') });
    });
});

//paraben_color

