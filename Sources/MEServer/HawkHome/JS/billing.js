/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="/JS/intellisense.knockout.js" />

$(document).ready(function () {
    var selectedLicense = '#selectedLicense';

    var viewModel = new TotalsViewModel(selectedLicense);
    ko.applyBindings(viewModel, document.getElementById('order'));
    viewModel.order().plan(0);
    $(selectedLicense).change(function () {
        viewModel.subscriptionOption($(this).val());
        viewModel.devices(viewModel.minDeviceThreshold());
    })
})

function Utils() {
    var self = this;

    self.ConvertDate = function (dateStr) {
        var convertedDate = '';
        if (typeof dateStr != 'undefined' && dateStr != null) {
            var subscrEndDate = new Date(parseInt(dateStr.substr(6)));
            var date = ('0' + subscrEndDate.getDate()).slice(-2);
            var month = ('0' + (subscrEndDate.getMonth() + 1)).slice(-2); //Months are zero based
            var year = ('0' + subscrEndDate.getFullYear()).slice(-2);
            convertedDate = month + '/' + date + '/' + year;
        }
        return convertedDate;
    }
}

function Totals(selectedLicense, selectedPlan, deviceNum, selectedSupportPlan) {
    var self = this;

    //init variables
    self.plan = ko.observable(0);
    self.supportPlan = ko.observable(0);
    self.selectedLicense = selectedLicense;
    self.deviceNumber = ko.observable(deviceNum);
    self.existingDevNumber = ko.observable(0);
    self.residualDays = ko.observable(0);
    self.oldPeriodCostEach = ko.observable(0);
    self.oldPeriodCostEachFormatted = ko.computed(function () {
        return self.oldPeriodCostEach().toFixed(2);
    });
    self.newPeriodFirst = ko.observable(0);
    self.newPeriodDisplay = ko.computed(function () {
        if (self.newPeriodFirst() > 0) {
            return true;
        }
        else {
            return false;
        }
    });
    self.oldPeriodDisplay = ko.computed(function () {
        if (parseFloat(self.oldPeriodCostEach()) > 0) {
            return true;
        }
        else {
            return false;
        }
    });
    self.supportTotal = ko.observable(0);
    self.supportResidualDays = ko.observable(0);
    self.deviceTotal = ko.observable(0);
   

    self.total = ko.observable(0);

    self.oldPeriodCostTotal = ko.computed(function () {
        return (self.deviceNumber() * self.oldPeriodCostEach()).toFixed(2);
    })
    self.newPeriodCostTotalFormatted = ko.computed(function () {
        var newTotal = parseFloat(self.total()) - parseFloat(self.oldPeriodCostTotal());
        return newTotal.toFixed(2);
    })

    self.continueEnabled = ko.computed(function () {
        if (self.total() > 0) {
            return true;
        }
        else {
            return false;
        }
    })

    self.plan.subscribe(function (newValue) {
        if (self.selectedLicense != -1) {

            if (typeof self.plan() !== 'undefined' && self.plan() !== 0 && typeof self.supportPlan() !== 'undefined' && self.supportPlan() !== 0) {
                $.getJSON("/User/Billing/Ajax/calculateTotals.ashx",
                    {
                        selectedLicense: self.selectedLicense,
                        devCount: self.deviceNumber(),
                        priceListId: self.plan(),
                        supportPriceListId: self.supportPlan()
                    },
                function (data) {
                    self.residualDays(data.residualDays);
                    self.existingDevNumber(data.existingDevices);
                    self.oldPeriodCostEach(data.oldPeriodDevEach);
                    self.newPeriodFirst(data.newPeriodFirst.toFixed(2));
                    self.supportResidualDays(data.supportResidualDays);
                    self.deviceTotal(data.deviceTotal.toFixed(2));
                    self.supportTotal(data.supportTotal.toFixed(2));
                    self.total(data.total.toFixed(2));
                });
            }
        }
    }, self);
    self.supportPlan.subscribe(function (newValue) {


            if (typeof self.supportPlan() !== 'undefined' && self.supportPlan() !== 0) {
                $.getJSON("/User/Billing/Ajax/calculateTotals.ashx",
                    {
                        selectedLicense: self.selectedLicense,
                        devCount: self.deviceNumber(),
                        priceListId: self.plan(),
                        supportPriceListId: self.supportPlan()
                    },
                function (data) {
                    self.residualDays(data.residualDays);
                    self.existingDevNumber(data.existingDevices);
                    self.oldPeriodCostEach(data.oldPeriodDevEach);
                    self.newPeriodFirst(data.newPeriodFirst.toFixed(2));
                    self.supportResidualDays(data.supportResidualDays);
                    self.deviceTotal(data.deviceTotal.toFixed(2));
                    self.supportTotal(data.supportTotal.toFixed(2));
                    self.total(data.total.toFixed(2));
                });
            }
      
    }, self);
    //set variables
    self.plan(selectedPlan); //to trigger subscription
    self.supportPlan(selectedSupportPlan);
}

function TotalsViewModel(licenseSelector) {
    var self = this;

    self.plans = ko.observableArray([]);
    self.phoneSupportPlans = ko.observableArray([]);
    self.subscriptionOption = ko.observable($(licenseSelector).val());
    self.minDeviceThreshold = ko.computed(function () {
        if (self.subscriptionOption() == 0) {
            return 1;
        }
        else {
            return 0;
        }
    });
    self.selectedPlanId = ko.observable(0);
    self.selectedSupportPlanId = ko.observable(0);
    self.selectedPlan = ko.computed(function ()
    {
        var selectedPlan = {
            id: "",
            duration: "",
            label: "",
            price: "",
            priceOther: "",
            retailPrice: "",
            subscriptionEnd: "",
            type: "",
            startedOn: ""
        };
        for (var i = 0; i < self.plans().length; i++)
        {
            if (self.plans()[i].id == self.selectedPlanId())
            {
                selectedPlan = self.plans()[i];
            }
        }
        return selectedPlan;
    });
    self.selectedSupportPlan = ko.computed(function () {
        var selectedPlan = {
            id: "",
            duration: "",
            label: "",
            price: "",
            priceOther: "",
            retailPrice: "",
            subscriptionEnd: "",
            type: "",
            startedOn: ""
        };
        for (var i = 0; i < self.phoneSupportPlans().length; i++) {
            if (self.phoneSupportPlans()[i].id == self.selectedSupportPlanId()) {
                selectedPlan = self.phoneSupportPlans()[i];
            }
        }
        return selectedPlan;
    });

    self.devicesActual = ko.observable(self.minDeviceThreshold());

    self.devices = ko.computed({
        read: this.devicesActual,
        write: function (value) {
            var threshold = self.minDeviceThreshold();
            if (value < threshold) {
                self.devicesActual(threshold);
                self.devices.notifySubscribers(threshold);
            }
            else {
                self.devicesActual(value);
            }
        },
        owner: self
    }).extend({ length: 4, isNum: true, notify: 'always' });

    self.plansCount = ko.computed(function () {
        return self.plans().length;
    });

    self.order = ko.computed(function () {
        return new Totals(self.subscriptionOption(), self.selectedPlanId(), self.devicesActual(), self.selectedSupportPlanId())
    });

    self.getPrices = function () {
        if (self.subscriptionOption() != -1) {
        
        self.plans.removeAll();
        var url = "/User/Billing/Ajax/billingService.asmx/GetSubscriptionPriceList";
        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: url,
            data: { selectedLicense: self.subscriptionOption() },
            dataType: 'json',
            success: function (data) {
                var response = data.d;
                var list = response;
                for (var i = 0, count = list.length; i < count; i++) {
                    var utils = new Utils();
                    list[i].subscriptionEnd = utils.ConvertDate(list[i].subscriptionEnd);
                    list[i].price = list[i].price.toFixed(2);
                    list[i].retailPrice = list[i].retailPrice.toFixed(2);
                    self.plans.push(list[i]);
                }
                self.selectedPlanId(list[0].id.toString());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                callback(errorThrown);
            }
        
        });
        }
    }

    self.getPhoneSupportPrices = function () {
        self.phoneSupportPlans.removeAll();
        var url = "/User/Billing/Ajax/billingService.asmx/GetPhoneSupportPriceList";
        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: url,
            data: { selectedLicense: self.subscriptionOption() },
            dataType: 'json',
            success: function (data) {
                var response = data.d;
                var list = response;
                for (var i = 0, count = list.length; i < count; i++) {
                    var utils = new Utils();
                    list[i].subscriptionEnd = utils.ConvertDate(list[i].subscriptionEnd);
                    list[i].price = list[i].price.toFixed(2);
                    list[i].retailPrice = list[i].retailPrice.toFixed(2);
                    self.phoneSupportPlans.push(list[i]);
                }
                self.selectedSupportPlanId(list[0].id.toString());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                callback(errorThrown);
            }
        });

    }
    self.showNewDevices = ko.computed(function () {
        return (self.order().existingDevNumber() == null);
    });
    self.showExistingDevices = ko.computed(function () {
        return (self.order().existingDevNumber() != null);
    });

    self.updatePlans = ko.computed(self.getPrices);
    self.updateSupportPlans = ko.computed(self.getPhoneSupportPrices);

    self.showFade = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    }
    self.additionalServicesOnly = ko.computed(() => {
        if (self.subscriptionOption() == -1) {
            return 1;
        }
        else {
            return 0;
        }
    });
}

ko.extenders.numeric = function (target, precision) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: target,  //always return the original observables value
        write: function (newValue) {
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? 0 : parseFloat(+newValue),
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });

    //initialize with current value to make sure it is rounded appropriately
    result(target());

    //return the new computed observable
    return result;
};

ko.extenders.length = function (target, length) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: target,  //always return the original observables value
        write: function (newValue) {
            var current = target(),
                valueToWrite = newValue;
            if (newValue.toString().length > length) {
                valueToWrite = parseInt(newValue.toString().substring(0, length));
            }
            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                if (newValue !== current) {
                    //we refresh target value, otherwise subscribers won't be notified
                    //target.notifySubscribers doesn't work for some reason
                    target(newValue);
                    target(valueToWrite);
                }
            }
        }
    });

    //initialize with current value to make sure it is appropriate
    result(target());

    //return the new computed observable
    return result;
};

ko.extenders.isNum = function (target, isNum) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: target,  //always return the original observables value
        write: function (newValue) {
            var current = target(),
                valueToWrite = parseInt(newValue);
            if ( typeof newValue == 'string' && isNaN(parseInt(newValue))) {
                valueToWrite = 0;
            }
            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });

    //initialize with current value to make sure it is appropriate
    result(target());

    //return the new computed observable
    return result;
};

ko.bindingHandlers.disableClick = {
    init: function (element, valueAccessor) {
        $(element).click(function (evt) {
            if (valueAccessor())
                evt.preventDefault();
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers.css.update(element, function () { return { disabled_anchor: value }; });
    }
};