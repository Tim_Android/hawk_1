﻿/// <reference path="/JS/intellisense.jquery.js" />

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(0,0),
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);
    SetMarkers(map);
}
    
function SetMarkers(map)
{
    var table_data = $('.gps_table tbody tr').map(function () {
        // $(this) is used more than once; cache it for performance.
        var $row = $(this);

        // For each row that's "mapped", return an object that
        //  describes the first and second <td> in the row.
        return {
            time: $row.find(':nth-child(3)').text(),
            type: $row.find(':nth-child(4)').text(),
            latitude: $row.find(':nth-child(5)').text(),
            longitude: $row.find(':nth-child(6)').text(),
            accuracy: $row.find(':nth-child(7)').text(),
            speed: $row.find(':nth-child(8)').text(),
            altitude: $row.find(':nth-child(9)').text(),
            bearing: $row.find(':nth-child(10)').text(),
        };
    }).get();
    var selectedIndex = $('.table_row_selected').prevAll("tr").length;
    var points = new Array();
    var positions = new Array();
                
    var markers = [];
    for (var i = 1, row_num = table_data.length; i < row_num - 2; i++)
    {
        try
        {
            if (!isNaN(table_data[i].latitude) && !isNaN(table_data[i].longitude) && table_data[i].longitude != '')
            {
                positions.push(new google.maps.LatLng(table_data[i].latitude, table_data[i].longitude));
                var content = '<div style="text-align:center"><strong><table class="table table-bordered table-striped table-condensed table-hover" style ="margin-top: 5px;margin-bottom: 0px;">'
                        + '<tr><td>Date/Time (UTC)</td><td>' + table_data[i].time
                        + '</td><tr><td>Type           </td><td>' + table_data[i].type
                        + '</td><tr><td>Latitude       </td><td>' + table_data[i].latitude
                        + '</td><tr><td>Longitude      </td><td>' + table_data[i].longitude
                        + '</td><tr><td>Accuracy (m)   </td><td>' + table_data[i].accuracy
                        + '</td><tr><td>Speed (m/s)    </td><td>' + table_data[i].speed
                        + '</td><tr><td>Altitude (m)   </td><td>' + table_data[i].altitude
                        + '</td><tr><td>Bearing        </td><td>' + table_data[i].bearing
                        + '</td></table></strong></div>';
                var isSelected = ((selectedIndex) == i) ? true : false;
                markers.push(createMarker(map, table_data[i].latitude, table_data[i].longitude, content, (table_data[i].type == 'GPS') ? true : false, isSelected));
            }
        }
        catch(e)
        {
            continue;
        }                    
    }
    var mc = new MarkerClusterer(map, markers);   
    if (positions.length != 0)
    {
        if (selectedIndex != 0) {
            map.setZoom(16);
            map.panTo(markers[selectedIndex - 1].position);
        }
        else { 
            setBounds(map, positions);
        }
        drawSequence(map, positions);
    }
    
}
    
function createMarker(map, lat, lon, html, isGPS, isSelected)
{
       
    var newmarker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lon),
        map: map
    });
    var icon;
    if( isGPS )
    {
        icon = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
    }
    else
    {
        icon = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
    }
    if (isSelected)
    {
        icon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    }
    newmarker.setIcon(icon);
        
    newmarker.infowindow = new google.maps.InfoWindow({
        content: html
    });

    google.maps.event.addListener(newmarker, 'click', function() {
        if (this.infowindow.getMap()!= null)
        {
            this.infowindow.close();
        }
        else
        {
            this.infowindow.open(map, this);
        }
    });
    return newmarker;
}
    
function setBounds(map, positions)
{
    var bounds = new google.maps.LatLngBounds ();
    for (var i = 0, LtLgLen = positions.length; i < LtLgLen; i++) {
        bounds.extend (positions[i]);
    }
    map.fitBounds(bounds);
}
    
function drawSequence(map, positions)
{
    var lineSequence = new google.maps.Polyline({
        map: map,
        path: positions,
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeWidth: 2
    });
} 