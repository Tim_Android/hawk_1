﻿$(document).ready(function () {
    $("#MainWindow_afterBoughtBehavior_1").click(function () {
        if ($('#MainWindow_notExistFreeDevices').val() == 'true')
        {
            $("#MainWindow_ddlDevices").prop("disabled", true);
        }
        else
        {
            $("#MainWindow_ddlDevices").prop("disabled", false);
        }
    });
    if ($('#MainWindow_notExistFreeDevices').val() == 'false') {
        $("#MainWindow_afterBoughtBehavior_0").click(function () {
            $("#MainWindow_ddlDevices").prop("disabled", true);
        }
        );
    }
});