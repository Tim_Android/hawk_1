﻿/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="/JS/intellisense.knockout.js" />

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("img.lazy").lazyload({
        effect: "fadeIn",
    });

    var viewModel = new TutorialViewModel();
    ko.applyBindings(viewModel);
})

function TutorialViewModel() {
    var self = this;


    self.osNavigation = ko.observableArray([
        new OsNavigationItem({ osType: 1, imagePath: '/Images/android.png', name: 'Android Agent' })/*,
        new OsNavigationItem({ osType: 2, imagePath: '/Images/apple.png', name: 'iOS Agent' })*/
    ]);


    //1 - Android
    //2 - iOS
    self.selectedOsType = ko.observable(1);

    //1 - Android generic
    //2 - iOS 6
    //3 - iOS 7-8
    self.selectedOsVersion = ko.observable(1);
    self.currentSlide = ko.observable(1);

    self.ChangeOs = function (data) {
        self.selectedOsType(data.osType)
        //initializing default version values for selected OS tutorial
        switch (data.osType) {
            case 1:
                self.selectedOsVersion(1);
                break;
            case 2:
                self.selectedOsVersion(2);
                break;
        }
        self.currentSlide(1);
        self.DoScroll();
    }

    self.ChangeOsVersion = function (osVersion) {
        self.currentSlide(1);
        self.selectedOsVersion(osVersion);
        self.DoScroll();
    }

    self.ChangeSlide = function (slideNumber)
    {
        self.currentSlide(slideNumber);
        self.DoScroll();
    }

    self.NextSlide = function ()
    {
        self.currentSlide(self.currentSlide() + 1);
        self.DoScroll();
    }

    self.PrevSlide = function () {
        self.currentSlide(self.currentSlide() - 1);
        self.DoScroll();
    }

    //initiating fake scroll event to load image that was previously hidden
    self.DoScroll = function () {
        $(document).scroll();
    }
}

function OsNavigationItem(data) {
    var self = this;

    self.osType = data.osType;
    self.imagePath = data.imagePath;
    self.name = data.name;
}
