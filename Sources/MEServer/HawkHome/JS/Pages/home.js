﻿$(document).ready(function () {
    InitializeReadMoreLink();

    $('.flicker-example').flickerplate({
        auto_flick: true,
        auto_flick_delay: 6,
    });
});