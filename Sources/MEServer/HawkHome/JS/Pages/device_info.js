﻿/// <reference path="/JS/intellisense.jquery.js" />
/// 
$(document).ready(function () {
    $.blockUI.defaults.css.border = '3px solid #f89406';
    $('#yes').click(function () {
        $.unblockUI();

        $(".deleteAgent")[0].click();
    });

    $('#no').click(function () {
        $.unblockUI();
        return false;
    });

    $('#deleteAgent').on("click", function (event) {
        $.blockUI.defaults.css.cursor = 'default';
        $.blockUI.defaults.overlayCSS.cursor = 'default';
        $.blockUI({ message: $('#popup') });
    });

});

