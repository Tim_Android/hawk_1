﻿
$(document).ready(function () {
    var viewModel = new ViewModel();
    if ($('.shown-attachments').length === 1) {
        viewModel.scrollDown();
    }
    ko.applyBindings(viewModel);
    InitializeReadMoreLink();
});

function ViewModel() {
    var self = this;

    //functions
    self.scrollDown = function () {
        var aTag = $("div#attachments-bottom");
        $('html,body').animate({ scrollTop: aTag.offset().top }, 2000);
    }
}
