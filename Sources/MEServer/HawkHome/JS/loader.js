﻿function Loader() {
    var self = this;

    self.ShowLoading = function (selector) {
        $(selector).nimbleLoader("show");
    }

    self.HideLoading = function (selector) {
        $(selector).nimbleLoader("hide");
    }

    self.InitLoader = function () {
        var myLoadingParams = {
            loaderClass: "loading-bar",
            hasBackground: true
        }
        $.fn.nimbleLoader.setSettings(myLoadingParams);
    }
}