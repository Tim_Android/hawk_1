﻿/// <reference path="/JS/intellisense.jquery.js" />

$(document).ready(function () {
    $('.toggle-menu').jPushMenu();

    // datepicker
    if ($(".filter_date_from").length && $(".filter_date_to").length) {
        $(".filter_date_from").each(function (index, value) {
            var dateFrom = $(this);
            var groupId = $(this).attr("date-group");

            $(".filter_date_to").each(function (index, value) {
                var dateTo = $(this);
                if (dateTo.attr("date-group") == groupId) {
                    initializeDataPicker(dateFrom, dateTo, true); //from js file
                }
            });
        });
    }

    // nav bar
    if (page != 'index') {
        $('#menu_' + page).addClass('active');
    }
});
