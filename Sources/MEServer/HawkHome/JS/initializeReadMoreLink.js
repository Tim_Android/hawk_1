﻿

function InitializeReadMoreLink() {
    $('.article').readmore({
        speed: 100,
        lessLink: '<a  href="#" class="paraben_color">Less <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;</a>',
        moreLink: '<a href="#"  class="paraben_color">More <span class="glyphicon glyphicon-chevron-right"></span></a>'
    });
}
