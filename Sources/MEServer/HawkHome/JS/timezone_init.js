﻿/// <reference path="/JS/intellisense.jquery.js" />

$(document).ready(function ()
{
    var d = new Date();
    var tz = -d.getTimezoneOffset();
    $(".userTimeZone").val(tz);
    $.ajax(
        {
            url: '/Public/Ajax/Helper.asmx/SetTimezone',
            type: 'POST',
            data: 'timezone=' + tz
        }
        )
});