﻿//                 Subscriptions
$(document).ready(function () {
    var viewModel = new LicenseSupportViewModel();
    viewModel.loadLicense();

    ko.applyBindings(viewModel, $('#chat-support')[0]);


});


//   class LicenseViewModel

function LicenseSupportViewModel() {
    var self = this;

    self.currentLicense = ko.observable();

    self.loadLicense = function () {

        var url = "/User/Billing/Ajax/billingService.asmx/GetSupportSubscription";
        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: url,
            dataType: 'json',
            success: function (data) {
                var response = data.d;
                if (response != null) {
                    self.currentLicense(new License(response));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
}


// class License

function License(data) {
    var self = this;

    self.phone = data;
}





