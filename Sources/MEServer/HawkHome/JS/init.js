﻿/// <reference path="/JS/intellisense.jquery.js" />

var InitBlinkEffect = function (selector, fadeInterval) {
    $(selector).fadeOut(fadeInterval).fadeIn(fadeInterval);
    window.setInterval(function () {
        $(selector).fadeOut(fadeInterval).fadeIn(fadeInterval);
    }
    , 2 * fadeInterval);
}

var InitNotifications = function (notificationFieldSelector, notificationIconSelector) {
    InitBlinkEffect(notificationIconSelector, 2000);

    var content = null;
    if (typeof (expiringLicenses) != 'undefined' && typeof (maxExpLicToDisplay) != 'undefined') {
        var notifications = $.parseJSON(expiringLicenses)
        var content = '<div class="notification_popover">';
        var loopsNum = (notifications.length < maxExpLicToDisplay) ? notifications.length : maxExpLicToDisplay;
        for (var i = 0; i < loopsNum; i++) {
            var subscrEndDate = new Date(parseInt(notifications[i].subscrEndDate.substr(6)));
            var date = subscrEndDate.getDate();
            var month = subscrEndDate.getMonth() + 1; //Months are zero based
            var year = subscrEndDate.getFullYear();
            content += '<div>\
                                    ' + notifications[i].licenseName + '\
                                </div>\
                                <div>\
                                    Expires: ' + month + '/' + date + '/' + year + '\
                                </div>\
                                <div>\
                                    Devices Count: ' + notifications[i].devices + '\
                                </div>';
            content += '<div class="license_divider"></div>';
        }
        content += '<div class="popover_footer">Displayed ' + loopsNum + ' of ' + notifications.length + ' </div>';
        content += '<div class="popover_footer" style="text-decoration:underline;"><a href="/User/AccountStatus.aspx">Check All</a></div>';
        content += '</div>';
    }

    $(notificationFieldSelector).popover({
        animation: true,
        html: true,
        placement: 'bottom',
        trigger: 'click',
        delay: { show: 1, hide: 1 },
        title: '<b>Expiring subscriptions</b>',
        content: content != null ? content : ''
    });
}


$(document).ready(function () {
    var notifArea = '.user_info';
    var notifIconArea = '.glyphicon-info-sign';
    if ($(notifIconArea).length > 0) {
        InitNotifications(notifArea, notifIconArea);
    }

    // dot dot plugin
    $(".ellipsis").dotdotdot({});
    // init popovers
    $('body').popover({
        selector: '.note',
        container: 'body',
        placement: 'top',
        trigger: 'hover',

    });
});