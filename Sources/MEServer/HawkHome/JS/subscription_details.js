﻿/// <reference path="loader.js" />
/// <reference path="/JS/intellisense.jquery.js" />

$(document).ready(function() {
    var loader = new Loader();
    loader.InitLoader();
    $(".autocharge").each(function() {
        var licId = $(this).data('license');
        var event = $.Event('cancelSub');
        event.licId = licId;
        event.self = $(this);
        event.loader = loader;
        $(this).click(event, CancelSubscription);
    });
    $('#licenseList').change(ChangeListItemSelection);
});

function CancelSubscription(event) {
    event.data.loader.ShowLoading("#scheduled_payment" + event.data.licId);
    event.data.self.prop('disabled', true);
    $.getJSON("/User/Ajax/manageSubscription.ashx", { licId: event.data.licId }, function (message) {
        event.data.self.prop('disabled', false);
        event.data.loader.HideLoading("#scheduled_payment" + event.data.licId);
        switch (message.code) {
            case 0:
                event.data.self.prop('checked', true);
                notificationHandler.showErrorMessage(message.message, 'Error');
                break;
            case 1:
                notificationHandler.showSuccessMessage(message.message);
                var parent = $('#scheduled_payment' + event.data.licId).parent();
                $('#scheduled_payment' + event.data.licId).slideUp();
                $('#scheduled_payment' + event.data.licId).remove();
                parent.html('<div>This subscription has no upcoming payments</div>')
                break;
            case 2:
                notificationHandler.showWarningMessage(message.message);
                break;
            default:
                notificationHandler.showWarningMessage("Unknown result");
                break;
        }
    });
}

function ChangeListItemSelection() {
    $('#licenseList option').each(function () {
        $('#license' + $(this).val()).slideUp();
    })
    $('#license' + $('#licenseList').val()).slideDown();
}