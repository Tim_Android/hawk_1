﻿var updateFromDate = function (dateFrom, dateTo) {
    var date = dateTo.data('DateTimePicker').date();
    if (date != null) {
        dateFrom.data('DateTimePicker').maxDate(date);
    }
    dateFrom.change();
    dateTo.change();
}

var updateToDate = function (dateFrom, dateTo) {
    var date = dateFrom.data('DateTimePicker').date();
    if (date != null) {
        dateTo.data('DateTimePicker').minDate(date);
    }
    dateFrom.change();
    dateTo.change();
}

var syncDateState = function (dateFrom, dateTo) {
    if (dateFrom.val() == '') {
        resetRestrictions(dateFrom);
        dateFrom.data('DateTimePicker').date(null);
    }
    if (dateTo.val() == '') {
        resetRestrictions(dateTo);
        dateTo.data('DateTimePicker').date(null);
    }
}

var resetRestrictions = function (date) {
    date.data('DateTimePicker').minDate(false);
    date.data('DateTimePicker').maxDate(false);
}

var initializeDataPicker = function (dateFrom, dateTo, isTimeShown) {
    dateFrom.filter_input({ regex: '^[0-9/]$' });
    dateTo.filter_input({ regex: '^[0-9/]$' });

    var format = isTimeShown ? false : 'MM/DD/YYYY';

    dateFrom.datetimepicker({
        format: format
    }).on('dp.hide', function (e) {
        syncDateState(dateFrom, dateTo);
        updateFromDate(dateFrom, dateTo);
        updateToDate(dateFrom, dateTo);
    });

    dateTo.datetimepicker({
        format: format
    }).on('dp.hide', function (e) {
        syncDateState(dateFrom, dateTo);
        updateFromDate(dateFrom, dateTo);
        updateToDate(dateFrom, dateTo);
    });
};
