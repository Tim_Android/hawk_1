﻿/// <reference path="/JS/intellisense.jquery.js" />

var map_initialized = false;

$(document).ready(function () {
    if ($('#map_page').hasClass('show'))
    {
        initialize();
        map_initialized = true;
        $('#map_page').scrollToMe();
    }
});

function ShowMap()
{
   if (!$('#map_tab').hasClass('active'))
   {
      $('#map_tab').addClass('active');
      $('#table_tab').removeClass('active');
      $('#table_page').hide(400);
      if (map_initialized == false) {
          $('#map_page').show(400, function () { initialize(); });
          map_initialized = true;
      }
      else
      {
          $('#map_page').show(400);
      }
      $('#map_page').scrollToMe();
   }
}

function ShowTable()
{
   if (!$('#table_tab').hasClass('active'))
   {
      $('#table_tab').addClass('active');
      $('#map_tab').removeClass('active');
      $('#map_page').hide(400);
      $('#table_page').show(400);
      $('#table_page').scrollToMe();
   }
}

jQuery.fn.extend({
 scrollToMe: function () {
   var x = jQuery(this).offset().top - 100;
   jQuery('html,body').animate({scrollTop: x}, 800);
}});