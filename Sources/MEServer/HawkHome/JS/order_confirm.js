/// <reference path="/JS/intellisense.jquery.js" />
/// <reference path="jquery.base64.js" />
/// <reference path="/JS/intellisense.knockout.js" />


$(document).ready(function () {
    var selectedLicense = '#selectedLicense';

    var viewModel = new TotalsViewModel(selectedLicense);
    ko.applyBindings(viewModel);
});

function Utils() {
    var self = this;

    self.ConvertDate = function (dateStr) {
        var convertedDate = '';
        if (typeof dateStr != 'undefined' && dateStr != null) {
            var subscrEndDate = new Date(parseInt(dateStr.substr(6)));
            var date = ('0' + subscrEndDate.getDate()).slice(-2);
            var month = ('0' + (subscrEndDate.getMonth() + 1)).slice(-2); //Months are zero based
            var year = ('0' + subscrEndDate.getFullYear()).slice(-2);
            convertedDate = month + '/' + date + '/' + year;
        }
        return convertedDate;
    };
}

function Order(orderData) {
    var self = this;

    self.deviceNumber = orderData.additionalDevices;
    self.existingDevNumber = orderData.totals.existingDevices;
    self.residualDays = orderData.totals.residualDays;
    self.oldPeriodCostEach = orderData.totals.oldPeriodDevEach;

    self.oldPeriodCostEachFormatted = ko.computed(function () {
        return self.oldPeriodCostEach.toFixed(2);
    });

    self.newPeriodFirst = orderData.totals.newPeriodFirst;

    self.newPeriodDisplay = ko.computed(function () {
        if (self.newPeriodFirst > 0) {
            return true;
        } else {
            return false;
        }
    });

    self.oldPeriodDisplay = ko.computed(function () {
        if (parseFloat(self.oldPeriodCostEach) > 0) {
            return true;
        } else {
            return false;
        }
    });

    self.oldPeriodCostTotal = ko.computed(function () {
        return (self.deviceNumber * self.oldPeriodCostEach).toFixed(2);
    });

    self.newPeriodCostTotalFormatted = ko.computed(function () {
        var newTotal = parseFloat(self.total) - parseFloat(self.oldPeriodCostTotal());
        return newTotal.toFixed(2);
    });

    self.continueEnabled = ko.computed(function () {
        if (self.total > 0) {
            return true;
        } else {
            return false;
        }
    });
    self.productType = ko.observable(orderData.productType);

    self.selectedPlan = ko.computed(function () {
        var utils = new Utils();
        var selectedPlan = {
            id: orderData.priceListItem.id,
            duration: orderData.priceListItem.duration,
            label: orderData.priceListItem.label,
            price: orderData.priceListItem.price,
            priceOther: orderData.priceListItem.priceOther,
            subscriptionEnd: utils.ConvertDate(orderData.priceListItem.subscriptionEnd),
            type: orderData.priceListItem.type,
            startedOn: utils.ConvertDate(orderData.priceListItem.startedOn)
        };

        return selectedPlan;
    });

    self.total = ko.observable(orderData.totals.total);

    self.showNewDevices = ko.computed(function () {
        return (self.existingDevNumber == null);
    });
    self.showExistingDevices = ko.computed(function () {
        return (self.existingDevNumber != null);
    });
}

function Totals(totalsData, orderData) {
    var self = this;

    self.products = ko.observableArray(orderData);
    self.total = totalsData.toFixed(2);

    self.continueEnabled = ko.computed(function () {
        if (self.total > 0) {
            return true;
        } else {
            return false;
        }
    });
}
function TotalsViewModel(licenseSelector) {
    var self = this;

    self.order = ko.computed(function () {
        var orderCase = $.parseJSON($.base64.decode($('#order').val()));
        var orders = [];
        for (var item in orderCase.Products) {
            orders.push(new Order(orderCase.Products[item]));
        }
        return new Totals(orderCase.total, orders);
    });

    self.changeOption = ko.computed(function () {
        $('.options input:radio').change(function () {
            var value = $(this).val() == "PayPal";
            result = false;
            for (var i in self.order().products()) {
                if (self.order().products()[i].productType() == PHONE_SUBSCRIPTION && value) {
                    result = true;
                }
            }

            self.isPayPal(result);
        });
    });

    self.isPayPal = ko.observable(false);

    self.showFade = function (elem) {
        if (elem.nodeType === 1) {
            $(elem).hide().fadeIn();
        }
    };
}
var PHONE_SUBSCRIPTION = 1;
var SUPPORT_SUBSCRIPTION = 2;