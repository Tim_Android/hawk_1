﻿/// <reference path="/JS/intellisense.jquery.js" />
$(document).ready(function () {

    //Init map
    var mapModel = new MapModel();
    mapModel.mapInit();
    mapModel.parserInit();

    //Init events
    $('#browseFile').click(function () {
        $('#file').click();
    });
    $('#file').change(mapModel.handleFileSelect);
    $('#yes').click(function () {
        $.unblockUI();
        return false;
    });

    //Check browser support
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        $('#notSupported').removeClass('hide');
        $('#browseFile').addClass('pure-button-disabled');
        $('#browseFile').unbind('click');
    }
});

function MapModel() {
    var self = this;

    self.map = new Object();
    self.parser = new Object();
    self.markers = [];

    self.mapInit = function () {
        var mapOptions = {
            center: new google.maps.LatLng(0, 0),
            zoom: 2,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        self.map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    }

    self.parserInit = function () {
        self.parser = new geoXML3.parser(
            {
                map: self.map,
                createMarker: self.addMarker
            }
            );
    }

    self.addMarker = function (placemark)
    {
        self.markers.push (self.parser.createMarker(placemark));
    }

    self.handleFileSelect = function (evt) {
        self.clearMarkers();
        var file = evt.target.files[0];
        var fileExtension = file.name.split('.').pop();
        if (fileExtension == 'kml') {
            $('#fileUploadName').text(file.name);

            var reader = new FileReader();

            // Closure that is triggered after file upload competion
            reader.onload = (function (theFile) {
                return function (e) {
                    self.parser.parseKmlString(e.target.result);
                };
            })(file);

            reader.readAsText(file);

            $('body').scrollTo('#map_canvas', { duration: '2000' });
        }
        else
        {
            $.blockUI.defaults.css.cursor = 'default';
            $.blockUI.defaults.overlayCSS.cursor = 'default';
            $.blockUI({ message: $('#popup') });
        }
    }

    self.clearMarkers = function ()
    {
            for (var i = 0; i < self.markers.length; i++) {
                self.markers[i].setMap(null);
            }
            self.markers = [];
    }
}





