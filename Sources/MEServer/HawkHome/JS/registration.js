﻿/// <reference path="/JS/intellisense.jquery.js" />

$(document).ready(function ()
{
    $(".nano").nanoScroller();
    $("#accept").click(function ()
    {
        $(".lic_agr").slideUp();
        $(".lic_agr").addClass('hide').removeClass('show');
        $(".reg_form").slideDown();
        $(".reg_form").addClass('show').removeClass('hide');
    }
    );
    $("#decline").click(function () {
        alert('You must agree to the terms in license agreement to continue with registration.');
    }
    );
}
);