﻿
var INTERNAL_SERVER_ERROR= 'The server is unavailable.';
var NO_RESPONSE = 'No response from the server. Please try again.';
var MAIN_SELECTOR = "loadedContent";
var NOT_AVAILABLE_LABEL = 'N/A';
var DATETIME_FORMAT = 'MM/DD/YYYY hh:mm:ss A';
