﻿// format date to "MM/DD/YYYY hh:mm:ss A" or if null to 'n/a'
function formatDate(data) {
    var formatedDate = data == null ? NOT_AVAILABLE_LABEL : moment(data).format(DATETIME_FORMAT);
    return formatedDate;
}

// convert retrieved data and notify
function notifyHandleResult(data) {
    var result = JSON.parse(data.d).Message;
    switch (result.MessType) {
        case 0: notificationHandler.showSuccessMessage(result.MessText);
            break
        case 1: notificationHandler.showWarningMessage(result.MessText);
            break
        case 2: notificationHandler.showErrorMessage(result.MessText);
            break
        default:
    }
}