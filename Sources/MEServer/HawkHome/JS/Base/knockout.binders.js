﻿ko.bindingHandlers.popover = {
	update: function (element, valueAccessor, allBindingsAccessor) {
		var value = valueAccessor(),
			valueUnwrap = ko.unwrap(value);
		if ($(element).hasClass('has-error')) {
			$(element).children('textarea, input').popover({
				content: $(element).attr('title'),
				placement: "top",
				trigger: "hover"
			});
		} else {
			$(element).children('textarea, input').popover("destroy");
		}
	}
};

ko.bindingHandlers.dateTimePicker = {
	init: function (element, valueAccessor, allBindingsAccessor) {
		//initialize datepicker with some optional options
		var options = allBindingsAccessor().dateTimePickerOptions || {};
		$(element).datetimepicker(options);

		//when a user changes the date, update the view model
		ko.utils.registerEventHandler(element, "dp.change", function (event) {
			var value = valueAccessor();
			if (ko.isObservable(value)) {
				if (ko.isObservable(value)) {
					if (event.date === false) {
						value(null);
					} else {
						value(event.date);
					}
				}
			}
		});

		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
			var picker = $(element).data("DateTimePicker");
			if (picker) {
				picker.destroy();
			}
		});
	},
	update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

		var picker = $(element).data("DateTimePicker");
		//when the view model is updated, update the widget
		if (picker) {
			var koDate = ko.utils.unwrapObservable(valueAccessor());

			koDate = (typeof (koDate) !== 'object') ? moment(koDate) : koDate;

			if (koDate == null) {
				picker.date(null);
			}
			else {
				picker.date(koDate);
			}
		}
	}
};