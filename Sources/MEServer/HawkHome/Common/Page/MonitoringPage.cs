﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.Entities;
using System.Web.Security;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using HawkDataAccess;
using System.Data;
using Domain.Entities.Abstract;
using Hawk.Services;

namespace HawkHome.Common.Page
{
    public abstract class MonitoringPage : BasePage, IGridDisplayable, IListViewDisplayable
    {
        public UserUI MonitoringUi
        {
            get
            {
                if (Master is UserUI)
                {
                    return (UserUI)Master;
                }
                else
                {
                    return null;
                }
            }
        }

        public DeviceInfoEx CurrentDeviceInfo
        {
            get
            {
                return m_currentDevice;
            }
        }
        public IList<DeviceInfo> DeviceList
        {
            get
            {
                return m_deviceList;
            }
        }

        private GridView m_gridDataLog = null;
        private IList<DeviceInfo> m_deviceList = null;
        private DeviceInfoEx m_currentDevice = null;
        private ListView _listViewDataLog = null;
        public GridView GridDataLog
        {
            get
            {
                return m_gridDataLog;
            }

            set
            {
                m_gridDataLog = value;
            }
        }

        public ListView ListViewDataLog
        {
            get
            {
                return _listViewDataLog;
            }

            set
            {
                _listViewDataLog = value;
            }
        }

        public void BindDataLog(SearchCondition searchCondition)
        {
            if (m_gridDataLog != null)
            {
                m_gridDataLog.PageSize = MonitoringUi.RecordsPerPage;
                DataSet content = DataAccess.GetDataLog(searchCondition);
                m_gridDataLog.DataSource = content;
                MonitoringUi.TotalRecords = content.Tables[0].Rows.Count;
                m_gridDataLog.DataBind();
            }
        }

        public void InitializeDeviceList(bool forceRefresh = false)
        {
            if (IsAuthenticated)
            {
                if (forceRefresh || Session[StorageConstants.DEVICE_LIST_STORAGE] == null)
                {
                    m_deviceList = DeviceManagement.GetAgentsForUser(Account.Value.AspUserId, true);
                    Session[StorageConstants.DEVICE_LIST_STORAGE] = m_deviceList;

                    if (m_deviceList.Count > 0)
                    {
                        DeviceInfoEx deviceFromSession = (DeviceInfoEx)Session[StorageConstants.CURRENT_DEVICE_STORAGE];

                        if (deviceFromSession == null || !m_deviceList.Any(device => device.deviceId == deviceFromSession.deviceId))
                        {
                            m_currentDevice = DeviceManagement.GetDeviceInfo(m_deviceList.First().id);
                            Session[StorageConstants.CURRENT_DEVICE_STORAGE] = m_currentDevice;
                        }
                        else
                        {
                            m_currentDevice = DeviceManagement.GetDeviceInfo(deviceFromSession.id);
                            Session[StorageConstants.CURRENT_DEVICE_STORAGE] = m_currentDevice;
                        }
                    }
                }
                else
                {
                    m_deviceList = (IList<DeviceInfo>)Session[StorageConstants.DEVICE_LIST_STORAGE];
                    m_currentDevice = (DeviceInfoEx)Session[StorageConstants.CURRENT_DEVICE_STORAGE];
                }
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            MonitoringUi.BindDataLoadMethod(DataAccess.GetDataLog);

            //validating current device if it is set
            if (CurrentDeviceInfo != null && !DataAccess.ValidateAgentId(CurrentDeviceInfo.id, Account.Value.AspUserId))
            {
                Server.Transfer("Default.aspx");
            }

            InitializeDeviceList();
            if (Master is UserUI)
            {
                MonitoringUi.BindDeviceList(CurrentDeviceInfo, DeviceList);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            BindDataLog(MonitoringUi.GetSearchCondition());
        }
        public void BindDataListViewLog(SearchCondition searchCondition)
        {
            if (_listViewDataLog != null)
            {
                DataSet content = DataAccess.GetDataLog(searchCondition);
                _listViewDataLog.DataSource = content.Tables[0];
                MonitoringUi.TotalRecords = content.Tables[0].Rows.Count;
                _listViewDataLog.DataBind();
            }
        }

        public int GetSMSTotalCount(SearchCondition searchCondition, string procedureName)
        {
            return DataAccess.GetItemTotalCount(searchCondition, procedureName);
        }
    }
}
