﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.Entities;
using System.Web.Security;
using HawkDataAccess.Models;
using AutoMapper;

namespace HawkHome.Common.Page
{
    public abstract class BasePage : System.Web.UI.Page, IAccountInfo
    {
        public BaseUI GenericUi
        {
            get
            {
                if (Master is BaseUI)
                {
                    return (BaseUI)Master;
                }
                else if (Master.Master is BaseUI)
                {
                    return (BaseUI)Master;
                }
                else
                {
                    return null;
                }
            }
        }

        private UserInfo? m_account = null;

        public UserInfo? Account
        {
            get
            {
                return m_account;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return Account.HasValue;
            }
        }

        public void Logout()
        {
            m_account = null;
            throw new NotImplementedException();
        }

        public void InitializeAccountInfo()
        {
            if (Session[StorageConstants.USER_SESSION_STORAGE] != null)
            {
                m_account = (UserInfo)Session[StorageConstants.USER_SESSION_STORAGE];
            }
            if (m_account == null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Guid userId = (Guid)(Membership.GetUser(HttpContext.Current.User.Identity.Name)).ProviderUserKey;
                UserInfoModel userData = UserInfoModel.GetUserInfo(userId);
                userData.aspUserId = userId;
                m_account = Mapper.Map<UserInfo>(userData);
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);

            InitializeAccountInfo();
        }

    }
}
