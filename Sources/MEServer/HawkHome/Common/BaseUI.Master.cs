﻿using Hawk.Services;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkHome.Code;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace HawkHome
{
    public partial class BaseUI : System.Web.UI.MasterPage
    {
        protected string expiringLicenses;
        protected int maxExpLicToShow;

        public Guid aspUserId { get; set; }
        public UserInfoModel userInfo { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                aspUserId = (Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey;
                userInfo = UserInfoModel.GetUserInfo(aspUserId);
            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();

            if (BanListService.isBanned(HttpContext.Current.User.Identity.Name) && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                Roles.DeleteCookie();
                Session.Abandon();
                Response.Redirect("~/Public/Default.aspx?warning=banned");
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Label userName = (Label)this.GreetingsView.FindControl("UserName");
                if (userName != null)
                {
                    userName.Text = userInfo.firstName + " " + userInfo.lastName;


                    int expiringSubscrPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["expiringSubscrPeriod"]);
                    int trialPromocodeDuration = Convert.ToInt32(ConfigurationManager.AppSettings["trialPromocodeDuration"]);
                    LicenseModel[] expiringLicenses = new LicenseModel[0];

                    LicenseModel[] allLicenses = LicenseModel.GetLicenseList(userInfo.id, null);
                    PriceListItemModel[] allPrices = PriceListItemModel.GetPriceList(null);

                    var nonExpiringNonTrialLicenses = LicenseModel.FilterLicenses(
                                                        allLicenses,
                                                        allPrices,
                                                        expiringSubscrPeriod,
                                                        trialPromocodeDuration,
                                                        (license, price) => !(price.duration == trialPromocodeDuration && price.type == HawkDataAccess.Types.PriceListItemType.Promocode) &&
                                                        license.subscrEndDate.Subtract(DateTime.UtcNow).Days > expiringSubscrPeriod);

                    if (!(nonExpiringNonTrialLicenses.Count() > 0))
                    {
                        var expiringNonTrialLicenses = LicenseModel.FilterLicenses(
                                                        allLicenses,
                                                        allPrices,
                                                        expiringSubscrPeriod,
                                                        trialPromocodeDuration,
                                                        (license, price) => !(price.duration == trialPromocodeDuration && price.type == HawkDataAccess.Types.PriceListItemType.Promocode) && 
                                                        license.subscrEndDate.Subtract(DateTime.UtcNow).Days <= expiringSubscrPeriod);

                        if (expiringNonTrialLicenses.Count() > 0)
                        {
                            expiringLicenses = expiringNonTrialLicenses;
                        }
                        else
                        {
                            var onlyExpiringTrialLicenses = LicenseModel.FilterLicenses(
                                                        allLicenses,
                                                        allPrices,
                                                        expiringSubscrPeriod,
                                                        trialPromocodeDuration,
                                                        (license, price) => (price.duration == trialPromocodeDuration && price.type == HawkDataAccess.Types.PriceListItemType.Promocode) && 
                                                        license.subscrEndDate.Subtract(DateTime.UtcNow).Days <= expiringSubscrPeriod);

                            if (onlyExpiringTrialLicenses.Count() > 0)
                            {
                                expiringLicenses = onlyExpiringTrialLicenses;
                            }
                        }
                    }

                    this.expiringLicenses = JSSerializer.GetInstance().Serialize(expiringLicenses);
                    maxExpLicToShow = Convert.ToInt32(ConfigurationManager.AppSettings["maxExpLicToShow"]);
                }
                PublicLoginStatusMenuItem.Visible = false;
            }
        }

        protected void loginStatus_LoggedOut(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Roles.DeleteCookie();
            Session.Abandon();
        }

        public void SetSuccessMessage(string message)
        {
            successMessage.Visible = true;
            lblSuccessMessageText.Text = message;
        }

        public void SetWarningMessage(string message)
        {
            warningMessage.Visible = true;
            lblWarningMessageText.Text = message;
        }

        public void SetErrorMessage(string message)
        {
            errorMessage.Visible = true;
            lblErrorMessageText.Text = message;
        }

        public void SetInfoMessage(string message)
        {
            infoMessage.Visible = true;
            lblInfoMessageText.Text = message;
        }

        public void ResetMessages()
        {
            successMessage.Visible = false;
            warningMessage.Visible = false;
            errorMessage.Visible = false;
            infoMessage.Visible = false;
        }
    }
}
