﻿using Domain.Entities;
using Domain.Entities.Reports;
using Domain.Entities.Abstract;
using Domain.Entities.Enums;
using Domain.Interfaces;
using HawkDataAccess;
using HawkDataAccess.Models;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;
using Hawk.Services;
using Hawk.Infrastructure.BLL;
using Hawk.Infrastructure.AppLogger;
using System.Net;

namespace HawkHome.Common.Controls.UserDataFilter
{

    public partial class DataFilter : System.Web.UI.UserControl
    {
        private ReportService ReportService = null;
        private TabsType m_currentTab;
        private DeviceInfoEx m_currentDevice;

        protected Func<SearchCondition, DataSet> GetFilteredData;

        public void SetFilterConfiguration(TabsType tab, DeviceInfoEx currentDevice)
        {
            m_currentTab = tab;
            m_currentDevice = currentDevice;

            divFilterForm.Visible = (tab == TabsType.CallLog)
               || (tab == TabsType.SMSLog) || (tab == TabsType.MMSLog)
               || (tab == TabsType.GPSLog) || (tab == TabsType.BrowserLog) || (tab == TabsType.BookmarkLog);
            divFilterFormDateFrom.Visible = divFilterFormDateTo.Visible = (tab == TabsType.CallLog)
               || (tab == TabsType.SMSLog) || (tab == TabsType.MMSLog)
               || (tab == TabsType.GPSLog) || (tab == TabsType.BrowserLog);
            divFilterForm_type.Visible = lstTypeFilter.Visible = (tab == TabsType.CallLog)
               || (tab == TabsType.SMSLog) || (tab == TabsType.MMSLog)
               || (tab == TabsType.GPSLog);
            lblNumber.Visible = txtNumberFilter.Visible = (tab == TabsType.CallLog) || (tab == TabsType.MMSLog);
            lblContent.Visible = txtContentFilter.Visible
                = (tab == TabsType.SMSLog) || (tab == TabsType.MMSLog);
            divFilterFormTitle.Visible = (tab == TabsType.BookmarkLog) || (tab == TabsType.BrowserLog);
            divFilterFormUrl.Visible = (tab == TabsType.BookmarkLog) || (tab == TabsType.BrowserLog);
            divFilterFormVisitCount.Visible = (tab == TabsType.BrowserLog);
            divFilterlstContacts.Visible = (tab == TabsType.SMSLog);
            divFilterlstDateVariant.Visible = (tab == TabsType.SMSLog);
            divView.Visible = (tab == TabsType.SMSLog);

            ListItem selectedType = lstTypeFilter.SelectedItem;
            ListItem selectedContact = lstBxContacts.SelectedItem;
            ListItem selectedDateVariant = ddlDateVariant.SelectedItem;
            ListItem selectedView = ddlView.SelectedItem;
            switch (tab)
            {
                case TabsType.CallLog:
                    lstTypeFilter.Items.Clear();
                    lstTypeFilter.Items.Add(new ListItem("Any", "0"));
                    lstTypeFilter.Items.Add(new ListItem("Incoming", "1"));
                    lstTypeFilter.Items.Add(new ListItem("Outgoing", "2"));
                    lstTypeFilter.Items.Add(new ListItem("Missed", "3"));
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.SMSLog:
                    lstTypeFilter.Items.Clear();
                    lstBxContacts.Items.Clear();
                    ddlDateVariant.Items.Clear();
                    ddlView.Items.Clear();

                    ddlView.Items.Add(new ListItem(HawkCommonResources.Common.BubbleStyle, "1"));
                    ddlView.Items.Add(new ListItem(HawkCommonResources.Common.Grid, "2"));

                    lstTypeFilter.Items.Add(new ListItem(HawkCommonResources.Common.Any, "0"));
                    lstBxContacts.Items.Add(new ListItem(HawkCommonResources.Common.AnyContact, WebUtility.UrlEncode(HawkCommonResources.Common.AnyContact)));
                    ddlDateVariant.Items.Add(new ListItem(HawkCommonResources.Common.DeviceDate, "0"));
                    ddlDateVariant.Items.Add(new ListItem(HawkCommonResources.Common.LocalDate, "1"));
                    Session[StorageConstants.DATE_VARIANT] = "0";

                    List<MessageType> messageTypes = DataAccess.GetMessageTypes();
                    foreach (MessageType messageType in messageTypes)
                    {
                        lstTypeFilter.Items.Add(new ListItem(messageType.Name, messageType.ID.ToString()));
                    }

                    List<MessageContact> messageContacts = DataAccess.GetMessageContacts(m_currentDevice.id);
                    foreach (MessageContact messageContact in messageContacts)
                    {
                        lstBxContacts.Items.Add(new ListItem(string.IsNullOrEmpty(messageContact.Name) || string.IsNullOrWhiteSpace(messageContact.Name) ? HawkCommonResources.Common.UnknownNumber : messageContact.Name, System.Net.WebUtility.UrlEncode((string.IsNullOrEmpty(messageContact.Name) || string.IsNullOrWhiteSpace(messageContact.Name) ? HawkCommonResources.Common.UnknownNumber : messageContact.Name))));
                    }
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.MMSLog:
                    lstTypeFilter.Items.Clear();
                    lstTypeFilter.Items.Add(new ListItem("Any", "0"));
                    List<MessageType> messgTypes = DataAccess.GetMessageTypes();
                    foreach (MessageType messageType in messgTypes)
                    {
                        lstTypeFilter.Items.Add(new ListItem(messageType.Name, messageType.ID.ToString()));
                    }
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.GPSLog:
                    if (m_currentDevice.operSysType == OperatingSystemType.Ios)
                    {
                        divFilterForm_type.Visible = false;
                    }
                    else
                    {
                        lstTypeFilter.Items.Clear();
                        lstTypeFilter.Items.Add(new ListItem("GPS & GSM/Wi-Fi coordinates", "3"));
                        lstTypeFilter.Items.Add(new ListItem("GPS coordinates (more accurate)", "1"));
                        lstTypeFilter.Items.Add(new ListItem("GSM/Wi-Fi coordinates (less accurate)", "2"));
                    }
                    btnExportToKml.Visible = true;
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.BrowserLog:
                    divFilterFormUrl.Visible = true;
                    divFilterFormTitle.Visible = true;
                    divFilterFormVisitCount.Visible = true;
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.BookmarkLog:
                    divFilterFormUrl.Visible = true;
                    divFilterFormTitle.Visible = true;
                    btnDownloadPdf.Visible = true;
                    break;
                case TabsType.AgentInfo:
                case TabsType.CrashLog:
                default:
                    break;
            }

            //updating selectedItem value if it has been saved and it has the same value
            if (selectedType != null && lstTypeFilter.Items.Contains(selectedType))
            {
                lstTypeFilter.SelectedValue = selectedType.Value;
            }
            if (selectedContact != null && lstBxContacts.Items.Contains(selectedContact))
            {
                lstBxContacts.SelectedValue = selectedContact.Value;
                Session[StorageConstants.SELECTED_CONTACT] = WebUtility.UrlDecode(selectedContact.Value) == HawkCommonResources.Common.AnyContact ? null : selectedContact.Value;
            }
            if (selectedDateVariant != null && ddlDateVariant.Items.Contains(selectedDateVariant))
            {
                ddlDateVariant.SelectedValue = selectedDateVariant.Value;
                Session[StorageConstants.DATE_VARIANT] = selectedDateVariant.Value;
            }
            if (Session[StorageConstants.SELECTED_VIEW] == null)
            {
                Session[StorageConstants.SELECTED_VIEW] = (byte)Domain.Entities.Enums.ViewDisplaying.BubbleStyle;
            }
            else if (selectedView != null && ddlView.Items.Contains(selectedView))
            {
                ddlView.SelectedValue = selectedView.Value;
                Session[StorageConstants.SELECTED_VIEW] = byte.Parse(selectedView.Value);
                if (byte.Parse(selectedView.Value) == (byte)Domain.Entities.Enums.ViewDisplaying.Grid)
                {
                    divFilterlstDateVariant.Visible = false;
                }
            }
            else
            {
                ddlView.SelectedValue = ((byte)Session[StorageConstants.SELECTED_VIEW]).ToString();
                if ((byte)Session[StorageConstants.SELECTED_VIEW] == (byte)Domain.Entities.Enums.ViewDisplaying.Grid)
                {
                    divFilterlstDateVariant.Visible = false;
                }
            }


            Session[StorageConstants.DEVICE_NAME] = m_currentDevice == null ? null : m_currentDevice.device;
        }

        public void SetDataLoadDelegate(Func<SearchCondition, DataSet> method)
        {
            GetFilteredData = method;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (lstTypeFilter.Items.Count > 0)
            {//restoring default value only if control is used
                lstTypeFilter.SelectedIndex = 0;
            }
            if (ddlDateVariant.Items.Count > 0)
            {//restoring default value only if control is used
                ddlDateVariant.SelectedIndex = 0;
                Session[StorageConstants.DATE_VARIANT] = ddlDateVariant.SelectedValue;
            }
            if (lstBxContacts.Items.Count > 0)
            {//restoring default value only if control is used
                lstBxContacts.SelectedIndex = 0;
                Session[StorageConstants.SELECTED_CONTACT] = null;
            }
            txtNumberFilter.Text = string.Empty;
            txtContentFilter.Text = string.Empty;
            txtUrl.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtVisitCount.Text = string.Empty;
            txtDateFrom.Text = SearchCondition.DefaultDateFrom.ToString(ConfigurationManager.AppSettings["defaultDateTimeFormat"]);
            txtDateTo.Text = SearchCondition.DefaultDateTo.ToString(ConfigurationManager.AppSettings["defaultDateTimeFormat"]);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
        }

        protected void btnExportToKml_Click(object sender, EventArgs e)
        {
            DataSet gpsData = GetFilteredData(GetSearchCondition());

            KmlExportModel exportData = new KmlExportModel();
            if (exportData.InitCoordinates(gpsData))
            {
                string kml = exportData.ExportCoordinates();
                Response.Clear();
                Response.Buffer = false;
                Response.ContentType = "application/vnd.google-earth.kml+xml";
                Response.AddHeader("Content-Disposition", "attachment;filename=\"coordinates.kml\"");
                Response.Write(kml.ToCharArray(), 0, kml.Length);
                Response.End();
            }
            else
            {
                BaseUI main = Page.Master.Master as BaseUI;
                main.SetErrorMessage(Resources.Error.KmlExportCoordinateInit);
            }
        }

        public SearchCondition GetSearchCondition()
        {
            SearchCondition retCondition = null;

            switch (m_currentTab)
            {
                case TabsType.CallLog:
                    retCondition = new CallSearchCondition(m_currentDevice.id,
                         DateTimeFromFilter, DateTimeToFilter, (PhoneCallType)TypeFilter,
                        NumberFilter);
                    break;
                case TabsType.SMSLog:
                    retCondition = new SMSSearchCondition(m_currentDevice.id,
                        DateTimeFromFilter, DateTimeToFilter, TypeFilter,
                        ContactFilter, ContentFilter);
                    break;
                case TabsType.MMSLog:
                    retCondition = new MMSSearchCondition(m_currentDevice.id,
                        DateTimeFromFilter, DateTimeToFilter, TypeFilter,
                        NumberFilter, ContentFilter);
                    break;
                case TabsType.GPSLog:
                    retCondition = new GPSSearchCondition(m_currentDevice.id,
                        DateTimeFromFilter, DateTimeToFilter,
                        (GPSProviderMaskType)TypeFilter);
                    break;
                case TabsType.BrowserLog:
                    retCondition = new BrowserHistorySearchCondition(m_currentDevice.id,
                        DateTimeFromFilter, DateTimeToFilter, UrlFilter, TitleFilter, VisitsCountFilter);
                    break;
                case TabsType.BookmarkLog:
                    retCondition = new BookmarkHistorySearchCondition(m_currentDevice.id, UrlFilter, TitleFilter);
                    break;
                default:
                    retCondition = null;
                    break;
            }

            if (retCondition != null)
            {
                retCondition.UserTimeZoneOffset = Convert.ToInt32(Session[StorageConstants.USER_TZ_OFFSET_STORAGE]);
            }
            return retCondition;
        }

        #region Download PDF Report

        protected void DownloadPDF_OnClick(object sender, EventArgs e)
        {
            try
            {
                DataTable data = GetFilteredData(GetSearchCondition()).Tables[0];

                ReportService = new ReportService();

                var report = ReportService.GetReport(data, m_currentTab, new FilterValues
                {
                    ContactFilter = ContactFilter,
                    ContentFilter = ContentFilter,
                    DateTimeFromFilter = DateTimeFromFilter,
                    DateTimeToFilter = DateTimeToFilter,
                    NumberFilter = NumberFilter,
                    TitleFilter = TitleFilter,
                    TypeFilter = TypeFilter,
                    UrlFilter = UrlFilter,
                    VisitsCountFilter = VisitsCountFilter,
                    GPSSelectedEntry = GPSSelectedEntry,
                    BasePath = Server.MapPath(HawkCommonResources.Common.CurrentDirectory),
                    Device = m_currentDevice.device
                });

                byte[] content = report.ToByteArray();

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + report.FileName + report.FileExtension);
                Response.BinaryWrite(content);
                Response.End();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                BaseUI main = Page.Master.Master as BaseUI;
                main.SetErrorMessage(Resources.Error.PDFReportGenerating);
            }
        }

        #endregion

        #region DataFilters Abstraction
        private DateTime DateTimeFromFilter
        {
            get
            {
                DateTime date = GetDate(txtDateFrom.Text, SearchCondition.DefaultDateFrom);

                txtDateFrom.Text = date.ToString(ConfigurationManager.AppSettings["defaultDateTimeFormat"]);
                return date;
            }
            set
            {
                txtDateFrom.Text = value.ToString("d", DateTimeFormatInfo.InvariantInfo);
            }
        }

        private DateTime DateTimeToFilter
        {
            get
            {
                DateTime date = GetDate(txtDateTo.Text, SearchCondition.DefaultDateTo);

                txtDateTo.Text = date.ToString(ConfigurationManager.AppSettings["defaultDateTimeFormat"]);
                return date;
            }
            set
            {
                txtDateTo.Text = value.ToString("d", DateTimeFormatInfo.InvariantInfo);
            }
        }

        private DateTime GetDate(string txtDate, DateTime defaultDate)
        {
            if (IsFiltered)
            {
                DateTime validDate;
                if (DateTime.TryParse(txtDate, out validDate))
                {
                    defaultDate = validDate;
                }
            }
            return defaultDate;
        }

        private int TypeFilter
        {
            get
            {
                int retValue = default(int);
                if (int.TryParse(lstTypeFilter.SelectedValue, out retValue))
                {
                    return retValue;
                }
                else
                {
                    return 0;
                }
            }
        }

        private string GPSSelectedEntry
        {
            get
            {
                return lstTypeFilter.SelectedItem == null ? null : lstTypeFilter.SelectedItem.Text;
            }
        }
        private string ContactFilter
        {
            get
            {
                if (WebUtility.UrlDecode(lstBxContacts.SelectedValue) == HawkCommonResources.Common.AnyContact)
                {
                    return null;
                }
                else if (WebUtility.UrlDecode(lstBxContacts.SelectedValue) == HawkCommonResources.Common.UnknownNumber)
                {
                    return string.Empty;
                }
                else
                {
                    return WebUtility.UrlDecode(lstBxContacts.SelectedValue);
                }
            }
        }

        private string DateVariantSelectedEntry
        {
            get
            {
                return ddlDateVariant.SelectedItem.Text;
            }
        }
        private string NumberFilter
        {
            get
            {
                if (txtNumberFilter.Text == string.Empty)
                {
                    return null;
                }
                else
                {
                    return txtNumberFilter.Text;
                }
            }
        }

        private string ContentFilter
        {
            get
            {
                return txtContentFilter.Text;
            }
        }

        private string UrlFilter
        {
            get
            {
                return txtUrl.Text;
            }
        }

        private string TitleFilter
        {
            get
            {
                return txtTitle.Text;
            }
        }

        private int? VisitsCountFilter
        {
            get
            {
                int retValue = default(int);

                if (txtVisitCount.Text.Trim() != string.Empty && int.TryParse(txtVisitCount.Text, out retValue))
                {
                    return retValue;
                }
                else
                {
                    //as the filter value is invalid we reset it to empty string
                    txtVisitCount.Text = string.Empty;
                    return null;
                }
            }
        }

        private bool IsFiltered
        {
            get
            {
                bool retVal = false;

                try
                {
                    if (divFilterForm.Visible)
                    {
                        retVal = true;
                    }
                }
                catch (Exception)
                {
                    retVal = false;
                }

                return retVal;
            }
        }
        #endregion DataFilters Absraction
        public int ViewDisplaying
        {
            get
            {
                return byte.Parse(ddlView.SelectedValue);
            }
            set
            {
                ddlView.SelectedValue = value.ToString();
            }
        }

    }
}