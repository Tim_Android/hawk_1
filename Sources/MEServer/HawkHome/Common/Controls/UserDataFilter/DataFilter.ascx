﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataFilter.ascx.cs" Inherits="HawkHome.Common.Controls.UserDataFilter.DataFilter" %>

<div id="filters">
    <asp:Panel ID="Panel1" DefaultButton="btnFilter" runat="server">
        <div id="divFilterForm" runat="server" visible="false" class="elements_margin">
            <div class="clearfix">
                <div class="pull-left filter_field form-group relative-pos" id="divFilterlstContacts" runat="server">
                    <label for="lstBxContacts">Contact:</label>
                    <asp:DropDownList CssClass="form-control" ID="lstBxContacts" runat="server"></asp:DropDownList>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterFormUrl" runat="server">
                    <label for="txtUrl">URL:</label>
                    <div>
                        <asp:TextBox ID="txtUrl" runat="server" CssClass="form-control" MaxLength="8000"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterFormTitle" runat="server">
                    <label for="txtTitle">Title:</label>
                    <div>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterFormVisitCount" runat="server">
                    <label for="txtVisitCount">Visit count more than:</label>
                    <div>
                        <asp:TextBox ID="txtVisitCount" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterForm_type" runat="server">
                    <label for="lblType">
                        <asp:Label ID="lblType" runat="server" Text="Type:"></asp:Label>
                    </label>
                    <div>
                        <asp:DropDownList CssClass="form-control" ID="lstTypeFilter" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterlstDateVariant" runat="server">
                        <label for="ddlDateVariant">Date Displaying:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlDateVariant" runat="server"></asp:DropDownList>
                </div>

                <div class="pull-left filter_field form-group relative-pos">
                    <label for="lblNumber">
                        <asp:Label ID="lblNumber" runat="server" Text="Address Book Entry:"></asp:Label>
                    </label>
                    <div>
                        <asp:TextBox ID="txtNumberFilter" runat="server" CssClass="filter_address_book form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left form-group relative-pos" id="divFilterFormDateFrom" runat="server">
                    <label for="txtDateFrom">Date from:</label>
                    <div>
                        <asp:TextBox ID="txtDateFrom" runat="server" CssClass="filter_date_from form-control date" date-group="1"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos" id="divFilterFormDateTo" runat="server">
                    <label for="txtDateTo">Date to:</label>
                    <div>
                        <asp:TextBox ID="txtDateTo" runat="server" CssClass="filter_date_to form-control date" date-group="1"></asp:TextBox>
                    </div>
                </div>
                <div class="pull-left filter_field form-group relative-pos">
                    <label for="lblContent">
                        <asp:Label ID="lblContent" runat="server" Text="Content:"></asp:Label>
                    </label>
                    <div>
                        <asp:TextBox ID="txtContentFilter" runat="server" CssClass="filter_content form-control"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div>
                <asp:LinkButton ID="btnClear" runat="server" OnClick="btnClear_Click" CssClass="btn btn-sm background_color_gray label_color_black">
                                               Clear filters
                </asp:LinkButton>
                <asp:LinkButton ID="btnFilter" runat="server" OnClick="btnFilter_Click" CssClass="btn btn-sm btn-warning label_color_black">
                                              Filter Data
                </asp:LinkButton>
                <asp:LinkButton ID="btnExportToKml" runat="server" OnClick="btnExportToKml_Click" Visible="false"
                    CssClass="btn btn-sm btn-warning label_color_black">
                                        <i class="icon-download"></i> Download GPS Data
                </asp:LinkButton>
                <asp:LinkButton ID="btnDownloadPdf" runat="server" OnClick="DownloadPDF_OnClick" Visible="false"
                    CssClass="btn btn-sm btn-warning label_color_black">
                                        <i class="icon-file-pdf"></i> Download PDF
                </asp:LinkButton>
                <a tabindex="0" class="note" title="Please Note" data-content="<%= HawkCommonResources.Common.PDFNote %>">
                                                <img class="btn btn-sm" src="<%= ResolveUrl("~/Images/sign-computer-icon-symbol-signs-info-information.png") %>" />
                                            </a>
            </div>
        </div>
        <div class="row mbottom10" runat="server" id="divView">
                            <div class="col-sm-12">
                                <div class="form-inline pull-right">
                                    <label class="control-label" for="ddlView">View:&nbsp;</label>
                                    <asp:DropDownList ID="ddlView" runat="server" AutoPostBack="true" CssClass="records_per_page form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
    </asp:Panel>
</div>
