﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Messages.ascx.cs" Inherits="RegSite.Common.Controls.Messages" %>

<div class="mbottom25">
    <div runat="server" id="successMessage" visible="false" class="mtop20">
        <div class="alert alert-success mbottom0">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <asp:Label runat="server" ID="lblSuccessMessageText"></asp:Label>
        </div>
    </div>
    <div runat="server" id="warningMessage" visible="false" class="mtop20">
        <div class="alert alert-warning mbottom0">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <asp:Label runat="server" ID="lblWarningMessageText"></asp:Label>
        </div>
    </div>
    <div runat="server" id="errorMessage" visible="false" class="mtop20">
        <div class="alert alert-danger mbottom0">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <asp:Label runat="server" ID="lblErrorMessageText"></asp:Label>
        </div>
    </div>
    <div runat="server" id="infoMessage" visible="false" class="mtop20">
        <div class="alert alert-info mbottom0">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <asp:Label runat="server" ID="lblInfoMessageText"></asp:Label>
        </div>
    </div>
</div>
