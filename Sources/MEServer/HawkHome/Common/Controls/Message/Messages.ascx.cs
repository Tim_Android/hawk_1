﻿using System;

namespace RegSite.Common.Controls
{
    public partial class Messages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e) { }

        public void HideMessage()
        {
            successMessage.Visible = false;
            warningMessage.Visible = false;
            errorMessage.Visible = false;
            infoMessage.Visible = false;
        }
        public void ShowError(string message)
        {
            errorMessage.Visible = true;
            lblErrorMessageText.Text = message;
        }

        public void ShowInfo(string message)
        {
            infoMessage.Visible = true;
            lblInfoMessageText.Text = message;
        }

        public void ShowWarning(string message)
        {
            warningMessage.Visible = true;
            lblWarningMessageText.Text = message;
        }

        public void ShowSuccess(string message)
        {
            successMessage.Visible = true;
            lblSuccessMessageText.Text = message;
        }
    }
}