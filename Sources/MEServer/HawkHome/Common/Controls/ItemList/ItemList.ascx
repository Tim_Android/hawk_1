﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemList.ascx.cs" Inherits="HawkHome.Common.ItemList" %>

<div>
    <div class="mbottom10">
        <span class="label_color_black">Device Name</span>
    </div>
</div>
<div id="jp-container" class="jp-container">
    <asp:ListView runat="server" ID="AgentList">
        <LayoutTemplate>
            <div runat="server" id="itemPlaceholder">
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="device-select-row <%# selectedClass(Eval("Active"))%> ">
                <div class="row zero_margin" data-no-collapse="true">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn-device-list clearfix" OnClick="btnAgentsList_Click"
                        data-agent='<%# DataBinder.Eval(Container.DataItem, "ID")%>' title='<%# DataBinder.Eval(Container.DataItem, "IMEI")%>'>
                        <div id="Div1" runat="server" class="col-sm-10">
                            <div class="hide_overflow_text item mtop5 mleft5 <%# selectedClass(Eval("Active"))%>">
                                <%# Eval("Name") %>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <img src="<%# Eval("ImageLink") %>" class="mtop3 no-border"/>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>
