﻿using HawkDataAccess.Models;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HawkDataAccess;
using Domain.Entities;

namespace HawkHome.Common
{
    public partial class ItemList : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string selectedClass(object strArg)
        {
            bool active = Convert.ToBoolean(strArg);
            if (active)
            {
                return "selected-item";
            }
            else
            {
                return "";
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            List<string> JS = new List<string>(){ "JS/jquery.jscrollpane.min.js",
                                          "JS/jquery.mousewheel.js",
                                          "JS/scroll-startstop.events.jquery.js",
                                          "JS/scroll_init.js"
                    };
            foreach (string ScriptName in JS)
            {
                using (HtmlGenericControl JSControl = new HtmlGenericControl())
                {
                    JSControl.TagName = "script";
                    JSControl.Attributes.Add("type", "text/javascript");
                    JSControl.Attributes.Add("language", "javascript");
                    JSControl.Attributes.Add("src", ResolveUrl(ScriptName));
                    Page.Header.Controls.Add(JSControl);
                }
            }
            //include CSS
            HtmlGenericControl CSSControl = new HtmlGenericControl();
            CSSControl.TagName = "link";
            CSSControl.Attributes.Add("type", "text/css");
            CSSControl.Attributes.Add("rel", "stylesheet");
            CSSControl.Attributes.Add("href", ResolveUrl("CSS/jquery.jscrollpane.css"));
            Page.Header.Controls.Add(CSSControl);
        }

        protected void btnAgentsList_Click(object sender, EventArgs e)
        {
            string selectedAgent = ((LinkButton)sender).Attributes["data-agent"];

            Session[StorageConstants.CURRENT_DEVICE_STORAGE] = DeviceManagement.GetDeviceInfo(Convert.ToInt32(selectedAgent));

            TabsType tab = TabsType.AgentInfo;
            if (Session[StorageConstants.CURRENT_TAB_STORAGE] != null)
            {
                tab = (TabsType)Session[StorageConstants.CURRENT_TAB_STORAGE];
            }
            RedirectToTab(tab);
        }

        public void SetDataBind(object DataBind)
        {
            AgentList.DataSource = DataBind;
            AgentList.DataBind();
        }

        public void RedirectToTab(TabsType tab)
        {
            string redirectUrl = string.Empty;
            switch (tab)
            {
                case TabsType.AgentInfo:
                    redirectUrl = "~/User/AgentInfo.aspx";
                    break;
                case TabsType.CallLog:
                    redirectUrl = "~/User/CallHistory.aspx";
                    break;
                case TabsType.SMSLog:
                    redirectUrl = "~/User/SMSLog.aspx";
                    break;
                case TabsType.MMSLog:
                    redirectUrl = "~/User/MMSLog.aspx";
                    break;
                case TabsType.GPSLog:
                    redirectUrl = "~/User/GPSLog.aspx";
                    break;
                case TabsType.CrashLog:
                    redirectUrl = "~/User/CrashLog.aspx";
                    break;
                default:
                    redirectUrl = "~/User/AgentInfo.aspx";
                    break;
            }
            Response.Redirect(redirectUrl, true);
        }
    }
}