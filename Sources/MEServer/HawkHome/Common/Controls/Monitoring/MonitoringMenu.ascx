﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonitoringMenu.ascx.cs" Inherits="HawkHome.Common.Controls.Monitoring.MonitoringMenu" %>

<div id="navigationUi">
    <ul class="nav nav-pills">
        <li id="menu_device_info" role="presentation">
            <asp:HyperLink ID="btnDevice" runat="server" Text="Device Info" NavigateUrl="~/User/AgentInfo.aspx" />
        </li>
        <li id="menu_call_list" role="presentation">
            <asp:HyperLink ID="btnCallList" runat="server" Text="Call History" NavigateUrl="~/User/CallHistory.aspx" />
        </li>
        <li id="menu_sms" role="presentation">
            <asp:HyperLink ID="btnSMS" runat="server" Text="SMS Messages" NavigateUrl="~/User/SMSLog.aspx" />
        </li>
        <li id="menu_mms" role="presentation">
            <asp:HyperLink ID="btnMMS" runat="server" Text="MMS Messages" NavigateUrl="~/User/MMSLog.aspx" />
        </li>
        <li id="menu_gps" role="presentation">
            <asp:HyperLink ID="btnGPS" runat="server" Text="GPS History" NavigateUrl="~/User/GPSLog.aspx" />
        </li>

        <% if (CurrentDevice.operSysType == Domain.Entities.Enums.OperatingSystemType.Android)
            { %>
        <li id="menu_browser_history" role="presentation">
                                        <asp:HyperLink ID="btnBrowserHistory" runat="server" Text="Browser History" NavigateUrl="~/User/BrowserHistory.aspx" />
                                    </!--li>
                                    <li id="menu_bookmark_history" role="presentation">
                                        <asp:HyperLink ID="btnBookmarkHistory" runat="server" Text="Browser Bookmark" NavigateUrl="~/User/BookmarkHistory.aspx" />
                                    </li>
        <% } %>
    </ul>
</div>
