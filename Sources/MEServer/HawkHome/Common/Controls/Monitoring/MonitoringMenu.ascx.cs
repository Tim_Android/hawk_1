﻿using Domain.Entities.Abstract;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HawkHome.Common.Controls.Monitoring
{
    public partial class MonitoringMenu : System.Web.UI.UserControl
    {
        private DeviceInfoEx m_currentDevice;

        public DeviceInfoEx CurrentDevice
        {
            get
            {
                return m_currentDevice;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetMenuConfiguration(DeviceInfoEx currentDevice)
        {
            m_currentDevice = currentDevice;
        }
    }
}