﻿using System;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Enums;
using HawkDataAccess.Models;

namespace Hawk.Infrastructure.Mapping
{
    public static class MappingsBootstrapper
    {
        public static void Initialize()
        {
            InitializeDbMappings();
        }

        private static void InitializeDbMappings()
        {
            //map from-to
            Mapper.CreateMap<GET_DEVICES_PAGED_Result, Device>()
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.USER_ID))
                .ForMember(dest => dest.AgentName, opts => opts.MapFrom(src => src.AGENT_NAME))
                .ForMember(dest => dest.AgentWasActivated, opts => opts.MapFrom(src => src.IS_ACTIVED))
                .ForMember(dest => dest.LastUpdate, opts => opts.MapFrom(src => src.LAST_UPDATED))
                .ForMember(dest => dest.PhoneNumber, opts => opts.MapFrom(src => src.PHONE_NUMBER))
                .ForMember(dest => dest.FirmwareId, opts => opts.MapFrom(src => src.FIRMWARE_ID))
                .ForMember(dest => dest.SvnVersion, opts => opts.MapFrom(src => src.SVN_VERSION))
                .ForMember(dest => dest.TimeZone, opts => opts.MapFrom(src => src.TIME_ZONE))
                .ForMember(dest => dest.DeviceName, opts => opts.MapFrom(src => src.DEVICE_NAME))
                .ForMember(dest => dest.DeviceId, opts => opts.MapFrom(src => src.DEVICE_ID))
                .ForMember(dest => dest.LicId, opts => opts.MapFrom(src => src.LIC_ID))
                .ForMember(dest => dest.LicActDate, opts => opts.MapFrom(src => src.LIC_ACT_DATE))
                .ForMember(dest => dest.CreationDate, opts => opts.MapFrom(src => src.CREATION_DATE))
                .ForMember(dest => dest.OperSysType, opts => opts.MapFrom(src => (OperatingSystemType)src.OPER_SYS_TYPE));
            Mapper.CreateMap<UserInfoModel, UserInfo>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.id))
                .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.firstName))
                .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.lastName))
                .ForMember(dest => dest.Email, opts => opts.MapFrom(src => src.email))
                .ForMember(dest => dest.AspUserId, opts => opts.MapFrom(src => src.aspUserId));
            Mapper.CreateMap<GET_BANNED_LIST_PAGED_Result, BannedEmailModel>()
                .ForMember(dest => dest.id, opts => opts.MapFrom(src => src.ID))
                .ForMember(dest => dest.email, opts => opts.MapFrom(src => src.EMAIL))
                .ForMember(dest => dest.banDate, opts => opts.MapFrom(src => src.BAN_DATE))
                .ForMember(dest => dest.note, opts => opts.MapFrom(src => src.NOTE));
        }
    }
}
