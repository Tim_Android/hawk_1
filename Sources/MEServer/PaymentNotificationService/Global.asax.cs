﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using PaymentNotificationService;
using NLog;

namespace PaymentNotificationService
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
        }

        void Application_End(object sender, EventArgs e)
        {
        }

        void Application_Error(object sender, EventArgs e)
        {
            NLog.Logger logger = LogManager.GetCurrentClassLogger();
            Exception ex = Context.Server.GetLastError();
            logger.DebugException(Resources.Settings.GeneralException, ex);
            logger.Debug(Resources.Settings.Divider);
        }
    }
}
