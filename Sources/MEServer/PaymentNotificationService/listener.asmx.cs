﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Linq;
using NLog;
using HawkDataAccess;
using EmailManagement;
using HawkDataAccess.Models;
using PayPal;
using HawkBilling.PaymentProcessors;

namespace PaymentNotificationService
{
    /// <summary>
    /// Class for handling SilentPost responses from payment gateways
    /// Logger message usage:
    ///        Error - failure of some processing logic or on payment system side
    ///        Debug - exception is thrown
    ///        Trace - fake request is sent
    /// </summary>
    public class PaymentProcessorListener : System.Web.Services.WebService
    {
        //TODO: Replace with Logging library
        private Logger logger;
        private IPNMessage ipn;

        public PaymentProcessorListener()
        {
            this.logger = LogManager.GetCurrentClassLogger();
        }

        [WebMethod]
        public void AuthorizeNetTransactionInfo()
        {
            logger.Trace("Request Host: " + Context.Request.UserHostAddress);
            logger.Trace(GetPostRequestAsString());
            logger.Trace(Resources.Settings.Divider);

            if (Context.Request.HttpMethod == "POST" && AcceptConnection())
            {
                AuthorizeNet.SIMResponse authNetResponse = new AuthorizeNet.SIMResponse(Context.Request.Params);
                bool isValid = IsValidAuthorizeNet(authNetResponse);

                if (Context.Request.Params["x_subscription_id"] != null && isValid)
                {//then this is subscription update response 
                    UserInfoModel userInfo = UserInfoModel.GetUserInfo(Context.Request.Params["x_subscription_id"]);

                    if (userInfo != null) //if there is no user, then this is most probable is malformed message
                    {
                        if (AuthorizeNetPaymentProcessor.IsCompleted(Context.Request.Params["x_response_code"]))
                        {//the transaction was successful
                            try
                            {
                                ExtendSubscription(Context.Request.Params["x_subscription_id"], Context.Request.Params["x_trans_id"], userInfo, HawkBilling.Core.Type.PaymentProcessors.AuthorizeNet);
                            }
                            catch (Exception ex)
                            {
                                logger.Debug(String.Format(Resources.Settings.ExceptionMessage, this.GetType().Name, ex.Message));
                                logger.Debug(GetPostRequestAsString());
                                logger.Debug(Resources.Settings.Divider);

                                //send notification about error during processing
                                Email emailData = EmailComposer.InitGeneralinfo(ConfigurationManager.AppSettings["paymentErrorNotificationEmail"], EmailManagement.EmailResources.AutochargeProcessingErrorSubj);
                                EmailManager.SendAdminAutochargeProcessingError(userInfo, Context.Request.Params["x_trans_id"], Context.Request.Params["x_subscription_id"], emailData);
                            }
                        }
                        else
                        {//log transaction result information
                            string errorInfo = String.Format("{0}.{1}.{2}:{3}", Context.Request.Params["x_response_code"], Context.Request.Params["x_response_subcode"], Context.Request.Params["x_response_reason_code"], Context.Request.Params["x_response_reason_text"]);
                            logger.Error(errorInfo);
                            logger.Error(Resources.Settings.Divider);

                            //send notification about failed transaction
                            Email emailData = EmailComposer.InitGeneralinfo(ConfigurationManager.AppSettings["paymentErrorNotificationEmail"], EmailManagement.EmailResources.AutochargeTransactionErrorSubj);
                            EmailManager.SendAdminAutochargeTransactionError(userInfo, errorInfo, Context.Request.Params["x_subscription_id"], emailData);
                        }
                    }
                }
            }
        }

        [WebMethod]
        public void PayPalIPNTransactionInfo()
        {
            byte[] parameters = HttpContext.Current.Request.BinaryRead(HttpContext.Current.Request.ContentLength);
            ipn = new IPNMessage(parameters);

            logger.Trace("Request Host: " + Context.Request.UserHostAddress);
            logger.Trace(GetPaypalRequestAsString(ipn));
            logger.Trace(Resources.Settings.Divider);

            if (parameters.Length > 0 && AcceptConnection())
            {
                bool isIpnValidated = IsValidPaypal(ipn);

                if (isIpnValidated && ipn.IpnMap["recurring_payment_id"] != null)
                {//the request has been sent from PayPal and can be trusted and this is notification about recurring payments
                    UserInfoModel userInfo = UserInfoModel.GetUserInfo(ipn.IpnMap["recurring_payment_id"]);

                    if (userInfo != null) //if there is no user, then this is most probable is malformed message
                    {
                        switch (ipn.IpnMap["txn_type"])
                        {
                            case "recurring_payment":
                                //check the payment_status is Completed
                                if (PaypalPaymentProcessor.IsCompleted(ipn.IpnMap["payment_status"]) && !AccountManagement.CheckIfTransProcessed(ipn.IpnMap["txn_id"])) // TODO determine later whether to include "Processed" status
                                {
                                    try
                                    {
                                        ExtendSubscription(ipn.IpnMap["recurring_payment_id"], ipn.IpnMap["txn_id"], userInfo, HawkBilling.Core.Type.PaymentProcessors.PayPal);
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Debug(String.Format(Resources.Settings.ExceptionMessage, this.GetType().Name, ex.Message));
                                        logger.Debug(GetPaypalRequestAsString(ipn));
                                        logger.Debug(Resources.Settings.Divider);

                                        //send notification about error during processing
                                        Email emailData = EmailComposer.InitGeneralinfo(ConfigurationManager.AppSettings["paymentErrorNotificationEmail"], EmailManagement.EmailResources.AutochargeProcessingErrorSubj);
                                        EmailManager.SendAdminAutochargeProcessingError(userInfo, ipn.IpnMap["recurring_payment_id"], ipn.IpnMap["txn_id"], emailData);
                                    }
                                }
                                else
                                {//log transaction result information
                                    ProcessTransactionFailurePaypal(ipn, userInfo);
                                }
                                break;
                            case "recurring_payment_profile_cancel":
                                if (!AccountManagement.DeleteSubscriptionRecord(ipn.IpnMap["recurring_payment_id"]))
                                {
                                    logger.Error(GetPaypalRequestAsString(ipn));
                                    logger.Error(Resources.Settings.Divider);
                                }
                                break;
                        }
                    }
                }
            }
        }

        protected string GetPostRequestAsString()
        {
            using (TextReader reader = new StreamReader(Context.Request.InputStream))
            {
                return reader.ReadToEnd();
            }
        }

        protected string GetPaypalRequestAsString(IPNMessage ipn)
        {
            string parameters = String.Join("&", ipn.IpnMap.AllKeys.Select(key => key + "=" + ipn.IpnMap[key]));
            return parameters;
        }

        protected bool AcceptConnection()
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            bool requireSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["requireSSL"]);
            bool acceptConnection = false;
            if (testMode)
            {
                acceptConnection = true;
            }
            else
            {
                if (requireSSL == true)
                {//accept connection only via secure channel
                    acceptConnection = this.Context.Request.IsSecureConnection; //accept connection only via secure channel
                }
                else
                {//accept any connection if SSL is not required
                    acceptConnection = true;
                }
            }
            return acceptConnection;
        }

        protected bool IsValidAuthorizeNet(AuthorizeNet.SIMResponse authNetResponse)
        {
            bool isValid = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            if (!isValid)
            {
                //as we validate SilentPostRequest we onlt need a MD5-Hash value (aka secret key)
                isValid = authNetResponse.Validate(ConfigurationManager.AppSettings["authorizeNetSecretKey"], String.Empty);
            }
            return isValid;
        }

        protected bool IsValidPaypal(IPNMessage ipn)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            bool isValid = false;
            if (testMode)
            {
                isValid = true;
            }
            else
            {
                isValid = ipn.Validate();
            }
            return isValid;
        }

        private void ExtendSubscription(string subscriptionId, string transactionId, UserInfoModel userInfo, HawkBilling.Core.Type.PaymentProcessors processorType)
        {
            string comment = PaymentHistoryItemModel.BuildCommentString(true, processorType);

            if (!AccountManagement.ExtendSubscription(subscriptionId, transactionId, comment))
            {
                switch (processorType)
                {
                    case HawkBilling.Core.Type.PaymentProcessors.AuthorizeNet:
                        logger.Error(GetPostRequestAsString());
                        break;
                    case HawkBilling.Core.Type.PaymentProcessors.PayPal:
                        logger.Error(GetPaypalRequestAsString(ipn));
                        break;
                }
                logger.Error(Resources.Settings.Divider);

                //send notification to admin about error during processing automatic payment
                Email emailData = EmailComposer.InitGeneralinfo(ConfigurationManager.AppSettings["paymentErrorNotificationEmail"], EmailManagement.EmailResources.AutochargeProcessingErrorSubj);
                EmailManager.SendAdminAutochargeProcessingError(userInfo, transactionId, subscriptionId, emailData);
            }
        }

        private void ProcessTransactionFailurePaypal(IPNMessage ipn, UserInfoModel userInfo)
        {
            string errorInfo = String.Empty;
            if (PaypalPaymentProcessor.IsPending(ipn.IpnMap["payment_status"]))
            {
                errorInfo = String.Format("{0}:{1}", ipn.IpnMap["payment_status"], ipn.IpnMap["pending_reason"]);
            }
            else if (PaypalPaymentProcessor.IsReversed(ipn.IpnMap["payment_status"]) || PaypalPaymentProcessor.IsRefunded(ipn.IpnMap["payment_status"]) ||
                     PaypalPaymentProcessor.IsCancelledReversal(ipn.IpnMap["payment_status"]) || PaypalPaymentProcessor.IsDenied(ipn.IpnMap["payment_status"]))
            {
                errorInfo = String.Format("Status:{0}, Reason:{1}", ipn.IpnMap["payment_status"], ipn.IpnMap["reason_code"]);
            }
            logger.Trace(errorInfo);
            logger.Trace(GetPostRequestAsString());
            logger.Trace(Resources.Settings.Divider);

            //send notification about failed transaction
            Email emailData = EmailComposer.InitGeneralinfo(ConfigurationManager.AppSettings["paymentErrorNotificationEmail"], EmailManagement.EmailResources.AutochargeTransactionErrorSubj);
            EmailManager.SendAdminAutochargeTransactionError(userInfo, errorInfo, ipn.IpnMap["recurring_payment_id"], emailData);
        }
    }
}
