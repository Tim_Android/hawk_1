﻿using Domain.Entities.Abstract;
using System.Web.UI.WebControls;

namespace Domain.Interfaces
{
    public interface IGridDisplayable
    {
        void BindDataLog(SearchCondition searchCondition);
        GridView GridDataLog { get; set; }
    }
}
