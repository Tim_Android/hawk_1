﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IAccountInfo
    {
        UserInfo? Account { get; }
        bool IsAuthenticated { get; }
        void InitializeAccountInfo();
        void Logout();
    }
}
