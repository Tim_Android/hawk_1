﻿using Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IReport
    {
        byte[] ToByteArray();
        string FileName { get; }
        string FileExtension { get; }
        
    }
}
