﻿using Domain.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Domain.Interfaces
{
    public interface IListViewDisplayable
    {
        void BindDataListViewLog(SearchCondition searchCondition);
        ListView ListViewDataLog { get; set; }
    }

}
