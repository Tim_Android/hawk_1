/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2005                    */
/* Created on:     04/03/14 1:57:45 PM                          */
/*==============================================================*/


if exists (select 1
          from sysobjects
          where  id = object_id('ACTIVATE_DEVICE')
          and type in ('P','PC'))
   drop procedure ACTIVATE_DEVICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_ACTUAL_AGENT_ID')
          and type in ('P','PC'))
   drop procedure GET_ACTUAL_AGENT_ID
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_AGENT')
          and type in ('P','PC'))
   drop procedure ADD_AGENT
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_CALL')
          and type in ('P','PC'))
   drop procedure ADD_CALL
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_COORDINATE')
          and type in ('P','PC'))
   drop procedure ADD_COORDINATE
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_CRASH_LOG')
          and type in ('P','PC'))
   drop procedure ADD_CRASH_LOG
go

if exists (select 1
          from sysobjects
          where  id = object_id('FINALIZE_AUTO_PAYMENT')
          and type in ('P','PC'))
   drop procedure FINALIZE_AUTO_PAYMENT
go

if exists (select 1
          from sysobjects
          where  id = object_id('FINALIZE_PAYMENT')
          and type in ('P','PC'))
   drop procedure FINALIZE_PAYMENT
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_LICENSE')
          and type in ('P','PC'))
   drop procedure ADD_LICENSE
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_MMS')
          and type in ('P','PC'))
   drop procedure ADD_MMS
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_MMS_ATTACH')
          and type in ('P','PC'))
   drop procedure ADD_MMS_ATTACH
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_MMS_ATTACH_TEMPL')
          and type in ('P','PC'))
   drop procedure ADD_MMS_ATTACH_TEMPL
go

if exists (select 1
          from sysobjects
          where  id = object_id('INIT_PRICE_LIST')
          and type in ('P','PC'))
   drop procedure INIT_PRICE_LIST
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_NEW_PRICE')
          and type in ('P','PC'))
   drop procedure ADD_NEW_PRICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_NEW_PRICE_LIST_ITEM')
          and type in ('P','PC'))
   drop procedure ADD_NEW_PRICE_LIST_ITEM
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_SMS')
          and type in ('P','PC'))
   drop procedure ADD_SMS
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_TOKEN')
          and type in ('P','PC'))
   drop procedure ADD_TOKEN
go

if exists (select 1
          from sysobjects
          where  id = object_id('ADD_USER_INFO')
          and type in ('P','PC'))
   drop procedure ADD_USER_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('CALCULATE_PAYMENT')
          and type in ('P','PC'))
   drop procedure CALCULATE_PAYMENT
go

if exists (select 1
          from sysobjects
          where  id = object_id('CLEAN_CRASH_LOGS')
          and type in ('P','PC'))
   drop procedure CLEAN_CRASH_LOGS
go

if exists (select 1
          from sysobjects
          where  id = object_id('CLEAN_TOKENS')
          and type in ('P','PC'))
   drop procedure CLEAN_TOKENS
go

if exists (select 1
          from sysobjects
          where  id = object_id('CLEAN_USERS_DATA')
          and type in ('P','PC'))
   drop procedure CLEAN_USERS_DATA
go

if exists (select 1
          from sysobjects
          where  id = object_id('DEACTIVATE_DEVICE')
          and type in ('P','PC'))
   drop procedure DEACTIVATE_DEVICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('DEACTIVATE_LICENSE')
          and type in ('P','PC'))
   drop procedure DEACTIVATE_LICENSE
go

if exists (select 1
          from sysobjects
          where  id = object_id('DELETE_USER_INFO')
          and type in ('P','PC'))
   drop procedure DELETE_USER_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('DELETE_DEVICE')
          and type in ('P','PC'))
   drop procedure DELETE_DEVICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('DELETE_CALLS')
          and type in ('P','PC'))
   drop procedure DELETE_CALLS
go

if exists (select 1
          from sysobjects
          where  id = object_id('DELETE_TOKEN')
          and type in ('P','PC'))
   drop procedure DELETE_TOKEN
go

if exists (select 1
          from sysobjects
          where  id = object_id('DELETE_UNUSED_PHONES')
          and type in ('P','PC'))
   drop procedure DELETE_UNUSED_PHONES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_AGENTS_BY_GUID')
          and type in ('P','PC'))
   drop procedure GET_AGENTS_BY_GUID
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_AGENTS_COUNT')
          and type in ('P','PC'))
   drop procedure GET_AGENTS_COUNT
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_AGENTS_COUNT_BY_GUID')
          and type in ('P','PC'))
   drop procedure GET_AGENTS_COUNT_BY_GUID
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_AGENTS_LAST_UPDATE_TIME')
          and type in ('P','PC'))
   drop procedure GET_AGENTS_LAST_UPDATE_TIME
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_ATTACH')
          and type in ('P','PC'))
   drop procedure GET_ATTACH
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_CALLS')
          and type in ('P','PC'))
   drop procedure GET_CALLS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_COORDINATES')
          and type in ('P','PC'))
   drop procedure GET_COORDINATES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_COST_ADDING_DEVICE')
          and type in ('P','PC'))
   drop procedure GET_COST_ADDING_DEVICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_CRASH_LOGS')
          and type in ('P','PC'))
   drop procedure GET_CRASH_LOGS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICES')
          and type in ('P','PC'))
   drop procedure GET_DEVICES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure GET_DEVICE_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_SMS')
          and type in ('P','PC'))
   drop procedure GET_SMS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_MMS')
          and type in ('P','PC'))
   drop procedure GET_MMS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICE_TIME_ZONE_OFFSET')
          and type in ('P','PC'))
   drop procedure GET_DEVICE_TIME_ZONE_OFFSET
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_EMAILS')
          and type in ('P','PC'))
   drop procedure GET_EMAILS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_EXPIRED_ACCOUNTS')
          and type in ('P','PC'))
   drop procedure GET_EXPIRED_ACCOUNTS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_LICENSE')
          and type in ('P','PC'))
   drop procedure GET_LICENSE
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_LICENSES_FOR_EXPIRATION')
          and type in ('P','PC'))
   drop procedure GET_LICENSES_FOR_EXPIRATION
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_MMS_ATTACH')
          and type in ('P','PC'))
   drop procedure GET_MMS_ATTACH
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_MSG_TYPES')
          and type in ('P','PC'))
   drop procedure GET_MSG_TYPES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_NEW_SUBSCR_END')
          and type in ('P','PC'))
   drop procedure GET_NEW_SUBSCR_END
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_NOT_ACTIVATED_ACCOUNTS')
          and type in ('P','PC'))
   drop procedure GET_NOT_ACTIVATED_ACCOUNTS
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_NUMBER_TYPE_ID')
          and type in ('P','PC'))
   drop procedure GET_NUMBER_TYPE_ID
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_PAYMENTS_HISTORY')
          and type in ('P','PC'))
   drop procedure GET_PAYMENTS_HISTORY
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_PHONE_ID')
          and type in ('P','PC'))
   drop procedure GET_PHONE_ID
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_PRICE_LIST')
          and type in ('P','PC'))
   drop procedure GET_PRICE_LIST
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_PRICE_LIST_ITEM')
          and type in ('P','PC'))
   drop procedure GET_PRICE_LIST_ITEM
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_TOKEN_INFO')
          and type in ('P','PC'))
   drop procedure GET_TOKEN_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_UNSUBSCR_LICENSES')
          and type in ('P','PC'))
   drop procedure GET_UNSUBSCR_LICENSES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_USERS_LICENSES')
          and type in ('P','PC'))
   drop procedure GET_USERS_LICENSES
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_USER_ID_BY_GUID')
          and type in ('P','PC'))
   drop procedure GET_USER_ID_BY_GUID
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_USER_INFO')
          and type in ('P','PC'))
   drop procedure GET_USER_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('IS_ACTIVATION_ALLOWED')
          and type in ('P','PC'))
   drop procedure IS_ACTIVATION_ALLOWED
go

if exists (select 1
          from sysobjects
          where  id = object_id('IS_DEACTIVATION_ALLOWED')
          and type in ('P','PC'))
   drop procedure IS_DEACTIVATION_ALLOWED
go

if exists (select 1
          from sysobjects
          where  id = object_id('IS_LICENSED_DEVICE')
          and type in ('P','PC'))
   drop procedure IS_LICENSED_DEVICE
go

if exists (select 1
          from sysobjects
          where  id = object_id('IS_TRANSACTION_PROCESSED')
          and type in ('P','PC'))
   drop procedure IS_TRANSACTION_PROCESSED
go

if exists (select 1
          from sysobjects
          where  id = object_id('REFRESGH_AGENTS_LAST_UPDATE_TIME')
          and type in ('P','PC'))
   drop procedure REFRESGH_AGENTS_LAST_UPDATE_TIME
go

if exists (select 1
          from sysobjects
          where  id = object_id('SET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure SET_DEVICE_INFO
go

if exists (select 1
          from sysobjects
          where  id = object_id('SET_LICENSE_SUBSCR_ID')
          and type in ('P','PC'))
   drop procedure SET_LICENSE_SUBSCR_ID
go

if exists (select 1
          from sysobjects
          where  id = object_id('UPDATE_EMAIL_CONTENT')
          and type in ('P','PC'))
   drop procedure UPDATE_EMAIL_CONTENT
go

if exists (select 1
          from sysobjects
          where  id = object_id('UPDATE_USER_INFO')
          and type in ('P','PC'))
   drop procedure UPDATE_USER_INFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CALLS') and o.name = 'FK_Calls_Agents')
alter table CALLS
   drop constraint FK_Calls_Agents
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CALLS') and o.name = 'FK_CALLS_FROM_NUM_TYPE')
alter table CALLS
   drop constraint FK_CALLS_FROM_NUM_TYPE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CALLS') and o.name = 'FK_CALLS_TO_NUM_TYPE')
alter table CALLS
   drop constraint FK_CALLS_TO_NUM_TYPE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CALLS') and o.name = 'FK_CALLS_FROM_PHONE')
alter table CALLS
   drop constraint FK_CALLS_FROM_PHONE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CALLS') and o.name = 'FK_CALLS_TO_PHONE')
alter table CALLS
   drop constraint FK_CALLS_TO_PHONE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COORDINATES') and o.name = 'FK_COORDINA_REFERENCE_DEVICES')
alter table COORDINATES
   drop constraint FK_COORDINA_REFERENCE_DEVICES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CRASH_LOGS') and o.name = 'FK_AgentErrorLog_Agent')
alter table CRASH_LOGS
   drop constraint FK_AgentErrorLog_Agent
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DEVICES') and o.name = 'FK_Agents_Users')
alter table DEVICES
   drop constraint FK_Agents_Users
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DEVICES') and o.name = 'FK_DEVICES_FK_DEVS_L_LICENSES')
alter table DEVICES
   drop constraint FK_DEVICES_FK_DEVS_L_LICENSES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LICENSES') and o.name = 'FK_LICENSES_FK_LICS_P_PRICE_LI')
alter table LICENSES
   drop constraint FK_LICENSES_FK_LICS_P_PRICE_LI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LICENSES') and o.name = 'FK_LICENSES_REFERENCE_USERS_IN')
alter table LICENSES
   drop constraint FK_LICENSES_REFERENCE_USERS_IN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MESSAGES') and o.name = 'FK_MESSAGES_FK_SMS_AG_DEVICES')
alter table MESSAGES
   drop constraint FK_MESSAGES_FK_SMS_AG_DEVICES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MESSAGES') and o.name = 'FK_MESSAGES_FK_SMS_TY_MSG_TYPE')
alter table MESSAGES
   drop constraint FK_MESSAGES_FK_SMS_TY_MSG_TYPE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MMS_ATTACH') and o.name = 'FK_MMS_ATTA_FK_MMS_AT_MESSAGES')
alter table MMS_ATTACH
   drop constraint FK_MMS_ATTA_FK_MMS_AT_MESSAGES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MMS_ATTACH') and o.name = 'FK_MMS_ATTA_REFERENCE_MIME_TYP')
alter table MMS_ATTACH
   drop constraint FK_MMS_ATTA_REFERENCE_MIME_TYP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PAYMENTS') and o.name = 'FK_PAYMENTS_FK_DEVS_P_LICENSES')
alter table PAYMENTS
   drop constraint FK_PAYMENTS_FK_DEVS_P_LICENSES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PAYMENTS') and o.name = 'FK_PAYMENTS_FK_PRICE__PRICE_LI')
alter table PAYMENTS
   drop constraint FK_PAYMENTS_FK_PRICE__PRICE_LI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRICE_HISTORY') and o.name = 'FK_PRICE_HI_FK_PRICE__PRICE_LI')
alter table PRICE_HISTORY
   drop constraint FK_PRICE_HI_FK_PRICE__PRICE_LI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ACTIVE_PRICE_LIST')
            and   type = 'V')
   drop view ACTIVE_PRICE_LIST
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PAYMENTS_HISTORY')
            and   type = 'V')
   drop view PAYMENTS_HISTORY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SMS')
            and   type = 'V')
   drop view SMS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CALLS')
            and   name  = 'IND_CALL_FROM_PHONE'
            and   indid > 0
            and   indid < 255)
   drop index CALLS.IND_CALL_FROM_PHONE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CALLS')
            and   name  = 'IND_CALL_TO_PHONE'
            and   indid > 0
            and   indid < 255)
   drop index CALLS.IND_CALL_TO_PHONE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CALLS')
            and   name  = 'IND_CALL_AGENT_ID'
            and   indid > 0
            and   indid < 255)
   drop index CALLS.IND_CALL_AGENT_ID
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CALLS')
            and   type = 'U')
   drop table CALLS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COORDINATES')
            and   name  = 'IND_GPS_ID_AGENT'
            and   indid > 0
            and   indid < 255)
   drop index COORDINATES.IND_GPS_ID_AGENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COORDINATES')
            and   type = 'U')
   drop table COORDINATES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CRASH_LOGS')
            and   name  = 'IND_CRASH_LOG_DATE'
            and   indid > 0
            and   indid < 255)
   drop index CRASH_LOGS.IND_CRASH_LOG_DATE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CRASH_LOGS')
            and   type = 'U')
   drop table CRASH_LOGS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DEVICES')
            and   name  = 'IND_IMEI'
            and   indid > 0
            and   indid < 255)
   drop index DEVICES.IND_IMEI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DEVICES')
            and   type = 'U')
   drop table DEVICES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EMAIL')
            and   type = 'U')
   drop table EMAIL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LICENSES')
            and   name  = 'IND_LIC_SUBSCR_ID'
            and   indid > 0
            and   indid < 255)
   drop index LICENSES.IND_LIC_SUBSCR_ID
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LICENSES')
            and   type = 'U')
   drop table LICENSES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MESSAGES')
            and   name  = 'IND_SMS_ID_AGENT'
            and   indid > 0
            and   indid < 255)
   drop index MESSAGES.IND_SMS_ID_AGENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MESSAGES')
            and   type = 'U')
   drop table MESSAGES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MIME_TYPES')
            and   name  = 'IND_MIME_TYPES_NAME'
            and   indid > 0
            and   indid < 255)
   drop index MIME_TYPES.IND_MIME_TYPES_NAME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MIME_TYPES')
            and   type = 'U')
   drop table MIME_TYPES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MMS_ATTACH')
            and   name  = 'IND_MMS'
            and   indid > 0
            and   indid < 255)
   drop index MMS_ATTACH.IND_MMS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MMS_ATTACH')
            and   type = 'U')
   drop table MMS_ATTACH
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MSG_TYPES')
            and   name  = 'IND_TYPE_NAME'
            and   indid > 0
            and   indid < 255)
   drop index MSG_TYPES.IND_TYPE_NAME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MSG_TYPES')
            and   type = 'U')
   drop table MSG_TYPES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('NUMBERS_TYPES')
            and   name  = 'IND_NUM_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index NUMBERS_TYPES.IND_NUM_TYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('NUMBERS_TYPES')
            and   type = 'U')
   drop table NUMBERS_TYPES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PAYMENTS')
            and   type = 'U')
   drop table PAYMENTS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PHONES')
            and   name  = 'IND_PHONE_NUM_NAME'
            and   indid > 0
            and   indid < 255)
   drop index PHONES.IND_PHONE_NUM_NAME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PHONES')
            and   type = 'U')
   drop table PHONES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRICE_HISTORY')
            and   type = 'U')
   drop table PRICE_HISTORY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRICE_LIST')
            and   type = 'U')
   drop table PRICE_LIST
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TOKENS')
            and   name  = 'IND_TOKENS_TOKEN'
            and   indid > 0
            and   indid < 255)
   drop index TOKENS.IND_TOKENS_TOKEN
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TOKENS')
            and   type = 'U')
   drop table TOKENS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('USERS_INFO')
            and   type = 'U')
   drop table USERS_INFO
go

if exists(select 1 from systypes where name='ADDRESS_TYPE')
   drop type ADDRESS_TYPE
go

if exists(select 1 from systypes where name='AGENT_NAME_TYPE')
   drop type AGENT_NAME_TYPE
go

if exists(select 1 from systypes where name='ATTACH_DATA_TYPE')
   drop type ATTACH_DATA_TYPE
go

if exists(select 1 from systypes where name='BRAND_TYPE')
   drop type BRAND_TYPE
go

if exists(select 1 from systypes where name='CALL_TYPE')
   drop type CALL_TYPE
go

if exists(select 1 from systypes where name='COMMENT_TYPE')
   drop type COMMENT_TYPE
go

if exists(select 1 from systypes where name='CRASH_DATA_TYPE')
   drop type CRASH_DATA_TYPE
go

if exists(select 1 from systypes where name='DEV_NAME_TYPE')
   drop type DEV_NAME_TYPE
go

if exists(select 1 from systypes where name='EMAIL_CONTENT_TYPE')
   drop type EMAIL_CONTENT_TYPE
go

if exists(select 1 from systypes where name='EMAIL_TYPE')
   drop type EMAIL_TYPE
go

if exists(select 1 from systypes where name='FIRMWARE_ID_TYPE')
   drop type FIRMWARE_ID_TYPE
go

if exists(select 1 from systypes where name='IMEI_TYPE')
   drop type IMEI_TYPE
go

if exists(select 1 from systypes where name='MODEL_TYPE')
   drop type MODEL_TYPE
go

if exists(select 1 from systypes where name='MSG_BODY_TYPE')
   drop type MSG_BODY_TYPE
go

if exists(select 1 from systypes where name='MSG_SUBJ_TYPE')
   drop type MSG_SUBJ_TYPE
go

if exists(select 1 from systypes where name='NAME_TYPE')
   drop type NAME_TYPE
go

if exists(select 1 from systypes where name='NOTE_TYPE')
   drop type NOTE_TYPE
go

if exists(select 1 from systypes where name='NUM_NAME_TYPE')
   drop type NUM_NAME_TYPE
go

if exists(select 1 from systypes where name='OS_TYPE')
   drop type OS_TYPE
go

if exists(select 1 from systypes where name='PHONE_TYPE')
   drop type PHONE_TYPE
go

if exists(select 1 from systypes where name='PNAME_TYPE')
   drop type PNAME_TYPE
go

if exists(select 1 from systypes where name='PROVIDER_TYPE')
   drop type PROVIDER_TYPE
go

if exists(select 1 from systypes where name='SDK_TYPE')
   drop type SDK_TYPE
go

if exists(select 1 from systypes where name='SUBSCR_ID_TYPE')
   drop type SUBSCR_ID_TYPE
go

if exists(select 1 from systypes where name='SVN_TYPE')
   drop type SVN_TYPE
go

if exists(select 1 from systypes where name='TIME_ZONE_TYPE')
   drop type TIME_ZONE_TYPE
go

/*==============================================================*/
/* Domain: ADDRESS_TYPE                                         */
/*==============================================================*/
create type ADDRESS_TYPE
   from nvarchar(500)
go

/*==============================================================*/
/* Domain: AGENT_NAME_TYPE                                      */
/*==============================================================*/
create type AGENT_NAME_TYPE
   from nvarchar(50)
go

/*==============================================================*/
/* Domain: ATTACH_DATA_TYPE                                     */
/*==============================================================*/
create type ATTACH_DATA_TYPE
   from image
go

/*==============================================================*/
/* Domain: BRAND_TYPE                                           */
/*==============================================================*/
create type BRAND_TYPE
   from nchar(30)
go

/*==============================================================*/
/* Domain: CALL_TYPE                                            */
/*==============================================================*/
create type CALL_TYPE
   from smallint
go

/*==============================================================*/
/* Domain: COMMENT_TYPE                                         */
/*==============================================================*/
create type COMMENT_TYPE
   from nvarchar(1000)
go

/*==============================================================*/
/* Domain: CRASH_DATA_TYPE                                      */
/*==============================================================*/
create type CRASH_DATA_TYPE
   from nvarchar(4000)
go

/*==============================================================*/
/* Domain: DEV_NAME_TYPE                                        */
/*==============================================================*/
create type DEV_NAME_TYPE
   from nchar(20)
go

/*==============================================================*/
/* Domain: EMAIL_CONTENT_TYPE                                   */
/*==============================================================*/
create type EMAIL_CONTENT_TYPE
   from nvarchar(4000)
go

/*==============================================================*/
/* Domain: EMAIL_TYPE                                           */
/*==============================================================*/
create type EMAIL_TYPE
   from nvarchar(256) not null
go

/*==============================================================*/
/* Domain: FIRMWARE_ID_TYPE                                     */
/*==============================================================*/
create type FIRMWARE_ID_TYPE
   from nvarchar(100)
go

/*==============================================================*/
/* Domain: IMEI_TYPE                                            */
/*==============================================================*/
create type IMEI_TYPE
   from nchar(15)
go

/*==============================================================*/
/* Domain: MODEL_TYPE                                           */
/*==============================================================*/
create type MODEL_TYPE
   from nvarchar(50)
go

/*==============================================================*/
/* Domain: MSG_BODY_TYPE                                        */
/*==============================================================*/
create type MSG_BODY_TYPE
   from nvarchar(4000)
go

/*==============================================================*/
/* Domain: MSG_SUBJ_TYPE                                        */
/*==============================================================*/
create type MSG_SUBJ_TYPE
   from nchar(50)
go

/*==============================================================*/
/* Domain: NAME_TYPE                                            */
/*==============================================================*/
create type NAME_TYPE
   from nvarchar(50)
go

/*==============================================================*/
/* Domain: NOTE_TYPE                                            */
/*==============================================================*/
create type NOTE_TYPE
   from nvarchar(500)
go

/*==============================================================*/
/* Domain: NUM_NAME_TYPE                                        */
/*==============================================================*/
create type NUM_NAME_TYPE
   from nchar(15)
go

/*==============================================================*/
/* Domain: OS_TYPE                                              */
/*==============================================================*/
create type OS_TYPE
   from nchar(20)
go

/*==============================================================*/
/* Domain: PHONE_TYPE                                           */
/*==============================================================*/
create type PHONE_TYPE
   from nchar(20)
go

/*==============================================================*/
/* Domain: PNAME_TYPE                                           */
/*==============================================================*/
create type PNAME_TYPE
   from nvarchar(50)
go

/*==============================================================*/
/* Domain: PROVIDER_TYPE                                        */
/*==============================================================*/
create type PROVIDER_TYPE
   from int
go

/*==============================================================*/
/* Domain: SDK_TYPE                                             */
/*==============================================================*/
create type SDK_TYPE
   from nchar(20)
go

/*==============================================================*/
/* Domain: SUBSCR_ID_TYPE                                       */
/*==============================================================*/
create type SUBSCR_ID_TYPE
   from nvarchar(32)
go

/*==============================================================*/
/* Domain: SVN_TYPE                                             */
/*==============================================================*/
create type SVN_TYPE
   from nchar(20)
go

/*==============================================================*/
/* Domain: TIME_ZONE_TYPE                                       */
/*==============================================================*/
create type TIME_ZONE_TYPE
   from nvarchar(100)
go

/*==============================================================*/
/* Table: CALLS                                                 */
/*==============================================================*/
create table CALLS (
   ID                   int                  identity(1, 1),
   AGENT_ID             int                  not null,
   DURATION             int                  not null,
   DATE                 datetime             not null,
   CTYPE                CALL_TYPE            not null,
   FROM_PHONE_ID        int                  not null,
   TO_PHONE_ID          int                  not null,
   TO_NUM_TYPE_ID       int                  not null,
   FROM_NUM_TYPE_ID     int                  not null,
   constraint PK_Call primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Index: IND_CALL_AGENT_ID                                     */
/*==============================================================*/
create index IND_CALL_AGENT_ID on CALLS (
AGENT_ID DESC,
TO_PHONE_ID DESC,
TO_NUM_TYPE_ID ASC,
FROM_PHONE_ID DESC,
FROM_NUM_TYPE_ID ASC
)
go

/*==============================================================*/
/* Index: IND_CALL_TO_PHONE                                     */
/*==============================================================*/
create index IND_CALL_TO_PHONE on CALLS (
TO_PHONE_ID DESC
)
go

/*==============================================================*/
/* Index: IND_CALL_FROM_PHONE                                   */
/*==============================================================*/
create index IND_CALL_FROM_PHONE on CALLS (
FROM_PHONE_ID DESC
)
go

/*==============================================================*/
/* Table: COORDINATES                                           */
/*==============================================================*/
create table COORDINATES (
   ID                   int                  identity(1,1),
   AGENT_ID             int                  not null,
   LATITUDE             double precision     null,
   LONGITUDE            double precision     null,
   ALTITUDE             double precision     null,
   ACCURACY             float                null,
   BEARING              float                null,
   PROVIDER             PROVIDER_TYPE        null default 0,
   SPEED                float                null,
   ARRIVAL_TIME         datetime             null,
   constraint PK_COORDINATES primary key (ID)
)
go

/*==============================================================*/
/* Index: IND_GPS_ID_AGENT                                      */
/*==============================================================*/
create index IND_GPS_ID_AGENT on COORDINATES (
AGENT_ID DESC
)
go

/*==============================================================*/
/* Table: CRASH_LOGS                                            */
/*==============================================================*/
create table CRASH_LOGS (
   ID                   int                  identity(1, 1),
   AGENT_ID             int                  not null,
   CRASH_DATA           CRASH_DATA_TYPE      not null,
   CRASH_DATETIME       datetime             not null,
   constraint PK_AgentErrorLog primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Index: IND_CRASH_LOG_DATE                                    */
/*==============================================================*/
create index IND_CRASH_LOG_DATE on CRASH_LOGS (
AGENT_ID DESC,
CRASH_DATETIME DESC
)
go

/*==============================================================*/
/* Table: DEVICES                                               */
/*==============================================================*/
create table DEVICES (
   ID                   int                  identity(1, 1),
   USER_ID              int                  not null,
   AGENT_NAME           AGENT_NAME_TYPE      not null,
   IS_ACTIVED           bit                  not null,
   LAST_UPDATED         datetime             null,
   IMEI                 IMEI_TYPE            null,
   PHONE_NUMBER         PHONE_TYPE           null,
   BRAND                BRAND_TYPE           null,
   MODEL                MODEL_TYPE           null,
   OS                   OS_TYPE              null,
   SDK                  SDK_TYPE             null,
   FIRMWARE_ID          FIRMWARE_ID_TYPE     null,
   SVN_VERSION          SVN_TYPE             null,
   TIME_ZONE            TIME_ZONE_TYPE       null,
   TZ_OFFSET            int                  null default 0,
   DEVICE_NAME          DEV_NAME_TYPE        null,
   NOTE                 NOTE_TYPE            null,
   LIC_ID               int                  null,
   LIC_ACT_DATE         datetime             null,
   CREATION_DATE        datetime             not null,
   OPER_SYS_TYPE        int                  not null,
   constraint PK_Agent primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Index: IND_IMEI                                              */
/*==============================================================*/
create index IND_IMEI on DEVICES (
IMEI DESC
)
go

/*==============================================================*/
/* Table: EMAIL                                                 */
/*==============================================================*/
create table EMAIL (
   ID                   int                  identity(1, 1),
   EMAIL_NAME           nvarchar(64)         not null,
   EMAIL_TYPE           int                  not null,
   EMAIL_TEXT_HTML      EMAIL_CONTENT_TYPE   not null,
   ALLOWED_VARS         EMAIL_CONTENT_TYPE   null,
   constraint PK_EMAIL primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '1 - registration validation,
2 - registration validation confirm,
3 - registration,
4 - password reset confirmation,
5 - new password',
   'user', @CurrentUser, 'table', 'EMAIL', 'column', 'EMAIL_TYPE'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'This field contains list of variables that are available for this template',
   'user', @CurrentUser, 'table', 'EMAIL', 'column', 'ALLOWED_VARS'
go

SET IDENTITY_INSERT [dbo].[EMAIL] ON
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (1, N'Registration Validation                                         ', 1, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

<p>You or somebody impersonated as you created new account at @HawkSiteLink@ using this E-Mail address.<br />
Please follow the <a href="@ValidationLink@">link</a> to confirm your control over it.<br />
You will receive confirmation email shortly after visiting the link</p>

<p>If you haven&#39;t requested account creation - please ignore the message.</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
', N'@HawkSiteLink@|Link to the HAWK site||@ValidationLink@|Link to validate registration. Ensure that it is included in E-Mail text (or link) or otherwise user won''t be able to confirm his account||@FirstName@|User''s first name||@LastName@|User''s last name')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (2, N'Registration Validation Confirm                                 ', 2, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

<p>You have successfully validated your account E-Mail address.<br />
You are now able to enter your account</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (3, N'Registration                                                    ', 3, N'<p><span style="color:#000080"><strong>Hello, @FirstName@ @LastName@</strong></span></p>

<p>Thank you for your registration. Here are your account details:</p>

<table>
	<tbody>
		<tr>
			<td>Login:</td>
			<td>@Login@</td>
		</tr>
		<tr>
			<td>Password:</td>
			<td>@Password@</td>
		</tr>
	</tbody>
</table>

<p>You can use credentials above to log in the site.</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
', N'|Currently notification is not used')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (4, N'New Password                                                    ', 5, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@, </strong></span></p>

<p>Congratulations!<br />
You have successfully completed password reset procedure.<br />
Your new password is: @Password@</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
', N'@Password@|User''s new password||@FirstName@|User''s first name||@LastName@|User''s last name')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (5, N'Confirm Password Reset                                          ', 4, N'<p><span style="color:#000080"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>You or somebody impersonated as you requested to reset your HAWK account password.<br />
Your password reset link will expire in 24 hours from request submission.<br />
Please follow the <a href="@ResetLink@">link</a> to reset your password.<br />
You will receive email with your new password shortly after visiting the link</p>

<p>If you haven&#39;t requested password reset you can ignore the message.</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span><br />
<span style="color:#FF8C00"><strong>Paraben Support team</strong></span></p>
', N'@ResetLink@|Password reset link. Ensure that it is included in E-Mail text (or link) or otherwise user won''t be able to reset password||@FirstName@|User''s first name||@LastName@|User''s last name')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (6, N'Price Edit (Subscription cancel notice)', 6, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>Please note the subscription prices were changed. The auto-charge for the next time period cannot be completed for License #@LicId@. Please check the new prices and click on the Buy HAWK Subscription page and verify your acceptance of the price changes.</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|License Id')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (7, N'Autocharge Transaction Error (Admn''s)', 7, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear Administrator,</strong></span></p>

<p>Error occured during transaction processing on payment processor side. Here is an additional info:</p>

<table border="0" cellpadding="1" cellspacing="1">
	<tbody>
		<tr>
			<td>Error Information:</td>
			<td>@ErrorInfo@</td>
		</tr>
		<tr>
			<td>SubscriptionId: &nbsp;</td>
			<td>@SubscrId@</td>
		</tr>
	</tbody>
</table>

<p>Please pay attention to user&#39;s account: @Email@ (@LastName@, @FirstName@ ).&nbsp;Additional information is stored in site&#39;s log files</p>

<p>&nbsp;</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@Email@|User''s Email||@ErrorInfo@|Error details||@SubscrId@|Subscription Id')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (8, N'Autocharge Processing Error (Admin''s)', 8, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear Administrator,</strong></span></p>

<p>Error occured during transaction processing. Here is an additional info:</p>

<table border="0" cellpadding="1" cellspacing="1">
	<tbody>
		<tr>
			<td>TransactionId:</td>
			<td>@TransId@</td>
		</tr>
		<tr>
			<td>SubscriptionId: &nbsp;</td>
			<td>@SubscrId@</td>
		</tr>
	</tbody>
</table>

<p>Please pay attention to user&#39;s account &nbsp;@Email@ (@LastName@, @FirstName@ ). Additional information is stored in site&#39;s log files</p>

<p>&nbsp;</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span></p>

<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@Email@|User''s Email||@TransId@|Transaction Id||@SubscrId@|Subscription Id')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (11, N'Autocharge Fail (Admin''s)', 9, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>Auto-charge failed and your Subscription #@LicId@&nbsp;canot be updated automatically</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|User''s Subscription Id')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (12, N'License Expiration Notice', 10, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>One or more of your HAWK Subscriptions is about to expire. Please check the following subscriptions:</p>

<p>@LicensesList@</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicensesList@|List of licenses to check. Please note that list of licenses is rendered as unordered list')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (13, N'License Expired Notice', 11, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>Your Subscription #@LicId@&nbsp;has expired. Please update your subscription to continue using the HAWK Mobile Monitoring System.</p>

<p><span style="color:#000080"><strong>Thank you,</strong></span></p>

<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@LicId@|License Id')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (14, N'Account Deleted Notice', 12, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>This message confirms that your account at HAWK has been deleted.</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span><br />
<span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name')
INSERT [dbo].[EMAIL] ([ID], [EMAIL_NAME], [EMAIL_TYPE], [EMAIL_TEXT_HTML], [ALLOWED_VARS]) VALUES (16, N'Not Activated Account Notice', 13, N'<p><span style="color:rgb(0, 0, 128)"><strong>Dear @FirstName@ @LastName@,</strong></span></p>

<p>You have received this message, because you have signed up for HAWK Mobile Monitoring System. However, your account has not been activated yet. To activate your account, you must purchase a subscription within the next @AccountActivationDeadline@ days. Otherwise your account will be deleted and deactivated. &nbsp;</p>

<p><span style="color:rgb(0, 0, 128)"><strong>Thank you,</strong></span></p>

<p><span style="color:rgb(255, 140, 0)"><strong>Paraben Support team</strong></span></p>
', N'@FirstName@|User''s first name||@LastName@|User''s last name||@SignupFeeAmount@|Signup Fee cost||@AccountActivationDeadline@|Number of days the we wait for account activation prior deleting the account')
SET IDENTITY_INSERT [dbo].[EMAIL] OFF

/*==============================================================*/
/* Table: LICENSES                                              */
/*==============================================================*/
create table LICENSES (
   ID                   int                  identity(1, 1),
   USER_ID              int                  not null,
   PRICE_LIST_ID        int                  null,
   SUBSCR_END           datetime             null,
   DEV_CNT              smallint             not null default 0,
   PAY_SYS_TYPE         smallint             null,
   SUBSCR_ID            SUBSCR_ID_TYPE       null,
   CREATION_DATE        datetime             null,
   constraint PK_LICENSES primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'NULL if it wasn''t licensed',
   'user', @CurrentUser, 'table', 'LICENSES', 'column', 'SUBSCR_END'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Payment Systems:
    1 - AutorizeNet,
    2 - PayPal',
   'user', @CurrentUser, 'table', 'LICENSES', 'column', 'PAY_SYS_TYPE'
go

/*==============================================================*/
/* Index: IND_LIC_SUBSCR_ID                                     */
/*==============================================================*/
create index IND_LIC_SUBSCR_ID on LICENSES (
SUBSCR_ID ASC
)
go

/*==============================================================*/
/* Table: MESSAGES                                              */
/*==============================================================*/
create table MESSAGES (
   ID_MSG               int                  identity(1,1),
   AGENT_ID_MSG         int                  not null,
   TYPE_ID              int                  not null,
   MSG_ADDR             ADDRESS_TYPE         null,
   MSG_DT               datetime             null,
   MSG_BODY             MSG_BODY_TYPE        null,
   MSG_SUBJ             MSG_SUBJ_TYPE        null,
   constraint PK_MESSAGES primary key (ID_MSG)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sys.sp_addextendedproperty 'MS_Description', 
   'SMS and MMS messages',
   'user', @CurrentUser, 'table', 'MESSAGES'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Identity',
   'user', @CurrentUser, 'table', 'MESSAGES', 'column', 'ID_MSG'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Message subject (for MMS)',
   'user', @CurrentUser, 'table', 'MESSAGES', 'column', 'MSG_SUBJ'
go

/*==============================================================*/
/* Index: IND_SMS_ID_AGENT                                      */
/*==============================================================*/
create index IND_SMS_ID_AGENT on MESSAGES (
AGENT_ID_MSG DESC
)
go

/*==============================================================*/
/* Table: MIME_TYPES                                            */
/*==============================================================*/
create table MIME_TYPES (
   ID_MIME              int                  identity(1, 1),
   EXT                  nchar(10)            null,
   MIME_NAME            nchar(50)            not null,
   constraint PK_MIME_TYPES primary key (ID_MIME)
)
go

INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.asp', 'text/asp');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.au', 'audio/basic');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.avi', 'video/avi');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.bmp', 'image/bmp');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.boo', 'application/book');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.book', 'application/book');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.c', 'text/plain');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.c', 'text/x-c');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.c++', 'text/plain');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.cc', 'text/plain');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.cc', 'text/x-c');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.class', 'application/java');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.class', 'application/java-byte-code');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.class', 'application/x-java-class');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.css', 'application/x-pointplus');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.css', 'text/css');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.doc', 'application/msword');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.dot', 'application/msword');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.exe', 'application/octet-stream');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.gif', 'image/gif');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.h', 'text/plain');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.h', 'text/x-h');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.html', 'text/html');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ico', 'image/x-icon');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.java', 'text/plain');
INSERT INTO MIME_TYPES( EXT, MIME_NAME)
VALUES ('.java', 'text/x-java-source');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.jpg', 'image/jpeg');
INSERT INTO MIME_TYPES( EXT, MIME_NAME)
VALUES ('.jpg', 'image/pjpeg');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.js', 'application/x-javascript');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.m3u', 'audio/x-mpequrl');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'application/x-midi');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'audio/midi');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'audio/x-mid');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'audio/x-midi');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'music/crescendo');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mid', 'x-music/x-midi');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mp2', 'audio/mpeg');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mp2', 'video/x-mpeg');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mp3', 'audio/mpeg3');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mp3', 'audio/x-mpeg-3');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mpg', 'audio/mpeg');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.mpg', 'video/mpeg');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.pas', 'text/pascal');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.pic', 'image/pict');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.png', 'image/png');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.pps', 'application/vnd.ms-powerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ppt', 'application/mspowerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ppt', 'application/powerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ppt', 'application/vnd.ms-powerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ppt', 'application/x-mspowerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.ppz', 'application/mspowerpoint');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.qif', 'image/x-quicktime');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.qt', 'video/quicktime');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.rmi', 'audio/mid');
INSERT INTO MIME_TYPES( EXT, MIME_NAME)
VALUES ('.rtf', 'application/rtf');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.rtf', 'application/x-rtf');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.rtf', 'text/richtext');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.tif', 'image/tiff');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.tif', 'image/x-tiff');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.txt', 'text/plain');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.wav', 'audio/wav');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.wav', 'audio/x-wav');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xls', 'application/excel');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xls', 'application/vnd.ms-excel');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xls', 'application/x-excel');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xls', 'application/x-msexcel');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xml', 'application/xml');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.xml', 'text/xml');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.zip', 'application/x-compressed');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.zip', 'application/x-zip-compressed');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.zip', 'application/zip');
INSERT INTO MIME_TYPES(EXT, MIME_NAME)
VALUES ('.zip', 'multipart/x-zip');

/*==============================================================*/
/* Index: IND_MIME_TYPES_NAME                                   */
/*==============================================================*/
create index IND_MIME_TYPES_NAME on MIME_TYPES (
MIME_NAME ASC
)
go

/*==============================================================*/
/* Table: MMS_ATTACH                                            */
/*==============================================================*/
create table MMS_ATTACH (
   ID_ATTACH            int                  identity(1,1),
   MSG_ID               int                  null,
   ID_MIME              int                  null,
   ATTACH_DATA          ATTACH_DATA_TYPE     null,
   AGENT_ATTACH_ID      INT                  null,
   constraint PK_MMS_ATTACH primary key (ID_ATTACH)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sys.sp_addextendedproperty 'MS_Description', 
   'The MMS messages attachments',
   'user', @CurrentUser, 'table', 'MMS_ATTACH'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Identity',
   'user', @CurrentUser, 'table', 'MMS_ATTACH', 'column', 'MSG_ID'
go

/*==============================================================*/
/* Index: IND_MMS                                               */
/*==============================================================*/
create index IND_MMS on MMS_ATTACH (
MSG_ID DESC
)
go

/*==============================================================*/
/* Table: MSG_TYPES                                             */
/*==============================================================*/
create table MSG_TYPES (
   ID_TYPE              int                  identity(1,1),
   TYPE_NAME            nchar(10)            null,
   constraint PK_MSG_TYPES primary key (ID_TYPE)
)
go

INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Inbox');
INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Sent');
INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Draft');
INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Outbox');
INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Failed');
INSERT INTO MSG_TYPES(TYPE_NAME)
VALUES('Queued');
GO

/*==============================================================*/
/* Index: IND_TYPE_NAME                                         */
/*==============================================================*/
create index IND_TYPE_NAME on MSG_TYPES (
TYPE_NAME ASC
)
go

/*==============================================================*/
/* Table: NUMBERS_TYPES                                         */
/*==============================================================*/
create table NUMBERS_TYPES (
   ID                   int                  identity(1, 1),
   NUMBER_TYPE          NUM_NAME_TYPE        null,
   constraint PK_NUMBERS_TYPES primary key (ID)
)
go

/*==============================================================*/
/* Index: IND_NUM_TYPE                                          */
/*==============================================================*/
create index IND_NUM_TYPE on NUMBERS_TYPES (
NUMBER_TYPE ASC
)
go

/*==============================================================*/
/* Table: PAYMENTS                                              */
/*==============================================================*/
create table PAYMENTS (
   ID                   int                  identity(1, 1),
   LIC_ID               int                  not null,
   PRICE_LIST_ID        int                  not null,
   PAY_DATE             datetime             null,
   TRANS_ID             SUBSCR_ID_TYPE       not null,
   AMOUNT               money                not null,
   DEV_CNT              smallint             not null default 0,
   SUBSCR_END           datetime             null,
   PREV_SUBSCR_END      datetime             null,
   PAY_COMMENT          COMMENT_TYPE         null,
   constraint PK_PAYMENTS primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Summa',
   'user', @CurrentUser, 'table', 'PAYMENTS', 'column', 'AMOUNT'
go

/*==============================================================*/
/* Table: PHONES                                                */
/*==============================================================*/
create table PHONES (
   ID                   int                  identity(1, 1),
   NUMBER               PHONE_TYPE           null,
   PHONE_NAME           PNAME_TYPE           null,
   constraint PK_PHONES primary key (ID)
)
go

/*==============================================================*/
/* Index: IND_PHONE_NUM_NAME                                    */
/*==============================================================*/
create index IND_PHONE_NUM_NAME on PHONES (
NUMBER ASC
)
go

/*==============================================================*/
/* Table: PRICE_HISTORY                                         */
/*==============================================================*/
create table PRICE_HISTORY (
   PRICE_LIST_ID        int                  null,
   DATE_FROM            datetime             not null,
   DATE_TO              datetime             null,
   PRICE                money                not null,
   PRICE_OTHER          money                null
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Active Price: (DATE_TO is NULL)',
   'user', @CurrentUser, 'table', 'PRICE_HISTORY', 'column', 'DATE_TO'
go

/*==============================================================*/
/* Table: PRICE_LIST                                            */
/*==============================================================*/
create table PRICE_LIST (
   ID                   int                  identity(1, 1),
   LABEL                NAME_TYPE            null,
   DURATION             smallint             null,
   PAY_TYPE             smallint             not null,
   constraint PK_PRICE_LIST primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'in days',
   'user', @CurrentUser, 'table', 'PRICE_LIST', 'column', 'DURATION'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '1 - Fee (Signup Fee),
2 - Recurring (regular payment)',
   'user', @CurrentUser, 'table', 'PRICE_LIST', 'column', 'PAY_TYPE'
go

/*==============================================================*/
/* Table: TOKENS                                                */
/*==============================================================*/
create table TOKENS (
   EMAIL                EMAIL_TYPE           not null,
   TOKEN                VARCHAR(100)         null,
   EXPIRATION_DATE      DATETIME             not null default DATEADD(d, 1, GETUTCDATE())
)
go

/*==============================================================*/
/* Index: IND_TOKENS_TOKEN                                      */
/*==============================================================*/
create index IND_TOKENS_TOKEN on TOKENS (
TOKEN ASC
)
go

/*==============================================================*/
/* Table: USERS_INFO                                            */
/*==============================================================*/
create table USERS_INFO (
   ID                   int                  identity(1, 1),
   FIRST_NAME           NAME_TYPE            null,
   LAST_NAME            NAME_TYPE            null,
   ASP_NET_USER_ID      uniqueidentifier     null,
   EMAIL                EMAIL_TYPE           not null,
   constraint PK_User primary key (ID)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* View: ACTIVE_PRICE_LIST                                      */
/*==============================================================*/
create view ACTIVE_PRICE_LIST as
select L.ID, L.LABEL, L.DURATION, L.PAY_TYPE,
    H.PRICE, H.PRICE_OTHER, H.DATE_FROM
from PRICE_LIST L
    left join PRICE_HISTORY H on H.PRICE_LIST_ID = L.ID
where H.DATE_TO is null
go

/*==============================================================*/
/* View: PAYMENTS_HISTORY                                       */
/*==============================================================*/
create view PAYMENTS_HISTORY as
select P.ID as ID_PAYMENT, P.LIC_ID, P.PAY_DATE, PL.LABEL, P.DEV_CNT, PH.PRICE, PH.PRICE_OTHER, P.AMOUNT, 
    P.TRANS_ID, P.PREV_SUBSCR_END as START_DATE, P.SUBSCR_END as FINISH_DATE, P.PAY_COMMENT
from  PAYMENTS P
	left join PRICE_LIST PL on PL.ID = P.PRICE_LIST_ID
	left join PRICE_HISTORY PH on PH.PRICE_LIST_ID = PL.ID
where (PH.DATE_FROM <= P.PAY_DATE ) 
    and (( P.PAY_DATE <= PH.DATE_TO ) or ( PH.DATE_TO is null ))
go

/*==============================================================*/
/* View: SMS                                                    */
/*==============================================================*/
create view SMS  as
select M.AGENT_ID_MSG as AGENT_ID_SMS, 
       M.TYPE_ID,
       M.MSG_ADDR as SMS_ADDR,
       M.MSG_DT as SMS_DT,
       M.MSG_BODY as SMS_BODY
from MESSAGES M
where M.MSG_SUBJ is null 
    and not exists(select A.MSG_ID 
                   from MMS_ATTACH A 
                   where A.MSG_ID = M.ID_MSG)
with check option
go

alter table CALLS
   add constraint FK_Calls_Agents foreign key (AGENT_ID)
      references DEVICES (ID)
go

alter table CALLS
   add constraint FK_CALLS_FROM_NUM_TYPE foreign key (FROM_NUM_TYPE_ID)
      references NUMBERS_TYPES (ID)
go

alter table CALLS
   add constraint FK_CALLS_TO_NUM_TYPE foreign key (TO_NUM_TYPE_ID)
      references NUMBERS_TYPES (ID)
go

alter table CALLS
   add constraint FK_CALLS_FROM_PHONE foreign key (FROM_PHONE_ID)
      references PHONES (ID)
go

alter table CALLS
   add constraint FK_CALLS_TO_PHONE foreign key (TO_PHONE_ID)
      references PHONES (ID)
go

alter table COORDINATES
   add constraint FK_COORDINA_REFERENCE_DEVICES foreign key (AGENT_ID)
      references DEVICES (ID)
         on update cascade
go

alter table CRASH_LOGS
   add constraint FK_AgentErrorLog_Agent foreign key (AGENT_ID)
      references DEVICES (ID)
go

alter table DEVICES
   add constraint FK_Agents_Users foreign key (USER_ID)
      references USERS_INFO (ID)
go

alter table DEVICES
   add constraint FK_DEVICES_FK_DEVS_L_LICENSES foreign key (LIC_ID)
      references LICENSES (ID)
         on update cascade
go

alter table LICENSES
   add constraint FK_LICENSES_FK_LICS_P_PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade
go

alter table LICENSES
   add constraint FK_LICENSES_REFERENCE_USERS_IN foreign key (USER_ID)
      references USERS_INFO (ID)
go

alter table MESSAGES
   add constraint FK_MESSAGES_FK_SMS_AG_DEVICES foreign key (AGENT_ID_MSG)
      references DEVICES (ID)
         on update cascade
go

alter table MESSAGES
   add constraint FK_MESSAGES_FK_SMS_TY_MSG_TYPE foreign key (TYPE_ID)
      references MSG_TYPES (ID_TYPE)
         on update cascade
go

alter table MMS_ATTACH
   add constraint FK_MMS_ATTA_FK_MMS_AT_MESSAGES foreign key (MSG_ID)
      references MESSAGES (ID_MSG)
         on update cascade on delete cascade
go

alter table MMS_ATTACH
   add constraint FK_MMS_ATTA_REFERENCE_MIME_TYP foreign key (ID_MIME)
      references MIME_TYPES (ID_MIME)
go

alter table PAYMENTS
   add constraint FK_PAYMENTS_FK_DEVS_P_LICENSES foreign key (LIC_ID)
      references LICENSES (ID)
go

alter table PAYMENTS
   add constraint FK_PAYMENTS_FK_PRICE__PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade
go

alter table PRICE_HISTORY
   add constraint FK_PRICE_HI_FK_PRICE__PRICE_LI foreign key (PRICE_LIST_ID)
      references PRICE_LIST (ID)
         on update cascade
go


create procedure IS_ACTIVATION_ALLOWED
    @LIC_ID int,
    @CAN_ACTIVATE bit output
with encryption
as
    declare @DEVICE_COUNT int
    declare @ACT_DEV_COUNT int
begin
    set nocount on;
 
    set @CAN_ACTIVATE = 0;
 
    select @DEVICE_COUNT = DEV_CNT
    from LICENSES
    where ID = @LIC_ID
 
    select @ACT_DEV_COUNT = isnull(count(ID), 0)
    from DEVICES
    where LIC_ID = @LIC_ID

    if( @DEVICE_COUNT - @ACT_DEV_COUNT > 0 )
    begin
        set @CAN_ACTIVATE = 1;
    end
end
go


create procedure ACTIVATE_DEVICE
    @ID_DEVICE int,
    @ID_LICENSE int,
    @LIC_ACT_DATE datetime
with encryption    
as
    declare @CAN_ACTIVATE bit
begin
    set nocount on;
    
    exec  IS_ACTIVATION_ALLOWED @ID_LICENSE, @CAN_ACTIVATE output

    if( @CAN_ACTIVATE = 1 )
    begin    
        update DEVICES
        set LIC_ID = @ID_LICENSE,
            LIC_ACT_DATE = isnull(@LIC_ACT_DATE, getdate())    
        where ID = @ID_DEVICE
    end
end
go


create procedure ADD_AGENT 
    @USER_ID int, 
 	@AGENT_NAME AGENT_NAME_TYPE,
    @IMEI IMEI_TYPE = null,
    @OPER_SYS_TYPE int,
    @AGENT_ID int output
with encryption
as
begin
    set nocount on;

	insert into DEVICES(USER_ID, AGENT_NAME, DEVICE_NAME, IMEI, IS_ACTIVED, LAST_UPDATED, CREATION_DATE, OPER_SYS_TYPE)
    values(@USER_ID, @AGENT_NAME, @AGENT_NAME, @IMEI, 'FALSE', null, getdate(), @OPER_SYS_TYPE)
    
	set @AGENT_ID=CAST(SCOPE_IDENTITY() as int)
end
go


create procedure GET_NUMBER_TYPE_ID
 	@NTYPE NUM_NAME_TYPE,
    @NUMBER_TYPE_ID int output
with encryption
as
begin
    set @NUMBER_TYPE_ID = null
    if (@NTYPE is not null)
    begin
        select @NUMBER_TYPE_ID=T.ID
        from NUMBERS_TYPES T
        where T.NUMBER_TYPE = @NTYPE

        if (@NUMBER_TYPE_ID is null)
        begin
    	    insert into NUMBERS_TYPES(NUMBER_TYPE)
            values(@NTYPE)
        
            set @NUMBER_TYPE_ID = CAST(SCOPE_IDENTITY() as int)
        end
    end
end
go


create procedure GET_PHONE_ID
    @NUMBER PHONE_TYPE,
    @PHONE_NAME PNAME_TYPE,    
    @PHONE_ID int output
with encryption
as
begin
    set nocount on;

    if( @NUMBER = '-1' )
    begin
        set @NUMBER = 'Unknown'
    end
    if( @NUMBER = '-2' )
    begin
        set @NUMBER = 'Private'
    end
    if( @NUMBER = '-3' )
    begin
        set @NUMBER = 'Payphone'
    end

    set @PHONE_ID = null
    if (@NUMBER is not null)
    begin
        if (@PHONE_NAME is not null)
        begin
            select @PHONE_ID=P.ID
            from PHONES P
            where (P.NUMBER = @NUMBER) and (P.PHONE_NAME = @PHONE_NAME)
        end
        else begin
            select @PHONE_ID=P.ID
            from PHONES P
            where (P.NUMBER = @NUMBER) and (P.PHONE_NAME is null)
        end

        if (@PHONE_ID is null)
        begin
    	    insert into PHONES(NUMBER, PHONE_NAME)
            values(@NUMBER, @PHONE_NAME)

            set @PHONE_ID = CAST(SCOPE_IDENTITY() as int)
        end
    end
end
go


create procedure ADD_CALL
    @AGENT_ID int,
    @CALL_DATE datetime,
    @CALL_TYPE nchar(15),
    @FROM_NUMBER PHONE_TYPE,
    @FROM_PHONE_NAME PNAME_TYPE,
    @FROM_NUM_TYPE NUM_NAME_TYPE,
    @TO_NUMBER PHONE_TYPE,
    @TO_PHONE_NAME PNAME_TYPE,
    @TO_NUM_TYPE NUM_NAME_TYPE,
    @DURATION int
with encryption
as
    declare @FROM_PHONE_ID int
    declare @TO_PHONE_ID int
    declare @FROM_NUM_TYPE_ID int
    declare @TO_NUM_TYPE_ID int
begin
    set nocount on;

    declare @CTYPE smallint    
    set @CTYPE = 0
    if (@CALL_TYPE like '%incoming%')
    begin
        set @CTYPE = 1
    end
    if (@CALL_TYPE like '%outgoing%')
    begin
        set @CTYPE = 2
    end     
    if (@CALL_TYPE like '%missed%')
    begin
        set @CTYPE = 3
    end

    exec GET_PHONE_ID @FROM_NUMBER, @FROM_PHONE_NAME, @FROM_PHONE_ID output
    exec GET_PHONE_ID @TO_NUMBER, @TO_PHONE_NAME, @TO_PHONE_ID output
    exec GET_NUMBER_TYPE_ID @FROM_NUM_TYPE, @FROM_NUM_TYPE_ID output    
    exec GET_NUMBER_TYPE_ID @TO_NUM_TYPE, @TO_NUM_TYPE_ID output

    insert into CALLS(AGENT_ID, DATE, CTYPE, DURATION, FROM_PHONE_ID, TO_PHONE_ID, FROM_NUM_TYPE_ID, TO_NUM_TYPE_ID)
    values (@AGENT_ID, @CALL_DATE, @CTYPE, @DURATION, @FROM_PHONE_ID, @TO_PHONE_ID, @FROM_NUM_TYPE_ID, @TO_NUM_TYPE_ID);
end
go


create procedure ADD_COORDINATE
    @AGENT_ID      int,
    @LATITUDE      double precision,
    @LONGITUDE     double precision,
    @ALTITUDE      double precision,
    @ACCURACY      float,
    @BEARING       float,
    @PROVIDER_NAME nchar(50),
    @SPEED         float,
    @ARRIVAL_TIME  datetime = null
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        if (@ARRIVAL_TIME is null)
        begin
            set @ARRIVAL_TIME = getdate()
        end

        declare @PROVIDER PROVIDER_TYPE
        set @PROVIDER = 0
        if (@PROVIDER_NAME like '%gps%')
        begin
            set @PROVIDER = 1
        end
        if (@PROVIDER_NAME like '%network%')
        begin
            set @PROVIDER = 2
        end        
        
        insert into COORDINATES(AGENT_ID, LATITUDE, LONGITUDE, ALTITUDE, 
            ACCURACY, BEARING, PROVIDER, SPEED, ARRIVAL_TIME)
        values(@AGENT_ID, @LATITUDE, @LONGITUDE, @ALTITUDE, @ACCURACY, @BEARING,
            @PROVIDER, @SPEED, @ARRIVAL_TIME)
    end        
end
go


create procedure ADD_CRASH_LOG
    @AGENT_ID int, 
    @CRASH_DATETIME datetime, 
    @CRASH_DATA CRASH_DATA_TYPE
with encryption
as
begin
    set nocount on;

    insert into CRASH_LOGS(AGENT_ID, CRASH_DATETIME, CRASH_DATA)
    values (@AGENT_ID, @CRASH_DATETIME, @CRASH_DATA);
end
go


create procedure ADD_LICENSE
    @USER_ID int,
    @PRICE_LIST_ID int,
    @SUBSCR_END datetime,
    @ADD_DEV_COUNT smallint,
    @PAY_SYS_TYPE smallint,
    @SUBSCR_ID SUBSCR_ID_TYPE,    
    @OUT_LIC_ID int output
with encryption
as
    declare @PAY_TYPE smallint
begin
	select @PAY_TYPE = PAY_TYPE
	from ACTIVE_PRICE_LIST
	where ID = @PRICE_LIST_ID

    if( @PAY_TYPE = 1 )  -- Signup Fee Payment (for first license)
    begin
        -- Please note that signup fee license is created as if user bought
        -- smallest period license. That's why we use search for price list
        -- if in different PAY_TYPE category. 
		select @PRICE_LIST_ID = APL.ID
		from ACTIVE_PRICE_LIST APL
		where APL.DURATION = ( select min(DURATION)
		                       from ACTIVE_PRICE_LIST
		                       where PAY_TYPE = 2 )
    end

	insert into LICENSES(USER_ID, PRICE_LIST_ID, SUBSCR_END, DEV_CNT, 
	    PAY_SYS_TYPE, SUBSCR_ID, CREATION_DATE)
	values(@USER_ID, @PRICE_LIST_ID, @SUBSCR_END, isnull(@ADD_DEV_COUNT, 0) + 1,
	    @PAY_SYS_TYPE, @SUBSCR_ID, getdate())

	set @OUT_LIC_ID=CAST(SCOPE_IDENTITY() as int);
end
go


create procedure ADD_SMS 
  @AGENT_ID int, @MSG_TYPE nchar(10), @SMS_ADDR ADDRESS_TYPE, @SMS_DT datetime, @SMS_BODY nchar(4000)
with encryption
as
begin
    set nocount on;

    declare @TYPE_ID int;
    
    select @TYPE_ID=ID_TYPE
    from MSG_TYPES
    where TYPE_NAME = @MSG_TYPE;

    insert into SMS(AGENT_ID_SMS, TYPE_ID, SMS_ADDR, SMS_DT, SMS_BODY)
    values(@AGENT_ID, @TYPE_ID, @SMS_ADDR, @SMS_DT, @SMS_BODY);
end
go


create procedure ADD_MMS
  @AGENT_ID int, @MSG_TYPE nchar(10), @MMS_ADDR nvarchar(500), @MMS_DT datetime, @MMS_BODY nchar(4000), @MMS_SUBJ nchar(50), 
  @ID_MSG int output
with encryption
as
begin
    set nocount on;

    exec ADD_SMS @AGENT_ID, @MSG_TYPE, @MMS_ADDR, @MMS_DT, @MMS_BODY 
      
    select @ID_MSG=max(ID_MSG)
    from MESSAGES
    where
        (AGENT_ID_MSG = @AGENT_ID) and (MSG_DT = @MMS_DT) and (MSG_ADDR = @MMS_ADDR) and (MSG_BODY = @MMS_BODY)
    
    update MESSAGES
    set
        MSG_SUBJ = @MMS_SUBJ
    where
        ID_MSG = @ID_MSG	
end
go


create procedure ADD_MMS_ATTACH 
    @AGENT_ID int, 
    @AGENT_ATTACH_ID int, 
    @ATTACH_DATA ATTACH_DATA_TYPE
with encryption
as
declare @MESSAGE_ID int
begin
    set nocount on;
    
    declare @ID_ATTACH int
    
    select @ID_ATTACH=A.ID_ATTACH
    from MESSAGES M
        left join MMS_ATTACH A on M.ID_MSG = A.MSG_ID
    where M.AGENT_ID_MSG = @AGENT_ID and A.AGENT_ATTACH_ID = @AGENT_ATTACH_ID
    
    update MMS_ATTACH 
    set ATTACH_DATA = @ATTACH_DATA
    where ID_ATTACH = @ID_ATTACH
end
go


create procedure ADD_MMS_ATTACH_TEMPL @MMS_ID int, @MIME_TYPE char(50), @AGENT_ATTACH_ID int 
with encryption
as
begin
    set nocount on;

    declare @ID_MIME int
    
    select @ID_MIME=ID_MIME 
    from MIME_TYPES
    where MIME_NAME=@MIME_TYPE
    
    if (@ID_MIME is NULL)
    begin
        insert into MIME_TYPES(MIME_NAME)
        values (@MIME_TYPE)

        select @ID_MIME=ID_MIME 
        from MIME_TYPES
        where MIME_NAME=@MIME_TYPE         
    end

    insert into MMS_ATTACH(MSG_ID, ID_MIME, AGENT_ATTACH_ID)
    values (@MMS_ID, @ID_MIME, @AGENT_ATTACH_ID)
end
go


create procedure ADD_NEW_PRICE
    @PL_ID int,
    @NEW_PRICE money,
    @NEW_PRICE_OTHER money = null,
    @DATE_FROM datetime = null
with encryption    
as
begin
    set nocount on;

    if (@DATE_FROM is null)
    begin
        set @DATE_FROM = getdate();
    end

    update PRICE_HISTORY
    set
        DATE_TO = @DATE_FROM
    where (PRICE_LIST_ID = @PL_ID) and (DATE_TO is null)

    insert into PRICE_HISTORY(PRICE_LIST_ID, DATE_FROM, PRICE, PRICE_OTHER)
    values (@PL_ID, @DATE_FROM, @NEW_PRICE, @NEW_PRICE_OTHER)
end
go


create procedure ADD_NEW_PRICE_LIST_ITEM
    @LABEL NAME_TYPE,
    @DURATION smallint,
    @PAY_TYPE smallint,
    @PL_ID int output
with encryption    
as
begin
    set nocount on;

    insert into PRICE_LIST(LABEL, DURATION, PAY_TYPE)
    values (@LABEL, @DURATION, @PAY_TYPE)

    set @PL_ID=CAST(SCOPE_IDENTITY() as int)
end
go


create procedure ADD_TOKEN
    @EMAIL EMAIL_TYPE,
    @TOKEN varchar(100)
with encryption
as
begin
    insert into TOKENS (EMAIL, TOKEN)
    values (@EMAIL, @TOKEN)        
end
go


create procedure ADD_USER_INFO
    @ASP_USER_ID uniqueidentifier,
    @FIRST_NAME nvarchar(50),
    @LAST_NAME nvarchar(50),
    @EMAIL EMAIL_TYPE,
    @ID int output
with encryption
as
begin
    set nocount on;

    insert into USERS_INFO(ASP_NET_USER_ID, FIRST_NAME, LAST_NAME, EMAIL)
    values (@ASP_USER_ID, @FIRST_NAME, @LAST_NAME, @EMAIL)

	select @ID=CAST(SCOPE_IDENTITY() as int)      
end
go


create procedure GET_LICENSE
    @LIC_ID int,
    @REPORT_DATE datetime = null,    
    @DEVICE_COUNT int output,
    @RESIDUAL_DAYS int output,
    @SUBSCR_END datetime output,
    @PRICE_LIST_ID int output,
    @SUBSCR_ID SUBSCR_ID_TYPE output,
    @PAY_SYS_TYPE smallint output,
    @FREE_DEV_COUNT int output
with encryption    
as
declare @ACT_DEV_COUNT int
begin
    set nocount on;
    
    select @SUBSCR_END = SUBSCR_END, @DEVICE_COUNT = isnull(DEV_CNT, 0), 
        @PRICE_LIST_ID = PRICE_LIST_ID, @SUBSCR_ID = SUBSCR_ID, @PAY_SYS_TYPE = PAY_SYS_TYPE 
    from LICENSES
    where ID = @LIC_ID

    select @ACT_DEV_COUNT = isnull(count(D.ID), 0)
    from DEVICES D
    where D.LIC_ID = @LIC_ID

    set @FREE_DEV_COUNT = @DEVICE_COUNT - @ACT_DEV_COUNT

    if(@REPORT_DATE is null)
    begin
        set @REPORT_DATE = getdate()
    end

    set @RESIDUAL_DAYS = datediff(day, @REPORT_DATE, @SUBSCR_END);
    if((@RESIDUAL_DAYS < 0) or (@RESIDUAL_DAYS is null))
    begin
        set @RESIDUAL_DAYS = 0
    end

    if(@DEVICE_COUNT is null)
    begin
        set @DEVICE_COUNT = 0 
    end
end
go


create procedure GET_PRICE_LIST_ITEM
    @PL_ID int,
    @DURATION smallint output,
    @PRICE money output,
    @PRICE_OTHER money output
with encryption    
as
begin
    set nocount on;

    select @DURATION=DURATION, @PRICE=PRICE, @PRICE_OTHER=isnull(PRICE_OTHER, 0.0)
    from ACTIVE_PRICE_LIST    
    where (ID = @PL_ID)
end
go


create procedure GET_COST_ADDING_DEVICE
    @RESIDUAL_DAYS int,
    @COST money output,
    @PRICE_LIST_ID int output
with encryption
as
    declare @DURATION int
    declare @NEXT_PRICE_LIST_ID int
    declare @NEXT_DURATION int
    declare @NEXT_COST money 
    declare @CURSOR cursor    
begin
    set nocount on;

    set @CURSOR  = cursor SCROLL for    
		select ID, PRICE_OTHER, DURATION
		from ACTIVE_PRICE_LIST
		where (PAY_TYPE=2) and (PRICE_OTHER is not null)
		order by DURATION
    open @CURSOR

    fetch next from @CURSOR into @PRICE_LIST_ID, @COST, @DURATION

	while @@FETCH_STATUS = 0
	begin
		fetch next from @CURSOR into @NEXT_PRICE_LIST_ID, @NEXT_COST, @NEXT_DURATION
		
		if( @NEXT_DURATION > @RESIDUAL_DAYS )
        begin
            break
        end
        else begin
            set @PRICE_LIST_ID = @NEXT_PRICE_LIST_ID;
			set @COST = @NEXT_COST;
			set @DURATION = @NEXT_DURATION;
        end
	end
	close @CURSOR

    set @COST = isnull(round(@COST * @RESIDUAL_DAYS / @DURATION, 2), 0.0);
end
go


create procedure CALCULATE_PAYMENT
    @LIC_ID int = null,  -- is null if new LICENSE
    @ADD_DEV_COUNT int = 0,  -- = 0 if useing the device count in license
    @PAYMENT_DATE datetime = null,
    @PL_ID int = null,  -- = (to existing subscription)
    @AMOUNT_OLD_PERIOD_NEW_DEV_EACH money output,
    @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL money output,
    @AMOUNT_NEW_PERIOD_FIRST money output,
    @AMOUNT_NEW_PERIOD_OTHER money output,
    @AMOUNT_TOTAL money output,
    @PRICE_LIST_ID_OLD_PERIOD int output
with encryption   
as
    declare @PREV_DEV_COUNT int
    declare @PREV_SUBSCR_END datetime
    declare @RESIDUAL_DAYS int
    declare @PREV_DURATION int
    declare @PREV_PL_ID int
    declare @PREV_SUBSCR_ID SUBSCR_ID_TYPE
    declare @PREV_PAY_SYS_TYPE smallint
    declare @FREE_DEV_COUNT int
begin    
    set @AMOUNT_OLD_PERIOD_NEW_DEV_EACH = 0.0
    set @AMOUNT_NEW_PERIOD_FIRST = 0.0
    set @AMOUNT_NEW_PERIOD_OTHER = 0.0
    set @AMOUNT_TOTAL = 0.0
    set @PRICE_LIST_ID_OLD_PERIOD = null

    if( (@LIC_ID is null) and (@PL_ID is null) )
    begin
        return
    end

    if( @LIC_ID is not null )
    begin
        exec GET_LICENSE @LIC_ID, @PAYMENT_DATE, @PREV_DEV_COUNT output, 
            @RESIDUAL_DAYS output, @PREV_SUBSCR_END output, 
            @PREV_PL_ID output, @PREV_SUBSCR_ID output, @PREV_PAY_SYS_TYPE output,
            @FREE_DEV_COUNT output
		if( @ADD_DEV_COUNT > 0 )  -- add device to current period
		begin
			exec GET_COST_ADDING_DEVICE @RESIDUAL_DAYS, @AMOUNT_OLD_PERIOD_NEW_DEV_EACH output, @PRICE_LIST_ID_OLD_PERIOD output
			set @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL = round(isnull(@AMOUNT_OLD_PERIOD_NEW_DEV_EACH, 0.0) * @ADD_DEV_COUNT, 2)
			set @AMOUNT_TOTAL = @AMOUNT_TOTAL + @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL
		end
    end
    else begin
        set @PREV_DEV_COUNT = 1;
        set @RESIDUAL_DAYS = 0;
        set @PREV_SUBSCR_END = null;
        set @PREV_PL_ID = null;
    end

    if( @PL_ID is not null )
    begin
		exec GET_PRICE_LIST_ITEM @PL_ID, @PREV_DURATION output, @AMOUNT_NEW_PERIOD_FIRST output, @AMOUNT_NEW_PERIOD_OTHER output

		set @AMOUNT_TOTAL = @AMOUNT_TOTAL + @AMOUNT_NEW_PERIOD_FIRST
		if( @PREV_DEV_COUNT - 1 + @ADD_DEV_COUNT > 0 )
		begin
			set @AMOUNT_NEW_PERIOD_OTHER = round(@AMOUNT_NEW_PERIOD_OTHER * (@PREV_DEV_COUNT - 1 + @ADD_DEV_COUNT), 2);
			set @AMOUNT_TOTAL = @AMOUNT_TOTAL + @AMOUNT_NEW_PERIOD_OTHER;
		end
		else begin
			set @AMOUNT_NEW_PERIOD_OTHER = 0.0;
		end
    end
end
go


create procedure CLEAN_CRASH_LOGS
    @MAX_DAY_COUNT int = 45,
    @MAX_ROW_COUNT int = 100
with encryption
as
begin
    set nocount on;

    delete 
    from CRASH_LOGS
    where datediff(day, CRASH_DATETIME, getdate()) > @MAX_DAY_COUNT

    delete from CRASH_LOGS
    where ID in ( select CL.ID
                  from CRASH_LOGS CL
                  where CL.AGENT_ID in ( select CL1.AGENT_ID
                                         from CRASH_LOGS CL1
                                         group by CL1.AGENT_ID
                                         having count(*) > @MAX_ROW_COUNT
                                       )
                        and CL.ID not in ( select top(@MAX_ROW_COUNT) CL2.ID
                                           from CRASH_LOGS CL2
                                           where CL2.AGENT_ID = CL.AGENT_ID
                                           order by CL2.ID desc
                                         )
                )
end
go


create procedure CLEAN_TOKENS
with encryption
as
begin
    delete from TOKENS
    where EXPIRATION_DATE < GETUTCDATE()
end
go


create procedure DELETE_UNUSED_PHONES
as
begin
    delete 
    from PHONES 
    where ID not in (select distinct FROM_PHONE_ID
                     from CALLS
                     union
                     select distinct TO_PHONE_ID
                     from CALLS)
end
go


create procedure CLEAN_USERS_DATA
    @MAX_DAY_COUNT int = 45
with encryption
as
begin
    set nocount on;

    delete from MESSAGES
    where datediff(day, MSG_DT, getutcdate()) > @MAX_DAY_COUNT

    delete from COORDINATES
    where datediff(day, ARRIVAL_TIME, getutcdate()) > @MAX_DAY_COUNT

    delete 
    from CALLS
    where datediff(day, DATE, getutcdate()) > @MAX_DAY_COUNT

    exec DELETE_UNUSED_PHONES
    
    delete 
    from DEVICES
    where (LIC_ID is null) 
        and (datediff(day, CREATION_DATE, getdate()) > @MAX_DAY_COUNT)
end
go


create procedure IS_DEACTIVATION_ALLOWED
    @ID_DEVICE int,
    @CAN_DEACTIVATE bit output
with encryption
as
    declare @LIC_ID int
    declare @LIC_ACT_DATE datetime
begin
    set nocount on;
 
    set @CAN_DEACTIVATE = 0;

    select @LIC_ID = LIC_ID, @LIC_ACT_DATE = LIC_ACT_DATE
    from DEVICES D
    where D.ID = @ID_DEVICE

    if( (@LIC_ID is not null) and (@LIC_ACT_DATE is not null) and (datediff(day, @LIC_ACT_DATE, getdate()) > 30) )
    begin
        set @CAN_DEACTIVATE = 1;
    end
end
go


create procedure DEACTIVATE_DEVICE
    @ID_DEVICE int
with encryption
as
    declare @CAN_DEACTIVATE bit
begin
    set nocount on;

    exec  IS_DEACTIVATION_ALLOWED @ID_DEVICE, @CAN_DEACTIVATE output

    if( @CAN_DEACTIVATE = 1 )
    begin
        update DEVICES
        set LIC_ID = null,
            LIC_ACT_DATE = null,
            CREATION_DATE = getdate()
        where ID = @ID_DEVICE
    end
end
go


create procedure DEACTIVATE_LICENSE
    @LICENSE_ID int
with encryption
as
begin
    set nocount on;

    update DEVICES
    set LIC_ID = null,
        LIC_ACT_DATE = null,
        CREATION_DATE = getdate()
    where LIC_ID = @LICENSE_ID
end
go


create procedure DELETE_CALLS
    @AGENT_ID int = null
with encryption
as
begin  
    if (@AGENT_ID is not null)
    begin
        delete 
        from CALLS
        where AGENT_ID = @AGENT_ID
        
        exec DELETE_UNUSED_PHONES
    end
end
go


create procedure DELETE_DEVICE
    @AGENT_ID int = null
with encryption
as
begin  
    if (@AGENT_ID is not null)
    begin
        -- prepare agent to delete
        exec DELETE_CALLS @AGENT_ID
        
        delete 
        from MESSAGES
        where AGENT_ID_MSG = @AGENT_ID
        
        delete 
        from COORDINATES
        where AGENT_ID = @AGENT_ID
        
		delete 
        from CRASH_LOGS
        where AGENT_ID = @AGENT_ID        
        
        -- delete agent
        delete 
        from DEVICES
        where ID = @AGENT_ID
    end
end
go


create procedure DELETE_TOKEN
    @TOKEN VARCHAR(100) = null
with encryption    
as
begin
    if (@TOKEN is not null)
    begin
        delete from TOKENS
        where TOKEN = @TOKEN
    end
end
go


create procedure DELETE_USER_INFO
    @ASP_USER_ID uniqueidentifier = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        -- delete the user's devices
        declare @AGENT_ID int
              
        declare @DEVICES_CURSOR cursor
        set @DEVICES_CURSOR = cursor scroll for
          select D.ID
          from USERS_INFO U
              left join DEVICES D on D.USER_ID = U.ID
          where U.ASP_NET_USER_ID = @ASP_USER_ID
          
        open @DEVICES_CURSOR
        
        fetch next from @DEVICES_CURSOR into @AGENT_ID
        while @@FETCH_STATUS = 0
        begin
            exec DELETE_DEVICE @AGENT_ID
            fetch next from @DEVICES_CURSOR into @AGENT_ID
        end
        
        close @DEVICES_CURSOR
    end
end
go


create procedure GET_NEW_SUBSCR_END
    @PL_ID int,
    @LIC_ID int = null,
    @PAYMENT_DATE datetime = null, 
    @NEW_SUBSCR_END datetime output
with encryption    
as
    declare @PREV_SUBSCR_END datetime
    declare @PREV_PL_ID int
    declare @DURATION int
begin
    if (@PAYMENT_DATE is null)
    begin
        set @PAYMENT_DATE = getdate()
    end
    
    set @PREV_SUBSCR_END = @PAYMENT_DATE;    

	if( @LIC_ID is not null )  -- Get previous SUBSCR_END
	begin
		select @PREV_SUBSCR_END = L.SUBSCR_END, @PREV_PL_ID = PRICE_LIST_ID
		from LICENSES L
		where L.ID = @LIC_ID
	end
	
    set @NEW_SUBSCR_END = @PREV_SUBSCR_END;
	
    if (@PL_ID is null)
    begin
		return
	end

    if (@PREV_SUBSCR_END < @PAYMENT_DATE)
    begin
        set @PREV_SUBSCR_END = @PAYMENT_DATE  -- fail to pay on time
    end

    select @DURATION = DURATION
    from ACTIVE_PRICE_LIST
    where ID = @PL_ID

    set @NEW_SUBSCR_END = dateadd(day, @DURATION, @PREV_SUBSCR_END);
end
go


create procedure FINALIZE_PAYMENT
    @USER_ID int,
    @PAY_SYS_TYPE smallint,
    @SUBSCR_ID SUBSCR_ID_TYPE = null,    -- <null> if don't subscribe
    @PL_ID int = null,                   -- is null if LICENSE extend
    @TRANS_ID SUBSCR_ID_TYPE,
    @ADD_DEV_COUNT int = 0,
    @IN_LIC_ID int = null,   
    @PAYMENT_DATE datetime = null,
    @PAY_COMMENT COMMENT_TYPE = null,
    @OUT_LIC_ID int output,              -- <Ok> if is not null
    @PAY_ID int output                   -- <Ok> if is not null
with encryption
as
    declare @PREV_PRICE_LIST_ID int
    declare @PREV_SUBSCR_END datetime
    declare @PREV_DEV_COUNT int
    declare @NEW_SUBSCR_END datetime    
    declare @SUBSCR_END datetime
    declare @AMOUNT_OLD_PERIOD_NEW_DEV_EACH money
    declare @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL money 
    declare @AMOUNT_NEW_PERIOD_FIRST money
    declare @AMOUNT_NEW_PERIOD_OTHER money
    declare @AMOUNT_NEW_PERIOD_TOTAL money
    declare @AMOUNT_TOTAL money
    declare @PRICE_LIST_ID_OLD_PERIOD int
begin
    set nocount on;

    set @OUT_LIC_ID = null;
    set @PAY_ID = null;

    if ((@IN_LIC_ID is null) and (@PL_ID is null))
    begin
        return
    end

    if (@PAYMENT_DATE is null)
    begin
      set @PAYMENT_DATE = getdate();
    end

	if (@IN_LIC_ID is not null)
	begin
        select @PREV_SUBSCR_END = SUBSCR_END, 
               @PREV_PRICE_LIST_ID = PRICE_LIST_ID, @PREV_DEV_COUNT = DEV_CNT
        from LICENSES
        where ID = @IN_LIC_ID
        
        set @OUT_LIC_ID = @IN_LIC_ID;
    end
    else begin
        set @PREV_SUBSCR_END = @PAYMENT_DATE;
        set @PREV_DEV_COUNT = 1;
    end

    exec CALCULATE_PAYMENT @IN_LIC_ID, @ADD_DEV_COUNT, @PAYMENT_DATE, @PL_ID,
        @AMOUNT_OLD_PERIOD_NEW_DEV_EACH output, 
        @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL output, 
        @AMOUNT_NEW_PERIOD_FIRST output, @AMOUNT_NEW_PERIOD_OTHER output, 
        @AMOUNT_TOTAL output, @PRICE_LIST_ID_OLD_PERIOD output

    set @AMOUNT_NEW_PERIOD_TOTAL = @AMOUNT_TOTAL - isnull(@AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL, 0);

    exec GET_NEW_SUBSCR_END @PL_ID, @IN_LIC_ID, @PREV_SUBSCR_END, @NEW_SUBSCR_END output

    set @ADD_DEV_COUNT = isnull(@ADD_DEV_COUNT, 0);

    begin transaction FINAL_PAY
    
	if (@IN_LIC_ID is null)
	begin
        exec ADD_LICENSE @USER_ID, @PL_ID, @NEW_SUBSCR_END, @ADD_DEV_COUNT, 
            @PAY_SYS_TYPE, @SUBSCR_ID, @OUT_LIC_ID output
	end
	else begin
		update LICENSES
		set  PRICE_LIST_ID = isnull(@PL_ID, PRICE_LIST_ID),
			 SUBSCR_END = @NEW_SUBSCR_END,
			 DEV_CNT = DEV_CNT + @ADD_DEV_COUNT,
			 PAY_SYS_TYPE = @PAY_SYS_TYPE,
			 SUBSCR_ID = @SUBSCR_ID
		where ID = @IN_LIC_ID
    end

    -- ADD NEW PAYMENT ------------------------------------------------------
	if( (@ADD_DEV_COUNT > 0) and (@IN_LIC_ID is not null) )
    begin    -- Added devices to current period -----------------------------
		insert into PAYMENTS( LIC_ID, PRICE_LIST_ID, PAY_DATE, TRANS_ID, 
            AMOUNT, DEV_CNT, SUBSCR_END, PREV_SUBSCR_END, PAY_COMMENT )
		values( @OUT_LIC_ID, isnull(@PRICE_LIST_ID_OLD_PERIOD, @PL_ID), 
		    @PAYMENT_DATE, @TRANS_ID, @AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL, 
		    @ADD_DEV_COUNT, @PREV_SUBSCR_END, @PREV_SUBSCR_END, @PAY_COMMENT);
	end

	if( @NEW_SUBSCR_END > @PREV_SUBSCR_END )
    begin    -- New period --------------------------------------------------
        insert into PAYMENTS( LIC_ID, PRICE_LIST_ID, PAY_DATE, TRANS_ID, 
            AMOUNT, DEV_CNT, SUBSCR_END, PREV_SUBSCR_END, PAY_COMMENT )
        values( @OUT_LIC_ID, isnull(@PL_ID, @PREV_PRICE_LIST_ID), @PAYMENT_DATE,
            @TRANS_ID, @AMOUNT_NEW_PERIOD_TOTAL, @PREV_DEV_COUNT + @ADD_DEV_COUNT,
            @NEW_SUBSCR_END, @PREV_SUBSCR_END, @PAY_COMMENT);	
    end

    set @PAY_ID=CAST(SCOPE_IDENTITY() as int);

    commit transaction FINAL_PAY
end
go


create procedure FINALIZE_AUTO_PAYMENT
    @PAYMENT_DATE datetime = null,    
    @SUBSCR_ID SUBSCR_ID_TYPE,
    @TRANS_ID SUBSCR_ID_TYPE,
    @PAY_COMMENT COMMENT_TYPE = null,
    @PAY_ID int output
with encryption
as
    declare @LIC_ID int
    declare @USER_ID int
    declare @PAY_SYS_TYPE smallint
    declare @OUT_LIC_ID int
    declare @PRICE_LIST_ID int
begin
    set nocount on;

	set @PAY_ID = null

    select @LIC_ID=L.ID, @PRICE_LIST_ID=PRICE_LIST_ID, 
        @USER_ID=L.USER_ID, @PAY_SYS_TYPE=L.PAY_SYS_TYPE
    from LICENSES L
    where L.SUBSCR_ID = @SUBSCR_ID

	if( @LIC_ID is not null )
	begin
		exec FINALIZE_PAYMENT @USER_ID, @PAY_SYS_TYPE, @SUBSCR_ID, @PRICE_LIST_ID, 
			@TRANS_ID, 0, @LIC_ID, @PAYMENT_DATE, @PAY_COMMENT, 
			@OUT_LIC_ID output, @PAY_ID output
    end
end
go


create procedure GET_ACTUAL_AGENT_ID
    @AGENT_ID int,    
    @IMEI IMEI_TYPE = null,
    @ACTUAL_AGENT_ID int output
with encryption
as
begin
    set nocount on;

    set @ACTUAL_AGENT_ID = null;
  
    if (@AGENT_ID is null)
    begin
        return;
    end

    declare @USER_ID int;
    declare @AGENT_NAME nvarchar(50)
    declare @OPER_SYS_TYPE int
    

    select @USER_ID=D.USER_ID, @AGENT_NAME=D.AGENT_NAME, @OPER_SYS_TYPE = d.OPER_SYS_TYPE
    from DEVICES D
    where D.ID = @AGENT_ID;
  
    -- agent is exist for this device
    select @ACTUAL_AGENT_ID = D.ID
    from DEVICES D
    where (D.IMEI = @IMEI) and (D.USER_ID = @USER_ID);
    
    if (@ACTUAL_AGENT_ID is not null)
    begin
        return;
    end    

    -- else  
    select @ACTUAL_AGENT_ID = D.ID
    from DEVICES D
    where (D.ID = @AGENT_ID) and (D.IMEI is null)

    if (@ACTUAL_AGENT_ID is not null)
    begin
        -- first agent's start
        update DEVICES
        set IMEI = @IMEI
        where ID = @AGENT_ID;
        return;
    end    
    else begin
        -- doubling agent (new device for AGENT)     
        exec ADD_AGENT @USER_ID, @AGENT_NAME, @IMEI, @OPER_SYS_TYPE, @ACTUAL_AGENT_ID output
    end
end
go

create procedure GET_AGENTS_BY_GUID
    @USER_GUID uniqueidentifier = null,
    @IS_ACTIVED bit = 1
with encryption
as
begin
    if (@USER_GUID is not null)
    begin
        select D.ID, isnull(D.DEVICE_NAME, D.AGENT_NAME) as DEVICE_NAME, 
            D.LAST_UPDATED, isnull(D.IMEI, '<no data>') as IMEI, D.LIC_ID
        from DEVICES D 
            join USERS_INFO U on U.ID = D.USER_ID
        where (U.ASP_NET_USER_ID = @USER_GUID) and (IS_ACTIVED = @IS_ACTIVED)
        order by D.DEVICE_NAME
    end
end
go


create procedure GET_AGENTS_COUNT
    @USER_ID int = null,
    @AGENT_ID int = null,
    @AGENTS_COUNT int output
with encryption
as
begin
    set nocount on;

	set @AGENTS_COUNT = 0
    if ((@USER_ID is not null) and (@AGENT_ID is not null))
    begin
        select @AGENTS_COUNT = count(*) 
        from DEVICES 
        where ID = @AGENT_ID 
            and USER_ID = @USER_ID

        if (@AGENTS_COUNT is null)
        begin
            set @AGENTS_COUNT = 0
        end
    end
end
go


create procedure GET_AGENTS_COUNT_BY_GUID
    @USER_GUID uniqueidentifier = null,
    @AGENT_ID int = null,
    @AGENTS_COUNT int output
with encryption
as
begin
    set nocount on;

    set @AGENTS_COUNT = 0
    if ((@USER_GUID is not null) and (@AGENT_ID is not null))
    begin
        select @AGENTS_COUNT = isnull(count(D.ID), 0) 
        from USERS_INFO U 
            left join DEVICES D on U.ID = D.USER_ID
        where U.ASP_NET_USER_ID = @USER_GUID 
            and D.ID = @AGENT_ID 
    end
end
go


create procedure GET_AGENTS_LAST_UPDATE_TIME 
    @AGENT_ID int = null,
    @LAST_UPDATED_TIME datetime output
with encryption
as
begin
    set nocount on;

	set @LAST_UPDATED_TIME = null
    if (@AGENT_ID is not null)
    begin
        select @LAST_UPDATED_TIME=LAST_UPDATED
        from DEVICES
        where ID = @AGENT_ID
    end
end
go


create procedure GET_ATTACH @ID_ATTACH int
with encryption
as
begin
    select ATTACH_DATA 
    from MMS_ATTACH
    where ID_ATTACH = @ID_ATTACH
end
go


create procedure GET_DEVICE_TIME_ZONE_OFFSET
    @AGENT_ID int = null,
    @TZ_DEV_OFFSET datetime output
with encryption
as
begin
    set @TZ_DEV_OFFSET = null
    if (@AGENT_ID is not null)
    begin 
        select @TZ_DEV_OFFSET = dateadd(ms, isnull(D.TZ_OFFSET, 0),  0)
        from DEVICES D 
        where D.ID = @AGENT_ID;
    end
end
go


create procedure GET_CALLS
    @AGENT_ID int = null,
    @CTYPE_FLTR smallint = null,
    @NUMBER_FLTR PNAME_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    declare @NUM_FLTR PNAME_TYPE;
    set @NUM_FLTR = @NUMBER_FLTR
    if (@NUM_FLTR is not null)
    begin
        set @NUM_FLTR='%' + ltrim(@NUM_FLTR);
        set @NUM_FLTR=rtrim(@NUM_FLTR) + '%';
    end

    if (@AGENT_ID is not null)
    begin 
		declare @TZ_DEV_OFFSET datetime

        exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

        select distinct DATE, (DATE + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), DATE) as USER_DATE,
            P_FROM.NUMBER as FROM_NUMBER, P_FROM.PHONE_NAME as FROM_PHONE_NAME, T_FROM.NUMBER_TYPE as FROM_NUMBER_TYPE,
            P_TO.NUMBER as TO_NUMBER, P_TO.PHONE_NAME as TO_PHONE_NAME, T_TO.NUMBER_TYPE as TO_NUMBER_TYPE,
            case C.CTYPE
                when 1 THEN 'Incoming'
                when 2 THEN 'Outgoing'
			    when 3 THEN 'Missed'
                else 'unknown'
            end as CALL_TYPE,
            case when (C.DURATION / 3600 < 10) then '0' else '' end + cast (C.DURATION/3600 as varchar(2)) + ':' 
            + case when (C.DURATION % 3600 / 60 < 10) then '0' else '' end + cast (C.DURATION%3600/60 as varchar(2)) + ':' 
            + case when (C.DURATION % 3600 % 60 < 10) then '0' else '' end + cast (C.DURATION%3600%60 as varchar(2)) as DURATION
        from CALLS C         
            left join PHONES P_TO on P_TO.ID = C.TO_PHONE_ID
            left join NUMBERS_TYPES T_TO on T_TO.ID = C.TO_NUM_TYPE_ID                                    
            left join PHONES P_FROM on P_FROM.ID = C.FROM_PHONE_ID
            left join NUMBERS_TYPES T_FROM on T_FROM.ID = C.FROM_NUM_TYPE_ID
        where (C.AGENT_ID = @AGENT_ID)            
            and ((@CTYPE_FLTR is null) or (C.CTYPE = @CTYPE_FLTR))
            and ((@BEG_DATE_FLTR is null) or (C.DATE >= @BEG_DATE_FLTR))
            and ((@END_DATE_FLTR is null) or (C.DATE <= @END_DATE_FLTR))
            and ((@NUMBER_FLTR is null) or (P_FROM.NUMBER like @NUM_FLTR) or (P_TO.NUMBER like @NUM_FLTR)
                or (P_FROM.PHONE_NAME like @NUM_FLTR) or (P_TO.PHONE_NAME like @NUM_FLTR))         
        order by DATE desc            
    end
end
go


create procedure GET_COORDINATES
    @AGENT_ID int = null,
    @PROVIDER_MASK int = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if (@AGENT_ID is not null)
    begin 
        declare @TZ_DEV_OFFSET datetime

        exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

        select distinct (ARRIVAL_TIME + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), ARRIVAL_TIME) as USER_DATE, LATITUDE, LONGITUDE, ALTITUDE, ACCURACY, BEARING, SPEED,
            case PROVIDER 
                when 1 THEN 'GPS'
                when 2 THEN 'GSM/Wi-Fi'
                ELSE 'unknown' 
            end as PROVIDER
        from COORDINATES
        where (AGENT_ID = @AGENT_ID)
            and ((@PROVIDER_MASK is null) or (@PROVIDER_MASK = 0) or (PROVIDER & @PROVIDER_MASK > 0))
            and ((@BEG_DATE_FLTR is null) or (ARRIVAL_TIME >= @BEG_DATE_FLTR))
            and ((@END_DATE_FLTR is null) or (ARRIVAL_TIME <= @END_DATE_FLTR))
        order by DEV_DATE desc
    end
end
go


create procedure GET_CRASH_LOGS
    @AGENT_ID int = null
with encryption
as
begin
    select distinct CRASH_DATETIME, CRASH_DATA
    from CRASH_LOGS CL
    where (@AGENT_ID is not null) and (CL.AGENT_ID = @AGENT_ID)
    order by CRASH_DATETIME desc
end
go


create procedure GET_DEVICES
    @USER_ID int = null,
    @IS_ACTIVED bit = 1
with encryption
as
begin
    if (@USER_ID is not null)
    begin
        select D.ID, isnull(D.DEVICE_NAME, D.AGENT_NAME), D.LAST_UPDATED, isnull(D.IMEI, '<no data>')
        from DEVICES D
        where (D.USER_ID = @USER_ID)
            and (D.IS_ACTIVED = @IS_ACTIVED)
        order by D.DEVICE_NAME
    end
end
go


create procedure GET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE output,
    @IMEI IMEI_TYPE output,
    @PHONE_NUMBER PHONE_TYPE output,
    @BRAND BRAND_TYPE output,
    @MODEL MODEL_TYPE output,
    @OS OS_TYPE output,
    @SDK SDK_TYPE output,
	@FIRMWARE_ID FIRMWARE_ID_TYPE output,
    @SVN_VERSION SVN_TYPE output,
    @TIME_ZONE TIME_ZONE_TYPE output,
    @DEVICE_NAME DEV_NAME_TYPE output,
    @NOTE NOTE_TYPE output,
    @LIC_ACT_DATE datetime output,
    @CAN_DEACTIVATE bit output,
    @LIC_ID int output
with encryption    
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin        
        select
            @DEVICE_NAME = isnull(D.DEVICE_NAME, D.AGENT_NAME),
            @AGENT_NAME = D.AGENT_NAME,
            @IMEI = isnull(D.IMEI, 'unknown'),
            @PHONE_NUMBER = isnull(D.PHONE_NUMBER, 'unknown'),
            @BRAND = isnull(D.BRAND, 'unknown'),
            @MODEL = isnull(D.MODEL, 'unknown'),
            @OS = isnull(D.OS, 'unknown'),
            @SDK = isnull(D.SDK, 'unknown'),
            @FIRMWARE_ID = isnull(D.FIRMWARE_ID, 'unknown'),
            @SVN_VERSION = isnull(D.SVN_VERSION, 'unknown'),
            @TIME_ZONE = isnull(D.TIME_ZONE, 'unknown'),
            @NOTE = isnull(D.NOTE, ''),
            @LIC_ACT_DATE = LIC_ACT_DATE,
            @LIC_ID = LIC_ID
        from DEVICES D
        where D.ID = @AGENT_ID
    end
    
    exec  IS_DEACTIVATION_ALLOWED @AGENT_ID, @CAN_DEACTIVATE output    
end
go


create procedure GET_EMAILS
    @EMAIL_TYPE int = null
as
begin
    select ID, EMAIL_NAME, EMAIL_TYPE, EMAIL_TEXT_HTML, ALLOWED_VARS
    from EMAIL
    where (@EMAIL_TYPE is null) or (EMAIL_TYPE = @EMAIL_TYPE)
end
go


create procedure GET_EXPIRED_ACCOUNTS
    @PERIOD_DAYS int = 365
with encryption
as
begin
    select UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME
    from USERS_INFO UI
        left join LICENSES L on L.USER_ID = UI.ID
    where ASP_NET_USER_ID is not null
    group by UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME      
    having datediff(day, max(L.SUBSCR_END), getdate()) > @PERIOD_DAYS
end
go


create procedure GET_LICENSES_FOR_EXPIRATION
with encryption
as
begin
    select UI.FIRST_NAME, UI.LAST_NAME, UI.EMAIL, L.ID, L.SUBSCR_END, L.DEV_CNT
    from LICENSES L
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where (SUBSCR_END <= getdate()) 
        and exists (select LIC_ID 
                    from DEVICES D
                    where D.LIC_ID = L.ID)
end
go


create procedure GET_MMS
    @AGENT_ID int = null,
    @TYPE_FLTR int = null,    
    @ADDR_FLTR ADDRESS_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @CONTENT_FLTR nvarchar(4000) = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if( @CONTENT_FLTR is not null)
    begin
        set @CONTENT_FLTR='%' + ltrim(@CONTENT_FLTR)
        set @CONTENT_FLTR=rtrim(@CONTENT_FLTR) + '%'
    end

    if( @ADDR_FLTR is not null)
    begin
        set @ADDR_FLTR='%' + ltrim(@ADDR_FLTR)
        set @ADDR_FLTR=rtrim(@ADDR_FLTR) + '%'
    end

    declare @TZ_DEV_OFFSET datetime

    exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

    select distinct M.ID_MSG, M.MSG_ADDR, (M.MSG_DT + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), M.MSG_DT) as USER_DATE, M.MSG_SUBJ, M.MSG_BODY, T.TYPE_NAME
    from MESSAGES M
        left join MSG_TYPES T on T.ID_TYPE=M.TYPE_ID
    where ((@AGENT_ID is not null) and (M.AGENT_ID_MSG = @AGENT_ID))
        and ((@TYPE_FLTR is null) or (M.TYPE_ID = @TYPE_FLTR))    
        and ((M.MSG_SUBJ is not null) or exists(select A.MSG_ID 
                                                from MMS_ATTACH A 
                                                where A.MSG_ID = M.ID_MSG))
        and ((@ADDR_FLTR is null) or (M.MSG_ADDR like @ADDR_FLTR))
        and ((@BEG_DATE_FLTR is null) or (M.MSG_DT >= @BEG_DATE_FLTR))
        and ((@END_DATE_FLTR is null) or (M.MSG_DT <= @END_DATE_FLTR))
        and ((@CONTENT_FLTR is null) or (M.MSG_BODY like @CONTENT_FLTR)
            or (M.MSG_SUBJ like @CONTENT_FLTR))
    order by DEV_DATE desc
end
go


create procedure GET_MMS_ATTACH @MSG_ID int
with encryption
as
begin
    select A.ID_ATTACH,
           MT.MIME_NAME,
           MT.EXT,
           case when (A.ATTACH_DATA is not NULL) then 'Ok' else 'Error' end as STATUS
    from MMS_ATTACH A
        left join MIME_TYPES MT on MT.ID_MIME=A.ID_MIME    
    where A.MSG_ID = @MSG_ID
end
go


create procedure GET_MSG_TYPES 
with encryption
as
begin
    select ID_TYPE as ID, TYPE_NAME as TNAME
    from MSG_TYPES
    order by ID_TYPE
end
go


create procedure GET_NOT_ACTIVATED_ACCOUNTS
    @START_TIME_HRS int,
    @FINISH_TIME_HRS int = null
with encryption
as
begin
    select UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME
    from USERS_INFO UI
        right join aspnet_Membership M on UI.ASP_NET_USER_ID = M.UserId
    where not exists (select L.ID from LICENSES L where L.USER_ID = UI.ID)
        and (@START_TIME_HRS < datediff(hour, CreateDate, getdate()))
        and ((@FINISH_TIME_HRS is null) or (datediff(hour, CreateDate, getdate()) <= @FINISH_TIME_HRS))
end
go


create procedure GET_PAYMENTS_HISTORY 
    @LIC_ID int = null,
    @USER_ID int = null,
    @DATE_FROM datetime = null,    
    @DATE_TO datetime = null,
    @EMAIL EMAIL_TYPE = null,
    @TRANS_ID SUBSCR_ID_TYPE = null,
    @ID_PAYMENT int = null
with encryption
as
begin
    select P.ID_PAYMENT, P.PAY_DATE, UI.EMAIL, UI.FIRST_NAME, UI.LAST_NAME, P.LABEL,
        P.DEV_CNT, P.PRICE, P.PRICE_OTHER, P.AMOUNT, P.TRANS_ID,
        P.START_DATE, P.FINISH_DATE, P.PAY_COMMENT
    from PAYMENTS_HISTORY P
        left join LICENSES L on L.ID = P.LIC_ID
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where ((@LIC_ID is null) or (LIC_ID = @LIC_ID))
        and ((@USER_ID is null) or (LIC_ID in (select distinct ID 
                                               from LICENSES
                                               where USER_ID = @USER_ID)))
        and ((@EMAIL is null) or (UI.EMAIL like '%' + @EMAIL + '%'))
        and ((@TRANS_ID is null) or (P.TRANS_ID like '%' + @TRANS_ID + '%'))
        and ((@DATE_FROM is null) or (@DATE_FROM <= PAY_DATE))
        and ((@DATE_TO is null) or (PAY_DATE <= @DATE_TO))
        and ((@ID_PAYMENT is null) or (P.ID_PAYMENT = @ID_PAYMENT))
    order by PAY_DATE desc
end
go


create procedure GET_PRICE_LIST 
    @PAYMENT_DATE datetime = null,
    @PAY_TYPE smallint = null
with encryption    
as
begin    
    if(@PAYMENT_DATE is null)
    begin
        set @PAYMENT_DATE = getdate()
    end

    select ID, LABEL, DURATION, PAY_TYPE, PRICE, PRICE_OTHER, 
        dateadd(day, DURATION, @PAYMENT_DATE) as SUBSCR_END, DATE_FROM
    from ACTIVE_PRICE_LIST    
    where ((@PAY_TYPE is null) or (PAY_TYPE=@PAY_TYPE))
end
go


create procedure GET_SMS
    @AGENT_ID int = null,
    @TYPE_FLTR int = null,
    @ADDR_FLTR ADDRESS_TYPE = null,
    @BEG_DATE_FLTR datetime = null,
    @END_DATE_FLTR datetime = null,
    @CONTENT_FLTR nvarchar(4000) = null,
    @TZ_USER_OFFSET int = null
with encryption
as
begin
    if( @CONTENT_FLTR is not null)
    begin
        set @CONTENT_FLTR='%' + ltrim(@CONTENT_FLTR)
        set @CONTENT_FLTR=rtrim(@CONTENT_FLTR) + '%'
    end

    if( @ADDR_FLTR is not null)
    begin
        set @ADDR_FLTR='%' + ltrim(@ADDR_FLTR)
        set @ADDR_FLTR=rtrim(@ADDR_FLTR) + '%'
    end

    declare @TZ_DEV_OFFSET datetime

    exec GET_DEVICE_TIME_ZONE_OFFSET @AGENT_ID, @TZ_DEV_OFFSET output

    select distinct S.SMS_ADDR, (S.SMS_DT + @TZ_DEV_OFFSET) as DEV_DATE, dateadd(mi, isnull(@TZ_USER_OFFSET, 0), S.SMS_DT) as USER_DATE, S.SMS_BODY, T.TYPE_NAME
    from SMS S
        left join MSG_TYPES T on T.ID_TYPE=S.TYPE_ID
    where ((@AGENT_ID is not null) and (S.AGENT_ID_SMS = @AGENT_ID))
        and ((@TYPE_FLTR is null) or (S.TYPE_ID = @TYPE_FLTR))
        and ((@ADDR_FLTR is null) or (S.SMS_ADDR like @ADDR_FLTR))
        and ((@BEG_DATE_FLTR is null) or (S.SMS_DT >= @BEG_DATE_FLTR))
        and ((@END_DATE_FLTR is null) or (S.SMS_DT <= @END_DATE_FLTR))
        and ((@CONTENT_FLTR is null) or (S.SMS_BODY like @CONTENT_FLTR))
    order by DEV_DATE desc
end
go


CREATE procedure GET_TOKEN_INFO
    @TOKEN VARCHAR(100) = null
with encryption    
as
begin
    if (@TOKEN is not null)
    begin
        select EMAIL
        from TOKENS
        where TOKEN = @TOKEN AND EXPIRATION_DATE > GETUTCDATE()
    end
end
go


create procedure GET_UNSUBSCR_LICENSES
    @PL_ID int
with encryption    
as
begin
    select U.UserName as EMAIL, UI.FIRST_NAME, UI.LAST_NAME, L.ID as LIC_ID, L.SUBSCR_ID, L.PAY_SYS_TYPE
    from ACTIVE_PRICE_LIST APL
        left join LICENSES L on L.PRICE_LIST_ID = APL.ID
        left join USERS_INFO UI on UI.ID = L.USER_ID
        left join aspnet_Users U on U.UserId = UI.ASP_NET_USER_ID
    where (APL.ID = @PL_ID) and (L.SUBSCR_ID is not null)
    order by LIC_ID desc
end
go


create procedure GET_USERS_LICENSES
    @USER_ID int = null,
    @EXPIRE_DAYS int = null
with encryption
as
begin
    select UI.FIRST_NAME, UI.LAST_NAME, UI.EMAIL, L.CREATION_DATE, L.ID, 
        L.PRICE_LIST_ID, L.SUBSCR_END, L.DEV_CNT, L.PAY_SYS_TYPE, 
        L.SUBSCR_ID, (L.DEV_CNT - (select count(D.ID) 
                                   from DEVICES D
                                   where D.LIC_ID = L.ID)) as FREE_DEV_COUNT
    from LICENSES L
        left join USERS_INFO UI on UI.ID = L.USER_ID
    where (((L.USER_ID = @USER_ID) or (@USER_ID is null))
        and ((datediff(day, getdate(), L.SUBSCR_END) <= @EXPIRE_DAYS) or (@EXPIRE_DAYS is null)))
    group by L.ID, L.PRICE_LIST_ID, L.SUBSCR_END, L.DEV_CNT, L.PAY_SYS_TYPE, 
        L.SUBSCR_ID, UI.FIRST_NAME, UI.LAST_NAME, UI.EMAIL, L.CREATION_DATE
    order by L.SUBSCR_END desc
end
go


create procedure GET_USER_ID_BY_GUID
    @ASP_USER_ID uniqueidentifier = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        select ID
        from USERS_INFO
        where ASP_NET_USER_ID = @ASP_USER_ID
    end
end
go


create procedure GET_USER_INFO
    @ASP_USER_ID uniqueidentifier = null,
    @SUBSCR_ID SUBSCR_ID_TYPE =  null
with encryption
as
begin
    if ((@ASP_USER_ID is null) and (@SUBSCR_ID is null))
    begin
        return;
    end

    select ID, FIRST_NAME, LAST_NAME, EMAIL
    from USERS_INFO
    where ((@ASP_USER_ID is null) or (ASP_NET_USER_ID = @ASP_USER_ID))
        and ((@SUBSCR_ID is null) or (ID in (select L.USER_ID
                                             from LICENSES L
                                             where L.SUBSCR_ID = @SUBSCR_ID)))
end
go


create procedure INIT_PRICE_LIST
with encryption
as
    declare @PL_ID int
begin
    set nocount on;

    delete from PRICE_HISTORY
    delete from PRICE_LIST

    exec ADD_NEW_PRICE_LIST_ITEM 'Signup Fee', 30, 1, @PL_ID output
    exec ADD_NEW_PRICE @PL_ID, 9.99

    exec ADD_NEW_PRICE_LIST_ITEM '30 days', 30, 2, @PL_ID output
    exec ADD_NEW_PRICE @PL_ID, 9.99, 9.99

    exec ADD_NEW_PRICE_LIST_ITEM '90 days', 90, 2, @PL_ID output
    exec ADD_NEW_PRICE @PL_ID, 24.99, 24.99

    exec ADD_NEW_PRICE_LIST_ITEM '180 days', 180, 2, @PL_ID output
    exec ADD_NEW_PRICE @PL_ID, 47.99, 47.99

    exec ADD_NEW_PRICE_LIST_ITEM '365 days', 365, 2, @PL_ID output
    exec ADD_NEW_PRICE @PL_ID, 79.99, 79.99
end
go


create procedure IS_LICENSED_DEVICE
    @AGENT_ID int = null,
    @IS_LICENSED bit output
with encryption    
as
    declare @LIC_ID int
    declare @SUBSCR_END datetime
begin
    set nocount on;

    set @IS_LICENSED = 0;

    if (@AGENT_ID is not null)
    begin
        select @LIC_ID = D.LIC_ID
        from DEVICES D
        where D.ID = @AGENT_ID;
        
        if( @LIC_ID is not null )
        begin
            select @SUBSCR_END = L.SUBSCR_END
            from LICENSES L
            where L.ID = @LIC_ID

            if( (@SUBSCR_END is not null) and (@SUBSCR_END >= getdate()) )
            begin
                set @IS_LICENSED = 1;
            end
        end
    end
end
go


create procedure IS_TRANSACTION_PROCESSED
    @TRANSACTION_ID SUBSCR_ID_TYPE,
    @PROCESSED bit output
with encryption
as
    declare @TRANSACTION_COUNT int
begin
    set nocount on;
 
    set @PROCESSED = 0;

    select @TRANSACTION_COUNT = count(*) 
    from PAYMENTS_HISTORY
    where TRANS_ID = @TRANSACTION_ID

    if( @TRANSACTION_COUNT > 0 )
    begin
        set @PROCESSED = 1;
    end
end
go


create procedure REFRESGH_AGENTS_LAST_UPDATE_TIME
    @AGENT_ID int = null
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        update DEVICES
        set
            IS_ACTIVED = 1,
            LAST_UPDATED = getdate()
        where ID = @AGENT_ID
    end
end
go


create procedure SET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE = null,
    @DEVICE_NAME DEV_NAME_TYPE = null,
    @NOTE NOTE_TYPE = null,
    @PHONE_NUMBER PHONE_TYPE = null,
    @BRAND BRAND_TYPE  = null,
    @MODEL MODEL_TYPE = null,
    @OS OS_TYPE = null,
    @SDK SDK_TYPE = null,
	@FIRMWARE_ID FIRMWARE_ID_TYPE = null,
    @SVN_VERSION SVN_TYPE = NULL,
    @TIME_ZONE TIME_ZONE_TYPE = NULL,
    @TZ_OFFSET int = 0
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        declare @BRAND_OLD_VAL BRAND_TYPE;
        declare @MODEL_OLD_VAL MODEL_TYPE;
        
        select @BRAND_OLD_VAL = D.BRAND, @MODEL_OLD_VAL = D.MODEL
        from DEVICES D
        where D.ID = @AGENT_ID;
              
        update DEVICES
        set
            DEVICES.AGENT_NAME = isnull(@AGENT_NAME, DEVICES.AGENT_NAME),
            DEVICES.DEVICE_NAME = isnull(@DEVICE_NAME, DEVICES.DEVICE_NAME),
            DEVICES.NOTE = isnull(@NOTE, DEVICES.NOTE),
            DEVICES.PHONE_NUMBER = isnull(@PHONE_NUMBER, DEVICES.PHONE_NUMBER),
            DEVICES.BRAND = isnull(@BRAND, DEVICES.BRAND),
            DEVICES.MODEL = isnull(@MODEL, DEVICES.MODEL),
            DEVICES.OS = isnull(@OS, DEVICES.OS),
            DEVICES.SDK = isnull(@SDK, DEVICES.SDK),
            DEVICES.FIRMWARE_ID = isnull(@FIRMWARE_ID, DEVICES.FIRMWARE_ID),
            DEVICES.SVN_VERSION = isnull(@SVN_VERSION, DEVICES.SVN_VERSION),
            DEVICES.TIME_ZONE = isnull(@TIME_ZONE, DEVICES.TIME_ZONE),
            DEVICES.TZ_OFFSET = isnull(@TZ_OFFSET, DEVICES.TZ_OFFSET)
        where DEVICES.ID = @AGENT_ID
        
        if ((@BRAND_OLD_VAL is null) and (@MODEL_OLD_VAL is null))
        begin
            UPDATE DEVICES
            SET 
              DEVICES.DEVICE_NAME = isnull(cast((rtrim(DEVICES.BRAND) + ' ' + rtrim(DEVICES.MODEL)) as nchar(20)), DEVICES.AGENT_NAME)
            where (DEVICES.ID = @AGENT_ID) and (DEVICES.AGENT_NAME = DEVICES.DEVICE_NAME)
        end        
    end
end
go


create procedure SET_LICENSE_SUBSCR_ID
    @LIC_ID int = null,
    @OLD_SUBSCR_ID SUBSCR_ID_TYPE = null,
    @NEW_SUBSCR_ID SUBSCR_ID_TYPE = null  -- null if cancelling subscription
with encryption    
as    
begin
    set nocount on;

    if((@LIC_ID is null) and (@OLD_SUBSCR_ID is null))
    begin
        return
    end

	update LICENSES
	set SUBSCR_ID = @NEW_SUBSCR_ID
	where ((ID = @LIC_ID) or (@LIC_ID is null))
        and ((SUBSCR_ID = @OLD_SUBSCR_ID) or (@OLD_SUBSCR_ID is null))
end
go


create procedure UPDATE_EMAIL_CONTENT
    @EMAIL_TYPE int,
    @EMAIL_TEXT_HTML nvarchar(4000)
as
begin
    update EMAIL
    set EMAIL_TEXT_HTML = @EMAIL_TEXT_HTML
    where EMAIL_TYPE = @EMAIL_TYPE
end
go


create procedure UPDATE_USER_INFO
    @ASP_USER_ID uniqueidentifier = null,
    @FIRST_NAME nvarchar(50) = null,
    @LAST_NAME nvarchar(50) = null,
    @EMAIL EMAIL_TYPE = null
with encryption
as
begin
    if (@ASP_USER_ID is not null)
    begin
        if ((@FIRST_NAME is not null) and (@LAST_NAME is not null))
        begin
			update USERS_INFO
			set FIRST_NAME = @FIRST_NAME,
				LAST_NAME = @LAST_NAME
			where ASP_NET_USER_ID = @ASP_USER_ID
	    end
        if (@EMAIL is not null)
        begin
			update USERS_INFO
			set EMAIL = @EMAIL
			where ASP_NET_USER_ID = @ASP_USER_ID
        end
    end    
end
go

