@echo HAKWK Database  creating...
@CreateDB.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK
@echo Database created
@echo HAKWK Database structure creating...
@runSQL.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK .\CreateHAWKDatabaseStructure.sql
@echo Structure created
@echo HAKWK Database users creating...
@runSQL.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK .\CreateUsers.sql
@echo Users created
@pause
