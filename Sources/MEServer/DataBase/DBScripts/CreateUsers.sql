/* 
---===###   Don't use it: you will use -d option of the sqlcmd.exe utility to specify of the database name   ###===---
use HAWK;
go
*/

/* * * * * * * * * * * * * * * *
 * -=-  Drop exist logins  -=- *
 * * * * * * * * * * * * * * * */

if  EXISTS (select * from SYS.DATABASE_PRINCIPALS where NAME = N'HAWKWebServiceUser')
drop user HAWKWebServiceUser;

if  EXISTS (select * from SYS.SERVER_PRINCIPALS where NAME = N'HAWKWebServiceUser')
drop login HAWKWebServiceUser;

if  EXISTS (select * from SYS.DATABASE_PRINCIPALS where NAME = N'HAWKWebSiteUser')
drop user HAWKWebSiteUser;

if  EXISTS (select * from SYS.SERVER_PRINCIPALS where NAME = N'HAWKWebSiteUser')
drop login HAWKWebSiteUser;

if  EXISTS (select * from SYS.DATABASE_PRINCIPALS where NAME = N'HAWKPeriodicEventsUser')
drop user HAWKPeriodicEventsUser;

if  EXISTS (select * from SYS.SERVER_PRINCIPALS where NAME = N'HAWKPeriodicEventsUser')
drop login HAWKPeriodicEventsUser;


/* * * * * * * * * * * * * * * *
 * -=-  Create HAWK users  -=- *
 * * * * * * * * * * * * * * * */

create login HAWKWebServiceUser with password='MEVeryStrongPasswordWith12345And_', 
                                     default_database=[HAWK], default_language=[us_english], check_expiration=OFF, CHECK_POLICY=OFF;
go

create user HAWKWebServiceUser for login HAWKWebServiceUser;
go

create login HAWKWebSiteUser with password='MEVeryStrongPasswordWith12345And_', 
                                     default_database=[HAWK], default_language=[us_english], check_expiration=OFF, CHECK_POLICY=OFF;
go

create user HAWKWebSiteUser for login HAWKWebSiteUser;
go

create login HAWKPeriodicEventsUser with password='MEVeryStrongPasswordWith12345And_', 
                                     default_database=[HAWK], default_language=[us_english], check_expiration=OFF, CHECK_POLICY=OFF;
go

create user HAWKPeriodicEventsUser for login HAWKPeriodicEventsUser;
go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -=-  Application and roles generation (ASP Membership)  -=- *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

-- -=#  Application  #=-
declare @ApplicationId uniqueidentifier

select @ApplicationId=ApplicationId
from aspnet_Applications
where (ApplicationName = '/') and (LoweredApplicationName = '/');

if (@ApplicationId is null)
begin
    set @ApplicationId = 'D292C95E-365A-411F-9D7F-EFC76165DEFA';
    insert into aspnet_Applications (ApplicationName, LoweredApplicationName, ApplicationId, Description)
    values ('/', '/', @ApplicationId, NULL);
end

-- -=#  Roles  #=-
if (not exists(select * from aspnet_Roles where (RoleName='admin') and (LoweredRoleName='admin')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'3358285A-340C-4C6B-BC1D-EA901F969ABC','admin','admin',NULL);
end 

if (not exists(select * from aspnet_Roles where (RoleName='paid') and (LoweredRoleName='paid')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'3E4630EF-E94C-4470-80E9-1E8FD1A48327','paid','paid',NULL);
end

if (not exists(select * from aspnet_Roles where (RoleName='user') and (LoweredRoleName='user')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'8362EF01-5A09-47FC-88BA-A2658A4C8BBA','user','user',NULL);
end

if (not exists(select * from aspnet_Roles where (RoleName='confirmed') and (LoweredRoleName='confirmed')))
begin
    insert into aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
    values (@ApplicationId,'0F3338D3-A6AC-4C00-B203-FF22D4E3BD83','confirmed','confirmed',NULL);
end

-- -=#  Admin user  #=-
declare @ADMIN_USER_UID uniqueidentifier;
declare @UserName nvarchar(256);
declare @UserEmail nvarchar(256);
declare @CURRENT_TIME datetime;
declare @CURRENT_TIME_UTC datetime;

set @UserName = 'admin';
set @UserEmail = 'admin@paraben.com';
set @CURRENT_TIME = getdate();
set @CURRENT_TIME_UTC = getutcdate();

exec dbo.aspnet_Membership_CreateUser '/', @UserName, 
    'XGOrHsipLyVKDWo1Rlcl0XCfIuw=', 'O08G7KnXON6qBwhjWD2bTA==', -- DefaultPassword: Admin$321_456!#
    @UserEmail, NULL, NULL, 1, @CURRENT_TIME_UTC, @CURRENT_TIME, 0, 1, 
    @ADMIN_USER_UID output;

exec aspnet_UsersInRoles_AddUsersToRoles '/', @UserName, 'Admin', @CURRENT_TIME_UTC;

declare @ID int;

exec ADD_USER_INFO @ADMIN_USER_UID, 'Site', 'Admin', @UserEmail, @ID output;

go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * -=-  Add grants for connecton to HAWK database  -=- *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

grant connect to HAWKWebServiceUser;
go

grant connect to HAWKWebSiteUser;
go

grant connect to HAWKPeriodicEventsUser;
go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -=-  Add grants for execution of stored procedures  -=- *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

grant execute on ADD_AGENT to HAWKWebSiteUser
go

grant execute on ADD_AGENT to HAWKWebServiceUser
go

grant execute on ADD_CALL to HAWKWebServiceUser
go

grant execute on ADD_COORDINATE to HAWKWebServiceUser
go

grant execute on ADD_CRASH_LOG to HAWKWebServiceUser
go

grant execute on ADD_MMS to HAWKWebServiceUser
go

grant execute on ADD_MMS_ATTACH to HAWKWebServiceUser
go

grant execute on ADD_MMS_ATTACH_TEMPL to HAWKWebServiceUser
go

grant execute on ADD_TOKEN to HAWKWebSiteUser
go

grant execute on ADD_SMS to HAWKWebServiceUser
go

grant execute on ADD_USER_INFO to HAWKWebSiteUser
go

grant execute on CLEAN_TOKENS to HAWKWebSiteUser
go

grant execute on DELETE_CALLS to HAWKWebSiteUser
go

grant execute on DELETE_DEVICE to HAWKWebSiteUser
go

grant execute on DELETE_DEVICE to HAWKWebServiceUser
go

grant execute on DELETE_TOKEN to HAWKWebSiteUser
go

grant execute on DELETE_USER_INFO to HAWKWebSiteUser
go

grant execute on GET_AGENTS_LAST_UPDATE_TIME to HAWKWebSiteUser
go

grant execute on GET_ACTUAL_AGENT_ID to HAWKWebServiceUser
go

grant execute on GET_ACTUAL_AGENT_ID to HAWKWebSiteUser
go

grant execute on GET_AGENTS_BY_GUID to HAWKWebSiteUser
go

grant execute on GET_AGENTS_BY_GUID to HAWKWebServiceUser
go

grant execute on GET_AGENTS_COUNT to HAWKWebSiteUser
go

grant execute on GET_AGENTS_COUNT_BY_GUID to HAWKWebSiteUser
go

grant execute on GET_ATTACH to HAWKWebSiteUser
go

grant execute on GET_CALLS to HAWKWebSiteUser
go

grant execute on GET_COORDINATES to HAWKWebSiteUser
go

grant execute on GET_CRASH_LOGS to HAWKWebSiteUser
go

grant execute on GET_DEVICE_INFO to HAWKWebSiteUser
go

grant execute on GET_DEVICES to HAWKWebSiteUser
go

grant execute on GET_DEVICE_TIME_ZONE_OFFSET to HAWKWebSiteUser
go

grant execute on GET_MSG_TYPES to HAWKWebSiteUser
go

grant execute on GET_MMS to HAWKWebSiteUser
go

grant execute on GET_MMS_ATTACH to HAWKWebSiteUser
go

grant execute on GET_NUMBER_TYPE_ID to HAWKWebSiteUser
go

grant execute on GET_TOKEN_INFO to HAWKWebSiteUser
go

grant execute on GET_PHONE_ID to HAWKWebSiteUser
go

grant execute on GET_SMS to HAWKWebSiteUser
go

grant execute on GET_USER_ID_BY_GUID to HAWKWebSiteUser
go

grant execute on GET_USER_ID_BY_GUID to HAWKWebServiceUser
go

grant execute on GET_USER_INFO to HAWKWebSiteUser
go

grant execute on GET_USER_INFO to HAWKWebServiceUser
go

grant execute on REFRESGH_AGENTS_LAST_UPDATE_TIME to HAWKWebServiceUser
go

grant execute on SET_DEVICE_INFO to HAWKWebServiceUser
go

grant execute on SET_DEVICE_INFO to HAWKWebSiteUser
go

grant execute on UPDATE_USER_INFO to HAWKWebSiteUser
go

grant execute on GET_PROMO_CODE_HISTORY_PAGED to HAWKWebSiteUser
go

grant execute on ACTIVATE_PROMO_CODE to HAWKWebSiteUser
go

grant execute on DELETE_POMO_CODE to HAWKWebSiteUser
go

grant execute on DISABLE_PROMO_CODE to HAWKWebSiteUser
go

grant execute on ADD_PROMO_CODE to HAWKWebSiteUser
go

grant execute on GET_DEVICES_PAGED to HAWKWebSiteUser
go

grant execute on GET_DEVICES_PAGED to HAWKWebServiceUser
go

grant execute on PAGER to HAWKWebSiteUser
go

grant execute on PAGER to HAWKWebServiceUser
go


-- - - - - - - - - - - - 
--   Licensing part   --
-- - - - - - - - - - - - 
grant execute on IS_LICENSED_DEVICE to HAWKWebServiceUser
go

grant execute on ACTIVATE_DEVICE to HAWKWebSiteUser
go

grant execute on DEACTIVATE_DEVICE to HAWKWebSiteUser
go

grant execute on GET_USERS_LICENSES to HAWKWebSiteUser
go

grant execute on ADD_NEW_PRICE_LIST_ITEM to HAWKWebSiteUser
go

grant execute on ADD_NEW_PRICE to HAWKWebSiteUser
go

grant execute on GET_LICENSE to HAWKWebSiteUser
go

grant execute on FINALIZE_AUTO_PAYMENT to HAWKWebSiteUser
go

grant execute on SET_LICENSE_SUBSCR_ID to HAWKWebSiteUser
go

grant execute on GET_PRICE_LIST_ITEM to HAWKWebSiteUser
go

grant execute on CALCULATE_PAYMENT to HAWKWebSiteUser
go

grant execute on FINALIZE_PAYMENT to HAWKWebSiteUser
go

grant execute on GET_PRICE_LIST to HAWKWebSiteUser
go

grant execute on GET_PAYMENTS_HISTORY to HAWKWebSiteUser
go

grant execute on GET_UNSUBSCR_LICENSES to HAWKWebSiteUser
go

grant execute on GET_EMAILS to HAWKWebSiteUser
go

grant execute on UPDATE_EMAIL_CONTENT to HAWKWebSiteUser
go

grant execute on IS_DEACTIVATION_ALLOWED to HAWKWebSiteUser
go

grant execute on IS_TRANSACTION_PROCESSED to HAWKWebSiteUser
go

-- - - - - - - - - - - - - 
--   Periodic Events part
-- - - - - - - - - - - - - 

grant execute on CLEAN_USERS_DATA to HAWKPeriodicEventsUser
go

grant execute on CLEAN_CRASH_LOGS to HAWKPeriodicEventsUser
go

grant execute on DEACTIVATE_LICENSE to HAWKPeriodicEventsUser
go

grant execute on GET_LICENSES_FOR_EXPIRATION to HAWKPeriodicEventsUser
go

grant execute on GET_NOT_ACTIVATED_ACCOUNTS to HAWKPeriodicEventsUser
go

grant execute on GET_USERS_LICENSES to HAWKPeriodicEventsUser
go

grant execute on GET_EMAILS to HAWKPeriodicEventsUser
go

grant execute on GET_PRICE_LIST to HAWKPeriodicEventsUser
go

grant execute on GET_EXPIRED_ACCOUNTS to HAWKPeriodicEventsUser
go

-- - - - - - - - - - - - - 
--   ASP Membership part 
-- - - - - - - - - - - - - 

grant execute on aspnet_Setup_RestorePermissions to HAWKWebSiteUser
go

grant execute on aspnet_Setup_RemoveAllRoleMembers to HAWKWebSiteUser
go

grant execute on aspnet_RegisterSchemaVersion to HAWKWebSiteUser
go

grant execute on aspnet_CheckSchemaVersion to HAWKWebSiteUser
go

grant execute on aspnet_CheckSchemaVersion to HAWKWebServiceUser
go

grant execute on aspnet_Applications_CreateApplication to HAWKWebSiteUser
go

grant execute on aspnet_UnRegisterSchemaVersion to HAWKWebSiteUser
go

grant execute on aspnet_Users_CreateUser to HAWKWebSiteUser
go

grant execute on aspnet_Users_DeleteUser to HAWKWebSiteUser
go

grant execute on aspnet_AnyDataInTables to HAWKWebSiteUser
go

grant execute on aspnet_Membership_CreateUser to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetUserByUserId to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetUserByEmail to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetPasswordWithFormat to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetPasswordWithFormat to HAWKWebServiceUser
go

grant execute on aspnet_Membership_UpdateUserInfo to HAWKWebSiteUser
go

grant execute on aspnet_Membership_UpdateUserInfo to HAWKWebServiceUser
go

grant execute on aspnet_Membership_GetPassword to HAWKWebSiteUser
go

grant execute on aspnet_Membership_SetPassword to HAWKWebSiteUser
go

grant execute on aspnet_Membership_ResetPassword to HAWKWebSiteUser
go

grant execute on aspnet_Membership_UnlockUser to HAWKWebSiteUser
go

grant execute on aspnet_Membership_UpdateUser to HAWKWebSiteUser
go

grant execute on aspnet_Membership_ChangePasswordQuestionAndAnswer to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetAllUsers to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetNumberOfUsersOnline to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetUserByName to HAWKWebSiteUser
go

grant execute on aspnet_Membership_GetUserByName to HAWKWebServiceUser
go

grant execute on aspnet_Membership_FindUsersByName to HAWKWebSiteUser
go

grant execute on aspnet_Membership_FindUsersByEmail to HAWKWebSiteUser
go

grant execute on aspnet_Profile_GetProperties to HAWKWebSiteUser
go

grant execute on aspnet_Profile_SetProperties to HAWKWebSiteUser
go

grant execute on aspnet_Profile_DeleteProfiles to HAWKWebSiteUser
go

grant execute on aspnet_Profile_DeleteInactiveProfiles to HAWKWebSiteUser
go

grant execute on aspnet_Profile_GetNumberOfInactiveProfiles to HAWKWebSiteUser
go

grant execute on aspnet_Profile_GetProfiles to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_GetRolesForUser to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_GetRolesForUser to HAWKWebSiteUser
go

grant execute on aspnet_Roles_CreateRole to HAWKWebSiteUser
go

grant execute on aspnet_Roles_DeleteRole to HAWKWebSiteUser
go

grant execute on aspnet_Roles_RoleExists to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_AddUsersToRoles to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_RemoveUsersFromRoles to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_RemoveUsersFromRoles to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_GetUsersInRoles to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_FindUsersInRole to HAWKWebSiteUser
go

grant execute on aspnet_Roles_GetAllRoles to HAWKWebSiteUser
go

grant execute on aspnet_Personalization_GetApplicationId to HAWKWebSiteUser
go

grant execute on aspnet_Paths_CreatePath to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAllUsers_GetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAllUsers_ResetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAllUsers_SetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationPerUser_GetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationPerUser_ResetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationPerUser_SetPageSettings to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAdministration_DeleteAllState to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAdministration_ResetSharedState to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAdministration_ResetUserState to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAdministration_FindState to HAWKWebSiteUser
go

grant execute on aspnet_PersonalizationAdministration_GetCountOfState to HAWKWebSiteUser
go

grant execute on aspnet_WebEvent_LogEvent to HAWKWebSiteUser
go

grant execute on aspnet_UsersInRoles_IsUserInRole to HAWKWebSiteUser
go

grant select on vw_aspnet_Users to HAWKWebSiteUser
go

grant select on vw_aspnet_MembershipUsers to HAWKWebSiteUser
go

grant select on vw_aspnet_Applications to HAWKWebSiteUser
go

grant select on vw_aspnet_Profiles to HAWKWebSiteUser
go

grant select on vw_aspnet_Roles to HAWKWebSiteUser
go

grant select on vw_aspnet_UsersInRoles to HAWKWebSiteUser
go

grant select on vw_aspnet_WebPartState_Paths to HAWKWebSiteUser
go

grant select on vw_aspnet_WebPartState_Shared to HAWKWebSiteUser
go

grant select on vw_aspnet_WebPartState_User to HAWKWebSiteUser
go

-- - - - - - - - - - - - - - - - -

grant execute on aspnet_Setup_RestorePermissions to HAWKPeriodicEventsUser
go

grant execute on aspnet_Setup_RemoveAllRoleMembers to HAWKPeriodicEventsUser
go

grant execute on aspnet_RegisterSchemaVersion to HAWKPeriodicEventsUser
go

grant execute on aspnet_CheckSchemaVersion to HAWKPeriodicEventsUser
go

grant execute on aspnet_Applications_CreateApplication to HAWKPeriodicEventsUser
go

grant execute on aspnet_UnRegisterSchemaVersion to HAWKPeriodicEventsUser
go

grant execute on aspnet_Users_CreateUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_Users_DeleteUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_AnyDataInTables to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_CreateUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetUserByUserId to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetUserByEmail to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetPasswordWithFormat to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_UpdateUserInfo to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetPassword to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_SetPassword to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_ResetPassword to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_UnlockUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_UpdateUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_ChangePasswordQuestionAndAnswer to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetAllUsers to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetNumberOfUsersOnline to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_GetUserByName to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_FindUsersByName to HAWKPeriodicEventsUser
go

grant execute on aspnet_Membership_FindUsersByEmail to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_GetProperties to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_SetProperties to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_DeleteProfiles to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_DeleteInactiveProfiles to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_GetNumberOfInactiveProfiles to HAWKPeriodicEventsUser
go

grant execute on aspnet_Profile_GetProfiles to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_GetRolesForUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_GetRolesForUser to HAWKPeriodicEventsUser
go

grant execute on aspnet_Roles_CreateRole to HAWKPeriodicEventsUser
go

grant execute on aspnet_Roles_DeleteRole to HAWKPeriodicEventsUser
go

grant execute on aspnet_Roles_RoleExists to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_AddUsersToRoles to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_RemoveUsersFromRoles to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_RemoveUsersFromRoles to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_GetUsersInRoles to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_FindUsersInRole to HAWKPeriodicEventsUser
go

grant execute on aspnet_Roles_GetAllRoles to HAWKPeriodicEventsUser
go

grant execute on aspnet_Personalization_GetApplicationId to HAWKPeriodicEventsUser
go

grant execute on aspnet_Paths_CreatePath to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAllUsers_GetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAllUsers_ResetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAllUsers_SetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationPerUser_GetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationPerUser_ResetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationPerUser_SetPageSettings to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAdministration_DeleteAllState to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAdministration_ResetSharedState to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAdministration_ResetUserState to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAdministration_FindState to HAWKPeriodicEventsUser
go

grant execute on aspnet_PersonalizationAdministration_GetCountOfState to HAWKPeriodicEventsUser
go

grant execute on aspnet_WebEvent_LogEvent to HAWKPeriodicEventsUser
go

grant execute on aspnet_UsersInRoles_IsUserInRole to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_Users to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_MembershipUsers to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_Applications to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_Profiles to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_Roles to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_UsersInRoles to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_WebPartState_Paths to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_WebPartState_Shared to HAWKPeriodicEventsUser
go

grant select on vw_aspnet_WebPartState_User to HAWKPeriodicEventsUser
go





-- - - - - - - - - - - - - - - - -
--   ASP Entities Framework part 
-- - - - - - - - - - - - - - - - -
-- USERS_INFO
--    GRANT SELECT to [name]
--    GRANT DELETE to [name]

-- EMAIL
--    GRANT INSERT to [name]
--    GRANT SELECT to [name]
--    GRANT UPDATE to [name]
--    GRANT DELETE to [name]


--grant execute on COUNT_EX to HAWKWebSiteUser
--go
-- 
-- EXEC sys.sp_addsrvrolemember @loginame = N'MobileEnterpriseService', @rolename = N'sysadmin'
--go

--SN>USE [master]
--SN>CREATE LOGIN [name] WITH PASSWORD=N'pass', DEFAULT_DATABASE=[DATABASE_NAME],
--SN>    CHECK_EXPIRATION=ON, CHECK_POLICY=ON

--USE [DATABASE_NAME] CREATE USER [name] FOR LOGIN [name] 
--    ALTER USER [name] WITH DEFAULT_SCHEMA=[dbo]
--    EXEC sp_addrolemember N'db_accessadmin', N'name'
--    EXEC sp_addrolemember N'db_datareader', N'name'
--    EXEC sp_addrolemember N'db_datawriter', N'name'
--    GRANT INSERT to [name]
--    GRANT SELECT to [name]
--    GRANT UPDATE to [name]

