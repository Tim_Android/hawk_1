/* 
---===###   Don't use it: you will use -d option of the sqlcmd.exe utility to specify of the database name   ###===---
use HAWK;
go
*/

-- link to the aspnet_Users table
alter table USERS_INFO with check add constraint FK_USERS_INFO_aspnet_Users foreign key(ASP_NET_USER_ID)
    references aspnet_Users (UserId)
        on update cascade 
        on delete set null
go

-- Price list initialization
exec INIT_PRICE_LIST
go
