@echo off || nul
rem  Creating empty database
rem  %1 - SQL server instance
rem  %1 -

cls
echo ***************************************************
echo CreateDB.cmd Server Login Password
echo ***************************************************
echo   Server - Name/IP of SQL Server
echo ***************************************************
echo                           -
echo ***************************************************
echo *   - - = = # #   PARAMETERS LIST   # # = = - -   *
echo ***************************************************
 
echo ***************************************************
echo || nul
sqlcmd -S %1 -U %2 -P %3 -Q "create database %4;"