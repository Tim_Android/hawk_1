rem -                                                        -
rem ----------------------------------------------------------
rem ---===###   1. Create emplty database for HAWK   ###===---
rem ----------------------------------------------------------
@CreateDB.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK
@echo Database created
pause
rem ______________________________________________________
rem -                                                              
rem ----------------------------------------------------------------
rem ---===###   2. Create the database struncture (HAWK)   ###===---
rem ----------------------------------------------------------------
copy ..\CreateHAWKDatabaseStructure.sql .
pause
runSQL.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK .\CreateHAWKDatabaseStructure.sql
@echo Database created
pause
rem _________________________________________________________________
rem -                                                                        
rem --------------------------------------------------------------------------
rem ---===###   3. Create the database struncture (ASP membership)   ###===---
rem --------------------------------------------------------------------------
set FRAMEWORK=C:\Windows\Microsoft.NET\Framework\v2.0.50727
rem %FRAMEWORK%\aspnet_regsql.exe /? > options.txt
%FRAMEWORK%\aspnet_regsql.exe -sqlexportonly crebasASP.sql -A all -d HAWK
pause
runSQL.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK .\crebasASP.sql
rem -                                                                            
rem ------------------------------------------------------------------------------
rem ---===###   4. Grant access ror the web-site and web-sevices users   ###===---
rem ------------------------------------------------------------------------------
rem -               
runSQL.cmd hawk-srv\sqlexpress sa MobileEnterprise HAWK .\CreateUsers.sql
pause