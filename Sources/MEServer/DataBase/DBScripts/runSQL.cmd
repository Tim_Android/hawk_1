@echo off || nul
cls
echo ***************************************************
echo  runSQL.cmd Server Login Password Database Script
echo ***************************************************
echo Parameters:
echo   Server    - Name/IP of SQL Server (%1)
echo   Login     - Admin login (%2)
echo   Password  - Admin password (%3)
echo   Database  - Database name (%4)
echo   Script    - Input *.sql file (%5)
echo ***************************************************
sqlcmd -S %1 -U %2 -P %3 -d %4 -i %5
