﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Web.UI;
using System.Configuration;
using Domain.Entities.WebServices.Wcf;

namespace Hawk.Infrastructure.BLL
{
    public static class Utils
    {
        const string SECURED_PORT = "443";
        public static string GetRandomString(int count)
        {
            StringBuilder sb = new StringBuilder();
            char[] charList = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
            Random r = new Random();
            for (int i = 0; i < count; i++)
            {
                sb.Append(charList[r.Next(0, charList.Length - 1)]);
            }
            return sb.ToString();
        }

        public static string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl, string port = null)
        {
            return string.Format("http{0}://{1}{2}{3}",
                            ConfigurationManager.AppSettings["portForEmailSending"] == SECURED_PORT ? "s" : String.Empty,
                            ConfigurationManager.AppSettings["host"],
                            (port != null) ? ":" + port : String.Empty,
                            relativeUrl);
        }

        public static string GetPortParameter()
        {
            return ConfigurationManager.AppSettings["portForEmailSending"];
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        /// <summary>
        /// method for converting a System.DateTime value to a UNIX Timestamp
        /// </summary>
        /// <param name="value">date to convert/// <returns></returns>
        public static string ConvertToTimestamp(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value - DateTime.SpecifyKind(new DateTime(1970, 1, 1, 0, 0, 0, 0), DateTimeKind.Utc));

            //return the total seconds (which is a UNIX timestamp)
            return span.TotalSeconds.ToString();
        }
        /// <summary>
        /// Get page contents from data set
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="page"></param>
        /// <param name="recordsOnPage"></param>
        /// <param name="totalRecords"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        public static IEnumerable<object> Paginate(IEnumerable<object> dataSet, int recordsOnPage, int totalRecords, ref int page, out int totalPages)
        {
            totalPages = (totalRecords + recordsOnPage - 1) / recordsOnPage;

            if (totalPages < page)
            {
                page = totalPages;
            }

            if (totalPages == 1)
            {
                return dataSet;
            }
            else
            {
                return dataSet.Skip(recordsOnPage * (page - 1)).Take(recordsOnPage);
            }
        }
    }
}