﻿using Domain.Entities.WebServices.Wcf;
using Hawk.Infrastructure.AppLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Hawk.Infrastructure.BLL
{
    public static class TokenService
    {
        public static bool VerifyDataToken(RegisterInfo model)
        {
            // get UTC time for current day
            var time = DateTime.Parse(DateTime.UtcNow.ToString("yyyy-MM-dd"));
            // convert DateTime to Timestamp
            var timeStamp = Utils.ConvertToTimestamp(time);
            // private key - is the  hash from Timestamp
            var privateKey = GetModelHash(timeStamp);
            //temporary solution
            Logger.LogInfo("time: " + time);
            Logger.LogInfo("timeStamp: " + timeStamp);
            Logger.LogInfo("privateKey: " + privateKey);
            Logger.LogInfo("token: " + GetModelHash(model.Email + model.Password + model.FirstName + privateKey));
            return GetModelHash(model.Email + model.Password + model.FirstName + privateKey) == model.Token;
        }
        private static string GetModelHash(string model)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(model)))
                    .Replace("-", string.Empty)
                    .ToLower();
            }
        }
    }
}
