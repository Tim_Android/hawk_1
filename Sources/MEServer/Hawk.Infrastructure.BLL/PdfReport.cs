﻿using Domain.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using Domain.Entities.Reports;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Domain.Entities;
using iTextSharp.text.pdf.draw;
using Enums = Domain.Entities.Enums;
using Domain.Entities.SystemSettings;

namespace Hawk.Infrastructure.BLL
{
    public class PdfReport : Report, IReport
    {
        private Document _document;
        private MemoryStream _stream;
        private ReportSettings _settings = new ReportSettings();
        public PdfReport(ReportSchemaEntity schema, DataTable data)
            : base(schema, data)
        {
            _fileExtension = HawkCommonResources.Report.PDFExtension;
            BuildReport();
        }
        public string FileName
        {
            get
            {
                return _fileName;
            }
        }
        public string FileExtension
        {
            get
            {
                return _fileExtension;
            }
        }
        public byte[] ToByteArray()
        {
            return _stream.ToArray();
        }

        private void BuildReport()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                _document = new Document(PageSize.A4, _settings.LeftDocumentMargin, _settings.RightDocumentMargin, _settings.TopDocumentMargin, _settings.BottomDocumentMargin);
                PdfWriter PDFWriter = PdfWriter.GetInstance(_document, memoryStream);

                // solution for right displaying Cyrillic and other symbols
                BaseFont baseNormalFont = CreateBaseFont(Enums.Font.ArialNormal);
                BaseFont baseBoldFont = CreateBaseFont(Enums.Font.ArialBold);
                BaseFont baseItalicFont = CreateBaseFont(Enums.Font.ArialItalic);

                Font titleFont = new Font(baseBoldFont, _settings.TitleFontSize);
                Font subTitleFont = new Font(baseBoldFont, _settings.SubTitleFontSize);
                Font boldTableFont = new Font(baseBoldFont, _settings.BodyFontSize);
                Font bodyFont = new Font(baseNormalFont, _settings.BodyFontSize);

                _document.Open();
                AddLogo();
                AddHeader(titleFont);
                AddSeparator();
                AddFilters(boldTableFont, bodyFont);
                AddTable(boldTableFont, bodyFont);
                _document.Close();

                _stream = memoryStream;
            }
        }

        private BaseFont CreateBaseFont(Enums.Font customFont)
        {
            return BaseFont.CreateFont(_schema.FontPaths[customFont], BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        }

        private void AddTable(Font boldTableFont, Font bodyFont)
        {
            PdfPTable table = GenerateTable(boldTableFont, bodyFont);
            table.WidthPercentage = _settings.TableWidthPercentage;
            table.SpacingBefore = _settings.TableSpacingBefore;
            table.SpacingAfter = _settings.TableSpacingAfter;
            _document.Add(table);
        }

        private void AddFilters(Font boldTableFont, Font bodyFont)
        {
            foreach (var name in _schema.Filters)
            {
                Chunk header = new Chunk(name.Key, boldTableFont);
                Chunk body = new Chunk(name.Value, bodyFont);
                Paragraph filter = new Paragraph();
                filter.Add(header);
                filter.Add(body);
                filter.SpacingBefore = _settings.FilterSpacingBefore;
                _document.Add(filter);
            }
        }

        private void AddSeparator()
        {
            LineSeparator line = new LineSeparator(_settings.LineSeparatorWidth, _settings.LineSeparatorPercentage, BaseColor.ORANGE, (int)Enums.Align.Center, _settings.LineSeparatorOffset);
            _document.Add(line);
        }

        private void AddHeader(Font titleFont)
        {
            Paragraph header = new Paragraph(_schema.Header, titleFont);
            header.SpacingAfter = _settings.HeaderSpacingAfter;
            header.Alignment = (int)Enums.Align.Center;
            _document.Add(header);
        }

        private void AddLogo()
        {
            Image logo = Image.GetInstance(_schema.LogoPath);
            logo.ScalePercent(_settings.LogoScalePercent);
            logo.Alignment = (int)Enums.Align.Right;
            logo.SpacingAfter = _settings.LogoSpacingAfter;
            _document.Add(logo);
        }

        private PdfPTable GenerateTable(Font boldTableFont, Font bodyFont)
        {
            int columnCount = _schema.TableColumns.Count;
            PdfPTable table = new PdfPTable(columnCount);

            // header
            foreach (var column in _schema.TableColumns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.Name, boldTableFont));
                cell.VerticalAlignment = (int)Enums.Align.Center;
                cell.HorizontalAlignment = (int)Enums.Align.Center;
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                table.AddCell(cell);
            }
            if (_data.Rows.Count > 0)
            {
                foreach (DataRow row in _data.Rows)
                {
                    foreach (var column in _schema.TableColumns)
                    {
                        string text = BuildCellText(row, column);

                        PdfPCell cell = new PdfPCell(new Phrase(text, bodyFont));
                        cell.VerticalAlignment = (int)Enums.Align.Center;
                        cell.HorizontalAlignment = (int)Enums.Align.Left;
                        table.AddCell(cell);
                    }
                }
            }
            else
            {
                PdfPCell cell = new PdfPCell(new Phrase(HawkCommonResources.Common.NoData, bodyFont));
                cell.VerticalAlignment = (int)Enums.Align.Center;
                cell.HorizontalAlignment = (int)Enums.Align.Center;
                cell.Colspan = _schema.TableColumns.Count;
                table.AddCell(cell);
            }
            table.HeaderRows = _settings.TableHeaderRows;
            return table;
        }
        /// <summary>
        /// Create string from existed arguments in column
        /// </summary>
        /// <param name="row">Standard Row from DataTable with data</param>
        /// <param name="column">Column Schema from ReportSchemaEntity that contains column names of DataTable</param>
        /// <returns></returns>
        private string BuildCellText(DataRow row, ColumnEntity column)
        {
            List<string> args = new List<string>();
            foreach (var item in column.Arguments)
            {
                args.Add(row[item].ToString());
            }
            // if one of args has empty value than all text will be Empty for properly data displaying
            string text = IsComposite(args) && args.Contains(string.Empty) ? string.Empty : string.Format(column.Field, args.ToArray<string>());
            return text;
        }

        private bool IsComposite(List<string> args)
        {
            return args.Count > 1;
        }
    }
}
