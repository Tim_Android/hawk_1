﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLogLogger = NLog.Logger;

namespace Hawk.Infrastructure.AppLogger
{
    public static class Logger
    {
        private static NLogLogger logger = LogManager.GetCurrentClassLogger();

        public static void LogError(string message)
        {
            logger.Error(message);
        }

        public static void LogInfo(string message)
        {
            logger.Info(message);
        }

        /// <summary>
        /// Recursively log exception (including all inner exceptions)
        /// </summary>
        /// <param name="e"></param>
        public static void LogError(Exception e)
        {
            if (e != null)
            {
                logger.Error(e.Message);
                logger.Debug(e.StackTrace);
                Logger.LogError(e.InnerException);
            }
        }

        public static void LogWarning(string message)
        {
            logger.Warn(message);
        }
    }
}
