﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Const
{
    public class RecurringTasks
    {
        public const string CLEAN_CRASH_LOGS = "CleanCrashLogs";
        public const string CLEAN_USER_DATA = "CleanUserData";
        public const string EXPIRE_LICENSES = "ExpireLicenses";
        public const string SEND_EXPIRATION_NOTICE = "SendExpirationNotice";
        public const string SEND_TRIAL_EXPIRED_NOTICE = "SendTrialExpiredNotice";
        public const string SEND_ACTIVATION_REMINDER = "SendActivationReminder";
        public const string DELETE_NOT_ACTIVATED_ACCOUNTS = "DeleteNotActivatedAccounts";
        public const string DELETE_EXPIRED_ACCOUNTS = "DeleteExpiredAccounts";
    }
}
