﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Const
{
    public class RecurringTasksParamsConst
    {
        public const string STORAGE_PERIOD_FOR_USER_DATA = "storagePeriodForUserData";
        public const string STORAGE_PERIOD_FOR_CRASH_LOGS = "storagePeriodForCrashLogs";
        public const string CRASH_LOGS_ENTRIES_ALLOWED = "crashLogsEntriesAllowed";
        
        public const string DAYS_PRIOR_EXPIRATION = "daysPriorExpiration";
        
        public const string DAYS_FROM_FOR_ACTIVATION_REMINDER = "daysFromForActivationReminder";
        public const string DAYS_TO_FOR_ACTIVATION_REMINDER = "daysToForActivationReminder";
        public const string DAYS_AFTER_EXPIRED_PERIOD = "daysAfterExpiredPeriod";

        public const string DAYS_PRIOR_NA_ACCOUNT_DEL = "daysPriorNotActAccountDel";
        public const string DAYS_PRIOR_EXP_ACC_DEL = "daysBeforeExpiredAccDeletion";

    }
}
