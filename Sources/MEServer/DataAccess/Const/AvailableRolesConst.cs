﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Const
{
    public static class AvailableRoles
    {
        public const string ADMIN = "admin"; //user is admin
        public const string CONFIRMED = "confirmed"; //user verifyied his email
        public const string PAID = "paid"; //user paid signup fee
        public const string USER = "user"; //registered user role
    }
}
