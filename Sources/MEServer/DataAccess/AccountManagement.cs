﻿using Domain.Entities.Enums;
using HawkBilling.Core;
using HawkBilling.Core.Type;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess
{
    public static class AccountManagement
    {
        /// <summary>
        /// Passes all the necessary information for license creation and payment history entries creation
        /// </summary>
        /// <param name="order"></param>
        /// <param name="userId"></param>
        /// <param name="paymProcessor"></param>
        /// <param name="newLicId"></param>
        /// <param name="paymId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static bool FinalizePayment(ProductModel order, int userId, string subscriptionId,IPaymentProcessor paymProcessor, out int? newLicId, out int? paymId, string comment = "")
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var outLicId = new ObjectParameter("OUT_LIC_ID", typeof(int));
                var outPaymId = new ObjectParameter("PAY_ID", typeof(int));
                int? licId = (order.licenseId == 0) ? null : order.licenseId;
                int additionalDevices = (licId == null) ? order.additionalDevices - 1 : order.additionalDevices;
                int? priceListId = order.priceListItem.id;
                if (priceListId < 0)
                {//we add devices only with no period extension
                    priceListId = null;
                }

                

                db.FINALIZE_PAYMENT(userId, (short)paymProcessor.paymentProcessor, subscriptionId, priceListId,
                                    paymProcessor.paymentInfo.operResult.transactionId, additionalDevices,
                                    licId, null, comment, outLicId, outPaymId);

                if (outLicId.Value != null && outPaymId.Value != null )
                {
                    newLicId = Convert.ToInt32(outLicId.Value);
                    paymId = Convert.ToInt32(outPaymId.Value);
                    return true;
                }
                else
                {
                    newLicId = null;
                    paymId = null;
                    return false;
                }
            }
        }


        /// <summary>
        /// Method for handling payment service listener updates
        /// </summary>
        /// <param name="subscrId"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public static bool ExtendSubscription(string subscrId, string transactionId, string paymentComment)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var outPayId = new ObjectParameter("PAY_ID", typeof(int));
                db.FINALIZE_AUTO_PAYMENT(null, subscrId, transactionId, paymentComment, outPayId);
                if (outPayId == null || Convert.ToInt32(outPayId.Value) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Deletes subsription record from database
        /// </summary>
        /// <param name="subscrId"></param>
        /// <returns></returns>
        public static bool DeleteSubscriptionRecord(string subscrId)
        {
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.SET_LICENSE_SUBSCR_ID(null, subscrId, null);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Stub method that checks if transaction has already been processed
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public static bool CheckIfTransProcessed(string transactionId)
        {
            //var outPayId = new ObjectParameter("PAY_ID", typeof(int));
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var outProcessed = new ObjectParameter("PROCESSED", typeof(bool));
                db.IS_TRANSACTION_PROCESSED(transactionId, outProcessed);
                return Convert.ToBoolean(outProcessed.Value);
            }
        }

        public static void DeactivateLicense(LicenseModel license)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.DEACTIVATE_LICENSE(license.id);
            }
        }
    }
}
