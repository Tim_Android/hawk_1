﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Exceptions
{
    public class SimultaniousBuildException : Exception
    {
        public SimultaniousBuildException()
            : base() { }

        public SimultaniousBuildException(string message)
            : base(message) { }

        public SimultaniousBuildException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public SimultaniousBuildException(string message, Exception innerException)
            : base(message, innerException) { }

        public SimultaniousBuildException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected SimultaniousBuildException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
