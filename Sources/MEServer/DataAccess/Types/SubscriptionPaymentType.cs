﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Types
{
    public enum SubscriptionPaymentType
    {
        SignupFee = 1,
        NewSubscription = 2,
        NewDevice = 3,
        SubscriptionExtend = 4 //includes period [and devices]
    }
}
