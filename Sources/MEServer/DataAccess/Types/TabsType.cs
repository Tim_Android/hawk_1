﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Types
{
    public enum TabsType: short
    {
        Inactive = -1,
        AgentInfo = 0,
        CallLog = 1,
        SMSLog = 2,
        MMSLog = 3,
        GPSLog = 4,
        CrashLog = 5,
        BrowserLog = 6,
        BookmarkLog = 7
    }
}
