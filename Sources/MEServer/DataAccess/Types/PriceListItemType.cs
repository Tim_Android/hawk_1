﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Types
{
    public enum PriceListItemType
    {
        SignupFee = 1,        //signup fee price type
        SubscriptionItem = 2, //subscription price type
        Promocode = 3, // promo code type
        
        // following types are intended for admin's license changes monitoring
        SubscriptionExtend = 4,
        SubscriptionDecrease = 5,
        DeviceAdd = 6,
        DeviceRemoval = 7,
        PhoneSupport = 8

    }
}
