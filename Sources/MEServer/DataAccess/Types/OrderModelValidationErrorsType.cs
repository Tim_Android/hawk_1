﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Types
{
    public enum OrderModelValidationErrorsType
    {
        InvalidPriceId = 1,
        InvalidDeviceCount =2
    }
}
