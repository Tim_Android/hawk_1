﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace HawkDataAccess.Types
{
    public enum ActivationStateType
    {
        NotActivated = 0,
        PartlyActivated = 1,
        FullyActivated = 2,
        Disabled = 3
    }
}
