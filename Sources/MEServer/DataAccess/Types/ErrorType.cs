﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace HawkDataAccess.Types
{
    public enum ErrorType
    {
        UnknownError = 110,
        ActivateInvalidOrDisabledPromoCode = 201,
        ActivationsHaveAlreadyBeenUsedUp = 202,
        ExpiredPromoCode = 203,
        StatusOfSelectedProCodeHasChanged = 301,
        PromoCodeEnteredManuallyAlreadyExists = 401
    }
}
