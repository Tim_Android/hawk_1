﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace HawkDataAccess.Types
{
    public enum ExpirationStateType
    {
        Expired = 1,
        NotExpired = 0
    }

}
