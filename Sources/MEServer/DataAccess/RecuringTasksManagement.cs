﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HawkDataAccess.Models;

namespace HawkRecurringTasks
{
    public class RecurringTasksManagement
    {
        /// <summary>
        /// Reduce number of crash logs stored i DB
        /// </summary>
        public static void CleanCrashLogs(int? maxDayCount, int? maxRecordCount)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.CLEAN_CRASH_LOGS(maxDayCount, maxRecordCount);
            }
        }

        /// <summary>
        /// Delete old user data and data from expired/not activated devices (defaults to 45 days if null value is passed)
        /// </summary>
        public static void CleanUserData(int? maxDayCount)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.CLEAN_USERS_DATA(maxDayCount);
            }
        }
    }
}
