﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Domain.Entities;
using Domain.Entities.Abstract;
using Domain.Entities.WebServices.Wcf;
using HawkDataAccess.Models;
using DeviceInfoWcfService = Domain.Entities.WebServices.Wcf.DeviceInfo;
using Hawk.Infrastructure.AppLogger;

namespace HawkDataAccess
{
    public static class DataAccess
    {
        public static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["MobileEnterprise"].ConnectionString;

        #region Site

        public static List<MessageType> GetMessageTypes()
        {
            List<MessageType> messageTypes = new List<MessageType>();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("GET_MSG_TYPES", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            messageTypes.Add(new MessageType
                            {
                                ID = (int)reader[0],
                                Name = (string)reader[1],
                            });
                        }
                    }
                }
            }

            return messageTypes;
        }

        public static List<MessageContact> GetMessageContacts(int agentId)
        {
            List<MessageContact> messageContacts = new List<MessageContact>();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("GET_MESSAGE_CONTACTS", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AGENT_ID", agentId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            messageContacts.Add(new MessageContact
                            {
                                Name = (string)reader[0]
                            });
                        }
                    }
                }
            }

            return messageContacts;
        }

        public static void UpdateDeviceInfo(DeviceInfoEx deviceInfo)
        {
            const string sqlSetDeviceInfoQuery = "SET_DEVICE_INFO";

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand(sqlSetDeviceInfoQuery, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AGENT_ID", deviceInfo.id);
                    command.Parameters.AddWithValue("@DEVICE_NAME", deviceInfo.device);
                    command.Parameters.AddWithValue("@NOTE", deviceInfo.note);

                    command.ExecuteNonQuery();
                }
            }
        }

        public static DataSet GetDataLog(SearchCondition condition)
        {
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand(condition.GetStoredProcedureName(), conn);
                    command.CommandType = CommandType.StoredProcedure;
                    condition.FillSQLCommandParameters(command.Parameters);
                    command.ExecuteNonQuery();

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                ds = null;
            }

            return ds;
        }
        public static int GetItemTotalCount(SearchCondition condition, string procedureName)
        {
            int result = default(int);
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand(procedureName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    condition.FillSQLCommandParameters(command.Parameters);
                    result = (int)command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }
        public static DataSet GetMMSAttachments(int mmsID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                string query = "exec GET_MMS_ATTACH " + mmsID.ToString();
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public static byte[] GetAttachment(int attachmentID)
        {
            byte[] attachment = null;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();

                    const string sqlQueryAttach = "GET_ATTACH";

                    SqlCommand command = new SqlCommand(sqlQueryAttach, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ID_ATTACH", attachmentID);

                    SqlDataReader r = command.ExecuteReader();
                    if (r.Read())
                    {
                        attachment = (byte[])r["ATTACH_DATA"];
                    }
                }
                catch (Exception)
                {
                    attachment = null;
                }
                finally
                {
                    conn.Close();
                }
            }

            return attachment;
        }

        public static DataSet GetCrashLogs(int agentId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                string query = "exec GET_CRASH_LOGS " + agentId.ToString();
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public static bool ValidateAgentId(int agentId, Guid userId)
        {
            int agentsCount = 0;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                const string sqlQueryAgentCount = "GET_AGENTS_COUNT_BY_GUID";

                using (SqlCommand command = new SqlCommand(sqlQueryAgentCount, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@USER_GUID", userId);
                    command.Parameters.AddWithValue("@AGENT_ID", agentId);
                    command.Parameters.Add("@AGENTS_COUNT", SqlDbType.Int);
                    command.Parameters["@AGENTS_COUNT"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    agentsCount = (int)command.Parameters["@AGENTS_COUNT"].Value;
                }
            }

            return agentsCount > 0;
        }

        public static int GetUserAgentsCount(Guid userId)
        {
            int agentsCount = 0;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                const string sqlQueryAgentCount = "GET_TOTAL_AGENTS_COUNT_BY_GUID";

                using (SqlCommand command = new SqlCommand(sqlQueryAgentCount, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@USER_ID", userId);
                    command.Parameters.Add("@AGENTS_COUNT", SqlDbType.Int);
                    command.Parameters["@AGENTS_COUNT"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    agentsCount = (int)command.Parameters["@AGENTS_COUNT"].Value;
                }
            }

            return agentsCount;
        }

        public static void RemoveAgent(int agentId)
        {
            //access from site and web-service
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                const string sqlQueryDeleteAgent = "DELETE_DEVICE";

                using (SqlCommand command = new SqlCommand(sqlQueryDeleteAgent, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AGENT_ID", agentId);
                    command.ExecuteNonQuery();
                }
            }
        }

        public static DateTime GetAgentLastUpdateTime(int agentId)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                const string sqlQueryAgentLastUpdate = "GET_AGENTS_LAST_UPDATE_TIME";

                using (SqlCommand command = new SqlCommand(sqlQueryAgentLastUpdate, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AGENT_ID", agentId);
                    command.Parameters.Add("@LAST_UPDATED_TIME", SqlDbType.DateTime);
                    command.Parameters["@LAST_UPDATED_TIME"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    return (DateTime)command.Parameters["@LAST_UPDATED_TIME"].Value;
                }
            }
        }

        public static bool IsAgentOnline(int agentId)
        {
            DateTime lastUpdate = GetAgentLastUpdateTime(agentId);

            TimeSpan dt = DateTime.Now - lastUpdate;
            return dt.TotalHours <= 3;
        }

        public static int GetUserId(string providerUserKey)
        {
            int userId = default(int);
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                const string sqlGetUserInfo = "GET_USER_INFO";
                using (SqlCommand command = new SqlCommand(sqlGetUserInfo, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ASP_USER_ID", providerUserKey);
                    command.Parameters.AddWithValue("@SUBSCR_ID", null);
                    var queryRes = command.ExecuteReader();

                    int count = 0;
                    while (queryRes.Read())
                    {
                        userId = (int)queryRes["ID"];
                        count++;
                    }
                    if (count != 1)
                    {
                        throw new InvalidOperationException();
                    }
                }
                connection.Close();
            }
            return userId;
        }

        public static int GetActualAgentId(string agentID, string deviceID, SqlConnection connection)
        {
            const string sqlGetActualAgentIDQuery = "GET_ACTUAL_AGENT_ID";

            int actualID = -1;

            using (SqlCommand command = new SqlCommand(sqlGetActualAgentIDQuery, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AGENT_ID", agentID);
                command.Parameters.AddWithValue("@DEVICE_ID", deviceID);
                command.Parameters.Add("@ACTUAL_AGENT_ID", SqlDbType.Int);
                command.Parameters["@ACTUAL_AGENT_ID"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                actualID = (int)command.Parameters["@ACTUAL_AGENT_ID"].Value;
            }

            return actualID;
        }

        public static bool IsLicensedDevice(int agentID, SqlConnection connection, SqlTransaction transaction)
        {
            const string sqlGetActualAgentIDQuery = "IS_LICENSED_DEVICE";

            bool isLicensed = false;

            using (SqlCommand command = new SqlCommand(sqlGetActualAgentIDQuery, connection, transaction))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AGENT_ID", agentID);
                command.Parameters.Add("@IS_LICENSED", SqlDbType.Bit);
                command.Parameters["@IS_LICENSED"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                isLicensed = (bool)command.Parameters["@IS_LICENSED"].Value;
            }

            return isLicensed;
        }

        public static void UpdateAgentsLastAccessTime(int agentId, SqlConnection connection, SqlTransaction transaction)
        {
            using (SqlCommand command = new SqlCommand("REFRESGH_AGENTS_LAST_UPDATE_TIME", connection, transaction))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AGENT_ID", agentId);

                command.ExecuteNonQuery();
            }
        }

        #endregion

        #region WCF Service

        public static int AddAgent(int userId, string name, int operationSystemType)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                const string sqlQueryAddAgent = "ADD_AGENT";

                using (SqlCommand command = new SqlCommand(sqlQueryAddAgent, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@USER_ID", userId);
                    command.Parameters.AddWithValue("@AGENT_NAME", name);
                    command.Parameters.AddWithValue("@OPER_SYS_TYPE", operationSystemType);
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters["@AGENT_ID"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    return (int)command.Parameters["@AGENT_ID"].Value;
                }
            }
        }

        public static void WriteCalls(int agentId, SqlConnection connection, SqlTransaction transaction, List<Call> calls)
        {
            const string sqlSetCallQuery = "ADD_CALL";

            //todo: what if null fields?
            if (calls != null && calls.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetCallQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@CALL_DATE", SqlDbType.DateTime);
                    command.Parameters.Add("@CALL_TYPE", SqlDbType.NChar);
                    command.Parameters.Add("@DURATION", SqlDbType.Int);
                    command.Parameters.Add("@FROM_NUMBER", SqlDbType.NVarChar);
                    command.Parameters.Add("@FROM_PHONE_NAME", SqlDbType.NChar);
                    command.Parameters.Add("@FROM_NUM_TYPE", SqlDbType.NVarChar);
                    command.Parameters.Add("@TO_NUMBER", SqlDbType.NVarChar);
                    command.Parameters.Add("@TO_PHONE_NAME", SqlDbType.NChar);
                    command.Parameters.Add("@TO_NUM_TYPE", SqlDbType.NVarChar);

                    foreach (Call c in calls)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@CALL_DATE"].Value = c.date;
                        command.Parameters["@CALL_TYPE"].Value = c.type;
                        command.Parameters["@DURATION"].Value = c.duration;
                        command.Parameters["@FROM_NUMBER"].Value = c.fromNumber;
                        command.Parameters["@FROM_PHONE_NAME"].Value = c.fromNumberName;
                        command.Parameters["@FROM_NUM_TYPE"].Value = c.fromNumberType;
                        command.Parameters["@TO_NUMBER"].Value = c.toNumber;
                        command.Parameters["@TO_PHONE_NAME"].Value = c.toNumberName;
                        command.Parameters["@TO_NUM_TYPE"].Value = c.toNumberType;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void WriteSMSs(int agentId, SqlConnection connection, SqlTransaction transaction, List<Sms> messages)
        {
            const string sqlSetSmsQuery = "ADD_SMS";

            if (messages != null && messages.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetSmsQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@MSG_TYPE", SqlDbType.NVarChar);
                    command.Parameters.Add("@SMS_ADDR", SqlDbType.NVarChar);
                    command.Parameters.Add("@CONTACT_NAME", SqlDbType.NVarChar);
                    command.Parameters.Add("@SMS_DT", SqlDbType.DateTime);
                    command.Parameters.Add("@SMS_BODY", SqlDbType.NVarChar);

                    foreach (Sms message in messages)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@MSG_TYPE"].Value = message.type;
                        command.Parameters["@SMS_ADDR"].Value = message.address;
                        command.Parameters["@CONTACT_NAME"].Value = message.contactName;
                        command.Parameters["@SMS_DT"].Value = message.date;
                        command.Parameters["@SMS_BODY"].Value = message.body;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void WriteMMSs(int agentId, SqlConnection connection, SqlTransaction transaction, List<Mms> messages)
        {
            const string sqlSetMmsQuery = "ADD_MMS";

            if (messages != null && messages.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetMmsQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@MSG_TYPE", SqlDbType.NVarChar);
                    command.Parameters.Add("@MMS_ADDR", SqlDbType.NVarChar);
                    command.Parameters.Add("@CONTACT_NAME", SqlDbType.NVarChar);
                    command.Parameters.Add("@MMS_DT", SqlDbType.DateTime);
                    command.Parameters.Add("@MMS_BODY", SqlDbType.NVarChar);
                    command.Parameters.Add("@MMS_SUBJ", SqlDbType.NVarChar);
                    command.Parameters.Add("@ID_MSG", SqlDbType.Int);
                    command.Parameters["@ID_MSG"].Direction = ParameterDirection.Output;

                    foreach (Mms message in messages)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@MSG_TYPE"].Value = message.type;
                        command.Parameters["@MMS_ADDR"].Value = message.address;
                        command.Parameters["@CONTACT_NAME"].Value = message.contactName;
                        command.Parameters["@MMS_DT"].Value = message.date;
                        command.Parameters["@MMS_BODY"].Value = message.body;
                        command.Parameters["@MMS_SUBJ"].Value = message.sub;

                        command.ExecuteNonQuery();

                        WriteMMSAttachmentsTemplates(connection, transaction, (int)command.Parameters["@ID_MSG"].Value,
                            message.attachments);
                    }
                }
            }
        }

        public static void WriteMMSAttachmentsTemplates(SqlConnection connection, SqlTransaction transaction, int mmsID, List<MMSAttachment> attachments)
        {
            const string sqlSetMmsAttachTemplateQuery = "ADD_MMS_ATTACH_TEMPL";

            using (SqlCommand command = new SqlCommand(sqlSetMmsAttachTemplateQuery, connection, transaction))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@MMS_ID", SqlDbType.Int);
                command.Parameters.Add("@MIME_TYPE", SqlDbType.NVarChar);
                command.Parameters.Add("@AGENT_ATTACH_ID", SqlDbType.Int);

                foreach (MMSAttachment attachment in attachments)
                {
                    command.Parameters["@MMS_ID"].Value = mmsID;
                    command.Parameters["@MIME_TYPE"].Value = attachment.type;
                    command.Parameters["@AGENT_ATTACH_ID"].Value = attachment._id;

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void WriteMMSAttachment(string agentID, string deviceID, int agentAttachmentID, byte[] attachment)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                int actualAgentID = GetActualAgentId(agentID, deviceID, connection);

                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        const string sqlSetMmsAttachQuery = "ADD_MMS_ATTACH";
                        if (!IsLicensedDevice(actualAgentID, connection, transaction))
                        {
                            return;
                        }

                        using (SqlCommand command = new SqlCommand(sqlSetMmsAttachQuery, connection, transaction))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                            command.Parameters.Add("@AGENT_ATTACH_ID", SqlDbType.Int);
                            command.Parameters.Add("@ATTACH_DATA", SqlDbType.Image);
                            command.Parameters["@AGENT_ID"].Value = actualAgentID;
                            command.Parameters["@AGENT_ATTACH_ID"].Value = agentAttachmentID;
                            command.Parameters["@ATTACH_DATA"].Value = attachment;
                            command.ExecuteNonQuery();
                        }

                        UpdateAgentsLastAccessTime(actualAgentID, connection, transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch (Exception rollbackFailedException)
                        {
                            Logger.LogError("Rollback failed.");
                            Logger.LogError(rollbackFailedException);
                        }

                        Logger.LogError("Transaction failed.");
                        Logger.LogError(ex);
                        throw;
                    }
                }
            }
        }

        public static void WriteCoordinates(int agentId, SqlConnection connection, SqlTransaction transaction, List<GPS> coordinates)
        {
            const string sqlSetMmsQuery = "ADD_COORDINATE";

            if (coordinates != null && coordinates.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetMmsQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@LATITUDE", SqlDbType.Decimal);
                    command.Parameters.Add("@LONGITUDE", SqlDbType.Decimal);
                    command.Parameters.Add("@ALTITUDE", SqlDbType.Decimal);
                    command.Parameters.Add("@ACCURACY", SqlDbType.Float);
                    command.Parameters.Add("@BEARING", SqlDbType.Float);
                    command.Parameters.Add("@PROVIDER_NAME", SqlDbType.NChar);
                    command.Parameters.Add("@SPEED", SqlDbType.Float);
                    command.Parameters.Add("@ARRIVAL_TIME", SqlDbType.DateTime);

                    GPS prevCoordinate = null;
                    foreach (GPS coordinate in coordinates)
                    {
                        if ((null == prevCoordinate) || (coordinate != prevCoordinate))
                        {
                            command.Parameters["@AGENT_ID"].Value = agentId;
                            command.Parameters["@LATITUDE"].Value = coordinate.latitude;
                            command.Parameters["@LONGITUDE"].Value = coordinate.longitude;
                            command.Parameters["@ALTITUDE"].Value = coordinate.altitude;
                            command.Parameters["@ACCURACY"].Value = coordinate.accuracy;
                            command.Parameters["@BEARING"].Value = coordinate.bearing;
                            command.Parameters["@PROVIDER_NAME"].Value = coordinate.provider;
                            command.Parameters["@SPEED"].Value = coordinate.speed;
                            command.Parameters["@ARRIVAL_TIME"].Value = coordinate.time;

                            command.ExecuteNonQuery();
                        }

                        prevCoordinate = coordinate;
                    }
                }
            }
        }

        public static void WriteCrashLogs(int agentId, SqlConnection connection, SqlTransaction transaction, List<CrashLogEntry> crashLog)
        {
            const string sqlSetCrashLogQuery = "ADD_CRASH_LOG";

            if (crashLog != null && crashLog.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetCrashLogQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@CRASH_DATETIME", SqlDbType.DateTime);
                    command.Parameters.Add("@CRASH_DATA", SqlDbType.NVarChar);

                    foreach (CrashLogEntry e in crashLog)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@CRASH_DATETIME"].Value = e.date;
                        command.Parameters["@CRASH_DATA"].Value = e.data;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void WriteDeviceInfo(int agentId, SqlConnection connection, SqlTransaction transaction, DeviceInfoWcfService deviceInfo)
        {
            const string sqlSetDeviceInfoQuery = "SET_DEVICE_INFO";

            if (null != deviceInfo)
            {
                using (SqlCommand command = new SqlCommand(sqlSetDeviceInfoQuery, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AGENT_ID", agentId);
                    command.Parameters.AddWithValue("@AGENT_NAME", deviceInfo.DEVICE_INFO_AGENT_NAME);
                    command.Parameters.AddWithValue("@PHONE_NUMBER", deviceInfo.DEVICE_INFO_PHONE_NUMBER);
                    command.Parameters.AddWithValue("@BRAND", deviceInfo.DEVICE_INFO_BRAND);
                    command.Parameters.AddWithValue("@MODEL", deviceInfo.DEVICE_INFO_MODEL_NAME);
                    command.Parameters.AddWithValue("@OS", deviceInfo.DEVICE_INFO_OS_VERSION);
                    command.Parameters.AddWithValue("@SDK", deviceInfo.DEVICE_INFO_SDK_VERSION);
                    command.Parameters.AddWithValue("@FIRMWARE_ID", deviceInfo.DEVICE_INFO_FIRMWARE_ID);
                    command.Parameters.AddWithValue("@SVN_VERSION", deviceInfo.DEVICE_INFO_SVN_VERSION);
                    command.Parameters.AddWithValue("@TIME_ZONE", deviceInfo.DEVICE_INFO_TIME_ZONE_NAME);
                    command.Parameters.AddWithValue("@TZ_OFFSET", deviceInfo.DEVICE_INFO_TIME_ZONE_RAW_OFFSET);

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void WriteBookmarks(int agentId, SqlConnection connection, SqlTransaction transaction, List<BrowserBookmark> bookmarks)
        {

            const string sqlSetBrowserBookmarkLog = "ADD_BOOKMARKS";

            if (bookmarks != null && bookmarks.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetBrowserBookmarkLog, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@BOOKMARK_URL", SqlDbType.VarChar);
                    command.Parameters.Add("@BOOKMARK_TITLE", SqlDbType.NVarChar);

                    foreach (BrowserBookmark bookmark in bookmarks)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@BOOKMARK_URL"].Value = bookmark.url;
                        command.Parameters["@BOOKMARK_TITLE"].Value = bookmark.title;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void WriteBrowserHistory(int agentId, SqlConnection connection, SqlTransaction transaction, List<BrowserHistoryItem> history)
        {

            const string sqlSetBrowserHistoryLog = "ADD_BROWSER_HISTORY";

            if (history != null && history.Count > 0)
            {
                using (SqlCommand command = new SqlCommand(sqlSetBrowserHistoryLog, connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AGENT_ID", SqlDbType.Int);
                    command.Parameters.Add("@URL", SqlDbType.VarChar);
                    command.Parameters.Add("@TITLE", SqlDbType.NVarChar);
                    command.Parameters.Add("@DATE", SqlDbType.DateTime);
                    command.Parameters.Add("@VISITS", SqlDbType.NVarChar);

                    foreach (BrowserHistoryItem item in history)
                    {
                        command.Parameters["@AGENT_ID"].Value = agentId;
                        command.Parameters["@URL"].Value = item.url;
                        command.Parameters["@TITLE"].Value = item.title;
                        command.Parameters["@DATE"].Value = item.date;
                        command.Parameters["@VISITS"].Value = item.visits;

                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void UpdatedAgent(string agentID, string deviceID, UpdateInfo info)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                int id = GetActualAgentId(agentID, deviceID, connection);

                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        WriteDeviceInfo(id, connection, transaction, info.deviceInfo);
                        // from June 2018 Calls collecting is without license
                        WriteCalls(id, connection, transaction, info.callLog);

                        if (IsLicensedDevice(id, connection, transaction))
                        {
                            WriteSMSs(id, connection, transaction, info.smsLog);
                            WriteMMSs(id, connection, transaction, info.mmsLog);
                            WriteCoordinates(id, connection, transaction, info.gpsLog);
                            WriteCrashLogs(id, connection, transaction, info.crashLog);
                            WriteBookmarks(id, connection, transaction, info.browserBookmarksLog);
                            WriteBrowserHistory(id, connection, transaction, info.browserHistoryLog);
                        }

                        //TODO: when use EF and transactionFlow
                        //Parallel.Invoke(
                        //    () => WriteCalls(id, connection, transaction, info.callLog),
                        //    () => WriteSMSs(id, connection, transaction, info.smsLog),
                        //     () => WriteMMSs(id, connection, transaction, info.mmsLog),
                        //     () => WriteCoordinates(id, connection, transaction, info.gpsLog),
                        //     () => WriteCrashLogs(id, connection, transaction, info.crashLog)
                        //    );

                        UpdateAgentsLastAccessTime(id, connection, transaction);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }



        #endregion

        #region UserManagement

        public static int? GetUserInfoId(Guid membershipId)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {

                conn.Open();
                using (SqlCommand command = new SqlCommand("GET_USER_ID_BY_GUID", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ASP_USER_ID", SqlDbType.UniqueIdentifier);
                    command.Parameters["@ASP_USER_ID"].Value = membershipId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return (int)reader["ID"];
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
