﻿using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess
{
    public class TokenManagement
    {
        public static bool AddToken(string email, string token)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.ADD_TOKEN(email, token);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static string CheckToken(string token)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_TOKEN_INFO(token).FirstOrDefault();
            }
        }

        public static void CleanTokens()
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.CLEAN_TOKENS();
            }
        }

        public static void DeleteToken(string token)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.DELETE_TOKEN(token);
            }
        }
    }
}
