﻿using Domain.Entities;
using Domain.Entities.Filters;
using HawkBilling.Core.Type;
using HawkDataAccess.Models.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using HawkCommonResources;
using System.Configuration;

namespace HawkDataAccess.Models
{
    public class LicenseModel : UserInfoBase
    {
        public int id { get; set; }
        public int userId { get; set; }
        public string licenseName { get; set; }
        public DateTime creationDate { get; set; }
        public DateTime subscrEndDate { get; set; }
        public int devices { get; set; }
        public int freeDevices { get; set; }
        public int? residualDays { get; set; }
        public int? priceListId { get; set; }
        public string subscriptionId { get; set; }
        public PaymentProcessors type { get; set; }
        public string periodName { get; set; }

        public bool isAutochargeEnabled
        {
            get
            {
                return (subscriptionId != null);
            }
        }

        /// <summary>
        /// Method for retrieving Licenses list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static LicenseModel[] GetLicenseList(int? userId, int? expirationPeriod)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                LicenseModel[] licenses = db.GET_USERS_LICENSES(userId, expirationPeriod)
                                            .Where(res => res.SUBSCR_END.Value > DateTime.Now)
                                            .Select(res => new LicenseModel
                                            {
                                                id = res.ID,
                                                licenseName = Common.SubscriptionNumber + res.ID,
                                                priceListId = res.PRICE_LIST_ID,
                                                devices = res.DEV_CNT,
                                                subscrEndDate = (DateTime)res.SUBSCR_END,
                                                subscriptionId = res.SUBSCR_ID,
                                                type = (PaymentProcessors)res.PAY_SYS_TYPE,
                                                freeDevices = (int)res.FREE_DEV_COUNT,
                                                firstName = res.FIRST_NAME,
                                                lastName = res.LAST_NAME,
                                                email = res.EMAIL,
                                                creationDate = res.CREATION_DATE.Value,
                                                userId = res.USER_ID
                                            }).OrderBy(res => res.id).ToArray();
                return licenses;
            }
        }
        /// <summary>
        /// Get Expired Trial Licenses after expiration period
        /// </summary>
        /// <param name="expirationPeriod">Days after license has been expired</param>
        /// <returns></returns>
        public static LicenseModel[] GetExpiredTrialLicenses(int? expirationPeriod)
        {
            return GetExpiredLicenses(expirationPeriod, PaymentProcessors.Promocode);
        }
        /// <summary>
        /// Get Expired Licenses after expiration period with PaymentProcessors type
        /// </summary>
        /// <param name="expirationPeriod">Days after license has been expired</param>
        /// <param name="type">Filter by type</param>
        /// <returns></returns>
        public static LicenseModel[] GetExpiredLicenses(int? expirationPeriod, PaymentProcessors type)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                LicenseModel[] allLicenses = db.GET_USERS_LICENSES(null, null)
                                            .Select(res => new LicenseModel
                                            {
                                                id = res.ID,
                                                userId = res.USER_ID,
                                                licenseName = "Subscription #" + res.ID,
                                                priceListId = res.PRICE_LIST_ID,
                                                devices = res.DEV_CNT,
                                                subscrEndDate = (DateTime)res.SUBSCR_END,
                                                subscriptionId = res.SUBSCR_ID,
                                                type = (PaymentProcessors)res.PAY_SYS_TYPE,
                                                freeDevices = (int)res.FREE_DEV_COUNT,
                                                firstName = res.FIRST_NAME,
                                                lastName = res.LAST_NAME,
                                                email = res.EMAIL,
                                                creationDate = res.CREATION_DATE.Value,
                                            }).OrderByDescending(res => res.id).ToArray();
                LicenseModel[] expiredLicenses = allLicenses
                                                    .Where(res => res.subscrEndDate < DateTime.UtcNow.AddDays(-(int)expirationPeriod)
                                                            && res.subscrEndDate > DateTime.UtcNow.AddDays(-(int)(expirationPeriod + 1))
                                                            && res.type == type
                                                            && !(allLicenses.Any(lic => lic.userId == res.userId
                                                                                && lic.subscrEndDate > DateTime.UtcNow)))
                                                    .ToArray();
                return expiredLicenses;
            }
        }
        /// <summary>
        /// Alias for GetUserLicensesList
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static LicenseModel[] GetUserLicensesWithSubscriptions(int userId)
        {
            return LicenseModel.GetLicenseList(userId, null)
                               .Where(res => res.subscriptionId != null).ToArray();
        }

        public static LicenseModel[] GetUserLicListForActivation(int userId)
        {
            LicenseModel[] licenses = LicenseModel.GetLicenseList(userId, null);
            licenses = licenses.Where(res => res.freeDevices > 0 && res.subscrEndDate > DateTime.Now).ToArray();
            return licenses;
        }

        /// <summary>
        /// Alias to get licenses that about to expire and have autocharge enabled. Methos is used on Subscription check page
        /// </summary>
        /// <param name="expirationPeriod"></param>
        /// <returns></returns>
        public static LicenseModel[] GetLicensesToCheck(int expirationPeriod)
        {
            LicenseModel[] licenses = GetLicenseList(null, expirationPeriod).Select(
                res => new LicenseModel
                {
                    id = res.id,
                    firstName = res.firstName,
                    lastName = res.lastName,
                    email = res.email,
                    creationDate = res.creationDate,
                    subscrEndDate = res.subscrEndDate,
                    subscriptionId = res.subscriptionId
                })
                .Where(res => res.subscriptionId != null).ToArray();
            return licenses;
        }

        /// <summary>
        /// Get all licenses that are about to expire and don't have autocharge enabled
        /// </summary>
        /// <param name="expirationperiod"></param>
        /// <returns></returns>
        public static LicenseModel[] GetExpiringLicenses(int expirationPeriod)
        {


            LicenseModel[] licenses = GetLicenseList(null, expirationPeriod).Select(
                res => new LicenseModel
                {
                    id = res.id,
                    firstName = res.firstName,
                    lastName = res.lastName,
                    licenseName = res.licenseName,
                    email = res.email,
                    subscrEndDate = res.subscrEndDate,
                    subscriptionId = res.subscriptionId,
                    type = res.type,
                    priceListId = res.priceListId,
                    userId = res.userId
                })
                .Where(res => res.subscriptionId == null)
                .ToArray();
            return DiscardExtendedLicenses(licenses);
        }

        public static LicenseModel GetLicenseInfo(int licId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var devCount = new ObjectParameter("DEVICE_COUNT", typeof(int));
                var residualDays = new ObjectParameter("RESIDUAL_DAYS", typeof(int));
                var subscrEnd = new ObjectParameter("SUBSCR_END", typeof(DateTime));
                var priceListId = new ObjectParameter("PRICE_LIST_ID", typeof(int));
                var subscrId = new ObjectParameter("SUBSCR_ID", typeof(string));
                var paySysType = new ObjectParameter("PAY_SYS_TYPE", typeof(PaymentProcessors));
                var freeDevCount = new ObjectParameter("FREE_DEV_COUNT", typeof(int));
                var license = db.GET_LICENSE(licId, null, devCount, residualDays, subscrEnd, priceListId, subscrId, paySysType, freeDevCount);
                return new LicenseModel
                {
                    id = (int)licId,
                    devices = Convert.ToInt32(devCount.Value),
                    residualDays = Convert.ToInt32(residualDays.Value),
                    licenseName = String.Empty,
                    subscrEndDate = Convert.ToDateTime(subscrEnd.Value),
                    priceListId = Convert.ToInt32(priceListId.Value),
                    subscriptionId = (subscrId.Value is DBNull) ? null : subscrId.Value.ToString().Trim(),
                    type = (PaymentProcessors)paySysType.Value
                };
            }
        }

        /// <summary>
        /// Gets list of licenses that have autocharge enabled, but which subscription needs to be cancelled due to prices change
        /// </summary>
        /// <param name="priceListId"></param>
        /// <returns></returns>
        public static LicenseModel[] GetCancelSubscriptionLicenseList(int priceListId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_UNSUBSCR_LICENSES(priceListId).Select(res => new LicenseModel
                {
                    id = (int)res.LIC_ID,
                    subscriptionId = res.SUBSCR_ID.Trim(),
                    type = (PaymentProcessors)res.PAY_SYS_TYPE,
                    email = res.EMAIL,
                    firstName = res.FIRST_NAME,
                    lastName = res.LAST_NAME
                }).ToArray();
            }
        }

        public static void UpdateLicenseSubscrId(int licId, string subscrId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.SET_LICENSE_SUBSCR_ID(licId, null, subscrId);
            }
        }

        /// <summary>
        /// Get expired license list for expire procedure initiation
        /// </summary>
        /// <returns></returns>
        private static LicenseModel[] GetExpiredLicenses()
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_LICENSES_FOR_EXPIRATION().Select(res => new LicenseModel
                {
                    id = res.ID,
                    firstName = res.FIRST_NAME,
                    lastName = res.LAST_NAME,
                    email = res.EMAIL,
                    subscrEndDate = res.SUBSCR_END.Value,
                    devices = res.DEV_CNT,
                    priceListId = res.PRICE_LIST_ID,
                    type = (PaymentProcessors)res.PAY_SYS_TYPE
                }).ToArray();
            }
        }
        public static LicenseModel[] GetExpiredLicensesFiltered()
        {
            return DiscardExtendedLicenses(GetExpiredLicenses());
        }
        private static LicenseModel[] DiscardExtendedLicenses(LicenseModel[] licenses)
        {
            int trialPromocodeDuration = Convert.ToInt32(ConfigurationManager.AppSettings["trialPromocodeDuration"]);

            PriceListItemModel[] allPrices = PriceListItemModel.GetPriceList(null);

            //get all active licenses
            LicenseModel[] allNotPromoLicenses = GetLicenseList(null, null)
                .Where(license => license.type != PaymentProcessors.Promocode)
                .ToArray();

            return licenses.Where(res => res.subscriptionId == null &&
                          //check if the license is not trial or trial and is not continued
                          (res.type != PaymentProcessors.Promocode || (res.type == PaymentProcessors.Promocode &&
                                                                          //is trial license
                                                                          ((allPrices.Where(price => price.id == res.priceListId)
                                                                                    .First().duration == trialPromocodeDuration) &&
                                                                          //does not contain any active purchased licenses
                                                                          !(allNotPromoLicenses.Any(notPromo => notPromo.userId == res.userId))) ||
                                                                          !(allPrices.Where(price => price.id == res.priceListId)
                                                                                    .First().duration == trialPromocodeDuration))))
                    .ToArray();
        }

        public static LicenseModel[] GetLicenseListPaged(LicenseFilter filter, ref Pager pager)
        {
            using (var db = new DataAccessEntities())
            {
                var outPage = new ObjectParameter("PAGE", pager.Page);
                var outTotalPages = new ObjectParameter("TOTAL_PAGES", typeof(int));
                var outTotalRecords = new ObjectParameter("TOTAL_RECORDS", typeof(int));


                var result = db.GET_USERS_LICENSES_PAGED(
                    pager.RecordsPerPage,
                    filter.Id,
                    filter.UserId, filter.SubscriptionEndFrom,
                    filter.SubscriptionEndTo, filter.DeviceCnt,
                    filter.PaySysType,
                    filter.SubscriptionId,
                    filter.CreationDateFrom,
                    filter.CreationDateTo,
                    outPage,
                    outTotalPages,
                    outTotalRecords)
                         .Select(res => new LicenseModel
                         {
                             id = res.ID,
                             subscrEndDate = res.SUBSCR_END.Value,
                             devices = res.DEV_CNT,
                             type = (PaymentProcessors)res.PAY_SYS_TYPE.Value,
                             subscriptionId = res.SUBSCR_ID,
                             creationDate = res.CREATION_DATE.Value,
                             userId = res.USER_ID,
                             priceListId = res.PRICE_LIST_ID,
                             periodName = res.PERIOD_NAME

                         }).ToArray();

                pager.Page = Convert.ToInt32(outPage.Value);
                pager.TotalPages = Convert.ToInt32(outTotalPages.Value);
                pager.TotalRecords = Convert.ToInt32(outTotalRecords.Value);

                return result;


            }
        }

        public static void UpdateLicense(PaymentHistoryItemModel paymentHistoryItem)
        {
            using (var db = new DataAccessEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.UPDATE_LICENSE(paymentHistoryItem.id, paymentHistoryItem.afterPaymentSubscrEnd, paymentHistoryItem.deviceCount);

                        var outLicId = new ObjectParameter("OUT_LIC_ID", paymentHistoryItem.id);
                        var payId = new ObjectParameter("PAY_ID", typeof(int));
                        db.ADD_PAYMENT_HISTORY_ENTRY(
                            outLicId,
                            paymentHistoryItem.priceId,
                            paymentHistoryItem.paymentDate,
                            paymentHistoryItem.transactionId,
                            paymentHistoryItem.amount,
                            paymentHistoryItem.deviceCount,
                            paymentHistoryItem.afterPaymentSubscrEnd,
                            paymentHistoryItem.originalSubscrEnd,
                            paymentHistoryItem.paymentComment,
                            payId);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }

                }
            }
        }
        public static LicenseModel[] FilterLicenses(LicenseModel[] allLicenses, PriceListItemModel[] allPrices, int expiringSubscrPeriod, int trialPromocodeDuration, Func<LicenseModel, PriceListItemModel, bool> condition)
        {
            return allLicenses
                .Join(allPrices, license => license.priceListId, price => price.id, (license, price) => new { price = price, license = license })
                .Where(x => condition(x.license, x.price))
                .Select(x => x.license).ToArray();
        }
    }
}