﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpKml.Engine;
using SharpKml.Dom;
using SharpKml.Base;
using System.Data;

namespace HawkDataAccess.Models
{
    public class KmlExportModel
    {
        public List<GpsEntryModel> gpsEntries { get; set; }

        public KmlExportModel()
        {
            gpsEntries = new List<GpsEntryModel>();
        }

        public bool InitCoordinates(DataSet gpsData)
        {
            try
            {
                if (gpsData.Tables[0] != null)
                {
                    foreach (DataRow row in gpsData.Tables[0].Rows)
                    {
                        GpsEntryModel entry = new GpsEntryModel();

                        entry.accuracy = Convert.ToInt32(row["ACCURACY"]);
                        entry.altitude = Convert.ToInt32(row["ALTITUDE"]);
                        entry.bearing = Convert.ToInt32(row["BEARING"]);
                        entry.deviceTime = row["DEV_DATE"].ToString();
                        entry.latitude = Convert.ToDouble(row["LATITUDE"]);
                        entry.longitude = Convert.ToDouble(row["LONGITUDE"]);
                        entry.speed = Convert.ToDouble(row["SPEED"]);
                        entry.type = row["PROVIDER"].ToString();

                        gpsEntries.Add(entry);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string ExportCoordinates()
        {
            try
            {   
                Document document = new Document();

                foreach (GpsEntryModel entry in gpsEntries)
                {
                    Placemark placemark = new Placemark();

                    Point position = new Point();
                    position.Coordinate = new Vector(entry.latitude, entry.longitude, entry.altitude);

                    placemark.Geometry = position;
                    placemark.Description = new Description();
                    placemark.Description.Text = string.Format(General.InfoWindowTemplate, 
                                                               entry.deviceTime,
                                                               entry.type,
                                                               entry.latitude,
                                                               entry.longitude,
                                                               entry.accuracy,
                                                               entry.speed,
                                                               entry.altitude,
                                                               entry.bearing);
                    document.AddFeature(placemark);
                }

                // It's conventional for the root element to be Kml,
                // but you could use document instead.
                Kml kml = new Kml();
                kml.Feature = document;

                Serializer serializer = new Serializer();
                serializer.Serialize(kml);

                return serializer.Xml;
            }
            catch (Exception){
                return null;
            }
        }
    }
}
