﻿using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class PriceListItemModel
    {
        public int id { get; set; }
        public short? duration { get; set; }
        public string label { get; set; }
        public decimal? price { get; set; }
        public decimal? priceOther { get; set; }
        public decimal? retailPrice { get; set; }
        public DateTime? subscriptionEnd { get; set; }
        public PriceListItemType type { get; set; }
        public DateTime? startedOn { get; set; }

        public static void AddNewPrice(PriceListItemModel priceListItem)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                db.ADD_NEW_PRICE(priceListItem.id, Math.Abs(priceListItem.price.Value), Math.Abs(priceListItem.priceOther.Value), Math.Abs(priceListItem.retailPrice.Value), null);
            }
        }

        public static PriceListItemModel[] GetPriceList(PriceListItemType? type, DateTime? startDate = null)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                short? priceListItemType = (type == null) ? null : (short?)type.Value;
                return db.GET_PRICE_LIST(startDate, priceListItemType).Select(res => new PriceListItemModel
                {
                    id = res.ID,
                    duration = res.DURATION,
                    price = res.PRICE,
                    priceOther = res.PRICE_OTHER,
                    retailPrice = res.RETAIL_PRICE,
                    label = res.LABEL,
                    subscriptionEnd = res.SUBSCR_END,
                    type = (PriceListItemType)res.PAY_TYPE,
                    startedOn = res.DATE_FROM
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets Price list including fake price list item for subscription extend (no period selected)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subscrEndDate"></param>
        /// <param name="subsctiptionExtendLabel">Display label</param>
        /// <returns></returns>
        public static PriceListItemModel[] GetPriceListForSubscrExtend(PriceListItemType? type, DateTime? subscrEndDate, string subsctiptionExtendLabel, int? currentPriceListId)
        {
            //The promo code prices are set to zero. As we need to get actual price for extending subscription we retrieve prices from 
            //corresponding subscription price list (we match them based on duration value)

            short? duration = default(short?);
            PriceListItemModel[] actualPriceList = GetPriceList(type, subscrEndDate);
            duration = actualPriceList.SingleOrDefault(res => res.id == currentPriceListId).duration;

            
            var index = default(int);

            
            var priceList = new List<PriceListItemModel>();
            var zeroDaysSubscription = new PriceListItemModel //adding price option for 0 month subscription extend
            {
                id = Convert.ToInt32(ProductModel.CURRENT_PERIOD_PRICE_LIST_VALUE),
                label = subsctiptionExtendLabel,
                price = 0,
                priceOther = 0,
                subscriptionEnd = subscrEndDate,
                duration = duration,
                type = PriceListItemType.SubscriptionItem,
                retailPrice = 0
            };

            priceList.AddRange(actualPriceList);

            if (type != PriceListItemType.PhoneSupport)
            {
                priceList.Insert(index, zeroDaysSubscription);
            }
            


            return priceList.ToArray();
        }

        /// <summary>
        /// This method is shortcut for GetPriceList to get Signup Fee price only
        /// </summary>
        /// <returns></returns>
        public static decimal GetSignupFeePrice()
        {
            PriceListItemModel item = GetPriceList(PriceListItemType.SignupFee).FirstOrDefault();
            return item.price.Value;
        }
    }
}
