﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkDataAccess.Models
{
    public struct AgentInfoModel
    {
        public string name {get;set;}
        public string id {get;set;}
        public string imageLink {get;set;}
        public string imei {get;set;}
        public bool active {get;set;}
    }
}