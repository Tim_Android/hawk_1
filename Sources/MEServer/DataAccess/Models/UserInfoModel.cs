﻿using Domain.Entities;
using Domain.Entities.Filters;
using HawkDataAccess.Models;
using HawkDataAccess.Models.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace HawkDataAccess.Models
{
    public class UserInfoModel : UserInfoBase
    {
        public int id { get; set; }
        public Guid? aspUserId { get; set; }

        public static bool AddUserInfo(Guid membershipId, UserInfoModel userInfo)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var outId = new ObjectParameter("ID", typeof(int));
                db.ADD_USER_INFO(membershipId, userInfo.firstName, userInfo.lastName, userInfo.email, outId);
                if (outId.Value != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static UserInfoModel GetUserInfo(Guid membershipId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var userInfo = db.GET_USER_INFO(membershipId, null).Select(res => new UserInfoModel
                    {
                        id = res.ID,
                        email = res.EMAIL,
                        firstName = res.FIRST_NAME,
                        lastName = res.LAST_NAME
                    }).FirstOrDefault();
                return userInfo;
            }
        }

        public static UserInfoModel GetUserInfo(string subscriptionId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var userInfo = db.GET_USER_INFO(null, subscriptionId).Select(res => new UserInfoModel
                {
                    id = res.ID,
                    email = res.EMAIL,
                    firstName = res.FIRST_NAME,
                    lastName = res.LAST_NAME
                }).FirstOrDefault();
                return userInfo;
            }
        }

        public static IList<UserInfoModel> GetUserInfoPaged(UserInfoFilter filter, ref Pager pager)
        {
            //out parametrs
            var outPage = new ObjectParameter("PAGE", pager.Page);
            var outTotalPages = new ObjectParameter("TOTAL_PAGES", typeof(int));
            var outTotalRecords = new ObjectParameter("TOTAL_RECORDS", typeof(int));

            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_USER_INFO_PAGED(pager.RecordsPerPage, filter.Id, filter.FirstName, filter.LastName, filter.AspUserId,
                                              filter.Email, outPage, outTotalPages, outTotalRecords)
                          .Select(res => new UserInfoModel{
                              id = res.ID,
                              firstName = res.FIRST_NAME,
                              lastName = res.LAST_NAME,
                              email = res.EMAIL,
                              aspUserId = res.ASP_NET_USER_ID
                          }).ToList();
            }
        }

        /// <summary>
        /// Returns User account information for accounts that hasn't been activated (have no licenses)
        /// </summary>
        /// <param name="hoursFrom"></param>
        /// <param name="hoursTo"></param>
        /// <returns></returns>
        public static UserInfoModel[] GetNotActivatedAccountUserInfo(int hoursFrom, int? hoursTo)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_NOT_ACTIVATED_ACCOUNTS(hoursFrom, hoursTo).Select(res => new UserInfoModel
                {
                    email = res.EMAIL,
                    firstName = res.FIRST_NAME,
                    lastName = res.LAST_NAME
                }).ToArray();
            }
        }

        public static UserInfoModel[] GetExpiredAccountUserInfo(int days)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_EXPIRED_ACCOUNTS(days).Select(res => new UserInfoModel
                    {
                        email = res.EMAIL,
                        firstName = res.FIRST_NAME,
                        lastName = res.LAST_NAME
                    }).ToArray();
            }
        }

        public static bool UpdateUserInfo(Guid membershipId, UserInfoModel userInfo)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.UPDATE_USER_INFO(membershipId, userInfo.firstName, userInfo.lastName, userInfo.email);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool DeleteUserInfo(Guid membershipId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.DELETE_USER_INFO(membershipId);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}