﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models.Base
{
    public class UserInfoBase
    {
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}
