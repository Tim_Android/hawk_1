﻿using System;
using Domain.Entities.Enums;
namespace HawkDataAccess.Models
{
    public class DeviceInfo
    {
        public int id { get; set; }
        public string device { get; set; }
        public string deviceId { get; set; }
        public bool isOnline { get; set; }
        public int? licId { get; set; }
    }

    public class DeviceInfoEx : DeviceInfo
    {
        public string phoneNumber { get; set; }
        public string brand { get; set; }
        public string model { get; set; }
        public string os { get; set; }
        public string sdk { get; set; }
        public string firmware { get; set; }
        public string agent { get; set; }
        public string svn { get; set; }
        public string timeZone { get; set; }
        public string note { get; set; }
        public DateTime? licActDate { get; set; }
        public bool canBeDeactivated { get; set; }
        public OperatingSystemType  operSysType { get; set; }
    }

  
}
