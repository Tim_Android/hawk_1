﻿using Domain.Entities.Filters;
using HawkBilling.Core.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class PaymentHistoryItemModel
    {
        public int id { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int priceId { get; set; }
        public DateTime? paymentDate { get; set; }
        public string label { get; set; }
        public int deviceCount { get; set; }
        public decimal? price { get; set; }
        public decimal? priceOther { get; set; }
        public decimal amount { get; set; }
        public string transactionId { get; set; }
        public DateTime? originalSubscrEnd { get; set; }
        public DateTime? afterPaymentSubscrEnd { get; set; }
        public string paymentComment { get; set; }

        /// <summary>
        /// Gets payment history information
        /// </summary>
        /// <param name="licId"></param>
        /// <param name="userId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static PaymentHistoryItemModel[] GetPaymentHistory(int? licId, int? userId, PaymentHistoryFilter filter)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_PAYMENTS_HISTORY(licId, userId, filter.dateFrom, filter.dateTo, filter.email, filter.transactionId, null).Select(res => new PaymentHistoryItemModel
                {
                    id = res.ID_PAYMENT,
                    email = (res.EMAIL == null) ? null : res.EMAIL.Trim(),
                    firstName = (res.FIRST_NAME == null) ? null : res.FIRST_NAME.Trim(),
                    lastName = (res.LAST_NAME == null) ? null : res.LAST_NAME.Trim(),
                    paymentDate = res.PAY_DATE,
                    label = res.LABEL,
                    deviceCount = res.DEV_CNT,
                    price = res.PRICE,
                    priceOther = res.PRICE_OTHER,
                    amount = res.AMOUNT,
                    transactionId = res.TRANS_ID,
                    originalSubscrEnd = res.START_DATE,
                    afterPaymentSubscrEnd = res.FINISH_DATE,
                    paymentComment = (res.PAY_COMMENT == null) ? String.Empty : res.PAY_COMMENT
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets history entry information. There can be maximum of 2 records returned depending on payment operation type. One for current period and one for new period.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public static PaymentHistoryItemModel[] GetPaymentHistoryEntry(int userId, string transactionId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                return db.GET_PAYMENTS_HISTORY(null, userId, null, null, null, transactionId, null).Select(res => new PaymentHistoryItemModel
                {
                    id = res.ID_PAYMENT,
                    email = (res.EMAIL == null) ? null : res.EMAIL.Trim(),
                    firstName = (res.FIRST_NAME == null) ? null : res.FIRST_NAME.Trim(),
                    lastName = (res.LAST_NAME == null) ? null : res.LAST_NAME.Trim(),
                    paymentDate = res.PAY_DATE,
                    label = res.LABEL,
                    deviceCount = res.DEV_CNT,
                    price = res.PRICE,
                    priceOther = res.PRICE_OTHER,
                    amount = res.AMOUNT,
                    transactionId = res.TRANS_ID,
                    originalSubscrEnd = res.START_DATE,
                    afterPaymentSubscrEnd = res.FINISH_DATE,
                    paymentComment = (res.PAY_COMMENT == null) ? String.Empty : res.PAY_COMMENT
                }).ToArray();
            }
        }

        public static string BuildCommentString(bool isRecurringBillingEnabled, PaymentProcessors type)
        {
            string autochargeStatus = isRecurringBillingEnabled ? HawkDataAccess.General.LabelEnabled : HawkDataAccess.General.LabelDisabled;
            string paymentProcessorName = String.Empty;

            switch (type)
            {
                case PaymentProcessors.AuthorizeNet:
                    paymentProcessorName = HawkDataAccess.General.AuthorizeNetShortLabel;
                    break;
                case PaymentProcessors.PayPal:
                    paymentProcessorName = HawkDataAccess.General.PayPalLabel;
                    break;
                default:
                    paymentProcessorName = HawkDataAccess.General.UnknownLabel;
                    break;
            }

            string comment = String.Format(HawkDataAccess.General.ReceiptDescription, autochargeStatus, paymentProcessorName);

            return comment;
        }
    }
}
