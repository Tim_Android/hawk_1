﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class AutochargeUpdateModel
    {
        public int licId { get; set; }
        public string transactionId { get; set; }
    }
}
