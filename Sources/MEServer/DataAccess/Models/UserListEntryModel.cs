﻿using Domain.Entities;
using Domain.Entities.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class UserListEntryModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "aspUserId")]
        public Guid AspUserId { get; set; }

        [JsonProperty(PropertyName = "registrationDate")]
        public DateTime RegistrationDate { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "isEmailVerified")]
        public bool IsEmailVerified { get; set; }

        [JsonProperty(PropertyName = "hasPurchasedLicenses")]
        public bool HasPurchasedLicenses { get; set; }

        [JsonProperty(PropertyName = "hasNotUpdatedSubscr")]
        public bool HasNotUpdatedSubscr { get; set; }

        [JsonProperty(PropertyName = "isBanned")]
        public bool IsBanned { get; set; }

        public static IList<UserListEntryModel> GetUserList(UserManagementFilter filter, int licenseCheckPeriod, ref Pager pager)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var outPage = new ObjectParameter("PAGE", pager.Page);
                var outTotalPages = new ObjectParameter("TOTAL_PAGES", typeof(int));
                var outTotalRecords = new ObjectParameter("TOTAL_RECORDS", typeof(int));

                var result = db.GET_USER_LIST_PAGED(pager.RecordsPerPage, licenseCheckPeriod, filter.Id, filter.AspUserId,
                                                      filter.RegisteredFrom, filter.RegisteredTo, filter.Email, filter.IsEmailVerified,
                                                      filter.HasPurchasedLicenses, filter.HasNotUpdatedSubscr, filter.IsBanned, outPage, outTotalPages, outTotalRecords)
                         .Select(res => new UserListEntryModel { 
                              Id = res.ID,
                              AspUserId = res.ASP_NET_USER_ID.Value,
                              RegistrationDate = res.REGISTRATION_DATE,
                              FirstName = res.FIRST_NAME,
                              LastName = res.LAST_NAME,
                              Email = res.EMAIL,
                              IsEmailVerified = Convert.ToBoolean(res.EMAIL_IS_VERIFIED),
                              HasPurchasedLicenses = Convert.ToBoolean(res.HAS_PURCHASED_LICENSES),
                              HasNotUpdatedSubscr = Convert.ToBoolean(res.HAS_NOT_UPDATED_SUBSCR),
                              IsBanned = Convert.ToBoolean(res.IS_BANNED)

                         }).ToList();

                pager.Page = Convert.ToInt32(outPage.Value);
                pager.TotalPages = Convert.ToInt32(outTotalPages.Value);
                pager.TotalRecords = Convert.ToInt32(outTotalRecords.Value);

                return result;
            }
        }
    }
}
