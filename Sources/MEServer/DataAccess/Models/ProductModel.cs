﻿using Domain.Entities.Enums;
using HawkBilling.Core.Type;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkDataAccess.Models
{
    public class ProductModel
    {
        protected const int MAX_DEVICE_COUNT = 9999;
        public const string SESSION_NAME = "order";
        //dropdown list values
        public const string NEW_LICENSE_VALUE = "0";
        public const string NO_LICENSE_VALUE = "-1";
        public const string CURRENT_PERIOD_PRICE_LIST_VALUE = "-1";
        //parameters values
        public const string SELECTED_LICENSE = "selectedLicense";
        public const string DEV_COUNT = "devCount";
        public const string PRICE_LIST_ID = "priceListId";
        public const string SUPPORT_PRICE_LIST_ID = "supportPriceListId";
        //messages names
        public const string NEW_SUBSCRIPTION1 = "newSubscription1";
        public const string NEW_SUBSCRIPTION2 = "newSubscription2";
        public const string NEW_DEVICE = "newDevice";
        public const string SIGNUP_FEE = "signupFee";
        public const string SUBSCRIPTION_EXT = "subscriptionExt";
        public CalculatedTotalsModel totals;
        public ProductType productType;
        protected PriceListItemModel _priceListItem;
        protected int _additionalDevices;
        protected int? _licenseId;
        protected bool _isValid;
        protected List<OrderModelValidationErrorsType> validationErrors;

        public PriceListItemModel priceListItem
        {
            get
            {
                return _priceListItem;
            }
        }
        public int additionalDevices
        {
            get
            {
                return _additionalDevices;
            }
        }
        public int? licenseId
        {
            get
            {
                return _licenseId;
            }
        }
        public bool isValid
        {
            get 
            { 
                return _isValid; 
            }
        }
        public SubscriptionPaymentType? type { get; set; }
        public Dictionary<string, string> messages { get; set; }
        public bool isRecurringBillingEnabled { get; set; }

        public ProductModel(PriceListItemModel item, int additionalDevices = 0, int licenseId = 0)
        {
            _priceListItem = item;
            _additionalDevices = additionalDevices;
            _licenseId = licenseId;
            _isValid = false;
            isRecurringBillingEnabled = false;
            totals = null;
            validationErrors = new List<OrderModelValidationErrorsType>();
            messages = new Dictionary<string, string>();
            productType = ProductType.PhoneSubscription;
            DetermineOperationType();
        }

        protected virtual void DetermineOperationType()
        {
            switch (priceListItem.type)
            {
                case PriceListItemType.SignupFee:
                    type = SubscriptionPaymentType.SignupFee;
                    break;
                case PriceListItemType.SubscriptionItem:
                    if (licenseId == Convert.ToInt32(NEW_LICENSE_VALUE))
                    {
                        type = SubscriptionPaymentType.NewSubscription;
                    }
                    else
                    {
                        if (priceListItem.id.ToString() == ProductModel.CURRENT_PERIOD_PRICE_LIST_VALUE)
                        {//extending device count
                            type = SubscriptionPaymentType.NewDevice;
                        }
                        else
                        {//extending period or/and device count
                            type = SubscriptionPaymentType.SubscriptionExtend;
                        }
                    }
                    break;
                default:
                    type = null;
                    break;
            }
        }

        public virtual CalculatedTotalsModel CalculateTotals()
        {
            
            if (totals == null)
            {//calculate totals if not calculated already
                switch (type.Value)
                {
                    case SubscriptionPaymentType.NewSubscription:
                        totals = CalculatedTotalsModel.CalculatePayment(null, //new license
                                                               additionalDevices - 1, //calculation method assumes that you have 1 device and submit additional number
                                                               priceListItem.id);
                        break;
                    case SubscriptionPaymentType.NewDevice:
                        totals = CalculatedTotalsModel.CalculatePayment(licenseId,
                                                                   additionalDevices,
                                                                   null);
                        break;
                    case SubscriptionPaymentType.SubscriptionExtend:
                        totals = CalculatedTotalsModel.CalculatePayment(licenseId,
                                                                   additionalDevices,
                                                                   priceListItem.id);
                        break;
                    case SubscriptionPaymentType.SignupFee:
                        totals = CalculatedTotalsModel.CalculatePayment(null,
                                                               additionalDevices,
                                                               priceListItem.id);
                        break;
                }
            }
            return totals;
        }

        /// <summary>
        /// Method for calculating subscription amount
        /// </summary>
        /// <returns></returns>
        public virtual decimal CalculateSubscriptionTotals()
        {
            decimal subscrTotals = 0;

            if (type.Value == SubscriptionPaymentType.NewSubscription || type.Value == SubscriptionPaymentType.SignupFee)
            {//new subscriptions or signup fee. In both cases we create new subscription item
                subscrTotals = CalculateTotals().total;
            }
            else
            {
                LicenseModel licInfo = LicenseModel.GetLicenseInfo((int)licenseId);
                int deviceCount = licInfo.devices + additionalDevices; //total device count for updated subscription

                //pretend that we calculate prices for new licenses
                if (type.Value == SubscriptionPaymentType.NewDevice)
                {//then we add new devices only and have to calculate subscription totals differently    
                    CalculatedTotalsModel totals = CalculatedTotalsModel.CalculatePayment(licInfo.id, 
                                                      deviceCount,                     
                                                      null);  
                    subscrTotals = totals.total;
                }
                if (type.Value == SubscriptionPaymentType.SubscriptionExtend)
                {
                    CalculatedTotalsModel totals = CalculatedTotalsModel.CalculatePayment(null, 
                                                      deviceCount - 1,                     //subtract one device as we supply additional phones only to method
                                                      priceListItem.id);    //using new priceListId to calculate period price
                    subscrTotals = totals.total;
                }
            }
            return subscrTotals;
        }

        public virtual string GetOrderDescription()
        {
            string descr = String.Empty;
            switch (type.Value)
            {
                case SubscriptionPaymentType.NewSubscription:
                    descr = String.Format(messages[ProductModel.NEW_SUBSCRIPTION1], _priceListItem.label);
                    if (additionalDevices > 0)
                    {
                        descr += String.Format(messages[ProductModel.NEW_SUBSCRIPTION2], additionalDevices);
                    }
                    break;
                case SubscriptionPaymentType.NewDevice:
                    descr = String.Format(messages[ProductModel.NEW_DEVICE], additionalDevices);
                    break;
                case SubscriptionPaymentType.SubscriptionExtend:
                    descr = String.Format(messages[ProductModel.SUBSCRIPTION_EXT], _priceListItem.label);
                    if (additionalDevices > 0)
                    {
                        descr += String.Format(messages[ProductModel.NEW_SUBSCRIPTION2], additionalDevices);
                    }
                    break;
                case SubscriptionPaymentType.SignupFee:
                    descr = messages[ProductModel.SIGNUP_FEE];
                    break;
            }
            return descr;
        }

        public virtual DateTime GetSubscriptionStartDate(short expiringSubscrPeriod)
        {
            DateTime? subscrEnd = null;
            if (priceListItem.id.ToString() == ProductModel.CURRENT_PERIOD_PRICE_LIST_VALUE && licenseId > 0)
            {//then we add new devices only and have to calculate subscription start date differently
                LicenseModel licInfo = LicenseModel.GetLicenseInfo((int)licenseId);
                subscrEnd = licInfo.subscrEndDate;
            }
            else
            {
                subscrEnd = priceListItem.subscriptionEnd.Value;
            }

            TimeSpan expPeriod = new TimeSpan(expiringSubscrPeriod, 0, 0, 0);
            return subscrEnd.Value.Subtract(expPeriod);
        }

        public void Validate()
        {
            if (ValidatePriceList() && ValidateDeviceCount())
            {
                _isValid = true;
            }
            else
            {
                _isValid = false;
            }
        }

        protected bool ValidatePriceList()
        {
            if (_priceListItem == null)
            {
                validationErrors.Add(OrderModelValidationErrorsType.InvalidPriceId);
                return false;
            }
            else 
            {
                return true;
            }
        }

        protected bool ValidateDeviceCount()
        {
            if (_additionalDevices >= 0 && _additionalDevices <= ProductModel.MAX_DEVICE_COUNT)
            {
                return true;
            }
            else
            {
                validationErrors.Add(OrderModelValidationErrorsType.InvalidDeviceCount);
                return false;
            }
        }
    }
}