﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class AutochargeInfoModel
    {
        string timePeriodName { get; set; }
        public string label { get; set; }
        public decimal price { get; set; }
        public decimal priceOther { get; set; }
        public decimal total { get; set; }
        public DateTime paymentDate { get; set; }
    }
}
