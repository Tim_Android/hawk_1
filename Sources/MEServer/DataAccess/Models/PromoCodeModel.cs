﻿#region namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HawkDataAccess.Types;
using NLog;
using Domain.Entities;
using Domain.Entities.Filters;

#endregion

namespace HawkDataAccess.Models
{
    public class PromoCodeModel
    {
        private string _state;
        private string _expired;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public int id { get; set; }
        public string promoCode { get; set; }
        public int deviceCount { get; set; }
        public int totalActivationsAvailable { get; set; }
        public int? activationsUsed { get; set; }
        public int? activationsLeft { get; set; }
        public DateTime generatedDate { get; set; }
        public DateTime validFrom { get; set; }
        public DateTime validTo { get; set; }
        public string subscriptionPeriod { get; set; }
        public bool toDisable { get; set; }
        public bool toDelete { get; set; }


        public string stateLabel
        {
            get { return _state; }
            set
            {
                int temp;

                if (int.TryParse(value, out temp))
                {
                    switch (temp)
                    {
                        case (int)ActivationStateType.NotActivated:
                            _state = HawkCommonResources.Common.labelNotActivated;
                            break;
                        case (int)ActivationStateType.FullyActivated:
                            _state = HawkCommonResources.Common.labelFullyActivated;
                            break;
                        case (int)ActivationStateType.PartlyActivated:
                            _state = HawkCommonResources.Common.labelPartlyActivated;
                            break;
                    }
                }
                else
                {
                    _state = value;
                }
            }
        }

        public string expired
        {
            get { return _expired; }
            set
            {
                switch (int.Parse(value))
                {
                    case (int)ExpirationStateType.Expired:
                        _expired = HawkCommonResources.Common.labelExpired;
                        break;
                    case (int)ExpirationStateType.NotExpired:
                        _expired = HawkCommonResources.Common.labelNotExpired;
                        break;
                }
            }
        }

        public static int? Activate(string promoCode, int userId)
        {
            var activationStatus = new ObjectParameter("STATUS_ID", typeof(int));
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.ACTIVATE_PROMO_CODE(promoCode, userId, activationStatus);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                activationStatus.Value = (int)ErrorType.UnknownError;

            }
            if (string.IsNullOrWhiteSpace(activationStatus.Value.ToString()))
            {
                return null;
            }
            return (int?)activationStatus.Value;
        }

        public static int? DeletePromoCode(int id)
        {
            var deleteStatus = new ObjectParameter("STATUS_ID", typeof(int));
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.DELETE_POMO_CODE(id, deleteStatus);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                deleteStatus.Value = (int)ErrorType.UnknownError;
            }
            if (string.IsNullOrWhiteSpace(deleteStatus.Value.ToString()))
            {
                return null;
            }
            return (int?)deleteStatus.Value;
        }

        public static int? DisablePromoCode(int id)
        {

            var disabledStatus = new ObjectParameter("STATUS_ID", typeof(int));
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.DISABLE_PROMO_CODE(id, disabledStatus);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                disabledStatus.Value = (int)ErrorType.UnknownError;
            }
            if (string.IsNullOrWhiteSpace(disabledStatus.Value.ToString()))
            {
                return null;
            }
            return (int?)disabledStatus.Value;
        }

        public static IList<PromoCodeModel> GetPromoCodes(PromoCodeHistoryFilter filter, ref Pager pager)
        {
            pager.Page = (pager.Page == 0) ? 1 : pager.Page;

            //out parametrs
            var outPage = new ObjectParameter("PAGE", pager.Page);
            var outTotalPages = new ObjectParameter("TOTAL_PAGES", typeof(int));
            var outTotalRecords = new ObjectParameter("TOTAL_RECORDS", typeof(int));

            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    var res = db.GET_PROMO_CODE_HISTORY_PAGED(
                         pager.RecordsPerPage,
                         filter.PromoCode,
                         filter.GeneratedFrom,
                         filter.GeneratedTo,
                         filter.SubscriptionPeriodId,
                         filter.State,
                         filter.IsExpired,
                         outPage,
                         outTotalPages,
                         outTotalRecords
                         ).ToList();

                    pager.TotalPages = (int)outTotalPages.Value;
                    pager.TotalRecords = (int)outTotalRecords.Value;
                    pager.Page = (int)outPage.Value;

                    return res.Select(z => new PromoCodeModel()
                         {
                             id = z.ID,
                             promoCode = z.PROMO_CODE,
                             deviceCount = z.DEVICE_COUNT,
                             totalActivationsAvailable = z.TOTAL_ACTIVATIONS_AVAILABLE,
                             activationsUsed = z.ACTIVATION_USED,
                             activationsLeft = z.ACTIVE == false ? 0 : z.ACTIVATIONS_LEFT,
                             generatedDate = z.DATE_GENERATED,
                             validFrom = z.VALID_FROM,
                             validTo = z.VALID_TO,
                             subscriptionPeriod = z.LABEL,
                             stateLabel = z.ACTIVE == true ? Convert.ToString(z.STATE) : ActivationStateType.Disabled.ToString(),
                             expired = Convert.ToString(z.EXPIRED),
                             toDelete = (z.STATE == (int)ActivationStateType.NotActivated),
                             toDisable = (z.STATE == (int)ActivationStateType.PartlyActivated && z.ACTIVE == true)
                         }).ToList();
                }
            }

            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return null;
        }

        public static int? GenerateManuallyPromoCode(int termId, int deviceCount, int activationCount, DateTime dateFrom, DateTime dateTo, string promoCode)
        {
            return GeneratePromoCode(termId, deviceCount, activationCount, dateFrom, dateTo, promoCode);
        }

        public static int? GenerateAutomaticallyPromoCode(int termId, int deviceCount, int activationCount, DateTime dateFrom, DateTime dateTo, int codeCount)
        {
            return GeneratePromoCode(termId, deviceCount, activationCount, dateFrom, dateTo, null, codeCount);
        }

        #region Private Methods

        private static int? GeneratePromoCode(int termId, int deviceCount, int activationCount, DateTime dateFrom, DateTime dateTo, string promoCode, int codeCount = 0)
        {
            var status = new ObjectParameter("STATUS_ID", typeof(int));
            using (DataAccessEntities db = new DataAccessEntities())
            {
                if (promoCode != null)
                {
                    //generate manually
                    //promo code count should be set 0
                    db.ADD_PROMO_CODE(promoCode, termId, dateFrom, dateTo, deviceCount, activationCount, true, 0, status);
                }
                else
                {
                    //generate automatically
                    db.ADD_PROMO_CODE(null, termId, dateFrom, dateTo, deviceCount, activationCount, true, codeCount, status);
                }
            }

            if (string.IsNullOrWhiteSpace(status.Value.ToString()))
            {
                return null;
            }
            return (int?)status.Value;
        }

        #endregion
    }
}
