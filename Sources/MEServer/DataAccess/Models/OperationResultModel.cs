﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class OperationResultModel
    {
        public const int CODE_ERROR = 0;
        public const int CODE_SUCCESS = 1;
        public const int CODE_WARNING = 2;

        public int code { get; set; }
        public string message { get; set; }
    }
}
