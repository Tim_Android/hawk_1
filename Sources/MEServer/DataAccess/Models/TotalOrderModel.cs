﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class TotalOrderModel
    {
        public TotalOrderModel()
        {
            Products = new List<ProductModel>();
        }
        public List<ProductModel> Products { get; set; }
        public bool isRecurringBillingEnabled { get; set; }
        public decimal total { get; set; }
        public List<CalculatedTotalsModel> CalculateTotals()
        {
            var totals = new List<CalculatedTotalsModel>();
            total = 0;
            foreach (var item in Products)
            {
                var calculated = item.CalculateTotals();
                
                totals.Add(calculated);
                total += calculated.total;
            }
            
            return totals;
        }
    }
}
