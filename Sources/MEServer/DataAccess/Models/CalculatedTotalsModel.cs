﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class CalculatedTotalsModel
    {
        public int? existingDevices { get; set; }
        public int? newDevices { get; set; }
        public int? residualDays { get; set; }
        public decimal oldPeriodDevEach { get; set; }
        public decimal oldPeriodDevTotal { get; set; }
        public decimal newPeriodFirst { get; set; }
        public decimal newPeriodOther { get; set; }
        public decimal supportTotal { get; set; }
        public int? supportResidualDays { get; set; }
        public decimal deviceTotal { get; set; }
        public decimal total { get; set; }

        public static CalculatedTotalsModel CalculatePayment(int? licenseId, int? devCount, int? priceListId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var oldPeriodNewDeviceCost = new ObjectParameter("AMOUNT_OLD_PERIOD_NEW_DEV_EACH", typeof(decimal));
                var oldPeriodNewDeviceTotal = new ObjectParameter("AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL", typeof(decimal));
                var newPeriodFirst = new ObjectParameter("AMOUNT_NEW_PERIOD_FIRST", typeof(decimal));
                var newPeriodOther = new ObjectParameter("AMOUNT_NEW_PERIOD_OTHER", typeof(decimal));
                var total = new ObjectParameter("AMOUNT_TOTAL", typeof(decimal));
                var priceListIdOldPeriod = new ObjectParameter("PRICE_LIST_ID_OLD_PERIOD", typeof(int));
                db.CALCULATE_PAYMENT(licenseId, devCount, null, priceListId,
                                     oldPeriodNewDeviceCost, oldPeriodNewDeviceTotal, newPeriodFirst, newPeriodOther, total, priceListIdOldPeriod);

                int? residualDays = null;
                int? totalLicDevices = null;

                if (licenseId.HasValue)
                {
                    LicenseModel license = LicenseModel.GetLicenseInfo(licenseId.Value);
                    residualDays = license.residualDays;
                    totalLicDevices = license.devices;
                }

                return new CalculatedTotalsModel
                {
                    residualDays = residualDays,
                    existingDevices = totalLicDevices,
                    newDevices = devCount,
                    oldPeriodDevEach = Convert.ToDecimal(oldPeriodNewDeviceCost.Value),
                    newPeriodFirst = Convert.ToDecimal(newPeriodFirst.Value),
                    newPeriodOther = Convert.ToDecimal(newPeriodOther.Value),
                    deviceTotal = Convert.ToDecimal(total.Value),
                    total = Convert.ToDecimal(total.Value)
                };
            }

        }

        public static CalculatedTotalsModel CalculateSupportPayment(int? licenseId, int? priceListId)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var oldPeriodNewDeviceCost = new ObjectParameter("AMOUNT_OLD_PERIOD_NEW_DEV_EACH", typeof(decimal));
                var oldPeriodNewDeviceTotal = new ObjectParameter("AMOUNT_OLD_PERIOD_NEW_DEV_TOTAL", typeof(decimal));
                var newPeriodFirst = new ObjectParameter("AMOUNT_NEW_PERIOD_FIRST", typeof(decimal));
                var newPeriodOther = new ObjectParameter("AMOUNT_NEW_PERIOD_OTHER", typeof(decimal));
                var total = new ObjectParameter("AMOUNT_TOTAL", typeof(decimal));
                var priceListIdOldPeriod = new ObjectParameter("PRICE_LIST_ID_OLD_PERIOD", typeof(int));
                db.CALCULATE_SUPPORT_PAYMENT(licenseId, null, priceListId, newPeriodFirst, newPeriodOther, total, priceListIdOldPeriod);

                int? residualDays = null;

                if (licenseId.HasValue)
                {
                    LicenseModel license = LicenseModel.GetLicenseInfo(licenseId.Value);
                    residualDays = license.residualDays;
                }

                return new CalculatedTotalsModel
                {
                    supportResidualDays = residualDays,
                    supportTotal = Convert.ToDecimal(total.Value),
                    total = Convert.ToDecimal(total.Value),
                    newPeriodFirst = Convert.ToDecimal(newPeriodFirst.Value),
                    newPeriodOther = Convert.ToDecimal(newPeriodOther.Value)
                };
            }

        }
    }
}
