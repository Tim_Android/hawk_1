﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class BannedEmailModel
    {
        public int id { get; set; }
        public string email { get; set; }
        public DateTime banDate { get; set; }
        public string note { get; set; }
    }
}
