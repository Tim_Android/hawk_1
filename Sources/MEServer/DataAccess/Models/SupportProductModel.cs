﻿using Domain.Entities.Enums;
using HawkBilling.Core.Type;
using HawkDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkDataAccess.Models
{
    public class SupportProductModel : ProductModel
    {
        const string ONLINE_LICENSE_VALUE = "Online Support";
        public SupportProductModel(PriceListItemModel item, int licenseId)
            : base(item, 0, licenseId) {
            productType = ProductType.SupportSubscription;
        }

        protected override void DetermineOperationType()
        {
            switch (priceListItem.type)
            {
                case PriceListItemType.PhoneSupport:
                    if (priceListItem.label == ONLINE_LICENSE_VALUE)
                    {
                        type = SubscriptionPaymentType.SignupFee;
                    }
                    else
                    {
                        if (licenseId == Convert.ToInt32(NEW_LICENSE_VALUE))
                        {
                            type = SubscriptionPaymentType.NewSubscription;
                        }
                        else
                        {
                            type = SubscriptionPaymentType.SubscriptionExtend;
                        }
                    }
                    break;
                default:
                    type = null;
                    break;
            }
        }

        public override CalculatedTotalsModel CalculateTotals()
        {
            
            if (totals == null)
            {//calculate totals if not calculated already
                switch (type.Value)
                {
                    case SubscriptionPaymentType.NewSubscription:
                        totals = CalculatedTotalsModel.CalculateSupportPayment(null, //new license
                                                               priceListItem.id);
                        break;
                    case SubscriptionPaymentType.SubscriptionExtend:
                        totals = CalculatedTotalsModel.CalculateSupportPayment(licenseId,
                                                                   priceListItem.id);
                        break;
                    case SubscriptionPaymentType.SignupFee:
                        totals = CalculatedTotalsModel.CalculateSupportPayment(null,
                                                               priceListItem.id);
                        break;
                }
            }
            return totals;
        }

        /// <summary>
        /// Method for calculating subscription amount
        /// </summary>
        /// <returns></returns>
        public override decimal CalculateSubscriptionTotals()
        {
            decimal subscrTotals = 0;

            if (type.Value == SubscriptionPaymentType.NewSubscription || type.Value == SubscriptionPaymentType.SignupFee)
            {//new subscriptions or signup fee. In both cases we create new subscription item
                subscrTotals = CalculateTotals().total;
            }
            else
            {
                LicenseModel licInfo = LicenseModel.GetLicenseInfo((int)licenseId);

               
                if (type.Value == SubscriptionPaymentType.SubscriptionExtend)
                {
                    CalculatedTotalsModel totals = CalculatedTotalsModel.CalculateSupportPayment(null, 
                                                      priceListItem.id);    //using new priceListId to calculate period price
                    subscrTotals = totals.total;
                }
            }
            return subscrTotals;
        }

        public override string GetOrderDescription()
        {
            string descr = String.Empty;
            switch (type.Value)
            {
                case SubscriptionPaymentType.NewSubscription:
                    descr = String.Format(messages[ProductModel.NEW_SUBSCRIPTION1], _priceListItem.label);
                    break;
                case SubscriptionPaymentType.SubscriptionExtend:
                    descr = String.Format(messages[ProductModel.SUBSCRIPTION_EXT], _priceListItem.label);
                    break;
                case SubscriptionPaymentType.SignupFee:
                    descr = messages[ProductModel.SIGNUP_FEE];
                    break;
            }
            return descr;
        }
    }
}