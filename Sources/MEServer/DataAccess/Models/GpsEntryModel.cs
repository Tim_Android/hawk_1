﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class GpsEntryModel
    {
        public string deviceTime { get; set; }
        public string type { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int accuracy { get; set; }
        public double speed { get; set; }
        public int altitude { get; set; }
        public int bearing { get; set; }
    }
}
