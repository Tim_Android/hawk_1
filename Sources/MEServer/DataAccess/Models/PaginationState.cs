﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class PaginationState
    {
        public int totalRecords { get; set; }
        public int totalPages { get; set; }
        public int page { get; set; }
        public int recordsPerPage { get; set; }
    }
}
