﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class AccountDetailsItemModel
    {
        public LicenseModel licenseInfo { get; set; }
        public AutochargeInfoModel autochargeInfo { get; set; }
    }
}
