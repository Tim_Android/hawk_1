﻿using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Domain.Entities;
using Domain.Entities.Enums;
using Domain.Entities.Filters;
using AutoMapper;
namespace HawkDataAccess
{
    public static class DeviceManagement
    {
        public const int idleTimeout = 3 * 60 * 60;

        public static DeviceInfoEx GetDeviceInfo(int agentId)
        {
            DeviceInfoEx deviceInfo = null;

            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    var agentName = new ObjectParameter("AGENT_NAME", typeof(string));
                    var deviceName = new ObjectParameter("DEVICE_NAME", typeof(string));
                    var deviceId = new ObjectParameter("DEVICE_ID", typeof(string));
                    var phoneNumber = new ObjectParameter("PHONE_NUMBER", typeof(string));
                    var brand = new ObjectParameter("BRAND", typeof(string));
                    var model = new ObjectParameter("MODEL", typeof(string));
                    var os = new ObjectParameter("OS", typeof(string));
                    var sdk = new ObjectParameter("SDK", typeof(string));
                    var firmwareId = new ObjectParameter("FIRMWARE_ID", typeof(string));
                    var svnVersion = new ObjectParameter("SVN_VERSION", typeof(string));
                    var timeZone = new ObjectParameter("TIME_ZONE", typeof(string));
                    var note = new ObjectParameter("NOTE", typeof(string));
                    var licActDate = new ObjectParameter("LIC_ACT_DATE", typeof(DateTime));
                    var canDeactivate = new ObjectParameter("CAN_DEACTIVATE", typeof(bool));
                    var licId = new ObjectParameter("LIC_ID", typeof(int));
                    var operSysType = new ObjectParameter("OPER_SYS_TYPE", typeof(int));

                    db.GET_DEVICE_INFO(agentId, agentName, deviceId, phoneNumber, brand, model, os, sdk, firmwareId, svnVersion, timeZone, deviceName, note, licActDate, canDeactivate, licId, operSysType);

                    deviceInfo = new DeviceInfoEx();
                    OperatingSystemType opSysType;
                    OperatingSystemType.TryParse(operSysType.Value.ToString(), out opSysType);

                    deviceInfo.agent = agentName.Value.ToString().Trim();
                    deviceInfo.brand = brand.Value.ToString().Trim();
                    deviceInfo.device = deviceName.Value.ToString().Trim();
                    deviceInfo.firmware = firmwareId.Value.ToString().Trim();
                    deviceInfo.id = agentId;
                    deviceInfo.deviceId = deviceId.Value.ToString().Trim();
                    deviceInfo.model = model.Value.ToString().Trim();
                    deviceInfo.note = note.Value.ToString().Trim();
                    deviceInfo.os = os.Value.ToString().Trim();
                    deviceInfo.phoneNumber = phoneNumber.Value.ToString().Trim();
                    deviceInfo.sdk = sdk.Value.ToString().Trim();
                    deviceInfo.svn = svnVersion.Value.ToString().Trim();
                    deviceInfo.timeZone = timeZone.Value.ToString().Trim();
                    deviceInfo.licActDate = (licActDate.Value is DBNull) ? (DateTime?)null : Convert.ToDateTime(licActDate.Value);
                    deviceInfo.canBeDeactivated = Convert.ToBoolean(canDeactivate.Value);
                    deviceInfo.licId = (licId.Value is DBNull) ? (int?)null : Convert.ToInt32(licId.Value);
                    deviceInfo.operSysType = opSysType;
                }
            }
            catch (Exception)
            {
                deviceInfo = null;
            }

            return deviceInfo;
        }

        //todo: use new methods bellow
        public static string GetAgentName(int agentId)
        {
            return GetDeviceInfo(agentId).agent;
        }

        public static List<DeviceInfo> GetAgentsForUser(Guid userID, bool onlyActive)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {

                return db.GET_AGENTS_BY_GUID(userID, onlyActive).Select(res => new DeviceInfo
                    {
                        id = res.ID,
                        device = res.DEVICE_NAME,
                        deviceId = res.DEVICE_ID,
                        isOnline = ((TimeSpan)(DateTime.Now - res.LAST_UPDATED)).TotalSeconds <= DeviceManagement.idleTimeout,
                        licId = res.LIC_ID
                    }).ToList<DeviceInfo>();
            }
        }

        public static bool ActivateDevice(int deviceId, int licenseId)
        {
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.ACTIVATE_DEVICE(deviceId, licenseId, null);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DeactivateDevice(int deviceId)
        {
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    db.DEACTIVATE_DEVICE(deviceId);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region NewMethods

        public static Device GetDevice(DeviceFilter filter)
        {
            var pager = new Pager();
            return GetDevices(filter, ref pager).SingleOrDefault();
        }

        public static IList<Device> GetDevices(DeviceFilter filter, ref Pager pager)
        {            
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var page = new ObjectParameter("Page", pager.Page);
                var totalRecords = new ObjectParameter("TotalRecords", typeof(int));
                var totalPages = new ObjectParameter("TotalPages", typeof(int));

                var result = db.GET_DEVICES_PAGED(
                    filter.Id,
                    filter.UserId,
                    filter.AgentWasActivated,
                    filter.LastUpdatedFrom,
                    filter.LastUpdatedTo,
                    filter.DeviceId,
                    filter.Agent,
                    filter.PhoneNumber,
                    filter.Brand,
                    filter.Model,
                    filter.Os,
                    filter.Sdk,
                    filter.FirmwareId,
                    filter.Svn,
                    filter.TimeZone,
                    filter.Device,
                    filter.Note,
                    filter.LicActDateFrom,
                    filter.LicActDateTo,
                    filter.LicId,
                    filter.OperSysType.HasValue ? (int)filter.OperSysType.Value : default(int?),
                    filter.CreationDateFrom,
                    filter.CreationDateTo,
                    filter.ShowUnassignedAgents,
                    pager.RecordsPerPage,
                    page,
                    totalRecords,
                    totalPages
                    ).Select(z => Mapper.Map<Device>(z)).ToList();

                pager.Page = (int)page.Value;
                pager.TotalPages = (int)totalPages.Value;
                pager.TotalRecords = (int)totalRecords.Value;

                return result;
            }

        }

        #endregion
    }
}
