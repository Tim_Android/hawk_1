﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities.Filters;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkDataAccess.Models
{
    public class BannedListManagement
    {
        public static IList<BannedEmailModel> GetBannedList(BannedEmailFilter filter, ref Pager pager)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                var page = new ObjectParameter("Page", pager.Page);
                var totalRecords = new ObjectParameter("TotalRecords", typeof(int));
                var totalPages = new ObjectParameter("TotalPages", typeof(int));

                var result = db.GET_BANNED_LIST_PAGED(
                    filter.Email,
                    filter.BanDateFrom,
                    filter.BanDateTo,
                    pager.RecordsPerPage,
                    page,
                    totalRecords,
                    totalPages
                    ).Select(z => Mapper.Map<BannedEmailModel>(z)).ToList();

                pager.Page = (int)page.Value;
                pager.TotalPages = (int)totalPages.Value;
                pager.TotalRecords = (int)totalRecords.Value;

                return result;
            }
        }

        public static bool AddEmailToBannedList(BannedEmailModel data)
        {
            var id = new ObjectParameter("ID", typeof(int));

            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.ADD_EMAIL_TO_BANNED_LIST(data.email, data.note, id);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool UpdateBannedEmail(BannedEmailModel data)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.UPDATE_BANNED_EMAIL(data.email, data.note);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool DeleteEmailFromBannedList(string email)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.DELETE_EMAIL_FROM_BANNED_LIST(email);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }       
    }
}
