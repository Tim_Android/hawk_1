﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Domain.Entities.WebServices.Wcf;
using Domain.Entities.WebServices.Wcf.Base;
namespace MobileEnterpriseServiceLib
{
    [ServiceContract]
    public interface IMobileEnterpriseService
    {
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "{agentID}/{deviceID}/Update",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        void OnUpdate(string agentID, string deviceID, UpdateInfo info);


        [OperationContract]
        [WebInvoke(
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "{agentID}/{deviceID}/UploadMMSAttachment/{agentAttachmentID}")]
        int OnUpload(string agentID, string deviceID, string agentAttachmentID, Stream data);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "Ping",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string OnPing();

        #region Secured API
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "Register",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        APIResponse Register(RegisterInfo info);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "ResetPassword",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        APIResponse ResetPassword(LoginInfo info);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "CreateAgent",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        CreationResponse OnCreateAgentSecured(DeviceInfo deviceinfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "Authentication",
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        Response OnAuthenticationSecured(AuthenticationInfo info);
        #endregion
    }
}
