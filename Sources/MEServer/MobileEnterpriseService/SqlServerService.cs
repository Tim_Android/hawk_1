﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web.Security;
using Domain.Entities;
using Domain.Entities.Enums;
using Domain.Entities.Filters;
using Domain.Entities.WebServices.Wcf;
using Domain.Entities.WebServices.Wcf.Base;
using Hawk.Infrastructure.WcfService;
using HawkDataAccess;
using Hawk.Infrastructure.AppLogger;
using System.IdentityModel.Claims;
using HawkDataAccess.Models;
using Hawk.Services;
using System.Configuration;
using HawkDataAccess.Const;
using Wcf = Domain.Entities.WebServices.Wcf;
using Hawk.Infrastructure.BLL;
using EmailManagement;
using EmailManagement.Types;

namespace MobileEnterpriseServiceLib
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    [AutomapperServiceBehavior]
    public class SqlServerService : IMobileEnterpriseService
    {
        public string OnPing()
        {
            return "Hello World";
        }

        public void OnUpdate(string agentID, string deviceID, UpdateInfo info)
        {
            if (null == info)
            {
                return;
            }

            try
            {
                DataAccess.UpdatedAgent(agentID, deviceID, info);
            }
            catch (Exception ex)
            {
                Logger.LogError("OnUpdate: transaction failed.");
                Logger.LogError(ex);
                throw;
            }
        }

        public int OnUpload(string agentID, string deviceID, string agentAttachmentID, Stream data)
        {
            int totalBytesRead = 0;

            try
            {
                const int bufferSize = 300 * 1024; // buffer 300 KB (max size of MMS messages)
                byte[] dataBuffer = new byte[bufferSize];
                using (MemoryStream dataMemory = new MemoryStream())
                {
                    dataMemory.Position = 0;
                    int bytesRead = 0;

                    do
                    {
                        bytesRead = data.Read(dataBuffer, 0, bufferSize);
                        dataMemory.Write(dataBuffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                    } while (bytesRead > 0);

                    DataAccess.WriteMMSAttachment(agentID, deviceID, Int32.Parse(agentAttachmentID), dataMemory.GetBuffer().Take(totalBytesRead).ToArray());
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Wrong attachment's data reading.");
                Logger.LogError(ex);
                throw;
            }
            finally
            {
                data.Close();
            }
            return totalBytesRead;
        }


        public CreationResponse OnCreateAgentSecured(Wcf.DeviceInfo deviceinfo)
        {
            return OnCreateAgent(deviceinfo.Login, deviceinfo.Password, deviceinfo.SystemType, deviceinfo);
        }

        public Response OnAuthenticationSecured(AuthenticationInfo info)
        {
            return OnAuthentication(info.Login, info.Password, info.AgentId);
        }
        public APIResponse Register(RegisterInfo info)
        {
            var status = ResponseStatus.UnknownError;
            try
            {
                if (!TokenService.VerifyDataToken(info))
                {
                    return new APIResponse
                    {
                        Status = ResponseStatus.NotValidData
                    };
                }
                if (BanListService.isBanned(info.Email))
                {
                    return new APIResponse
                    {
                        Status = ResponseStatus.UserBanned
                    };
                }

                if (Membership.GetUser(info.Email) != null)
                {
                    if (Roles.IsUserInRole(info.Email, AvailableRoles.CONFIRMED))
                    {
                        status = ResponseStatus.AccountExistsActivated;
                    }
                    else
                    {
                        status = ResponseStatus.AccountExistsNotVerifiedYet;
                    }
                    return new APIResponse
                    {
                        Status = status
                    };
                }


                Guid membershipId = (Guid)Membership.CreateUser(info.Email, info.Password, info.Email).ProviderUserKey;
                UserInfoModel userInfo = new UserInfoModel
                {
                    firstName = info.FirstName,
                    lastName = info.LastName,
                    email = info.Email,
                };

                if (!UserInfoModel.AddUserInfo(membershipId, userInfo))
                {//if we failed to add info, then try to remove created account
                    Membership.DeleteUser(info.Email);
                }
                else
                {
                    Roles.AddUserToRole(info.Email, HawkDataAccess.Const.AvailableRoles.USER);

                    AccountService.SendValidationEmail(info.Email,
                                                       Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]),
                                                       ConfigurationManager.AppSettings["RegistrationValidationUrl"],
                                                       userInfo);
                    status = ResponseStatus.Success;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("Register: registration failed.");
                Logger.LogError(ex);
                return new APIResponse
                {
                    Status = ResponseStatus.UnknownError
                };
            }
            return new APIResponse
            {
                Status = status
            };

        }

        public APIResponse ResetPassword(LoginInfo info)
        {
            var status = ResponseStatus.UnknownError;
            try
            {
                MembershipUser user = Membership.GetUser(info.Login);
                if (user != null)
                {
                    string token = Utils.GetRandomString(Convert.ToInt32(ConfigurationManager.AppSettings["defaultTokenLength"]));
                    if (TokenManagement.AddToken(user.Email, token))
                    {
                        UserInfoModel userInfo = UserInfoModel.GetUserInfo((Guid)user.ProviderUserKey);
                        Email emailData = EmailComposer.InitGeneralinfo(user.Email, EmailManagement.EmailResources.ConfirmPassResetSubj);

                        string port = Utils.GetPortParameter();
                        string passwordResetLink = Utils.ConvertRelativeUrlToAbsoluteUrl(ConfigurationManager.AppSettings["ResetPasswordUrl"] + token, port);

                        emailData.bodyParams.Add("@ResetLink@", passwordResetLink);
                        emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
                        emailData.bodyParams.Add("@LastName@", userInfo.lastName);
                        EmailComposer.SendEmailNotification(EmailTypes.ConfirmPasswordReset, emailData);

                        status = ResponseStatus.Success;
                    }
                    else
                    {
                        status = ResponseStatus.UnknownError;
                    }
                }
                else
                {
                    status = ResponseStatus.UserNotExists;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("ResetPassword: password resseting failed failed.");
                Logger.LogError(ex);
                return new APIResponse
                {
                    Status = ResponseStatus.UnknownError
                };
            }
            return new APIResponse
            {
                Status = status
            };
        }
        #region Private Methods

        private CreationResponse OnCreateAgent(string login, string password, string systemType, Wcf.DeviceInfo deviceinfo)
        {
            try
            {
                int? userId = null;
                int? agentId = null;

                bool isValid = Membership.ValidateUser(login, password);
                if (!isValid)
                {
                    return new CreationResponse { State = ResponseState.NotAuthenticated };
                }

                Guid providerUserKey = (Guid)(Membership.GetUser(login).ProviderUserKey);
                userId = DataAccess.GetUserInfoId(providerUserKey);

                // if agent already exists for current device, don`t create it and send old AgentId
                agentId = GetExistingAgentIdForUser(userId.Value, deviceinfo.DEVICE_INFO_DEVICE_ID);

                if (!agentId.HasValue)
                {
                    agentId = CreateAndUpdateAgent(login, systemType, deviceinfo);
                }

                return new CreationResponse
                {
                    State = ResponseState.Authenticated,
                    AgentId = agentId.Value
                };
            }
            catch (Exception ex)
            {
                Logger.LogError("Create agent failed.");
                Logger.LogError(ex);
            }
            return new CreationResponse { State = ResponseState.InternalError };
        }
        private Response OnAuthentication(string login, string password, string agentId)
        {
            try
            {
                bool isValid = Membership.ValidateUser(login, password);

                if (isValid)
                {
                    int agentIdValue = int.Parse(agentId);
                    Guid providerUserKey = (Guid)(Membership.GetUser(login).ProviderUserKey);

                    var result = DeviceManagement.GetAgentsForUser(providerUserKey, true).SingleOrDefault(z => z.id == agentIdValue);
                    if (result != null)
                    {
                        return new Response
                        {
                            State = ResponseState.Authenticated
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Authentication failed.");
                Logger.LogError(ex);

                return new Response
                {
                    State = ResponseState.InternalError
                };
            }
            return new Response
            {
                State = ResponseState.NotAuthenticated
            };
        }
        private int CreatAgent(string login, OperatingSystemType systemType)
        {
            string providerUserKey = Membership.GetUser(login).ProviderUserKey.ToString();
            int userId = DataAccess.GetUserId(providerUserKey);
            string agentName = systemType + " agent";

            return DataAccess.AddAgent(userId, agentName, (int)systemType);
        }

        private int CreateAndUpdateAgent(string login, string systemType, Wcf.DeviceInfo deviceinfo)
        {
            int agentId = default(int);
            OperatingSystemType operSysResult = default(OperatingSystemType);

            if (!OperatingSystemType.TryParse(systemType, out operSysResult))
            {
                throw new Exception("Invalid system type");
            }

            agentId = CreatAgent(login, operSysResult);
            try
            {
                //write info to show agent on the site
                UpdateInfo updatedInfo = new UpdateInfo
                {
                    deviceInfo = deviceinfo
                };
                DataAccess.UpdatedAgent(agentId.ToString(), deviceinfo.DEVICE_INFO_DEVICE_ID, updatedInfo);
            }
            catch (Exception ex)
            {
                DataAccess.RemoveAgent(agentId);
                throw ex;
            }
            return agentId;
        }

        private int? GetExistingAgentIdForUser(int userId, string deviceId)
        {
            DeviceFilter filter = new DeviceFilter
            {
                DeviceId = deviceId,
                UserId = userId
            };
            Device device = DeviceManagement.GetDevice(filter);

            if (device != null)
            {
                return device.Id;
            }
            return null;
        }

        #endregion
    }
}
