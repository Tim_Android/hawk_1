﻿use HAWK;

BEGIN TRANSACTION;
  BEGIN TRY;     
  declare @PL_ID int
    exec ADD_NEW_PRICE_LIST_ITEM  '7 days', 7, 3, @PL_ID output 
    exec ADD_NEW_PRICE @PL_ID, 0, 0
	

    COMMIT TRANSACTION;
  END TRY
BEGIN CATCH
  ROLLBACK TRANSACTION;
  SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
END CATCH
GO


-- updating stored procedures

if exists (select 1
          from sysobjects
          where  id = object_id('GET_USER_INFO_PAGED')
          and type in ('P','PC'))
   drop procedure GET_USER_INFO_PAGED
go


create procedure GET_USER_INFO_PAGED
	@RECORDS_PER_PAGE int,

	--search
	@ID int = null,
	@FIRST_NAME NAME_TYPE = null,
	@LAST_NAME NAME_TYPE = null,
	@ASP_NET_USER_ID uniqueidentifier = null,
	@EMAIL EMAIL_TYPE = null,
	--end search
			
	@PAGE int output,
	@TOTAL_PAGES int output,
    @TOTAL_RECORDS int output
with encryption
as
begin
    declare @START int
    declare @END int

	--show all records - should set @RECORDS_PER_PAGE and @PAGE, another values can be null
	;With Cte As
	(
		select ID 
		from USERS_INFO
		where(
                (@ID is null or ID = @ID) and
                (@FIRST_NAME is null or FIRST_NAME like '%'+@FIRST_NAME+'%') and
                (@LAST_NAME is null or LAST_NAME like '%'+@LAST_NAME+'%') and
                (@ASP_NET_USER_ID is null or ASP_NET_USER_ID = @ASP_NET_USER_ID) and
                (@EMAIL is null or EMAIL = @EMAIL)
              )
	)--With Cte As			
	select @TOTAL_RECORDS = count(ID)
	from Cte;

    if (@RECORDS_PER_PAGE is null)
    begin
        if (@TOTAL_RECORDS > 0)
           begin
               SELECT @RECORDS_PER_PAGE = @TOTAL_RECORDS;
           end
        else
           begin -- if no records found, then we assume that we have 1 record per page to avoid division by zero
               select @RECORDS_PER_PAGE = 1;
           end
    end

	set @TOTAL_PAGES = (@TOTAL_RECORDS + @RECORDS_PER_PAGE - 1) / @RECORDS_PER_PAGE;
	
	if @PAGE > @TOTAL_PAGES
		set @PAGE = @TOTAL_PAGES 
	
	set @START = ((@PAGE - 1) * @RECORDS_PER_PAGE)+1;
	set @END = @PAGE * @RECORDS_PER_PAGE;
	
	;With Cte As
	(
		select
			rn = row_number() Over(Order by (ID) DESC), 
			ID, 
			FIRST_NAME ,
			LAST_NAME,
			ASP_NET_USER_ID,
			EMAIL
		from USERS_INFO
		where(
                (@ID is null or ID = @ID) and
                (@FIRST_NAME is null or FIRST_NAME like '%'+@FIRST_NAME+'%') and
                (@LAST_NAME is null or LAST_NAME like '%'+@LAST_NAME+'%') and
                (@ASP_NET_USER_ID is null or ASP_NET_USER_ID = @ASP_NET_USER_ID) and
                (@EMAIL is null or EMAIL = @EMAIL)
              )
	)--With Cte As
	
	select  ID, 
			FIRST_NAME ,
			LAST_NAME,
			ASP_NET_USER_ID,
			EMAIL
	from Cte
	where rn between  @START and @END
end
go


grant execute on GET_USER_INFO_PAGED to HAWKWebSiteUser
go