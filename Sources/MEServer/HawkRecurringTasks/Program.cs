﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using HawkDataAccess;
using HawkDataAccess.Const;
using HawkDataAccess.Extensions;
using HawkDataAccess.Models;
using MoreLinq;
using EmailManagement;
using NLog;
using System.Web.Security;

namespace HawkRecurringTasks
{
    class Program
    {
        const int HOURS_IN_DAY = 24;
        const string ROOT_SERVER_KEY = "RootServerName";

        static Dictionary<string, string> parameters { get; set; }
        //TODO: Replace with Logging library
        static Logger logger { get; set; }


        static void Main(string[] args)
        {
            logger = LogManager.GetCurrentClassLogger();
            logger.Trace(General.RecurringTaskStarted);
            if (args.Length > 0)
            {
                try
                {
                    parameters = ProcessParameters(args.Skip(1).ToArray());
                    switch (args[0])
                    {
                        case RecurringTasks.CLEAN_CRASH_LOGS:
                            logger.Trace(RecurringTasks.CLEAN_CRASH_LOGS);
                            int? storagePeriodForUserData = GetParameter(RecurringTasksParamsConst.STORAGE_PERIOD_FOR_CRASH_LOGS).GetValueOrNull<int>();
                            int? crashLogsEntriesAllowed = GetParameter(RecurringTasksParamsConst.CRASH_LOGS_ENTRIES_ALLOWED).GetValueOrNull<int>();
                            RecurringTasksManagement.CleanCrashLogs(storagePeriodForUserData,
                                                                    crashLogsEntriesAllowed);
                            break;
                        case RecurringTasks.CLEAN_USER_DATA:
                            logger.Trace(RecurringTasks.CLEAN_USER_DATA);
                            int? storagePeriod = GetParameter(RecurringTasksParamsConst.STORAGE_PERIOD_FOR_USER_DATA).GetValueOrNull<int>();
                            RecurringTasksManagement.CleanUserData(storagePeriod);
                            break;
                        case RecurringTasks.EXPIRE_LICENSES:
                            logger.Trace(RecurringTasks.EXPIRE_LICENSES);
                            ExpireLicenses();
                            break;
                        case RecurringTasks.SEND_EXPIRATION_NOTICE:
                            logger.Trace(RecurringTasks.SEND_EXPIRATION_NOTICE);
                            int daysPriorExpiration = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_PRIOR_EXPIRATION));
                            SendExpirationNotice(daysPriorExpiration);
                            break;
                        case RecurringTasks.SEND_ACTIVATION_REMINDER:
                            logger.Trace(RecurringTasks.SEND_ACTIVATION_REMINDER);
                            int daysFromForReminder = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_FROM_FOR_ACTIVATION_REMINDER));
                            int daysToForReminder = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_TO_FOR_ACTIVATION_REMINDER));
                            int daysPriorAccountDeletion = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_PRIOR_NA_ACCOUNT_DEL));
                            SendActivationReminder(daysFromForReminder, daysToForReminder, daysPriorAccountDeletion);
                            break;
                        case RecurringTasks.DELETE_NOT_ACTIVATED_ACCOUNTS:
                            logger.Trace(RecurringTasks.DELETE_NOT_ACTIVATED_ACCOUNTS);
                            int daysPriorDeletion = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_PRIOR_NA_ACCOUNT_DEL));
                            DeleteNotActivatedAccounts(daysPriorDeletion);
                            break;
                        case RecurringTasks.DELETE_EXPIRED_ACCOUNTS:
                            logger.Trace(RecurringTasks.DELETE_EXPIRED_ACCOUNTS);
                            int daysBeforeExpiredAccDeletion = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_PRIOR_EXP_ACC_DEL));
                            DeleteExpiredAccounts(daysBeforeExpiredAccDeletion);
                            break;
                        case RecurringTasks.SEND_TRIAL_EXPIRED_NOTICE:
                            logger.Trace(RecurringTasks.SEND_TRIAL_EXPIRED_NOTICE);
                            int daysAfterExpiredPeriod = Convert.ToInt32(GetParameter(RecurringTasksParamsConst.DAYS_AFTER_EXPIRED_PERIOD));
                            SendTrialExpiredNotice(daysAfterExpiredPeriod);
                            break;
                        default:
                            PrintHelp();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Debug(ex.StackTrace);
                }
            }
            else
            {
                PrintHelp();
            }
            logger.Trace(General.RecurringTaskFinished);
        }

        static Dictionary<string, string> ProcessParameters(string[] args)
        {
            Dictionary<string, string> parsedParameters = new Dictionary<string, string>();
            foreach (string parameter in args)
            {
                string[] splitParameter = parameter.Split('=');
                if (splitParameter.Length != 2 || splitParameter[0] == String.Empty || splitParameter[1] == String.Empty) //invalid parameters are ignored
                {
                    continue;
                }
                parsedParameters.Add(splitParameter[0], splitParameter[1]);
            }
            return parsedParameters;
        }

        static string GetParameter(string paramName)
        {
            string result = null;
            if (parameters.ContainsKey(paramName))
            {//if parameters were supplied - use them
                result = parameters[paramName];
            }
            else if (ConfigurationManager.AppSettings[paramName] != null)
            {//if parameters weren't supplied trying to init with default ones from config
                result = ConfigurationManager.AppSettings[paramName];
            }
            return result;
        }

        static void PrintHelp()
        {
            Console.WriteLine("Application supports one of the following commands:\n");
            Console.WriteLine(RecurringTasks.CLEAN_CRASH_LOGS + " - Cleans crash logs\n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.STORAGE_PERIOD_FOR_CRASH_LOGS + " - period in days that is crash logs are stored \n");
            Console.WriteLine("       " + RecurringTasksParamsConst.CRASH_LOGS_ENTRIES_ALLOWED + " - maximal number of crash log records that are stored in database \n");

            Console.WriteLine(RecurringTasks.CLEAN_USER_DATA + " - Deletes user's data that is older than 45 days, deletes not activated or expired devices and data\n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.STORAGE_PERIOD_FOR_USER_DATA + " - period in days inactivated or expired devices are stored on server \n");

            Console.WriteLine(RecurringTasks.EXPIRE_LICENSES + " - Initiates expiration procedure for licenses where subscription has ended\n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       - as of now does not expect any parameters. All supplied parameters will be ignored \n");

            Console.WriteLine(RecurringTasks.SEND_EXPIRATION_NOTICE + " - Sends notifications to the users who are about to expire\n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_PRIOR_EXPIRATION + " - how many days in advance of expiration expiration notice will be sent \n");

            Console.WriteLine(RecurringTasks.SEND_ACTIVATION_REMINDER + " - Sends reminders to users that hasn't activated account yet. Typical usage scenario for parameters here is 1 and 2. That way notification are send only once\n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_FROM_FOR_ACTIVATION_REMINDER + " - number of days which have to pass for notifications to start sending to user \n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_TO_FOR_ACTIVATION_REMINDER + " - number of days which have to pass for notifications to stop sending to user \n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_PRIOR_NA_ACCOUNT_DEL + " - number of days which have to pass for not activated account to be deleted. Displayed in e-mail message \n");

            Console.WriteLine(RecurringTasks.DELETE_NOT_ACTIVATED_ACCOUNTS + " - Deletes not activated accounts that are \n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_PRIOR_NA_ACCOUNT_DEL + " - number of days which have to pass for not activated account to be deleted \n");

            Console.WriteLine(RecurringTasks.DELETE_EXPIRED_ACCOUNTS + " - Deletes accounts that don't have valid licenses for specified period of time \n");
            Console.WriteLine("   Expects:\n");
            Console.WriteLine("       " + RecurringTasksParamsConst.DAYS_PRIOR_EXP_ACC_DEL + " - number of days which have to pass for account with expired licenses to be deleted \n");
        }

        /// <summary>
        /// Send reminder that License is about to expire
        /// </summary>
        /// <param name="daysPriorExpiration"></param>
        /// <returns></returns>
        public static void SendExpirationNotice(int daysPriorExpiration)
        {
            LicenseModel[] expiringLicenses = LicenseModel.GetExpiringLicenses(daysPriorExpiration);
            //Get list of users that will get notification email about expiring subscriptions
            var affectedUserList = expiringLicenses.DistinctBy(res => res.email).ToArray();
            foreach (LicenseModel userLicense in affectedUserList)
            {
                LicenseModel[] userLicenses = expiringLicenses.Where(res => res.email == userLicense.email).ToArray();

                Email emailData = EmailComposer.InitGeneralinfo(userLicense.email, EmailManagement.EmailResources.LicenseExpirationNoticeSubj);
                string serverName = ConfigurationManager.AppSettings[ROOT_SERVER_KEY];
                EmailManager.SendLicenseExpirationNotice(userLicenses, emailData, serverName + EmailResources.BuySubscriptionLink);
            }
        }

        public static void SendTrialExpiredNotice(int daysAfterExpiredPeriod)
        {
            LicenseModel[] expiredLicenses = LicenseModel.GetExpiredTrialLicenses(daysAfterExpiredPeriod);

            //Get list of users that will get notification email about trial expired and not updated subscriptions
            foreach (LicenseModel userLicense in expiredLicenses)
            {
                Email emailData = EmailComposer.InitGeneralinfo(userLicense.email, EmailManagement.EmailResources.TrialLicenseExpiredNoticeSubj);
                EmailManager.SendTrialLicenseExpiredNotice(userLicense, emailData);
            }
        }
        public static void ExpireLicenses()
        {
            LicenseModel[] expiredLicenses = LicenseModel.GetExpiredLicensesFiltered();

            foreach (LicenseModel license in expiredLicenses)
            {
                Email emailData = EmailComposer.InitGeneralinfo(license.email, EmailManagement.EmailResources.LicenseExpiredNoticeSubj);
                EmailManager.SendLicenseExpiredNotice(license, emailData);

                AccountManagement.DeactivateLicense(license);
            }
        }

        public static void SendActivationReminder(int daysFrom, int daysTo, int daysPriorAccountDeletion)
        {
            UserInfoModel[] userAccounts = UserInfoModel.GetNotActivatedAccountUserInfo(daysFrom * Program.HOURS_IN_DAY, daysTo * Program.HOURS_IN_DAY);// converting days into hours

            decimal signupFeePrice = PriceListItemModel.GetSignupFeePrice();

            foreach (UserInfoModel user in userAccounts)
            {
                Email emailData = EmailComposer.InitGeneralinfo(user.email, EmailManagement.EmailResources.NotActivatedReminderSubj);
                EmailManager.SendAccountActivationReminder(user, signupFeePrice, daysPriorAccountDeletion, emailData);
            }
        }

        public static void DeleteNotActivatedAccounts(int days)
        {
            UserInfoModel[] userAccounts = UserInfoModel.GetNotActivatedAccountUserInfo(days * Program.HOURS_IN_DAY, null);// converting days into hours

            foreach (UserInfoModel user in userAccounts)
            {
                Email emailData = EmailComposer.InitGeneralinfo(user.email, EmailManagement.EmailResources.AccountDeletionSubj);
                EmailManager.SendAccountDeletionNotice(user, emailData);

                Membership.DeleteUser(user.email);
            }
        }

        public static void DeleteExpiredAccounts(int days)
        {
            UserInfoModel[] userAccounts = UserInfoModel.GetExpiredAccountUserInfo(days);
            foreach (UserInfoModel user in userAccounts)
            {
                Email emailData = EmailComposer.InitGeneralinfo(user.email, EmailManagement.EmailResources.AccountDeletionSubj);
                EmailManager.SendAccountDeletionNotice(user, emailData);

                Membership.DeleteUser(user.email);
            }
        }
    }
}
