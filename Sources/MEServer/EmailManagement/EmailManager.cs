﻿using EmailManagement.Models;
using EmailManagement.Types;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailManagement
{
    public static class EmailManager
    {
        public static bool SendSubscriptionCancellationNotice(LicenseModel licenseInfo, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", licenseInfo.firstName);
            emailData.bodyParams.Add("@LastName@", licenseInfo.lastName);
            emailData.bodyParams.Add("@LicId@", licenseInfo.id.ToString());
            try
            {
                EmailComposer.SendEmailNotification(EmailTypes.CancelSubscriptionAsPriceChanged, emailData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Occurs when payment system doesn't finish payment successfully
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="errorInfo"></param>
        /// <param name="subscriptionId"></param>
        /// <param name="emailData"></param>
        /// <returns></returns>
        public static bool SendAdminAutochargeTransactionError(UserInfoModel userInfo, string errorInfo, string subscriptionId, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);
            emailData.bodyParams.Add("@Email@", userInfo.email);
            emailData.bodyParams.Add("@ErrorInfo@", errorInfo);
            emailData.bodyParams.Add("@SubscrId@", subscriptionId);

            try
            {
                EmailComposer.SendEmailNotification(EmailTypes.AutochargeTransactionError, emailData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Occurs when payment has been successful but error ocured during subscription extension
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="transactionId"></param>
        /// <param name="subscriptionId"></param>
        /// <param name="emailData"></param>
        /// <returns></returns>
        public static bool SendAdminAutochargeProcessingError(UserInfoModel userInfo, string transactionId, string subscriptionId, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);
            emailData.bodyParams.Add("@Email@", userInfo.email);
            emailData.bodyParams.Add("@TransId@", transactionId);
            emailData.bodyParams.Add("@SubscrId@", subscriptionId);

            try
            {
                EmailComposer.SendEmailNotification(EmailTypes.AutochargeProcessingError, emailData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Sends user a message that autocharge cannot be completed for selected license
        /// </summary>
        /// <param name="licenseInfo"></param>
        /// <param name="emailData"></param>
        /// <returns></returns>
        public static bool SendAutochargeFailNotice(LicenseModel licenseInfo, UserInfoModel userInfo, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);
            emailData.bodyParams.Add("@LicId@", licenseInfo.id.ToString());
            try
            {
                EmailComposer.SendEmailNotification(EmailTypes.AutochargeFailed, emailData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void SendLicenseExpirationNotice(LicenseModel[] licenses, Email emailData, string redirectUrl)
        {
            emailData.bodyParams.Add("@FirstName@", licenses[0].firstName);
            emailData.bodyParams.Add("@LastName@", licenses[0].lastName);
            emailData.bodyParams.Add("@RedirectUrl@", redirectUrl);
            string renderedLicensesList = EmailComposer.RenderUnorderedList(licenses.Select(res => res.licenseName).ToList<string>());
            emailData.bodyParams.Add("@LicensesList@", renderedLicensesList);

            EmailComposer.SendEmailNotification(EmailTypes.LicenseExpirationNotice, emailData);
        }

        public static void SendTrialLicenseExpiredNotice(LicenseModel license, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", license.firstName);
            emailData.bodyParams.Add("@LastName@", license.lastName);
            emailData.bodyParams.Add("@LicId@", license.id.ToString());


            EmailComposer.SendEmailNotification(EmailTypes.TrialLicenseExpiredNotice, emailData);
        }

        public static bool SendAccountDeletionNotice(UserInfoModel userInfo, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", userInfo.firstName);
            emailData.bodyParams.Add("@LastName@", userInfo.lastName);

            try
            {
                EmailComposer.SendEmailNotification(EmailTypes.AccountDeletedNotice, emailData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void SendLicenseExpiredNotice(LicenseModel license, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", license.firstName);
            emailData.bodyParams.Add("@LastName@", license.lastName);
            emailData.bodyParams.Add("@LicId@", license.id.ToString());

            EmailComposer.SendEmailNotification(EmailTypes.LicenseExpiredNotice, emailData);
        }

        public static void SendAccountActivationReminder(UserInfoModel user, decimal signupFeeAmount, int daysPriorDeletion, Email emailData)
        {
            emailData.bodyParams.Add("@FirstName@", user.firstName);
            emailData.bodyParams.Add("@LastName@", user.lastName);
            emailData.bodyParams.Add("@SignupFeeAmount@", signupFeeAmount.ToString("F"));
            emailData.bodyParams.Add("@AccountActivationDeadline@", daysPriorDeletion.ToString());

            EmailComposer.SendEmailNotification(EmailTypes.NotActivatedAccountReminder, emailData);
        }
    }
}
