﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using EmailManagement.Models;
using HawkDataAccess.Models;
using EmailManagement.Types;

namespace EmailManagement
{
    public struct Email
    {
        public string from { get; set; }
        public string fromDisplayName { get; set; }
        public string to { get; set; }
        public string subj { get; set; }
        public string bodyTemplate { get; set; }
        //requires a list of parameters
        public Hashtable bodyParams { get; set; }
    }

    public static class EmailComposer
    {
        private static string body;

        public static Email InitGeneralinfo(string to, string subject)
        {
            Email emailData = new Email();
            emailData.from = EmailResources.DefaultAddr;
            emailData.fromDisplayName = EmailResources.FromDisplayName;
            emailData.subj = subject;
            emailData.to = to;
            emailData.bodyTemplate = EmailResources.BodyTemplate;
            emailData.bodyParams = new Hashtable();
            return emailData;
        }

        public static Email InitGeneralinfo(string to, string subject, string fromAddress, string fromAddressDisplay, string bodyTemplate)
        {
            Email emailData = new Email();
            emailData.from = fromAddress;
            emailData.fromDisplayName = fromAddressDisplay;
            emailData.subj = subject;
            emailData.to = to;
            emailData.bodyTemplate = bodyTemplate;
            emailData.bodyParams = new Hashtable();
            return emailData;
        }

        private static bool SendEmail(Email mailData)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(mailData.from, mailData.fromDisplayName);
            message.To.Add(mailData.to);
            message.Subject = mailData.subj;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Body = EmailComposer.body;
            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void CompileBody(Hashtable bodyParameters)
        {
            foreach (DictionaryEntry entry in bodyParameters)
            {
                EmailComposer.body = EmailComposer.body.Replace((string)entry.Key, (string)entry.Value);
            }
        }

        private static bool LoadBody(EmailTypes emailType)
        {
            try
            {
                using (DataAccessEntities db = new DataAccessEntities())
                {
                    EmailComposer.body = db.GET_EMAILS((int)emailType).Select(res => res.EMAIL_TEXT_HTML).FirstOrDefault();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SendEmailNotification(EmailTypes emailType, Email mailContent)
        {
            if (EmailComposer.LoadBody(emailType))
            {
                EmailComposer.body = String.Format(mailContent.bodyTemplate, EmailComposer.body); //merging body with template
                EmailComposer.CompileBody(mailContent.bodyParams);
                return EmailComposer.SendEmail(mailContent);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns rendered HTML string with rendered unordered list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string RenderUnorderedList(List<string> list)
        {
            string renderedString = String.Empty;
            foreach (string listItem in list)
            {
                renderedString += String.Format(EmailResources.UnorderedListItem, listItem);
            }
            renderedString = String.Format(EmailResources.UnorderedList, renderedString);
            return renderedString;
        }
    }
}
