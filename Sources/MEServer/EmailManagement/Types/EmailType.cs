﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailManagement.Types
{
    public enum EmailTypes
    {
        RegistrationValidation = 1,
        RegistrationValidationConfirm = 2,
        Registration = 3,
        ConfirmPasswordReset = 4,
        NewPassword = 5,
        CancelSubscriptionAsPriceChanged = 6,
        AutochargeTransactionError = 7,
        AutochargeProcessingError = 8,
        AutochargeFailed = 9, //admin's message to user that autocharge cannot be completed
        LicenseExpirationNotice = 10,
        LicenseExpiredNotice = 11,
        AccountDeletedNotice = 12,
        NotActivatedAccountReminder = 13,//user warning if he doesn't pay signup fee
        TrialLicenseExpiredNotice = 14
    }
}
