﻿using EmailManagement.Types;
using HawkDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailManagement.Models
{
    public class EmailModel 
    {
        public int id { get; set; }
        public string name { get; set; }
        public EmailTypes type { get; set; }
        public string content { get; set; }
        public string allowedVars { get; set; }

        public static EmailModel[] GetEmailList()
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                EmailModel[] emails = db.GET_EMAILS(null).Select(res => new EmailModel
                {
                    id = res.ID,
                    name = res.EMAIL_NAME,
                    type = (EmailTypes)res.EMAIL_TYPE,
                    content = res.EMAIL_TEXT_HTML,
                    allowedVars = res.ALLOWED_VARS
                }).ToArray();
                return emails;
            }
        }

        public static bool SaveEmailContent(string content, EmailTypes type)
        {
            using (DataAccessEntities db = new DataAccessEntities())
            {
                try
                {
                    db.UPDATE_EMAIL_CONTENT((int)type, content);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
