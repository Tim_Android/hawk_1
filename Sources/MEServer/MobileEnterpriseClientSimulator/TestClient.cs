﻿using System;
using System.IO;
using System.ServiceModel;
using Domain.Entities.WebServices.Wcf;
using Domain.Entities.WebServices.Wcf.Base;
using MobileEnterpriseServiceLib;
namespace MobileEnterpriseClientSimulator
{
    public class TestClient : ClientBase<IMobileEnterpriseService>, IMobileEnterpriseService
    {
        #region IMobileEnterpriseService Members

        public void OnUpdate(string agentID, string deviceID, UpdateInfo info)
        {
            base.Channel.OnUpdate(agentID, deviceID, info);
        }

        public int OnUpload(string agentID, string deviceID, string agentAttachmentID, Stream data)
        {
            return base.Channel.OnUpload(agentID, deviceID, agentAttachmentID, data);
        }

        public string OnPing()
        {
            return base.Channel.OnPing();
        }

        public Response OnAuthentication(string login, string password, string systemType)
        {
            throw new NotImplementedException();
        }

        public CreationResponse OnCreateAgent(string login, string password, string systemType, DeviceInfo deviceinfo)
        {
            throw new NotImplementedException();
        }

        public APIResponse Register(RegisterInfo info)
        {
            throw new NotImplementedException();
        }

        public APIResponse ResetPassword(LoginInfo info)
        {
            throw new NotImplementedException();
        }

        public CreationResponse OnCreateAgentSecured(DeviceInfo deviceinfo)
        {
            throw new NotImplementedException();
        }

        public Response OnAuthenticationSecured(AuthenticationInfo info)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
