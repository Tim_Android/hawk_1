﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using Domain.Entities.Enums;
using Domain.Entities.WebServices.Wcf;
using Domain.Entities.WebServices.Wcf.Base;

namespace MobileEnterpriseClientSimulator
{
    public class Program
    {
        private static int _mmsCount = 6;
        private static string _deviceID = ConfigurationManager.AppSettings["DeviceId"];
        private static string _login = ConfigurationManager.AppSettings["Login"];
        private static string _password = ConfigurationManager.AppSettings["Password"];
        private static string _operationSysType = ConfigurationManager.AppSettings["OperationSysType"];

        /// <summary>
        /// Start the application for the first time to create device. After that you should 
        /// activate devices on the site and start the application at second time
        /// </summary>
        private static void Main(string[] args)
        {
            using (TestClient client = new TestClient())
            {
                //check ping
                string pingResult = client.OnPing();
                if (string.IsNullOrWhiteSpace(pingResult))
                {
                    throw new Exception("Can not ping the web-service");
                }

                DeviceInfo devInfo = GetDevInfo(_deviceID);
                CreationResponse resCreateAgent = client.OnCreateAgent(_login, _password, _operationSysType, devInfo);
                if (resCreateAgent.State != ResponseState.Authenticated)
                {
                    throw new Exception("Can not create device");
                }
                Console.WriteLine("Device was created.........");

                string actualAgentId = resCreateAgent.AgentId.ToString();

                client.OnUpdate(actualAgentId, _deviceID, new UpdateInfo
                {
                    deviceInfo = devInfo,
                    callLog = GetCallList(1),
                    gpsLog = GetGpsList(),
                    smsLog = GetSmsList(),
                    mmsLog = GetMmsList(_mmsCount),
                    crashLog = new List<CrashLogEntry>
                    {
                        new CrashLogEntry
                        {
                            date = DateTime.Now,
                            data = "Test Data"
                        }
                    },
                    browserBookmarksLog = GetBookmarksList(),
                    browserHistoryLog = GetBrowserHistoryList()
                });
                Console.WriteLine("Device info was updated.........");

                UploadData(client, actualAgentId);
                Console.WriteLine("Added attachments(if device was activated).........");

                Response resAuth = client.OnAuthentication(_login, _password, actualAgentId);
                if (resAuth.State == ResponseState.Authenticated)
                {
                    Console.WriteLine("Authenticated.........");
                }
                else
                {
                    throw new Exception("Not Authenticated");
                }
            }

            Console.WriteLine("Everything was successfully." + Environment.NewLine + "Press any key.....");
            Console.ReadKey();
        }

        public static string GetType(int index)
        {
            switch (index)
            {
                case 1:
                    return "Inbox";
                case 2:
                    return "Sent";
                case 3:
                    return "Draft";
                case 4:
                    return "Outbox";
                case 5:
                    return "Failed";
                case 6:
                    return "Queued";
            }
            return null;
        }

        public static List<Mms> GetMmsList(int mmsCount)
        {
            string mmsType = null;
            string subject = null;
            int id = default(int);
            List<Mms> list = new List<Mms>();

            for (int i = 1; i <= mmsCount; i++)
            {
                mmsType = GetType(i);

                switch (i)
                {
                    case 1:
                        subject = "Subject " + i;
                        id = 0;
                        break;
                    case 2:
                        subject = "Subject " + i;
                        id = i;
                        break;
                    case 3:
                        subject = "Subject " + i;
                        id = i;
                        break;
                    case 4:
                        subject = "-=- Тема на русском-=-";
                        id = i;
                        break;
                    case 5:
                        subject = "Subject " + i + Environment.NewLine + "Attachments are failed";
                        id = 100;
                        break;
                    case 6:
                        subject = "Subject " + i;
                        id = i;
                        break;
                }

                list.Add(new Mms
                {
                    address = string.Format("{0}{0}-{0}{0}-{0}{0}", i),
                    contactName = string.Format("MMS Contact " + i),
                    body = string.Format("-=- M M S -=- : {0} {0} {0} - {0} {0} {0}", i),
                    date = DateTime.Now,
                    type = mmsType,
                    sub = subject,
                    attachments = new List<MMSAttachment>
                    {
                        new MMSAttachment
                        {
                            _id = int.Parse(id + "1"),
                            type = "text/plain"
                        },
                        new MMSAttachment
                        {
                            _id = int.Parse(id + "2"),
                            type = "image/jpeg"
                        },
                        new MMSAttachment
                        {
                            _id = int.Parse(id + "3"),
                            type = "image/png"
                        }
                    }
                });
            }
            return list;
        }

        public static List<Sms> GetSmsList()
        {
            string smsType = null;
            string body = null;

            List<Sms> list = new List<Sms>();
            for (int i = 1; i <= 6; i++)
            {
                smsType = GetType(i);
                switch (i)
                {
                    case 1:
                        body = string.Format("* * *  S M S  * * *: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                    case 2:
                        body = string.Format("* * *  S M S  * * *: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                    case 3:
                        body = string.Format("* * *  S M S  * * *: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                    case 4:
                        body = string.Format("* * *  S M S  * * *: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                    case 5:
                        body = string.Format("русский текст  С М С  русский текст: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                    case 6:
                        body = string.Format("* * *  S M S  * * *: {0} {0} {0} - {0} {0} {0}", i);
                        break;
                }
                list.Add(new Sms
                {
                    address = string.Format("{0}{0}{0}-{0}{0}{0}", i),
                    body = body,
                    date = DateTime.Now,
                    type = smsType,
                    contactName = string.Format("contactName" + i)
                });
            }
            return list;
        }

        public static List<Call> GetCallList(int index)
        {
            List<Call> list = new List<Call>();

            //set max lenght of fromNumberName
            StringBuilder name = new StringBuilder();
            for (int i = 0; i < 110; i++)
            {
                name.Append('t');
            }

            for (int i = 0; i < 3; i++)
            {
               
                list.Add(new Call
                {
                    fromNumber = "555-555",
                    fromNumberName = "TestFromNumberName" + name,
                    fromNumberType = "Type",
                    toNumber = "666-666",
                    toNumberName = "TestToNumberName",
                    toNumberType = "Type",
                    duration = index * (100 + i * 10),
                    date = DateTime.Now,
                    type = "Incoming"
                });
            }
            return list;
        }

        public static List<GPS> GetGpsList()
        {
            List<GPS> list = new List<GPS>();

            Random generator = new Random();

            for (int i = 0; i < 3; i++)
            {
                list.Add(new GPS
                {
                    latitude = generator.Next(-90, 90),
                    longitude = generator.Next(-180, 180),
                    altitude = 3.3,
                    accuracy = 4.4f,
                    bearing = 5.5f,
                    provider = "network",
                    speed = 11.11f,
                    time = DateTime.Now
                });
            }

            for (int i = 0; i < 2; i++)
            {
                list.Add(new GPS
                {
                    latitude = generator.Next(-90, 90),
                    longitude = generator.Next(-180, 180),
                    altitude = 33.33,
                    accuracy = 44.44f,
                    bearing = 55.55f,
                    provider = "gps",
                    speed = 12.12f,
                    time = DateTime.Now
                });
            }

            list.Add(new GPS
            {
                latitude = generator.Next(-90, 90),
                longitude = generator.Next(-180, 180),
                altitude = 333.333,
                accuracy = 444.444f,
                bearing = 555.555f,
                provider = "network",
                speed = 13.13f,
                time = DateTime.Now
            });

            for (int i = 0; i < 5; i++)
            {
                list.Add(new GPS
                {
                    latitude = generator.Next(-90, 90),
                    longitude = generator.Next(-180, 180),
                    altitude = 432.234,
                    accuracy = 345.543f,
                    bearing = 456.654f,
                    provider = "gps",
                    speed = 12345.54321f,
                    time = DateTime.Now
                });

            }

            return list;
        }

        private static DeviceInfo GetDevInfo(string deviceID)
        {
            return new DeviceInfo
            {
                DEVICE_INFO_AGENT_NAME = "TestEmulator",
                DEVICE_INFO_BRAND = "console application",
                DEVICE_INFO_DEVICE_ID = deviceID,
                DEVICE_INFO_FIRMWARE_ID = null,
                DEVICE_INFO_MODEL_NAME = null,
                DEVICE_INFO_OS_VERSION = null,
                DEVICE_INFO_PHONE_NUMBER = "0638074397",
                DEVICE_INFO_SDK_VERSION = null,
                DEVICE_INFO_SVN_VERSION = null,
                DEVICE_INFO_TIME_ZONE_NAME = null,
                DEVICE_INFO_TIME_ZONE_RAW_OFFSET = 0
            };
        }

        private static List<BrowserBookmark> GetBookmarksList()
        {
            List<BrowserBookmark> list = new List<BrowserBookmark>();

            list.Add(new BrowserBookmark
            {
                url = "http://google.com",
                title = "Google"
            });

            list.Add(new BrowserBookmark
            {
                url = "https://www.paraben.com/ultimate-forensic-bundle.html",
                title = "Paraben Corporation - Ultimate Forensic Bundle"
            });

            list.Add(new BrowserBookmark
            {
                url = "https://uk.wikipedia.org/wiki/%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D0%B0",
                title = "Україна — Вікіпедія"
            });

            return list;
        }

        private static List<BrowserHistoryItem> GetBrowserHistoryList()
        {
            List<BrowserHistoryItem> list = new List<BrowserHistoryItem>();

            Random generator = new Random();

            list.Add(new BrowserHistoryItem
            {
                url = "http://google.com",
                title = "Google",
                date = DateTime.UtcNow.AddDays(-1 * generator.Next(0, 30)),
                visits = 1
            });

            list.Add(new BrowserHistoryItem
            {
                url = "https://www.paraben.com/ultimate-forensic-bundle.html",
                title = "Paraben Corporation - Ultimate Forensic Bundle",
                date = DateTime.UtcNow.AddDays(-1 * generator.Next(0, 30)),
                visits = 1
            });

            list.Add(new BrowserHistoryItem
            {
                url = "https://uk.wikipedia.org/wiki/%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D0%B0",
                title = "Україна — Вікіпедія",
                date = DateTime.UtcNow.AddDays(-1 * generator.Next(0, 30)),
                visits = 4
            });

            list.Add(new BrowserHistoryItem
            {
                url = "http://google.com",
                title = "Google",
                date = DateTime.UtcNow.AddDays(-1 * generator.Next(0, 30)),
                visits = 2
            });

            list.Add(new BrowserHistoryItem
            {
                url = "https://www.google.com.ua/?gfe_rd=cr&ei=2YCWVqb0JqGO6AS71JKgBA&gws_rd=ssl#q=Paraben",
                title = "Paraben - Google Search",
                date = DateTime.UtcNow.AddDays(-1 * generator.Next(0, 30)),
                visits = 1
            });

            return list;
        }

        /// <summary>
        /// This code creates attachments for 6 MMS messages (each one for different MMS status type - inbox, sent, draft etc.) 
        /// in the DB for 1 device. Each MMS has 3 attachments with different MIME types (txt, png, jpg). Each attachment has its 
        /// unique id to prevent storing multiple copies of the same attachment in database if there were issues in connection with server.
        /// </summary>
        /// <remarks>
        /// To see attachments id in DB
        ///     <code>
        ///     select top(100) * 
        ///     from MESSAGES M
        ///     left join MMS_ATTACH A on M.ID_MSG = A.MSG_ID
        ///     order by ID_MSG desc
        ///     </code>
        /// </remarks>
        private static void UploadData(TestClient client, string actualAgentId)
        {
            string baseAddress = AppDomain.CurrentDomain.BaseDirectory;
            string contentAddress;
            int intLocation;

            intLocation = baseAddress.IndexOf("bin");

            contentAddress = baseAddress.Substring(0, intLocation);
            contentAddress = Path.Combine(contentAddress, "content");

            for (int i = 1; i <= 61; i += 10)
            {
                using (FileStream fs = File.OpenRead(Path.Combine(contentAddress, "test.txt")))
                {

                    client.OnUpload(actualAgentId, _deviceID, i.ToString(), fs);
                }
                using (FileStream fs = File.OpenRead(Path.Combine(contentAddress, "ALogo.jpg")))
                {
                    client.OnUpload(actualAgentId, _deviceID, (i + 1).ToString(), fs);
                }

                using (FileStream fs = File.OpenRead(Path.Combine(contentAddress, "logo.png")))
                {
                    client.OnUpload(actualAgentId, _deviceID, (i + 2).ToString(), fs);
                }
            }
        }
    }
}
