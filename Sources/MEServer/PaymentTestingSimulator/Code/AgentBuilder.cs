﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Net;

namespace HawkMobileEnterpriseWebSite
{
    static class AgentBuilder
    {
        private static string GetLocalIP()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if ((ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    /*|| (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)*/)
                {
                    return ip.ToString() + ":" + ConfigurationManager.AppSettings["portDataServiceConnection"];
                }
            }

            return "?";
        }

        private static string GetSVNRevision(string pathToSrc)
        {
            const string nameToolToGetSVNVersion = "svnversion";

            string svnRevision = "unknown";

            Process p = new Process();

            p.StartInfo.WorkingDirectory = pathToSrc;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = nameToolToGetSVNVersion;
            p.Start();
            svnRevision = p.StandardOutput.ReadLine();
            p.WaitForExit();

            return svnRevision;
        }

        private static void PatchAgentSettings(string pathToSrc, string agentId, string agentName)
        {
            string text;
            string path = Path.Combine(pathToSrc, ConfigurationManager.AppSettings["settingsPath"]);
            using (var reader = new StreamReader(path))
            {
                text = reader.ReadToEnd();
            }
            
            text = Regex.Replace(text, "AGENT_ID\\s+=\\s+\"([^\"]*)\"", String.Format("AGENT_ID = \"{0}\"", agentId));
            text = Regex.Replace(text, "DEVICE_INFO_AGENT_NAME\\s+=\\s+\"([^\"]*)\"", String.Format("DEVICE_INFO_AGENT_NAME = \"{0}\"", agentName));

            DateTime dt = DateTime.Now;
            text = Regex.Replace(text, "DEVICE_INFO_SVN_VERSION\\s+=\\s+\"([^\"]*)\"", String.Format("DEVICE_INFO_SVN_VERSION = \"{0}\"",
                dt.Year.ToString() + "." + dt.Month.ToString() + "." + dt.Day.ToString() + "." + GetSVNRevision(pathToSrc)));

            string ip = GetLocalIP();
            text = Regex.Replace(text, "OPT_WEBSERVER_DEF\\s+=\\s+\"([^\"]*)\"", String.Format("OPT_WEBSERVER_DEF = \"http://{0}\"", ip));
            text = Regex.Replace(text, "OPT_WEBSERVER_SSL_DEF\\s+=\\s+\"([^\"]*)\"", String.Format("OPT_WEBSERVER_SSL_DEF = \"https://{0}\"", ip));

            using (var writer = new StreamWriter(path))
            {
                writer.Write(text);
            }
        }

        public static string BuildAgentInstaller(string agentId, bool isForceBuild, string agentName)
        {
            string buildRoot = ConfigurationManager.AppSettings["buildRoot"];
            string resultApkName = ConfigurationManager.AppSettings["resultApkName"];
            string buildCommand = ConfigurationManager.AppSettings["buildCommand"];
            string sourceApkName = ConfigurationManager.AppSettings["apkName"];
            string buildEnv = ConfigurationManager.AppSettings["buildEnv"];

            string outDir = Path.Combine(Path.Combine(buildRoot, "installs"), agentId);
            string outFilePath = Path.Combine(outDir, resultApkName);

            try
            {
                bool isAgentExists = File.Exists(outFilePath);
                bool isBuildNeeds = isForceBuild || !isAgentExists;

                if (isAgentExists && isForceBuild)
                {
                    File.Delete(outFilePath);
                }

                if (isBuildNeeds)
                {
                    //// svn up
                    //using (Process p = new Process())
                    //{
                    //    p.StartInfo.WorkingDirectory = Path.Combine(buildRoot, "src");
                    //    p.StartInfo.FileName = Environment.GetEnvironmentVariable("COMSPEC");
                    //    p.StartInfo.Arguments = "/c \"svn up\"";
                    //    p.StartInfo.UseShellExecute = false;
                    //    p.StartInfo.RedirectStandardOutput = true;
                    //    p.StartInfo.RedirectStandardError = true;
                    //    p.StartInfo.CreateNoWindow = true;

                    //    StringBuilder sb = new StringBuilder();
                    //    p.Start();
                    //    sb.Append(p.StandardOutput.ReadToEnd());
                    //    sb.Append(p.StandardError.ReadToEnd());
                    //    p.WaitForExit();

                    //    if (p.ExitCode != 0)
                    //        throw new Exception("Build failed:\n" + sb.ToString());
                    //}

                    string dirName = Path.Combine(buildRoot, Guid.NewGuid().ToString());
                    DirectoryCopy(Path.Combine(buildRoot, "src"), dirName, true);

                    try
                    {
                        PatchAgentSettings(dirName, agentId, agentName);

                        using (Process p = new Process())
                        {
                            p.StartInfo.EnvironmentVariables["PATH"] = buildEnv + ";" + p.StartInfo.EnvironmentVariables["PATH"];
                            p.StartInfo.WorkingDirectory = dirName;
                            p.StartInfo.FileName = Environment.GetEnvironmentVariable("COMSPEC");
                            p.StartInfo.Arguments = String.Format("/c \"{0}\"", buildCommand);
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = true;
                            p.StartInfo.RedirectStandardError = true;
                            p.StartInfo.CreateNoWindow = true;

                            StringBuilder sb = new StringBuilder();
                            p.Start();
                            sb.Append(p.StandardOutput.ReadToEnd());
                            sb.Append(p.StandardError.ReadToEnd());
                            p.WaitForExit();

                            if (p.ExitCode != 0)
                            {
                                throw new Exception("Build failed:\n" + sb.ToString());
                            }

                            string apkPath = Path.Combine(dirName, Path.Combine("bin", sourceApkName));

                            Directory.CreateDirectory(outDir);
                            File.Copy(apkPath, outFilePath);
                        }
                    }
                    finally
                    {
                        Directory.Delete(dirName, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                throw new Exception("Agent build failed", ex);
            }
            return outFilePath;

        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}