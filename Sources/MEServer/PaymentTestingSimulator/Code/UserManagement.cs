﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.Web.Caching;
using HawkMobileEnterpriseWebSite.Models;

namespace HawkMobileEnterpriseWebSite
{
    public struct UserInfo
    {
        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

    }

    public class UserManagement
    {

        static UserManagement()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MobileEnterprise"].ConnectionString;
        }

        public static bool AddUserInfo(Guid membershipId, string firstName, string lastName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("ADD_USER_INFO", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ASP_USER_ID", SqlDbType.UniqueIdentifier);
                    command.Parameters["@ASP_USER_ID"].Value = membershipId;
                    command.Parameters.AddWithValue("@FIRST_NAME", firstName);
                    command.Parameters.AddWithValue("@LAST_NAME", lastName);
                    command.Parameters.Add("@ID", SqlDbType.Int);
                    command.Parameters["@ID"].Direction = ParameterDirection.Output;
                    command.ExecuteNonQuery();
                    if (command.Parameters["@ID"].Value != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public static int GetUserInfoId(Guid membershipId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                conn.Open();
                using (SqlCommand command = new SqlCommand("GET_USER_ID_BY_GUID", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ASP_USER_ID", SqlDbType.UniqueIdentifier);
                    command.Parameters["@ASP_USER_ID"].Value = membershipId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return (int)reader["ID"];
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
        }

        public static UserInfo GetUserInfo(Guid membershipId)
        {
            UserInfo result = new UserInfo();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("GET_USER_INFO", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ASP_USER_ID", SqlDbType.UniqueIdentifier);
                    command.Parameters["@ASP_USER_ID"].Value = membershipId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.FirstName = reader["FIRST_NAME"].ToString();
                            result.LastName = reader["LAST_NAME"].ToString();
                        }
                    }
                }
            }
            return result;
        }

        public static CustomerModel GetUserInfoEn(Guid membershipId)
        {
            using (HAWKEntities hawkDb = new HAWKEntities())
            {
                var userInfo = hawkDb.USERS_INFO.Where(res => res.ASP_NET_USER_ID == membershipId)
                                                .Select(res => new CustomerModel
                                                {
                                                    id = res.ID,
                                                    firstName = res.FIRST_NAME,
                                                    lastName = res.LAST_NAME
                                                }).FirstOrDefault();
                return userInfo;
            }
        }

        public static bool UpdateUserInfo(Guid membershipId, string firstName, string lastName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("UPDATE_USER_INFO", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ASP_USER_ID", SqlDbType.UniqueIdentifier);
                    command.Parameters["@ASP_USER_ID"].Value = membershipId;
                    command.Parameters.AddWithValue("@FIRST_NAME", firstName);
                    command.Parameters.AddWithValue("@LAST_NAME", lastName);

                    if (command.ExecuteNonQuery() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        private static readonly string connectionString;

    }
}
