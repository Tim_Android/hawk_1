﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PayPal.Manager;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;

namespace HawkMobileEnterpriseWebSite.Code.Billing
{
    public class PaypalPaymentProcessor : PaymentProcessor
    {
        private PayPalAPIInterfaceServiceService service;
        private PayPalDetails payPalInfo;

        public string token { get; set; }
        public string payerId { get; set; }

        public PaypalPaymentProcessor(CustomerDetails customerInfo,
                                      PaymentDetails paymentInfo,
                                      PayPalDetails payPalInfo,
                                      bool testMode,                          
                                      SubscriptionDetails subscrDetails = null)
        {
            this.customerInfo = customerInfo;
            this.paymentInfo = paymentInfo;
            this.subscrInfo = subscrDetails;
            this.payPalInfo = payPalInfo;
            this.testMode = testMode;
            this.service = new PayPalAPIInterfaceServiceService();
            token = null;
        }

        public override void ProcessPayment()
        {
            if (this.token == null)
            {
                throw new Exception(Resources.error_en.MissingTokenPayPal);
            }
            if (this.payerId == null)
            {
                throw new Exception(Resources.error_en.MissingPayerIdPayPal);
            }

            GetExpressCheckoutDetailsReq ecDetailsData = new GetExpressCheckoutDetailsReq();
            ecDetailsData.GetExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType(this.token);
            GetExpressCheckoutDetailsResponseType ecDetailsResponse = this.service.GetExpressCheckoutDetails(ecDetailsData);
            if (ecDetailsResponse.Ack == AckCodeType.SUCCESS)
            {
                // Create request object
                DoExpressCheckoutPaymentRequestType ecPaymentData = new DoExpressCheckoutPaymentRequestType();
                DoExpressCheckoutPaymentRequestDetailsType requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
                ecPaymentData.DoExpressCheckoutPaymentRequestDetails = requestDetails;

                requestDetails.PaymentDetails = ecDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PaymentDetails;
                requestDetails.Token = this.token;
                requestDetails.PayerID = this.payerId;
                requestDetails.PaymentAction = PaymentActionCodeType.SALE;

                DoExpressCheckoutPaymentReq ecPaymentRequest = new DoExpressCheckoutPaymentReq();
                ecPaymentRequest.DoExpressCheckoutPaymentRequest = ecPaymentData;

                DoExpressCheckoutPaymentResponseType ecPaymentResponse = this.service.DoExpressCheckoutPayment(ecPaymentRequest);

                if (ecPaymentResponse.Ack == AckCodeType.SUCCESS)
                {
                    this.paymentInfo.operResult.result = PaymentResult.Success;
                    this.paymentInfo.operResult.transactionId = ecPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID;
                    this.paymentInfo.operResult.message = Resources.success_en.Payment;
                    if (this.subscrInfo != null)
                    {
                        this.AddSubscription();
                    }
                }
                else
                {
                    this.paymentInfo.operResult.result = PaymentResult.Error;
                    this.paymentInfo.operResult.message = Resources.error_en.Payment;
                }
            }
            else
            {
                throw new Exception(Resources.error_en.MissingCheckoutDetailsPayPal);
            }
        }

        protected void AddSubscription()
        {
            CreateRecurringPaymentsProfileRequestType rpData = new CreateRecurringPaymentsProfileRequestType();
            CreateRecurringPaymentsProfileRequestDetailsType rpDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            rpData.CreateRecurringPaymentsProfileRequestDetails = rpDetails;

            rpDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType(this.subscrInfo.subscriptionStartDate.ToString("yyyy-MM-dd HH:mm:ss"));

            rpDetails.ScheduleDetails = new ScheduleDetailsType();
            rpDetails.ScheduleDetails.MaxFailedPayments = 0;
            rpDetails.ScheduleDetails.AutoBillOutstandingAmount = AutoBillType.NOAUTOBILL;

            rpDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            rpDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.DAY;
            rpDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = subscrInfo.billingInterval;
            rpDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = 0; //infinite subscription
            rpDetails.ScheduleDetails.Description = this.payPalInfo.productName; //should be exactly the same as baType.BillingAgreementDescription during initial call to SetExpressCheckout


            BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, this.paymentInfo.amount.ToString());
            rpDetails.ScheduleDetails.PaymentPeriod.Amount = amount;

            rpDetails.Token = this.token;

            CreateRecurringPaymentsProfileReq rpRequest = new CreateRecurringPaymentsProfileReq();
            rpRequest.CreateRecurringPaymentsProfileRequest = rpData;
            CreateRecurringPaymentsProfileResponseType rpResponse = service.CreateRecurringPaymentsProfile(rpRequest);
            if (rpResponse.Ack == AckCodeType.SUCCESS)
            {
                this.subscrInfo.operResult.result = PaymentResult.Success;
                this.subscrInfo.operResult.subscriptionId = rpResponse.CreateRecurringPaymentsProfileResponseDetails.ProfileID;
                this.subscrInfo.operResult.message = Resources.success_en.Subscription;
            }
            else
            {
                this.subscrInfo.operResult.result = PaymentResult.Error;
                this.subscrInfo.operResult.message = Resources.error_en.SubscriptionCreation;
            }
        }

        public string RequestRedirectUrl()
        {
            SetExpressCheckoutRequestType requestData = new SetExpressCheckoutRequestType();
            populateRequest(requestData);

            SetExpressCheckoutReq expressCheckoutRequest = new SetExpressCheckoutReq();
            expressCheckoutRequest.SetExpressCheckoutRequest = requestData;
            
            SetExpressCheckoutResponseType expressCheckoutResponse = this.service.SetExpressCheckout(expressCheckoutRequest);

            if (expressCheckoutResponse.Ack == AckCodeType.SUCCESS)
            {
                this.token = expressCheckoutResponse.Token;
                string paypalRedirect = null;
                if (testMode)
                {
                    paypalRedirect = Resources.billing.PayPalTestUrl + Resources.billing.PayPalExpressCheckoutCmd + expressCheckoutResponse.Token;
                }
                else
                {
                    paypalRedirect = Resources.billing.PayPalLiveUrl + Resources.billing.PayPalExpressCheckoutCmd + expressCheckoutResponse.Token;
                }
                return paypalRedirect;
            }
            else
            {
                return String.Empty;
            }
        }

        private void populateRequest(SetExpressCheckoutRequestType request)
        {
            SetExpressCheckoutRequestDetailsType expressCheckoutDetails = new SetExpressCheckoutRequestDetailsType();

            expressCheckoutDetails.ReturnURL = Utils.ConvertRelativeUrlToAbsoluteUrl(System.Web.VirtualPathUtility.ToAbsolute(this.payPalInfo.returnURL), HttpContext.Current.Request.Url.Port.ToString());
            expressCheckoutDetails.CancelURL = Utils.ConvertRelativeUrlToAbsoluteUrl(System.Web.VirtualPathUtility.ToAbsolute(this.payPalInfo.cancelURL), HttpContext.Current.Request.Url.Port.ToString());
            
            expressCheckoutDetails.ReqConfirmShipping = "0";
            expressCheckoutDetails.AddressOverride = "0";
            expressCheckoutDetails.NoShipping = "0";
            expressCheckoutDetails.SolutionType = SolutionTypeType.MARK; //SOLE - user does not need to have paypal account to checkout
                                                                         //MARK - user has to have paypal account
            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            expressCheckoutDetails.PaymentDetails.Add(paymentDetails);

            double orderTotal = 0;
            double itemTotal = 0;  // Sum of cost of all items in this order. For digital goods, this field is required.

            CurrencyCodeType currency = CurrencyCodeType.USD;
            paymentDetails.OrderDescription = this.paymentInfo.description; //max 127 alphanum chars
            paymentDetails.PaymentAction = PaymentActionCodeType.SALE;

            PaymentDetailsItemType itemDetails = new PaymentDetailsItemType();
            itemDetails.Name = this.payPalInfo.productName;
            itemDetails.Amount = new BasicAmountType(currency, this.paymentInfo.amount.ToString());
            itemDetails.Quantity = 1;
            itemDetails.Description = this.paymentInfo.description;
            paymentDetails.PaymentDetailsItem.Add(itemDetails);

            itemDetails.ItemCategory = ItemCategoryType.DIGITAL;

            itemTotal += Double.Parse(itemDetails.Amount.value) * itemDetails.Quantity.Value;
            orderTotal += itemTotal;

            paymentDetails.ItemTotal = new BasicAmountType(currency, itemTotal.ToString());
            paymentDetails.OrderTotal = new BasicAmountType(currency, orderTotal.ToString());

            if (this.subscrInfo != null)
            {
                //----------------Required for recurring billing initialization
                BillingCodeType billingCodeType = BillingCodeType.RECURRINGPAYMENTS;
                BillingAgreementDetailsType baType = new BillingAgreementDetailsType(billingCodeType);
                baType.BillingAgreementDescription = this.payPalInfo.productName;
                expressCheckoutDetails.BillingAgreementDetails.Add(baType);
            }

            request.SetExpressCheckoutRequestDetails = expressCheckoutDetails;
        }
    }
}