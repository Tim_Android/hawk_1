﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkMobileEnterpriseWebSite.Code.Billing
{
    public class Cart
    {
        public int planId { get; set; }
        public int additionalDevices { get; set; }

        public decimal CalculateOrderTotals()
        {
            return 15.00M;
        }

        public string GetSubscriptionOrderDescription()
        {
            return "1 month Subscription, 3 devices";
        }
    }
}