﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuthorizeNet;

namespace HawkMobileEnterpriseWebSite.Code.Billing
{
    public class AuthorizeNetPaymentProcessor : PaymentProcessor
    {
        public AuthorizeNetPaymentProcessor(CustomerDetails customerInfo,
                                            PaymentDetails paymentInfo,
                                            bool testMode,
                                            SubscriptionDetails subscrDetails = null)
        {
            this.customerInfo = customerInfo;
            this.paymentInfo = paymentInfo;
            this.subscrInfo = subscrDetails;
            this.testMode = testMode;
        }

        public override void ProcessPayment()
        {
            string expirationDate = this.paymentInfo.expirationMonth.ToString() + this.paymentInfo.expirationYear.ToString();
            var authorizationRequest = new AuthorizationRequest(this.paymentInfo.CC, expirationDate, this.paymentInfo.amount, this.paymentInfo.description);

            authorizationRequest.AddCardCode(this.paymentInfo.CVV);

            //Customer info - this is used for Fraud Detection
            authorizationRequest.AddCustomer(this.customerInfo.customerId.ToString(), this.customerInfo.firstName, this.customerInfo.lastName, String.Empty, String.Empty, String.Empty);

            Gateway gateway = null;
            if (testMode)
            {
                gateway = new Gateway(Resources.billing.AuthorizeNetTestAPILogin, Resources.billing.AuthorizeNetTestTransactionKey, this.testMode);
            }
            else
            {
                gateway = new Gateway(Resources.billing.AuthorizeNetLiveAPILogin, Resources.billing.AuthorizeNetLiveTransactionKey, this.testMode);
            }

            var authorizationResponse = gateway.Send(authorizationRequest);
            if (authorizationResponse.Approved)
            {
                this.paymentInfo.operResult.result = PaymentResult.Success;
                this.paymentInfo.operResult.message = authorizationResponse.Message;
                this.paymentInfo.operResult.transactionId = authorizationResponse.TransactionID;
                if (this.subscrInfo != null)
                {//if subscription information has been set, then create new subscription
                    this.AddSubscription();
                }
            }
            else
            {
                this.paymentInfo.operResult.result = PaymentResult.Error;
                this.paymentInfo.operResult.message = authorizationResponse.Message;
                this.paymentInfo.operResult.transactionId = String.Empty;
            }
        }


        public void AddSubscription()
        {
            SubscriptionRequest subscr = SubscriptionRequest.CreateNew();
            subscr.UsingCreditCard(this.customerInfo.firstName, this.customerInfo.lastName, this.paymentInfo.CC, this.paymentInfo.expirationYear, this.paymentInfo.expirationMonth);
            subscr.StartsOn = this.subscrInfo.subscriptionStartDate;
            subscr.CardCode = this.paymentInfo.CVV;

            subscr.Amount = this.paymentInfo.amount;
            subscr.BillingInterval = this.subscrInfo.billingInterval;
            subscr.BillingIntervalUnits = BillingIntervalUnits.Days;
            SubscriptionGateway gateway = null;
            if (this.testMode)
            {
                gateway = new SubscriptionGateway(Resources.billing.AuthorizeNetTestAPILogin, Resources.billing.AuthorizeNetTestTransactionKey, ServiceMode.Test);
            }
            else
            {
                gateway = new SubscriptionGateway(Resources.billing.AuthorizeNetLiveAPILogin, Resources.billing.AuthorizeNetLiveTransactionKey, ServiceMode.Live);
            }
            try
            {
                var subscriptionResponse = gateway.CreateSubscription(subscr);
                if (subscriptionResponse.SubscriptionID != null)
                {
                    this.subscrInfo.operResult.result = PaymentResult.Success;
                    this.subscrInfo.operResult.subscriptionId = subscriptionResponse.SubscriptionID;
                }
                else
                {//this is unknown error, should investigate additionally
                    this.subscrInfo.operResult.result = PaymentResult.Error;
                    this.subscrInfo.operResult.message = Resources.error_en.UnknownError;
                }
            }
            catch (Exception ex)
            {
                this.subscrInfo.operResult.result = PaymentResult.Error;
                this.subscrInfo.operResult.message = ex.Message;
            }
        }
    }
}