﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkMobileEnterpriseWebSite.Code.Billing
{
    public abstract class PaymentProcessor
    {
        public CustomerDetails customerInfo { get; set; }
        public PaymentDetails paymentInfo { get; set; }
        public SubscriptionDetails subscrInfo { get; set; }
        public bool testMode { get; set; }

        public abstract void ProcessPayment();
    }
}