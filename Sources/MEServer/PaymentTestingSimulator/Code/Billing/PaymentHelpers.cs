﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkMobileEnterpriseWebSite.Code.Billing
{
    public enum PaymentResult
    {
        Success = 1,
        Error = 0
    }

    public class CustomerDetails
    {
        public long customerId { set; get; }
        public string firstName { set; get; }
        public string lastName { set; get; }
    }

    public class PaymentDetails
    {
        public decimal amount { set; get; }
        public string CC { set; get; }
        public string CVV { set; get; }
        public string cardholderName { set; get; }
        public int expirationMonth { set; get; }
        public int expirationYear { set; get; }
        public string description { set; get; } //<num> month subscr with <num> devices
        public PaymentResponse operResult { get; set; }

        public PaymentDetails()
        {
            this.operResult = new PaymentResponse();
        }
    }

    public class SubscriptionDetails
    {
        public DateTime subscriptionStartDate { get; set; }
        public short billingInterval { get; set; }
        public PaymentResponse operResult { get; set; }

        public SubscriptionDetails()
        {
            this.operResult = new PaymentResponse();
        }
    }

    public class PayPalDetails
    {
        public string returnURL { get; set; }
        public string cancelURL { get; set; }
        public string productName { get; set; } //Hawk subscription
    }

    public class PaymentResponse
    {
        public PaymentResult result { get; set; }
        public string message { get; set; }
        public string transactionId { get; set; }
        public string subscriptionId { get; set; }

        public PaymentResponse()
        {
            this.transactionId = null;
            this.subscriptionId = null;
        }
    }
}