﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace HawkMobileEnterpriseWebSite
{
    public static class Utils
    {
        public static string GetRandomString(int count)
        {
            StringBuilder sb = new StringBuilder();
            char[] charList = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
            Random r = new Random();
            for (int i = 0; i < count; i++)
            {
                sb.Append(charList[r.Next(0, charList.Length - 1)]);
            }
            return sb.ToString();
        }

        public static string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl, string port = null) 
        {
            return string.Format("http{0}://{1}{2}{3}",
                            (System.Web.HttpContext.Current.Request.IsSecureConnection) ? "s" : String.Empty, 
                            System.Web.HttpContext.Current.Request.Url.Host,
                            (port != null) ? ":" + port : String.Empty,
                            relativeUrl);
        }
    }
}