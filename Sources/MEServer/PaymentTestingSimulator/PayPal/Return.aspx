﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Return.aspx.cs" Inherits="PaymentTestingSimulator.PayPal.Return" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        It happens: <br>
        <asp:Label runat="server" ID="error"></asp:Label>
    </div>
    <div>
       <asp:Label runat="server" ID="transactionInfo"></asp:Label>
    </div>
    <div>
        <asp:Label runat="server" ID="subscriptionInfo"></asp:Label>
    </div>
    </form>
</body>
</html>
