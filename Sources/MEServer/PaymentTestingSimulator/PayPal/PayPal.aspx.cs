﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HawkMobileEnterpriseWebSite.Code.Billing;
using PayPal.PayPalAPIInterfaceService.Model;
using HawkMobileEnterpriseWebSite;
using System.Configuration;
using NLog;

namespace PaymentTestingSimulator.PayPal
{
    public partial class PayPal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

            Cart shoppingCart = new Cart();

            CustomerDetails custInfo = new CustomerDetails();


            custInfo.customerId = 1;
            custInfo.firstName = "Tester";
            custInfo.lastName = "Test";

            PaymentDetails paymInfo = new PaymentDetails();
            paymInfo.amount = shoppingCart.CalculateOrderTotals();
            paymInfo.description = shoppingCart.GetSubscriptionOrderDescription();



            PayPalDetails paypalInfo = new PayPalDetails();
            paypalInfo.returnURL = Resources.billing.PayPalReturnURL;
            paypalInfo.cancelURL = Resources.billing.PayPalCancelURL;
            paypalInfo.productName = Resources.billing.ProductName;

            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);

            PaypalPaymentProcessor processor = new PaypalPaymentProcessor(custInfo, paymInfo, paypalInfo, testMode);

            string redirectURL = processor.RequestRedirectUrl();
            if (redirectURL != String.Empty)
            {
                Session["PayPalProcessor"] = processor;
                Response.Redirect(redirectURL, true);
            }


        }

       
    }
}