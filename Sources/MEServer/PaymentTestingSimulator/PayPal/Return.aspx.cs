﻿using HawkMobileEnterpriseWebSite.Code.Billing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PaymentTestingSimulator.PayPal
{
    public partial class Return : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PaypalPaymentProcessor processor = null;

            if (Session["PayPalProcessor"] != null)
            {
                processor = Session["PayPalProcessor"] as PaypalPaymentProcessor;
            }
            else
            {
                //TODO error processing
            }

            if (HttpContext.Current.Request.Params["token"] == null || HttpContext.Current.Request.Params["PayerID"] == null)
            {
            }
            // (Required) Unique PayPal buyer account identification number as returned in the GetExpressCheckoutDetails response
            processor.token = HttpContext.Current.Request.Params["token"];
            processor.payerId = HttpContext.Current.Request.Params["PayerID"];

            try
            {
                processor.ProcessPayment();
            }
            catch (Exception ex)
            {
                this.error.Text = ex.Message;
            }

            this.transactionInfo.Text = "TransactionId: " + processor.paymentInfo.operResult.transactionId.ToString() + "<br>"
                                        + "Message: " + processor.paymentInfo.operResult.message;
            if (processor.paymentInfo.operResult.result == PaymentResult.Success)
            {
                this.transactionInfo.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                this.transactionInfo.ForeColor = System.Drawing.Color.Red;
            }

            if (processor.subscrInfo != null)
            {
                this.subscriptionInfo.Text = "SubscriptionId: " + processor.subscrInfo.operResult.subscriptionId.ToString() + "<br>"
                                        + "Message: " + processor.subscrInfo.operResult.message;
                if (processor.subscrInfo.operResult.result == PaymentResult.Success)
                {
                    this.subscriptionInfo.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    this.subscriptionInfo.ForeColor = System.Drawing.Color.Red;
                }
            }

            //removing paypal payment information
            Session.Remove("PayPalProcessor");
            /*
            // Create the PayPalAPIInterfaceServiceService service object to make the API call
            PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService();

            GetExpressCheckoutDetailsReq ecDetailsData = new GetExpressCheckoutDetailsReq();
            // (Required) A timestamped token, the value of which was returned by SetExpressCheckout response.
            // Character length and limitations: 20 single-byte characters
            ecDetailsData.GetExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType(HttpContext.Current.Request.Params["token"]);
            // # API call 
            // Invoke the GetExpressCheckoutDetails method in service wrapper object
            GetExpressCheckoutDetailsResponseType ecDetailsResponse = service.GetExpressCheckoutDetails(ecDetailsData);

            // Create request object
            DoExpressCheckoutPaymentRequestType ecPaymentData = new DoExpressCheckoutPaymentRequestType();
            DoExpressCheckoutPaymentRequestDetailsType requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
            ecPaymentData.DoExpressCheckoutPaymentRequestDetails = requestDetails;

            requestDetails.PaymentDetails = ecDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PaymentDetails;
            // (Required) The timestamped token value that was returned in the SetExpressCheckout response and passed in the GetExpressCheckoutDetails request.
            requestDetails.Token = HttpContext.Current.Request.Params["token"];
            // (Required) Unique PayPal buyer account identification number as returned in the GetExpressCheckoutDetails response
            requestDetails.PayerID = HttpContext.Current.Request.Params["PayerID"];
            // (Required) How you want to obtain payment. It is one of the following values:
            // * Authorization – This payment is a basic authorization subject to settlement with PayPal Authorization and Capture.
            // * Order – This payment is an order authorization subject to settlement with PayPal Authorization and Capture.
            // * Sale – This is a final sale for which you are requesting payment.
            // Note: You cannot set this value to Sale in the SetExpressCheckout request and then change this value to Authorization in the DoExpressCheckoutPayment request.
            requestDetails.PaymentAction = PaymentActionCodeType.SALE;

            // Invoke the API
            DoExpressCheckoutPaymentReq ecPaymentRequest = new DoExpressCheckoutPaymentReq();
            ecPaymentRequest.DoExpressCheckoutPaymentRequest = ecPaymentData;
            // # API call 
            // Invoke the DoExpressCheckoutPayment method in service wrapper object
            DoExpressCheckoutPaymentResponseType ecPaymentResponse = service.DoExpressCheckoutPayment(ecPaymentRequest);

            if (ecPaymentResponse.Ack == AckCodeType.SUCCESS)
            { 
            }
            else
            { 
            }
            string transactionId = ecPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID;


            //////////////////////////////////Recurring Billing Section ///////////////////////////////////////////////////////


            CreateRecurringPaymentsProfileRequestType rpData = new CreateRecurringPaymentsProfileRequestType();
            CreateRecurringPaymentsProfileRequestDetailsType rpDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            rpData.CreateRecurringPaymentsProfileRequestDetails = rpDetails;

            //rpDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType(DateTime.Now.AddDays(30.0).ToString("yyyy-MM-dd HH:mm:ss"));
            rpDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType(DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss"));

            rpDetails.ScheduleDetails = new ScheduleDetailsType();
            rpDetails.ScheduleDetails.MaxFailedPayments = 0;
            rpDetails.ScheduleDetails.AutoBillOutstandingAmount = AutoBillType.NOAUTOBILL;

            rpDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            rpDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.DAY;
            rpDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = 1;
            rpDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = 0; //infinite subscription
            rpDetails.ScheduleDetails.Description = "HAWK Subscription"; //should be exactly the same as baType.BillingAgreementDescription during initial call to SetExpressCheckout

            
            BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, "16.0");
            rpDetails.ScheduleDetails.PaymentPeriod.Amount = amount;

            rpDetails.Token = HttpContext.Current.Request.Params["token"];

            CreateRecurringPaymentsProfileReq rpRequest = new CreateRecurringPaymentsProfileReq();
            rpRequest.CreateRecurringPaymentsProfileRequest = rpData;
            CreateRecurringPaymentsProfileResponseType rpResponse = service.CreateRecurringPaymentsProfile(rpRequest);
            if (rpResponse.Ack == AckCodeType.SUCCESS)
            {
            }
            else
            { 
            }*/
        }
    }
}