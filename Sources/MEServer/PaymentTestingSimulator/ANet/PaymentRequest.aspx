﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentRequest.aspx.cs" Inherits="PaymentTestingSimulator.ANet.PaymentRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">
            <div class="form-horizontal">
                <div class="control-group">
                    <asp:Label ID="lblCreditCard" runat="server" AssociatedControlID="tbCreditCard" CssClass="control-label">Credit Card Number:</asp:Label>
                    <div class="controls">
                        <asp:TextBox ID="tbCreditCard" runat="server" placeholder="Credit Card Number" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <asp:Label ID="lblCardHolderName" runat="server" AssociatedControlID="tbCardHolderName" CssClass="control-label">Name on Card:</asp:Label>
                    <div class="controls">
                        <asp:TextBox ID="tbCardHolderName" runat="server" placeholder="Name on Card"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <asp:Label ID="lblCVV" runat="server" AssociatedControlID="tbCVV" CssClass="control-label">CVC/CVV:</asp:Label>
                    <div class="controls">
                        <asp:TextBox ID="tbCVV" runat="server" placeholder="XXX"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <asp:Label ID="lblExpDate" runat="server" CssClass="control-label">Expiration Date:</asp:Label>
                    <div class="controls">
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="input-medium"></asp:DropDownList>
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="input-small"></asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-small" OnClick="Button1_Click" Text="Pay" />
                    </div>
                </div>
            </div>
            <div>
                <asp:Label ID="Result" runat="server"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
