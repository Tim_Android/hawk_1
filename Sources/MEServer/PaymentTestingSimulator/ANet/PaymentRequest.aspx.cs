﻿using HawkMobileEnterpriseWebSite;
using HawkMobileEnterpriseWebSite.Code.Billing;
using HawkMobileEnterpriseWebSite.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PaymentTestingSimulator.ANet
{
    public partial class PaymentRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["testingMode"]);
            Cart shoppingCart = new Cart();


            CustomerDetails custInfo = new CustomerDetails();

            //CustomerModel userInfo = UserManagement.GetUserInfoEn((Guid)Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey);
            custInfo.customerId = 1;
            custInfo.firstName = "Nik";
            custInfo.lastName = "Test";

            PaymentDetails paymInfo = new PaymentDetails();
            paymInfo.CC = tbCreditCard.Text;
            paymInfo.CVV = tbCVV.Text;
            paymInfo.amount = shoppingCart.CalculateOrderTotals();
            paymInfo.description = shoppingCart.GetSubscriptionOrderDescription();
            paymInfo.expirationMonth = Convert.ToInt32(ddlMonth.SelectedValue);
            paymInfo.expirationYear = Convert.ToInt32(ddlYear.SelectedValue);

            //subscription section
            SubscriptionDetails subscrInfo = new SubscriptionDetails();
            subscrInfo.billingInterval = 30;
            subscrInfo.subscriptionStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(30));

            AuthorizeNetPaymentProcessor processor = new AuthorizeNetPaymentProcessor(custInfo, paymInfo, testMode, subscrInfo);
            processor.ProcessPayment();

            Result.Text = processor.paymentInfo.operResult.message;
        }
    }
}