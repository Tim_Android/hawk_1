﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkMobileEnterpriseWebSite.Models
{
    public class CustomerModel
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}