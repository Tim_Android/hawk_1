﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HawkBilling.PaymentProcessors
{
    [Serializable]
    public class PaypalInitException : Exception
    {
        public PaypalInitException()
            : base() { }

        public PaypalInitException(string message)
            : base(message) { }

        public PaypalInitException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public PaypalInitException(string message, Exception innerException)
            : base(message, innerException) { }

        public PaypalInitException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected PaypalInitException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
