﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayPal.Manager;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;
using System.Configuration;
using HawkBilling.Core;
using HawkBilling.Core.Model;
using HawkBilling.Core.Type;

namespace HawkBilling.PaymentProcessors
{
    public class PaypalPaymentProcessor : PaymentProcessor
    {
        private const string PAYMENT_COMPLETED = "Completed";
        private const string PAYMENT_PENDING = "Pending";
        private const string PAYMENT_REVERSED = "Reversed";
        private const string PAYMENT_REFUNDED = "Refunded";
        private const string PAYMENT_CANCELLED_REVERSAL = "Canceled_Reversal";
        private const string PAYMENT_DENIED = "Denied";

        private PayPalAPIInterfaceServiceService service;
        
        public PayPalDetails payPalInfo;
        public string token { get; set; }
        public string payerId { get; set; }

        public PaypalPaymentProcessor(CustomerDetails customerInfo,
                                      PaymentDetails paymentInfo,
                                      PayPalDetails payPalInfo,
                                      bool testMode,                          
                                      List<SubscriptionDetails> subscrDetails = null)
        {
            this.customerInfo = customerInfo;
            this.paymentInfo = paymentInfo;
            this.subscrInfo = subscrDetails;
            this.payPalInfo = payPalInfo;
            this.testMode = testMode;
            this.paymentProcessor = HawkBilling.Core.Type.PaymentProcessors.PayPal;
            this.service = new PayPalAPIInterfaceServiceService();
            token = null;
        }

        public PaypalPaymentProcessor(bool testMode)
        {
            this.testMode = testMode;
            this.paymentProcessor = HawkBilling.Core.Type.PaymentProcessors.PayPal;
            this.service = new PayPalAPIInterfaceServiceService();
            token = null;
        }

        public override void ProcessPayment(Dictionary<string, string> additionalData = null)
        {
            if (additionalData["token"] != this.token)
            {//token mismatch that indicates possible forgery attempt
                throw new Exception(HawkBilling.Error.Payment);
            }
            this.payerId = additionalData["PayerID"];

            if (this.token == null)
            {
                throw new Exception(HawkBilling.Error.MissingTokenPayPal);
            }
            if (this.payerId == null)
            {

                throw new Exception(HawkBilling.Error.MissingPayerIdPayPal);
            }
            GetExpressCheckoutDetailsReq ecDetailsData = new GetExpressCheckoutDetailsReq();
            ecDetailsData.GetExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType(this.token);
            GetExpressCheckoutDetailsResponseType ecDetailsResponse = this.service.GetExpressCheckoutDetails(ecDetailsData);
            if (ecDetailsResponse.Ack == AckCodeType.SUCCESS)
            {
                
                // Create request object
                DoExpressCheckoutPaymentRequestType ecPaymentData = new DoExpressCheckoutPaymentRequestType();
                DoExpressCheckoutPaymentRequestDetailsType requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
                ecPaymentData.DoExpressCheckoutPaymentRequestDetails = requestDetails;

                requestDetails.PaymentDetails = ecDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PaymentDetails;
                requestDetails.Token = this.token;
                requestDetails.PayerID = this.payerId;
                requestDetails.PaymentAction = PaymentActionCodeType.SALE;

                DoExpressCheckoutPaymentReq ecPaymentRequest = new DoExpressCheckoutPaymentReq();
                ecPaymentRequest.DoExpressCheckoutPaymentRequest = ecPaymentData;

                DoExpressCheckoutPaymentResponseType ecPaymentResponse = this.service.DoExpressCheckoutPayment(ecPaymentRequest);
                if (ecPaymentResponse.Ack == AckCodeType.SUCCESS)
                {
                    this.paymentInfo.operResult.result = PaymentResult.Success;
                    this.paymentInfo.operResult.transactionId = ecPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID;
                    this.paymentInfo.operResult.message = HawkBilling.Success.Payment;
                    if (this.subscrInfo != null)
                    {
                        foreach (var item in subscrInfo)
                        {
                            this.AddSubscription(item);
                        }
                    }
                }
                else
                {
                    this.paymentInfo.operResult.result = PaymentResult.Error;
                    this.paymentInfo.operResult.message = HawkBilling.Error.Payment;
                }
            }
            else
            {
                throw new Exception(HawkBilling.Error.MissingCheckoutDetailsPayPal);
            }
        }

        protected void AddSubscription(SubscriptionDetails item)
        {
            CreateRecurringPaymentsProfileRequestType rpData = new CreateRecurringPaymentsProfileRequestType();
            CreateRecurringPaymentsProfileRequestDetailsType rpDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            rpData.CreateRecurringPaymentsProfileRequestDetails = rpDetails;
            rpDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType(item.subscriptionStartDate.ToString("yyyy-MM-dd HH:mm:ss"));

            rpDetails.ScheduleDetails = new ScheduleDetailsType();
            rpDetails.ScheduleDetails.MaxFailedPayments = 0;
            rpDetails.ScheduleDetails.AutoBillOutstandingAmount = AutoBillType.NOAUTOBILL;

            rpDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            rpDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.DAY;
            rpDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = item.billingInterval;
            rpDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = 0; //infinite subscription
            rpDetails.ScheduleDetails.Description = this.payPalInfo.productName; //should be exactly the same as baType.BillingAgreementDescription during initial call to SetExpressCheckout


            BasicAmountType amount = new BasicAmountType(CurrencyCodeType.USD, item.amount.ToString("F"));
            rpDetails.ScheduleDetails.PaymentPeriod.Amount = amount;

            rpDetails.Token = this.token;

            CreateRecurringPaymentsProfileReq rpRequest = new CreateRecurringPaymentsProfileReq();
            rpRequest.CreateRecurringPaymentsProfileRequest = rpData;
            CreateRecurringPaymentsProfileResponseType rpResponse = service.CreateRecurringPaymentsProfile(rpRequest);
            if (rpResponse.Ack == AckCodeType.SUCCESS)
            {
                item.operResult.result = PaymentResult.Success;
                item.operResult.subscriptionId = rpResponse.CreateRecurringPaymentsProfileResponseDetails.ProfileID;
                item.operResult.message = HawkBilling.Success.SubscriptionCreation;
            }
            else
            {
                item.operResult.result = PaymentResult.Error;
                item.operResult.message = HawkBilling.Error.SubscriptionCreation;
            }
        }

        public override SubscriptionDetails CancelSubscription(string subscriptionId)
        {
            ManageRecurringPaymentsProfileStatusReq request = new ManageRecurringPaymentsProfileStatusReq();

            request.ManageRecurringPaymentsProfileStatusRequest = new ManageRecurringPaymentsProfileStatusRequestType();

            request.ManageRecurringPaymentsProfileStatusRequest.ManageRecurringPaymentsProfileStatusRequestDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType(subscriptionId, StatusChangeActionType.CANCEL);

            ManageRecurringPaymentsProfileStatusResponseType response = this.service.ManageRecurringPaymentsProfileStatus(request);

            var result = new SubscriptionDetails();
            if (response.Ack == AckCodeType.SUCCESS)
            {
                result.operResult.result = PaymentResult.Success;
                result.operResult.message = HawkBilling.Success.SubscriptionCancellation;
            }
            else
            {
                result.operResult.result = PaymentResult.Error;
                result.operResult.message = HawkBilling.Error.Unknown;
            }
            return result;
        }

        public string RequestRedirectUrl(string returnUrl, string cancelUrl)
        {
            SetExpressCheckoutRequestType requestData = new SetExpressCheckoutRequestType();
            PopulateRequest(requestData, returnUrl, cancelUrl);

            SetExpressCheckoutReq expressCheckoutRequest = new SetExpressCheckoutReq();
            expressCheckoutRequest.SetExpressCheckoutRequest = requestData;
            
            SetExpressCheckoutResponseType expressCheckoutResponse = this.service.SetExpressCheckout(expressCheckoutRequest);

            if (expressCheckoutResponse.Ack == AckCodeType.SUCCESS)
            {
                this.token = expressCheckoutResponse.Token;
                string paypalRedirect = null;
                if (testMode)
                {
                    paypalRedirect = ConfigurationManager.AppSettings["PayPalTestUrl"] + ConfigurationManager.AppSettings["PayPalExpressCheckoutCmd"] + expressCheckoutResponse.Token;
                }
                else
                {
                    paypalRedirect = ConfigurationManager.AppSettings["PayPalLiveUrl"] + ConfigurationManager.AppSettings["PayPalExpressCheckoutCmd"] + expressCheckoutResponse.Token;
                }
                return paypalRedirect;
            }
            else
            {
                return String.Empty;
            }
        }

        private void PopulateRequest(SetExpressCheckoutRequestType request, string returnUrl, string cancelUrl)
        {
            SetExpressCheckoutRequestDetailsType expressCheckoutDetails = new SetExpressCheckoutRequestDetailsType();

            expressCheckoutDetails.ReturnURL = returnUrl;
            expressCheckoutDetails.CancelURL = cancelUrl;
            
            expressCheckoutDetails.ReqConfirmShipping = "0";
            expressCheckoutDetails.AddressOverride = "0";
            expressCheckoutDetails.NoShipping = "0";
            expressCheckoutDetails.SolutionType = SolutionTypeType.MARK; //SOLE - user does not need to have paypal account to checkout
                                                                         //MARK - user has to have paypal account
            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            expressCheckoutDetails.PaymentDetails.Add(paymentDetails);

            double orderTotal = 0;
            double itemTotal = 0;  // Sum of cost of all items in this order. For digital goods, this field is required.

            CurrencyCodeType currency = CurrencyCodeType.USD;
            paymentDetails.OrderDescription = this.paymentInfo.description; //max 127 alphanum chars
            paymentDetails.PaymentAction = PaymentActionCodeType.SALE;

            PaymentDetailsItemType itemDetails = new PaymentDetailsItemType();
            itemDetails.Name = this.payPalInfo.productName;
            itemDetails.Amount = new BasicAmountType(currency, this.paymentInfo.amount.ToString("F"));
            itemDetails.Quantity = 1;
            itemDetails.Description = this.paymentInfo.description;
            paymentDetails.PaymentDetailsItem.Add(itemDetails);

            itemDetails.ItemCategory = ItemCategoryType.PHYSICAL; //needs to be physical as Paraben's default account is not applicable to digital goods

            itemTotal += Double.Parse(itemDetails.Amount.value) * itemDetails.Quantity.Value;
            orderTotal += itemTotal;

            paymentDetails.ItemTotal = new BasicAmountType(currency, itemTotal.ToString());
            paymentDetails.OrderTotal = new BasicAmountType(currency, orderTotal.ToString());

            if (this.subscrInfo != null)
            {
                //----------------Required for recurring billing initialization
                BillingCodeType billingCodeType = BillingCodeType.RECURRINGPAYMENTS;
                BillingAgreementDetailsType baType = new BillingAgreementDetailsType(billingCodeType);
                baType.BillingAgreementDescription = this.payPalInfo.productName;
                expressCheckoutDetails.BillingAgreementDetails.Add(baType);
            }

            request.SetExpressCheckoutRequestDetails = expressCheckoutDetails;
        }

        #region Payment Status check methods

        public static bool IsCompleted(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_COMPLETED);
        }

        public static bool IsPending(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_PENDING);
        }

        public static bool IsReversed(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_REVERSED);
        }

        public static bool IsRefunded(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_REFUNDED);
        }

        public static bool IsCancelledReversal(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_CANCELLED_REVERSAL);
        }

        public static bool IsDenied(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_DENIED);
        }
        #endregion

    }
}