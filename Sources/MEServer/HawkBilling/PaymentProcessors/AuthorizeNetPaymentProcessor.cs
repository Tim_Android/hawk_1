﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuthorizeNet;
using System.Configuration;
using HawkBilling.Core.Model;
using HawkBilling.Core;
using HawkBilling.Core.Type;

namespace HawkBilling.PaymentProcessors
{
    public class AuthorizeNetPaymentProcessor : PaymentProcessor
    {
        private const string PAYMENT_COMPLETED = "1";

        #region Capture-specific data

        private string authorizationCode;
        private string transactionId;

        #endregion

        public AuthorizeNetPaymentProcessor(CustomerDetails customerInfo,
                                            PaymentDetails paymentInfo,
                                            bool testMode,
                                            List<SubscriptionDetails> subscrDetails = null)
        {
            this.customerInfo = customerInfo;
            this.paymentInfo = paymentInfo;
            this.subscrInfo = subscrDetails;
            this.testMode = testMode;
            this.paymentProcessor = HawkBilling.Core.Type.PaymentProcessors.AuthorizeNet;
        }

        public AuthorizeNetPaymentProcessor(bool testMode)
        {
            this.testMode = testMode;
            this.paymentProcessor = HawkBilling.Core.Type.PaymentProcessors.AuthorizeNet;
        }

        /// <summary>
        /// Authorizing transaction. Should always be called prior to ProcessPayment method
        /// </summary>
        /// <returns></returns>
        public bool AuthorizePayment()
        {
            string expirationDate = this.paymentInfo.expirationMonth.ToString() + this.paymentInfo.expirationYear.ToString();
            var authorizationRequest = new AuthorizationRequest(this.paymentInfo.CC, expirationDate, this.paymentInfo.amount, this.paymentInfo.description, false);

            authorizationRequest.AddCardCode(this.paymentInfo.CVV);

            //Customer info - this is used for Fraud Detection
            authorizationRequest.AddCustomer(this.customerInfo.customerId.ToString(), customerInfo.email, this.customerInfo.firstName, this.customerInfo.lastName, String.Empty, String.Empty, String.Empty, String.Empty);

            Gateway gateway = null;
            if (testMode)
            {
                gateway = new Gateway(ConfigurationManager.AppSettings["AuthorizeNetTestAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetTestTransactionKey"], testMode);
            }
            else
            {
                gateway = new Gateway(ConfigurationManager.AppSettings["AuthorizeNetLiveAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetLiveTransactionKey"], testMode);
            }

            var authorizationResponse = gateway.Send(authorizationRequest);
            if (authorizationResponse.Approved)
            {
                this.authorizationCode = authorizationResponse.AuthorizationCode;
                this.transactionId = authorizationResponse.TransactionID;
                return true;
            }
            else
            {
                this.paymentInfo.operResult.result = PaymentResult.Error;
                this.paymentInfo.operResult.message = authorizationResponse.Message;
                this.paymentInfo.operResult.transactionId = String.Empty;
                return false;
            }
        }

        public override void ProcessPayment(Dictionary<string, string> additionalData = null)
        {

            var expirationDate = paymentInfo.expirationMonth.ToString() + paymentInfo.expirationYear.ToString();
            var captureRequest = new PriorAuthCaptureRequest(paymentInfo.amount, transactionId);

            Gateway gateway = null;
            if (this.testMode)
            {
                gateway = new Gateway(ConfigurationManager.AppSettings["AuthorizeNetTestAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetTestTransactionKey"], testMode);
            }
            else
            {
                gateway = new Gateway(ConfigurationManager.AppSettings["AuthorizeNetLiveAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetLiveTransactionKey"], testMode);
            }

            var captureResponse = gateway.Send(captureRequest);
            if (captureResponse.Approved)
            {
                this.paymentInfo.operResult.result = PaymentResult.Success;
                this.paymentInfo.operResult.message = HawkBilling.Success.Payment;
                this.paymentInfo.operResult.transactionId = captureResponse.TransactionID;
                if (this.subscrInfo != null)
                {//if subscription information has been set, then create new subscription
                    foreach (var item in subscrInfo)
                    {
                        this.AddSubscription(item);
                    }
                }
            }
            else
            {
                this.paymentInfo.operResult.result = PaymentResult.Error;
                this.paymentInfo.operResult.message = HawkBilling.Error.Payment;
                this.paymentInfo.operResult.transactionId = String.Empty;
            }
        }

        public void AddSubscription(SubscriptionDetails item)
        {
            SubscriptionRequest subscr = SubscriptionRequest.CreateNew();
            subscr.UsingCreditCard(this.customerInfo.firstName, this.customerInfo.lastName, this.paymentInfo.CC, this.paymentInfo.expirationYear, this.paymentInfo.expirationMonth);
            subscr.StartsOn = item.subscriptionStartDate;
            subscr.CardCode = this.paymentInfo.CVV;

            subscr.Amount = item.amount;
            subscr.BillingInterval = item.billingInterval;
            subscr.BillingIntervalUnits = BillingIntervalUnits.Days;
            SubscriptionGateway gateway = null;
            if (testMode)
            {
                gateway = new SubscriptionGateway(ConfigurationManager.AppSettings["AuthorizeNetTestAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetTestTransactionKey"], ServiceMode.Test);
            }
            else
            {
                gateway = new SubscriptionGateway(ConfigurationManager.AppSettings["AuthorizeNetLiveAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetLiveTransactionKey"], ServiceMode.Live);
            }
            try
            {
                var subscriptionResponse = gateway.CreateSubscription(subscr);
                if (subscriptionResponse.SubscriptionID != null)
                {
                    item.operResult.result = PaymentResult.Success;
                    item.operResult.subscriptionId = subscriptionResponse.SubscriptionID;
                    item.operResult.message = HawkBilling.Success.SubscriptionCreation;
                }
                else
                {//this is unknown error, should investigate additionally
                    item.operResult.result = PaymentResult.Error;
                    item.operResult.message = HawkBilling.Error.Unknown;
                }
            }
            catch (Exception ex)
            {
                item.operResult.result = PaymentResult.Error;
                item.operResult.message = ex.Message;
            }
        }

        public override SubscriptionDetails CancelSubscription(string subscriptionId)
        {
            SubscriptionGateway gateway = null;
            if (testMode)
            {
                gateway = new SubscriptionGateway(ConfigurationManager.AppSettings["AuthorizeNetTestAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetTestTransactionKey"], ServiceMode.Test);
            }
            else
            {
                gateway = new SubscriptionGateway(ConfigurationManager.AppSettings["AuthorizeNetLiveAPILogin"], ConfigurationManager.AppSettings["AuthorizeNetLiveTransactionKey"], ServiceMode.Live);
            }
            var result = new SubscriptionDetails();
            try
            {
                if (gateway.CancelSubscription(subscriptionId))
                {
                    result.operResult.result = PaymentResult.Success;
                    result.operResult.message = HawkBilling.Success.SubscriptionCancellation;
                }
                else
                {
                    result.operResult.result = PaymentResult.Error;
                    result.operResult.message = HawkBilling.Error.Unknown;
                }
            }
            catch (Exception ex)
            {
                result.operResult.result = PaymentResult.Error;
                result.operResult.message = ex.Message;
                return result;
            }
            return result;
        }

        #region Payment Status check methods

        public static bool IsCompleted(string paymentStatus)
        {
            return (paymentStatus == PAYMENT_COMPLETED);
        }
        #endregion
    }
}