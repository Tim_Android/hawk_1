﻿using HawkBilling.Core.Model;
using HawkBilling.Core.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HawkBilling.Core
{
    public interface IPaymentProcessor
    {
        Type.PaymentProcessors paymentProcessor { get; set; }

        CustomerDetails customerInfo { get; set; }
        PaymentDetails paymentInfo { get; set; }
        List<SubscriptionDetails> subscrInfo { get; set; }
        bool testMode { get; set; }

        void ProcessPayment(Dictionary<string,string> additionalData = null);

        SubscriptionDetails CancelSubscription(string subscriptionId);
    }

    /// <summary>
    /// Class for adding constant with session name
    /// </summary>
    public abstract class PaymentProcessor : IPaymentProcessor
    {
        public const string SESSION_NAME = "paymentProcessor";

        public Type.PaymentProcessors paymentProcessor { get; set; }

        public CustomerDetails customerInfo { get; set; }

        public PaymentDetails paymentInfo { get; set; }

        public List<SubscriptionDetails> subscrInfo { get; set; }

        public bool testMode { get; set; }

        public abstract void ProcessPayment(Dictionary<string, string> additionalData = null);

        public abstract SubscriptionDetails CancelSubscription(string subscriptionId);
    }

}