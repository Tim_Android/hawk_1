﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkBilling.Core.Type
{
    public enum PaymentProcessors
    {
        AuthorizeNet = 1,
        PayPal = 2,
        Promocode = 3
    }

    public enum PaymentResult
    {
        Success = 1,
        Error = 0
    }
}
