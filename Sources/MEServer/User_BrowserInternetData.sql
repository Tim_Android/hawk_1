﻿USE [HAWK]
GO

/****** Object:  Table [dbo].[BOOKMARKS]    Script Date: 1/13/2016 7:00:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BOOKMARKS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AGENT_ID] [int] NULL,
	[URL] [varchar](max) NULL,
	[TITLE] [nvarchar](1000) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** ADD_BOOKMARKS procedure ******/
create procedure ADD_BOOKMARKS
    @AGENT_ID int,
	@BOOKMARK_URL varchar(max),
	@BOOKMARK_TITLE nvarchar(1000)
with encryption
as
begin
    set nocount on;

    insert into BOOKMARKS(AGENT_ID, URL, TITLE)
    values (@AGENT_ID, @BOOKMARK_URL, @BOOKMARK_TITLE);
end

/****** Object:  Table [dbo].[HISTORY] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BROWSER_HISTORY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AGENT_ID] [int] NULL,
	[URL] [varchar](max) NULL,
	[TITLE] [nvarchar](1000) NULL,
	[VISITS] [int] NULL,
	[DATE] [DateTime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** ADD_HISTORY procedure ******/
create procedure ADD_BROWSER_HISTORY
    @AGENT_ID int,
	@URL varchar(max),
	@TITLE nvarchar(1000),
	@VISITS int,
	@DATE DateTime
with encryption
as
begin
    set nocount on;

    insert into BROWSER_HISTORY(AGENT_ID, URL, TITLE, VISITS, DATE)
    values (@AGENT_ID, @URL, @TITLE, @VISITS, @DATE);
end

grant execute on ADD_BOOKMARKS to HAWKWebSiteUser
go

grant execute on ADD_BOOKMARKS to HAWKWebServiceUser
go

grant execute on ADD_BROWSER_HISTORY to HAWKWebSiteUser
go

grant execute on ADD_BROWSER_HISTORY to HAWKWebServiceUser
go