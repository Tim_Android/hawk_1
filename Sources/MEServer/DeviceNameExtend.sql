﻿use HAWK;

--Add extenstion of DEVICE_NAME column type in "DEVICES" table

if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure GET_DEVICE_INFO

if exists (select 1
          from sysobjects
          where  id = object_id('SET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure SET_DEVICE_INFO
   
if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICES_PAGED')
          and type in ('P','PC'))
   drop procedure GET_DEVICES_PAGED
go

if exists(select 1 from systypes where name='DEV_NAME_TYPE_TEMP')
   drop type DEV_NAME_TYPE_TEMP

go

exec sp_rename DEV_NAME_TYPE, DEV_NAME_TYPE_TEMP

--CREATE NEW TYPE
if exists(select 1 from systypes where name='DEV_NAME_TYPE')
   drop type DEV_NAME_TYPE

create type DEV_NAME_TYPE
    from nchar(32)

go

ALTER table DEVICES alter column DEVICE_NAME DEV_NAME_TYPE

--delete old user-defined type
if exists(select 1 from systypes where name='DEV_NAME_TYPE_TEMP')
   drop type DEV_NAME_TYPE_TEMP


-- end Add extenstion of DEVICE_NAME column type in "DEVICES" table


-- Re-add stored procedures
if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure GET_DEVICE_INFO
go


create procedure GET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE output,
    @DEVICE_ID DEVICE_ID_TYPE output,
    @PHONE_NUMBER PHONE_TYPE output,
    @BRAND BRAND_TYPE output,
    @MODEL MODEL_TYPE output,
    @OS OS_TYPE output,
    @SDK SDK_TYPE output,
	@FIRMWARE_ID FIRMWARE_ID_TYPE output,
    @SVN_VERSION SVN_TYPE output,
    @TIME_ZONE TIME_ZONE_TYPE output,
    @DEVICE_NAME DEV_NAME_TYPE output,
    @NOTE NOTE_TYPE output,
    @LIC_ACT_DATE datetime output,
    @CAN_DEACTIVATE bit output,
    @LIC_ID int output,
    @OPER_SYS_TYPE int output
with encryption    
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin        
        select
            @DEVICE_NAME = isnull(D.DEVICE_NAME, D.AGENT_NAME),
            @AGENT_NAME = D.AGENT_NAME,
            @DEVICE_ID = isnull(D.DEVICE_ID, 'unknown'),
            @PHONE_NUMBER = isnull(D.PHONE_NUMBER, 'unknown'),
            @BRAND = isnull(D.BRAND, 'unknown'),
            @MODEL = isnull(D.MODEL, 'unknown'),
            @OS = isnull(D.OS, 'unknown'),
            @SDK = isnull(D.SDK, 'unknown'),
            @FIRMWARE_ID = isnull(D.FIRMWARE_ID, 'unknown'),
            @SVN_VERSION = isnull(D.SVN_VERSION, 'unknown'),
            @TIME_ZONE = isnull(D.TIME_ZONE, 'unknown'),
            @NOTE = isnull(D.NOTE, ''),
            @LIC_ACT_DATE = LIC_ACT_DATE,
            @LIC_ID = LIC_ID,
            @OPER_SYS_TYPE = OPER_SYS_TYPE 
        from DEVICES D
        where D.ID = @AGENT_ID
    end
    
    exec  IS_DEACTIVATION_ALLOWED @AGENT_ID, @CAN_DEACTIVATE output    
end
go

if exists (select 1
          from sysobjects
          where  id = object_id('SET_DEVICE_INFO')
          and type in ('P','PC'))
   drop procedure SET_DEVICE_INFO
go


create procedure SET_DEVICE_INFO
    @AGENT_ID int = null,
    @AGENT_NAME AGENT_NAME_TYPE = null,
    @DEVICE_NAME DEV_NAME_TYPE = null,
    @NOTE NOTE_TYPE = null,
    @PHONE_NUMBER PHONE_TYPE = null,
    @BRAND BRAND_TYPE  = null,
    @MODEL MODEL_TYPE = null,
    @OS OS_TYPE = null,
    @SDK SDK_TYPE = null,
	@FIRMWARE_ID FIRMWARE_ID_TYPE = null,
    @SVN_VERSION SVN_TYPE = NULL,
    @TIME_ZONE TIME_ZONE_TYPE = NULL,
    @TZ_OFFSET int = NULL
with encryption
as
begin
    set nocount on;

    if (@AGENT_ID is not null)
    begin
        declare @BRAND_OLD_VAL BRAND_TYPE;
        declare @MODEL_OLD_VAL MODEL_TYPE;
        
        select @BRAND_OLD_VAL = D.BRAND, @MODEL_OLD_VAL = D.MODEL
        from DEVICES D
        where D.ID = @AGENT_ID;
              
        update DEVICES
        set
            DEVICES.AGENT_NAME = isnull(@AGENT_NAME, DEVICES.AGENT_NAME),
            DEVICES.DEVICE_NAME = isnull(@DEVICE_NAME, DEVICES.DEVICE_NAME),
            DEVICES.NOTE = isnull(@NOTE, DEVICES.NOTE),
            DEVICES.PHONE_NUMBER = isnull(@PHONE_NUMBER, DEVICES.PHONE_NUMBER),
            DEVICES.BRAND = isnull(@BRAND, DEVICES.BRAND),
            DEVICES.MODEL = isnull(@MODEL, DEVICES.MODEL),
            DEVICES.OS = isnull(@OS, DEVICES.OS),
            DEVICES.SDK = isnull(@SDK, DEVICES.SDK),
            DEVICES.FIRMWARE_ID = isnull(@FIRMWARE_ID, DEVICES.FIRMWARE_ID),
            DEVICES.SVN_VERSION = isnull(@SVN_VERSION, DEVICES.SVN_VERSION),
            DEVICES.TIME_ZONE = isnull(@TIME_ZONE, DEVICES.TIME_ZONE),
            DEVICES.TZ_OFFSET = isnull(@TZ_OFFSET, DEVICES.TZ_OFFSET)
        where DEVICES.ID = @AGENT_ID
        
        if ((@BRAND_OLD_VAL is null) and (@MODEL_OLD_VAL is null))
        begin
            UPDATE DEVICES
            SET 
              DEVICES.DEVICE_NAME = isnull(cast((rtrim(DEVICES.BRAND) + ' ' + rtrim(DEVICES.MODEL)) as nchar(20)), DEVICES.AGENT_NAME)
            where (DEVICES.ID = @AGENT_ID) and (DEVICES.AGENT_NAME = DEVICES.DEVICE_NAME)
        end        
    end
end
go

if exists (select 1
          from sysobjects
          where  id = object_id('GET_DEVICES_PAGED')
          and type in ('P','PC'))
   drop procedure GET_DEVICES_PAGED
go


create procedure GET_DEVICES_PAGED
    @AGENT_ID int = null,
    @USER_ID INT = null,
    @IS_ACTIVED bit = null,
    @LAST_UPDATED_FROM datetime = null,
    @LAST_UPDATED_TO datetime = null,
    @DEVICE_ID DEVICE_ID_TYPE = null,
    @AGENT_NAME AGENT_NAME_TYPE = null,
    @PHONE_NUMBER PHONE_TYPE = null,
    @BRAND BRAND_TYPE = null,
    @MODEL MODEL_TYPE = null,
    @OS OS_TYPE = null,
    @SDK SDK_TYPE = null,
	@FIRMWARE_ID FIRMWARE_ID_TYPE = null,
    @SVN_VERSION SVN_TYPE = null,
    @TIME_ZONE TIME_ZONE_TYPE = null,
    @DEVICE_NAME DEV_NAME_TYPE = null,
    @NOTE NOTE_TYPE = null,
    @LIC_ACT_DATE_FROM datetime = null,
    @LIC_ACT_DATE_TO datetime = null,
    @LIC_ID int = null,
    @OPER_SYS_TYPE int = null,
    @RecordsPerPage INT = NULL, -- if null or 0 - all records
    @Page INT = NULL output, -- if null or 0 - set first page
    @TotalRecords int output, -- this is output parameter
    @TotalPages int output-- this is output parameter

with encryption    
as
begin
    set nocount on;

	declare @Start int
	declare @End int

    select @TotalRecords = COUNT(id)
    from DEVICES
    where (
        (@AGENT_ID is null or ID = @AGENT_ID) and
        (@USER_ID is null or USER_ID = @USER_ID) and
        (@IS_ACTIVED is null or IS_ACTIVED = @IS_ACTIVED) and
        (@LAST_UPDATED_FROM is null or LAST_UPDATED >= @LAST_UPDATED_FROM) and
        (@LAST_UPDATED_TO is null or LAST_UPDATED <= @LAST_UPDATED_TO) and
        (@DEVICE_ID is null or DEVICE_ID = @DEVICE_ID) and
        (@AGENT_NAME is null or AGENT_NAME = @AGENT_NAME) and
        (@PHONE_NUMBER is null or PHONE_NUMBER = @PHONE_NUMBER) and
        (@BRAND is null or BRAND = @BRAND) and
        (@MODEL is null or MODEL = @MODEL) and
        (@OS is null or OS = @OS) and
        (@SDK is null or SDK = @SDK) and
        (@FIRMWARE_ID is null or FIRMWARE_ID = @FIRMWARE_ID) and
        (@SVN_VERSION is null or SVN_VERSION = @SVN_VERSION) and
        (@TIME_ZONE is null or TIME_ZONE = @TIME_ZONE) and
        (@DEVICE_NAME is null or DEVICE_NAME = @DEVICE_NAME) and
        (@NOTE is null or NOTE = @NOTE) and
        (@LIC_ACT_DATE_FROM is null or LIC_ACT_DATE >= @LIC_ACT_DATE_FROM) and
        (@LIC_ACT_DATE_TO is null or LIC_ACT_DATE <= @LIC_ACT_DATE_TO) and
        (@OPER_SYS_TYPE is null or OPER_SYS_TYPE = @OPER_SYS_TYPE)
        ) 
	--DON`T FORGET MAKE CHANGES IN FILTER "WHERE" IN Cte
    
    EXEC PAGER @RecordsPerPage, @Page OUTPUT, @TotalRecords OUTPUT, @Start OUTPUT, @End OUTPUT,  @TotalPages OUTPUT
    
	;With Cte As
	(
        SELECT
            rn = ROW_NUMBER() Over(Order by (Select 1)),
            ID,
            USER_ID,
            AGENT_NAME,
            IS_ACTIVED,
            LAST_UPDATED,
            DEVICE_ID,
            PHONE_NUMBER,
            BRAND,
            MODEL,
            OS,
            SDK,
            FIRMWARE_ID,
            SVN_VERSION, 
            TIME_ZONE,
            DEVICE_NAME,
            NOTE, LIC_ID, 
            LIC_ACT_DATE,
            CREATION_DATE,
            OPER_SYS_TYPE         
        from DEVICES
        where (
                (@AGENT_ID is null or ID = @AGENT_ID) and
                (@USER_ID is null or USER_ID = @USER_ID) and
                (@IS_ACTIVED is null or IS_ACTIVED = @IS_ACTIVED) and
                (@LAST_UPDATED_FROM is null or LAST_UPDATED >= @LAST_UPDATED_FROM) and
                (@LAST_UPDATED_TO is null or LAST_UPDATED <= @LAST_UPDATED_TO) and
                (@DEVICE_ID is null or DEVICE_ID = @DEVICE_ID) and
                (@AGENT_NAME is null or AGENT_NAME = @AGENT_NAME) and
                (@PHONE_NUMBER is null or PHONE_NUMBER = @PHONE_NUMBER) and
                (@BRAND is null or BRAND = @BRAND) and
                (@MODEL is null or MODEL = @MODEL) and
                (@OS is null or OS = @OS) and
                (@SDK is null or SDK = @SDK) and
                (@FIRMWARE_ID is null or FIRMWARE_ID = @FIRMWARE_ID) and
                (@SVN_VERSION is null or SVN_VERSION = @SVN_VERSION) and
                (@TIME_ZONE is null or TIME_ZONE = @TIME_ZONE) and
                (@DEVICE_NAME is null or DEVICE_NAME = @DEVICE_NAME) and
                (@NOTE is null or NOTE = @NOTE) and
                (@LIC_ACT_DATE_FROM is null or LIC_ACT_DATE >= @LIC_ACT_DATE_FROM) and
                (@LIC_ACT_DATE_TO is null or LIC_ACT_DATE <= @LIC_ACT_DATE_TO) and
                (@OPER_SYS_TYPE is null or OPER_SYS_TYPE = @OPER_SYS_TYPE)
               ) 
     )
	SELECT 
            ID,
            USER_ID,
            AGENT_NAME,
            IS_ACTIVED,
            LAST_UPDATED,
            DEVICE_ID,
            PHONE_NUMBER,
            BRAND,
            MODEL,
            OS,
            SDK,
            FIRMWARE_ID,
            SVN_VERSION, 
            TIME_ZONE,
            DEVICE_NAME,
            NOTE, LIC_ID, 
            LIC_ACT_DATE,
            CREATION_DATE,
            OPER_SYS_TYPE
	FROM Cte
	WHERE rn between  @Start and @End
end
go


grant execute on GET_DEVICE_INFO to HAWKWebSiteUser
go

grant execute on SET_DEVICE_INFO to HAWKWebServiceUser
go

grant execute on SET_DEVICE_INFO to HAWKWebSiteUser
go

grant execute on GET_DEVICES_PAGED to HAWKWebServiceUser
go

grant execute on GET_DEVICES_PAGED to HAWKWebSiteUser
go