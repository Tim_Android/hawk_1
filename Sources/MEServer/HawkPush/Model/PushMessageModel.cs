﻿using HawkPush.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkPush.Model
{
    public class PushMessageModel
    {
        public PushType type { get; set; }
        public string additionalInfo { get; set; }

        public PushMessageModel(PushType type, string additionalInfo)
        {
            this.type = type;
            this.additionalInfo = additionalInfo;
        }
    }
}
