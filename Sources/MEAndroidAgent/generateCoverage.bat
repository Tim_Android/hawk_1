echo off 
if "%1" == "" goto :ERROR_ANROID_TOOLS_PATH
if "%2" == "" goto :ERROR_ANT_TOOL_PATH

set android_program=%1\android.bat
set ant_program=%2\ant.bat

call "%android_program%" update project -p .
cd tests
call "%android_program%" update test-project -m "F:\HAWK\BrowserHistoryBranch" -p .
call "%ant_program%" emma debug install test

bin\coverage.html

:ERROR_ANT_TOOL_PATH
echo set path to and tool. For example F:\adt-eclipse\adt-bundle-windows-x86_64-20140702\sdk\tools\
goto :END

:ERROR_ANROID_TOOLS_PATH
echo set path to adnroid sdk tools. For example F:\apache-ant-1.9.6-bin\apache-ant-1.9.6\bin\
goto :END

:END