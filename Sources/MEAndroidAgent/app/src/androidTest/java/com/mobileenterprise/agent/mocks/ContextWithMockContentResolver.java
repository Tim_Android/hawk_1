package com.mobileenterprise.agent.mocks;

import android.content.ContentResolver;
import android.content.Context;
import android.test.RenamingDelegatingContext;

public class ContextWithMockContentResolver extends RenamingDelegatingContext 
{
    private ContentResolver contentResolver;

    public void setContentResolver(ContentResolver contentResolver)
    {
        this.contentResolver = contentResolver;
    }

    public ContextWithMockContentResolver(Context targetContext)
    {
        super(targetContext, "test");
    }

    @Override 
    public ContentResolver getContentResolver()
    { 
        return contentResolver;
    }

    @Override
    public Context getApplicationContext()
    {
        return this;
    }
}
