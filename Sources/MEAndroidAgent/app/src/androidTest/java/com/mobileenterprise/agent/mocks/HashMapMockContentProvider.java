package com.mobileenterprise.agent.mocks;

import java.util.HashMap;

import android.database.Cursor;
import android.net.Uri;
import android.test.mock.MockContentProvider;

public class HashMapMockContentProvider extends MockContentProvider
{
    private HashMap<Uri, Cursor> expectedResults = new HashMap<Uri, Cursor>();

    public void addQueryResult(Uri uriIn, Cursor expectedResult)
    {
        expectedResults.put(uriIn, expectedResult);
    }

    @Override 
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        return expectedResults.get(uri);
    }
} 
