package com.mobileenterprise.agent.test;

import java.util.Date;

import com.mobileenterprise.agent.helper.MessageInformation;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

public class MessageInformationTest extends AndroidTestCase
{
    public void testNeedSend_Read_RecieveCheck()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x87);

        assertEquals(false, mms.needSend());
    }

    public void testNeedSend_NOTIFICATION()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x82);

        assertEquals(false, mms.needSend());
    }

    public void testNeedSend_Another()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x80);

        assertEquals(true, mms.needSend());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        localContext = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        this.mContext = null;
    }   

    public void testPreConditions() {
        assertNotNull(this.mContext);
    }

    private static final String TEST_FILE_PREFIX = "test_";
    private RenamingDelegatingContext localContext;
}
