package com.mobileenterprise.agent.test;

import java.util.ArrayList;

import android.content.Context;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.test.AndroidTestCase;
import android.test.mock.MockContentResolver;

import com.mobileenterprise.agent.helper.MessageUtils;
import com.mobileenterprise.agent.mocks.ContextWithMockContentResolver;
import com.mobileenterprise.agent.mocks.HashMapMockContentProvider;

import junit.framework.Assert;

public class MessagesUtilsTest extends AndroidTestCase{

    public void testFindContactName_byNumber()
    {
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{"Alexandr"});

        Uri rightPhoneUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, "80938245234");
        Context mockContext = InitMockContext(rows, rightPhoneUri , "com.android.contacts");

        Assert.assertEquals("Alexandr", MessageUtils.findContactNameByAddress("80938245234", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("245234523", mockContext));
    }

    public void testFindContactName_byEmail() {
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{"Alexandr"});

        Uri rightPhoneUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, "test.email1test.com");
        Context mockContext = InitMockContext(rows, rightPhoneUri , "com.android.contacts");

        Assert.assertNull(MessageUtils.findContactNameByAddress("345234534", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("email.test@test.com", mockContext));
        Assert.assertEquals("Alexandr", MessageUtils.findContactNameByAddress("test.email1test.com", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("245234523", mockContext));
    }

    public void testFindContactName_NegativeCase() {
        Uri rightPhoneUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, "test");
        Context mockContext = InitMockContext(new ArrayList<Object[]>(), rightPhoneUri , "com.android.contacts");

        Assert.assertNull(MessageUtils.findContactNameByAddress("email.test@test.com", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("345234534", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("test.email@test.com", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress("245234523", mockContext));

        Assert.assertNull(MessageUtils.findContactNameByAddress("", mockContext));
        Assert.assertNull(MessageUtils.findContactNameByAddress(null, mockContext));
        try {
            MessageUtils.findContactNameByAddress("", null);
            Assert.fail("The code need to throw exception because context is null");
        }
        catch (Exception ex) {
            //ok
        }
    }


    private Context InitMockContext(ArrayList<Object[]> params, Uri uri, String providerName) {
        MatrixCursor mockCursor = new MatrixCursor(new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME});

        for (Object[] row : params) {
            mockCursor.addRow(row);
        }

        HashMapMockContentProvider mockProvider = new HashMapMockContentProvider();
        mockProvider.addQueryResult(uri, mockCursor);

        MockContentResolver mockResolver = new MockContentResolver();
        mockResolver.addProvider(providerName, mockProvider);

        ContextWithMockContentResolver mockContext = new ContextWithMockContentResolver(this.getContext());
        mockContext.setContentResolver(mockResolver);

        return mockContext;
    }
}
