package com.mobileenterprise.agent.test;

import junit.framework.Assert;
import com.mobileenterprise.agent.helper.BookmarkInformation;
import com.mobileenterprise.agent.helper.DuplicatedSiteInformation;
import com.mobileenterprise.agent.helper.SiteInformation;
import com.mobileenterprise.agent.helper.Settings.BrowserType;

import android.test.AndroidTestCase;

public class BookmarkInformationTest extends AndroidTestCase 
{
    public void testBookmarkInfo()
    {
    	BookmarkInformation bookmark = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search");
    	
    	Assert.assertEquals(bookmark.getUrl(), "https://www.google.com.ua/#q=test+search");
    	Assert.assertEquals(bookmark.getTitle(), "test search");
    }
    
    public void testBookmarkInfo_NullData()
    {
    	String NotInicializedString = null;
    	BookmarkInformation bookmark1 = new BookmarkInformation(NotInicializedString, "test title");

    	Assert.assertEquals(bookmark1.getUrl(), "");
    	Assert.assertEquals(bookmark1.getTitle(), "test title");
    	
    	BookmarkInformation bookmark2 = new BookmarkInformation("test url", NotInicializedString);

    	Assert.assertEquals(bookmark2.getUrl(), "test url");
    	Assert.assertEquals(bookmark2.getTitle(), "");
    }

    public void testBookmarkInfoEquels()
    {
    	{
    		BookmarkInformation bookmark1 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search");
    		BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search");
    		
    		assertTrue(bookmark1.equals(bookmark2));
    	}
    	
    	{
    		BookmarkInformation bookmark1 = new BookmarkInformation("https://www.google.com.ua/", "test search");
    		BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search");
    		
    		assertFalse(bookmark1.equals(bookmark2));
    	}
    	
    	{
    		BookmarkInformation bookmark1 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search1");
    		BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search");
    		
    		assertFalse(bookmark1.equals(bookmark2));
    	}
    	
    	{
    		BookmarkInformation bookmark1 = new BookmarkInformation("https://www.google.com.ua/#q=test+search", "test search1");
    		
    		assertFalse(bookmark1.equals(this));
    	}
    }

    public void testSiteInformation()
	{
		String url = "test_url";
		String title = "test_title";
		int visits = 2;
		Long date = Long.parseLong("1442916876738");
		
		SiteInformation site = new SiteInformation(url, title, date, visits);
		
		Assert.assertEquals(url, site.getUrl());
		Assert.assertEquals(title, site.getTitle());
		Assert.assertEquals(visits, site.getVisits());
		Assert.assertTrue(date.equals(site.getDate()));
		
		Long newDate = Long.parseLong("14429111");
		site.setDate(newDate);
		Assert.assertTrue(newDate.equals(site.getDate()));

		Assert.assertTrue(site.dateIsValid());
		site.setDateValidState(false);
		Assert.assertFalse(site.dateIsValid());
		
		BookmarkInformation bookmark = new SiteInformation(url, title, date, visits);
		Assert.assertEquals(url, bookmark.getUrl());
		Assert.assertEquals(title, bookmark.getTitle());
		
		DuplicatedSiteInformation duplicatedSite = new DuplicatedSiteInformation(1, date, visits, BrowserType.ChromeBrowser);
		
		Assert.assertEquals(1, duplicatedSite.getSiteID());
		Assert.assertEquals(visits, duplicatedSite.getVisits());
		Assert.assertTrue(date.equals(duplicatedSite.getDate()));

		duplicatedSite.setDate(newDate);
		duplicatedSite.setSiteID(50);
		
		Assert.assertEquals(50, duplicatedSite.getSiteID());
		Assert.assertTrue(newDate.equals(duplicatedSite.getDate()));
	}
}
