package com.mobileenterprise.agent.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.TimeZone;

import junit.framework.Assert;

import com.mobileenterprise.agent.R;
import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.MMSAttachment;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

public class CommonTest extends AndroidTestCase 
{
    public void testTimeZone()
    {
        String timeZoneName = "America/Los_Angeles";
        String timeZoneDisplayName = "PST, Pacific Standard Time, America/Los_Angeles";
        int rawOffset = -28800000; 
        
        TimeZone.setDefault(TimeZone.getTimeZone(timeZoneName));
        
        int rawOffsetResult = Common.GetTimeZoneRawOffset();
        String displayNameResult = Common.GetTimeZoneDisplayName();

        assertEquals(rawOffset, rawOffsetResult);
        assertEquals(displayNameResult, timeZoneDisplayName);        
    }
    
    public void testPrintDeviceInfo()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());
        Common.printDeviceInfo(stringBuilder);
        assertEquals(true, stringBuilder.length() > 0);
    }

    public void testPrintDeviceInfoNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Common.printDeviceInfo(stringBuilder);
        assertNull(stringBuilder);
    }

    @SuppressWarnings("null")
    public void testPrintStack()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {

            e = ex;
        }

        assertNotNull(e);

        Common.printStack(stringBuilder, e);

        assertEquals(true, stringBuilder.length() > 0);
    }

    public void testPrintStackNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Throwable e = null;

        Common.printStack(stringBuilder, e);

        assertNull(stringBuilder);
        assertNull(e);
    }

    @SuppressWarnings("null")
    public void testPrintCauseWithoutCause()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {

            e = ex;
        }

        assertNotNull(e);

        Common.printCause(stringBuilder, e);

        assertEquals(0, stringBuilder.length());
    }

    @SuppressWarnings("null")
    public void testPrintCause()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {


            try {

                testNullException.getDay();

            } 
            catch (Exception exNew) {
                e = exNew;
                e.initCause(ex);				 
            }

        }

        assertNotNull(e);

        Common.printCause(stringBuilder, e);

        assertEquals(true, stringBuilder.length() > 0);
    }

    public void testPrintCausekNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Throwable e = null;

        Common.printStack(stringBuilder, e);

        assertNull(stringBuilder);
        assertNull(e);
    }

    public void testReadBytes() throws IOException
    {
        InputStream in = null;
        byte[] image = {};
        final int imageSize = 153237;
        try
        {

            in = getContext().getResources().openRawResource(R.raw.audi_small);
            image = Common.readBytes(in);

            assertEquals(imageSize, image.length);
        }
        finally
        {
            if(in != null)
            {
                in.close();
            }
        }
    }

    public void testReadBytesEmpty()
    {
        InputStream in = null;
        byte[] image = {};
        final int imageSize = 0;

        try
        {
            in = getContext().getResources().openRawResource(R.raw.empty);
            image = Common.readBytes(in);

            assertEquals(imageSize, image.length);
        } 
        catch (IOException e) 
        {
            Assert.fail("Shouldn't through an exception!");
        }
        finally
        {
            if(in != null)
            {
                try {
                    in.close();
                } catch (IOException e) 
                {
                    Assert.fail("Shouldn't through an exception!");
                }
            }
        }
    }

    public void testReadBytesBad() throws IOException
    {
        InputStream in = null;
        byte[] image = {};

        try 
        {
            image = Common.readBytes(in);
            Assert.fail("Should throw an exception!");
        } 
        catch (NullPointerException ex) 
        {
            // success
        }
    }

    public void testGetDeviceID()
    {
        String deviceID = Common.getDeviceID(localContext);
        assertNotNull(deviceID);
        assertEquals(true, deviceID.length() > 0);
    }
    
    public void testGetPhoneNumber()
    {
        String phoneNumber = Common.getPhoneNumber(localContext);
        assertNotNull(phoneNumber);
        assertEquals(true, phoneNumber.length() > 0);
    }

    public void testRemoveLastComma()
    {
        String numberList = "324324, 423423, ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, "324324, 423423");
    }
    
    public void testRemoveLastCommaWithoutModification()
    {
        String numberList = "324324, 423423";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }
    
    public void testRemoveLastComma_Empty()
    {
        String numberList = "";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }
    
    public void testRemoveLastComma_WithoutComma()
    {
        String numberList = "324324423423 ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }
    
    public void testRemoveLastComma_LenthEqualPattern()
    {
        String numberList = ", ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, "");
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        localContext = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);

    }

    private static final String TEST_FILE_PREFIX = "test_";
    private RenamingDelegatingContext localContext;
}
