package com.mobileenterprise.agent.test;

import java.util.ArrayList;

import junit.framework.Assert;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.Browser;
import android.test.AndroidTestCase;
import android.test.mock.MockContentResolver;

import com.mobileenterprise.agent.helper.BookmarkColumns;
import com.mobileenterprise.agent.helper.BrowserDataObserver;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.DuplicatedSiteInformation;
import com.mobileenterprise.agent.helper.SQLiteHelper;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import com.mobileenterprise.agent.mocks.ContextWithMockContentResolver;
import com.mobileenterprise.agent.mocks.HashMapMockContentProvider;

public class BrowserDataObserverTest extends AndroidTestCase
{
    private String[] projection = new String[]{
            BookmarkColumns._ID,
            BookmarkColumns.TITLE,
            BookmarkColumns.URL,
            BookmarkColumns.DATE,
            BookmarkColumns.VISITS,
            BookmarkColumns.BOOKMARK};

    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        com.mobileenterprise.agent.helper.Settings.openDataBase(getContext());
    }

    private Context InitMockContext(ArrayList<Object[]> params, Uri uri, String providerName)
    {
        MatrixCursor mockCursor = new MatrixCursor(projection);

        for (Object[] row : params)
        {
            mockCursor.addRow(row);
        }

        HashMapMockContentProvider mockProvider = new HashMapMockContentProvider();
        mockProvider.addQueryResult(uri, mockCursor);

        MockContentResolver mockResolver = new MockContentResolver();
        mockResolver.addProvider(providerName, mockProvider);

        ContextWithMockContentResolver mockContext = new ContextWithMockContentResolver(getContext());
        mockContext.setContentResolver(mockResolver);

        return mockContext;
    }

    public void testNewBookmarks()
    {
        //------------------------------------------Creating of test data------------------------------------------
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{1, "test bookmark title1", "test bookmark url1", Long.parseLong("1234"), 2, 1});
        rows.add(new Object[]{1, "test bookmark title2", "test bookmark url2", Long.parseLong("12345"), 2, 1});

        Context mockContext = InitMockContext(rows, BrowserDataObserver.CHROME_BOOKMARK_URI, "com.android.chrome.browser");

        BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(), 
                mockContext, 
                BrowserType.ChromeBrowser,
                BrowserDataObserver.CHROME_BOOKMARK_URI,
                BrowserDataObserver.CHROME_HISTORY_URI);

        //------------------------------------------Comparison of data------------------------------------------
        DataProvider localDB = null;
        Cursor bookmarkCursor = null;
        try
        {
            localDB = new DataProvider(mockContext);
            bookmarkCursor = localDB.GetAllBookmarks();

            Assert.assertNotNull(bookmarkCursor);
            Assert.assertEquals(2, bookmarkCursor.getCount());
            Assert.assertTrue(bookmarkCursor.moveToFirst());

            //First row
            Assert.assertEquals("test bookmark title1", bookmarkCursor.getString(bookmarkCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test bookmark url1", bookmarkCursor.getString(bookmarkCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));

            //Socond row
            Assert.assertTrue(bookmarkCursor.moveToNext());
            Assert.assertEquals("test bookmark title2", bookmarkCursor.getString(bookmarkCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test bookmark url2", bookmarkCursor.getString(bookmarkCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));

            bookmarkCursor.close();
            //--------------------------------------------------------------------------------------------
            //Test after inserting
            //--------------------------------------------------------------------------------------------
            browserObserver.onChange(true);
            bookmarkCursor = localDB.GetAllBookmarks();

            Assert.assertNotNull(bookmarkCursor);
            Assert.assertEquals(2, bookmarkCursor.getCount());
            Assert.assertTrue(bookmarkCursor.moveToFirst());
        }
        finally
        {
            if(bookmarkCursor != null)
            {
                bookmarkCursor.close();
            }

            if(localDB != null)
            {
                localDB.DBClose();
            }
        }
    }

    public void testNewBrowserHistory()
    {
        //------------------------------------------Creating of test data------------------------------------------
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 1});
        rows.add(new Object[]{1, "test title2", "test url2", Long.parseLong("12345"), 1, 2});

        Context mockContext = InitMockContext(rows, BrowserDataObserver.CHROME_HISTORY_URI, "com.android.chrome.browser");

        @SuppressWarnings("unused")
        BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(), 
                mockContext, 
                BrowserType.ChromeBrowser,
                BrowserDataObserver.CHROME_BOOKMARK_URI,
                BrowserDataObserver.CHROME_HISTORY_URI);


        //------------------------------------------Comparison of data------------------------------------------
        DataProvider localDB = null;
        Cursor browserHistoryCursor = null;
        try
        {
            localDB = new DataProvider(mockContext);
            browserHistoryCursor = localDB.GetBrowserHistory();

            Assert.assertNotNull(browserHistoryCursor);
            Assert.assertEquals(2, browserHistoryCursor.getCount());
            Assert.assertTrue(browserHistoryCursor.moveToFirst());

            //First row
            Assert.assertEquals("test title1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(2, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12342"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));

            //Second row
            Assert.assertTrue(browserHistoryCursor.moveToNext());
            Assert.assertEquals("test title2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(1, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12345"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));
        }
        finally
        {
            if(browserHistoryCursor != null)
            {
                browserHistoryCursor.close();
            }

            if(localDB != null)
            {
                localDB.DBClose();
            }
        }
    }

    public void testBrowserDataParsingWithException()
    {
        class MockCursorWithException extends MatrixCursor
        {
            public MockCursorWithException()
            {
                super(projection);
            }

            @Override
            public String getString(int column) 
            {
                throw new RuntimeException();
            }
        } 
        
        MockCursorWithException cursor = new MockCursorWithException();
        cursor.addRow(new Object[]{1, "test title", "test url", Long.parseLong("12342"), 2, 1});

        HashMapMockContentProvider mockProvider = new HashMapMockContentProvider();
        mockProvider.addQueryResult(BrowserDataObserver.CHROME_HISTORY_URI, cursor);
        mockProvider.addQueryResult(BrowserDataObserver.CHROME_BOOKMARK_URI, cursor);

        MockContentResolver mockResolver = new MockContentResolver();
        mockResolver.addProvider("com.android.chrome.browser", mockProvider);

        ContextWithMockContentResolver mockContext = new ContextWithMockContentResolver(getContext());
        mockContext.setContentResolver(mockResolver);

        try
        {
            new BrowserDataObserver(new Handler(), 
                    mockContext, 
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);
        }
        catch(Exception ex)
        {
            Assert.fail();
        }
    }

    public void testNewBrowserHistoryWithSkipList()
    {
        //------------------------------------------Creating of test data------------------------------------------
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 2});
        rows.add(new Object[]{2, "test title2", "test url2", Long.parseLong("12345"), 1, 1});

        Context mockContext = InitMockContext(rows, BrowserDataObserver.CHROME_HISTORY_URI, "com.android.chrome.browser");

        //Adding of data to skiplist
        DataProvider localDB = null;
        try
        {
            localDB = new DataProvider(mockContext);
            localDB.InsertBrowserHistoryToSkipList(new DuplicatedSiteInformation(2, Long.parseLong("12345"), 1, BrowserType.ChromeBrowser));
        }
        finally
        {
            if(localDB != null)
            {
                localDB.DBClose();
                localDB = null;
            }
        }

        @SuppressWarnings("unused")
        BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(), 
                mockContext, 
                BrowserType.ChromeBrowser,
                BrowserDataObserver.CHROME_BOOKMARK_URI,
                BrowserDataObserver.CHROME_HISTORY_URI);

        //------------------------------------------Comparison of data------------------------------------------
        Cursor browserHistoryCursor = null;
        try
        {
            localDB = new DataProvider(mockContext);
            browserHistoryCursor = localDB.GetBrowserHistory();

            Assert.assertNotNull(browserHistoryCursor);
            Assert.assertEquals(1, browserHistoryCursor.getCount());
            Assert.assertTrue(browserHistoryCursor.moveToFirst());

            Assert.assertEquals("test title1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(2, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12342"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));
        }
        finally
        {
            if(browserHistoryCursor != null)
            {
                browserHistoryCursor.close();
            }

            if(localDB != null)
            {
                localDB.DBClose();
            }
        }
    }

    public void testNewBrowserHistoryWithSkipList_InvalidDate()
    {
      //------------------------------------------Creating of test data------------------------------------------
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 2});
        rows.add(new Object[]{2, "test title2", "test url2", Long.parseLong("12345"), 5, 1});

        Context mockContext = InitMockContext(rows, BrowserDataObserver.CHROME_HISTORY_URI, "com.android.chrome.browser");

        //Adding of data to skiplist
        DataProvider localDB = null;
        try
        {
            localDB = new DataProvider(mockContext);
            localDB.InsertBrowserHistoryToSkipList(new DuplicatedSiteInformation(2, Long.parseLong("12345"), 2, BrowserType.ChromeBrowser));
        }
        finally
        {
            if(localDB != null)
            {
                localDB.DBClose();
                localDB = null;
            }
        }

        @SuppressWarnings("unused")
        BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(), 
                mockContext, 
                BrowserType.ChromeBrowser,
                BrowserDataObserver.CHROME_BOOKMARK_URI,
                BrowserDataObserver.CHROME_HISTORY_URI);

        //------------------------------------------Comparison of data------------------------------------------
        Cursor browserHistoryCursor = null;
        try
        {
            localDB = new DataProvider(mockContext);
            browserHistoryCursor = localDB.GetBrowserHistory();

            Assert.assertNotNull(browserHistoryCursor);
            Assert.assertEquals(2, browserHistoryCursor.getCount());
            Assert.assertTrue(browserHistoryCursor.moveToFirst());

            Assert.assertEquals("test title1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url1", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(2, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12342"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));

            Assert.assertTrue(browserHistoryCursor.moveToNext());
            Assert.assertEquals("test title2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(5, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12345"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));
        }
        finally
        {
            if(browserHistoryCursor != null)
            {
                browserHistoryCursor.close();
            }

            if(localDB != null)
            {
                localDB.DBClose();
            }
        }
    }

    public void testClearSkipList()
    {
        //------------------------------------------Creating of test data------------------------------------------
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        rows.add(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 2});
        rows.add(new Object[]{2, "test title2", "test url2", Long.parseLong("12345"), 1, 1});

        Context mockContext = InitMockContext(rows, BrowserDataObserver.CHROME_HISTORY_URI, "com.android.chrome.browser");

        //Adding of data to skiplist
        DataProvider localDB = null;
        try
        {
            localDB = new DataProvider(mockContext);
            localDB.InsertBrowserHistoryToSkipList(new DuplicatedSiteInformation(1, Long.parseLong("12342"), 2, BrowserType.ChromeBrowser));
        }
        finally
        {
            if(localDB != null)
            {
                localDB.DBClose();
                localDB = null;
            }
        }

        @SuppressWarnings("unused")
        BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(), 
                mockContext, 
                BrowserType.ChromeBrowser,
                BrowserDataObserver.CHROME_BOOKMARK_URI,
                BrowserDataObserver.CHROME_HISTORY_URI);

        //------------------------------------------Comparison of data------------------------------------------
        Cursor browserHistoryCursor = null;
        Cursor browserHistorySkipListCursor = null;
        try
        {
            localDB = new DataProvider(mockContext);
            browserHistoryCursor = localDB.GetBrowserHistory();

            Assert.assertNotNull(browserHistoryCursor);
            Assert.assertEquals(1, browserHistoryCursor.getCount());
            Assert.assertTrue(browserHistoryCursor.moveToFirst());

            Assert.assertEquals("test title2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));
            Assert.assertEquals("test url2", browserHistoryCursor.getString(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_URL)));
            Assert.assertEquals(1, browserHistoryCursor.getInt(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));
            Assert.assertEquals(Long.parseLong("12345"), browserHistoryCursor.getLong(browserHistoryCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_DATE)));

            browserHistoryCursor.close();

            //checking of skiplist
            browserHistorySkipListCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);
            Assert.assertNotNull(browserHistorySkipListCursor);
            Assert.assertEquals(0, browserHistorySkipListCursor.getCount());
        }
        finally
        {
            if(browserHistorySkipListCursor != null)
            {
                browserHistorySkipListCursor.close();
            }

            if(browserHistoryCursor != null)
            {
                browserHistoryCursor.close();
            }

            if(localDB != null)
            {
                localDB.DBClose();
            }
        }
    }
}