package com.mobileenterprise.agent.test;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.mobileenterprise.agent.R;
import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.MMSAttachment;
import com.mobileenterprise.agent.helper.MessageInformation;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.TransferEngine;

import android.location.Location;
import android.location.LocationManager;
import android.test.AndroidTestCase;
import android.test.MoreAsserts;
import android.test.RenamingDelegatingContext;

public class TransferEngineTest extends AndroidTestCase 
{
	public void testPreConditions() {
		assertNotNull(localDB);
		assertNotNull(localContext);
	}
	
	public void testSendMultiPartData() throws IOException
	{
		this.fillDatabase();
		TransferEngine transfer = new TransferEngine(localContext);
		transfer.ResendAllData();
	}
	
	public void testSendGPSData()
	{
		this.fillGPSCoordinates();
		TransferEngine transfer = new TransferEngine(localContext);
		transfer.ResendAllData();
	}
	
	public void testSendCrashLogsData()
	{
		this.fillDatabaseWithFakeCrashLogs();
		TransferEngine transfer = new TransferEngine(localContext);
		transfer.ResendAllData();
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		localContext = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);

		localDB = new DataProvider(localContext);
		
		Settings.openDataBase(localContext);

	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

		localDB.DBClose();
		localDB = null;
	}
	
	private void fillDatabase() throws IOException
	{
		{
			Date date = new Date();
			ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
			String imageType = "image/jpeg";
			InputStream in = null;
			byte[] image = {};
			try
			{
				in = localContext.getResources().openRawResource(R.raw.audi_small);
				image = Common.readBytes(in);
				attachments.add(new MMSAttachment(image, imageType));
				attachments.add(new MMSAttachment(image, imageType));
				attachments.add(new MMSAttachment(image, imageType));
			}
			finally
			{
				if(in != null)
				{
					in.close();
				}
			}

	
			MessageInformation mms = new MessageInformation("+380502341115", 
					"���� ����", 
					"Inbox", 
					date.getTime(), 
					"���� ����", 
					attachments,
					MessageInformation.MESSAGE_TYPE_UNKNOWN);
			
			long id = localDB.InsertMMS(mms);
		}
		
		{
			Date date = new Date();
			MessageInformation sms = new MessageInformation("+380502341115", 
					"Test sms Body �������� �����", 
					"Inbox", 
					date.getTime(), 
					"", 
					null,
					MessageInformation.MESSAGE_TYPE_UNKNOWN);
			
			
			long id = localDB.InsertSMS(sms);
		}
	}
	
	private void fillGPSCoordinates()
	{
		Date date = new Date();
		
		Location location = new Location(LocationManager.NETWORK_PROVIDER);
		
		location.setAccuracy(10);
		location.setAltitude(10.4356);
		location.setLatitude(47.4356231312);
		location.setLongitude(54.2132434);
		location.setBearing(47.25f);
		location.setSpeed(40);
		location.setTime(date.getTime());
		
		{
			long id = localDB.InsertGPSLocation(location);

			MoreAsserts.assertNotEqual(-1, id);
			assertEquals(1, id);
		}
	}
	
	private void fillDatabaseWithFakeCrashLogs()
	{

		String crashReport = getTestCrashReport();
		int crashReportsCount = DataProvider.TRANSFER_MAX_SIZE / crashReport.length();
		for (int i = 0; i < crashReportsCount; i++) 
		{
			long id = localDB.InsertCrashLog(System.currentTimeMillis(), crashReport);
		}
	}
	
	@SuppressWarnings("null")
	private String getTestCrashReport()
	{
		Date testNullException = null;

		Throwable e = null;

		try {

			testNullException.getDate();

		} catch (Exception ex) {

			e = ex;
		}

		assertNotNull(e);

		final StringBuilder report = new StringBuilder();
		Date date = new Date();
		report.append( "Error Report collected on : " + date.toString() + "\n\n" );
		report.append( "Informations :\n" );


		Common.printDeviceInfo(report);

		report.append('\n').append('\n');

		Common.printStack( report, e );

		Common.printCause(report, e );
		
		return report.toString();
	}
	
	private static final String TEST_FILE_PREFIX = "test_";
	private DataProvider localDB;
	private RenamingDelegatingContext localContext;
}
