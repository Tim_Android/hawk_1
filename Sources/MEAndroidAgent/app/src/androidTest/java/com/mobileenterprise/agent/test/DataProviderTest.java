package com.mobileenterprise.agent.test;

import java.util.ArrayList;
import java.util.Date;
import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobileenterprise.agent.helper.BookmarkInformation;
import com.mobileenterprise.agent.helper.CallInformation;
import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.DuplicatedSiteInformation;
import com.mobileenterprise.agent.helper.MMSAttachment;
import com.mobileenterprise.agent.helper.MessageInformation;
import com.mobileenterprise.agent.helper.ResendStruct;
import com.mobileenterprise.agent.helper.SQLiteHelper;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import com.mobileenterprise.agent.helper.SiteInformation;

import android.R.integer;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Browser;
import android.test.AndroidTestCase;
import android.test.AssertionFailedError;
import android.test.RenamingDelegatingContext;
import android.test.MoreAsserts;

public class DataProviderTest extends AndroidTestCase 
{
    public void testPreConditions() {
        assertNotNull(localDB);
        assertNotNull(localContext);
    }

    public void testFakeBinary()
    {
        localDB.SendFakeBinary(localContext);
    }

    public void testGPSLogs() throws JSONException
    {
        Date date = new Date();

        Location location = new Location(LocationManager.NETWORK_PROVIDER);

        location.setAccuracy(10f);
        location.setAltitude(10.4356);
        location.setLatitude(47.4356231312);
        location.setLongitude(54.2132434);
        location.setBearing(47.25f);
        location.setSpeed(40f);
        location.setTime(date.getTime());

        {
            long id = localDB.InsertGPSLocation(location);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        // Check ----------------------------- GetGPSLogsToSend------------------------------

        final String featureName = DataProvider.GPS_LOG;
        ArrayList<ResendStruct> gpsLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(gpsLogsList, featureName);

        assertEquals(1, gpsLogsList.size());
        ResendStruct gpsLogs = gpsLogsList.get(0);

        ArrayList<Integer> idsToRemove = gpsLogs.getIdsToRemove();

        assertEquals(1, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));

        JSONObject dataArray = gpsLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);
        assertEquals((double)location.getAccuracy(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_ACCURACY));
        assertEquals(location.getAltitude(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_ALTITUDE));
        assertEquals((double)location.getBearing(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_BEARING));
        assertEquals((double)location.getSpeed(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_SPEED));
        assertEquals(location.getLatitude(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_LATITUDE));
        assertEquals(location.getLongitude(),json_data.getDouble(SQLiteHelper.GPS_COLUMN_LONGITUDE));
        assertEquals(location.getProvider(),json_data.getString(SQLiteHelper.GPS_COLUMN_PROVIDER));
        assertEquals("/Date(" + Long.toString(location.getTime())+ ")/",json_data.getString(SQLiteHelper.GPS_COLUMN_TIME));

        // finish ----------------------------- GetGPSLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetGPSLogsCount());

        localDB.DeleteGPSLogs(idsToRemove);

        assertEquals(0, localDB.GetGPSLogsCount());
    }

    public void testDeleteAllGPSLogs()
    {
        Date date = new Date();

        Location location = new Location(LocationManager.NETWORK_PROVIDER);

        location.setAccuracy(10f);
        location.setAltitude(10.4356);
        location.setLatitude(47.4356231312);
        location.setLongitude(54.2132434);
        location.setBearing(47.25f);
        location.setSpeed(40f);
        location.setTime(date.getTime());

        {
            long id = localDB.InsertGPSLocation(location);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertGPSLocation(location);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        assertEquals(true, localDB.GetGPSLogsCount() > 0);

        localDB.DeleteAllGPSLogs();

        assertEquals(0, localDB.GetGPSLogsCount());
    }

    // No Data to Sent
    public void testGPSLogsEmpty()
    {	 
        final String featureName = DataProvider.GPS_LOG;
        ArrayList<ResendStruct> gpsLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(gpsLogsList, featureName);

        assertEquals(true, gpsLogsList.isEmpty());

    }

    public void testGPSLogsNothingToKill()
    {
        localDB.DeleteGPSLogs(new ArrayList<Integer>());
    }

    public void testGPSLogsBadCase()
    {
        try
        {
            Location location = null;
            localDB.InsertGPSLocation(location);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }

    }

    public void testCallLogs() throws JSONException 
    {

        int dur = 33;
        Date date = new Date();
        CallInformation callInfo = new CallInformation("+38050334565", 
            "������� �����", 
            "Mobile", 
            "Device", 
            "", 
            "", 
            "Incoming", 
            date.getTime(), 
            dur);
        {
            long id = localDB.InsertCallLog(callInfo);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertCallLog(callInfo);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        // Check ----------------------------- GetCallLogsToSend-----------------------------
        final String featureName = DataProvider.CALL_LOG;
        ArrayList<ResendStruct> callLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(callLogsList, featureName);

        assertEquals(1, callLogsList.size());
        ResendStruct callLogs = callLogsList.get(0);

        ArrayList<Integer> idsToRemove = callLogs.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = callLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);
        assertEquals(callInfo.getFromNumber(),json_data.getString("fromNumber"));
        assertEquals(callInfo.getFromNumberName(),json_data.getString("fromNumberName"));
        assertEquals(callInfo.getFromNumberType(),json_data.getString("fromNumberType"));
        assertEquals(callInfo.getToNumber(),json_data.getString("toNumber"));
        assertEquals(callInfo.getToNumberName(),json_data.getString("toNumberName"));
        assertEquals(callInfo.getToNumberType(),json_data.getString("toNumberType"));
        assertEquals(callInfo.getType(),json_data.getString("type"));
        assertEquals("/Date(" + Long.toString(callInfo.getDate())+ ")/",json_data.getString("date"));
        assertEquals(callInfo.getDuration(),json_data.getInt("duration"));

        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetCallLogsCount());

        localDB.DeleteCallLogs(idsToRemove);

        assertEquals(0, localDB.GetCallLogsCount());

    }

    public void testDeleteAllCallLogs()
    {
        int dur = 33;
        Date date = new Date();
        CallInformation callInfo = new CallInformation("+38050334565", 
            "������� �����", 
            "Mobile", 
            "Device", 
            "", 
            "", 
            "Incoming", 
            date.getTime(), 
            dur);
        {
            long id = localDB.InsertCallLog(callInfo);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertCallLog(callInfo);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        assertEquals(true, localDB.GetCallLogsCount() > 0);

        localDB.DeleteAllCallLogs();

        assertEquals(0, localDB.GetCallLogsCount());
    }

    // No Data to Sent
    public void testCallLogsEmpty()
    {	 
        final String featureName = DataProvider.CALL_LOG;
        ArrayList<ResendStruct> callLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(callLogsList, featureName);

        assertEquals(true, callLogsList.isEmpty());

    }

    public void testCallLogsNothingToKill()
    {
        localDB.DeleteCallLogs(new ArrayList<Integer>());
    }

    public void testCallLogsBadCase()
    {
        try
        {
            CallInformation callInfo = null;
            localDB.InsertCallLog(callInfo);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }

    }

    public void testCrashLogsLargeData() throws JSONException
    {
        long date = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder("/Date(");
        sb.append(Long.toString(date));
        sb.append(")/");

        String crashReport = getTestCrashReport();
        int crashReportsCount = DataProvider.TRANSFER_MAX_SIZE / crashReport.length();
        for (int i = 0; i < crashReportsCount; i++) 
        {
            long id = localDB.InsertCrashLog(date,crashReport);
        }

        // Check ----------------------------- GetCrashLogsToSend-----------------------------
        final String featureName = DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(2, crashLogsList.size());
        ResendStruct crashLogs = crashLogsList.get(0);

        ArrayList<Integer> idsToRemove = crashLogs.getIdsToRemove();

        assertEquals(67, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));		

        JSONObject dataArray = crashLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);
        assertEquals(sb.toString(),json_data.getString("date"));
        assertEquals(crashReport,json_data.getString("data"));


        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        int logsCount = 0;

        for (ResendStruct crashLog : crashLogsList) 
        {
            logsCount += crashLog.getIdsToRemove().size();
        }

        assertEquals(logsCount, localDB.GetCrashLogsCount());

        for (ResendStruct crashLog : crashLogsList) 
        {
            localDB.DeleteCrashLogs(crashLog.getIdsToRemove());
        }

        assertEquals(0, localDB.GetCrashLogsCount());

    }

    public void testCrashLogs() throws JSONException
    {
        String crashReport = getTestCrashReport();
        long date = System.currentTimeMillis();

        StringBuilder sb = new StringBuilder("/Date(");
        sb.append(Long.toString(date));
        sb.append(")/");

        long id = localDB.InsertCrashLog(date, crashReport);

        assertEquals(1, id);

        // Check ----------------------------- GetCrashLogsToSend-----------------------------
        final String featureName =DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(1, crashLogsList.size());
        ResendStruct crashLogs = crashLogsList.get(0);

        ArrayList<Integer> idsToRemove = crashLogs.getIdsToRemove();

        assertEquals(1, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));		

        JSONObject dataArray = crashLogs.getDataArray();


        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(sb.toString(),json_data.getString("date"));
        assertEquals(crashReport,json_data.getString("data"));


        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetCrashLogsCount());

        localDB.DeleteCrashLogs(idsToRemove);

        assertEquals(0, localDB.GetCrashLogsCount());

    }

    public void testDeleteAllCrashLogs()
    {

        String crashReport = getTestCrashReport();
        long id = localDB.InsertCrashLog(System.currentTimeMillis(), crashReport);

        assertEquals(1, id);

        assertEquals(true, localDB.GetCrashLogsCount() > 0);

        localDB.DeleteAllCrashLogs();

        assertEquals(0, localDB.GetCrashLogsCount());
    }

    // No Data to Sent
    public void testCrashLogsEmpty()
    {	 
        final String featureName = DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(true, crashLogsList.isEmpty());

    }

    public void testCrashLogsNothingToKill()
    {
        localDB.DeleteCrashLogs(new ArrayList<Integer>());
    }

    public void testCrashLogsBadCase()
    {
        long id = localDB.InsertCrashLog(0,null);
        assertEquals(-1, id);
    }

    public void testSMSLogs() throws JSONException
    {
        Date date = new Date();
        MessageInformation sms = new MessageInformation("+380502341115", 
            "Test sms Body", 
            "Inbox", 
            date.getTime(), 
            "", 
            null,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        sms.setContactName("contact name");

        {
            long id = localDB.InsertSMS(sms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertSMS(sms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        // Check ----------------------------- GetSMSLogsToSend-----------------------------

        final String featureName =DataProvider.SMS_LOG;
        ArrayList<ResendStruct> smsLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(smsLogsList, featureName);

        assertEquals(1, smsLogsList.size());
        ResendStruct smsLogs = smsLogsList.get(0);

        ArrayList<Integer> idsToRemove = smsLogs.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = smsLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(sms.getAddress(),          json_data.getString("address"));
        assertEquals(sms.getContactName(),      json_data.getString("contactName"));
        assertEquals(sms.getBody(),             json_data.getString("body"));
        assertEquals(sms.getType(),             json_data.getString("type"));
        assertEquals("/Date(" + Long.toString(sms.getDate())+ ")/", json_data.getString("date"));

        // Finish ----------------------------- GetSMSLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetSMSLogsCount());		

        localDB.DeleteSMSLogs(idsToRemove);

        assertEquals(0, localDB.GetSMSLogsCount());		

    }

    public void testDeleteAllSMSLogs()
    {
        Date date = new Date();
        MessageInformation sms = new MessageInformation("+380502341115", 
            "Test sms Body", 
            "Inbox", 
            date.getTime(), 
            "", 
            null,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        {
            long id = localDB.InsertSMS(sms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertSMS(sms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        assertEquals(true, localDB.GetSMSLogsCount() > 0);		

        localDB.DeleteAllSMSLogs();

        assertEquals(0, localDB.GetSMSLogsCount());
    }

    // No Data to Sent
    public void testSMSLogsEmpty()
    {	 
        final String featureName =DataProvider.SMS_LOG;
        ArrayList<ResendStruct> smsLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(smsLogsList, featureName);

        assertEquals(true, smsLogsList.isEmpty());

    }

    public void testSMSLogsNothingToKill()
    {
        localDB.DeleteSMSLogs(new ArrayList<Integer>());
    }

    public void testSMSLogsBadCase()
    {
        try
        {
            MessageInformation smsInfo = null;
            localDB.InsertSMS(smsInfo);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }

    }

    public void testMMSLogsLargeData() throws JSONException
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        int mmsSize = 0;
        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);

            ArrayList<ResendStruct> mmsLogsList1 = new ArrayList<ResendStruct>();
            localDB.GetMMSLogsToSend(mmsLogsList1);

            mmsSize = mmsLogsList1.get(0).getDataArray().toString().length() - 20;
        }

        int mmsCount = DataProvider.TRANSFER_MAX_SIZE / mmsSize;
        for (int i = 0; i < mmsCount; i++) 
        {
            long id = localDB.InsertMMS(mms);
        }		

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(2, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(725, idsToRemove.size());


        JSONObject dataArray = mmsLogs.getDataArray();

        final String featureName = DataProvider.MMS_LOG;
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(mms.getAddress(),json_data.getString(SQLiteHelper.MT_COLUMN_ADRESS));
        assertEquals(mms.getBody(),json_data.getString(SQLiteHelper.MT_COLUMN_BODY));
        assertEquals(mms.getType(),json_data.getString(SQLiteHelper.MT_COLUMN_TYPE));
        assertEquals(mms.getSubject(),json_data.getString(SQLiteHelper.MT_COLUMN_SUBJECT));
        assertEquals("/Date(" + Long.toString(mms.getDate())+ ")/",json_data.getString(SQLiteHelper.MT_COLUMN_DATE));

        JSONArray jsonAttachments = json_data.getJSONArray(SQLiteHelper.MT_COLUMN_ATTACH_LIST);
        assertEquals(1, jsonAttachments.length());

        JSONObject attach = jsonAttachments.getJSONObject(0);

        assertEquals(attach.getInt(SQLiteHelper.MT_COLUMN_ID),1);
        assertEquals(attach.getString(SQLiteHelper.MT_COLUMN_TYPE), imageType);		
        assertEquals(mmsCount + 1 , localDB.GetMMSLogsCount());

        for (ResendStruct mmsLogs1 : mmsLogsList) 
        {           
            localDB.DeleteMMSLogs(mmsLogs1.getIdsToRemove());       
        }

        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();

        MoreAsserts.assertEquals(fakeImage, sentAttachments.get(0).getData());


        // Finish ----------------------------- GetMMSLogsToSend-----------------------------


        assertEquals(mmsCount + 1, localDB.GetMMSAttachmentsCount());


        for (MMSAttachment attachment : localDB.getAttachmentsToSend())
        {                       
            localDB.DeleteAttachmentByID(attachment.getId());                        
        }   

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    public void testMMSLogs() throws JSONException
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        mms.setContactName("test contact name");

        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        MessageInformation mmsWithoutAttach = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            new ArrayList<MMSAttachment>(),
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        {
            long id = localDB.InsertMMS(mmsWithoutAttach);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(3, id);
        }

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(1, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(3, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));           

        JSONObject dataArray = mmsLogs.getDataArray();

        final String featureName = DataProvider.MMS_LOG;
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(mms.getAddress(),          json_data.getString("address"));
        assertEquals(mms.getContactName(),      json_data.getString("contactName"));
        assertEquals(mms.getBody(),             json_data.getString("body"));
        assertEquals(mms.getType(),             json_data.getString("type"));
        assertEquals(mms.getSubject(),          json_data.getString("sub"));
        assertEquals("/Date(" + Long.toString(mms.getDate())+ ")/", json_data.getString("date"));

        JSONArray jsonAttachments = json_data.getJSONArray("attachments");
        assertEquals(1, jsonAttachments.length());

        JSONObject attach = jsonAttachments.getJSONObject(0);

        assertEquals(attach.getInt(SQLiteHelper.MT_COLUMN_ID),1);
        assertEquals(attach.getString(SQLiteHelper.MT_COLUMN_TYPE), imageType);

        assertEquals(idsToRemove.size(), localDB.GetMMSLogsCount());
        localDB.DeleteMMSLogs(idsToRemove);


        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(2, sentAttachments.size());

        MoreAsserts.assertEquals(fakeImage, sentAttachments.get(0).getData());


        // Finish ----------------------------- GetMMSLogsToSend-----------------------------


        assertEquals(2, localDB.GetMMSAttachmentsCount());


        for (MMSAttachment attachment : sentAttachments)
        {
            localDB.DeleteAttachmentByID(attachment.getId());
        }

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    public void testAttachmentsToSend()
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(1, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(1, idsToRemove.size());

        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(0, sentAttachments.size()); //because nothing was sent.

        localDB.DeleteMMSLogs(idsToRemove); // after that we can sent all attachments.

        sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(1, sentAttachments.size()); //because nothing was sent.
    }

    public void testDeleteAllMMSLogs()
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        {
            long id = localDB.InsertMMS(mms);

            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        assertEquals(true, localDB.GetMMSLogsCount()>0);
        assertEquals(true, localDB.GetMMSAttachmentsCount()>0);

        localDB.DeleteAllMMSLogs();

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    // No Data to Sent
    public void testMMSLogsEmpty()
    {	 
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(true, mmsLogsList.isEmpty());
    }

    public void testMMSLogsNothingToKill()
    {
        localDB.DeleteMMSLogs(new ArrayList<Integer>());
    }

    public void testMMSLogsBadCase()
    {
        try
        {
            MessageInformation mmsInfo = null;
            localDB.InsertMMS(mmsInfo);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }

    }

    public void testMMSLogsBadCaseNoAttachments()
    {
        try
        {
            Date date = new Date();
            MessageInformation mms = new MessageInformation("+380502341115", 
                "Test Body", 
                "Inbox", 
                date.getTime(), 
                "test Subject", 
                null,
                MessageInformation.MESSAGE_TYPE_UNKNOWN);

            localDB.InsertMMS(mms);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }

    }

    // Test Browser Bookmarks
    public void testBookmarkLogsEmpty()
    {
        ArrayList<ResendStruct> bookmarksList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(bookmarksList, DataProvider.BROWSER_BOOKMARKS_LOG);
        assertEquals(true, bookmarksList.isEmpty());
    }
    
    public void testBookmarksLogs() throws JSONException 
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow.com");
        {
            long id = localDB.InsertBookmark(bookmark1);
            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        BookmarkInformation bookmark2 = new BookmarkInformation("https://login.yahoo.com/", "Yahoo - login");
        {
            long id = localDB.InsertBookmark(bookmark2);
            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        // Check -----------------------------GetLogsToSend-----------------------------
        ArrayList<ResendStruct> bookmarksLogs = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(bookmarksLogs, DataProvider.BROWSER_BOOKMARKS_LOG);

        assertEquals(1, bookmarksLogs.size());
        ResendStruct bookmakLog = bookmarksLogs.get(0);

        ArrayList<Integer> idsToRemove = bookmakLog.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = bookmakLog.getDataArray();
        JSONArray jsonArray = dataArray.getJSONArray(DataProvider.BROWSER_BOOKMARKS_LOG);

        JSONObject json_data1 = jsonArray.getJSONObject(0);
        assertEquals(bookmark1.getUrl(), json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(bookmark1.getTitle(), json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));

        JSONObject json_data2 = jsonArray.getJSONObject(1);
        assertEquals(bookmark2.getUrl(), json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(bookmark2.getTitle(), json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));

        // Finish ----------------------------- GetLogsToSend-----------------------------
        Cursor cur = localDB.GetNewBookmarks();
        int bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(idsToRemove.size(), bookmarksCount);
        localDB.MarkedBookmarksAsSent(idsToRemove);

        cur = localDB.GetNewBookmarks();
        bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(0, bookmarksCount);

        cur = localDB.GetAllBookmarks();
        bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(2, bookmarksCount);
    }

    public void testBookmarksGetting()
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow");
        BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/", "Google");

        ArrayList<Integer> ids = new ArrayList<Integer>();
        ids.add((int)localDB.InsertBookmark(bookmark1));
        ids.add((int)localDB.InsertBookmark(bookmark2));

        //Check count after simple inserting
        Cursor cursor = localDB.GetAllBookmarks();
        int allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        int newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(2, allBookmarkCount);
        assertEquals(2, newBookmarkCount);

        localDB.MarkedBookmarksAsSent(ids);

        //Check count after MarkedBookmarksAsSent
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(2, allBookmarkCount);
        assertEquals(0, newBookmarkCount);

        BookmarkInformation bookmark3 = new BookmarkInformation("https://login.yahoo.com", "Yahoo - login");
        localDB.InsertBookmark(bookmark3);

        //Check count after inserting bookmark into non empty table
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(3, allBookmarkCount);
        assertEquals(1, newBookmarkCount);
    }

    public void testBookmarksBadCase()
    {
        try
        {
            BookmarkInformation bookmark = null;
            localDB.InsertBookmark(bookmark);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }
    }

    public void testBookmarksNothingToKill()
    {
        localDB.MarkedBookmarksAsSent(new ArrayList<Integer>());
    }

    public void testDeleteAllBookmarks()
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow");
        BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/", "Google");

        localDB.InsertBookmark(bookmark1);
        localDB.InsertBookmark(bookmark2);

        Cursor cursor = localDB.GetAllBookmarks();
        int allBookmarkCount = cursor.getCount();
        cursor.close();
        assertEquals(2, allBookmarkCount);
        
        localDB.DeleteAllBookmarks();
        
        //Check count after deleting
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();
        assertEquals(0, allBookmarkCount);
    }
    // Test Browser History
    public void testBrowserHistoryLogsEmpty()
    {
        ArrayList<ResendStruct> browserHistoryList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(browserHistoryList, DataProvider.BROWSER_HISTORY_LOG);
        assertTrue(browserHistoryList.isEmpty());
    }

    public void testBrowserHistoryLogs() throws JSONException 
    {
        SiteInformation testSite1 = new SiteInformation("http://stackoverflow.com/", 
                "stackoverflow.com", 
                Long.parseLong("1442850898"),
                2);
        {
            long id = localDB.InsertBrowserHistory(testSite1);
            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(1, id);
        }

        SiteInformation testSite2 = new SiteInformation("https://login.yahoo.com/", 
                "Yahoo - login", 
                Long.parseLong("1442850895"),
                15);
        {
            long id = localDB.InsertBrowserHistory(testSite2);
            MoreAsserts.assertNotEqual(-1, id);
            assertEquals(2, id);
        }

        // Check -----------------------------GetLogsToSend-----------------------------
        final String featureName = DataProvider.BROWSER_HISTORY_LOG;
        ArrayList<ResendStruct> browserHistoryList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(browserHistoryList, featureName);

        assertEquals(1, browserHistoryList.size());
        ResendStruct historyLog = browserHistoryList.get(0);

        ArrayList<Integer> idsToRemove = historyLog.getIdsToRemove();
        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = historyLog.getDataArray();
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data1 = jsonArray.getJSONObject(0);
        assertEquals(testSite1.getUrl(),    json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(testSite1.getTitle(),  json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));
        assertEquals(testSite1.getVisits(), json_data1.getInt(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        assertEquals("/Date(" + testSite1.getDate() + ")/", json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_DATE));

        JSONObject json_data2 = jsonArray.getJSONObject(1);
        assertEquals(testSite2.getUrl(),    json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(testSite2.getTitle(),  json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));
        assertEquals(testSite2.getVisits(), json_data2.getInt(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        assertEquals("/Date(" + testSite2.getDate() + ")/", json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_DATE));

        // Finish ----------------------------- GetLogsToSend-----------------------------
        Cursor cur = localDB.GetBrowserHistory();
        int count = cur.getCount();
        cur.close();

        assertEquals(idsToRemove.size(), count);

        localDB.DeleteBrowserHistory(idsToRemove);

        cur = localDB.GetBrowserHistory();
        count = cur.getCount();
        cur.close();

        assertEquals(0, count);
    }

    public void testBrowserHistoryNothingToKill()
    {
        localDB.DeleteBrowserHistory(new ArrayList<Integer>());
        localDB.DeleteBrowserHistoryByID(0);
        localDB.DeleteBrowserHistoryByID(-1);
    }

    public void testBrowserHistoryLogsBadCase()
    {
        try
        {
            SiteInformation site = null;
            localDB.InsertBrowserHistory(site);

            Assert.fail("Should have thrown NullPointerException");
        }
        catch(NullPointerException e)
        {
            //success
        }
    }

    public void testBrowserHistoryWithDuplicates()
    {
        SiteInformation site1 = new SiteInformation("http://stackoverflow.com/some_site",
                "stackoverflow",
                Long.parseLong("1442912340"),
                1);

        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 1, BrowserType.ChromeBrowser);

        SiteInformation site3 = new SiteInformation("http://stackoverflow.com/",
                "stackoverflow.com.ua",
                Long.parseLong("1442912340"),
                1);

        localDB.InsertBrowserHistory(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistory(site3);


        //Check --------------------------GetBrowserHistory-----------------------------------------
        Cursor cursor = localDB.GetBrowserHistory();
        int count = cursor.getCount();
        cursor.close();

        Assert.assertEquals(2, count);
        //Finish -------------------------GetBrowserHistory-----------------------------------------

        //Check --------------------------FindDuplicatedBrowserHistory------------------------------

        //wrong id
        cursor = localDB.FindDuplicatedBrowserHistory(2, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();

        Assert.assertEquals(0, count);

        //wrong date
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912341"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);

        //wrong browser type
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.DefaultBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);

        //right data
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(1, count);

        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        cursor.moveToFirst();
        int visits = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        Long date = cursor.getLong(cursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_DATE));
        cursor.close();

        Assert.assertEquals(visits, site3.getVisits());
        Assert.assertEquals(date, site3.getDate());

        localDB.DeleteBrowserHistoryFromSkipListByID(1);

        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);
        //Finish --------------------------FindDuplicatedBrowserHistory------------------------------
    }

    public void testClearSkipListByDate()
    {
        DuplicatedSiteInformation site1 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 1, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(2, Long.parseLong("1442912343"), 1, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site3 = new DuplicatedSiteInformation(3, Long.parseLong("1442912346"), 1, BrowserType.ChromeBrowser);

        localDB.InsertBrowserHistoryToSkipList(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistoryToSkipList(site3);

        Cursor browserHistorySkiplistCursor = null;
        try
        {
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertTrue(browserHistorySkiplistCursor.moveToFirst());
            Assert.assertEquals(3, browserHistorySkiplistCursor.getCount());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;

            localDB.ClearSkipListByDate(Long.parseLong("1442912341"));
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertTrue(browserHistorySkiplistCursor.moveToFirst());
            Assert.assertEquals(2, browserHistorySkiplistCursor.getCount());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;

            localDB.ClearSkipListByDate(Long.parseLong("1442912348"));
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertEquals(0, browserHistorySkiplistCursor.getCount());
            Assert.assertFalse(browserHistorySkiplistCursor.moveToFirst());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;
        }
        finally
        {
            if(browserHistorySkiplistCursor != null)
            {
                browserHistorySkiplistCursor.close();
            }
        }
    }

    public void testDeleteAllBrowserHistory()
    {
        SiteInformation testSite1 = new SiteInformation("http://stackoverflow.com/", 
                "stackoverflow.com", 
                Long.parseLong("1442850898"),
                2);

        SiteInformation testSite2 = new SiteInformation("https://login.yahoo.com/", 
                "Yahoo - login", 
                Long.parseLong("1442850895"),
                15);
        localDB.InsertBrowserHistory(testSite1);
        localDB.InsertBrowserHistory(testSite2);

        Cursor cursor = localDB.GetBrowserHistory();
        int count = cursor.getCount();
        cursor.close();
        assertEquals(2, count);
        
        localDB.DeleteAllBrowserHistory();
        
        cursor = localDB.GetBrowserHistory();
        count = cursor.getCount();
        cursor.close();
        assertEquals(0, count);
    }

    public void testDeleteAllBrowserHistorySkipList()
    {
        DuplicatedSiteInformation site1 = new DuplicatedSiteInformation(1, Long.parseLong("1442916453"), 1, BrowserType.DefaultBrowser);
        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 2, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site3 = new DuplicatedSiteInformation(2, Long.parseLong("1222912341"), 5, BrowserType.ChromeBrowser);

        localDB.InsertBrowserHistoryToSkipList(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistoryToSkipList(site3);

        Cursor cursor = localDB.GetBrowserHistorySkipList(Long.parseLong("9999999999"));
        int count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(3, count);

        localDB.DeleteAllBrowserHistorySkipList();

        cursor = localDB.GetBrowserHistorySkipList(Long.parseLong("9999999999"));
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        localContext = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);

        localDB = new DataProvider(localContext);

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        localDB.DBClose();
        localDB = null;
    }

    @SuppressWarnings("null")
    private String getTestCrashReport()
    {
        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {

            e = ex;
        }

        assertNotNull(e);

        final StringBuilder report = new StringBuilder();
        Date date = new Date();
        report.append( "Error Report collected on : " + date.toString() + "\n\n" );
        report.append( "Informations :\n" );


        Common.printDeviceInfo(report);

        report.append('\n').append('\n');

        Common.printStack( report, e );

        Common.printCause(report, e );

        return report.toString();
    }

    private static final String TEST_FILE_PREFIX = "test_";
    private DataProvider localDB;
    private RenamingDelegatingContext localContext;
}
