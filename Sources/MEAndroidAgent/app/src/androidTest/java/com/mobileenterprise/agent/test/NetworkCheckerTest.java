package com.mobileenterprise.agent.test;

import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.NetworkChecker;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

public class NetworkCheckerTest extends AndroidTestCase {
	
	 private static final String TEST_FILE_PREFIX = "test_";
	
	 private RenamingDelegatingContext mContext = null;
	 
	 @Override
	 protected void setUp() throws Exception {
	     super.setUp();

	     this.mContext = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);     
	     
	 }

	 @Override
	 protected void tearDown() throws Exception {
	     super.tearDown();
	     
	     this.mContext = null;
	 }	 
	 
	 public void testPreConditions() {
	     assertNotNull(this.mContext);
	 }
	 
	 public void testIsNetworkAvailable()
	 {
		 NetworkChecker.IsNetworkAvailable(this.mContext);
	 }

}
