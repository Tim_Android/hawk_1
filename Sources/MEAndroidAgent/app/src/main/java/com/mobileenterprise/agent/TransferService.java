package com.mobileenterprise.agent;

import com.mobileenterprise.agent.helper.NetworkChecker;
import com.mobileenterprise.agent.helper.TransferEngine;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class TransferService extends Service
{
    
    @Override
    public void onCreate() 
    {
        super.onCreate();
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) 
    {
        Log.v(this.getClass().getName(), "onStartCommand BEGIN!");
        Toast.makeText(this, "It's Service Time!", Toast.LENGTH_LONG).show();
        Boolean isNetworkAvailable = NetworkChecker.IsNetworkAvailable(this);
        Toast.makeText(this, "Is Network available = " + isNetworkAvailable.toString(), Toast.LENGTH_LONG).show();
        if(isNetworkAvailable)
        {
            if(backgroundWorker == null)
            {
                backgroundWorker = new Thread(null, doBackgroundThreadProcessing,
                    this.getClass().getName() + ": Background");
                Log.v(this.getClass().getName(), "backgroundWorker created!");
            }

            if(!backgroundWorker.isAlive())
            {
                backgroundWorker.start();
                Log.v(this.getClass().getName(), "backgroundWorker started!");
            }
        }

        Log.v(this.getClass().getName(), "Timed alarm onReceive() started at time: " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
        Log.v(this.getClass().getName(), "onStartCommand END!");

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    private Runnable doBackgroundThreadProcessing = new Runnable() 
    {
        public void run() {
        backgroundThreadProcessing();
        }
    };
    
    private void backgroundThreadProcessing() 
    {
        TransferEngine transfer = new TransferEngine(this);
        transfer.ResendAllData();
        
        TransferService.this.stopSelf();
    }
    
    private Thread backgroundWorker = null;

}
