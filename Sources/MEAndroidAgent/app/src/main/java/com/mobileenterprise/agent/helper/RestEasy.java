package com.mobileenterprise.agent.helper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobileenterprise.agent.R;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class RestEasy {
	 
    
    // Post JSON to the resteasy web service
    public static HttpResponse doPost(String url, JSONObject c, Context paramContext) throws ClientProtocolException, IOException 
    {
        HttpClient httpclient = RestEasy.getClient(paramContext);
        HttpPost request = new HttpPost(url);
        
        String entity = c.toString();
        StringEntity s = new StringEntity(entity,"utf-8");
        s.setContentType("application/json; charset=utf-8");
        s.setContentEncoding("utf-8");        

        request.setEntity(s);
        request.addHeader("accept", "application/json");
        
        return httpclient.execute(request);
    }
    
    public static HttpResponse doBinaryPostFake(String url, String fileName, Context paramContext) throws ClientProtocolException, IOException 
    {
    	
    	//File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
    	File file = new File(Environment.getExternalStorageDirectory(), fileName);
    	    	
    	HttpClient httpclient = RestEasy.getClient(paramContext);
        HttpPost httppost = new HttpPost(url);
        InputStreamEntity reqEntity = new InputStreamEntity(
                new FileInputStream(file), -1);
        reqEntity.setContentType("binary/octet-stream");
        //reqEntity.setContentType("application/octet-stream");
        
        reqEntity.setChunked(true); // Send in multiple parts if needed
        httppost.setEntity(reqEntity);        
        
        return httpclient.execute(httppost);
    }
    
    public static HttpResponse doBinaryPost(String url, MMSAttachment attach, Context paramContext) throws ClientProtocolException, IOException 
    {	
    	HttpClient httpclient = RestEasy.getClient(paramContext);
        HttpPost httppost = new HttpPost(url);
        InputStreamEntity reqEntity = new InputStreamEntity(
        		new ByteArrayInputStream(attach.getData()), attach.getData().length);
        reqEntity.setContentType("binary/octet-stream");
        
        reqEntity.setChunked(true); // Send in multiple parts if needed
        httppost.setEntity(reqEntity);        
        
        return httpclient.execute(httppost);
    }
    
    public static HttpResponse doMultiPartPostFake(String url, String fileName, Context paramContext) throws ClientProtocolException, IOException
    {
    	File file = new File(Environment.getExternalStorageDirectory(), fileName);
    	
    	HttpClient httpclient = RestEasy.getClient(paramContext);
        HttpPost httppost = new HttpPost(url);
        
//        MultipartEntity multipartContent = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);        
//        ContentBody cbFile        = new FileBody( file, "image/png" );
//        ContentBody cbMessage     = new StringBody( "TEST TSET" );
        
//        multipartContent.addPart( "source",cbFile);
//        multipartContent.addPart( "message",cbMessage);        
//        
//        httppost.setEntity(multipartContent);
//        Log.i("RestEasy", "executing request " + httppost.getRequestLine());        

        
        return httpclient.execute(httppost);
    }
    
    public static HttpResponse doMultiPartPost(String url, MMSInfoToTansfer mms, Context paramContext) throws ClientProtocolException, IOException
    {  	
    	HttpClient httpclient = RestEasy.getClient(paramContext);
        HttpPost httppost = new HttpPost(url);
        
//        MultipartEntity multipartContent = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//        
//        int i = 1;
//        for (MMSAttachment attachment : mms.getAttachments()) 
//        {
//        	String uniqueName = new String("attachment# " + i);
//        	InputStreamBody attachmentBody = new InputStreamBody(new ByteArrayInputStream(attachment.getData()), attachment.getType(),uniqueName);
//        	multipartContent.addPart(uniqueName,attachmentBody);
//        	++i;
//		}
//        
//        for (String key : mms.getMessageInfo().keySet()) 
//        {
//        	ContentBody mmsField = new StringBody(mms.getMessageInfo().get(key));
//        	multipartContent.addPart( key,mmsField);
//		}      
//        
//        httppost.setEntity(multipartContent);
        Log.i("RestEasy", "executing request " + httppost.getRequestLine());     

        return httpclient.execute(httppost);
    }
    
    // Put JSON to the resteasy web services
    public static HttpResponse doPut(String url, JSONObject c) throws ClientProtocolException, IOException
    {
    	HttpClient httpclient = new DefaultHttpClient();
    	HttpPut request = new HttpPut(url);
    	StringEntity s = new StringEntity(c.toString());
    	s.setContentEncoding("UTF-8");
        s.setContentType("application/json");

        request.setEntity(s);
        request.addHeader("accept", "application/json");
        
        return httpclient.execute(request);
    	
    }
    
    // Delete a resource in the resteasy web services
    public static void doDelete(String url) throws ClientProtocolException, IOException{
    	HttpClient httpclient = new DefaultHttpClient();
    	HttpDelete delete = new HttpDelete(url);
    	delete.addHeader("accept", "application/json");
    	httpclient.execute(delete);
    }

 
    // Retrieve a resource from the resteasy web service
    public static JSONObject doGet(String url)
    {
    	JSONObject json = null;
    	
        HttpClient httpclient = new DefaultHttpClient();
 
        // Prepare a request object
        HttpGet httpget = new HttpGet(url); 
        
        // Accept JSON
        httpget.addHeader("accept", "application/json");
 
        // Execute the request
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);
 
            // Get the response entity
            HttpEntity entity = response.getEntity();
            
            // If response entity is not null 
            if (entity != null) {
 
                // get entity contents and convert it to string
                InputStream instream = entity.getContent();
                String result= convertStreamToString(instream);
 
                // construct a JSON object with result
                json=new JSONObject(result);
     
                // Closing the input stream will trigger connection release
                instream.close();
            }
             
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // Return the json
        return json;
    }
    
    private static DefaultHttpClient getClient(Context paramContext) 
    {
    	
    	DefaultHttpClient ret = null;   	
    	    	
    	//SETS UP PARAMETERS
    	
    	HttpParams params = new BasicHttpParams();
    	
    	HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    	
    	HttpProtocolParams.setContentCharset(params, "utf-8");
    	
    	params.setBooleanParameter("http.protocol.expect-continue", false);
    	
	    // Set the timeout in milliseconds until a connection is established.
	    int timeoutConnection = 3000;
	    HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
	    // Set the default socket timeout (SO_TIMEOUT) 
	    // in milliseconds which is the timeout for waiting for data.
	    int timeoutSocket = 5 * 1000;
	    HttpConnectionParams.setSoTimeout(params, timeoutSocket);
	    
	    final int bufferSize = 1*1024;
	    HttpConnectionParams.setSocketBufferSize(params, bufferSize);
    	
    	
    	//REGISTERS SCHEMES FOR BOTH HTTP AND HTTPS
    	
    	SchemeRegistry registry = new SchemeRegistry();
    	
    	registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 8080/*80*/));
    	   	  	   	
    	registry.register(new Scheme("https", newSslSocketFactory(paramContext), 443/*8000*/));    	    	
    	
    	ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(params, registry);
    	
    	ret = new DefaultHttpClient(manager, params);
    	
    	return ret;
    	
    	}
    	

    private static SSLSocketFactory newSslSocketFactory(Context context) {
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = context.getResources().openRawResource(R.raw.mykeystore);
            try {
                // Initialize the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, "123456".toCharArray());
            } finally {
                in.close();
            }
            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            SSLSocketFactory sf = new SSLSocketFactory(trusted);
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            
            // TODO change to STRICT_HOSTNAME_VERIFIER
            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER/*ALLOW_ALL_HOSTNAME_VERIFIER*/);
            return sf;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }
    // Convert Inputstream to string
    public static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }    
 
}