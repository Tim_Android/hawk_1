package com.mobileenterprise.agent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.mobileenterprise.agent.helper.PermissionsHelper;
import com.mobileenterprise.agent.helper.Settings;

import java.util.List;

public class Splash extends Activity {
    private interface State {
        public void doAction();
    }

    private abstract class StateBase implements State {
        protected boolean checkOnly = false;
    }

    private class GeneralPermissionsState extends StateBase {
        private String[] permissions;
        private int[] grantResults;

        public GeneralPermissionsState() {
            this.checkOnly = false;
        }

        public GeneralPermissionsState(String[] permissions, int[] grantResults) {
            this.checkOnly = true;
            this.permissions = permissions;
            this.grantResults = grantResults;
        }

        @Override
        public void doAction() {
            if (this.checkOnly) {
                if (PermissionsHelper.checkRequestPermissionResults(permissions, grantResults)) {
                    stateContext.setState(new EnableWriteState(false));
                }
                else {
                    stateContext.setState(new ShowErrorAndExitState("You have to enable all HAWK permissions to use the app"));
                }
            }
            else {
                try {
                    String[] manifestPermissions = stateContext.getParentActivity().getPackageManager().getPackageInfo(
                            stateContext.getParentActivity().getPackageName(),
                            PackageManager.GET_PERMISSIONS).requestedPermissions;

                    List<String> deniedPermissions = PermissionsHelper.checkPermissions(stateContext.getParentActivity(), manifestPermissions);
                    if (deniedPermissions.isEmpty()) {
                        stateContext.setState(new EnableWriteState(false));
                    } else {
                        ActivityCompat.requestPermissions(stateContext.getParentActivity(), deniedPermissions.toArray(new String[0]), PERMISSION_ENABLED);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("Splash", e.getMessage());
                    stateContext.setState(new ShowErrorAndExitState("Unfortunately, HAWK has stopped"));
                }
            }
        }
    }

    private class EnableWriteState extends StateBase {

        public EnableWriteState(boolean checkOnly) {
            this.checkOnly = checkOnly;
        }

        @Override
        public void doAction() {
            if (PermissionsHelper.isWriteEnabled(stateContext.getParentActivity())) {
                stateContext.setState(new CheckAccessibilityState(false));
            }
            else {
                if (!checkOnly) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + stateContext.getParentActivity().getPackageName()));
                    startActivityForResult(intent, CODE_WRITE_SETTINGS_ENABLED);
                }
                else {
                    stateContext.setState(new ShowErrorAndExitState("You have to allow HAWK to modify system settings to use the app"));
                }
            }
        }
    }

    private class CheckAccessibilityState extends StateBase {
        public CheckAccessibilityState(boolean checkOnly) {
            this.checkOnly = checkOnly;
        }

        @Override
        public void doAction() {
            if (PermissionsHelper.isAccessibilityEnabled(stateContext.getParentActivity())) {
                stateContext.setState(new LaunchMainActivityState());
            }
            else {
                if (!checkOnly) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivityForResult(intent, ACCESSIBILITY_RESULT_ENABLED);
                }
                else {
                    stateContext.setState(new ShowErrorAndExitState("You have to enable Accessibility for HAWK to use the app"));
                }
            }
        }
    }

    private class LaunchMainActivityState implements State {
        @Override
        public void doAction() {
            h.postDelayed(r, DELAY);
        }
    }

    private class ShowErrorAndExitState implements State {
        private String message;

        public ShowErrorAndExitState(String message) {
            this.message = message;
        }

        @Override
        public void doAction() {
            Toast.makeText(stateContext.getParentActivity(), message, Toast.LENGTH_LONG).show();
            stateContext.getParentActivity().finish();
        }
    }

    private class StateContext {
        private State state = null;
        StateContext(final Activity parent) {
            this.parent = parent;
        }

        void setState(final State newState) {
            state = newState;
            newState.doAction();
        }

        public Activity getParentActivity() {
            return this.parent;
        }

        private final Activity parent;
    }

    private Handler h;
    private Runnable r;
    private StateContext stateContext;

    private final int PERMISSION_ENABLED = 0;
    private final int CODE_WRITE_SETTINGS_ENABLED = 1;
    private final int ACCESSIBILITY_RESULT_ENABLED = 2;
    private final int DELAY = 1500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        h = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            }
        };

        stateContext = new StateContext(this);

        Settings.openDataBase(this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            stateContext.setState(new LaunchMainActivityState());
        }
        else {
            if (Settings.getKeyStatus().equals(Settings.PARAMETER_FALSE)) {
                // This is the first launch of application
                stateContext.setState(new GeneralPermissionsState());
            } else {
                stateContext.setState(new LaunchMainActivityState());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
       this.stateContext.setState(new GeneralPermissionsState(permissions, grantResults));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_WRITE_SETTINGS_ENABLED) {
            this.stateContext.setState(new EnableWriteState(true));
        }

        if (requestCode == ACCESSIBILITY_RESULT_ENABLED) {
            this.stateContext.setState(new CheckAccessibilityState(true));
        }
    }
}
