package com.mobileenterprise.agent.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;

public class CustomLogger 
{
	public static void Write(Throwable e, Context paramContext)
	{
		final StringBuilder report = new StringBuilder();
        Date date = new Date();
        report.append( "Error Report collected on : " + date.toString() + "\n\n" );
        report.append( "Informations :\n" );
        
        DataProvider localDB = null;
        try
        {
	        Common.printDeviceInfo(report);
	        
	        report.append('\n').append('\n');
	        
	        Common.printStack( report, e );
	        
	        Common.printCause(report, e );
	        
	        /*File logFile = new File( Logger.getLogFilePath() );
	        File logsDir = logFile.getParentFile();*/
	        /*Common.saveCrashReport( logsDir, name, report );
	        
	        File crashReportFile = new File( logsDir, "crash_" + logFile.getName() );
	        Common.copyFile( logFile, crashReportFile );*/
	        
	        localDB = new DataProvider(paramContext);
	        localDB.InsertCrashLog(System.currentTimeMillis(), report.toString());
	        
        }
        catch(Exception ex)
        {
        	ex.printStackTrace();
        }
        catch(Throwable ex)
		{
			 ex.printStackTrace();
		}
        finally
        {
        	if(null != localDB)
        	{
        		localDB.DBClose();
        	}
        }
	}
}
