package com.mobileenterprise.agent;

import com.mobileenterprise.agent.helper.BookmarkColumns;
import com.mobileenterprise.agent.helper.BrowserDataObserver;
import com.mobileenterprise.agent.helper.CustomLogger;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.DuplicatedSiteInformation;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.Settings.BrowserType;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import android.util.Log;

public class DateChangeService extends IntentService 
{
    public DateChangeService() 
    {
        super("DateChangeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) 
    {
        updateBrowserHistorySkipList(Settings.BrowserType.ChromeBrowser, BrowserDataObserver.CHROME_HISTORY_URI);
        updateBrowserHistorySkipList(Settings.BrowserType.DefaultBrowser, BrowserDataObserver.BOOKMARKS_URI);
    }

    private void updateBrowserHistorySkipList(Settings.BrowserType browserType, Uri browserHistoryUri)
    {
        Long lastBrowserHistoryDate = Settings.getBrowserDate(browserType);
        if (!Settings.getBrowserActualDataStatus(browserType))
        {
            readBrowserHistorySkipList(lastBrowserHistoryDate, browserHistoryUri, browserType);
            Settings.setBrowserActualDataStatus(browserType, true);
            Log.i(this.getClass().getName(), "SkipList for browser history has been updated");
        }
    }

    private void readBrowserHistorySkipList(Long startDate, Uri browserHistoryUri, BrowserType browserType)
    {
        String[] queryProjection = new String[]{BookmarkColumns._ID, BookmarkColumns.TITLE, BookmarkColumns.URL, BookmarkColumns.DATE, BookmarkColumns.VISITS};
        String querySelection = BookmarkColumns.DATE + " > " + startDate;

        Cursor browserCursor = getContentResolver().query(browserHistoryUri, queryProjection, querySelection, null, null);
        if(browserCursor == null)
        {
            return;
        }

        if (browserCursor.getCount() <= 0 || !browserCursor.moveToFirst())
        {
            browserCursor.close();
            return;
        }

        DataProvider localUtils = null;
        try
        {
            localUtils = new DataProvider(this);
            do
            {
                Long date = browserCursor.getLong(browserCursor.getColumnIndexOrThrow(BookmarkColumns.DATE));
                Integer id = browserCursor.getInt(browserCursor.getColumnIndexOrThrow(BookmarkColumns._ID));
                Integer visits = browserCursor.getInt(browserCursor.getColumnIndexOrThrow(BookmarkColumns.VISITS));

                localUtils.InsertBrowserHistoryToSkipList(new DuplicatedSiteInformation(id, date, visits, browserType));
            }
            while (browserCursor.moveToNext());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CustomLogger.Write(ex, this);
            Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
        }
        finally
        {
            browserCursor.close();

            if(localUtils != null)
            {
                localUtils.DBClose();
            }
        }
    }
}