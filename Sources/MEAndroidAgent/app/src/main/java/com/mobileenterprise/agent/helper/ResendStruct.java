package com.mobileenterprise.agent.helper;

import java.util.ArrayList;

import org.json.JSONObject;

public class ResendStruct 
{
	public ResendStruct()
	{
		setDataArray(new JSONObject());
		setIdsToRemove(new ArrayList<Integer>());
	}
	
	private void setDataArray(JSONObject dataArray) {
		this.dataArray = dataArray;
	}
	public JSONObject getDataArray() {
		return dataArray;
	}

	private void setIdsToRemove(ArrayList<Integer> idsToRemove) {
		this.idsToRemove = idsToRemove;
	}

	public ArrayList<Integer> getIdsToRemove() {
		return idsToRemove;
	}

	private JSONObject dataArray;
	private ArrayList<Integer> idsToRemove;
}
