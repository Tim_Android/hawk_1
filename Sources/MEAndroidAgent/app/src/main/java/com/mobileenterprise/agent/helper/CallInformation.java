package com.mobileenterprise.agent.helper;

public class CallInformation
{
	  public CallInformation(String fromNumber, 
			  				String fromNumberName,
			  				String fromNumberType,
			  				String toNumber, 
			  				String toNumberName,
			  				String toNumberType,
			  				String type, 
			  				long dateN, 
			  				int duration)
	  {
		  this.setFromNumber(fromNumber);
		  this.setFromNumberName(fromNumberName);
		  this.setFromNumberType(fromNumberType);
		  this.setToNumber(toNumber);
		  this.setToNumberName(toNumberName);
		  this.setToNumberType(toNumberType);
		  this.setType(type);
		  this.setDate(dateN); 
		  this.setDuration(duration);
	  }
	  
		public void setFromNumber(String fromNumber) 
		{
			this.fromNumber = fromNumber;
		}
		public String getFromNumber() 
		{
			return fromNumber;
		}

		public void setToNumber(String toNumber) {
			this.toNumber = toNumber;
		}

		public String getToNumber() {
			return toNumber;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public void setDate(long dateN) {
			this.date = dateN;
		}

		public long getDate() {
			return date;
		}
		
		public void setDuration(int duration) {
			this.duration = duration;
		}

		public int getDuration() {
			return duration;
		}

		public void setFromNumberName(String fromNumberName) {
			this.fromNumberName = fromNumberName;
		}

		public String getFromNumberName() {
			return fromNumberName;
		}

		public void setToNumberName(String toNumberName) {
			this.toNumberName = toNumberName;
		}

		public String getToNumberName() {
			return toNumberName;
		}

		public void setFromNumberType(String fromNumberType) {
			this.fromNumberType = fromNumberType;
		}

		public String getFromNumberType() {
			return fromNumberType;
		}

		public void setToNumberType(String toNumberType) {
			this.toNumberType = toNumberType;
		}

		public String getToNumberType() {
			return toNumberType;
		}

		private String fromNumber;
		private String fromNumberName;
		private String fromNumberType;
		private String toNumber;
		private String toNumberName;
		private String toNumberType;
		private String type;
		private long date;
		private int duration;
}
