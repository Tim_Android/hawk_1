package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.TimeZone;

public class Common {
    private static TelephonyManager TelephonyManager_ = null;
    private static final int RESPONSE_SUCCESS = 1;
    private static final int RESPONSE_INTERNAL_ERROR = 2;
    private static final int RESPONSE_INCORRECT_CREDS = 0;

    public static int GetTimeZoneRawOffset() {
        TimeZone tz = TimeZone.getDefault();
        int rawTimeZoneOffset = 0;
        if (tz != null) {
            rawTimeZoneOffset = tz.getRawOffset();
        }
        return rawTimeZoneOffset;
    }

    public static String GetTimeZoneDisplayName() {
        TimeZone tz = TimeZone.getDefault();
        String result = "Unknown time zone";
        if (tz != null) {
            String tzShortName = tz.getDisplayName(false, TimeZone.SHORT);
            String tzLongName = tz.getDisplayName(false, TimeZone.LONG);
            String tzID = tz.getID();

            result = tzShortName + ", " + tzLongName + ", " + tzID;
        }
        return result;
    }

    public static void printDeviceInfo(StringBuilder stringBuilder) {
        if (stringBuilder != null) {
            stringBuilder.append("Brand: " + Build.BRAND + "\n");
            stringBuilder.append("Device: " + Build.DEVICE + "\n");
            stringBuilder.append("Display: " + Build.DISPLAY + "\n");
            stringBuilder.append("Fingerprint: " + Build.FINGERPRINT + "\n");
            stringBuilder.append("ID: " + Build.ID + "\n");
            stringBuilder.append("Manufacturer: " + Build.MANUFACTURER + "\n"); // support from API level 4
            stringBuilder.append("Model: " + Build.MODEL + "\n");
            stringBuilder.append("Product: " + Build.PRODUCT + "\n");
            stringBuilder.append("Tags: " + Build.TAGS + "\n");
            stringBuilder.append("Type: " + Build.TYPE + "\n");

            stringBuilder.append("Hardware: " + Build.HARDWARE + "\n"); // support from API level 8
            stringBuilder.append("Board: " + Build.BOARD + "\n");
            stringBuilder.append("Bootloader: " + Build.BOOTLOADER + "\n");
            stringBuilder.append("ABI: " + Build.CPU_ABI + "\n");
            stringBuilder.append("ABI2: " + Build.CPU_ABI2 + "\n");

            stringBuilder.append("Host: " + Build.HOST + "\n");

            stringBuilder.append("SDK Version: " + Build.VERSION.SDK + "\n");
            stringBuilder.append("Release Version: " + Build.VERSION.RELEASE + "\n");

            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            long blockSize = statFs.getBlockSize();
            long availableBlocks = statFs.getAvailableBlocks();
            long blockCount = statFs.getBlockCount();

            stringBuilder.append("Total Internal memory: " + (blockCount * blockSize) + "\n");
            stringBuilder.append("Available Internal memory: " + (availableBlocks * blockSize) + "\n");
        }
    }

    //TODO look at Log.getStackTraceString(Throwable tr) method insted
    public static void printStack(StringBuilder stringBuilder, Throwable e) {
        if (stringBuilder != null && e != null) {
            stringBuilder.append("Stack:\n");

            stringBuilder.append(e.toString() + "\n\n");

            stringBuilder.append("--------- Stack trace ---------\n\n");
            Writer stackTrace = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stackTrace);
            e.printStackTrace(printWriter);

            stringBuilder.append(stackTrace.toString());
            printWriter.close();

            stringBuilder.append("-------------------------------\n\n");
        }
    }

    public static void printCause(StringBuilder report, Throwable e) {
        if (report != null && e != null) {

            Throwable cause = e.getCause();
            if (cause != null) {
                report.append("Cause:\n");
                report.append(cause.toString() + "\n\n");

                report.append("--------- Cause trace ---------\n\n");
                Writer stackTrace = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stackTrace);
                e.printStackTrace(printWriter);

                report.append(stackTrace.toString());
                printWriter.close();

                report.append("-------------------------------\n\n");
            }
        }
    }

    public static byte[] readBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        byteBuffer.flush();
        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public static String getDeviceID(Context context) {
        if (TelephonyManager_ == null) {
            TelephonyManager_ = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        }
        String deviceID = TelephonyManager_.getDeviceId();
        if (deviceID == null || deviceID.equals("")) {
            deviceID = "Unknown";
        }
        return deviceID;

    }

    public static String getPhoneNumber(Context context) {
        if (TelephonyManager_ == null) {
            TelephonyManager_ = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        }
        String line1Number = TelephonyManager_.getLine1Number();
        if (line1Number == null || line1Number.equals("")) {
            line1Number = "Unknown";
        }

        return line1Number;

    }

    public static String removeLastComma(String address) {
        final String pattern = ", ";
        int resultIndex = address.lastIndexOf(pattern);
        int endPosition = address.length() - pattern.length();
        if (resultIndex != -1 && endPosition >= 0 && resultIndex == endPosition) {
            String result = address.substring(0, resultIndex);
            return result;
        } else {
            return address;
        }
    }


    public static String parseServerResponse(String response, Context context) {
        JSONObject jsonObjectID = null;
        String agentID = null;
        try {
            jsonObjectID = new JSONObject(response);

            //check response state
            int state = Integer.parseInt(jsonObjectID.getString("state"));
            //get agentID from server respond
            switch (state) {
                case RESPONSE_INCORRECT_CREDS: {
                    agentID = String.valueOf(RESPONSE_INCORRECT_CREDS);
                    Log.w("parseServerResponse", "Client with this credentials was not authorized on the web site.");
                    Throwable e = new Throwable("Client with this credentials was not authorized on the web site");
                    CustomLogger.Write(e, context);
                }
                break;
                case RESPONSE_SUCCESS: {
                    agentID = jsonObjectID.getString("agentId");
                    Throwable e = new Throwable("Client was authorized. AgentID=" + agentID);
                    CustomLogger.Write(e, context);
                }
                break;

                case RESPONSE_INTERNAL_ERROR: {
                    agentID = String.valueOf(RESPONSE_INTERNAL_ERROR);
                    Log.w("parseServerResponse", "Internal error");
                    Throwable e = new Throwable("Internal error");
                    CustomLogger.Write(e, context);
                }
                break;
                default:
                    Log.w("parseServerResponse", "State didn't match. State:" + state);
                    Throwable e = new Throwable("State didn't match. State:" + state);
                    CustomLogger.Write(e, context);
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
            CustomLogger.Write(e, context);
        }
        return agentID;
    }
}
