package com.mobileenterprise.agent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobileenterprise.agent.helper.DataChecker;
import com.mobileenterprise.agent.helper.NetworkChecker;

import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity {
    private Button btnSignUp;
    private CheckBox chkAgreement;
    private TextInputLayout tilFirstName;
    private TextInputLayout tilLastName;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private AlertDialog alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        chkAgreement = (CheckBox) findViewById(R.id.chkAgreement);


        tilFirstName = (TextInputLayout) findViewById(R.id.tilFirstName);
        tilLastName = (TextInputLayout) findViewById(R.id.tilLastName);
        tilEmail = (TextInputLayout) findViewById(R.id.tilEmailReg);
        tilPassword = (TextInputLayout) findViewById(R.id.tilPasswordReg);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etEmail = (EditText) findViewById(R.id.etEmailReg);
        etPassword = (EditText) findViewById(R.id.etPasswordReg);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);

        TextView tvAgreement = (TextView) findViewById(R.id.tvAgreement);

        btnSignUp.setEnabled(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        alert = builder.create();
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                onLicenseAgreement();
            }
        };

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(getString(R.string.accept_agreement_text));
        int index = getString(R.string.accept_agreement_text).lastIndexOf(getString(R.string.license_agreement_text));
        spannableStringBuilder.setSpan(clickableSpan, index, index + getString(R.string.license_agreement_text).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAgreement.setText(spannableStringBuilder);
        tvAgreement.setMovementMethod(LinkMovementMethod.getInstance());

        setTextListener(etPassword);
        setTextListener(etFirstName);
        setTextListener(etLastName);
        setTextListener(etEmail);

    }

    public void setTextListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                DataChecker.isFieldEmpty(tilFirstName, RegistrationActivity.this);
                DataChecker.isFieldEmpty(tilLastName, RegistrationActivity.this);
                DataChecker.isFieldEmpty(tilPassword, RegistrationActivity.this);
                DataChecker.isPasswordCorrectLength(tilPassword, RegistrationActivity.this);
                DataChecker.isFieldEmpty(tilEmail, RegistrationActivity.this);
                DataChecker.isEmailCorrectFormat(tilEmail, RegistrationActivity.this);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }

    public void onLicenseAgreement() {
        Uri address = Uri.parse(getString(R.string.license_agreement));
        Intent openlink = new Intent(Intent.ACTION_VIEW, address);
        startActivity(openlink);
    }

    public void onCheckAgreement(View view) {
        if (chkAgreement.isChecked()) {
            btnSignUp.setEnabled(true);
        } else
            btnSignUp.setEnabled(false);
    }

    public void onSignUp(View view) {
        boolean network = NetworkChecker.IsNetworkAvailable(this);
        String firstName = DataChecker.isFieldEmpty(etFirstName, RegistrationActivity.this);
        String lastName = DataChecker.isFieldEmpty(etLastName, RegistrationActivity.this);
        String password = DataChecker.isFieldEmpty(etPassword, RegistrationActivity.this);
        String email = DataChecker.isFieldEmpty(etEmail, RegistrationActivity.this);


        if (firstName == null || lastName == null || password == null || email == null)
            return;
        if (!DataChecker.isPasswordCorrectLength(etPassword, RegistrationActivity.this))
            return;
        if (!DataChecker.isEmailCorrectFormat(etEmail, RegistrationActivity.this))
            return;
        if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            Toast.makeText(this, getString(R.string.passwords_do_not_match), Toast.LENGTH_SHORT).show();
            return;
        }

        if (network) {
            //do request on site to register client
            startConfirmationPage();
        } else {
            alert.setMessage(getString(R.string.no_internet));
            alert.show();
        }
    }

    private void startConfirmationPage() {
        Intent activityIntent = new Intent(this, ConfirmActivity.class);
        startActivityForResult(activityIntent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish();
    }


    private void accountIsNotActivated(String email) {
        SpannableStringBuilder ss = new SpannableStringBuilder(getString(R.string.existing_email_is_not_activated));
        alert.setMessage(ss);
        Pattern pattern = Pattern.compile(getString(R.string.tap_here));
        Linkify.addLinks(ss, pattern, getString(R.string.resend_email) + email);
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.log_in), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
        ((TextView) alert.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
