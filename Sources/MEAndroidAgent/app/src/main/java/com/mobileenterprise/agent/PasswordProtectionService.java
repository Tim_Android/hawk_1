package com.mobileenterprise.agent;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.mobileenterprise.agent.helper.PermissionsHelper;
import com.mobileenterprise.agent.helper.Settings;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class PasswordProtectionService extends AccessibilityService {

    public static final String ACTION_CORRECT_PASSWORD = "Correct";
    private boolean isApproved;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        final int eventType = event.getEventType();

        String packageName;
        switch (eventType) {
            case AccessibilityEvent.TYPE_VIEW_CLICKED:
            case AccessibilityEvent.TYPE_VIEW_SCROLLED: {

                AccessibilityNodeInfo node = getRootInActiveWindow();

                if (node == null) {
                    return;
                }

                packageName = String.valueOf(node.getPackageName());

                if (packageName.equals(getString(R.string.package_settings)) || packageName.equals(getString(R.string.package_miui_settings))) {

                    List<AccessibilityNodeInfo> listName = node.findAccessibilityNodeInfosByText(getString(R.string.app_name));
                    List<AccessibilityNodeInfo> listAccessibilityfirst = node.findAccessibilityNodeInfosByText(getString(R.string.accessibility_service_settings_description));
                    List<AccessibilityNodeInfo> listAccessibilitysecond = node.findAccessibilityNodeInfosByText(getString(R.string.accessibility_service_description));

                    List<AccessibilityNodeInfo> listLeft = node.findAccessibilityNodeInfosByViewId(getString(R.string.settings_button_left));
                    List<AccessibilityNodeInfo> listRight = node.findAccessibilityNodeInfosByViewId(getString(R.string.settings_button_right));

                    //for xiaomi miui7
                    List<AccessibilityNodeInfo> listIdAppName = node.findAccessibilityNodeInfosByViewId(getString(R.string.settings_id_app_name));
                    List<AccessibilityNodeInfo> listMiuiActionBar = node.findAccessibilityNodeInfosByViewId(getString(R.string.settings_miui_action_bar_title));

                    //for xiaomi miui9
                    List<AccessibilityNodeInfo> idAppDetails = node.findAccessibilityNodeInfosByViewId(getString(R.string.settings_miui_app_manager_details_applabel));

                    if ((listLeft.size() != 0 && listRight.size() != 0)
                            || (checkSettings(listIdAppName) && checkSettings(listMiuiActionBar))
                            || checkSettings(idAppDetails)
                            || ((listAccessibilityfirst.size() != 0 || listAccessibilitysecond.size() != 0) && AccessibilityEvent.TYPE_VIEW_CLICKED == eventType)) {

                        if (listName.size() != 0 && !isApproved && (PermissionsHelper.isAccessibilityEnabled(getApplicationContext()) && !Settings.isFirstRun())) {
                            performGlobalAction(GLOBAL_ACTION_BACK);
                            Intent activityIntent = new Intent(this, SettingsProtectionActivity.class);
                            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(activityIntent);
                            setTimer();
                        }
                    }


                }

                Log.d("SettingsProtectService:", " ClassName:" + node.getChildCount() + " nodeInfo.getPackageName(): " + node.getPackageName());

            }
            break;
        }
    }

    @Override
    public void onInterrupt() {
    }


    @Override
    public void onServiceConnected() {

        Toast.makeText(getApplicationContext(), "onServiceConnected", Toast.LENGTH_SHORT).show();
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();

        info.eventTypes = AccessibilityEvent.TYPE_VIEW_CLICKED | AccessibilityEvent.TYPE_VIEW_SCROLLED;

        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;

        info.notificationTimeout = 100;

        this.setServiceInfo(info);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_CORRECT_PASSWORD: {
                        isApproved = true;
                    }
                    break;
                }
            }
        }
        return START_NOT_STICKY;
    }

    private boolean checkSettings(List<AccessibilityNodeInfo> list) {
        return list.size() != 0;
    }


    private void setTimer() {
        Timer timer = new Timer();
        TimerTask mTimerTask = new MyTimerTask();

        //set timer delay 20 sec
        timer.schedule(mTimerTask, 20000);
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            isApproved = false;
        }
    }


}
