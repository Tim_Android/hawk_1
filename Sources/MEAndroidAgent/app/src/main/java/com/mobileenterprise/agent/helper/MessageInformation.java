package com.mobileenterprise.agent.helper;

import java.util.ArrayList;

public class MessageInformation 
{
    private final static String UNKNOWN_TYPE = "Unknown type";

    private final static int MESSAGE_TYPE_ALL    = 0;
    private final static int MESSAGE_TYPE_INBOX  = 1;
    private final static int MESSAGE_TYPE_SENT   = 2;
    private final static int MESSAGE_TYPE_DRAFT  = 3;
    private final static int MESSAGE_TYPE_OUTBOX = 4;
    private final static int MESSAGE_TYPE_FAILED = 5; // for failed outgoing messages
    private final static int MESSAGE_TYPE_QUEUED = 6; // for messages to send later

    /**
     * X-Mms-Message-Type field types.
     */
    // not all types
    private static final int MESSAGE_TYPE_SEND_REQ           = 0x80;
    private static final int MESSAGE_TYPE_NOTIFICATION_IND   = 0x82;
    private static final int MESSAGE_TYPE_RETRIEVE_CONF      = 0x84;
    private static final int MESSAGE_TYPE_READ_REC_IND       = 0x87;

    public final static int MESSAGE_TYPE_UNKNOWN = -1;

    public MessageInformation(
        String address,
        String body,
        String type,
        long date,
        String subject,
        ArrayList<MMSAttachment> attachments,
        int messageType)
    {
        setAddress(address);
        setContactName("");
        setBody(body);
        setDate(date);
        setType(type);
        setSubject(subject);
        setAttachments(attachments);
        this.messageType = messageType;
    }

    public boolean needSend()
    {
        if (MESSAGE_TYPE_NOTIFICATION_IND == this.messageType 
            || MESSAGE_TYPE_READ_REC_IND  == this.messageType)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static String ConvertToStringType(String numberType)
    {
        String result = UNKNOWN_TYPE;

        switch (Integer.parseInt(numberType))
        {
        case MESSAGE_TYPE_INBOX :
            result = "Inbox";
            break;
        case MESSAGE_TYPE_SENT :
            result = "Sent";
            break;
        case MESSAGE_TYPE_DRAFT :
            result = "Draft";
            break;
        case MESSAGE_TYPE_OUTBOX :
            result = "Outbox";
            break;
        case MESSAGE_TYPE_FAILED :
            result = "Failed";
            break;
        case MESSAGE_TYPE_QUEUED :
            result = "Queued";
            break;
        default:
            break;	
        }

        return result;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setContactName(String name) {
        this.contactName = name;
    }

    public String getContactName(){
        return this.contactName;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setAttachments(ArrayList<MMSAttachment> attachments) {
        this.attachments = attachments;
    }

    public ArrayList<MMSAttachment> getAttachments() {
        return attachments;
    }

    private String address;
    private String contactName;
    private String body;
    private String subject;
    private String type;
    private long date;
    private ArrayList<MMSAttachment> attachments;
    private int messageType;
}
