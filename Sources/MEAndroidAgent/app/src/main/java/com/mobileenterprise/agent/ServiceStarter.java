package com.mobileenterprise.agent;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServiceStarter extends BroadcastReceiver 
{

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		Intent serviceLauncher = new Intent(context, BackgroundService.class);
		ComponentName workerService = context.startService(serviceLauncher);
		if(workerService != null)
		{
			Log.v(this.getClass().getName(), "Service loaded by shedule time.");
		}
	}

}
