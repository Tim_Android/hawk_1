
package com.mobileenterprise.agent.helper;

import java.lang.Thread.UncaughtExceptionHandler;
import android.app.Service;
import android.util.Log;


public class MEUncaughtExceptionHandler implements UncaughtExceptionHandler
{
    private UncaughtExceptionHandler defaultUEH = null;
    private Service service = null;
    
    public MEUncaughtExceptionHandler(Service service)
    {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        this.service = service;
    }

    public void uncaughtException( Thread t, Throwable e )
    {       
        try
        {
        	CustomLogger.Write(e, this.service.getBaseContext());
        	Log.i(this.getClass().getName(),"Crash Log inserted");
	    }
        finally
        {
        	defaultUEH.uncaughtException( t, e );
        }
    }
    
}
