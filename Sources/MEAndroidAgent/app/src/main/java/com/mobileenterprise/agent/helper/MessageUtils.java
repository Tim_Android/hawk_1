package com.mobileenterprise.agent.helper;

import java.io.UnsupportedEncodingException;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.provider.*;

public class MessageUtils 
{
	private static final String TAG = "MessageUtils";

	public static String extractEncStrFromCursor(Cursor cursor,
            int columnRawBytes, int columnCharset) {
        String rawBytes = cursor.getString(columnRawBytes);
        int charset = cursor.getInt(columnCharset);

        if (TextUtils.isEmpty(rawBytes)) {
            return "";
        } else if (charset == CharacterSets.ANY_CHARSET) {
            return rawBytes;
        } else {
            return new EncodedStringValue(charset, getBytes(rawBytes)).getString();
        }
    }
	
	/**
     * Unpack a given String into a byte[].
     */
    public static byte[] getBytes(String data) {
        try {
            return data.getBytes(CharacterSets.MIMENAME_ISO_8859_1);
        } catch (UnsupportedEncodingException e) {
            // Impossible to reach here!
            Log.e(TAG, "ISO_8859_1 must be supported!", e);
            return new byte[0];
        }
    }

    public static String findContactNameByAddress(String address, Context context){

        String contactName = findContactNameByPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, address, context);
        if(contactName == null){
            contactName = findContactNameByPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, address, context);
        }

        return contactName;
    }

    private static String findContactNameByPath(Uri baseUri, String contactField, Context context){

        Uri uri = Uri.withAppendedPath(baseUri, Uri.encode(contactField));
        String [] projection = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME };

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) {
            return null;
        }

        try {
            if (cursor.moveToFirst()) {
                return cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
        }
        catch (Exception ex){
            Log.e(TAG, "Cannot find contact name!", ex);
        }
        finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }
}
