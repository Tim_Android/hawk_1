package com.mobileenterprise.agent.helper;

// Class android.provider.Browser.BookmarkColumns is not aviable sinse SDK Level 23
public class BookmarkColumns {
    public static final String _ID = "_id";
    public static final String URL = "url";
    public static final String VISITS = "visits";
    public static final String DATE = "date";
    public static final String BOOKMARK = "bookmark";
    public static final String TITLE = "title";
    public static final String CREATED = "created";
    public static final String FAVICON = "favicon";
}