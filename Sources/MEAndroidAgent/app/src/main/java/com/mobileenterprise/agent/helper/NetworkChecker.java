package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkChecker {
	
	public static Boolean IsNetworkAvailable(Context paramContext)
	{
		Boolean isNetworkAvailable = false;
		
		String service = Context.CONNECTIVITY_SERVICE;
		ConnectivityManager connectivity = (ConnectivityManager) paramContext.getSystemService(service);
				
		NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();		
	
		int networkPreference = connectivity.getNetworkPreference();
		Log.i("com.mobileenterprise.agent.NetworkChecker","networkPreference is " + networkPreference);		
		if(null != activeNetwork)
		{
			Log.i("com.mobileenterprise.agent.NetworkChecker","networkName is " + activeNetwork.getTypeName());
			//connectivity.setNetworkPreference(ConnectivityManager.NetworkPreference.PREFER_WIFI);
			
			isNetworkAvailable = activeNetwork.isAvailable() && activeNetwork.isConnected() && connectivity.getBackgroundDataSetting();
		}
		
		return isNetworkAvailable;
	}

}
