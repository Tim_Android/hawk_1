package com.mobileenterprise.agent.helper;

public class MMSAttachment 
{
    public MMSAttachment(byte[] data, String type, int id, int mmsID)
    {
        this.setData(data);
        this.setType(type);
        this.setId(id);
        this.setMmsID(mmsID);
    }

    public MMSAttachment(byte[] data, String type)
    {
        this.setData(data);
        this.setType(type);
    }

    public void setData(byte[] data) {
        this.data = data;
    }
    public byte[] getData() {
        return data;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setMmsID(int mmsID)
    {
        this.mmsID = mmsID;
    }

    public int getMmsID()
    {
        return mmsID;
    }

    private String type;
    private int id;
    private int mmsID;
    private byte[] data;
}
