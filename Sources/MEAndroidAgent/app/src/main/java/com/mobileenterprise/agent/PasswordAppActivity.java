package com.mobileenterprise.agent;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobileenterprise.agent.helper.DataChecker;
import com.mobileenterprise.agent.helper.Settings;

public class PasswordAppActivity extends AppCompatActivity {


    private Button btnStartAgent;
    private TextInputLayout tilPassword;
    private TextInputLayout tilPasswordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_app);

        btnStartAgent = (Button) findViewById(R.id.btnAgentStart);
        btnStartAgent.setEnabled(false);

        EditText etConfirmPassword = (EditText) findViewById(R.id.etConfirmPasswordStep2);

        tilPassword = (TextInputLayout) findViewById(R.id.tilPasswordStep2);
        tilPasswordConfirm = (TextInputLayout) findViewById(R.id.tilConfirmPasswordStep2);

        btnStartAgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passwordStr = DataChecker.isFieldEmpty(tilPassword, PasswordAppActivity.this);
                String confirmPassword = DataChecker.isFieldEmpty(tilPasswordConfirm, PasswordAppActivity.this);
                if (passwordStr == null || confirmPassword == null)
                    return;
                if (!passwordStr.equals(confirmPassword)) {
                    Toast.makeText(PasswordAppActivity.this, getString(R.string.passwords_do_not_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                Settings.setPassword(passwordStr);
                finish();
            }
        });

        etConfirmPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() >= Settings.MIN_PASSWORD_LENGTH) {
                    btnStartAgent.setEnabled(true);
                } else {
                    btnStartAgent.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

    }


}
