package com.mobileenterprise.agent.helper;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class DeviceInfo
{
    public static final String KEY_DEVICE_INFO_DEVICE_ID = "DEVICE_INFO_DEVICE_ID";
    public static final String KEY_DEVICE_INFO_PHONE_NUMBER = "DEVICE_INFO_PHONE_NUMBER";
    public static final String KEY_DEVICE_INFO_BRAND = "DEVICE_INFO_BRAND";
    public static final String KEY_DEVICE_INFO_MODEL_NAME = "DEVICE_INFO_MODEL_NAME";
    public static final String KEY_DEVICE_INFO_OS_VERSION = "DEVICE_INFO_OS_VERSION";
    public static final String KEY_DEVICE_INFO_SDK_VERSION = "DEVICE_INFO_SDK_VERSION";
    public static final String KEY_DEVICE_INFO_FIRMWARE_ID = "DEVICE_INFO_FIRMWARE_ID";
    public static final String KEY_DEVICE_INFO_SVN_VERSION = "DEVICE_INFO_SVN_VERSION";
    public static final String KEY_DEVICE_INFO_AGENT_NAME = "DEVICE_INFO_AGENT_NAME";
    
    public static final String KEY_DEVICE_INFO_TIME_ZONE_NAME = "DEVICE_INFO_TIME_ZONE_NAME";
    public static final String KEY_DEVICE_INFO_TIME_ZONE_RAW_OFFSET = "DEVICE_INFO_TIME_ZONE_RAW_OFFSET";
    public static final String DEVICE_INFO = "deviceInfo";
    
    public DeviceInfo(Context paramContext,
                      String deviceID,
                      String phoneNumber,
                      String brand,
                      String modelName,
                      String osVersion,
                      String sdkVersion,
                      String firmwareID,
                      String svnVersion,
                      String agentName,
                      String timeZoneFullName,
                      int timeZoneRawOffset)
    {
        this.context            = paramContext;
        this.setDeviceID(deviceID);
        this.setPhoneNumber(phoneNumber);
        this.setBrand(brand);
        this.setModelName(modelName);
        this.setOsVersion(osVersion);
        this.setSdkVersion(sdkVersion);
        this.setFirmwareID(firmwareID);
        this.setSvnVersion(svnVersion);
        this.setAgentName(agentName);
        this.setTimeZoneFullName(timeZoneFullName);
        this.setTimeZoneRawOffset(timeZoneRawOffset); 
    }

    public JSONObject ConvertToJson()
    {
        JSONObject devInfoFeature = new JSONObject();
        
        JSONObject devInfo = new JSONObject();
        
        try
        {
            devInfo.put(KEY_DEVICE_INFO_DEVICE_ID, this.getDeviceID());
            devInfo.put(KEY_DEVICE_INFO_PHONE_NUMBER, this.getPhoneNumber());
            devInfo.put(KEY_DEVICE_INFO_BRAND, this.getBrand());
            devInfo.put(KEY_DEVICE_INFO_MODEL_NAME, this.getModelName());
            devInfo.put(KEY_DEVICE_INFO_OS_VERSION, this.getOsVersion());
            devInfo.put(KEY_DEVICE_INFO_SDK_VERSION, this.getSdkVersion());
            devInfo.put(KEY_DEVICE_INFO_FIRMWARE_ID, this.getFirmwareID());
            devInfo.put(KEY_DEVICE_INFO_SVN_VERSION, this.getSvnVersion());
            devInfo.put(KEY_DEVICE_INFO_AGENT_NAME, this.getAgentName());
            devInfo.put(KEY_DEVICE_INFO_TIME_ZONE_NAME, this.getTimeZoneFullName());
            devInfo.put(KEY_DEVICE_INFO_TIME_ZONE_RAW_OFFSET, this.getTimeZoneRawOffset());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
            Log.e(this.getClass().getName(), "Error! " + e.getMessage());
        }
        
        try
        {
            devInfoFeature.put(DEVICE_INFO, devInfo);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
            Log.e(this.getClass().getName(), "Error! " + e.getMessage());
        }
        
        return devInfoFeature;
    }
    public JSONObject ConvertToJsonForRequest()
    {

        JSONObject devInfo = new JSONObject();

        try
        {
            devInfo.put(KEY_DEVICE_INFO_DEVICE_ID, this.getDeviceID());
            devInfo.put(KEY_DEVICE_INFO_PHONE_NUMBER, this.getPhoneNumber());
            devInfo.put(KEY_DEVICE_INFO_BRAND, this.getBrand());
            devInfo.put(KEY_DEVICE_INFO_MODEL_NAME, this.getModelName());
            devInfo.put(KEY_DEVICE_INFO_OS_VERSION, this.getOsVersion());
            devInfo.put(KEY_DEVICE_INFO_SDK_VERSION, this.getSdkVersion());
            devInfo.put(KEY_DEVICE_INFO_FIRMWARE_ID, this.getFirmwareID());
            devInfo.put(KEY_DEVICE_INFO_SVN_VERSION, this.getSvnVersion());
            devInfo.put(KEY_DEVICE_INFO_AGENT_NAME, this.getAgentName());
            devInfo.put(KEY_DEVICE_INFO_TIME_ZONE_NAME, this.getTimeZoneFullName());
            devInfo.put(KEY_DEVICE_INFO_TIME_ZONE_RAW_OFFSET, this.getTimeZoneRawOffset());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
            Log.e(this.getClass().getName(), "Error! " + e.getMessage());
        }

        return devInfo;
    }
    public String getDeviceID()
    {
        return deviceID;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getBrand()
    {
        return brand;
    }

    public String getModelName()
    {
        return modelName;
    }

    public String getOsVersion()
    {
        return osVersion;
    }

    public String getSdkVersion()
    {
        return sdkVersion;
    }

    public String getFirmwareID()
    {
        return firmwareID;
    }

    public String getSvnVersion()
    {
        return svnVersion;
    }

    public String getAgentName()
    {
        return agentName;
    }

    public String getTimeZoneFullName()
    {
        return timeZoneFullName;
    }
    
    public int getTimeZoneRawOffset()
    {
        return timeZoneRawOffset;
    }
    
    private void setDeviceID(String deviceID)
    {
        this.deviceID = deviceID;   
    }
    
    private void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    private void setBrand(String brand)
    {
        this.brand = brand;
    }

    private void setModelName(String modelName)
    {
        this.modelName = modelName;
    }

    private void setOsVersion(String osVersion)
    {
        this.osVersion = osVersion;
    }

    private void setSdkVersion(String sdkVersion)
    {
        this.sdkVersion = sdkVersion;
    }

    private void setFirmwareID(String firmwareID)
    {
        this.firmwareID = firmwareID;
    }

    private void setSvnVersion(String svnVersion)
    {
        this.svnVersion = svnVersion;
    }

    private void setAgentName(String agentName)
    {
        this.agentName = agentName;
    }

    private void setTimeZoneFullName(String timeZoneFullName)
    {
        this.timeZoneFullName = timeZoneFullName;
    }

    private void setTimeZoneRawOffset(int timeZoneRawOffset)
    {
        this.timeZoneRawOffset = timeZoneRawOffset;
    }


    private Context context;
    private String deviceID;
    private String phoneNumber;
    private String brand;
    private String modelName;
    private String osVersion;
    private String sdkVersion;
    private String firmwareID;
    private String svnVersion;
    private String agentName;
    private String timeZoneFullName;
    private int    timeZoneRawOffset;
}
