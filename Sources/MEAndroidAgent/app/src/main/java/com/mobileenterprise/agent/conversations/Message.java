package com.mobileenterprise.agent.conversations;

public class Message {
    private String sender;
    private String recipient;
    private String body;
    private String type;
    private String chatName;
    private long timestamp;
    private String applicationName;
    private int hash;

    public Message(String sender, String recipient, String body, String type, String chatName, long timestamp, String applicationName) {
        this.sender = sender;
        this.recipient = recipient;
        this.body = body;
        this.type = type;
        this.chatName = chatName;
        this.timestamp = timestamp;
        this.applicationName = applicationName;

        this.hash = new StringBuilder("sender").append(this.sender).append("recipient").append(this.recipient).append("body").append(this.body).
                append("type").append(this.type).append("chatName").append(this.chatName).append("timestamp").append(this.timestamp).
                append("applicationName").append(this.applicationName).toString().hashCode();
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getBody() {
        return body;
    }

    public String getType() {
        return type;
    }

    public String getChatName() {
        return chatName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public int getHash() { return this.hash; }
}
