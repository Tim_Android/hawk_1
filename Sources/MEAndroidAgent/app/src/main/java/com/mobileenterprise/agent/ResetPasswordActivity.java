package com.mobileenterprise.agent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mobileenterprise.agent.helper.DataChecker;
import com.mobileenterprise.agent.helper.NetworkChecker;

public class ResetPasswordActivity extends AppCompatActivity {

    private TextInputLayout tilEmailReset;
    private AlertDialog alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        tilEmailReset = (TextInputLayout) findViewById(R.id.tilEmailReset);
        AlertDialog.Builder builder = new AlertDialog.Builder(ResetPasswordActivity.this);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        alert = builder.create();
    }

    public void onConfirm(View view)
    {

        String email = DataChecker.isFieldEmpty(tilEmailReset, this);
        if (email==null) {
            return;
        }
        boolean network = NetworkChecker.IsNetworkAvailable(this);

        if (!DataChecker.isEmailCorrectFormat(tilEmailReset, this)) {
            return;
        }
        if (network) {
            alert.setMessage(getString(R.string.email_reset_password));
            alert.show();

            //do request on site to reset password
        } else {
            alert.setMessage(getString(R.string.no_internet));
            alert.show();
        }
    }

}
