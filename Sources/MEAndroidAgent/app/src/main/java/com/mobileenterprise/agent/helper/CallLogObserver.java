package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;

public class CallLogObserver extends ContentObserver
{
	  private Context context = null;
	  private Handler m_handle = null;

	  public CallLogObserver(Handler paramHandler, Context paramContext)
	  {
	    super(paramHandler);
	    this.context = paramContext;
	    this.m_handle = paramHandler;
	  }
	  
	  @Override
      public void onChange(boolean selfChange) {
          super.onChange(selfChange);

          // Send the on change message to your database manager.
          FindNewCall();          
      }
	  
	  private void FindNewCall()
	  {
		  String[] projection = new String[]{BaseColumns._ID, 
				  							Calls.DATE, 
				  							Calls.NUMBER, 
				  							Calls.DURATION, 
				  							Calls.TYPE,
				  							Calls.CACHED_NAME,
				  							Calls.CACHED_NUMBER_TYPE,
				  							Calls.CACHED_NUMBER_LABEL}; 
		  Cursor localCursor = this.context.getContentResolver().query(Calls.CONTENT_URI, projection, null, null, "_id DESC");
		  
		  if ((localCursor == null) || (localCursor.getCount() <= 0) || !localCursor.moveToFirst())
		  {
			  if (localCursor != null)
				  localCursor.close();
		  }
		  else
		  {	   
			  try
			  {
				  String currentID = localCursor.getString(localCursor.getColumnIndexOrThrow(Calls._ID));

				  long lastCallID = Long.parseLong(Settings.getCallId());
				  if (Long.parseLong(currentID) > lastCallID)
				  {
					  Settings.setCallId(currentID);

					  String number = 	localCursor.getString(localCursor.getColumnIndexOrThrow(Calls.NUMBER));
					  String cashedName = (null == localCursor.getString(localCursor.getColumnIndex(Calls.CACHED_NAME))) 
					  						? "" : localCursor.getString(localCursor.getColumnIndex(Calls.CACHED_NAME));
					  int cashedNumberType = (localCursor.isNull(localCursor.getColumnIndex(Calls.CACHED_NUMBER_TYPE))
							  					? -1 : localCursor.getInt(localCursor.getColumnIndex(Calls.CACHED_NUMBER_TYPE)));
					  String cashedNumberLable = (null == localCursor.getString(localCursor.getColumnIndex(Calls.CACHED_NUMBER_LABEL)))
												? "" : localCursor.getString(localCursor.getColumnIndex(Calls.CACHED_NUMBER_LABEL));
					  
					  String type = 	localCursor.getString(localCursor.getColumnIndexOrThrow(Calls.TYPE)); 
					  long date = 	localCursor.getLong(localCursor.getColumnIndexOrThrow(Calls.DATE)); 
					  int dur = 		localCursor.getInt(localCursor.getColumnIndexOrThrow(Calls.DURATION));
					  
					  Log.i(this.getClass().getName(), "cashedNumberType: " + String.valueOf(cashedNumberType) + ", cashedNumberLable: " + cashedNumberLable);
					  Log.i(this.getClass().getName(), date+":"+currentID+", "+number+", " +cashedName+", " +getNumberType(cashedNumberType, cashedNumberLable)+", "+type  +", "+dur);

					  switch (Integer.parseInt(type))
					  {
					  case Calls.INCOMING_TYPE:
						  RecordCallLog(new CallInformation(number, cashedName, getNumberType(cashedNumberType, cashedNumberLable), "Device", "", "", "Incoming", date, dur));
						  break;
					  case Calls.OUTGOING_TYPE:			        	
						  RecordCallLog(new CallInformation("Device", "", "", number, cashedName, getNumberType(cashedNumberType, cashedNumberLable),"Outgoing", date, dur));
						  break;
					  case Calls.MISSED_TYPE:			        	
						  RecordCallLog(new CallInformation(number, cashedName, getNumberType(cashedNumberType, cashedNumberLable), "Device", "", "","Missed", date, dur));
						  break;
					  default:
						  break;	
					  }
				  }
			  }
			  catch(Exception ex)
			  {
				  ex.printStackTrace();
				  CustomLogger.Write(ex, this.context);
				  Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			  }
			  catch(Throwable ex)
			  {
				  ex.printStackTrace();
				  CustomLogger.Write(ex, this.context);
				  Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
			  }
			  finally
			  {
				  localCursor.close();
			  }
		  }
	  }
	  
	  private void RecordCallLog(CallInformation callInfo)
	  {
		  DataProvider localUtils = new DataProvider(this.context);
          long id = localUtils.InsertCallLog(callInfo);
          localUtils.DBClose();
          Log.i(this.getClass().getName(),"Call Log inserted");
	  }
	  
	  private String getNumberType(int type, String customLabel)
	  {

	        switch (type)
	        {
	        case Phone.TYPE_FAX_HOME:
	            return "Home Fax";
	        case Phone.TYPE_FAX_WORK:
	            return "Work Fax";
	        case Phone.TYPE_HOME:
	            return "Home";
	        case Phone.TYPE_MOBILE:
	            return "Mobile";
	        case Phone.TYPE_OTHER:
	            return "Other";
	        case Phone.TYPE_PAGER:
	            return "Pager";
	        case Phone.TYPE_WORK:
	            return "Work";
	        case Phone.TYPE_CALLBACK:
	            return "Callback";
	        case Phone.TYPE_CAR:
	            return "Car";
	        case Phone.TYPE_COMPANY_MAIN:
	            return "Company Main";
	        case Phone.TYPE_ISDN:
	            return "ISDN";
	        case Phone.TYPE_MAIN:
	            return "Main";
	        case Phone.TYPE_OTHER_FAX:
	            return "Other Fax";
	        case Phone.TYPE_RADIO:
	            return "Radio";
	        case Phone.TYPE_TELEX:
	            return "Telex";
	        case Phone.TYPE_TTY_TDD:
	            return "TTY TDD";
	        case Phone.TYPE_WORK_MOBILE:
	            return "Work Mobile";
	        case Phone.TYPE_WORK_PAGER:
	            return "Work Pager";
	        case Phone.TYPE_ASSISTANT:
	            return "Assistant";
	        case Phone.TYPE_MMS:
	            return "MMS";
	        case Phone.TYPE_CUSTOM:
	            return customLabel;
	        default:
	            return "";                
	        }  
	  }
	  
}
