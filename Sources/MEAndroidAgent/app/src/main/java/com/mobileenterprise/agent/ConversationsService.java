package com.mobileenterprise.agent;

import android.accessibilityservice.AccessibilityService;
import android.os.AsyncTask;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.mobileenterprise.agent.conversations.AccessibilityEventProcessor;
import com.mobileenterprise.agent.conversations.Message;
import com.mobileenterprise.agent.helper.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class ConversationsService extends AccessibilityService {
    private List<AccessibilityEventProcessor> eventProcessors = new ArrayList<AccessibilityEventProcessor>();

    private class ProcessingTask extends AsyncTask<AccessibilityEvent, Void, List<Message>> {
        private final AccessibilityEventProcessor eventProcessor;

        public ProcessingTask(AccessibilityEventProcessor eventProcessor) {
            this.eventProcessor = eventProcessor;
        }

        @Override
        protected List<Message> doInBackground(AccessibilityEvent... accessibilityEvents) {
            return eventProcessor.process(accessibilityEvents[0]);
        }

        @Override
        protected void onPostExecute(List<Message> result) {
            saveMessages(result);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        Log.i("ConversationsService", "onAccessibilityEvent is called");

        CharSequence packageName = accessibilityEvent.getPackageName();

        for (final AccessibilityEventProcessor processor : eventProcessors) {
            if (processor.getPackageName().equals(packageName)) {
                new ProcessingTask(processor).execute(accessibilityEvent);
                break;
            }
        }
    }

    @Override
    public void onInterrupt() {
        // Do nothing
    }

    private synchronized void saveMessages(List<Message> messages) {
        DataProvider localDB = new DataProvider(this.getApplicationContext());
        for (Message message : messages) {
            localDB.InsertConversationMessage(message);
        }
    }
}
