package com.mobileenterprise.agent.helper;

import java.util.ArrayList;
import java.util.HashMap;

public class MMSInfoToTansfer 
{
	public MMSInfoToTansfer()
	{
		this.setMessageInfo(new HashMap<String, String>());
		this.setAttachments(new ArrayList<MMSAttachment>());
		this.setMessageID(-1);
		
	}
	public void setMessageInfo(HashMap<String, String> messageInfo) {
		this.messageInfo = messageInfo;
	}
	public HashMap<String, String> getMessageInfo() {
		return messageInfo;
	}
	public void setAttachments(ArrayList<MMSAttachment> attachments) {
		this.attachments = attachments;
	}
	public ArrayList<MMSAttachment> getAttachments() {
		return attachments;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public int getMessageID() {
		return messageID;
	}
	private HashMap<String, String> messageInfo;
	private ArrayList<MMSAttachment> attachments;
	private int messageID;
	
}
