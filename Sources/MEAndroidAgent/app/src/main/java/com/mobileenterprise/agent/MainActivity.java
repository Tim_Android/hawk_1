package com.mobileenterprise.agent;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mobileenterprise.agent.helper.Settings;

public class MainActivity extends AppCompatActivity {
    private static final int RESULT_ENABLE = 1;
    private static final int RESULT_AUTHENTICATION = 8;
    private static final int RESULT_SET_PASSWORD = 9;


    private DevicePolicyManager devicePolicyManager = null;
    private ComponentName deviceAdminComponent = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponent = new ComponentName(MainActivity.this, MEDeviceAdminReceiver.class);

        this.initSettings();
        this.initContentView();
    }

    private void initContentView() {
        if (Settings.ServiceIsRunning()) {
            enableAdmin();
            startRunningActivity();
        } else {
            if (Settings.isFirstRun()) {
                Log.i(this.getClass().getName(), "Initializing standard Activity");
                this.setContentView(R.layout.main);
                startAuthenticationActivity();
            } else {
                startAgent();
                enableAdmin();
                startRunningActivity();
            }
        }
    }

    private void initSettings() {
        Settings.openDataBase(this);
        if (Settings.getKeyStatus().equals(Settings.PARAMETER_FALSE)) {
            Log.i(this.getClass().getName(), "First run!");

            Settings.createDataBase(this);
            Settings.setKeyStatus(Settings.PARAMETER_TRUE);
        }
        // HotFix. In future it can be updated just if it's changed.
        Settings.InitDeviceInfo(this);
    }

    private void startAgent() {
        Intent serviceLauncher = new Intent(this, BackgroundService.class);
        ComponentName workerService = startService(serviceLauncher);
        if (workerService != null) {
            Log.v(this.getClass().getName(), "Service loaded by main activity!");
        }
    }

    private void enableAdmin() {
        if ((null != devicePolicyManager)) {
            if (!devicePolicyManager.isAdminActive(deviceAdminComponent)) {
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminComponent);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                        "Device admin forbids the application to uninstall.");
                startActivityForResult(intent, RESULT_ENABLE);
            }
        } else {
            Log.v(this.getClass().getName(), "devicePolicyManager is null! It's unsupported on this OS(API level < 8).");
        }

    }

    private void startRunningActivity() {
        Log.i(this.getClass().getName(), "Initializing RunningActivity because background service already is running");
        Intent activityIntent = new Intent(this, RunningActivity.class);
        startActivity(activityIntent);
        finish();
    }

    private void startSubscriptionActivivty() {

        Intent activityIntent = new Intent(this, SubscriptionActivity.class);
        startActivityForResult(activityIntent, 0);
    }

    private void startSetPasswordActivivty() {

        Intent activityIntent = new Intent(this, PasswordAppActivity.class);
        startActivityForResult(activityIntent, RESULT_SET_PASSWORD);
    }

    private void startAuthenticationActivity() {
        Log.i(this.getClass().getName(), "Initializing AuthenticationActivity");
        Intent activityIntent = new Intent(this, AuthenticationActivity.class);
        startActivityForResult(activityIntent, RESULT_AUTHENTICATION);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_ENABLE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.i("DeviceAdmin", "Admin enabled!");
                } else {
                    Log.i("DeviceAdmin", "Admin enable FAILED! Service was not Started!");
                }
                return;
            case RESULT_AUTHENTICATION: {
                if (resultCode == Activity.RESULT_OK)
                    startSetPasswordActivivty();

            }
            break;
            case RESULT_SET_PASSWORD: {
                if (resultCode == Activity.RESULT_OK) {
                    startAgent();
                    enableAdmin();

                    Settings.setFirstRun(false);
                    startSubscriptionActivivty();
                    finish();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
