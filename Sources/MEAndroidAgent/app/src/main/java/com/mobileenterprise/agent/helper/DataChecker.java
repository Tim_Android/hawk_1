package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import com.mobileenterprise.agent.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataChecker {

    public static String isFieldEmpty(EditText edit, Context context) {
        String str = edit.getText().toString();
        if (str.isEmpty()) {
            edit.setError(context.getResources().getString(R.string.empty_field));
            return null;
        }
        return str;
    }

    public static boolean isEmailCorrectFormat(EditText edit, Context context) {
        Pattern pattern = Pattern.compile("\\w+([\\+-_'\\.]?)*\\w*@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(edit.getText().toString());
        if (!matcher.matches()) {
            edit.setError(context.getResources().getString(R.string.incorrect_email_format));
            return false;
        }
        return true;
    }

    public static boolean isPasswordCorrectLength(EditText edit, Context context) {
        if (edit.getText().toString().length() < 8) {
            edit.setError(context.getResources().getString(R.string.password_length));
            return false;
        }
        return true;
    }

    public static String isFieldEmpty(TextInputLayout edit, Context context) {
        String str = edit.getEditText().getText().toString();
        if (str.isEmpty()) {
            edit.setErrorEnabled(true);
            edit.setError(context.getResources().getString(R.string.empty_field));
            return null;
        }
        edit.setErrorEnabled(false);
        return str;
    }

    public static boolean isEmailCorrectFormat(TextInputLayout edit, Context context) {
        Pattern pattern=Pattern.compile("\\w+([\\+-_'\\.]?)*\\w*@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(edit.getEditText().getText().toString());
        if (!matcher.matches()) {
            edit.setErrorEnabled(true);
            edit.setError(context.getResources().getString(R.string.incorrect_email_format));
            return false;
        }
        edit.setErrorEnabled(false);
        return true;
    }


    public static boolean isPasswordCorrectLength(TextInputLayout edit, Context context) {
        edit.setErrorEnabled(false);
        if (edit.getEditText().getText().toString().length() < 8) {
            edit.setErrorEnabled(true);
            edit.setError(context.getResources().getString(R.string.password_length));
            return false;
        }
        return true;
    }
}
