package com.mobileenterprise.agent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataChecker;
import com.mobileenterprise.agent.helper.NetworkChecker;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.TransferEngine;

public class AuthenticationActivity extends AppCompatActivity {

    private AlertDialog alert;
    private TransferEngine transfer;
    private EditText editEmail;
    private EditText editPassword;
    private final int MIN_PASSWORD_LENGTH = 8;
    private Button btnLogIn;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPasswordLogIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        TextView webRef = (TextView) findViewById(R.id.textViewGoToWebSite);
        TextView tvCreateAccount = (TextView) findViewById(R.id.tvCreateAccount);
        TextView tvResetPassword = (TextView) findViewById(R.id.tvResetPassword);

        editEmail = (EditText) findViewById(R.id.editTextEmail);
        editPassword = (EditText) findViewById(R.id.editTextPassword);
        btnLogIn = (Button) findViewById(R.id.btnLogIn);
        tilEmail = (TextInputLayout) findViewById(R.id.tilEmail);
        tilPasswordLogIn = (TextInputLayout) findViewById(R.id.tilPasswordLogIn);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                switch (textView.getId()) {
                    case R.id.textViewGoToWebSite:
                        onWebSite();
                        break;
                    case R.id.tvCreateAccount:
                        onCreateAccount();
                        break;
                    case R.id.tvResetPassword:
                        onResetPassword();
                        break;
                }
            }
        };
        createRef(webRef, clickableSpan);

        createRef(tvCreateAccount, clickableSpan);

        createRef(tvResetPassword, clickableSpan);

        AlertDialog.Builder builder = new AlertDialog.Builder(AuthenticationActivity.this);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();

        btnLogIn.setEnabled(false);


        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (DataChecker.isPasswordCorrectLength(tilPasswordLogIn, AuthenticationActivity.this) && DataChecker.isFieldEmpty(tilEmail, AuthenticationActivity.this) != null) {
                    btnLogIn.setEnabled(true);
                } else {

                    btnLogIn.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (DataChecker.isEmailCorrectFormat(tilEmail, AuthenticationActivity.this) && DataChecker.isFieldEmpty(tilPasswordLogIn, AuthenticationActivity.this) != null) {
                    btnLogIn.setEnabled(true);

                } else {
                    btnLogIn.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

    }

    private void createRef(TextView textView, ClickableSpan clickableSpan) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(textView.getText().toString());
        spannableStringBuilder.setSpan(clickableSpan, 0, textView.getText().toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableStringBuilder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onWebSite() {
        Uri address = Uri.parse(getString(R.string.site_support));
        Intent openlink = new Intent(Intent.ACTION_VIEW, address);
        startActivity(openlink);
    }

    public void onCreateAccount()
    {
        Intent activityIntent = new Intent(this, RegistrationActivity.class);
        startActivity(activityIntent);
    }

    public void onResetPassword()
    {
        Intent activityIntent = new Intent(this, ResetPasswordActivity.class);
        startActivity(activityIntent);
    }
    public void onButtonClick(View view) {
        DoRequestTask task;
        boolean network = NetworkChecker.IsNetworkAvailable(this);
        if (network) {
            transfer = new TransferEngine(this);
            task = new DoRequestTask();
            task.execute(editEmail.getText().toString(), editPassword.getText().toString());
        } else {
            alert.setMessage(getString(R.string.no_internet));
            alert.show();
        }
    }

    class DoRequestTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            String answer;
            answer = transfer.GetAgentID(params[0], params[1]);
            if (answer.isEmpty()) {
                return null;
            }
            return Common.parseServerResponse(answer, getApplicationContext());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result == null) {
                alert.setMessage(getString(R.string.error_request));
                alert.show();
                return;
            }
            if (Integer.parseInt(result) == 0) {
                alert.setMessage(getString(R.string.incorrect_creds));
                alert.show();
                return;
            }
            if (Integer.parseInt(result) == 2) {
                alert.setMessage(getString(R.string.internal_error));
                alert.show();
                return;
            }
            //save in prefs
            Settings.setAgentId(result);
            //close authentication activity
            System.out.println("in onPostExecute agentID=" + result);
            Intent answerIntent = new Intent();
            setResult(RESULT_OK, answerIntent);
            finish();
        }
    }

}
