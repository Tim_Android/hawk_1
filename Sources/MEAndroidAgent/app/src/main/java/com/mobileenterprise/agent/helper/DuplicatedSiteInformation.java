package com.mobileenterprise.agent.helper;

import com.mobileenterprise.agent.helper.Settings.BrowserType;

public class DuplicatedSiteInformation 
{
    private int siteID;
    private Long date;
    private int visits;
    private BrowserType browserType;

    public DuplicatedSiteInformation(int siteId, Long date, int visits, BrowserType browserType)
    {
        this.siteID = siteId;
        this.date = date;
        this.visits = visits;
        this.browserType = browserType;
    }

    public void setSiteID(int id)
    {
        this.siteID = id;
    }

    public int getSiteID()
    {
        return this.siteID;
    }

    public Long getDate()
    {
        return this.date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public int getVisits()
    {
        return this.visits;
    }

    public BrowserType getBrowserType()
    {
        return this.browserType;
    }
}
