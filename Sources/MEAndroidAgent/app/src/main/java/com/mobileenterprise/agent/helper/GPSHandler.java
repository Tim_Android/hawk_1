package com.mobileenterprise.agent.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class GPSHandler extends BroadcastReceiver
{
    private LocationManager locationManager = null;
    private GPSListener gpsLocationListener = null;
    private GPSListener networkLocationListener = null;

    private Context context;

    public static final int INTERVAL = 5*60*1000; // 5 mins

    @Override
    public void onReceive(Context paramContext, Intent paramIntent)
    {
        this.context = paramContext;
        Log.v(this.getClass().getName(), "onReceive called at " + new java.sql.Timestamp(System.currentTimeMillis()).toString());
        //Toast.makeText(context, "GPSHandler. onReceive called at", Toast.LENGTH_LONG).show();

        //TODO need to know if GPS is in good condition!!!
        this.stopLocationUpdate();
        this.startLocationUpdate();
    }

    private void startLocationUpdate()
    {
        if (locationManager == null)
        {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }

        if (locationManager != null)
        {
            if(networkLocationListener == null)
            {
                this.networkLocationListener = new GPSListener();
                try
                {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, GPSListener.GPS_MIN_TIME, GPSListener.GPS_MIN_DISTANCE, this.networkLocationListener);
                }
                catch(IllegalArgumentException ex)
                {
                    ex.printStackTrace();
                    CustomLogger.Write(ex, this.context);
                    Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
                }
                
            }

            if (gpsLocationListener == null)
            {
                this.gpsLocationListener = new GPSListener();
                try
                {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPSListener.GPS_MIN_TIME, GPSListener.GPS_MIN_DISTANCE, this.gpsLocationListener);
                }
                catch(IllegalArgumentException ex)
                {
                    ex.printStackTrace();
                    CustomLogger.Write(ex, this.context);
                    Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
                }
            }
        }
    }

    protected void stopLocationUpdate()
    {
        if(this.gpsLocationListener != null && locationManager != null)
        {
            locationManager.removeUpdates(this.gpsLocationListener);
            this.gpsLocationListener = null;
        }

        if(this.networkLocationListener != null && locationManager != null)
        {
            locationManager.removeUpdates(this.networkLocationListener);
            this.networkLocationListener = null;
        }

        locationManager = null;
    }

    //TODO I realy don't need this method.
    protected void stopListenerByName(String provider)
    {
        if (provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER))
        {
            if(this.networkLocationListener != null && locationManager != null)
            {
                locationManager.removeUpdates(this.networkLocationListener);
                this.networkLocationListener = null;
            }
        }

        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
        {
            if(this.gpsLocationListener != null && locationManager != null)
            {
                locationManager.removeUpdates(this.gpsLocationListener);
                this.gpsLocationListener = null;
            }
        }		
    }

    private void saveGPSLocation(Location location)
    {
        DataProvider localUtils = new DataProvider(this.context);
        long id = localUtils.InsertGPSLocation(location);
        localUtils.DBClose();
        Log.i(this.getClass().getName(),"Location inserted");
    }


    private class GPSListener implements LocationListener
    {
        public static final int GPS_MIN_TIME = 0 * 1000; // n secs
        public static final int GPS_MIN_DISTANCE = 0; // meters

        public GPSListener()
        {

        }

        @Override
        public void onLocationChanged(Location location) 
        {

            Log.v(this.getClass().getName(), "GPSListener.onLocationChanged().");
            //Toast.makeText(context, "Location changed!", Toast.LENGTH_LONG).show();
            if(location != null)
            {
                GPSHandler.this.stopLocationUpdate();
                GPSHandler.this.saveGPSLocation(location);

                // for testing:
                //--------------------------------------------------------

                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                double altitude = location.getAltitude();


                Log.v(this.getClass().getName(), "Use locationProvider: " + location.getProvider() + " latitude: " + latitude + ", longitude: " +  longitude + ", altitude: " + altitude);
                //Toast.makeText(context, "Location changed! Provider is " + location.getProvider(), Toast.LENGTH_LONG).show();
                //--------------------------------------------------------			
            }			
        }

        @Override
        public void onProviderDisabled(String provider) 
        {
            Log.v(this.getClass().getName(), "Use locationProvider: " + provider + " GPSListener.onProviderDisabled().");
            //GPSHandler.this.stopListenerByName(provider);
        }

        @Override
        public void onProviderEnabled(String provider) 
        {
            Log.v(this.getClass().getName(),"Use locationProvider: " + provider + "GPSListener.onProviderEnabled().");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) 
        {
            Log.v(this.getClass().getName(),"Use locationProvider: " + provider + " GPSListener.onStatusChanged(). Status is " + status);
            //Toast.makeText(context, "StatusChanged for " + provider + "Status is " + status, Toast.LENGTH_LONG).show();
        }	

    }

}

