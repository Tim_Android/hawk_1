package com.mobileenterprise.agent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.mobileenterprise.agent.helper.DataChecker;
import com.mobileenterprise.agent.helper.Settings;

public class SettingsProtectionActivity extends AppCompatActivity {

    private EditText edtxtPassword;
    private TextInputLayout tilPassword;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_protection);

        edtxtPassword = (EditText) findViewById(R.id.edit_text_password);
        tilPassword = (TextInputLayout) findViewById(R.id.tilPasswordSettings);

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsProtectionActivity.this);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = builder.create();
    }

    public void onClick(View view) {
        String password = DataChecker.isFieldEmpty(tilPassword, this);
        if (password == null) {
            return;
        }
        if (password.equals(Settings.getPassword())) {
            Intent intent = new Intent(this, PasswordProtectionService.class);
            intent.setAction(PasswordProtectionService.ACTION_CORRECT_PASSWORD);
            startService(intent);
            finish();
        } else {
            edtxtPassword.setText("");
            alert.setMessage(getString(R.string.incorrect_password));
            alert.show();
        }
    }

}
