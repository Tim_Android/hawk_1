package com.mobileenterprise.agent.conversations;


import android.view.accessibility.AccessibilityEvent;

import java.util.List;

public interface AccessibilityEventProcessor {
    String getPackageName();
    List<Message> process(AccessibilityEvent event);
}
