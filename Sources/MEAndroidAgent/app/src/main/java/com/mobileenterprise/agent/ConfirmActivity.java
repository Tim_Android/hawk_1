package com.mobileenterprise.agent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
    }

    public void backToLogIn(View view)
    {
        Intent answerIntent = new Intent();
        setResult(RESULT_OK, answerIntent);
        finish();
    }
}
