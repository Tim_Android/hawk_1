package com.mobileenterprise.agent;

import com.mobileenterprise.agent.helper.CustomLogger;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class DateTimeReceiver extends BroadcastReceiver 
{
    @Override 
    public void onReceive(final Context context, Intent intent) 
    {
        try
        {
            Log.i(this.getClass().getName(), "DateTime on device has been changed");
            Toast.makeText(context, "DateTime on device has been changed", Toast.LENGTH_SHORT).show();

            Long currentTimestamp = (Long)(System.currentTimeMillis());

            setNewCurrentDate(BrowserType.ChromeBrowser, currentTimestamp);
            setNewCurrentDate(BrowserType.DefaultBrowser, currentTimestamp);

            Intent serviceLauncher = new Intent(context, DateChangeService.class);
            ComponentName workerService = context.startService(serviceLauncher);
            if(workerService != null)
            {
                Log.v(this.getClass().getName(), "Service for skiplist creation has been started.");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CustomLogger.Write(ex, context);
            Log.e(this.getClass().getName(), "Error! " + ex.getMessage());
        }
    }

    private void setNewCurrentDate(BrowserType browserType, Long timestamp)
    {
        if (Settings.getBrowserDate(browserType) > timestamp)
        {
            Settings.setBrowserDate(browserType, timestamp);
            Settings.setBrowserActualDataStatus(browserType, false);
        }
    }
}
