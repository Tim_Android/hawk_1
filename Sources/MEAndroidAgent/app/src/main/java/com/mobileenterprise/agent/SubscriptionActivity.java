package com.mobileenterprise.agent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.mobileenterprise.agent.helper.Settings;

public class SubscriptionActivity extends AppCompatActivity {
    private TextView tvStep3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        tvStep3 = (TextView) findViewById(R.id.tvStep3);
        ClickableSpan clickableSpanWebsite = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                onRefClick(Settings.getWebAddress());
            }
        };
        ClickableSpan clickableSpanBuySubscription = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                onRefClick(getString(R.string.buy_subscription));
            }
        };
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(getString(R.string.step_3_text));
        setRefInText(spannableStringBuilder, getString(R.string.website_string), clickableSpanWebsite);
        setRefInText(spannableStringBuilder, getString(R.string.buy_a_subscription_string), clickableSpanBuySubscription);
    }

    private void setRefInText(SpannableStringBuilder spannableStringBuilder, String text, ClickableSpan clickableSpan) {
        int index = getString(R.string.step_3_text).lastIndexOf(text);
        spannableStringBuilder.setSpan(clickableSpan, index, index + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvStep3.setText(spannableStringBuilder);
        tvStep3.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onContinueClick(View view) {
        finish();
    }

    private void onRefClick(String ref)
    {
        Uri address = Uri.parse(ref);
        Intent openlink = new Intent(Intent.ACTION_VIEW, address);
        startActivity(openlink);
    }

}
