package com.mobileenterprise.agent.helper;

public class BookmarkInformation 
{
    private String url;
    private String title;

    public BookmarkInformation(String url, String title)
    {
        //Bookmark url can be null. For example bookmarks folder
        if(url == null)
        {
            url = "";
        }

        if(title == null)
        {
            title = "";
        }

        this.url = url;
        this.title = title;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getTitle()
    {
        return this.title;
    }

    @Override 
    public boolean equals(Object object) 
    {
        if (object != null && object instanceof BookmarkInformation)
        {
            BookmarkInformation bookmark = (BookmarkInformation)object;
            return bookmark.getTitle().equals(this.title) && bookmark.getUrl().equals(this.url);
        }
        return false;
    } 
}
