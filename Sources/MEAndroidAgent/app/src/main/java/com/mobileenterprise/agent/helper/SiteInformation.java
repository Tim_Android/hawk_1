package com.mobileenterprise.agent.helper;

public class SiteInformation extends BookmarkInformation 
{
    private Long date;
    private int visits;
    private boolean dateValidState = true;

    public SiteInformation(String url, String title, Long date, int visits)
    {
        super(url, title);
        this.date = date;
        this.visits = visits;
    }

    public Long getDate()
    {
        return this.date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public int getVisits()
    {
        return this.visits;
    }

    public void setDateValidState(boolean state)
    {
        this.dateValidState = state;
    }

    public boolean dateIsValid()
    {
        return this.dateValidState;
    }
}
