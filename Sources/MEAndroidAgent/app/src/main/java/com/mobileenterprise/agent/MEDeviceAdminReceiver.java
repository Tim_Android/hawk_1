package com.mobileenterprise.agent;

import com.mobileenterprise.agent.helper.Settings;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

public class MEDeviceAdminReceiver extends DeviceAdminReceiver
{
    static SharedPreferences getSamplePreferences(Context context) 
    {
        return context.getSharedPreferences(MEDeviceAdminReceiver.class.getName(), 0);
    }
    
    void showToast(Context context, CharSequence msg) 
    {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    
    public void onEnabled(Context context, Intent intent) 
    {
        showToast(context, "Device Admin: enabled");
        super.onEnabled(context, intent);
    }
    
    public void onDisabled(Context paramContext, Intent intent) 
    {
        showToast(paramContext, "Device Admin: disabled");
        super.onDisabled(paramContext, intent);
    }
    
    public CharSequence onDisableRequested(Context paramContext, Intent paramIntent)
    {
        ComponentName localComponentName = new ComponentName(paramContext, MEDeviceAdminReceiver.class);

        DevicePolicyManager localDevicePolicyManager = (DevicePolicyManager)paramContext.getSystemService(Context.DEVICE_POLICY_SERVICE );
        if (localDevicePolicyManager.isAdminActive(localComponentName))
        {
            localDevicePolicyManager.setPasswordQuality(localComponentName, DevicePolicyManager.PASSWORD_QUALITY_NUMERIC);
            localDevicePolicyManager.setPasswordMinimumLength(localComponentName, Settings.MIN_PASSWORD_LENGTH);
        }

        String pwd = Settings.getPassword();
        Log.v(this.getClass().getName(), "Password = " + pwd.toString());
        localDevicePolicyManager.resetPassword(pwd, DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);

        localDevicePolicyManager.lockNow();
        
        return super.onDisableRequested(paramContext, paramIntent);            
    }  

}



