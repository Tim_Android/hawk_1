package com.mobileenterprise.agent.mocks;

import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;

public class MockStatusLine implements StatusLine {
    private ProtocolVersion protocolVersion;
    private int statusCode = 0;
    private String reason = null;

    public MockStatusLine(ProtocolVersion protocolVersion, int statusCode, String reason) {
        this.protocolVersion = protocolVersion;
        this.statusCode = statusCode;
        this.reason = reason;
    }

    @Override
    public ProtocolVersion getProtocolVersion() {
        return this.protocolVersion;
    }

    @Override
    public int getStatusCode() {
        return this.statusCode;
    }

    @Override
    public String getReasonPhrase() {
        return this.reason;
    }
}
