package com.mobileenterprise.agent.test;

import java.util.Date;
import com.mobileenterprise.agent.helper.MessageInformation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageInformationTest
{
    @Test
    public void testNeedSend_Read_RecieveCheck()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x87);

        assertEquals(false, mms.needSend());
    }

    @Test
    public void testNeedSend_NOTIFICATION()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x82);

        assertEquals(false, mms.needSend());
    }

    @Test
    public void testNeedSend_Another()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115", 
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            null,
            0x80);

        assertEquals(true, mms.needSend());
    }
}
