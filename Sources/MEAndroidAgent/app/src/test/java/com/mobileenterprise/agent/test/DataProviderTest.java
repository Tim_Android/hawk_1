package com.mobileenterprise.agent.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.message.BasicHttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.Returns;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mobileenterprise.agent.conversations.Message;
import com.mobileenterprise.agent.helper.BookmarkInformation;
import com.mobileenterprise.agent.helper.CallInformation;
import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.DuplicatedSiteInformation;
import com.mobileenterprise.agent.helper.MMSAttachment;
import com.mobileenterprise.agent.helper.MessageInformation;
import com.mobileenterprise.agent.helper.ResendStruct;
import com.mobileenterprise.agent.helper.RestEasy;
import com.mobileenterprise.agent.helper.SQLiteHelper;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import com.mobileenterprise.agent.helper.SiteInformation;
import com.mobileenterprise.agent.mocks.MockCursor;
import com.mobileenterprise.agent.mocks.MockHttpResponse;
import com.mobileenterprise.agent.mocks.MockStatusLine;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.test.AndroidTestCase;
import android.test.AssertionFailedError;
import android.test.RenamingDelegatingContext;
import android.test.MoreAsserts;
import android.util.Log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyFloat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Matchers.notNull;
import static org.mockito.Matchers.refEq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.constructor;
import static org.powermock.api.support.membermodification.MemberMatcher.constructorsDeclaredIn;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataProvider.class, Settings.class, ContentValues.class, RestEasy.class, Log.class})
public class DataProviderTest
{
    private class LocationData
    {
        public  LocationData(String providerName, float accuracy, float bearing, float speed, double latitude, double longitude, double altitude, long time)
        {
            ProviderName = providerName;
            Accuracy = accuracy;
            Bearing = bearing;
            Speed = speed;
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            Time = time;
        }

        public String ProviderName;
        float Accuracy;
        float Bearing;
        float Speed;
        double Latitude;
        double Longitude;
        double Altitude;
        long Time;
    };

    private class MockDatabaseHelper
    {
        public MockDatabaseHelper(final String tableName, final String[] captions, final ArrayList<Object[]> data)
        {
            this.sqlTableName = tableName;
            this.tableCaptions = captions;
            this.dataToInsert = data;

            // Query table conent
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    final MockCursor mockCursor = new MockCursor(tableCaptions);

                    for (Object[] row : insertedData.values())
                    {
                        mockCursor.addRow(row);
                    }
                    return mockCursor;
                }
            }).when(mockDB).query(eq(sqlTableName), (String[])isNull(), (String)isNull(), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

            // Delete by row ID
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[0];
                    statement = statement.replaceAll("\\s+","");

                    String prefix = "deletefrom" + sqlTableName + "where_id=";
                    if (statement.startsWith(prefix))
                    {
                        Integer id = Integer.parseInt(statement.substring(prefix.length()));
                        insertedData.remove(id);
                    }

                    return null;
                }
            }).when(mockDB).execSQL(anyString());

            // Delete all
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    insertedData.clear();
                    return null;
                }
            }).when(mockDB).execSQL(eq("delete from " + sqlTableName));

            // Insert row
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    if (dataToInsert == null)
                    {
                        return -1;
                    }

                    int nextValue = nextId.getAndIncrement();
                    ArrayList<Object> data = new ArrayList<Object>();
                    data.add(nextValue);
                    for (Object obj : dataToInsert.get(nextValue - 1))
                    {
                        data.add(obj);
                    }
                    insertedData.put(nextValue, data.toArray());
                    return nextValue;
                }
            }).when(mockDB).insert(eq(sqlTableName), (String)isNull(), refEq(mockContentValues));
        }

        protected final AtomicInteger nextId = new AtomicInteger(1);
        protected final HashMap<Integer, Object[]> insertedData = new HashMap<Integer, Object[]>();
        protected final String sqlTableName;
        protected final ArrayList<Object[]> dataToInsert;
        protected final String[] tableCaptions;
    };

    private Context mockContext;
    private SQLiteHelper mockHelper;
    private SQLiteDatabase mockDB;
    private ContentValues mockContentValues;

    @Before
    public void initialize()
    {
        this.mockContext = mock(Context.class);
        this.mockHelper = mock(SQLiteHelper.class);
        this.mockDB = mock(SQLiteDatabase.class);
        this.mockContentValues = mock(ContentValues.class);

        PowerMockito.mockStatic(RestEasy.class);
        PowerMockito.mockStatic(Log.class);

        try {
            PowerMockito.whenNew(SQLiteHelper.class).withArguments(this.mockContext).thenReturn(mockHelper);
            PowerMockito.whenNew(ContentValues.class).withAnyArguments().thenReturn(mockContentValues);
            when(this.mockHelper.getWritableDatabase()).thenReturn(this.mockDB);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testFakeBinary()
    {
        try
        {
            ProtocolVersion mockProtocolVersion = mock(ProtocolVersion.class);

            TelephonyManager mockTelephonyManager = PowerMockito.mock(TelephonyManager.class);

            when(mockHelper.getWritableDatabase()).thenReturn(mockDB);

            when(this.mockContext.getSystemService(Context.TELEPHONY_SERVICE)).thenReturn(mockTelephonyManager);
            when(mockTelephonyManager.getDeviceId()).thenReturn("Mock Device ID");

            HttpResponse response = new MockHttpResponse();
            response.setStatusLine(new MockStatusLine(mockProtocolVersion, 404, ""));

            PowerMockito.when(RestEasy.class, "doBinaryPostFake", any(String.class), any(String.class), refEq(this.mockContext)).thenReturn(response);

            DataProvider localDB = new DataProvider(this.mockContext);
            localDB.SendFakeBinary(this.mockContext);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testGPSLogs() throws JSONException
    {
        try
        {
            final LocationData locData = new LocationData(
                    LocationManager.NETWORK_PROVIDER, 10f, 47.25f, 40f, 47.4356231312, 54.2132434, 10.4356, new Date().getTime());

            String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.GPS_COLUMN_PROVIDER, SQLiteHelper.GPS_COLUMN_ACCURACY, SQLiteHelper.GPS_COLUMN_BEARING,
                    SQLiteHelper.GPS_COLUMN_SPEED, SQLiteHelper.GPS_COLUMN_LATITUDE, SQLiteHelper.GPS_COLUMN_LONGITUDE, SQLiteHelper.GPS_COLUMN_ALTITUDE, SQLiteHelper.GPS_COLUMN_TIME};
            ArrayList<Object[]> data = new ArrayList<Object[]>();
            data.add(new Object[] {locData.ProviderName, locData.Accuracy, locData.Bearing, locData.Speed, locData.Latitude, locData.Longitude, locData.Altitude, locData.Time});
            MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.GPSTABLE_NAME, captions, data);

            Location mockLocation = mock(Location.class);

            when(mockLocation.getProvider()).thenReturn(locData.ProviderName);
            when(mockLocation.getAccuracy()).thenReturn(locData.Accuracy);
            when(mockLocation.getBearing()).thenReturn(locData.Bearing);
            when(mockLocation.getSpeed()).thenReturn(locData.Speed);
            when(mockLocation.getLatitude()).thenReturn(locData.Latitude);
            when(mockLocation.getLongitude()).thenReturn(locData.Longitude);
            when(mockLocation.getAltitude()).thenReturn(locData.Altitude);
            when(mockLocation.getTime()).thenReturn(locData.Time);

            DataProvider localDB = new DataProvider(this.mockContext);
            assertEquals(localDB.InsertGPSLocation(mockLocation), 1);

            // Check ----------------------------- GetGPSLogsToSend------------------------------

            final String featureName = DataProvider.GPS_LOG;
            ArrayList<ResendStruct> gpsLogsList = new ArrayList<ResendStruct>();
            localDB.GetLogsToSend(gpsLogsList, featureName);

            assertEquals(1, gpsLogsList.size());
            ResendStruct gpsLogs = gpsLogsList.get(0);

            ArrayList<Integer> idsToRemove = gpsLogs.getIdsToRemove();

            assertEquals(1, idsToRemove.size());
            assertEquals(1, (int)idsToRemove.get(0));

            JSONObject dataArray = gpsLogs.getDataArray();

            JSONArray jsonArray = dataArray.getJSONArray(featureName);

            JSONObject json_data = jsonArray.getJSONObject(0);

            double delta = 0.00001;
            Assert.assertEquals(locData.Accuracy, json_data.getDouble(SQLiteHelper.GPS_COLUMN_ACCURACY), delta);
            Assert.assertEquals(locData.Altitude,json_data.getDouble(SQLiteHelper.GPS_COLUMN_ALTITUDE), delta);
            Assert.assertEquals(locData.Bearing, json_data.getDouble(SQLiteHelper.GPS_COLUMN_BEARING), delta);
            Assert.assertEquals(locData.Speed, json_data.getDouble(SQLiteHelper.GPS_COLUMN_SPEED), delta);
            Assert.assertEquals(locData.Latitude, json_data.getDouble(SQLiteHelper.GPS_COLUMN_LATITUDE), delta);
            Assert.assertEquals(locData.Longitude, json_data.getDouble(SQLiteHelper.GPS_COLUMN_LONGITUDE), delta);
            Assert.assertEquals(locData.ProviderName, json_data.getString(SQLiteHelper.GPS_COLUMN_PROVIDER));
            Assert.assertEquals("/Date(" + Long.toString(locData.Time)+ ")/",json_data.getString(SQLiteHelper.GPS_COLUMN_TIME));

            // finish ----------------------------- GetGPSLogsToSend-----------------------------

            assertEquals(idsToRemove.size(), localDB.GetGPSLogsCount());

            localDB.DeleteGPSLogs(idsToRemove);

            assertEquals(0, localDB.GetGPSLogsCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteAllGPSLogs()
    {
        try
        {
            final LocationData locData = new LocationData(
                    LocationManager.NETWORK_PROVIDER, 10f, 47.25f, 40f, 47.4356231312, 54.2132434, 10.4356, new Date().getTime());

            Location mockLocation = mock(Location.class);

            String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.GPS_COLUMN_PROVIDER, SQLiteHelper.GPS_COLUMN_ACCURACY, SQLiteHelper.GPS_COLUMN_BEARING,
                    SQLiteHelper.GPS_COLUMN_SPEED, SQLiteHelper.GPS_COLUMN_LATITUDE, SQLiteHelper.GPS_COLUMN_LONGITUDE, SQLiteHelper.GPS_COLUMN_ALTITUDE, SQLiteHelper.GPS_COLUMN_TIME};

            ArrayList<Object[]> data = new ArrayList<Object[]>();
            data.add(new Object[] {locData.ProviderName, locData.Accuracy, locData.Bearing, locData.Speed, locData.Latitude,
                    locData.Longitude, locData.Altitude, locData.Time});
            data.add(new Object[] {locData.ProviderName, locData.Accuracy, locData.Bearing, locData.Speed, locData.Latitude,
                    locData.Longitude, locData.Altitude, locData.Time});

            MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.GPSTABLE_NAME, captions, data);

            when(mockLocation.getProvider()).thenReturn(locData.ProviderName);
            when(mockLocation.getAccuracy()).thenReturn(locData.Accuracy);
            when(mockLocation.getBearing()).thenReturn(locData.Bearing);
            when(mockLocation.getSpeed()).thenReturn(locData.Speed);
            when(mockLocation.getLatitude()).thenReturn(locData.Latitude);
            when(mockLocation.getLongitude()).thenReturn(locData.Longitude);
            when(mockLocation.getAltitude()).thenReturn(locData.Altitude);
            when(mockLocation.getTime()).thenReturn(locData.Time);

            DataProvider localDB = new DataProvider(this.mockContext);

            assertEquals(1, localDB.InsertGPSLocation(mockLocation));
            assertEquals(2, localDB.InsertGPSLocation(mockLocation));

            assertTrue(localDB.GetGPSLogsCount() > 0);

            localDB.DeleteAllGPSLogs();

            assertEquals(0, localDB.GetGPSLogsCount());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testGPSLogsEmpty()
    {	 
        final String featureName = DataProvider.GPS_LOG;
        ArrayList<ResendStruct> gpsLogsList = new ArrayList<ResendStruct>();

        when(this.mockDB.query(eq(SQLiteHelper.GPSTABLE_NAME), (String[])isNull(), (String)isNull(), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull())).thenReturn(new MockCursor());

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(gpsLogsList, featureName);

        assertEquals(true, gpsLogsList.isEmpty());

    }

    @Test
    public void testGPSLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteGPSLogs(new ArrayList<Integer>());
    }

    @Test (expected = NullPointerException.class)
    public void testGPSLogsBadCase()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertGPSLocation(null);
    }

    @Test
    public void testCallLogs() throws JSONException
    {
        final CallInformation callInfo = new CallInformation("+38050334565",
                "Stephen King",
                "Mobile",
                "Device",
                "",
                "",
                "Incoming",
                new Date().getTime(),
                33);

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_fromNumber", "_fromNumberName", "_fromNumberType", "_toNumber", "_toNumberName", "_toNumberType", "_type", "_date", "_duration"};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {callInfo.getFromNumber(), callInfo.getFromNumberName(), callInfo.getFromNumberType(), callInfo.getToNumber(),
                callInfo.getToNumberName(), callInfo.getToNumberType(), callInfo.getType(), callInfo.getDate(), callInfo.getDuration()});
        data.add(new Object[] {callInfo.getFromNumber(), callInfo.getFromNumberName(), callInfo.getFromNumberType(), callInfo.getToNumber(),
                callInfo.getToNumberName(), callInfo.getToNumberType(), callInfo.getType(), callInfo.getDate(), callInfo.getDuration()});

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CALLLOGSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertCallLog(callInfo));
        assertEquals(2, localDB.InsertCallLog(callInfo));

        final String featureName = DataProvider.CALL_LOG;
        ArrayList<ResendStruct> callLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(callLogsList, featureName);

        // Check ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(1, callLogsList.size());
        ResendStruct callLogs = callLogsList.get(0);

        ArrayList<Integer> idsToRemove = callLogs.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = callLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);
        assertEquals(callInfo.getFromNumber(),json_data.getString("fromNumber"));
        assertEquals(callInfo.getFromNumberName(),json_data.getString("fromNumberName"));
        assertEquals(callInfo.getFromNumberType(),json_data.getString("fromNumberType"));
        assertEquals(callInfo.getToNumber(),json_data.getString("toNumber"));
        assertEquals(callInfo.getToNumberName(),json_data.getString("toNumberName"));
        assertEquals(callInfo.getToNumberType(),json_data.getString("toNumberType"));
        assertEquals(callInfo.getType(),json_data.getString("type"));
        assertEquals("/Date(" + Long.toString(callInfo.getDate())+ ")/",json_data.getString("date"));
        assertEquals(callInfo.getDuration(),json_data.getInt("duration"));

        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetCallLogsCount());

        localDB.DeleteCallLogs(idsToRemove);

        assertEquals(0, localDB.GetCallLogsCount());
    }

    @Test
    public void testDeleteAllCallLogs()
    {
        final CallInformation callInfo = new CallInformation("+38050334565",
                "Stephen King",
                "Mobile",
                "Device",
                "",
                "",
                "Incoming",
                new Date().getTime(),
                33);

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_fromNumber", "_fromNumberName", "_fromNumberType", "_toNumber", "_toNumberName", "_toNumberType", "_type", "_date", "_duration"};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {callInfo.getFromNumber(), callInfo.getFromNumberName(), callInfo.getFromNumberType(), callInfo.getToNumber(),
                callInfo.getToNumberName(), callInfo.getToNumberType(), callInfo.getType(), callInfo.getDate(), callInfo.getDuration()});
        data.add(new Object[] {callInfo.getFromNumber(), callInfo.getFromNumberName(), callInfo.getFromNumberType(), callInfo.getToNumber(),
                callInfo.getToNumberName(), callInfo.getToNumberType(), callInfo.getType(), callInfo.getDate(), callInfo.getDuration()});

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CALLLOGSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertCallLog(callInfo));
        assertEquals(2, localDB.InsertCallLog(callInfo));


        assertEquals(true, localDB.GetCallLogsCount() > 0);

        localDB.DeleteAllCallLogs();

        assertEquals(0, localDB.GetCallLogsCount());
    }

    @Test
    public void testCallLogsEmpty()
    {	 
        final String featureName = DataProvider.CALL_LOG;
        ArrayList<ResendStruct> callLogsList = new ArrayList<ResendStruct>();

        when(this.mockDB.query(eq(SQLiteHelper.CALLLOGSTABLE_NAME), (String[])isNull(), (String)isNull(), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull())).thenReturn(new MockCursor());

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(callLogsList, featureName);

        assertEquals(true, callLogsList.isEmpty());

    }

    @Test
    public void testCallLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteCallLogs(new ArrayList<Integer>());
    }

    @Test(expected = NullPointerException.class)
    public void testCallLogsBadCase()
    {
        CallInformation callInfo = null;
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertCallLog(null);

        Assert.fail("Should have thrown NullPointerException");
    }

    @Test
    public void testCrashLogsLargeData() throws JSONException
    {
        final long date = System.currentTimeMillis();
        final String crashReport = getTestCrashReport();

        int crashReportsCount = (int)Math.ceil((double)DataProvider.TRANSFER_MAX_SIZE / crashReport.length());

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_date", "_data"};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        for (int i = 0; i < crashReportsCount; ++i)
        {
            data.add(new Object[] { date, crashReport});
        }

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CRASHLOGSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        for (int i = 0; i < crashReportsCount; i++)
        {
            long id = localDB.InsertCrashLog(date, crashReport);
        }

        StringBuilder sb = new StringBuilder("/Date(");
        sb.append(Long.toString(date));
        sb.append(")/");

        // Check ----------------------------- GetCrashLogsToSend-----------------------------
        final String featureName = DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(2, crashLogsList.size());
        ResendStruct crashLogs = crashLogsList.get(0);

        ArrayList<Integer> idsToRemove = crashLogs.getIdsToRemove();

        assertEquals(29, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));		

        JSONObject dataArray = crashLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);
        assertEquals(sb.toString(),json_data.getString("date"));
        assertEquals(crashReport,json_data.getString("data"));


        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        int logsCount = 0;

        for (ResendStruct crashLog : crashLogsList) 
        {
            logsCount += crashLog.getIdsToRemove().size();
        }

        assertEquals(logsCount, localDB.GetCrashLogsCount());

        for (ResendStruct crashLog : crashLogsList) 
        {
            localDB.DeleteCrashLogs(crashLog.getIdsToRemove());
        }

        assertEquals(0, localDB.GetCrashLogsCount());

    }

    @Test
    public void testCrashLogs() throws JSONException
    {
        final String crashReport = getTestCrashReport();
        final long date = System.currentTimeMillis();

        StringBuilder sb = new StringBuilder("/Date(");
        sb.append(Long.toString(date));
        sb.append(")/");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_date", "_data"};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] { date, crashReport});
        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CRASHLOGSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);
        long id = localDB.InsertCrashLog(date, crashReport);

        assertEquals(1, id);

        // Check ----------------------------- GetCrashLogsToSend-----------------------------
        final String featureName =DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(1, crashLogsList.size());
        ResendStruct crashLogs = crashLogsList.get(0);

        ArrayList<Integer> idsToRemove = crashLogs.getIdsToRemove();

        assertEquals(1, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));		

        JSONObject dataArray = crashLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(sb.toString(),json_data.getString("date"));
        assertEquals(crashReport,json_data.getString("data"));


        // Finish ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetCrashLogsCount());

        localDB.DeleteCrashLogs(idsToRemove);

        assertEquals(0, localDB.GetCrashLogsCount());
    }

    @Test
    public void testDeleteAllCrashLogs()
    {
        final String crashReport = getTestCrashReport();
        final long date = System.currentTimeMillis();

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_date", "_data"};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] { date, crashReport});

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CRASHLOGSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);
        long id = localDB.InsertCrashLog(System.currentTimeMillis(), crashReport);

        assertEquals(1, id);

        assertEquals(true, localDB.GetCrashLogsCount() > 0);

        localDB.DeleteAllCrashLogs();

        assertEquals(0, localDB.GetCrashLogsCount());
    }

    @Test
    public void testCrashLogsEmpty()
    {	 
        final String featureName = DataProvider.CRASH_LOG;
        ArrayList<ResendStruct> crashLogsList = new ArrayList<ResendStruct>();

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(crashLogsList, featureName);

        assertEquals(true, crashLogsList.isEmpty());

    }

    @Test
    public void testCrashLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteCrashLogs(new ArrayList<Integer>());
    }

    @Test
    public void testCrashLogsBadCase()
    {
        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, "_date", "_data"};
        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.CRASHLOGSTABLE_NAME, captions, null);

        DataProvider localDB = new DataProvider(this.mockContext);
        long id = localDB.InsertCrashLog(0,null);
        assertEquals(-1, id);
    }

    @Test
    public void testSMSLogs() throws JSONException
    {
        Date date = new Date();
        MessageInformation sms = new MessageInformation("+380502341115", 
            "Test sms Body", 
            "Inbox", 
            date.getTime(), 
            "", 
            null,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        sms.setContactName("contact name");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_ADRESS, SQLiteHelper.MT_COLUMN_CONTACT_NAME, SQLiteHelper.MT_COLUMN_BODY, SQLiteHelper.MT_COLUMN_TYPE,
                SQLiteHelper.MT_COLUMN_DATE};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] { sms.getAddress(), sms.getContactName(), sms.getBody(), sms.getType(), sms.getDate() });
        data.add(new Object[] { sms.getAddress(), sms.getContactName(), sms.getBody(), sms.getType(), sms.getDate() });

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.SMSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);
        assertEquals(1, localDB.InsertSMS(sms));
        assertEquals(2, localDB.InsertSMS(sms));

        // Check ----------------------------- GetSMSLogsToSend-----------------------------

        final String featureName = DataProvider.SMS_LOG;
        ArrayList<ResendStruct> smsLogsList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(smsLogsList, featureName);

        assertEquals(1, smsLogsList.size());
        ResendStruct smsLogs = smsLogsList.get(0);

        ArrayList<Integer> idsToRemove = smsLogs.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = smsLogs.getDataArray();

        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(sms.getAddress(),          json_data.getString("address"));
        assertEquals(sms.getContactName(),      json_data.getString("contactName"));
        assertEquals(sms.getBody(),             json_data.getString("body"));
        assertEquals(sms.getType(),             json_data.getString("type"));
        assertEquals("/Date(" + Long.toString(sms.getDate())+ ")/", json_data.getString("date"));

        // Finish ----------------------------- GetSMSLogsToSend-----------------------------

        assertEquals(idsToRemove.size(), localDB.GetSMSLogsCount());		

        localDB.DeleteSMSLogs(idsToRemove);

        assertEquals(0, localDB.GetSMSLogsCount());		

    }

    @Test
    public void testDeleteAllSMSLogs()
    {
        Date date = new Date();
        MessageInformation sms = new MessageInformation("+380502341115", 
            "Test sms Body", 
            "Inbox", 
            date.getTime(), 
            "", 
            null,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_ADRESS, SQLiteHelper.MT_COLUMN_CONTACT_NAME, SQLiteHelper.MT_COLUMN_BODY, SQLiteHelper.MT_COLUMN_TYPE,
                SQLiteHelper.MT_COLUMN_DATE};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] { sms.getAddress(), sms.getContactName(), sms.getBody(), sms.getType(), sms.getDate() });
        data.add(new Object[] { sms.getAddress(), sms.getContactName(), sms.getBody(), sms.getType(), sms.getDate() });

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.SMSTABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);
        assertEquals(1, localDB.InsertSMS(sms));
        assertEquals(2, localDB.InsertSMS(sms));

        assertEquals(true, localDB.GetSMSLogsCount() > 0);		

        localDB.DeleteAllSMSLogs();

        assertEquals(0, localDB.GetSMSLogsCount());
    }

    @Test
    public void testSMSLogsEmpty()
    {	 
        final String featureName =DataProvider.SMS_LOG;
        ArrayList<ResendStruct> smsLogsList = new ArrayList<ResendStruct>();

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(smsLogsList, featureName);

        assertEquals(true, smsLogsList.isEmpty());

    }

    @Test
    public void testSMSLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteSMSLogs(new ArrayList<Integer>());
    }

    @Test(expected = NullPointerException.class)
    public void testSMSLogsBadCase()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertSMS(null);
    }

    private class MockDatabaseHelperMms
    {
        public MockDatabaseHelperMms(final ArrayList<MessageInformation> msms)
        {
            mmsObjects = msms;

            // Query table conent
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String tableName = (String)invocation.getArguments()[0];
                    String[] captions;
                    Collection<Object[]> values;

                    if (tableName.equals(SQLiteHelper.MMSTABLE_NAME))
                    {
                        captions = mmsCaptions;
                        values = mmsData.values();
                    }
                    else
                    {
                        captions = mmsAttachCaptions;
                        values = mmsAttachData.values();
                    }

                    final MockCursor mockCursor = new MockCursor(captions);

                    for (Object[] row : values)
                    {
                        mockCursor.addRow(row);
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(anyString(), (String[])isNull(), (String)isNull(), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

            // Delete from table by row ID
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[0];

                    String prefix = "delete from " + SQLiteHelper.MMSTABLE_NAME + " where _id=";
                    if (statement.startsWith(prefix))
                    {
                        Integer id = Integer.parseInt(statement.substring(prefix.length()));
                        mmsData.remove(id);
                    }
                    else
                    {
                        prefix = "delete from " + SQLiteHelper.MMS_ATTACH_TABLE_NAME + " where _id=";
                        if (statement.startsWith(prefix))
                        {
                            Integer id = Integer.parseInt(statement.substring(prefix.length()));
                            mmsAttachData.remove(id);
                        }
                    }

                    return null;
                }
            }).when(mockDB).execSQL(anyString());

            // Delete all from MMS table
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    mmsData.clear();
                    return null;
                }
            }).when(mockDB).execSQL(eq("delete from " + SQLiteHelper.MMSTABLE_NAME));

            // Delete all from MMS attach table
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    mmsAttachData.clear();
                    return null;
                }
            }).when(mockDB).execSQL(eq("delete from " + SQLiteHelper.MMS_ATTACH_TABLE_NAME));

            // Insert row to MMS table
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    int nextValue = nextMmsId.getAndIncrement();
                    processingMmsId.set(nextValue - 1);
                    currentAttachmentId.set(0);
                    MessageInformation currentMms = mmsObjects.get(nextValue - 1);
                    mmsData.put(nextValue, new Object[] { nextValue, currentMms.getAddress(), currentMms.getContactName(), currentMms.getBody(), currentMms.getType(), currentMms.getDate(), currentMms.getSubject()});
                    return nextValue;
                }
            }).when(mockDB).insert(eq(SQLiteHelper.MMSTABLE_NAME), (String)isNull(), refEq(mockContentValues));

            // Insert row to Attach table
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    int nextValue = nextAttachId.getAndIncrement();

                    int attachId = currentAttachmentId.getAndIncrement();
                    MMSAttachment attachment = mmsObjects.get(processingMmsId.get()).getAttachments().get(attachId);

                    mmsAttachData.put(nextValue, new Object[] { nextValue, nextMmsId.get() - 1, attachment.getData(), attachment.getType() });
                    return nextValue;
                }
            }).when(mockDB).insert(eq(SQLiteHelper.MMS_ATTACH_TABLE_NAME), (String)isNull(), refEq(mockContentValues));

            // Select attachment by row ID
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[2];

                    final MockCursor mockCursor = new MockCursor(mmsAttachCaptions);

                    int equalIndex = statement.indexOf("=");
                    if (equalIndex != -1)
                    {
                        String value = statement.substring(equalIndex + 1);
                        int mmsIdColumnIndex = 1;

                        for (Object[] row : mmsAttachData.values())
                        {
                            if (row[mmsIdColumnIndex].toString().equals(value))
                            {
                                mockCursor.addRow(row);
                            }
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(SQLiteHelper.MMS_ATTACH_TABLE_NAME), (String[])isNull(), notNull(String.class), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[2];

                    final MockCursor mockCursor = new MockCursor(mmsCaptions);

                    int equalIndex = statement.indexOf("=");
                    if (equalIndex != -1)
                    {
                        String value = statement.substring(equalIndex + 1);
                        int idIndex = 0;

                        for (Object[] row : mmsData.values())
                        {
                            if (row[idIndex].toString().equals(value))
                            {
                                mockCursor.addRow(row);
                            }
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(SQLiteHelper.MMSTABLE_NAME), (String[])isNull(), notNull(String.class), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

        }

        final AtomicInteger nextMmsId = new AtomicInteger(1);
        final HashMap<Integer, Object[]> mmsData = new HashMap<Integer, Object[]>();

        final AtomicInteger nextAttachId = new AtomicInteger(1);
        final HashMap<Integer, Object[]> attachData = new HashMap<Integer, Object[]>();

        final String[] mmsCaptions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_ADRESS, SQLiteHelper.MT_COLUMN_CONTACT_NAME, SQLiteHelper.MT_COLUMN_BODY, SQLiteHelper.MT_COLUMN_MMS_TYPE,
                SQLiteHelper.MT_COLUMN_DATE, SQLiteHelper.MT_COLUMN_SUBJECT};

        final HashMap<Integer, Object[]> mmsAttachData = new HashMap<Integer, Object[]>();
        final String[] mmsAttachCaptions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_MMS_ID, SQLiteHelper.MT_COLUMN_ATTACH, SQLiteHelper.MT_COLUMN_TYPE};

        final AtomicInteger processingMmsId = new AtomicInteger();
        final AtomicInteger currentAttachmentId = new AtomicInteger();

        final ArrayList<MessageInformation> mmsObjects;
    };

    @Test
    public void testMMSLogsLargeData() throws JSONException
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        final MessageInformation mms = new MessageInformation("+380502341115",
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        ArrayList<MessageInformation> messages = new ArrayList<MessageInformation>();
        messages.add(mms);

        MockDatabaseHelperMms helperMms = new MockDatabaseHelperMms(messages);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertMMS(mms));

        ArrayList<ResendStruct> mmsLogsList1 = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList1);

        int mmsCount = 1000;

        messages.clear();
        for (int i = 0; i < mmsCount; i++)
        {
            messages.add(mms);
        }

        helperMms = new MockDatabaseHelperMms(messages);

        for (int i = 0; i < mmsCount; i++)
        {
            localDB.InsertMMS(mms);
        }

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(2, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(737, idsToRemove.size());


        JSONObject dataArray = mmsLogs.getDataArray();

        final String featureName = DataProvider.MMS_LOG;
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(mms.getAddress(),json_data.getString(SQLiteHelper.MT_COLUMN_ADRESS));
        assertEquals(mms.getBody(),json_data.getString(SQLiteHelper.MT_COLUMN_BODY));
        assertEquals(mms.getType(),json_data.getString(SQLiteHelper.MT_COLUMN_TYPE));
        assertEquals(mms.getSubject(),json_data.getString(SQLiteHelper.MT_COLUMN_SUBJECT));
        assertEquals("/Date(" + Long.toString(mms.getDate())+ ")/",json_data.getString(SQLiteHelper.MT_COLUMN_DATE));

        JSONArray jsonAttachments = json_data.getJSONArray(SQLiteHelper.MT_COLUMN_ATTACH_LIST);
        assertEquals(1, jsonAttachments.length());

        JSONObject attach = jsonAttachments.getJSONObject(0);

        assertEquals(attach.getInt(SQLiteHelper.MT_COLUMN_ID),1);
        assertEquals(attach.getString(SQLiteHelper.MT_COLUMN_TYPE), imageType);		
        assertEquals(mmsCount, localDB.GetMMSLogsCount());

        for (ResendStruct mmsLogs1 : mmsLogsList) 
        {           
            localDB.DeleteMMSLogs(mmsLogs1.getIdsToRemove());       
        }

        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();
        Assert.assertEquals(fakeImage, sentAttachments.get(0).getData());


        // Finish ----------------------------- GetMMSLogsToSend-----------------------------


        assertEquals(mmsCount, localDB.GetMMSAttachmentsCount());


        for (MMSAttachment attachment : localDB.getAttachmentsToSend())
        {                       
            localDB.DeleteAttachmentByID(attachment.getId());                        
        }   

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    @Test
    public void testMMSLogs() throws JSONException
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        MessageInformation mms = new MessageInformation("+380502341115",
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        mms.setContactName("test contact name");

        MessageInformation mmsWithoutAttach = new MessageInformation("+380502341115",
                "Test Body",
                "Inbox",
                date.getTime(),
                "test Subject",
                new ArrayList<MMSAttachment>(),
                MessageInformation.MESSAGE_TYPE_UNKNOWN);


        ArrayList<MessageInformation> messages = new ArrayList<MessageInformation>();
        messages.add(mms);
        messages.add(mms);
        messages.add(mmsWithoutAttach);

        MockDatabaseHelperMms helperMms = new MockDatabaseHelperMms(messages);

        DataProvider localDB = new DataProvider(this.mockContext);
        assertEquals(1, localDB.InsertMMS(mms));
        assertEquals(2, localDB.InsertMMS(mms));

        assertEquals(3, localDB.InsertMMS(mmsWithoutAttach));

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(1, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(3, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));           

        JSONObject dataArray = mmsLogs.getDataArray();

        final String featureName = DataProvider.MMS_LOG;
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data = jsonArray.getJSONObject(0);

        assertEquals(mms.getAddress(),          json_data.getString("address"));
        assertEquals(mms.getContactName(),      json_data.getString("contactName"));
        assertEquals(mms.getBody(),             json_data.getString("body"));
        assertEquals(mms.getType(),             json_data.getString("type"));
        assertEquals(mms.getSubject(),          json_data.getString("sub"));
        assertEquals("/Date(" + Long.toString(mms.getDate())+ ")/", json_data.getString("date"));

        JSONArray jsonAttachments = json_data.getJSONArray("attachments");
        assertEquals(1, jsonAttachments.length());

        JSONObject attach = jsonAttachments.getJSONObject(0);

        assertEquals(attach.getInt(SQLiteHelper.MT_COLUMN_ID),1);
        assertEquals(attach.getString(SQLiteHelper.MT_COLUMN_TYPE), imageType);

        assertEquals(idsToRemove.size(), localDB.GetMMSLogsCount());
        localDB.DeleteMMSLogs(idsToRemove);


        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(2, sentAttachments.size());

        Assert.assertEquals(fakeImage, sentAttachments.get(0).getData());

        // Finish ----------------------------- GetMMSLogsToSend-----------------------------


        assertEquals(2, localDB.GetMMSAttachmentsCount());


        for (MMSAttachment attachment : sentAttachments)
        {
            localDB.DeleteAttachmentByID(attachment.getId());
        }

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    @Test
    public void testAttachmentsToSend()
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        final MessageInformation mms = new MessageInformation("+380502341115",
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        ArrayList<MessageInformation> messages = new ArrayList<MessageInformation>();
        messages.add(mms);

        MockDatabaseHelperMms helperMms = new MockDatabaseHelperMms(messages);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertMMS(mms));

        // Check ----------------------------- GetMMSLogsToSend-----------------------------
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(1, mmsLogsList.size());
        ResendStruct mmsLogs = mmsLogsList.get(0);

        ArrayList<Integer> idsToRemove = mmsLogs.getIdsToRemove();
        assertEquals(1, idsToRemove.size());

        ArrayList<MMSAttachment> sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(0, sentAttachments.size()); //because nothing was sent.

        localDB.DeleteMMSLogs(idsToRemove); // after that we can sent all attachments.

        sentAttachments = localDB.getAttachmentsToSend();

        assertEquals(1, sentAttachments.size()); //because nothing was sent.
    }

    @Test
    public void testDeleteAllMMSLogs()
    {
        Date date = new Date();
        ArrayList<MMSAttachment> attachments = new ArrayList<MMSAttachment>();
        byte[] fakeImage = {12,13,14,15,16,17,123, 12};
        String imageType = "image/jpeg";
        attachments.add(new MMSAttachment(fakeImage, imageType));

        final MessageInformation mms = new MessageInformation("+380502341115",
            "Test Body", 
            "Inbox", 
            date.getTime(), 
            "test Subject", 
            attachments,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        ArrayList<MessageInformation> messages = new ArrayList<MessageInformation>();
        messages.add(mms);
        messages.add(mms);

        MockDatabaseHelperMms helperMms = new MockDatabaseHelperMms(messages);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertMMS(mms));
        assertEquals(2, localDB.InsertMMS(mms));


        assertEquals(true, localDB.GetMMSLogsCount()>0);
        assertEquals(true, localDB.GetMMSAttachmentsCount()>0);

        localDB.DeleteAllMMSLogs();

        assertEquals(0, localDB.GetMMSLogsCount());
        assertEquals(0, localDB.GetMMSAttachmentsCount());
    }

    @Test
    public void testMMSLogsEmpty()
    {	 
        ArrayList<ResendStruct> mmsLogsList = new ArrayList<ResendStruct>();

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetMMSLogsToSend(mmsLogsList);

        assertEquals(true, mmsLogsList.isEmpty());
    }

    @Test
    public void testMMSLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteMMSLogs(new ArrayList<Integer>());
    }

    @Test(expected = NullPointerException.class)
    public void testMMSLogsBadCase()
    {
        MessageInformation mmsInfo = null;
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertMMS(mmsInfo);
    }

    @Test(expected = NullPointerException.class)
    public void testMMSLogsBadCaseNoAttachments()
    {
        Date date = new Date();
        MessageInformation mms = new MessageInformation("+380502341115",
            "Test Body",
            "Inbox",
            date.getTime(),
            "test Subject",
            null,
            MessageInformation.MESSAGE_TYPE_UNKNOWN);

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertMMS(mms);
    }

    // Test Browser Bookmarks
    @Test
    public void testBookmarkLogsEmpty()
    {
        ArrayList<ResendStruct> bookmarksList = new ArrayList<ResendStruct>();
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(bookmarksList, DataProvider.BROWSER_BOOKMARKS_LOG);
        assertEquals(true, bookmarksList.isEmpty());
    }

    private class MockDatabaseHelperBookmarks extends  MockDatabaseHelper
    {
        public MockDatabaseHelperBookmarks(final String tableName, final String[] captions, final ArrayList<Object[]> data)
        {
            super(tableName, captions, data);

            // Select rows by selection
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    final MockCursor mockCursor = new MockCursor(tableCaptions);

                    int isSentIndex = 3;

                    for (Object[] row : insertedData.values())
                    {
                        if ((Integer)row[isSentIndex] == 0)
                        {
                            mockCursor.addRow(row);
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(sqlTableName), (String[])isNull(), eq(new String(SQLiteHelper.BOOKMARK_COLUMN_ISSENT + " = 0")), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

            final String prefix = "UPDATE " + SQLiteHelper.BOOKMARK_TABLE_NAME + " SET " + SQLiteHelper.BOOKMARK_COLUMN_ISSENT + " = 1" + " WHERE " + SQLiteHelper.BOOKMARK_COLUMN_ID + "=";

            // Update bookmark
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[0];
                    String id =  statement.substring(prefix.length());

                    int isSentIndex = 3;
                    insertedData.get(Integer.parseInt(id))[isSentIndex] = 1;

                    return null;
                }
            }).when(mockDB).execSQL(startsWith(prefix));
        }
    };

    @Test
    public void testBookmarksLogs() throws JSONException
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow.com");
        BookmarkInformation bookmark2 = new BookmarkInformation("https://login.yahoo.com/", "Yahoo - login");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_TITLE, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_ISSENT};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {bookmark1.getTitle(), bookmark1.getUrl(), 0});
        data.add(new Object[] {bookmark2.getTitle(), bookmark2.getUrl(), 0});

        MockDatabaseHelperBookmarks databaseHelper = new MockDatabaseHelperBookmarks(SQLiteHelper.BOOKMARK_TABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertBookmark(bookmark1));
        assertEquals(2, localDB.InsertBookmark(bookmark2));

        // Check -----------------------------GetLogsToSend-----------------------------
        ArrayList<ResendStruct> bookmarksLogs = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(bookmarksLogs, DataProvider.BROWSER_BOOKMARKS_LOG);

        assertEquals(1, bookmarksLogs.size());
        ResendStruct bookmakLog = bookmarksLogs.get(0);

        ArrayList<Integer> idsToRemove = bookmakLog.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = bookmakLog.getDataArray();
        JSONArray jsonArray = dataArray.getJSONArray(DataProvider.BROWSER_BOOKMARKS_LOG);

        JSONObject json_data1 = jsonArray.getJSONObject(0);
        assertEquals(bookmark1.getUrl(), json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(bookmark1.getTitle(), json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));

        JSONObject json_data2 = jsonArray.getJSONObject(1);
        assertEquals(bookmark2.getUrl(), json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(bookmark2.getTitle(), json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));

        // Finish ----------------------------- GetLogsToSend-----------------------------
        Cursor cur = localDB.GetNewBookmarks();
        int bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(idsToRemove.size(), bookmarksCount);
        localDB.MarkedBookmarksAsSent(idsToRemove);

        cur = localDB.GetNewBookmarks();
        bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(0, bookmarksCount);

        cur = localDB.GetAllBookmarks();
        bookmarksCount = cur.getCount();
        cur.close();

        assertEquals(2, bookmarksCount);
    }

    @Test
    public void testBookmarksGetting()
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow");
        BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/", "Google");
        BookmarkInformation bookmark3 = new BookmarkInformation("https://login.yahoo.com", "Yahoo - login");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_TITLE, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_ISSENT};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {bookmark1.getTitle(), bookmark1.getUrl(), 0});
        data.add(new Object[] {bookmark2.getTitle(), bookmark2.getUrl(), 0});
        data.add(new Object[] {bookmark3.getTitle(), bookmark3.getUrl(), 0});

        MockDatabaseHelperBookmarks databaseHelper = new MockDatabaseHelperBookmarks(SQLiteHelper.BOOKMARK_TABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);
        ArrayList<Integer> ids = new ArrayList<Integer>();
        ids.add((int)localDB.InsertBookmark(bookmark1));
        ids.add((int)localDB.InsertBookmark(bookmark2));

        //Check count after simple inserting
        Cursor cursor = localDB.GetAllBookmarks();
        int allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        int newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(2, allBookmarkCount);
        assertEquals(2, newBookmarkCount);

        localDB.MarkedBookmarksAsSent(ids);

        //Check count after MarkedBookmarksAsSent
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(2, allBookmarkCount);
        assertEquals(0, newBookmarkCount);

        localDB.InsertBookmark(bookmark3);

        //Check count after inserting bookmark into non empty table
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();

        cursor = localDB.GetNewBookmarks();
        newBookmarkCount = cursor.getCount();
        cursor.close();

        assertEquals(3, allBookmarkCount);
        assertEquals(1, newBookmarkCount);
    }

    @Test (expected = NullPointerException.class)
    public void testBookmarksBadCase()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertBookmark(null);
    }

    @Test
    public void testBookmarksNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.MarkedBookmarksAsSent(new ArrayList<Integer>());
    }

    @Test
    public void testDeleteAllBookmarks()
    {
        BookmarkInformation bookmark1 = new BookmarkInformation("http://stackoverflow.com/", "stackoverflow");
        BookmarkInformation bookmark2 = new BookmarkInformation("https://www.google.com.ua/", "Google");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_TITLE, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_ISSENT};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {bookmark1.getTitle(), bookmark1.getUrl(), 0});
        data.add(new Object[] {bookmark2.getTitle(), bookmark2.getUrl(), 0});

        MockDatabaseHelperBookmarks databaseHelper = new MockDatabaseHelperBookmarks(SQLiteHelper.BOOKMARK_TABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        localDB.InsertBookmark(bookmark1);
        localDB.InsertBookmark(bookmark2);

        Cursor cursor = localDB.GetAllBookmarks();
        int allBookmarkCount = cursor.getCount();
        cursor.close();
        assertEquals(2, allBookmarkCount);
        
        localDB.DeleteAllBookmarks();
        
        //Check count after deleting
        cursor = localDB.GetAllBookmarks();
        allBookmarkCount = cursor.getCount();
        cursor.close();
        assertEquals(0, allBookmarkCount);
    }

    // Test Browser History
    @Test
    public void testBrowserHistoryLogsEmpty()
    {
        ArrayList<ResendStruct> browserHistoryList = new ArrayList<ResendStruct>();
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(browserHistoryList, DataProvider.BROWSER_HISTORY_LOG);
        assertTrue(browserHistoryList.isEmpty());
    }

    @Test
    public void testBrowserHistoryLogs() throws JSONException
    {
        SiteInformation testSite1 = new SiteInformation("http://stackoverflow.com/", 
                "stackoverflow.com", 
                Long.parseLong("1442850898"),
                2);
        SiteInformation testSite2 = new SiteInformation("https://login.yahoo.com/",
                "Yahoo - login",
                Long.parseLong("1442850895"),
                15);

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_TITLE,SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {testSite1.getUrl(), testSite1.getTitle(), testSite1.getDate(), testSite1.getVisits()});
        data.add(new Object[] {testSite2.getUrl(), testSite2.getTitle(), testSite2.getDate(), testSite2.getVisits()});

        MockDatabaseHelper databaseHelper = new MockDatabaseHelper(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, captions, data);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertBrowserHistory(testSite1));
        assertEquals(2, localDB.InsertBrowserHistory(testSite2));

        // Check -----------------------------GetLogsToSend-----------------------------
        final String featureName = DataProvider.BROWSER_HISTORY_LOG;
        ArrayList<ResendStruct> browserHistoryList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(browserHistoryList, featureName);

        assertEquals(1, browserHistoryList.size());
        ResendStruct historyLog = browserHistoryList.get(0);

        ArrayList<Integer> idsToRemove = historyLog.getIdsToRemove();
        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = historyLog.getDataArray();
        JSONArray jsonArray = dataArray.getJSONArray(featureName);

        JSONObject json_data1 = jsonArray.getJSONObject(0);
        assertEquals(testSite1.getUrl(),    json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(testSite1.getTitle(),  json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));
        assertEquals(testSite1.getVisits(), json_data1.getInt(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        assertEquals("/Date(" + testSite1.getDate() + ")/", json_data1.getString(SQLiteHelper.BOOKMARK_COLUMN_DATE));

        JSONObject json_data2 = jsonArray.getJSONObject(1);
        assertEquals(testSite2.getUrl(),    json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_URL));
        assertEquals(testSite2.getTitle(),  json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_TITLE));
        assertEquals(testSite2.getVisits(), json_data2.getInt(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        assertEquals("/Date(" + testSite2.getDate() + ")/", json_data2.getString(SQLiteHelper.BOOKMARK_COLUMN_DATE));

        // Finish ----------------------------- GetLogsToSend-----------------------------
        Cursor cur = localDB.GetBrowserHistory();
        int count = cur.getCount();
        cur.close();

        assertEquals(idsToRemove.size(), count);

        localDB.DeleteBrowserHistory(idsToRemove);

        cur = localDB.GetBrowserHistory();
        count = cur.getCount();
        cur.close();

        assertEquals(0, count);
    }

    @Test
    public void testBrowserHistoryNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteBrowserHistory(new ArrayList<Integer>());
        localDB.DeleteBrowserHistoryByID(0);
        localDB.DeleteBrowserHistoryByID(-1);
    }

    @Test (expected = NullPointerException.class)
    public void testBrowserHistoryLogsBadCase()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertBrowserHistory(null);
    }

    private class MockDatabaseHelpeBrowserHistoryDuplicated extends  MockDatabaseHelper
    {
        public MockDatabaseHelpeBrowserHistoryDuplicated(final String tableName, final String[] captions, final ArrayList<Object[]> data)
        {
            super(tableName, captions, data);

            final String selectionPrefix = SQLiteHelper.BOOKMARK_COLUMN_SITE_ID + " = ";

            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[2];
                    statement = statement.replaceAll("\\s+","");

                    String[] conditions = statement.split("AND");
                    String id = conditions[0].substring(conditions[0].indexOf("=") + 1);
                    String date = conditions[1].substring(conditions[1].indexOf("=") + 1);
                    String type = conditions[2].substring(conditions[2].indexOf("=") + 1);

                    int idIndex = 1;
                    int dateIndex = 3;
                    int typeIndex = 4;

                    final MockCursor mockCursor = new MockCursor(tableCaptions);

                    for (Object[] row : insertedData.values())
                    {
                        if (row[idIndex].toString().equals(id) && row[dateIndex].toString().equals(date) && row[typeIndex].toString().equals(type))
                        {
                            mockCursor.addRow(row);
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(sqlTableName), any(String[].class), startsWith(selectionPrefix), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());

            final String selectionPrefix2 = SQLiteHelper.BOOKMARK_COLUMN_DATE + " < ";

            // Update bookmark
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[2];
                    statement = statement.replaceAll("\\s+","");

                    long date = Long.parseLong(statement.substring(statement.indexOf("<") + 1));

                    int dateIndex = 3;

                    final MockCursor mockCursor = new MockCursor(tableCaptions);

                    for (Object[] row : insertedData.values())
                    {
                        if ((Long)row[dateIndex] < date)
                        {
                            mockCursor.addRow(row);
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(sqlTableName), any(String[].class), startsWith(selectionPrefix2), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());
        }
    };

    @Test
    public void testBrowserHistoryWithDuplicates()
    {
        SiteInformation site1 = new SiteInformation("http://stackoverflow.com/some_site",
                "stackoverflow",
                Long.parseLong("1442912340"),
                1);

        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 1, BrowserType.ChromeBrowser);

        SiteInformation site3 = new SiteInformation("http://stackoverflow.com/",
                "stackoverflow.com.ua",
                Long.parseLong("1442912340"),
                1);

        String[] captionsBrowserHistory = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_TITLE,SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> dataBrowserHistory = new ArrayList<Object[]>();
        dataBrowserHistory.add(new Object[] {site1.getUrl(), site1.getTitle(), site1.getDate(), site1.getVisits()});
        dataBrowserHistory.add(new Object[] {site3.getUrl(), site3.getTitle(), site3.getDate(), site3.getVisits()});

        MockDatabaseHelper databaseHelperBrowserHistory = new MockDatabaseHelper(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, captionsBrowserHistory, dataBrowserHistory);

        String[] captionsBrowserHistoryDuplicated = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_SITE_ID, SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE,
                SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> dataBrowserHistoryDuplicated = new ArrayList<Object[]>();
        dataBrowserHistoryDuplicated.add(new Object[] {site2.getSiteID(), site2.getBrowserType().ordinal(), site2.getDate(), site2.getVisits()});

        MockDatabaseHelpeBrowserHistoryDuplicated databaseHelperDuplicated = new MockDatabaseHelpeBrowserHistoryDuplicated(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, captionsBrowserHistoryDuplicated, dataBrowserHistoryDuplicated);

        DataProvider localDB = new DataProvider(this.mockContext);

        localDB.InsertBrowserHistory(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistory(site3);


        //Check --------------------------GetBrowserHistory-----------------------------------------
        Cursor cursor = localDB.GetBrowserHistory();
        int count = cursor.getCount();
        cursor.close();

        Assert.assertEquals(2, count);
        //Finish -------------------------GetBrowserHistory-----------------------------------------

        //Check --------------------------FindDuplicatedBrowserHistory------------------------------

        //wrong id
        cursor = localDB.FindDuplicatedBrowserHistory(2, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();

        Assert.assertEquals(0, count);

        //wrong date
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912341"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);

        //wrong browser type
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.DefaultBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);

        //right data
        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(1, count);

        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        cursor.moveToFirst();
        int visits = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_VISITS));
        Long date = cursor.getLong(cursor.getColumnIndex(SQLiteHelper.BOOKMARK_COLUMN_DATE));
        cursor.close();

        Assert.assertEquals(visits, site3.getVisits());
        Assert.assertEquals(date, site3.getDate());

        localDB.DeleteBrowserHistoryFromSkipListByID(1);

        cursor = localDB.FindDuplicatedBrowserHistory(1, Long.parseLong("1442912340"), BrowserType.ChromeBrowser);
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);
        //Finish --------------------------FindDuplicatedBrowserHistory------------------------------
    }

    @Test
    public void testClearSkipListByDate()
    {
        DuplicatedSiteInformation site1 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 1, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(2, Long.parseLong("1442912343"), 1, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site3 = new DuplicatedSiteInformation(3, Long.parseLong("1442912346"), 1, BrowserType.ChromeBrowser);

        String[] captionsBrowserHistoryDuplicated = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_SITE_ID, SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE,
                SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> dataBrowserHistoryDuplicated = new ArrayList<Object[]>();
        dataBrowserHistoryDuplicated.add(new Object[] {site1.getSiteID(), site1.getBrowserType().ordinal(), site1.getDate(), site1.getVisits()});
        dataBrowserHistoryDuplicated.add(new Object[] {site2.getSiteID(), site2.getBrowserType().ordinal(), site2.getDate(), site2.getVisits()});
        dataBrowserHistoryDuplicated.add(new Object[] {site3.getSiteID(), site3.getBrowserType().ordinal(), site3.getDate(), site3.getVisits()});

        MockDatabaseHelpeBrowserHistoryDuplicated databaseHelperDuplicated = new MockDatabaseHelpeBrowserHistoryDuplicated(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, captionsBrowserHistoryDuplicated, dataBrowserHistoryDuplicated);

        DataProvider localDB = new DataProvider(this.mockContext);

        localDB.InsertBrowserHistoryToSkipList(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistoryToSkipList(site3);

        Cursor browserHistorySkiplistCursor = null;
        try
        {
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertTrue(browserHistorySkiplistCursor.moveToFirst());
            Assert.assertEquals(3, browserHistorySkiplistCursor.getCount());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;

            localDB.ClearSkipListByDate(Long.parseLong("1442912341"));
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertTrue(browserHistorySkiplistCursor.moveToFirst());
            Assert.assertEquals(2, browserHistorySkiplistCursor.getCount());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;

            localDB.ClearSkipListByDate(Long.parseLong("1442912348"));
            browserHistorySkiplistCursor = localDB.GetBrowserHistorySkipList(Long.MAX_VALUE);

            Assert.assertNotNull(browserHistorySkiplistCursor);
            Assert.assertEquals(0, browserHistorySkiplistCursor.getCount());
            Assert.assertFalse(browserHistorySkiplistCursor.moveToFirst());
            browserHistorySkiplistCursor.close();
            browserHistorySkiplistCursor = null;
        }
        finally
        {
            if(browserHistorySkiplistCursor != null)
            {
                browserHistorySkiplistCursor.close();
            }
        }
    }

    @Test
    public void testDeleteAllBrowserHistory()
    {
        SiteInformation testSite1 = new SiteInformation("http://stackoverflow.com/", 
                "stackoverflow.com", 
                Long.parseLong("1442850898"),
                2);

        SiteInformation testSite2 = new SiteInformation("https://login.yahoo.com/", 
                "Yahoo - login", 
                Long.parseLong("1442850895"),
                15);

        String[] captionsBrowserHistory = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_TITLE,SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> dataBrowserHistory = new ArrayList<Object[]>();
        dataBrowserHistory.add(new Object[] {testSite1.getUrl(), testSite1.getTitle(), testSite1.getDate(), testSite1.getVisits()});
        dataBrowserHistory.add(new Object[] {testSite2.getUrl(), testSite2.getTitle(), testSite2.getDate(), testSite2.getVisits()});

        MockDatabaseHelper databaseHelperBrowserHistory = new MockDatabaseHelper(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, captionsBrowserHistory, dataBrowserHistory);

        DataProvider localDB = new DataProvider(this.mockContext);

        localDB.InsertBrowserHistory(testSite1);
        localDB.InsertBrowserHistory(testSite2);

        Cursor cursor = localDB.GetBrowserHistory();
        int count = cursor.getCount();
        cursor.close();
        assertEquals(2, count);
        
        localDB.DeleteAllBrowserHistory();
        
        cursor = localDB.GetBrowserHistory();
        count = cursor.getCount();
        cursor.close();
        assertEquals(0, count);
    }

    @Test
    public void testDeleteAllBrowserHistorySkipList()
    {
        DuplicatedSiteInformation site1 = new DuplicatedSiteInformation(1, Long.parseLong("1442916453"), 1, BrowserType.DefaultBrowser);
        DuplicatedSiteInformation site2 = new DuplicatedSiteInformation(1, Long.parseLong("1442912340"), 2, BrowserType.ChromeBrowser);
        DuplicatedSiteInformation site3 = new DuplicatedSiteInformation(2, Long.parseLong("1222912341"), 5, BrowserType.ChromeBrowser);

        String[] captionsBrowserHistoryDuplicated = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_SITE_ID, SQLiteHelper.BOOKMARK_COLUMN_BROWSER_TYPE,
                SQLiteHelper.BOOKMARK_COLUMN_DATE,
                SQLiteHelper.BOOKMARK_COLUMN_VISITS};

        ArrayList<Object[]> dataBrowserHistoryDuplicated = new ArrayList<Object[]>();
        dataBrowserHistoryDuplicated.add(new Object[] {site1.getSiteID(), site1.getBrowserType().ordinal(), site1.getDate(), site1.getVisits()});
        dataBrowserHistoryDuplicated.add(new Object[] {site2.getSiteID(), site2.getBrowserType().ordinal(), site2.getDate(), site2.getVisits()});
        dataBrowserHistoryDuplicated.add(new Object[] {site3.getSiteID(), site3.getBrowserType().ordinal(), site3.getDate(), site3.getVisits()});

        MockDatabaseHelpeBrowserHistoryDuplicated databaseHelperDuplicated = new MockDatabaseHelpeBrowserHistoryDuplicated(SQLiteHelper.BROWSER_HISTORY_DUBLICATED_TABLE_NAME, captionsBrowserHistoryDuplicated, dataBrowserHistoryDuplicated);

        DataProvider localDB = new DataProvider(this.mockContext);

        localDB.InsertBrowserHistoryToSkipList(site1);
        localDB.InsertBrowserHistoryToSkipList(site2);
        localDB.InsertBrowserHistoryToSkipList(site3);

        Cursor cursor = localDB.GetBrowserHistorySkipList(Long.parseLong("9999999999"));
        int count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(3, count);

        localDB.DeleteAllBrowserHistorySkipList();

        cursor = localDB.GetBrowserHistorySkipList(Long.parseLong("9999999999"));
        count = cursor.getCount();
        cursor.close();
        Assert.assertEquals(0, count);
    }

    private class ConversationsMockDatabaseHelper extends MockDatabaseHelper {

        public ConversationsMockDatabaseHelper(String tableName, String[] captions, ArrayList<Object[]> data, final boolean immitateCollision) {
            super(tableName, captions, data);

            final String selectionPrefix = SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH + "=";

            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String statement = (String)invocation.getArguments()[2];

                    final MockCursor mockCursor = new MockCursor(tableCaptions);

                    if (immitateCollision)
                    {
                        if (insertedData.containsKey(1)) {
                            mockCursor.addRow(insertedData.get(1));
                        }
                    }
                    else {
                        int hash = Integer.parseInt(statement.substring(selectionPrefix.length()));

                        int hashIndex = mockCursor.getColumnIndex(SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH);

                        for (Object[] row : insertedData.values()) {
                            if ((Integer) row[hashIndex] == hash) {
                                mockCursor.addRow(row);
                            }
                        }
                    }

                    return mockCursor;
                }
            }).when(mockDB).query(eq(sqlTableName), any(String[].class), startsWith(selectionPrefix), (String[])isNull(), (String)isNull(), (String)isNull(), (String)isNull());
        }
    }

    @Test
    public void testGetAllConversationMessages() {
        Message msg1 = new Message("Test sender", "Test recipient", "Test body", "Incoming", "Test chat name",
                new Date().getTime(), "Kik");
        Message msg2 = new Message("Test sender2", "Test recipient2", "Test body2", "Incoming2", "Test chat name2",
                new Date().getTime(), "Kik");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {msg1.getSender(), msg1.getRecipient(),  msg1.getBody(), msg1.getType(), msg1.getChatName(), msg1.getTimestamp(),
                msg1.getApplicationName(), msg1.getHash() });
        data.add(new Object[] {msg2.getSender(), msg2.getRecipient(),  msg2.getBody(), msg2.getType(), msg2.getChatName(), msg2.getTimestamp(),
                msg2.getApplicationName(), msg2.getHash() });

        ConversationsMockDatabaseHelper databaseHelper = new ConversationsMockDatabaseHelper(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, captions, data, false);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertConversationMessage(msg1));
        assertEquals(2, localDB.InsertConversationMessage(msg2));

        final String featureName = DataProvider.CONVERSATIONS_LOG;
        ArrayList<ResendStruct> messagesList = new ArrayList<ResendStruct>();
        localDB.GetLogsToSend(messagesList, featureName);

        // Check ----------------------------- GetCallLogsToSend-----------------------------

        assertEquals(1, messagesList.size());
        ResendStruct msgLogs = messagesList.get(0);

        ArrayList<Integer> idsToRemove = msgLogs.getIdsToRemove();

        assertEquals(2, idsToRemove.size());
        assertEquals(1, (int)idsToRemove.get(0));
        assertEquals(2, (int)idsToRemove.get(1));

        JSONObject dataArray = msgLogs.getDataArray();

        JSONArray jsonArray = null;
        try {
            jsonArray = dataArray.getJSONArray(featureName);
            JSONObject json_data = jsonArray.getJSONObject(0);

            assertEquals(msg1.getSender(),           json_data.getString("sender"));
            assertEquals(msg1.getRecipient(),        json_data.getString("recipient"));
            assertEquals(msg1.getBody(),             json_data.getString("body"));
            assertEquals(msg1.getType(),             json_data.getString("type"));
            assertEquals(msg1.getChatName(),         json_data.getString("chat_name"));
            assertEquals(msg1.getTimestamp(),        json_data.getLong("timestamp"));
            assertEquals(msg1.getApplicationName(),  json_data.getString("application_name"));
        } catch (JSONException e) {
            Assert.fail();
        }

        assertEquals(idsToRemove.size(), localDB.GetConversationsLogsCount());

        localDB.DeleteConversationMessages(idsToRemove);

        assertEquals(0, localDB.GetConversationsLogsCount());

    }

    @Test
    public void testDeleteAllConversationMessages() {
        Message msg1 = new Message("Test sender", "Test recipient", "Test body", "Incoming", "Test chat name",
                new Date().getTime(), "Kik");
        Message msg2 = new Message("Test sender2", "Test recipient2", "Test body2", "Incoming2", "Test chat name2",
                new Date().getTime(), "Kik");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {msg1.getSender(), msg1.getRecipient(),  msg1.getBody(), msg1.getType(), msg1.getChatName(), msg1.getTimestamp(),
                msg1.getApplicationName(), msg1.getHash() });
        data.add(new Object[] {msg2.getSender(), msg2.getRecipient(),  msg2.getBody(), msg2.getType(), msg2.getChatName(), msg2.getTimestamp(),
                msg2.getApplicationName(), msg2.getHash() });

        ConversationsMockDatabaseHelper databaseHelper = new ConversationsMockDatabaseHelper(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, captions, data, false);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1L, localDB.InsertConversationMessage(msg1));
        assertEquals(2L, localDB.InsertConversationMessage(msg2));

        localDB.DeleteAllConversationMessages();

        assertEquals(0, localDB.GetConversationsLogsCount());

    }

    @Test
    public void testInsertDuplicateMessage() {
        Message msg = new Message("Test sender", "Test recipient", "Test body", "Incoming", "Test chat name",
                new Date().getTime(), "Kik");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {msg.getSender(), msg.getRecipient(),  msg.getBody(), msg.getType(), msg.getChatName(), msg.getTimestamp(),
                msg.getApplicationName(), msg.getHash() });
        data.add(new Object[] {msg.getSender(), msg.getRecipient(),  msg.getBody(), msg.getType(), msg.getChatName(), msg.getTimestamp(),
                msg.getApplicationName(), msg.getHash() });

        ConversationsMockDatabaseHelper databaseHelper = new ConversationsMockDatabaseHelper(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, captions, data, false);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertConversationMessage(msg));
        assertEquals(-1, localDB.InsertConversationMessage(msg));

    }

    @Test
    public void testInsertMessageCollision() {
        Message etalonMessage = new Message("Test sender", "Test recipient", "Test body", "Incoming", "Test chat name",
                new Date().getTime(), "AppName1");
        Message msg2 = new Message("Test sender2", "Test recipient2", "Test body2", "Incoming2", "Test chat name2",
                new Date().getTime(), "AppName2");
        Message msg3 = new Message(etalonMessage.getSender(), "Test recipient2", "Test body2", "Incoming2", "Test chat name2",
                new Date().getTime(), "AppName2");
        Message msg4 = new Message(etalonMessage.getSender(), etalonMessage.getRecipient(), "Test body2", "Incoming2", "Test chat name2",
                new Date().getTime(), "AppName2");
        Message msg5 = new Message(etalonMessage.getSender(), etalonMessage.getRecipient(), etalonMessage.getBody(), "Incoming2", "Test chat name2",
                new Date().getTime(), "AppName2");
        Message msg6 = new Message(etalonMessage.getSender(), etalonMessage.getRecipient(), etalonMessage.getBody(), etalonMessage.getType(), "Test chat name2",
                new Date().getTime(), "AppName2");
        Message msg7 = new Message(etalonMessage.getSender(), etalonMessage.getRecipient(), etalonMessage.getBody(), etalonMessage.getType(), etalonMessage.getChatName(),
                new Date().getTime(), "AppName2");
        Message msg8 = new Message(etalonMessage.getSender(), etalonMessage.getRecipient(), etalonMessage.getBody(), etalonMessage.getType(), etalonMessage.getChatName(),
                etalonMessage.getTimestamp(), "AppName2");

        String[] captions = new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_SENDER, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_RECIPIENT,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_BODY, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TYPE, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_CHAT_NAME,
                SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_TIMESTAMP, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_APPLICATION_NAME, SQLiteHelper.CONVERSATION_MESSAGES_COLUMN_HASH};

        ArrayList<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {etalonMessage.getSender(), etalonMessage.getRecipient(),  etalonMessage.getBody(), etalonMessage.getType(), etalonMessage.getChatName(), etalonMessage.getTimestamp(),
                etalonMessage.getApplicationName(), etalonMessage.getHash() });
        data.add(new Object[] {msg2.getSender(), msg2.getRecipient(),  msg2.getBody(), msg2.getType(), msg2.getChatName(), msg2.getTimestamp(),
                msg2.getApplicationName(), msg2.getHash() });
        data.add(new Object[] {msg3.getSender(), msg3.getRecipient(),  msg3.getBody(), msg3.getType(), msg3.getChatName(), msg3.getTimestamp(),
                msg3.getApplicationName(), msg3.getHash() });
        data.add(new Object[] {msg4.getSender(), msg4.getRecipient(),  msg4.getBody(), msg4.getType(), msg4.getChatName(), msg4.getTimestamp(),
                msg4.getApplicationName(), msg4.getHash() });
        data.add(new Object[] {msg5.getSender(), msg5.getRecipient(),  msg5.getBody(), msg5.getType(), msg5.getChatName(), msg5.getTimestamp(),
                msg5.getApplicationName(), msg5.getHash() });
        data.add(new Object[] {msg6.getSender(), msg6.getRecipient(),  msg6.getBody(), msg6.getType(), msg6.getChatName(), msg6.getTimestamp(),
                msg6.getApplicationName(), msg6.getHash() });
        data.add(new Object[] {msg7.getSender(), msg7.getRecipient(),  msg7.getBody(), msg7.getType(), msg7.getChatName(), msg7.getTimestamp(),
                msg7.getApplicationName(), msg7.getHash() });
        data.add(new Object[] {msg8.getSender(), msg8.getRecipient(),  msg8.getBody(), msg8.getType(), msg8.getChatName(), msg8.getTimestamp(),
                msg8.getApplicationName(), msg8.getHash() });

        ConversationsMockDatabaseHelper databaseHelper = new ConversationsMockDatabaseHelper(SQLiteHelper.CONVERSATION_MESSAGES_TABLE_NAME, captions, data, true);

        DataProvider localDB = new DataProvider(this.mockContext);

        assertEquals(1, localDB.InsertConversationMessage(etalonMessage));
        assertEquals(2, localDB.InsertConversationMessage(msg2));
        assertEquals(3, localDB.InsertConversationMessage(msg3));
        assertEquals(4, localDB.InsertConversationMessage(msg4));
        assertEquals(5, localDB.InsertConversationMessage(msg5));
        assertEquals(6, localDB.InsertConversationMessage(msg6));
        assertEquals(7, localDB.InsertConversationMessage(msg7));
        assertEquals(8, localDB.InsertConversationMessage(msg8));
    }

    @Test
    public void testConversationsLogsEmpty()
    {
        ArrayList<ResendStruct> logs = new ArrayList<ResendStruct>();

        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.GetLogsToSend(logs, DataProvider.CONVERSATIONS_LOG);

        assertEquals(true, logs.isEmpty());
    }

    @Test
    public void testConversationsLogsNothingToKill()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.DeleteConversationMessages(new ArrayList<Integer>());
    }

    @Test(expected = NullPointerException.class)
    public void testConversationsLogsCase()
    {
        DataProvider localDB = new DataProvider(this.mockContext);
        localDB.InsertConversationMessage(null);
    }

    private String getTestCrashReport()
    {
        return "Error Report collected on : Fri Apr 13 10:38:50 EEST 2018\n"
                + "\n"
                + "Informations :\n"
                + "Brand: google\n"
                + "Device: bullhead\n"
                + "Display: OPM3.171019.016\n"
                + "Fingerprint: google/bullhead/bullhead:8.1.8/OPM3.171019.016/4565142:user/release-keys\n"
                + "ID: OPM3.171019.016\n"
                + "Manufacturer: LGE\n"
                + "Model: Nexus 5X\n"
                + "Product: bullhead\n"
                + "Tags: release-keys\n"
                + "Type: user\n"
                + "Hardware: bullhead\n"
                + "Board: bullhead\n"
                + "Bootloader: BHZ31b\n"
                + "ABI: arm64-v8a\n"
                + "ABI2: \n"
                + "Host: wpdt4.hot.corp.google.com\n"
                + "SDK Version: 27\n"
                + "Release Version: 8.1.0\n"
                + "Total Internal memory: 1234567\n"
                + "Available Internal memory: 1234567\n"
                + "\n"
                + "\n"
                + "Stack:\n"
                + "java.lang.NullPointerException\n"
                + "\n"
                + "--------- Stack trace ---------\n"
                + "\n"
                + "java.lang.NullPointerException\n"
                + "	at com.mobileenterprise.agent.test.DataProviderTest.getTestCrashReport(DataProviderTest.java:1673)\n"
                + "	at com.mobileenterprise.agent.test.DataProviderTest.testCrashLogsLargeData(DataProviderTest.java:575)\n"
                + "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n"
                + "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n"
                + "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n"
                + "	at java.lang.reflect.Method.invoke(Method.java:498)\n"
                + "	at org.junit.internal.runners.TestMethod.invoke(TestMethod.java:68)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl$PowerMockJUnit44MethodRunner.runTestMethod(PowerMockJUnit44RunnerDelegateImpl.java:316)\n"
                + "	at org.junit.internal.runners.MethodRoadie$2.run(MethodRoadie.java:89)\n"
                + "	at org.junit.internal.runners.MethodRoadie.runBeforesThenTestThenAfters(MethodRoadie.java:97)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl$PowerMockJUnit44MethodRunner.executeTest(PowerMockJUnit44RunnerDelegateImpl.java:300)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.executeTestInSuper(PowerMockJUnit47RunnerDelegateImpl.java:131)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.access$100(PowerMockJUnit47RunnerDelegateImpl.java:59)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner$TestExecutorStatement.evaluate(PowerMockJUnit47RunnerDelegateImpl.java:147)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.evaluateStatement(PowerMockJUnit47RunnerDelegateImpl.java:107)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit47RunnerDelegateImpl$PowerMockJUnit47MethodRunner.executeTest(PowerMockJUnit47RunnerDelegateImpl.java:82)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl$PowerMockJUnit44MethodRunner.runBeforesThenTestThenAfters(PowerMockJUnit44RunnerDelegateImpl.java:288)\n"
                + "	at org.junit.internal.runners.MethodRoadie.runTest(MethodRoadie.java:87)\n"
                + "	at org.junit.internal.runners.MethodRoadie.run(MethodRoadie.java:50)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl.invokeTestMethod(PowerMockJUnit44RunnerDelegateImpl.java:208)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl.runMethods(PowerMockJUnit44RunnerDelegateImpl.java:147)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl$1.run(PowerMockJUnit44RunnerDelegateImpl.java:121)\n"
                + "	at org.junit.internal.runners.ClassRoadie.runUnprotected(ClassRoadie.java:34)\n"
                + "	at org.junit.internal.runners.ClassRoadie.runProtected(ClassRoadie.java:44)\n"
                + "	at org.powermock.modules.junit4.internal.impl.PowerMockJUnit44RunnerDelegateImpl.run(PowerMockJUnit44RunnerDelegateImpl.java:123)\n"
                + "	at org.powermock.modules.junit4.common.internal.impl.JUnit4TestSuiteChunkerImpl.run(JUnit4TestSuiteChunkerImpl.java:121)\n"
                + "	at org.powermock.modules.junit4.common.internal.impl.AbstractCommonPowerMockRunner.run(AbstractCommonPowerMockRunner.java:53)\n"
                + "	at org.powermock.modules.junit4.PowerMockRunner.run(PowerMockRunner.java:59)\n"
                + "	at org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n"
                + "	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:68)\n"
                + "	at com.intellij.rt.execution.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:47)\n"
                + "	at com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:242)\n"
                + "	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)\n"
                + "-------------------------------\n";
    }

    private static final String TEST_FILE_PREFIX = "test_";
    //private DataProvider localDB;
    private RenamingDelegatingContext localContext;
}
