package com.mobileenterprise.agent.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.provider.Browser;
import android.util.Log;
import android.widget.Toast;

import static org.mockito.AdditionalMatchers.*;

import com.mobileenterprise.agent.helper.BookmarkColumns;
import com.mobileenterprise.agent.helper.BookmarkInformation;
import com.mobileenterprise.agent.helper.BrowserDataObserver;
import com.mobileenterprise.agent.helper.CustomLogger;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.Settings.BrowserType;
import com.mobileenterprise.agent.helper.SiteInformation;
import com.mobileenterprise.agent.mocks.MockCursor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.isNull;
import static org.mockito.Matchers.refEq;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, Log.class, Toast.class, DataProvider.class, BrowserDataObserver.class, CustomLogger.class, Settings.class})
public class BrowserDataObserverTest
{
    private String[] projection = new String[]{BookmarkColumns._ID,
            BookmarkColumns.TITLE,
            BookmarkColumns.URL,
            BookmarkColumns.DATE,
            BookmarkColumns.VISITS,
            BookmarkColumns.BOOKMARK};

    @Test
    public void testNewBookmarks()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ))).
                    thenReturn(new MockCursor(projection));

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};
            Mockito.doReturn(new MockCursor(bookmarksColumns)).when(mockDataProvider).GetAllBookmarks();

            MockCursor bookmarksHistoryCursor = new MockCursor(bookmarksColumns);
            bookmarksHistoryCursor.addRow(new Object[]{"test bookmark title1", "test bookmark url1"});
            bookmarksHistoryCursor.addRow(new Object[]{"test bookmark title2", "test bookmark url2"});

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                        thenReturn(bookmarksHistoryCursor);

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            ArgumentCaptor<BookmarkInformation> bookmarkInfoCaptor = ArgumentCaptor.forClass(BookmarkInformation.class);
            Mockito.verify(mockDataProvider, Mockito.times(2)).InsertBookmark(bookmarkInfoCaptor.capture());

            List<BookmarkInformation> capturedInfos = bookmarkInfoCaptor.getAllValues();

            Assert.assertEquals(capturedInfos.get(0).getTitle(), "test bookmark title1");
            Assert.assertEquals(capturedInfos.get(0).getUrl(), "test bookmark url1");

            Assert.assertEquals(capturedInfos.get(1).getTitle(), "test bookmark title2");
            Assert.assertEquals(capturedInfos.get(1).getUrl(), "test bookmark url2");
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void testNewBrowserHistory()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);
            PowerMockito.mockStatic(Settings.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            MockCursor chromeHistoryCursor = new MockCursor(projection);
            chromeHistoryCursor.addRow(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 1});
            chromeHistoryCursor.addRow(new Object[]{1, "test title2", "test url2", Long.parseLong("12345"), 1, 2});

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ))).
                    thenReturn(chromeHistoryCursor);

            Mockito.doReturn(new MockCursor(projection)).when(mockDataProvider).FindDuplicatedBrowserHistory(anyInt(), anyLong(), Matchers.<BrowserType>any());

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};
            Mockito.doReturn(new MockCursor(bookmarksColumns)).when(mockDataProvider).GetAllBookmarks();

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                    thenReturn(new MockCursor(bookmarksColumns));

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            ArgumentCaptor<SiteInformation> siteInfoCaptor = ArgumentCaptor.forClass(SiteInformation.class);
            Mockito.verify(mockDataProvider, Mockito.times(2)).InsertBrowserHistory(siteInfoCaptor.capture());

            List<SiteInformation> capturedInfos = siteInfoCaptor.getAllValues();

            Assert.assertEquals(capturedInfos.get(0).getDate().longValue(), 12342L);
            Assert.assertEquals(capturedInfos.get(0).getVisits(), 2);
            Assert.assertEquals(capturedInfos.get(0).dateIsValid(), true);
            Assert.assertEquals(capturedInfos.get(0).getTitle(), "test title1");
            Assert.assertEquals(capturedInfos.get(0).getUrl(), "test url1");

            Assert.assertEquals(capturedInfos.get(1).getDate().longValue(), 12345L);
            Assert.assertEquals(capturedInfos.get(1).getVisits(), 1);
            Assert.assertEquals(capturedInfos.get(1).dateIsValid(), true);
            Assert.assertEquals(capturedInfos.get(1).getTitle(), "test title2");
            Assert.assertEquals(capturedInfos.get(1).getUrl(), "test url2");
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void testNewBrowserHistoryWithSkipList()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);
            PowerMockito.mockStatic(Settings.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            MockCursor chromeHistoryCursor = new MockCursor(projection);
            chromeHistoryCursor.addRow(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 1});
            chromeHistoryCursor.addRow(new Object[]{1, "test title2", "test url2", Long.parseLong("12345"), 1, 2});

            Mockito.doReturn(chromeHistoryCursor).when(mockContentResolver).query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ));

            MockCursor skipListCursor = new MockCursor(projection);
            skipListCursor.addRow(new Object[]{1, "test title2", "test url2", Long.parseLong("12345"), 1, 2});
            Mockito.doReturn(skipListCursor).when(mockDataProvider).FindDuplicatedBrowserHistory(anyInt(), anyLong(), Mockito.eq(BrowserType.ChromeBrowser));

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};
            Mockito.doReturn(new MockCursor(bookmarksColumns)).when(mockDataProvider).GetAllBookmarks();

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                    thenReturn(new MockCursor(bookmarksColumns));

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            ArgumentCaptor<SiteInformation> siteInfoCaptor = ArgumentCaptor.forClass(SiteInformation.class);
            Mockito.verify(mockDataProvider, Mockito.times(1)).InsertBrowserHistory(siteInfoCaptor.capture());

            List<SiteInformation> capturedInfos = siteInfoCaptor.getAllValues();

            Assert.assertEquals(capturedInfos.get(0).getDate().longValue(), 12342L);
            Assert.assertEquals(capturedInfos.get(0).getVisits(), 2);
            Assert.assertEquals(capturedInfos.get(0).dateIsValid(), false);
            Assert.assertEquals(capturedInfos.get(0).getTitle(), "test title1");
            Assert.assertEquals(capturedInfos.get(0).getUrl(), "test url1");
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void testNewBookmarksWithSkipList()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ))).
                    thenReturn(new MockCursor(projection));

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};

            MockCursor skipListCursor = new MockCursor(bookmarksColumns);
            skipListCursor.addRow(new Object[]{"test bookmark title1", "test bookmark url1"});
            Mockito.doReturn(skipListCursor).when(mockDataProvider).GetAllBookmarks();

            MockCursor bookmarksHistoryCursor = new MockCursor(bookmarksColumns);
            bookmarksHistoryCursor.addRow(new Object[]{"test bookmark title1", "test bookmark url1"});
            bookmarksHistoryCursor.addRow(new Object[]{"test bookmark title2", "test bookmark url2"});

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                    thenReturn(bookmarksHistoryCursor);

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            ArgumentCaptor<BookmarkInformation> bookmarkInfoCaptor = ArgumentCaptor.forClass(BookmarkInformation.class);
            Mockito.verify(mockDataProvider, Mockito.times(1)).InsertBookmark(bookmarkInfoCaptor.capture());

            List<BookmarkInformation> capturedInfos = bookmarkInfoCaptor.getAllValues();

            Assert.assertEquals(capturedInfos.get(0).getTitle(), "test bookmark title2");
            Assert.assertEquals(capturedInfos.get(0).getUrl(), "test bookmark url2");
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void testBrowserFindNewHistoryWithException()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);
            PowerMockito.mockStatic(Settings.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            MockCursor chromeHistoryCursor = new MockCursor(new String[]{"Fake Column"});
            chromeHistoryCursor.addRow(new Object[]{1, "test title1", "test url1", Long.parseLong("12342"), 2, 1});

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ))).
                    thenReturn(chromeHistoryCursor);

            Mockito.doReturn(new MockCursor(projection)).when(mockDataProvider).FindDuplicatedBrowserHistory(anyInt(), anyLong(), Matchers.<BrowserType>any());

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};
            Mockito.doReturn(new MockCursor(bookmarksColumns)).when(mockDataProvider).GetAllBookmarks();

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                    thenReturn(new MockCursor(bookmarksColumns));

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            Mockito.verify(mockDataProvider, Mockito.times(0)).InsertBrowserHistory(Mockito.<SiteInformation>any());
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void testNewBookmarksWithException()
    {
        try
        {
            PowerMockito.mockStatic(Uri.class);
            PowerMockito.mockStatic(Log.class);
            PowerMockito.mockStatic(Toast.class);
            PowerMockito.mockStatic(CustomLogger.class);

            Uri mockChromeHistoryUri = PowerMockito.mock(Uri.class);
            Uri mockChromeBookmarkUri = PowerMockito.mock(Uri.class);
            Toast mockToast = PowerMockito.mock(Toast.class);

            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/history")).thenReturn(mockChromeHistoryUri);
            PowerMockito.when(Uri.class, "parse", Mockito.eq("content://com.android.chrome.browser/bookmarks")).thenReturn(mockChromeBookmarkUri);
            PowerMockito.when(Toast.class, "makeText", any(), anyInt(), anyInt()).thenReturn(mockToast);
            PowerMockito.when(Toast.class, "makeText", any(), anyObject(), anyInt()).thenReturn(mockToast);

            final Context mockContext = Mockito.mock(Context.class);
            final SharedPreferences mockSharedPrefs = Mockito.mock(SharedPreferences.class);
            final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);
            final DataProvider mockDataProvider = Mockito.mock(DataProvider.class);

            Mockito.when(mockContext.getSharedPreferences("MobileEnterprise_0.1", Context.MODE_PRIVATE)).thenReturn(mockSharedPrefs);
            Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

            PowerMockito.whenNew(DataProvider.class).withAnyArguments().thenReturn(mockDataProvider);

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeHistoryUri),
                    aryEq(new String[] {"_id", "title", "url", "date", "visits"}),
                    Mockito.eq("date > 0"),
                    (String[]) isNull(),
                    Mockito.eq("date" ))).
                    thenReturn(new MockCursor(projection));

            String[] bookmarksColumns = new String[] {BookmarkColumns.TITLE, BookmarkColumns.URL};
            Mockito.doReturn(new MockCursor(bookmarksColumns)).when(mockDataProvider).GetAllBookmarks();

            MockCursor bookmarksHistoryCursor = new MockCursor(new String[]{"Fake column"});
            bookmarksHistoryCursor.addRow(new Object[]{"test bookmark title1", "test bookmark url1"});

            Mockito.when(mockContentResolver.query(
                    refEq(mockChromeBookmarkUri),
                    aryEq(bookmarksColumns),
                    Mockito.eq("bookmark = 1"),
                    (String[])isNull(),
                    (String)isNull())).
                    thenReturn(bookmarksHistoryCursor);

            Settings.openDataBase(mockContext);

            BrowserDataObserver browserObserver = new BrowserDataObserver(new Handler(),
                    mockContext,
                    BrowserType.ChromeBrowser,
                    BrowserDataObserver.CHROME_BOOKMARK_URI,
                    BrowserDataObserver.CHROME_HISTORY_URI);

            Mockito.verify(mockDataProvider, Mockito.times(0)).InsertBookmark(Mockito.<BookmarkInformation>any());
        }
        catch (Exception e)
        {
            Assert.fail();
        }
    }
}