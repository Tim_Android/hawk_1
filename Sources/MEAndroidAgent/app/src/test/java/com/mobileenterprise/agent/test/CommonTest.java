package com.mobileenterprise.agent.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.TimeZone;

import junit.framework.Assert;

import com.mobileenterprise.agent.helper.Common;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Environment.class, StatFs.class})
public class CommonTest
{
    @Test
    public void testTimeZone()
    {
        String timeZoneName = "America/Los_Angeles";
        String timeZoneDisplayName = "PST, Pacific Standard Time, America/Los_Angeles";
        int rawOffset = -28800000; 
        
        TimeZone.setDefault(TimeZone.getTimeZone(timeZoneName));
        
        int rawOffsetResult = Common.GetTimeZoneRawOffset();
        String displayNameResult = Common.GetTimeZoneDisplayName();

        assertEquals(rawOffset, rawOffsetResult);
        assertEquals(displayNameResult, timeZoneDisplayName);        
    }

    @Test
    public void testPrintDeviceInfo()
    {
        PowerMockito.mockStatic(Environment.class);

        suppress(method(StatFs.class, "getBlockSize"));
        suppress(method(StatFs.class, "getAvailableBlocks"));
        suppress(method(StatFs.class, "getBlockCount"));

        try {
            StatFs mockStatFs = mock(StatFs.class);
            File mockFile = mock(File.class);

            PowerMockito.whenNew(StatFs.class).withAnyArguments().thenReturn(mockStatFs);

            PowerMockito.when(Environment.class, "getDataDirectory").thenReturn(mockFile);
            PowerMockito.when(mockFile.getPath()).thenReturn("test");

            PowerMockito.when(mockStatFs.getBlockSize()).thenReturn(1024);
            PowerMockito.when(mockStatFs.getAvailableBlocks()).thenReturn(100000);
            PowerMockito.when(mockStatFs.getBlockCount()).thenReturn(200000);

            StringBuilder stringBuilder = new StringBuilder();
            assertEquals(0, stringBuilder.length());
            Common.printDeviceInfo(stringBuilder);
            assertEquals(true, stringBuilder.length() > 0);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testPrintDeviceInfoNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Common.printDeviceInfo(stringBuilder);
        assertNull(stringBuilder);
    }

    @SuppressWarnings("null")
    public void testPrintStack()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {

            e = ex;
        }

        assertNotNull(e);

        Common.printStack(stringBuilder, e);

        assertEquals(true, stringBuilder.length() > 0);
    }

    @Test
    public void testPrintStackNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Throwable e = null;

        Common.printStack(stringBuilder, e);

        assertNull(stringBuilder);
        assertNull(e);
    }

    @SuppressWarnings("null")
    public void testPrintCauseWithoutCause()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {

            e = ex;
        }

        assertNotNull(e);

        Common.printCause(stringBuilder, e);

        assertEquals(0, stringBuilder.length());
    }

    @SuppressWarnings("null")
    public void testPrintCause()
    {		 
        StringBuilder stringBuilder = new StringBuilder();
        assertEquals(0, stringBuilder.length());

        Date testNullException = null;

        Throwable e = null;

        try {

            testNullException.getDate();

        } catch (Exception ex) {


            try {

                testNullException.getDay();

            } 
            catch (Exception exNew) {
                e = exNew;
                e.initCause(ex);				 
            }

        }

        assertNotNull(e);

        Common.printCause(stringBuilder, e);

        assertEquals(true, stringBuilder.length() > 0);
    }

    @Test
    public void testPrintCausekNullCheck()
    {		 
        StringBuilder stringBuilder = null;
        Throwable e = null;

        Common.printStack(stringBuilder, e);

        assertNull(stringBuilder);
        assertNull(e);
    }

    @Test
    public void testReadBytes() throws IOException
    {
        InputStream in = null;
        byte[] image = {};
        final int imageSize = 153237;
        try
        {
            in = new ByteArrayInputStream(new byte[imageSize]);
            image = Common.readBytes(in);

            assertEquals(imageSize, image.length);
        }
        finally
        {
            if(in != null)
            {
                in.close();
            }
        }
    }

    @Test
    public void testReadBytesEmpty()
    {
        InputStream in = null;
        byte[] image = {};
        final int imageSize = 0;

        try
        {
            in = new ByteArrayInputStream(new byte[]{});
            image = Common.readBytes(in);

            assertEquals(imageSize, image.length);
        } 
        catch (IOException e) 
        {
            Assert.fail("Shouldn't through an exception!");
        }
        finally
        {
            if(in != null)
            {
                try {
                    in.close();
                } catch (IOException e) 
                {
                    Assert.fail("Shouldn't through an exception!");
                }
            }
        }
    }

    @Test
    public void testReadBytesBad() throws IOException
    {
        InputStream in = null;
        byte[] image = {};

        try 
        {
            image = Common.readBytes(in);
            Assert.fail("Should throw an exception!");
        } 
        catch (NullPointerException ex) 
        {
            // success
        }
    }

    @Test
    public void testTelephonyManager()
    {
        Context mockContext = mock(Context.class);
        TelephonyManager mockTelephonyManager = mock(TelephonyManager.class);

        when(mockContext.getSystemService(Context.TELEPHONY_SERVICE)).thenReturn(mockTelephonyManager);
        when(mockTelephonyManager.getDeviceId()).thenReturn("Mock Device ID", null, "");
        when(mockTelephonyManager.getLine1Number()).thenReturn("12345678", null, "");

        assertEquals(Common.getDeviceID(mockContext), "Mock Device ID");
        assertEquals(Common.getDeviceID(mockContext), "Unknown");
        assertEquals(Common.getDeviceID(mockContext), "Unknown");

        assertEquals(Common.getPhoneNumber(mockContext), "12345678");
        assertEquals(Common.getPhoneNumber(mockContext), "Unknown");
        assertEquals(Common.getPhoneNumber(mockContext), "Unknown");
    }

    @Test
    public void testRemoveLastComma()
    {
        String numberList = "324324, 423423, ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, "324324, 423423");
    }

    @Test
    public void testRemoveLastCommaWithoutModification()
    {
        String numberList = "324324, 423423";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }

    @Test
    public void testRemoveLastComma_Empty()
    {
        String numberList = "";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }

    @Test
    public void testRemoveLastComma_WithoutComma()
    {
        String numberList = "324324423423 ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, numberList);
    }

    @Test
    public void testRemoveLastComma_LengthEqualPattern()
    {
        String numberList = ", ";
        
        String result = Common.removeLastComma(numberList);
        
        assertEquals(result, "");
    }
}
