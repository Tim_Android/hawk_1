package com.mobileenterprise.agent.test;

import com.mobileenterprise.agent.helper.NetworkChecker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class NetworkCheckerTest {
	
	 private static final String TEST_FILE_PREFIX = "test_";

    @Test
	 public void testIsNetworkAvailable() {
        PowerMockito.mockStatic(Log.class);

        final Context mockContext = Mockito.mock(Context.class);
        final ConnectivityManager connManagerMock = Mockito.mock(ConnectivityManager.class);
        final NetworkInfo networkInfoMock = Mockito.mock(NetworkInfo.class);

        Mockito.when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManagerMock);
        Mockito.when(connManagerMock.getActiveNetworkInfo()).thenReturn(networkInfoMock);
        Mockito.when(connManagerMock.getBackgroundDataSetting()).thenReturn(true);
        Mockito.when(networkInfoMock.isAvailable()).thenReturn(true);
        Mockito.when(networkInfoMock.isConnected()).thenReturn(true);

        NetworkChecker.IsNetworkAvailable(mockContext);
	 }

    @Test
    public void testIsNetworkAvailableGetBackgroundDataSettingFailed() {
        PowerMockito.mockStatic(Log.class);

        final Context mockContext = Mockito.mock(Context.class);
        final ConnectivityManager connManagerMock = Mockito.mock(ConnectivityManager.class);
        final NetworkInfo networkInfoMock = Mockito.mock(NetworkInfo.class);

        Mockito.when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManagerMock);
        Mockito.when(connManagerMock.getActiveNetworkInfo()).thenReturn(networkInfoMock);
        Mockito.when(connManagerMock.getBackgroundDataSetting()).thenReturn(false);
        Mockito.when(networkInfoMock.isAvailable()).thenReturn(true);
        Mockito.when(networkInfoMock.isConnected()).thenReturn(true);

        NetworkChecker.IsNetworkAvailable(mockContext);
    }

    @Test
    public void testIsNetworkAvailableIsNotAvailable() {
        PowerMockito.mockStatic(Log.class);

        final Context mockContext = Mockito.mock(Context.class);
        final ConnectivityManager connManagerMock = Mockito.mock(ConnectivityManager.class);
        final NetworkInfo networkInfoMock = Mockito.mock(NetworkInfo.class);

        Mockito.when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManagerMock);
        Mockito.when(connManagerMock.getActiveNetworkInfo()).thenReturn(networkInfoMock);
        Mockito.when(connManagerMock.getBackgroundDataSetting()).thenReturn(true);
        Mockito.when(networkInfoMock.isAvailable()).thenReturn(false);
        Mockito.when(networkInfoMock.isConnected()).thenReturn(true);

        NetworkChecker.IsNetworkAvailable(mockContext);
    }

    @Test
    public void testIsNetworkAvailableIsNotConnected() {
        PowerMockito.mockStatic(Log.class);

        final Context mockContext = Mockito.mock(Context.class);
        final ConnectivityManager connManagerMock = Mockito.mock(ConnectivityManager.class);
        final NetworkInfo networkInfoMock = Mockito.mock(NetworkInfo.class);

        Mockito.when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connManagerMock);
        Mockito.when(connManagerMock.getActiveNetworkInfo()).thenReturn(networkInfoMock);
        Mockito.when(connManagerMock.getBackgroundDataSetting()).thenReturn(true);
        Mockito.when(networkInfoMock.isAvailable()).thenReturn(true);
        Mockito.when(networkInfoMock.isConnected()).thenReturn(false);

        NetworkChecker.IsNetworkAvailable(mockContext);
    }
}
