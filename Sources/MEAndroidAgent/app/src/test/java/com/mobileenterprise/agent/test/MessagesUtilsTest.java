package com.mobileenterprise.agent.test;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.mobileenterprise.agent.helper.MessageUtils;
import com.mobileenterprise.agent.mocks.MockCursor;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.Matchers.isNull;
import static org.powermock.api.mockito.PowerMockito.mock;

@PrepareForTest({Uri.class, Log.class})
@RunWith(PowerMockRunner.class)
public class MessagesUtilsTest
{
    @Test
    public void testFindContactName_byNumber()
    {
        PowerMockito.mockStatic(Uri.class);
        final Context mockContext = mock(Context.class);
        final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);

        Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

        MockCursor mockCursor = new MockCursor(new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME});
        mockCursor.addRow(new Object[] {"Alexandr"});

        Mockito.when(mockContentResolver.query(
                (Uri)isNull(),
                aryEq(new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }),
                (String)isNull(),
                (String[])isNull(),
                (String)isNull())).
                thenReturn(mockCursor);

        Assert.assertEquals("Alexandr", MessageUtils.findContactNameByAddress("80938245234", mockContext));
    }

    @Test
    public void testFindContactName_byEmail() {
        PowerMockito.mockStatic(Uri.class);
        final Context mockContext = mock(Context.class);
        final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);

        Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

        MockCursor mockCursor = new MockCursor(new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME});
        mockCursor.addRow(new Object[] {"Alexandr"});

        Mockito.when(mockContentResolver.query(
                (Uri)isNull(),
                aryEq(new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }),
                (String)isNull(),
                (String[])isNull(),
                (String)isNull())).
                thenReturn(new MockCursor(new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME}), mockCursor);

        Assert.assertEquals("Alexandr", MessageUtils.findContactNameByAddress("email.test@test.com", mockContext));
    }

    @Test
    public void testFindContactName_NullCursor() {
        PowerMockito.mockStatic(Uri.class);
        final Context mockContext = mock(Context.class);
        final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);

        Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

        Mockito.when(mockContentResolver.query(
                (Uri)isNull(),
                aryEq(new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }),
                (String)isNull(),
                (String[])isNull(),
                (String)isNull())).
                thenReturn(null);

        Assert.assertNull(MessageUtils.findContactNameByAddress("email.test@test.com", mockContext));
    }

    @Test
    public void testFindContactName_InvalidCursor() {
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.mockStatic(Log.class);

        final Context mockContext = mock(Context.class);
        final ContentResolver mockContentResolver = Mockito.mock(ContentResolver.class);

        Mockito.when(mockContext.getContentResolver()).thenReturn(mockContentResolver);

        MockCursor mockCursor = new MockCursor(new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME});
        mockCursor.addRow(new Object[] {2456});

        Mockito.when(mockContentResolver.query(
                (Uri)isNull(),
                aryEq(new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }),
                (String)isNull(),
                (String[])isNull(),
                (String)isNull())).
                thenReturn(mockCursor);

        Assert.assertNull(MessageUtils.findContactNameByAddress("email.test@test.com", mockContext));
    }
}
