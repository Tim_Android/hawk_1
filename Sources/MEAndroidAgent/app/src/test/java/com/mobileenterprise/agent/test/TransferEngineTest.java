package com.mobileenterprise.agent.test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.mobileenterprise.agent.R;
import com.mobileenterprise.agent.helper.CallInformation;
import com.mobileenterprise.agent.helper.Common;
import com.mobileenterprise.agent.helper.DataProvider;
import com.mobileenterprise.agent.helper.MMSAttachment;
import com.mobileenterprise.agent.helper.MessageInformation;
import com.mobileenterprise.agent.helper.RestEasy;
import com.mobileenterprise.agent.helper.SQLiteHelper;
import com.mobileenterprise.agent.helper.Settings;
import com.mobileenterprise.agent.helper.TransferEngine;
import com.mobileenterprise.agent.mocks.MockCursor;
import com.mobileenterprise.agent.mocks.MockEntity;
import com.mobileenterprise.agent.mocks.MockHttpResponse;
import com.mobileenterprise.agent.mocks.MockSharedPreferences;
import com.mobileenterprise.agent.mocks.MockStatusLine;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import android.test.AndroidTestCase;
import android.test.MoreAsserts;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataProvider.class, Log.class, RestEasy.class})
public class TransferEngineTest {
    private Context mockContext;
    private SQLiteHelper mockHelper;
    private SQLiteDatabase mockDB;
    private ContentValues mockContentValues;
    private DataProvider localDB;

    @Before
    public void initialize() {
        this.mockContext = mock(Context.class);
        this.mockHelper = Mockito.mock(SQLiteHelper.class);
        this.mockDB = Mockito.mock(SQLiteDatabase.class);
        this.mockContentValues = Mockito.mock(ContentValues.class);

        PowerMockito.mockStatic(RestEasy.class);
        PowerMockito.mockStatic(Log.class);

        try {
            PowerMockito.whenNew(SQLiteHelper.class).withArguments(this.mockContext).thenReturn(mockHelper);
            PowerMockito.whenNew(ContentValues.class).withAnyArguments().thenReturn(mockContentValues);
            when(this.mockHelper.getWritableDatabase()).thenReturn(this.mockDB);
        } catch (Exception e) {
            Assert.fail();
        }

        this.localDB = new DataProvider(this.mockContext);
    }


    @Test
    public void testSendMultiPartData() throws IOException {
        try {
            MockSharedPreferences prefsMock = new MockSharedPreferences();
            File mockFile = mock(File.class);

            when(this.mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(prefsMock);
            TelephonyManager mockTelephonyManager = mock(TelephonyManager.class);

            PowerMockito.when(mockContext.getSystemService(Context.TELEPHONY_SERVICE)).thenReturn(mockTelephonyManager);
            PowerMockito.when(mockTelephonyManager.getDeviceId()).thenReturn("Mock Device ID", null, "");
            when(mockContext.getDatabasePath(anyString())).thenReturn(mockFile);
            when(mockFile.length()).thenReturn(1000L);

            Settings.openDataBase(this.mockContext);

            HttpResponse response = new MockHttpResponse();
            ProtocolVersion mockProtocolVersion = Mockito.mock(ProtocolVersion.class);
            response.setStatusLine(new MockStatusLine(mockProtocolVersion, 200, ""));

            PowerMockito.when(RestEasy.class, "doPost", any(String.class), any(JSONObject.class), refEq(this.mockContext)).thenReturn(response);

            MockCursor callInfoCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, "_fromNumber", "_fromNumberName", "_fromNumberType", "_toNumber", "_toNumberName", "_toNumberType", "_type", "_date", "_duration"});
            callInfoCursor.addRow(new Object[]{1, "+38050334565", "Stephen King", "Mobile", "Device", "", "", "Incoming", new Date().getTime(), 33});

            MockCursor crashLogCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, "_date", "_data"});
            crashLogCursor.addRow(new Object[]{1, new Date().getTime(), "Crash report data"});

            MockCursor smsCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_ADRESS, SQLiteHelper.MT_COLUMN_CONTACT_NAME, SQLiteHelper.MT_COLUMN_BODY, SQLiteHelper.MT_COLUMN_TYPE,
                    SQLiteHelper.MT_COLUMN_DATE});
            smsCursor.addRow(new Object[]{1, "+380502341115", "Contact Name", "SMS Body", "Inbox", new Date().getTime()});

            MockCursor gpsCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.GPS_COLUMN_PROVIDER, SQLiteHelper.GPS_COLUMN_ACCURACY, SQLiteHelper.GPS_COLUMN_BEARING,
                    SQLiteHelper.GPS_COLUMN_SPEED, SQLiteHelper.GPS_COLUMN_LATITUDE, SQLiteHelper.GPS_COLUMN_LONGITUDE, SQLiteHelper.GPS_COLUMN_ALTITUDE, SQLiteHelper.GPS_COLUMN_TIME});
            gpsCursor.addRow(new Object[]{1, LocationManager.NETWORK_PROVIDER, 10f, 47.25f, 40f, 47.4356231312, 54.2132434, 10.4356, new Date().getTime()});

            MockCursor browserHistoryCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_TITLE, SQLiteHelper.BOOKMARK_COLUMN_DATE,
                    SQLiteHelper.BOOKMARK_COLUMN_VISITS});
            browserHistoryCursor.addRow(new Object[]{1, "Test URL", "Test title", new Date().getTime(), 25});

            MockCursor browserBookmarksCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.BOOKMARK_COLUMN_TITLE,
                    SQLiteHelper.BOOKMARK_COLUMN_URL, SQLiteHelper.BOOKMARK_COLUMN_ISSENT});
            browserBookmarksCursor.addRow(new Object[]{1, "Bookmark Title", "Bookmark URL", 0});

            MockCursor mmsCursor = new MockCursor(new String[]{SQLiteHelper.MT_COLUMN_ID, SQLiteHelper.MT_COLUMN_ADRESS, SQLiteHelper.MT_COLUMN_CONTACT_NAME, SQLiteHelper.MT_COLUMN_BODY,
                    SQLiteHelper.MT_COLUMN_MMS_TYPE,
                    SQLiteHelper.MT_COLUMN_DATE, SQLiteHelper.MT_COLUMN_SUBJECT});
            mmsCursor.addRow(new Object[]{1, "Address", "Contact Name", "Body", "Incoming", new Date().getTime(), "Subject"});

            final HashMap<String, MockCursor> data = new HashMap<String, MockCursor>();
            data.put(SQLiteHelper.CALLLOGSTABLE_NAME, callInfoCursor);
            data.put(SQLiteHelper.CRASHLOGSTABLE_NAME, crashLogCursor);
            data.put(SQLiteHelper.SMSTABLE_NAME, smsCursor);
            data.put(SQLiteHelper.GPSTABLE_NAME, gpsCursor);
            data.put(SQLiteHelper.BROWSER_HISTORY_TABLE_NAME, browserHistoryCursor);
            data.put(SQLiteHelper.BOOKMARK_TABLE_NAME, browserBookmarksCursor);
            data.put(SQLiteHelper.MMSTABLE_NAME, mmsCursor);

            TransferEngine transfer = new TransferEngine(mockContext);

            // Query table conent
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    String tableName = (String) invocation.getArguments()[0];
                    if (data.containsKey(tableName)) {
                        return data.get(tableName);
                    } else {
                        return new MockCursor();
                    }
                }
            }).when(mockDB).query(anyString(), (String[]) isNull(), anyString(), (String[]) isNull(), (String) isNull(), (String) isNull(), (String) isNull());

            transfer.ResendAllData();
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testParseResponse() throws JSONException {
        String answer;

        answer = Common.parseServerResponse("{\"state\": \"1\",\"agentId\":\"6265460\"}", this.mockContext);
        Assert.assertEquals("6265460", answer);

        answer = Common.parseServerResponse("{\"state\": \"0\",\"agentId\":\"null\"}", this.mockContext);
        Assert.assertEquals("0", answer);

        answer = Common.parseServerResponse("{\"state\": \"2\",\"agentId\":\"null\"}", this.mockContext);
        Assert.assertEquals("2", answer);
    }

    @Test
    public void testRequestAgentID() throws Exception {

        MockSharedPreferences prefsMock = new MockSharedPreferences();
        File mockFile = mock(File.class);

        when(this.mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(prefsMock);
        TelephonyManager mockTelephonyManager = mock(TelephonyManager.class);

        PowerMockito.when(mockContext.getSystemService(Context.TELEPHONY_SERVICE)).thenReturn(mockTelephonyManager);
        PowerMockito.when(mockTelephonyManager.getDeviceId()).thenReturn("Mock Device ID", null, "");
        when(mockContext.getDatabasePath(anyString())).thenReturn(mockFile);
        when(mockFile.length()).thenReturn(1000L);

        Settings.openDataBase(this.mockContext);


        HttpResponse response = new MockHttpResponse();
        ProtocolVersion mockProtocolVersion = Mockito.mock(ProtocolVersion.class);
        response.setStatusLine(new MockStatusLine(mockProtocolVersion, 200, ""));

        response.setEntity(new MockEntity());

        try {
            PowerMockito.when(RestEasy.class, "doPost", any(String.class), any(JSONObject.class), refEq(this.mockContext)).thenReturn(response);
            PowerMockito.when(RestEasy.class, "convertStreamToString", any(InputStream.class)).thenReturn("{\"state\": \"1\",\"agentId\":\"6265460\"}");
            System.out.println(response.getStatusLine().getStatusCode());

        } catch (Exception e) {
            Assert.fail();
        }

        TransferEngine transfer = new TransferEngine(mockContext);
        transfer.GetAgentID("nastiushar1@gmail.com", "12345678");
        Settings.getAgentId();
    }
}
