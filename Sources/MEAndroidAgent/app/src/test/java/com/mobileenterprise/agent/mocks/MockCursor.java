package com.mobileenterprise.agent.mocks;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

import org.apache.http.MethodNotSupportedException;

import java.util.ArrayList;

public class MockCursor implements Cursor {
    private String[] columns;
    private ArrayList<Object[]> rows;
    private int currentIndex = -1;
    private boolean isClosed = false;

    public MockCursor()
    {
        this.columns = new String[] {};
        this.rows = new ArrayList<Object[]>();
    }

    public MockCursor(String[] columns)
    {
        this.columns = columns.clone();
        this.rows = new ArrayList<Object[]>();
    }

    public void addRow(Object[] row)
    {
        rows.add(row.clone());
    }

    @Override
    public int getCount() {
        return this.rows.size();
    }

    @Override
    public int getPosition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean move(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean moveToPosition(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean moveToFirst() {
        if (!this.rows.isEmpty()) {
            currentIndex = 0;
            return true;
        }

        return false;
    }

    @Override
    public boolean moveToLast() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean moveToNext() {
        if (!this.rows.isEmpty()) {
            if (currentIndex != -1 && currentIndex < this.rows.size()) {
                ++currentIndex;
                return currentIndex < this.rows.size();
            }
        }

        return false;
    }

    @Override
    public boolean moveToPrevious() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isFirst() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isLast() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isBeforeFirst() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAfterLast() {
        return false;
    }

    @Override
    public int getColumnIndex(String s) {
        for (int i = 0; i < this.columns.length; ++i) {
            if (this.columns[i].equals(s)) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int getColumnIndexOrThrow(String s) throws IllegalArgumentException {
        for (int index = 0; index < this.columns.length; ++index) {
            if (s.equals(this.columns[index])) {
                return index;
            }
        }

        throw new IllegalArgumentException();
    }

    @Override
    public String getColumnName(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String[] getColumnNames() {
        return new String[0];
    }

    @Override
    public int getColumnCount() {
        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] getBlob(int i) {
        return (byte[])(this.rows.get(currentIndex)[i]);
    }

    @Override
    public String getString(int i) {
        return (String)(this.rows.get(currentIndex)[i]);
    }

    @Override
    public void copyStringToBuffer(int i, CharArrayBuffer charArrayBuffer) {
        throw new UnsupportedOperationException();
    }

    @Override
    public short getShort(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getInt(int i) {
        return (Integer)(this.rows.get(currentIndex)[i]);
    }

    @Override
    public long getLong(int i) {
        return (Long)(this.rows.get(currentIndex)[i]);
    }

    @Override
    public float getFloat(int i) {
        return (Float)(this.rows.get(currentIndex)[i]);
    }

    @Override
    public double getDouble(int i) {
        return (Double) (this.rows.get(currentIndex)[i]);
    }

    @Override
    public int getType(int i) {
        return 0;
    }

    @Override
    public boolean isNull(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deactivate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean requery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        this.isClosed = true;
    }

    @Override
    public boolean isClosed() {
        return this.isClosed;
    }

    @Override
    public void registerContentObserver(ContentObserver contentObserver) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void unregisterContentObserver(ContentObserver contentObserver) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setNotificationUri(ContentResolver contentResolver, Uri uri) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Uri getNotificationUri() {
        return null;
    }

    @Override
    public boolean getWantsAllOnMoveCalls() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setExtras(Bundle bundle) {

    }

    @Override
    public Bundle getExtras() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle respond(Bundle bundle) {
        throw new UnsupportedOperationException();
    }
}
